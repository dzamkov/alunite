﻿using System;
using System.Diagnostics;

using Alunite.Data;
using Alunite.Data.Geometry;
using Alunite.Data.Probability;
using Alunite.Resources;
using Alunite.Reactive;
using Alunite.Universe;
using Alunite.Universe.Grids;
using Alunite.Universe.Generation;
using Alunite.Graphics;

namespace Alunite.Game
{
    class Program
    {
        /// <summary>
        /// Program main entry point.
        /// </summary>
        public static void Main(string[] args)
        {
            // Register assets
            Asset.Register(typeof(Util).Assembly);
            Asset.Register(typeof(Program).Assembly);

            var seed = Data.Probability.Seed.Initial;
            
            ViewBuilder view = new ViewBuilder();
            var world = new Delay<World>(() =>
            {
                var init = view.Initializer.Value;
                var w = _createWorldPrototype().Instantiate(init);
                /* w.TryGetChunk(init, w.GetRegion(new WorldTopology.Region(0, 0, 0, 0)), out WorldChunk chunk, out _);
                using (chunk.GetBlock(new ShortVector3i(3, 3, 0)).Edit(init, out BlockEditor block))
                {
                    var subs = Substances.Mud;
                    var mat = Materials.Dirt;
                    block.Prototype = subs;
                    block.Surface(Dir3.NegX).Prototype = mat;
                    block.Surface(Dir3.PosX).Prototype = mat;
                    block.Surface(Dir3.NegY).Prototype = mat;
                    block.Surface(Dir3.PosZ).Prototype = Materials.Mud;
                    block.Face(Axis3.Z, Dir1.Neg).Prototype = FacePrototype.Fill(subs);
                    block = block.Neighbor(Dir3.PosY);
                    block.Prototype = subs;
                    block.Surface(Dir3.NegX).Prototype = mat;
                    block.Surface(Dir3.PosX).Prototype = mat;
                    block.Surface(Dir3.PosY).Prototype = mat;
                    block.Surface(Dir3.PosZ).Prototype = Materials.Mud;
                    block.Face(Axis3.Y, Dir1.Neg).Prototype = FacePrototype.Fill(subs);
                    block.Face(Axis3.Z, Dir1.Neg).Prototype = FacePrototype.Fill(subs);
                    block = block.Neighbor(Dir3.PosX);
                    block.Prototype = subs;
                    block.Surface(Dir3.PosX).Prototype = mat;
                    block.Surface(Dir3.NegY).Prototype = mat;
                    block.Surface(Dir3.PosY).Prototype = mat;
                    block.Surface(Dir3.PosZ).Prototype = Materials.Mud;
                    block.Face(Axis3.X, Dir1.Neg).Prototype = FacePrototype.Fill(subs);
                    block.Face(Axis3.Z, Dir1.Neg).Prototype = FacePrototype.Fill(subs);
                    block = block.Neighbor(Dir3.NegX);
                    block = block.Neighbor(Dir3.NegX);
                    block.Prototype = subs;
                    block.Surface(Dir3.NegX).Prototype = mat;
                    block.Surface(Dir3.NegY).Prototype = mat;
                    block.Surface(Dir3.PosY).Prototype = mat;
                    block.Surface(Dir3.PosZ).Prototype = Materials.Mud;
                    block.Face(Axis3.X, Dir1.Pos).Prototype = FacePrototype.Fill(subs);
                    block.Face(Axis3.Z, Dir1.Neg).Prototype = FacePrototype.Fill(subs);
                    block = block.Neighbor(Dir3.PosX);
                    block = block.Neighbor(Dir3.PosZ);
                    block.Prototype = subs;
                    block.Surface(Dir3.NegX).Prototype = mat;
                    block.Surface(Dir3.PosX).Prototype = mat;
                    block.Surface(Dir3.NegY).Prototype = mat;
                    block.Surface(Dir3.PosY).Prototype = mat;
                    block.Surface(Dir3.PosZ).Prototype = Materials.Mud;
                    block.Face(Axis3.Z, Dir1.Neg).Prototype = FacePrototype.Fill(subs);

                    block = block.Neighbor(Dir3.NegZ);
                    block = block.Neighbor(Dir3.PosY);
                    block = block.Neighbor(Dir3.PosY);
                    block = block.Neighbor(Dir3.NegZ);
                    block.Surface(Dir3.NegX).Opposing.Prototype = mat;
                    block.Surface(Dir3.PosX).Opposing.Prototype = mat;
                    block.Surface(Dir3.NegY).Opposing.Prototype = mat;
                    block.Surface(Dir3.PosY).Opposing.Prototype = mat;
                    block.Surface(Dir3.NegZ).Opposing.Prototype = mat;
                    block.Face(Axis3.Z, Dir1.Pos).Prototype = FacePrototype.Undefined;

                    block = block.Neighbor(Dir3.PosZ);
                    block = block.Neighbor(Dir3.PosZ);
                    block = block.Neighbor(Dir3.PosZ);
                    block = block.Neighbor(Dir3.PosZ);
                    block = block.Neighbor(Dir3.NegY);
                    block = block.Neighbor(Dir3.NegX);
                    block.Prototype = subs;
                    block.Surface(Dir3.NegX).Prototype = mat;
                    block.Surface(Dir3.PosX).Prototype = mat;
                    block.Surface(Dir3.NegY).Prototype = mat;
                    block.Surface(Dir3.PosY).Prototype = mat;
                    block.Surface(Dir3.NegZ).Prototype = mat;
                    block.Surface(Dir3.PosZ).Prototype = mat;
                } */
                return w;
            });
            view.InitialSpan = new Delay<FreeLookSpan>(() =>
                new FreeLookSpan
                {
                    Start = Time.Origin,
                    Initial = new FreeLook
                    {
                        Position = new Vector3(0.2, 0.2, 0.2),
                        Theta = (5.0 / 4.0) * Scalar.Pi,
                        Phi = -0.5
                    }
                });
            view.InitialFrame = new Delay<Space>(() => world.Value.GetSpace(world.Value.GetRegion(new WorldTopology.Region(0, 0, 0, 0))));
            view.ViewToDevice = new Delay<Dynamic<Projection>>(() =>
            {
                Dynamic<int> width = view.Width.Value;
                Dynamic<int> height = view.Height.Value;
                return Dynamic.Compute(obs => Projection.Perspective(0.2 * width[obs] / height[obs], 0.2, 0.2));
            });
            
            MainWindow window = new MainWindow(view);
            window.Run();
        }

        private static Universe.WorldPrototype _createWorldPrototype()
        {
            uint scale = 6;
            WorldTopology topology = new WorldTopology(
                new AxialDuet3<WorldTopology.Region>(
                    new WorldTopology.Region(0, -1, 0, 0),
                    new WorldTopology.Region(0, 1, 0, 0),
                    new WorldTopology.Region(0, 0, -1, 0),
                    new WorldTopology.Region(0, 0, 1, 0),
                    new WorldTopology.Region(0, 0, 0, -1),
                    new WorldTopology.Region(0, 0, 0, 1)));
            UglProcedure proc = UglProcedure.Leaf(default);
            while (proc.Scale < scale)
                proc = UglProcedure.Split(Octet.Uniform(proc));
            UglAssembly assem = proc.Compile();
            return new WorldPrototype(
                new WorldCharter(topology, (int)scale), assem,
                Data.Probability.Seed.Initial);
        }
    }
}