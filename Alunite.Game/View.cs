﻿using System;
using System.Collections.Generic;
using System.Threading;

using Alunite.Data;
using Alunite.Data.Geometry;
using Alunite.Data.Probability;
using Alunite.UI;
using Alunite.Reactive;
using Alunite.Graphics;
using Alunite.Resources;
using Alunite.Universe;
using Alunite.Universe.Grids;

using OpenTK.Graphics.OpenGL;

namespace Alunite.Game
{
    /// <summary>
    /// A control which provides a view of the game world.
    /// </summary>
    public sealed class View : IControl, IMouseListener, IMouseClickHandler
    {
        internal readonly State<FreeLookSpan> _freeLookSpan;
        internal readonly State<Space> _space;
        public Dynamic<Vector2i> TopLeft { get; }
        public Dynamic<int> Width { get; }
        public Dynamic<int> Height { get; }
        public Dynamic<Vector3> Movement { get; }
        public Dynamic<FreeLook> FreeLook { get; }
        public Dynamic<Viewport> Viewport { get; }
        internal View(ViewBuilder builder)
        {
            _freeLookSpan = builder._freeLookSpan.Value;
            _space = new State<Space>(builder.Initializer.Value, builder.InitialFrame.Value);
            TopLeft = builder.TopLeft.Value;
            Width = builder.Width.Value;
            Height = builder.Height.Value;
            Movement = builder.Movement.Value;
            FreeLook = builder.FreeLook.Value;
            var viewToDevice = builder.ViewToDevice.Value;
            Viewport = Dynamic.Compute(obs => new Viewport
            {
                WorldToView = FreeLook[obs].WorldToView,
                ViewToDevice = viewToDevice[obs],
                Target = new Box2i(0, Width[obs], 0, Height[obs])
            });
        }

        private static PlaneMaterial _mud =
            PlaneMaterial.Build(
                new MaterialTexture2(
                    true, true, true,
                    BigImage2.FromAsset(Path.Base / "Mud" / "Albedo.png", true).AsPaint,
                    BigImage2.FromAsset(Path.Base / "Mud" / "Normal.png", false).AsColor.ToNormal(true),
                    Image2.Singleton<Scalar>(0),
                    BigImage2.FromAsset(Path.Base / "Mud" / "Roughness.png", false).AsScalar),
                new Vector2(1, 1), true);

        private static PlaneMaterial _grass =
            PlaneMaterial.Build(
                new MaterialTexture2(
                    true, true, true,
                    BigImage2.FromAsset(Path.Base / "Grass" / "Albedo.png", true).AsPaint,
                    BigImage2.FromAsset(Path.Base / "Grass" / "Normal.png", false).AsColor.ToNormal(true),
                    Image2.Singleton<Scalar>(0),
                    BigImage2.FromAsset(Path.Base / "Grass" / "Roughness.png", false).AsScalar),
                new Vector2(1, 1), true);

        private static PlaneMaterial _dirt =
            PlaneMaterial.Build(
                new MaterialTexture2(
                    true, true, true,
                    BigImage2.FromAsset(Path.Base / "Dirt" / "Albedo.png", true).AsPaint,
                    BigImage2.FromAsset(Path.Base / "Dirt" / "Normal.png", false).AsColor.ToNormal(true),
                    Image2.Singleton<Scalar>(0),
                    BigImage2.FromAsset(Path.Base / "Dirt" / "Roughness.png", false).AsScalar),
                new Vector2(1, 1), true);

        private static Model _testModel = 
            Motion3.Translate(0.5, 0.5, 0.2) *
            (Rotation3)Rotation3i.PosZ_PosX_PosY *
            Model.FromAsset(Path.Base / "Models" / "Test" / "BoomBox.gltf");

        private static GltfAsset _corners = GltfAsset.FromAsset(Path.Base / "Corners.glb");

        private static BigImage2<Radiance> _skybox = BigImage2.FromAsset(Path.Base / "Skybox.hdr").AsRadiance;
        
        void IControl.Draw(Observer obs, IDrawer2 drawer)
        {
            // GL.PolygonMode(MaterialFace.Front, PolygonMode.Line);

            // TODO: Respect placement of control
            Skybox skybox = new Skybox { Image = _skybox };
            using (((IDynamicDrawer2)drawer).ProjectRender(skybox, Viewport, default, out IRenderer renderer))
            {
                var sampler = new SeedSampler(Seed.Initial);

                var corner_0 = _corners.Nodes["Corner_0"].ImportLocal();
                var corner_1 = _corners.Nodes["Corner_1"].ImportLocal();
                var corner_2 = _corners.Nodes["Corner_2"].ImportLocal();
                var corner_3 = _corners.Nodes["Corner_3"].ImportLocal();
                var corner_5 = _corners.Nodes["Corner_5"].ImportLocal();
                var corner_7 = _corners.Nodes["Corner_7"].ImportLocal();

                Scalar s = 1 / 16.0;
                SpanList<Graphics.Material> list(params Graphics.Material[] mats) => List.Of(mats);
                var model =
                    corner_5.Apply(list(_dirt, _dirt, _dirt, _mud)) +
                    Motion3.Translate(-s, 0, 0) * corner_3.Apply(list(_mud)) +
                    Motion3.Translate(0, s, 0) * Rotation3i.PosY_NegX_PosZ * corner_2.Apply(list(_dirt, _dirt, _mud)) +
                    Motion3.Translate(s, s, 0) * Rotation3i.PosY_NegX_PosZ * corner_0.Apply(list(_dirt, _dirt, _mud)) +
                    Motion3.Translate(0, 0, s) * Rotation3i.NegY_PosX_PosZ * corner_0.Apply(list(_dirt, _dirt, _grass)) +
                    Motion3.Translate(s, 0, 0) * Rotation3i.PosZ_PosY_NegX * corner_1.Apply(list(_dirt, _dirt)) +
                    Motion3.Translate(s, 0, s) * corner_0.Apply(list(_dirt, _dirt, _grass)) +
                    Motion3.Translate(-s, s, 0) * Rotation3i.PosY_NegX_PosZ * corner_7.Apply(list(_mud, _mud, _mud)) +
                    Motion3.Translate(-s, s, s) * Rotation3i.PosY_NegX_PosZ * corner_0.Apply(list(_mud, _mud, _mud));

                renderer.Render(model.Instantiate(sampler));
                // renderer.Render(_testModel);
            }

            // Render world
            /* _space[obs].Render(obs, drawer, Viewport);
            drawer.Draw(_overdrawable, obs); */
        }
        
        public void Update(Transactor trans)
        {
            Time updateRate = Time.FromSeconds(0.01);
            FreeLookSpan span = _freeLookSpan[trans];
            var updateTime = span.Start + updateRate;

            // Find time at frame switch
            Space frame = _space[trans];
            Box3 bounds = frame.Bounds;
            Scalar arg = new Ray3(span.Initial.Position, span.Velocity).IntersectExit(bounds, out _);
            bool shouldSwitch = false;
            if (arg < Scalar.Inf)
            {
                Time switchTime = span.Start + Time.FromSeconds(arg);
                if (switchTime <= updateTime)
                {
                    shouldSwitch = true;
                    updateTime = switchTime;
                }
            }

            // Update if needed
            if (trans.Time >= updateTime)
            {
                FreeLook freeLook = span[trans.Time];
                if (shouldSwitch)
                {
                    _space[trans] = frame.Transfer(trans, freeLook.Position, 0, out SpaceTransform frameTrans);
                    freeLook.Position = frameTrans.Spatial * freeLook.Position;
                }
                _freeLookSpan[trans] = freeLook.Extend(trans.Time, freeLook.GetVelocity(Movement[trans]));
            }
        }

        MouseHandler IMouseSurface.GetHandler(Observer obs, Vector2i point)
        {
            MouseHandler handler = MouseHandler.None;
            handler[UI.MouseButton.Left] = new MouseButtonHandler(this, null);
            return handler;
        }

        void IMouseListener.Move(Transactor trans, Vector2 delta)
        {
            FreeLook freeLook = FreeLook[trans];
            delta *= 0.003;
            freeLook.Rotate(-delta.X, -delta.Y);
            _freeLookSpan[trans] = freeLook.Extend(trans.Time, freeLook.GetVelocity(Movement[trans]));
        }

        void IMouseListener.ButtonDown(Transactor trans, MouseButton button)
        {

        }

        void IMouseListener.ButtonUp(Transactor trans, MouseButton button, out bool exit)
        {
            exit = false;
        }

        void IMouseClickHandler.Click(Transactor trans, IMouse mouse, MouseButton button, Vector2i pos, out IMouseListener listener)
        {
            listener = this;
        }
    }

    public sealed class ViewBuilder : IControlBuilder, IBuilder<View>
    {
        internal readonly Delay<State<FreeLookSpan>> _freeLookSpan;
        private Delay<Space> _initialFrame;
        private Delay<Transactor> _init;
        private Delay<IInterface> _interface;
        private Delay<FreeLookSpan> _state;
        private Delay<Dynamic<Vector2i>> _topLeft;
        private Delay<Dynamic<int>> _width;
        private Delay<Dynamic<int>> _height;
        private Delay<Dynamic<Projection>> _viewToDevice;
        public ViewBuilder()
        {
            Movement = new Delay<Dynamic<Vector3>>(() =>
            {
                var keyboard = Interface.Value.Keyboard;
                keyboard.TryGetStandardKeyIsDown(StandardKey.Fixed('W'), out Dynamic<bool> wIsDown);
                keyboard.TryGetStandardKeyIsDown(StandardKey.Fixed('A'), out Dynamic<bool> aIsDown);
                keyboard.TryGetStandardKeyIsDown(StandardKey.Fixed('S'), out Dynamic<bool> sIsDown);
                keyboard.TryGetStandardKeyIsDown(StandardKey.Fixed('D'), out Dynamic<bool> dIsDown);
                keyboard.TryGetStandardKeyIsDown(StandardKey.LShift, out Dynamic<bool> shiftIsDown);
                keyboard.TryGetStandardKeyIsDown(StandardKey.Space, out Dynamic<bool> spaceIsDown);
                keyboard.TryGetStandardKeyIsDown(StandardKey.LControl, out Dynamic<bool> ctrlIsDown);
                return Dynamic.Compute(obs =>
                {
                    Scalar forward = 0;
                    Scalar left = 0;
                    Scalar up = 0;
                    if (wIsDown[obs])
                        forward += 1;
                    if (aIsDown[obs])
                        left += 1;
                    if (sIsDown[obs])
                        forward -= 1;
                    if (dIsDown[obs])
                        left -= 1;
                    if (spaceIsDown[obs])
                        up += 1;
                    if (ctrlIsDown[obs])
                        up -= 1;
                    Scalar speed = 1.5;
                    if (shiftIsDown[obs])
                        speed = 3;
                    Vector3 vec = new Vector3(forward, left, up);
                    Scalar length = vec.Length;
                    if (length > 0)
                        return vec * (speed / length);
                    else
                        return Vector3.Zero;
                });
            });
            _freeLookSpan = new Delay<State<FreeLookSpan>>(() =>
            {
                return new State<FreeLookSpan>(Initializer.Value, InitialSpan.Value);
            });
            FreeLook = new Delay<Dynamic<FreeLook>>(() =>
            {
                return _freeLookSpan.Value.Animate<FreeLookSpan, FreeLook>();
            });
        }

        public Delay<FreeLookSpan> InitialSpan
        {
            get { return Delay.Get(ref _state); }
            set { Delay.Set(ref _state, value); }
        }

        public Delay<Space> InitialFrame
        {
            get { return Delay.Get(ref _initialFrame); }
            set { Delay.Set(ref _initialFrame, value); }
        }

        public Delay<Transactor> Initializer
        {
            get { return Delay.Get(ref _init); }
            set { Delay.Set(ref _init, value); }
        }

        public Delay<IInterface> Interface
        {
            get { return Delay.Get(ref _interface); }
            set { Delay.Set(ref _interface, value); }
        }
        
        public Delay<Dynamic<Vector2i>> TopLeft
        {
            get { return Delay.Get(ref _topLeft); }
            set { Delay.Set(ref _topLeft, value); }
        }

        public Delay<Dynamic<int>> Width
        {
            get { return Delay.Get(ref _width); }
            set { Delay.Set(ref _width, value); }
        }

        public Delay<Dynamic<int>> Height
        {
            get { return Delay.Get(ref _height); }
            set { Delay.Set(ref _height, value); }
        }
        
        public Delay<Dynamic<Projection>> ViewToDevice
        {
            get { return Delay.Get(ref _viewToDevice); }
            set { Delay.Set(ref _viewToDevice, value); }
        }

        public Delay<Dynamic<Vector3>> Movement { get; }

        public Delay<Dynamic<FreeLook>> FreeLook { get; }

        public View Finish()
        {
            return new View(this);
        }

        IControl IBuilder<IControl>.Finish()
        {
            return Finish();
        }
    }

    /// <summary>
    /// Describes the state of the camera during free-look.
    /// </summary>
    public struct FreeLook
    {
        /// <summary>
        /// The position of the camera.
        /// </summary>
        public Vector3 Position;

        /// <summary>
        /// The angle of the camera about the vertical axis.
        /// </summary>
        public Scalar Theta;

        /// <summary>
        /// The angle of the camera above the horizontal plane.
        /// </summary>
        public Scalar Phi;

        /// <summary>
        /// The forward direction of the camera.
        /// </summary>
        public Vector3 Forward
        {
            get
            {
                Scalar cosPhi = Scalar.Cos(Phi);
                return new Vector3(Scalar.Cos(Theta) * cosPhi, Scalar.Sin(Theta) * cosPhi, Scalar.Sin(Phi));
            }
        }

        /// <summary>
        /// The transformation from world space to the view space of the camera.
        /// </summary>
        public Motion3 WorldToView
        {
            get
            {
                return Motion3.LookAt(Position, Position + Forward, new Vector3(0, 0, 1));
            }
        }

        /// <summary>
        /// The maximum allowable value and minimum allowable negation for <see cref="Phi"/>.
        /// </summary>
        public static readonly Scalar MaxPhi = (Scalar.Pi / 2) * 0.9;

        /// <summary>
        /// Rotates the camera by the given angles.
        /// </summary>
        public void Rotate(Scalar theta, Scalar phi)
        {
            Theta += theta;
            Phi += phi;
            if (Phi > MaxPhi)
                Phi = MaxPhi;
            if (Phi < -MaxPhi)
                Phi = -MaxPhi;
        }

        /// <summary>
        /// Gets the target velocity of the camera given the target movement in camera space.
        /// </summary>
        public Vector3 GetVelocity(Vector3 movement)
        {
            return WorldToView.Linear.Inverse * movement;
        }

        /// <summary>
        /// Constructs a <see cref="FreeLookSpan"/> representing the continuation of this
        /// starting at the given time, subject to the given velocity.
        /// </summary>
        public FreeLookSpan Extend(Time start, Vector3 vel)
        {
            return new FreeLookSpan
            {
                Start = start,
                Initial = this,
                Velocity = vel
            };
        }
    }

    /// <summary>
    /// Describes the progression of a <see cref="FreeLook"/> state over a short span of time.
    /// </summary>
    public struct FreeLookSpan : ISpan<FreeLook>
    {
        public Time Start;
        public FreeLook Initial;
        public Vector3 Velocity;

        public FreeLook this[Time time]
        {
            get
            {
                Scalar delta = time.Seconds - Start.Seconds;
                return new FreeLook
                {
                    Position = Initial.Position + Velocity * delta,
                    Theta = Initial.Theta,
                    Phi = Initial.Phi
                };
            }
        }

        Time ISpan<FreeLook>.Start
        {
            get
            {
                return Start;
            }
        }

        FreeLook ISpan<FreeLook>.Initial
        {
            get
            {
                return Initial;
            }
        }
    }
}