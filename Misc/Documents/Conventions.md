# Architecture

# Naming

## Types

- Concrete types should be named after what they are instead of what they do. Thus, they should not be named as a verb + er suffix, unless this is the common name for an object (e.g. "Player").

- Conversely, helper types and interfaces should be named after what they allow the client to do. For example, an `IBinaryReader` is an interface that allows the client to read binary data. Contrary to the literal meaning of the phrase, it is *not* an interface to some entity which is independently reading binary data.

- A numeric postfix to a type indicates the dimensionality of that type. For example, a `Vector3` is a vector in three-dimensional space. An `Int32` is an integer of 32 bits.

- The use of an underscore in a type signifies that the type is a simple [product](https://en.wikipedia.org/wiki/Product_type) of the named types or components surronding the underscores. For example, a `Position_Normal_Color` is a plain struct containing a position, normal and color component. The individual components must be meaningful independent of the others. For example, it is not okay to combine an array and an index into the array in this manner, since the index is meaningless by itself.

- Underscores may also be used in type parameters to introduce subscripts, as they would be used in math. For example, `T_X` refers to a type that is relevant along the X axis.

## Members

## Locals

