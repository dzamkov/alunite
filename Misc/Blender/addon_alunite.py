bl_info = {
    "name": "Alunite Tools",
    "blender": (2, 80, 0)
}

import bpy
import bmesh

def simplest_between(x, y):
    """
    Simplest fraction between x and y, inclusive of x and y.
    """
    # Reduce to case 0 < x <= y
    (x, y) = (min(x, y), max(x, y))
    if y < 0:
        (n, d) = simplest_between(-y, -x)
        return (-n, d)
    elif x <= 0:
        return (0, 1)

    # Find the simplest fraction in [s/t, u/v]
    ((s, t), (u, v)) = (x.as_integer_ratio(), y.as_integer_ratio())
    (a, b, c, d) = (1, 0, 0, 1)
    while True:
        q = (s - 1) // t
        (s, t, u, v) = (v, u - q * v, t, s - q * t)
        (a, b, c, d) = (b + q * a, a, d + q * c, c)
        if t >= s:
            return (a + b, c + d)

def rationalize(values, eps = 0.000001, max_denom = 400):
    """
    Given a list of scalar values, attempts to coalesce similar values and approximate
    them as simple rationals.
    """
    # Get the indices in sorted order
    inds = list(range(0, len(values)))
    inds.sort(key = lambda x: values[x])
    
    # Rationalizes a chunk of values between the given indices
    def rationalize_chunk(start_i, end_i):
        min_value = values[inds[start_i]] - eps
        max_value = values[inds[end_i - 1]] + eps
        (n, d) = simplest_between(min_value, max_value)
        
        # Determine value to use for all indices in the chunk
        if d <= max_denom:
            # Use simplest fraction in the interval
            value = n / d
        else:
            # Use average of values in the chunk
            value = 0
            for i in range(start_i, end_i):
                value = value + values[inds[i]]
            value = value / (end_i - start_i)
            
        # Set all indices in chunk to the selected value
        for i in range(start_i, end_i):
            values[inds[i]] = value
    
    # Traverse through indices, finding chunks of similar values
    start_i = 0
    last_value = values[inds[0]]
    i = 1
    while i < len(inds):
        value = values[inds[i]]
        if value - last_value > eps:
            rationalize_chunk(start_i, i)
            start_i = i
        last_value = value
        i = i + 1
    rationalize_chunk(start_i, i)

def rationalize_mesh(bm, eps = 0.000001, max_denom = 400):
    """
    Coalesces similar values in a mesh's data and appromximates them as simple rationals.
    """

    # Rationalize coordinates
    values = list()
    for v in bm.verts:
        values.append(v.co.x)
        values.append(v.co.y)
        values.append(v.co.z)
    rationalize(values, eps, max_denom)
    i = 0
    for v in bm.verts:
        v.co.x = values[i]
        i = i + 1
        v.co.y = values[i]
        i = i + 1
        v.co.z = values[i]
        i = i + 1
    
    # Rationalize UVs
    for layer in bm.loops.layers.uv.values():
        values = list()
        for f in bm.faces:
            for l in f.loops:
                uv = l[layer].uv
                values.append(uv.x)
                values.append(uv.y)
        rationalize(values, eps, max_denom)
        i = 0
        for f in bm.faces:
            for l in f.loops:
                luv = l[layer]
                luv.uv.x = values[i]
                i = i + 1
                luv.uv.y = values[i]
                i = i + 1
    
class ObjectRationalizeMeshOperator(bpy.types.Operator):
    """
    Coalesces similar values in a mesh's data and appromximates them as simple rationals.
    """
    bl_idname = "object.rationalize_mesh"
    bl_label = "Rationalize Mesh"
    bl_options = {'REGISTER', 'UNDO'}

    eps = bpy.props.FloatProperty(name = "Epsilon", default = 0.000001, min = 0)
    max_denom = bpy.props.IntProperty(name = "Max Denominator", default = 400, min = 1)

    def execute(self, context):
        print("Rationalizing")
        for object in context.selected_objects:
            me = object.data
            bm = bmesh.new()
            bm.from_mesh(me)
            rationalize_mesh(bm, self.eps, self.max_denom)
            bm.to_mesh(me)
            bm.free()
        return {'FINISHED'}

def rationalize_mesh_button(self, context):
    self.layout.operator(
        ObjectRationalizeMeshOperator.bl_idname,
        text="Rationalize Mesh",
        icon='PLUGIN')

def register():
    bpy.utils.register_class(ObjectRationalizeMeshOperator)
    bpy.types.VIEW3D_MT_object.append(rationalize_mesh_button)


def unregister():
    bpy.utils.unregister_class(ObjectRationalizeMeshOperator)
    bpy.types.VIEW3D_MT_object.remove(rationalize_mesh_button)

if __name__ == "__main__":
    register()