﻿using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Alunite
{
    [TestClass]
    public class SortTests
    {
        [TestMethod]
        public void TestModular()
        {
            // Generate an array of all values between 1 and n - 1, scrambled using
            // modular arithmetic tricks
            uint n = 65519;
            uint g = 11;
            uint t = 1;
            uint[] items = new uint[n - 1];
            for (int i = 0; i < items.Length; i++)
            {
                items[i] = t;
                t *= g;
                t %= n;
            }

            // Sort the list
            ((Span<uint>)items).Sort();

            // Verify all items are present
            for (uint i = 0; i < (uint)items.Length; i++)
                Assert.AreEqual(i + 1, items[i]);
        }

        [TestMethod]
        public void TestSorted()
        {
            uint[] items = new uint[100000];
            for (uint i = 0; i < items.Length; i++)
                items[i] = i;
            ((Span<uint>)items).Sort();
            for (uint i = 0; i < items.Length; i++)
                Assert.AreEqual(i, items[i]);
        }

        [TestMethod]
        public void TestStable()
        {
            _Tagged<string>[] items = new _Tagged<string>[]
            {
                new _Tagged<string>("Alpha", 0),
                new _Tagged<string>("Charlie", 0),
                new _Tagged<string>("Delta", 0),
                new _Tagged<string>("Alpha", 1),
                new _Tagged<string>("Bravo", 0),
                new _Tagged<string>("Delta", 1),
                new _Tagged<string>("Charlie", 1),
                new _Tagged<string>("Alpha", 2),
                new _Tagged<string>("Bravo", 1),
                new _Tagged<string>("Bravo", 2),
                new _Tagged<string>("Delta", 2),
                new _Tagged<string>("Charlie", 2),
            };
            ((Span<_Tagged<string>>)items).Sort();
            for (uint i = 0; i < (uint)items.Length; i++)
                Assert.AreEqual(i % 3, items[i].Tag);
        }

        private struct _Tagged<T> : IComparable<_Tagged<T>>
            where T : IComparable<T>
        {
            public T Value;
            public uint Tag;
            public _Tagged(T value, uint tag)
            {
                Value = value;
                Tag = tag;
            }

            int IComparable<_Tagged<T>>.CompareTo(_Tagged<T> other)
            {
                return Value.CompareTo(other.Value);
            }
        }
    }
}