﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Alunite.Resources
{
    [TestClass]
    public class RegistryTests
    {
        [TestMethod]
        public void TestBasics()
        {
            // Initial population
            Registry<uint, _Resource<uint>> registry = Registry<uint, _Resource<uint>>.CreateEmpty();
            const uint limit = 100;
            for (uint i = 0; i < limit; i++)
            {
                if (i % 2 == 0)
                {
                    // Test adding one by one
                    for (uint j = 0; j < i; j++)
                        registry.Add(i, new _Resource<uint>(j));
                }
                else
                {
                    // Test locking and adding
                    using (var bag = registry.Browse(i))
                    {
                        for (uint j = 0; j < i; j++)
                            bag.Add(new _Resource<uint>(j));
                    }
                }
            }

            // Check contents
            for (uint i = 0; i < limit; i++)
            {
                uint sum = 0;
                if (i % 2 == 0)
                {
                    using (var it = registry.Iterate(i))
                    {
                        while (it.TryNext(out _Resource<uint> resource))
                            sum += resource.Value;
                    }
                }
                else
                {
                    using (var it = registry.View(i).Iterate())
                    {
                        while (it.TryNext(out _Resource<uint> resource))
                            sum += resource.Value;
                    }
                }
                Assert.AreEqual(i * (i - 1) / 2, sum);
            }

            // Test removal (all even keys and values)
            for (uint i = 0; i < limit; i++)
            {
                using (var bag = registry.Browse(i))
                {
                    if (i % 2 == 0)
                    {
                        using (var it = bag.Iterate())
                        {
                            while (it.TryNext(out _Resource<uint> resource))
                            {
                                resource.Dispose();
                                it.Clean();
                            }
                        }
                    }
                    else
                    {
                        using (var it = bag.Iterate())
                        {
                            while (it.TryNext(out _Resource<uint> resource))
                            {
                                if (resource.Value % 2 == 0)
                                {
                                    resource.Dispose();
                                    it.Clean();
                                }
                            }
                        }
                    }
                }
            }

            // Test further addition
            for (uint i = limit; i < limit * 10; i++)
                registry.Add(i, new _Resource<uint>(i));

            // Check contents
            for (uint i = 0; i < limit * 10; i++)
            {
                uint sum = 0;
                using (var it = registry.Iterate(i))
                {
                    while (it.TryNext(out _Resource<uint> resource))
                        sum += resource.Value;
                }
                if (i >= limit)
                    Assert.AreEqual(i, sum);
                else if (i % 2 == 0)
                    Assert.AreEqual(0u, sum);
                else
                    Assert.AreEqual((i * (i - 2) + 1) / 4, sum);
            }
        }

        private sealed class _Resource<T> : IDisposable, ITransient
        {
            public T Value;
            private bool _isAlive;
            public _Resource(T value)
            {
                Value = value;
                _isAlive = true;
            }

            public bool IsAlive => _isAlive;

            public void Dispose()
            {
                _isAlive = false;
            }
        }
    }
}