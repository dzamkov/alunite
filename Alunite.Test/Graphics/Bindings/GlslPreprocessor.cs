﻿using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Alunite.Data;
using Alunite.Resources;
using Alunite.Graphics.Bindings;

namespace Alunite.Test.Graphics.Bindings
{
    [TestClass]
    public class GlslPreprocessorTests
    {
        [TestMethod]
        public void TestSimple()
        {
            string input =
                "#version 400\n" +
                "int main() {\n" +
                "    color = vec4(COLOR, 1);\n" +
                "}\n";
            GlslPreprocessor preprocessor = GlslPreprocessor.CreateInitial();
            preprocessor.Define("YELLOW", "vec3(1, 1, 0)");
            preprocessor.Define("COLOR", "YELLOW");
            StringBuilder outputBuilder = StringBuilder.CreateEmpty();
            preprocessor.ProcessDocument(outputBuilder, RepositoryPathViewer.Void, StringReader.Create(input));
            string output = outputBuilder.Finish();
            string expected =
                "#version 400\n" +
                "int main() {\n" +
                "    color = vec4(vec3(1, 1, 0), 1);\n" +
                "}\n";
            Assert.AreEqual(expected, output);
        }

        [TestMethod]
        public void TestObjectDefine()
        {
            string input =
                "#version 400\n" +
                "#define M_PI 3.1415\n" +
                "#define M_TAU (2 * M_PI)\n" +
                "#define M_PI 3.14\n" +
                "M_TAU";
            GlslPreprocessor preprocessor = GlslPreprocessor.CreateInitial();
            StringBuilder outputBuilder = StringBuilder.CreateEmpty();
            preprocessor.ProcessDocument(outputBuilder, RepositoryPathViewer.Void, StringReader.Create(input));
            string output = outputBuilder.Finish();
            string expected =
                "#version 400\n" +
                "(2 * 3.14)";
            Assert.AreEqual(expected, output);
        }
    }
}