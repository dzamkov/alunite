﻿using System;
using System.Diagnostics;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Alunite.Data;
using Alunite.Data.Probability;
using Alunite.Data.Geometry;

namespace Alunite.Universe.Generation
{
    [TestClass]
    public class ProofOfConcept
    {
        [TestMethod]
        public void Test1()
        {
            // This is a very simple tileset with no high-level constraints
            FiniteKernelBuilder<Tile> builder = new FiniteKernelBuilder<Tile>();
            builder.Add(Tile.Get('·'), 0.6);
            builder.Add(Tile.Get('╸'), 0.05);
            builder.Add(Tile.Get('╹'), 0.05);
            builder.Add(Tile.Get('╺'), 0.05);
            builder.Add(Tile.Get('╻'), 0.05);
            builder.Add(Tile.Get('┃'), 0.1);
            builder.Add(Tile.Get('━'), 0.1);
            var image = Generate((FiniteDistribution<Tile>)builder.Finish(), 6, Seed.Initial);
            string str = Print(image);
            // TODO: Checks
        }

        [TestMethod]
        public void Test2()
        {
            // This tileset requires some high-level constraints to maintain the parity of lines
            // flowing into and out of a space, but is otherwise very simple. There is no way to create
            // a "collision" that would prevent a space of the correct parity from being filled
            FiniteKernelBuilder<Tile> builder = new FiniteKernelBuilder<Tile>();
            builder.Add(Tile.Get('·'), 0.55);
            builder.Add(Tile.Get('━'), 0.1);
            builder.Add(Tile.Get('┃'), 0.1);
            builder.Add(Tile.Get('┏'), 0.05);
            builder.Add(Tile.Get('┓'), 0.05);
            builder.Add(Tile.Get('┗'), 0.05);
            builder.Add(Tile.Get('┛'), 0.05);
            builder.Add(Tile.Get('╋'), 0.05);
            var image = Generate((FiniteDistribution<Tile>)builder.Finish(), 6, Seed.Initial);
            string str = Print(image);
            // TODO: Checks
        }

        [TestMethod]
        public void Test3()
        {
            // The high-level structure of this tileset is identical to the one above, but has more
            // constraints at lower, local levels
            FiniteKernelBuilder<Tile> builder = new FiniteKernelBuilder<Tile>();
            builder.Add(Tile.Get('·'), 0.6);
            builder.Add(Tile.Get('━'), 0.1);
            builder.Add(Tile.Get('┃'), 0.1);
            builder.Add(Tile.Get('┏'), 0.05);
            builder.Add(Tile.Get('┓'), 0.05);
            builder.Add(Tile.Get('┗'), 0.05);
            builder.Add(Tile.Get('┛'), 0.05);
            var image = Generate((FiniteDistribution<Tile>)builder.Finish(), 6, Seed.Initial);
            string str = Print(image);
            // TODO: Checks
        }

        /// <summary>
        /// Describes a two-dimensional tile in the proof-of-concept test for generation. A tile consists
        /// of up to four lines emanating from the center.
        /// </summary>
        public sealed class Tile : IEquatable<Tile> // TODO: Switch back to struct once this stops breaking Visual Studio
        {
            private byte _code;
            
            /// <summary>
            /// Gets the <see cref="Tile"/> corresponding to the given drawing character.
            /// </summary>
            public static Tile Get(char ch)
            {
                return _byChar[ch];
            }

            /// <summary>
            /// Determines whether the given tiles are compatible along the X axis.
            /// </summary>
            public static bool AreCompatible_X(Tile neg, Tile pos)
            {
                return _connTable[neg._code].PosX == _connTable[pos._code].NegX;
            }

            /// <summary>
            /// Determines whether the given tiles are compatible along the Y axis.
            /// </summary>
            public static bool AreCompatible_Y(Tile neg, Tile pos)
            {
                return _connTable[neg._code].PosY == _connTable[pos._code].NegY;
            }

            public override string ToString()
            {
                return _charTable[_code].ToString();
            }
            
            /// <summary>
            /// Identifies a possible connection for one side of a tile.
            /// </summary>
            private enum _Connection
            {
                None,
                Light,
                Heavy,
                Double
            }

            /// <summary>
            /// A mapping from characters to the corresponding tile.
            /// </summary>
            private static readonly Map<char, Tile> _byChar;

            /// <summary>
            /// The character for the tile with the corresponding code.
            /// </summary>
            private static readonly char[] _charTable;

            /// <summary>
            /// Describes the connections from the tile with the corresponding code.
            /// </summary>
            private static readonly AxialDuet2<_Connection>[] _connTable;

            static Tile()
            {
                var byCharBuilder = new MapBuilder<char, Tile>();
                var charTableBuilder = new ListBuilder<char>();
                var connTableBuilder = new ListBuilder<AxialDuet2<_Connection>>();
                define('·', _Connection.None, _Connection.None, _Connection.None, _Connection.None);
                define('─', _Connection.Light, _Connection.Light, _Connection.None, _Connection.None);
                define('━', _Connection.Heavy, _Connection.Heavy, _Connection.None, _Connection.None);
                define('│', _Connection.None, _Connection.None, _Connection.Light, _Connection.Light);
                define('┃', _Connection.None, _Connection.None, _Connection.Heavy, _Connection.Heavy);
                define('┌', _Connection.None, _Connection.Light, _Connection.Light, _Connection.None);
                define('┍', _Connection.None, _Connection.Heavy, _Connection.Light, _Connection.None);
                define('┎', _Connection.None, _Connection.Light, _Connection.Heavy, _Connection.None);
                define('┏', _Connection.None, _Connection.Heavy, _Connection.Heavy, _Connection.None);
                define('┐', _Connection.Light, _Connection.None, _Connection.Light, _Connection.None);
                define('┑', _Connection.Heavy, _Connection.None, _Connection.Light, _Connection.None);
                define('┒', _Connection.Light, _Connection.None, _Connection.Heavy, _Connection.None);
                define('┓', _Connection.Heavy, _Connection.None, _Connection.Heavy, _Connection.None);
                define('└', _Connection.None, _Connection.Light, _Connection.None, _Connection.Light);
                define('┕', _Connection.None, _Connection.Heavy, _Connection.None, _Connection.Light);
                define('┖', _Connection.None, _Connection.Light, _Connection.None, _Connection.Heavy);
                define('┗', _Connection.None, _Connection.Heavy, _Connection.None, _Connection.Heavy);
                define('┘', _Connection.Light, _Connection.None, _Connection.None, _Connection.Light);
                define('┙', _Connection.Heavy, _Connection.None, _Connection.None, _Connection.Light);
                define('┚', _Connection.Light, _Connection.None, _Connection.None, _Connection.Heavy);
                define('┛', _Connection.Heavy, _Connection.None, _Connection.None, _Connection.Heavy);
                define('├', _Connection.None, _Connection.Light, _Connection.Light, _Connection.Light);
                define('┝', _Connection.None, _Connection.Heavy, _Connection.Light, _Connection.Light);
                define('┞', _Connection.None, _Connection.Light, _Connection.Light, _Connection.Heavy);
                define('┟', _Connection.None, _Connection.Light, _Connection.Heavy, _Connection.Light);
                define('┠', _Connection.None, _Connection.Light, _Connection.Heavy, _Connection.Heavy);
                define('┡', _Connection.None, _Connection.Heavy, _Connection.Light, _Connection.Heavy);
                define('┢', _Connection.None, _Connection.Heavy, _Connection.Heavy, _Connection.Light);
                define('┣', _Connection.None, _Connection.Heavy, _Connection.Heavy, _Connection.Heavy);
                define('┤', _Connection.Light, _Connection.None, _Connection.Light, _Connection.Light);
                define('┥', _Connection.Heavy, _Connection.None, _Connection.Light, _Connection.Light);
                define('┦', _Connection.Light, _Connection.None, _Connection.Light, _Connection.Heavy);
                define('┧', _Connection.Heavy, _Connection.None, _Connection.Heavy, _Connection.Light);
                define('┨', _Connection.Light, _Connection.None, _Connection.Heavy, _Connection.Heavy);
                define('┩', _Connection.Heavy, _Connection.None, _Connection.Light, _Connection.Heavy);
                define('┪', _Connection.Heavy, _Connection.None, _Connection.Heavy, _Connection.Light);
                define('┫', _Connection.Heavy, _Connection.None, _Connection.Heavy, _Connection.Heavy);
                define('┬', _Connection.Light, _Connection.Light, _Connection.Light, _Connection.None);
                define('┭', _Connection.Heavy, _Connection.Light, _Connection.Light, _Connection.None);
                define('┮', _Connection.Light, _Connection.Heavy, _Connection.Light, _Connection.None);
                define('┯', _Connection.Heavy, _Connection.Heavy, _Connection.Light, _Connection.None);
                define('┰', _Connection.Light, _Connection.Light, _Connection.Heavy, _Connection.None);
                define('┱', _Connection.Heavy, _Connection.Light, _Connection.Heavy, _Connection.None);
                define('┲', _Connection.Light, _Connection.Heavy, _Connection.Heavy, _Connection.None);
                define('┳', _Connection.Heavy, _Connection.Heavy, _Connection.Heavy, _Connection.None);
                define('┴', _Connection.Light, _Connection.Light, _Connection.None, _Connection.Light);
                define('┵', _Connection.Heavy, _Connection.Light, _Connection.None, _Connection.Light);
                define('┶', _Connection.Light, _Connection.Heavy, _Connection.None, _Connection.Light);
                define('┷', _Connection.Heavy, _Connection.Heavy, _Connection.None, _Connection.Light);
                define('┸', _Connection.Light, _Connection.Light, _Connection.None, _Connection.Heavy);
                define('┹', _Connection.Heavy, _Connection.Light, _Connection.None, _Connection.Heavy);
                define('┺', _Connection.Light, _Connection.Heavy, _Connection.None, _Connection.Heavy);
                define('┻', _Connection.Heavy, _Connection.Heavy, _Connection.None, _Connection.Heavy);
                define('┼', _Connection.Light, _Connection.Light, _Connection.Light, _Connection.Light);
                define('┽', _Connection.Heavy, _Connection.Light, _Connection.Light, _Connection.Light);
                define('┾', _Connection.Light, _Connection.Heavy, _Connection.Light, _Connection.Light);
                define('┿', _Connection.Heavy, _Connection.Heavy, _Connection.Light, _Connection.Light);
                define('╀', _Connection.Light, _Connection.Light, _Connection.Light, _Connection.Heavy);
                define('╁', _Connection.Light, _Connection.Light, _Connection.Heavy, _Connection.Light);
                define('╂', _Connection.Light, _Connection.Light, _Connection.Heavy, _Connection.Heavy);
                define('╃', _Connection.Heavy, _Connection.Light, _Connection.Light, _Connection.Heavy);
                define('╄', _Connection.Light, _Connection.Heavy, _Connection.Light, _Connection.Heavy);
                define('╅', _Connection.Heavy, _Connection.Light, _Connection.Heavy, _Connection.Light);
                define('╆', _Connection.Light, _Connection.Heavy, _Connection.Heavy, _Connection.Light);
                define('╇', _Connection.Heavy, _Connection.Heavy, _Connection.Light, _Connection.Heavy);
                define('╈', _Connection.Heavy, _Connection.Heavy, _Connection.Heavy, _Connection.Light);
                define('╉', _Connection.Heavy, _Connection.Light, _Connection.Heavy, _Connection.Heavy);
                define('╊', _Connection.Light, _Connection.Heavy, _Connection.Heavy, _Connection.Heavy);
                define('╋', _Connection.Heavy, _Connection.Heavy, _Connection.Heavy, _Connection.Heavy);
                define('═', _Connection.Double, _Connection.Double, _Connection.None, _Connection.None);
                define('║', _Connection.None, _Connection.None, _Connection.Double, _Connection.Double);
                define('╒', _Connection.None, _Connection.Double, _Connection.Light, _Connection.None);
                define('╓', _Connection.None, _Connection.Light, _Connection.Double, _Connection.None);
                define('╔', _Connection.None, _Connection.Double, _Connection.Double, _Connection.None);
                define('╕', _Connection.Double, _Connection.None, _Connection.Light, _Connection.None);
                define('╖', _Connection.Light, _Connection.None, _Connection.Double, _Connection.None);
                define('╗', _Connection.Double, _Connection.None, _Connection.Double, _Connection.None);
                define('╘', _Connection.None, _Connection.Double, _Connection.None, _Connection.Light);
                define('╙', _Connection.None, _Connection.Light, _Connection.None, _Connection.Double);
                define('╚', _Connection.None, _Connection.Double, _Connection.None, _Connection.Double);
                define('╛', _Connection.Double, _Connection.None, _Connection.None, _Connection.Light);
                define('╜', _Connection.Light, _Connection.None, _Connection.None, _Connection.Double);
                define('╝', _Connection.Double, _Connection.None, _Connection.None, _Connection.Double);
                define('╞', _Connection.None, _Connection.Double, _Connection.Light, _Connection.Light);
                define('╟', _Connection.None, _Connection.Light, _Connection.Double, _Connection.Double);
                define('╠', _Connection.None, _Connection.Double, _Connection.Double, _Connection.Double);
                define('╡', _Connection.Double, _Connection.None, _Connection.Light, _Connection.Light);
                define('╢', _Connection.Light, _Connection.None, _Connection.Double, _Connection.Double);
                define('╣', _Connection.Double, _Connection.None, _Connection.Double, _Connection.Double);
                define('╤', _Connection.Double, _Connection.Double, _Connection.Light, _Connection.None);
                define('╥', _Connection.Light, _Connection.Light, _Connection.Double, _Connection.None);
                define('╦', _Connection.Double, _Connection.Double, _Connection.Double, _Connection.None);
                define('╧', _Connection.Double, _Connection.Double, _Connection.None, _Connection.Light);
                define('╨', _Connection.Light, _Connection.Light, _Connection.None, _Connection.Double);
                define('╩', _Connection.Double, _Connection.Double, _Connection.None, _Connection.Double);
                define('╪', _Connection.Double, _Connection.Double, _Connection.Light, _Connection.Light);
                define('╫', _Connection.Light, _Connection.Light, _Connection.Double, _Connection.Double);
                define('╬', _Connection.Double, _Connection.Double, _Connection.Double, _Connection.Double);
                define('╭', _Connection.None, _Connection.Light, _Connection.Light, _Connection.None);
                define('╮', _Connection.Light, _Connection.None, _Connection.Light, _Connection.None);
                define('╯', _Connection.Light, _Connection.None, _Connection.None, _Connection.Light);
                define('╰', _Connection.None, _Connection.Light, _Connection.None, _Connection.Light);
                define('╴', _Connection.Light, _Connection.None, _Connection.None, _Connection.None);
                define('╵', _Connection.None, _Connection.None, _Connection.None, _Connection.Light);
                define('╶', _Connection.None, _Connection.Light, _Connection.None, _Connection.None);
                define('╷', _Connection.None, _Connection.None, _Connection.Light, _Connection.None);
                define('╸', _Connection.Heavy, _Connection.None, _Connection.None, _Connection.None);
                define('╹', _Connection.None, _Connection.None, _Connection.None, _Connection.Heavy);
                define('╺', _Connection.None, _Connection.Heavy, _Connection.None, _Connection.None);
                define('╻', _Connection.None, _Connection.None, _Connection.Heavy, _Connection.None);
                define('╼', _Connection.Light, _Connection.Heavy, _Connection.None, _Connection.None);
                define('╽', _Connection.None, _Connection.None, _Connection.Heavy, _Connection.Light);
                define('╾', _Connection.Heavy, _Connection.Light, _Connection.None, _Connection.None);
                define('╿', _Connection.None, _Connection.None, _Connection.Light, _Connection.Heavy);
                _byChar = byCharBuilder.Finish();
                _charTable = charTableBuilder.FinishToArray();
                _connTable = connTableBuilder.FinishToArray();

                // Defines a new tile based on the given drawing character.
                void define(char ch, _Connection negX, _Connection posX, _Connection negY, _Connection posY)
                {
                    byte code = (byte)charTableBuilder.Append(ch);
                    connTableBuilder.Append(new AxialDuet2<_Connection>(negX, posX, negY, posY));
                    byCharBuilder.Add(ch, new Tile { _code = code });
                }
            }

            public static bool operator ==(Tile a, Tile b)
            {
                return a._code == b._code;
            }

            public static bool operator !=(Tile a, Tile b)
            {
                return !(a == b);
            }

            public override bool Equals(object obj)
            {
                if (!(obj is Tile))
                    return false;
                return this == (Tile)obj;
            }

            bool IEquatable<Tile>.Equals(Tile other)
            {
                return this == other;
            }

            public override int GetHashCode()
            {
                return _code.GetHashCode();
            }
        }

        /// <summary>
        /// Generates an image from the given tile distribution, respecting tile compatibility.
        /// </summary>
        public static Image2<Tile> Generate(FiniteDistribution<Tile> tiles, uint scale, Seed seed)
        {
            // Assign an index for each tile
            IndexKernelBuilder tileProbsBuilder = new IndexKernelBuilder();
            ListBuilder<Tile> tilesBuilder = new ListBuilder<Tile>();
            foreach (var tile in tiles)
            {
                uint index = tilesBuilder.Append(tile.Value);
                tileProbsBuilder.Add(index, tile.Weight);
            }
            IndexKernel tileProbs = tileProbsBuilder.Finish();
            uint numTiles = tilesBuilder.Length;

            // Construct prior kernel
            RegularIndexListSet compat_x = RegularIndexListSet.Build(
                List.Of(numTiles, numTiles),
                list => Tile.AreCompatible_X(tilesBuilder[list[0]], tilesBuilder[list[1]]));
            RegularIndexListSet compat_y = RegularIndexListSet.Build(
                List.Of(numTiles, numTiles),
                list => Tile.AreCompatible_Y(tilesBuilder[list[0]], tilesBuilder[list[1]]));
            RegularIndexListKernel prior = RegularIndexListKernel
                .IndependentIndicatorBelow(numTiles, numTiles, numTiles, numTiles)
                .ConflateItem(3, tileProbs)
                .IntersectSubseq(IndexSet.Of(2, 3), compat_x)
                .IntersectSubseq(IndexSet.Of(1, 3), compat_y);

            // Build universe
            Universe<Tile> universe = Universe<Tile>.Build(tilesBuilder.Finish(), prior.Distribution, scale);

            // Prime data
            SeedSampler sampler = new SeedSampler(seed);
            List<uint> data_x = List.Empty<uint>();
            List<uint> data_y_0 = List.Empty<uint>();
            List<uint> data_y_1 = List.Empty<uint>();
            List<uint> data_y_2 = List.Empty<uint>();
            universe.Prime(scale, 0, sampler, PrimingMode.Boundary_X, ref data_x, ref data_y_0);
            universe.Prime(scale, 0, sampler, PrimingMode.Boundary_Y, ref data_x, ref data_y_1);
            universe.Prime(scale, 0, sampler, PrimingMode.Boundary_Y, ref data_x, ref data_y_2);
            data_x = List.Empty<uint>();
            universe.Prime(scale, 0, sampler, PrimingMode.Boundary_X, ref data_x, ref data_y_0);
            universe.Prime(scale, 0, sampler, PrimingMode.Interior_NegX_NegY, ref data_x, ref data_y_1);
            universe.Prime(scale, 0, sampler, PrimingMode.Interior_PosX_NegY, ref data_x, ref data_y_2);
            data_x = List.Empty<uint>();
            universe.Prime(scale, 0, sampler, PrimingMode.Boundary_X, ref data_x, ref data_y_0);
            universe.Prime(scale, 0, sampler, PrimingMode.Interior_NegX_PosY, ref data_x, ref data_y_1);

            // Generate content
            uint size = 1u << (int)scale;
            Image2Builder<Tile> imageBuilder = new Image2Builder<Tile>(size, size);
            universe.Generate(scale, 0, sampler, imageBuilder, Index2.Zero, ref data_x, ref data_y_2);
            return imageBuilder.Finish();
        }

        /// <summary>
        /// Converts the given tile image into a multiline string.
        /// </summary>
        public static string Print(Image2<Tile> image)
        {
            StringBuilder str = StringBuilder.CreateEmpty();
            Index2 size = image.Size;
            uint y = size.Y;
            while (y > 0)
            {
                y--;
                for (uint x = 0; x < size.X; x++)
                    str.Write(image[new Index2(x, y)].ToString());
                str.WriteItem('\n');
            }
            return str.Finish();
        }

        /// <summary>
        /// Encapsulates the generation parameters for filling a two-dimensional space with tiles of type <typeparamref name="T"/>.
        /// </summary>
        public struct Universe<T>
        {
            public Universe(List<T> @base, List<RegularIndexListMap<RegularIndexListDistribution>> gens)
            {
                Base = @base;
                Generators = gens;
            }

            /// <summary>
            /// Provides the mapping from 0-level metatiles to base tiles.
            /// </summary>
            public List<T> Base { get; }

            /// <summary>
            /// For each metatile level, provides the <see cref="RegularIndexListDistribution"/> for generation of a 2x2 grid of
            /// metatiles given the 2x2 grid of the next-higher-level metatiles surronding it.
            /// </summary>
            public List<RegularIndexListMap<RegularIndexListDistribution>> Generators { get; }

            /// <summary>
            /// Constructs a universe from a given set of tiles and a distribution specifying the prior probability of a given
            /// 2x2 grid of tiles occuring.
            /// </summary>
            /// <param name="depth">The maximum metatile level of the resulting universe.</param>
            public static Universe<T> Build(List<T> @base, RegularIndexListDistribution prior, uint depth)
            {
                uint numTiles = @base.Length;
                var gens = new RegularIndexListMap<RegularIndexListDistribution>[depth];
                for (uint scale = 0; scale < depth; scale++)
                {
                    // TODO: Stop cheating, keep track of probabilities
                    RegularIndexListSet priorSupport = prior.Support;

                    // Build 3x3 region kernel
                    RegularIndexListSet region = RegularIndexListSet
                        .IndependentBelow(List.Replicate(9, numTiles))
                        .IntersectSubseq(IndexSet.Of(0, 1, 2, 4), priorSupport)
                        .IntersectSubseq(IndexSet.Of(1, 3, 4, 6), priorSupport)
                        .IntersectSubseq(IndexSet.Of(2, 4, 5, 7), priorSupport)
                        .IntersectSubseq(IndexSet.Of(4, 6, 7, 8), priorSupport);

                    // Factorize into independent components
                    RegularIndexListSet adregion = region.Select(0, 1, 2, 4, 3, 6, 5, 7, 8);
                    var factors = adregion.Factorize(List.Of<uint>(4, 2, 2, 1));
                    var factorsImage = (Image2<RegularIndexListSet>)factors.SortArbitrary();

                    // Construct lens kernel, which relates metatiles of the next level to metatiles of this level.
                    uint nNumTiles = factorsImage.Size.Y;
                    RegularIndexListSet lensSupport = RegularIndexListSet
                        .IndependentBelow(nNumTiles, nNumTiles, nNumTiles, nNumTiles, numTiles, numTiles, numTiles, numTiles)
                        .IntersectSubseq(IndexSet.Of(0, 4), RegularIndexListSet.Switch(factorsImage.At_X(3)))
                        .IntersectSubseq(IndexSet.Of(1, 4, 5), RegularIndexListSet.Switch(factorsImage.At_X(2)))
                        .IntersectSubseq(IndexSet.Of(2, 4, 6), RegularIndexListSet.Switch(factorsImage.At_X(1)))
                        .IntersectSubseq(IndexSet.Of(3, 4, 5, 6, 7), RegularIndexListSet.Switch(factorsImage.At_X(0)));
                    RegularIndexListDistribution lens = RegularIndexListDistribution.Uniform(lensSupport);

                    // Split into prior kernel for next level, and generator
                    lens.Split(4, out prior, out gens[scale]);
                    numTiles = nNumTiles;
                }

                // To top things off, pick the metatile which has the most likely self-tiling and redirect all higher-level
                // metatiles to generate that tiling
                RegularIndexListDistribution bestGen = default;
                LogScalar bestProb = LogScalar.Zero;
                Span<uint> items = stackalloc uint[4];
                uint lastScale = depth - 1;
                for (uint i = 0; i < numTiles; i++)
                {
                    items[0] = i;
                    items[1] = i;
                    items[2] = i;
                    items[3] = i;
                    LogScalar curProb = prior.Evaluate(List.Of(items));
                    if (bestProb < curProb)
                    {
                        bool hasGen = gens[lastScale].TryGet(items, out bestGen);
                        Debug.Assert(hasGen);
                        bestProb = curProb;
                    }
                }
                gens[lastScale] = RegularIndexListMap.Singleton(List.Of<uint>(0, 0, 0, 0), bestGen);

                // Finalize universe
                return new Universe<T>(@base, List.Of(gens));
            }

            /// <summary>
            /// Generates a square of tiles using this universe.
            /// </summary>
            /// <param name="id">The index of the metatile to generate content with.</param>
            /// <param name="data_x">The data passed along the X direction.</param>
            /// <param name="data_y">The data passed along the Y direction.</param>
            public void Generate(
                uint scale, uint id,
                ISampler sampler,
                Image2Builder<T> image, Index2 offset,
                ref List<uint> data_x,
                ref List<uint> data_y)
            {
                var dataBuilder_posX = new ListBuilder<uint>();
                var dataBuilder_posY = new ListBuilder<uint>();
                Generate(scale, id, sampler, image, offset,
                    new ListReader<uint>(data_x),
                    new ListReader<uint>(data_y),
                    dataBuilder_posX,
                    dataBuilder_posY);
                data_x = dataBuilder_posX.Finish();
                data_y = dataBuilder_posY.Finish();
            }

            /// <summary>
            /// Generates a square of tiles using this universe.
            /// </summary>
            /// <param name="id">The index of the metatile to generate content with.</param>
            /// <param name="data_negX">The data passed from the neighboring metatile in the negative X direction.</param>
            /// <param name="data_negY">The data passed from the neighboring metatile in the negative Y direction.</param>
            /// <param name="data_posX">The data passed to the neighboring metatile in the positive X direction.</param>
            /// <param name="data_posY">The data passed to the neighboring metatile in the positive Y direction.</param>
            public void Generate(
                uint scale, uint id,
                ISampler sampler,
                Image2Builder<T> image, Index2 offset,
                ListReader<uint> data_negX,
                ListReader<uint> data_negY,
                ListBuilder<uint> data_posX,
                ListBuilder<uint> data_posY)
            {
                if (scale == 0)
                {
                    image[offset] = Base[id];
                }
                else
                {
                    uint id_negX_negY = data_negY.ReadItem();
                    uint id_posX_negY = data_negY.ReadItem();
                    uint id_negX_posY = data_negX.ReadItem();
                    uint id_posX_posY = id;
                    data_posX.Append(id_posX_posY);
                    data_posY.Append(id_negX_posY);
                    data_posY.Append(id_posX_posY);

                    uint nScale = scale - 1;
                    uint hsize = 1u << (int)nScale;
                    Span<uint> group = stackalloc uint[4];
                    group[0] = id_negX_negY;
                    group[1] = id_posX_negY;
                    group[2] = id_negX_posY;
                    group[3] = id_posX_posY;
                    bool hasGen = Generators[nScale].TryGet(group, out RegularIndexListDistribution dist);
                    Debug.Assert(hasGen);
                    Span<uint> nGroup = stackalloc uint[4];
                    dist.Sample(sampler, nGroup);
                    ListBuilder<uint> dataBuilder_midX = new ListBuilder<uint>();
                    ListBuilder<uint> dataBuilder_midY = new ListBuilder<uint>();
                    Generate(nScale, nGroup[0], sampler, image, offset,
                        data_negX, data_negY, dataBuilder_midX, dataBuilder_midY);
                    List<uint> data_midX = dataBuilder_midX.Finish();
                    ListReader<uint> dataReader_midX = new ListReader<uint>(data_midX);
                    Generate(nScale, nGroup[1], sampler, image, offset + new Index2(hsize, 0),
                        dataReader_midX, data_negY, data_posX, dataBuilder_midY);
                    List<uint> data_midY = dataBuilder_midY.Finish();
                    ListReader<uint> dataReader_midY = new ListReader<uint>(data_midY);
                    dataBuilder_midX = new ListBuilder<uint>();
                    Generate(nScale, nGroup[2], sampler, image, offset + new Index2(0, hsize),
                        data_negX, dataReader_midY, dataBuilder_midX, data_posY);
                    data_midX = dataBuilder_midX.Finish();
                    dataReader_midX = new ListReader<uint>(data_midX);
                    Generate(nScale, nGroup[3], sampler, image, offset + new Index2(hsize, hsize),
                        dataReader_midX, dataReader_midY, data_posX, data_posY);
                }
            }

            /// <summary>
            /// Generates the data passed from a metatile to its neighbors without generating content or requiring all
            /// data to be passed to the metatile.
            /// </summary>
            /// <param name="id">The index of the metatile to prime.</param>
            public void Prime(
                uint scale, uint id,
                ISampler sampler, PrimingMode mode,
                ref List<uint> data_x,
                ref List<uint> data_y)
            {
                var dataBuilder_posX = new ListBuilder<uint>();
                var dataBuilder_posY = new ListBuilder<uint>();
                Prime(scale, id, sampler, mode,
                    new ListReader<uint>(data_x),
                    new ListReader<uint>(data_y),
                    dataBuilder_posX,
                    dataBuilder_posY);
                data_x = dataBuilder_posX.Finish();
                data_y = dataBuilder_posY.Finish();
            }

            /// <summary>
            /// Generates the data passed from a metatile to its neighbors without generating content or requiring all
            /// data to be passed to the metatile.
            /// </summary>
            /// <param name="id">The index of the metatile to prime.</param>
            public void Prime(
                uint scale, uint id,
                ISampler sampler, PrimingMode mode,
                ListReader<uint> data_negX,
                ListReader<uint> data_negY,
                ListBuilder<uint> data_posX,
                ListBuilder<uint> data_posY)
            {
                if (scale > 0)
                {
                    if (mode.IsInterior(out Quadrant quad))
                    {
                        uint id_negX_negY = data_negY.ReadItem();
                        uint id_posX_negY = data_negY.ReadItem();
                        uint id_negX_posY = data_negX.ReadItem();
                        uint id_posX_posY = id;
                        data_posX.Append(id_posX_posY);
                        data_posY.Append(id_negX_posY);
                        data_posY.Append(id_posX_posY);

                        uint nScale = scale - 1;
                        Span<uint> group = stackalloc uint[4];
                        group[0] = id_negX_negY;
                        group[1] = id_posX_negY;
                        group[2] = id_negX_posY;
                        group[3] = id_posX_posY;
                        bool hasGen = Generators[nScale].TryGet(group, out RegularIndexListDistribution dist);
                        Debug.Assert(hasGen);
                        Span<uint> nGroup = stackalloc uint[4];
                        dist.Sample(sampler, nGroup);
                        ListBuilder<uint> dataBuilder_midX = new ListBuilder<uint>();
                        ListBuilder<uint> dataBuilder_midY = new ListBuilder<uint>();
                        Prime(nScale, nGroup[0], sampler,
                            PrimingMode.InteriorChild(quad, Quadrant.NegX_NegY),
                            data_negX, data_negY, dataBuilder_midX, dataBuilder_midY);
                        List<uint> data_midX = dataBuilder_midX.Finish();
                        ListReader<uint> dataReader_midX = new ListReader<uint>(data_midX);
                        Prime(nScale, nGroup[1], sampler,
                            PrimingMode.InteriorChild(quad, Quadrant.PosX_NegY),
                            dataReader_midX, data_negY, data_posX, dataBuilder_midY);
                        List<uint> data_midY = dataBuilder_midY.Finish();
                        ListReader<uint> dataReader_midY = new ListReader<uint>(data_midY);
                        dataBuilder_midX = new ListBuilder<uint>();
                        Prime(nScale, nGroup[2], sampler,
                            PrimingMode.InteriorChild(quad, Quadrant.NegX_PosY),
                            data_negX, dataReader_midY, dataBuilder_midX, data_posY);
                        data_midX = dataBuilder_midX.Finish();
                        dataReader_midX = new ListReader<uint>(data_midX);
                        Prime(nScale, nGroup[3], sampler,
                            PrimingMode.InteriorChild(quad, Quadrant.PosX_PosY),
                            dataReader_midX, dataReader_midY, data_posX, data_posY);
                    }
                    else if (mode.IsBoundary_X)
                    {
                        data_posX.Append(id);
                    }
                    else // if (mode.IsBoundary_Y)
                    {
                        uint id_negX_posY = data_negX.ReadItem();
                        data_posX.Append(id);
                        data_posY.Append(id_negX_posY);
                        data_posY.Append(id);
                    }
                }
            }
        }

        /// <summary>
        /// When "priming" a <see cref="Universe"/> for content generation, specifies what data is given as input, and consequently,
        /// what data will be generated as output.
        /// </summary>
        public struct PrimingMode
        {
            private byte _boundary;
            private Quadrant _interior;
            private PrimingMode(byte boundary, Quadrant interior)
            {
                _boundary = boundary;
                _interior = interior;
            }

            /// <summary>
            /// Receives no data and transmits root-level data along the X direction.
            /// </summary>
            public static PrimingMode Boundary_X => new PrimingMode(0b00, default);

            /// <summary>
            /// Receives root-level data from the X direction and transmits root-level data along both the X and Y directions.
            /// </summary>
            public static PrimingMode Boundary_Y => new PrimingMode(0b01, default);

            /// <summary>
            /// Receives root-level data from both the X and Y directions and transmits partial data along
            /// both the X and Y directions.
            /// </summary>
            public static PrimingMode Interior_NegX_NegY => new PrimingMode(0b11, Quadrant.NegX_NegY);

            /// <summary>
            /// Receives partial data from the X direction and root-level data from the Y direction and transmits partial
            /// data along the X direction and full data along the Y direction.
            /// </summary>
            public static PrimingMode Interior_PosX_NegY => new PrimingMode(0b11, Quadrant.PosX_NegY);

            /// <summary>
            /// Receives root-level data from the X direction and partial data from the Y direction and transmits full
            /// data along the X direction and partial data along the Y direction.
            /// </summary>
            public static PrimingMode Interior_NegX_PosY => new PrimingMode(0b11, Quadrant.NegX_PosY);

            /// <summary>
            /// Receives full data from both the X and Y direction and transmits full data along both the X and Y
            /// direction.
            /// </summary>
            public static PrimingMode Interior_PosX_PosY => new PrimingMode(0b11, Quadrant.PosX_PosY);

            /// <summary>
            /// Receives some combination of root-level and partial data from both the X and Y directions and generates
            /// similar data for the X and Y directions.
            /// </summary>
            public static PrimingMode Interior(Quadrant quad)
            {
                return new PrimingMode(0b11, quad);
            }

            /// <summary>
            /// Gets the <see cref="PrimingMode"/> for a child of a tile with an <see cref="Interior(Quadrant)"/> priming mode.
            /// </summary>
            public static PrimingMode InteriorChild(Quadrant parentQuad, Quadrant childQuad)
            {
                return new PrimingMode((byte)(parentQuad | childQuad), parentQuad);
            }

            /// <summary>
            /// Indicates whether this is an <see cref="Interior"/> priming mode, and if so, gets its quadrant.
            /// </summary>
            public bool IsInterior(out Quadrant quad)
            {
                quad = _interior;
                return _boundary == 0b11;
            }

            /// <summary>
            /// Indicates whether this is <see cref="Boundary_X"/>.
            /// </summary>
            public bool IsBoundary_X => (_boundary & 0b1) == 0b0;

            /// <summary>
            /// Indicates whether this is <see cref="Boundary_Y"/>.
            /// </summary>
            public bool IsBoundary_Y => _boundary == 0b10;
        }
    }
}
