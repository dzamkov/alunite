﻿using System;
using System.Collections.Generic;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Alunite.Reactive;

namespace Alunite
{
    [TestClass]
    public class ReactiveTests
    {
        [TestMethod]
        public void Test1()
        {
            Integrator integrator = new Integrator(Time.Zero);
            State<int> a;
            Dynamic<int> b;
            using (integrator.Transact(out var trans))
            {
                a = new State<int>(trans, 0);
                b = Dynamic.Compute(obs => a[obs] * 2);
                Assert.AreEqual(0, a[trans]);
                Assert.AreEqual(0, b[trans]);
                a[trans] = 5;
                Assert.AreEqual(5, a[trans]);
                Assert.AreEqual(10, b[trans]);
                a[trans] = 9;
                Assert.AreEqual(9, a[trans]);
                Assert.AreEqual(18, b[trans]);
            }
            using (integrator.Transact(out var trans))
            {
                Assert.AreEqual(9, a[trans]);
                a[trans] = 15;
                Assert.AreEqual(15, a[trans]);
                Assert.AreEqual(30, b[trans]);
            }
        }

        [TestMethod]
        public void Test2()
        {
            State<int> a = null;
            State<int> b = null;
            Integrator integrator = new Integrator(Time.Zero);
            using (integrator.Transact(out Transactor init))
            {
                a = new State<int>(init, 0);
                b = new State<int>(init, 1);
            }

            integrator.Update = trans =>
            {
                a[trans] = (int)(trans.Time / Time.FromSeconds(1.0));
                if (trans.Time > Time.FromSeconds(3.0))
                    b[trans] = 2;
            };

            Dynamic<int> c = Dynamic.Compute(obs => a[obs] + b[obs]);
            Dynamic<int> d = Dynamic.Compute(obs => a[obs] * b[obs]);

            using (integrator.Observe(Time.Zero, out Observer obs))
            {
                Assert.AreEqual(0, a[obs]);
                Assert.AreEqual(1, b[obs]);
                Assert.AreEqual(1, c[obs]);
                Assert.AreEqual(0, d[obs]);
            }

            using (integrator.Observe(Time.FromSeconds(0.5), out Observer obs))
            {
                Assert.AreEqual(0, a[obs]);
                Assert.AreEqual(1, b[obs]);
                Assert.AreEqual(1, c[obs]);
                Assert.AreEqual(0, d[obs]);
            }

            using (integrator.Observe(Time.FromSeconds(1.5), out Observer obs))
            {
                Assert.AreEqual(1, a[obs]);
                Assert.AreEqual(1, b[obs]);
                Assert.AreEqual(2, c[obs]);
                Assert.AreEqual(1, d[obs]);
            }

            using (integrator.Observe(Time.FromSeconds(2.5), out Observer obs))
            {
                Assert.AreEqual(2, a[obs]);
                Assert.AreEqual(1, b[obs]);
                Assert.AreEqual(3, c[obs]);
                Assert.AreEqual(2, d[obs]);
            }

            using (integrator.Observe(Time.FromSeconds(3.5), out Observer obs))
            {
                Assert.AreEqual(3, a[obs]);
                Assert.AreEqual(2, b[obs]);
                Assert.AreEqual(5, c[obs]);
                Assert.AreEqual(6, d[obs]);
            }

            using (integrator.Observe(Time.FromSeconds(4.5), out Observer obs))
            {
                Assert.AreEqual(4, a[obs]);
                Assert.AreEqual(2, b[obs]);
                Assert.AreEqual(6, c[obs]);
                Assert.AreEqual(8, d[obs]);
            }
        }

        [TestMethod]
        public void Test3()
        {
            State<int> a = null;
            State<int> b = null;
            Integrator integrator = new Integrator(Time.Zero);
            using (integrator.Transact(out Transactor init))
            {
                a = new State<int>(init, 5);
                b = new State<int>(init, 0);
            }

            integrator.Update = trans =>
            {
                b[trans] = a[trans];
            };

            using (integrator.Observe(Time.Zero, out Observer obs))
            {
                Assert.AreEqual(5, a[obs]);
                Assert.AreEqual(0, b[obs]);
            }

            using (integrator.Observe(Time.FromSeconds(1.0), out Observer obs))
            {
                Assert.AreEqual(5, a[obs]);
                Assert.AreEqual(5, b[obs]);
            }

            using (integrator.Transact(out Transactor trans))
            {
                a[trans] = 3;
                Assert.AreEqual(0, b[trans]);
            }

            using (integrator.Observe(Time.FromSeconds(1.0), out Observer obs))
            {
                Assert.AreEqual(3, a[obs]);
                Assert.AreEqual(3, b[obs]);
            }
        }

        [TestMethod]
        public void Test4()
        {
            State<Time> a = null;
            Integrator integrator = new Integrator(Time.Zero);
            using (integrator.Transact(out Transactor init))
            {
                a = new State<Time>(init, Time.FromSeconds(1));
            };

            integrator.Update = trans =>
            {
                if (a[trans] <= trans.Time)
                {
                    a[trans] = a[trans] * 2;
                }
            };

            using (integrator.Observe(Time.FromSeconds(0.5), out Observer obs))
            {
                Assert.AreEqual(Time.FromSeconds(1), a[obs]);
            }

            using (integrator.Observe(Time.FromSeconds(1.5), out Observer obs))
            {
                Assert.AreEqual(Time.FromSeconds(2), a[obs]);
            }

            using (integrator.Observe(Time.FromSeconds(2.5), out Observer obs))
            {
                Assert.AreEqual(Time.FromSeconds(4), a[obs]);
            }

            using (integrator.Observe(Time.FromSeconds(3.5), out Observer obs))
            {
                Assert.AreEqual(Time.FromSeconds(4), a[obs]);
            }

            using (integrator.Observe(Time.FromSeconds(750), out Observer obs))
            {
                Assert.AreEqual(Time.FromSeconds(1024), a[obs]);
            }
        }

        [TestMethod]
        public void TestGC()
        {
            // There's a potential problem with garbage collection due to using
            // weak references for invalidation propogation. If an intermediate value gets collected,
            // its dependents may not be notified of transitive invalidations. This test checks
            // for appropriate behavior in this scenario.

            State<int> a = null;
            Dynamic<int> b = null;
            Integrator integrator = new Integrator(Time.Zero);
            using (integrator.Transact(out Transactor init))
            {
                a = new State<int>(init, 1);
                b = Dynamic.Compute(obs => Dynamic.Compute(obs2 => a[obs2])[obs]);
            };

            integrator.Update = trans =>
            {
                if (trans.Time > Time.FromSeconds(1))
                    a[trans] = 2;
            };

            using (integrator.Observe(Time.FromSeconds(0.5), out Observer obs))
            {
                Assert.AreEqual(1, b[obs]);
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();

            using (integrator.Observe(Time.FromSeconds(1.5), out Observer obs))
            {
                Assert.AreEqual(2, b[obs]);
            }
        }

        [TestMethod]
        public void TestBag1()
        {
            StateBag<ulong> bag = null;
            Dynamic<ulong> sum = null;
            Integrator integrator = new Integrator(Time.Zero);
            using (integrator.Transact(out Transactor init))
            {
                bag = new StateBag<ulong>(init);
                sum = Dynamic.Compute(obs =>
                {
                    ulong x = 0;
                    foreach (var y in bag[obs])
                        x += y;
                    return x;
                });
            };

            integrator.Update = trans =>
            {
                bag.Add(trans, trans.Time.Ticks);
            };

            using (integrator.Observe(new Time(5), out Observer obs))
            {
                Assert.AreEqual<ulong>(10, sum[obs]);
            }

            StateBag<ulong>.ItemRef item;
            using (integrator.Transact(out Transactor trans))
            {
                item = bag.Add(trans, 100);
            }

            using (integrator.Observe(new Time(7), out Observer obs))
            {
                Assert.AreEqual<ulong>(121, sum[obs]);
            }

            using (integrator.Transact(out Transactor trans))
            {
                bag.Remove(trans, item);
            }

            using (integrator.Observe(new Time(7), out Observer obs))
            {
                Assert.AreEqual<ulong>(21, sum[obs]);
            }

            using (integrator.Transact(out Transactor trans))
            {
                item = bag.Add(trans, 10000);
            }

            using (integrator.Observe(new Time(50), out Observer obs))
            {
                Assert.AreEqual<ulong>(11225, sum[obs]);
            }
        }

        [TestMethod]
        public void TestShared()
        {
            State<int> a = null;
            DynamicShared<_TestDisposable> b = null;
            Integrator integrator = new Integrator(Time.Zero);
            using (integrator.Transact(out Transactor init))
            {
                a = new State<int>(init, 0);
                b = new DynamicShared<_TestDisposable>(x => new _TestDisposable(a[x]));
            };

            integrator.Update = trans =>
            {
                a[trans] = (int)(trans.Time / Time.FromSeconds(1));
            };

            Assert.AreEqual(0, _TestDisposable.Live);
            using (integrator.Observe(Time.FromSeconds(1.5), out Observer obs))
            {
                using (var shared = b.Use(obs))
                {
                    Assert.AreEqual(1, shared.Target.Value);
                    Assert.AreEqual(1, _TestDisposable.Live);
                }
            }

            using (integrator.Observe(Time.FromSeconds(2.5), out Observer obs))
            {
                using (var shared = b.Use(obs))
                {
                    Assert.AreEqual(2, shared.Target.Value);
                    Assert.AreEqual(2, _TestDisposable.Live);
                }
            }

            integrator.Base = Time.FromSeconds(2.5);
            Assert.AreEqual(1, _TestDisposable.Live);
            b.Dispose();
            Assert.AreEqual(0, _TestDisposable.Live);
        }

        private sealed class _TestDisposable : IDisposable
        {
            public static int Live = 0;
            public int Value;

            public _TestDisposable(int value)
            {
                Value = value;
                Interlocked.Increment(ref Live);
            }

            public void Dispose()
            {
                Value = 0;
                Interlocked.Decrement(ref Live);
            }
        }
    }
}