﻿using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Alunite.Data.Probability;

namespace Alunite.Data
{
    [TestClass]
    public class ProbabilityTests
    {
        /// <summary>
        /// Builds a <see cref="FiniteDistribution{T}"/> by repeatedly sampling the given distribution.
        /// </summary>
        private static FiniteDistribution<T> _analyze<T>(IDistribution<T> dist, uint samples)
        {
            SeedSampler sampler = new SeedSampler(Seed.Initial);
            LogScalar weight = ((LogScalar)samples).Reciprocal;
            FiniteKernelBuilder<T> builder = new FiniteKernelBuilder<T>();
            for (uint i = 0; i < samples; i++)
            {
                T value = sampler.Sample(dist);
                builder.Add(value, weight);
            }
            return (FiniteDistribution<T>)builder.Finish();
        }

        /// <summary>
        /// Computes the Kullback–Leibler divergence between two distributions.
        /// </summary>
        private static Scalar _divergence<T>(FiniteDistribution<T> p, FiniteDistribution<T> q)
        {
            Scalar res = 0;
            foreach (var entry in p)
                res += (Scalar)entry.Weight * (entry.Weight / q[entry.Value]).Ln;
            return res;
        }

        [TestMethod]
        public void TestLogScalarAdd()
        {
            uint count = 100;
            LogScalar x = 1.0 / count;
            LogScalar y = 0;
            for (uint i = 0; i < count; i++)
                y += x;
            Assert.IsTrue(Scalar.Abs((Scalar)y - 1) < 0.001);
        }

        [TestMethod]
        public void TestFiniteDistributionUniform()
        {
            FiniteDistribution<int> dice = Distribution.Uniform(0, 1, 2, 3, 4, 5);
            foreach (var entry in dice)
                Assert.AreEqual(1.0 / 6, entry.Weight);
            Assert.AreEqual<uint>(6, dice.Support.Size);
            Assert.AreEqual(1.0 / 6, dice[4]);
            FiniteDistribution<int> diceTest = _analyze(dice, 1000);
            Assert.IsTrue(_divergence(diceTest, dice) < 0.01);
            FiniteDistribution<int> diceOptimizeTest = _analyze(dice.Optimize(), 10000);
            Assert.IsTrue(_divergence(diceOptimizeTest, dice) < 0.001);
        }

        [TestMethod]
        public void TestFiniteDistributionCustom()
        {
            FiniteKernelBuilder<string> builder = new FiniteKernelBuilder<string>();
            builder.Add("Sunny", 0.3);
            builder.Add("Cloudy", 0.3);
            builder.Add("Rainy", 0.1);
            builder.Add("Foggy", 0.1);
            builder.Add("Stormy", 0.1);
            builder.Add("Snowy", 0.05);
            builder.Add("Misty", 0.045);
            builder.Add("Smokey", 0.005);
            FiniteDistribution<string> weather = (FiniteDistribution<string>)builder.Finish();
            FiniteDistribution<string> weatherTest = _analyze(weather, 1000);
            Assert.IsTrue(_divergence(weatherTest, weather) < 0.01);
            FiniteDistribution<string> weatherOptimizeTest = _analyze(weather.Optimize(), 10000);
            Assert.IsTrue(_divergence(weatherOptimizeTest, weather) < 0.001);
        }

        [TestMethod]
        public void TestIndexDistributionCommon()
        {
            Assert.AreEqual(1.0 / 5, IndexDistribution.UniformBelow(5)[2]);
            Assert.AreEqual(1.0 / 65, IndexDistribution.UniformBelow(65)[1]);
            Assert.AreEqual(0, IndexDistribution.UniformBelow(65)[67]);
            Assert.AreEqual(0, IndexDistribution.Point(3)[6]);
            Assert.AreEqual(1, IndexDistribution.Point(3)[3]);
        }

        [TestMethod]
        public void TestIndexDistributionCustom()
        {
            FiniteKernelBuilder<uint> builder = new FiniteKernelBuilder<uint>();
            builder.Add(0, 0.1);
            builder.Add(1, 0.2);
            builder.Add(3, 0.2);
            builder.Add(5, 0.2);
            builder.Add(6, 0.001);
            builder.Add(8, 0.099);
            builder.Add(9, 0.2);
            FiniteDistribution<uint> distSource = (FiniteDistribution<uint>)builder.Finish();
            IndexDistribution dist = distSource;
            Assert.AreEqual(0, dist[2]);
            Assert.AreEqual(0.2, dist[5]);
            Assert.AreEqual(0.099, dist[8]);
            FiniteDistribution<uint> distTest = _analyze(dist, 1000);
            Assert.IsTrue(_divergence(distTest, dist) < 0.01);
            FiniteDistribution<uint> distOptimizeTest = _analyze(dist.Optimize(), 10000);
            Assert.IsTrue(_divergence(distOptimizeTest, dist) < 0.001);
        }

        [TestMethod]
        public void TestIndexListDistributionChoice()
        {
            FiniteDistribution<List<uint>> distSource = Distribution.Uniform<List<uint>>(
                List.Of<uint>(0, 1, 0),
                List.Of<uint>(0, 1, 1),
                List.Of<uint>(1, 2, 2),
                List.Of<uint>(1, 3, 2));
            RegularIndexListDistribution dist = (RegularIndexListDistribution)distSource;
            Assert.AreEqual<uint>(4, dist.Support.Size);
            Assert.AreEqual(1.0 / 4, dist[List.Of<uint>(0, 1, 1)]);
            Assert.AreEqual(0, dist[List.Of<uint>(2, 1, 4)]);
            FiniteDistribution<List<uint>> distTest = _analyze(dist, 1000);
            Assert.IsTrue(_divergence(distTest, distSource) < 0.01);
            /* FiniteDistribution<List<uint>> distOptimizeTest = _analyze(dist.Optimize(), 10000);
            Assert.IsTrue(_divergence(distOptimizeTest, distSource) < 0.001); */
        }

        [TestMethod]
        public void TestIndexListDistributionCommon()
        {
            Assert.AreEqual(1.0 / (2 * 3 * 4), RegularIndexListDistribution.IndependentUniformBelow(2, 3, 4)[List.Of<uint>(1, 2, 3)]);
            Assert.AreEqual(0, RegularIndexListDistribution.IndependentUniformBelow(2, 3, 4)[List.Of<uint>(3, 2, 1)]);
            Assert.AreEqual(RegularIndexListSet.IndependentBelow(1, 2, 3), RegularIndexListDistribution.IndependentUniformBelow(1, 2, 3).Support);
            Assert.AreEqual(List.Of<uint>(5, 6, 7, 6, 5, 2), RegularIndexListDistribution.IndependentUniformBelow(5, 6, 7, 6, 5, 2).Bounds);
        }

        [TestMethod]
        public void TestIndexListDistributionEquality()
        {
            Predicate<List<uint>> pred = list => list[0] == 1 || list[1] == 1;
            RegularIndexListKernel kernel_0 = RegularIndexListKernel
                .IndependentIndicatorBelow(2, 2)
                .FilterSubseq(IndexSet.Of(0, 1), pred);
            RegularIndexListKernel kernel_1 = RegularIndexListKernel
                .IndependentIndicatorBelow(2, 2)
                .ConflateSubseq(IndexSet.Of(0, 1), kernel_0);
            RegularIndexListKernel kernel_2 = RegularIndexListKernel
                .Indicator(RegularIndexListSet.Build(List.Of<uint>(2, 2), pred));
            Assert.AreEqual(kernel_0, kernel_1);
            Assert.IsTrue(RegularIndexListKernel.Distance(kernel_0, kernel_2) < 0.00001);
        }

        [TestMethod]
        public void TestIndexListDistributionFiltering()
        {
            FiniteKernelBuilder<List<uint>> neighborsBuilder = new FiniteKernelBuilder<List<uint>>();
            neighborsBuilder.Add(List.Of<uint>(0, 0), 1);
            neighborsBuilder.Add(List.Of<uint>(1, 2), 1);
            neighborsBuilder.Add(List.Of<uint>(2, 1), 1);
            neighborsBuilder.Add(List.Of<uint>(2, 2), 1);
            RegularIndexListKernel neighbors = (RegularIndexListKernel)neighborsBuilder.Finish();
            RegularIndexListKernel kernel = RegularIndexListDistribution.IndependentUniformBelow(3, 3, 3, 3);

            // Verify support works properly
            var support = RegularIndexListSet.Build(List.Of<uint>(3, 3), list => !neighbors[list].IsZero);
            Assert.AreEqual<uint>(4, support.Size);
            Assert.AreEqual(support, neighbors.Support);

            // There's three equivalent ways of doing this. Make sure they all yield the same results
            RegularIndexListKernel kernel_0 = kernel
                .ConflateSubseq(IndexSet.Of(0, 1), neighbors)
                .ConflateSubseq(IndexSet.Of(1, 2), neighbors)
                .ConflateSubseq(IndexSet.Of(2, 3), neighbors);
            RegularIndexListKernel kernel_1 = kernel
                .IntersectSubseq(IndexSet.Of(0, 1), support)
                .IntersectSubseq(IndexSet.Of(1, 2), support)
                .IntersectSubseq(IndexSet.Of(2, 3), support);
            Predicate<List<uint>> areNeighbors = list => !neighbors[list].IsZero;
            RegularIndexListKernel kernel_2 = kernel
                .FilterSubseq(IndexSet.Of(0, 1), areNeighbors)
                .FilterSubseq(IndexSet.Of(1, 2), areNeighbors)
                .FilterSubseq(IndexSet.Of(2, 3), areNeighbors);
            Assert.IsTrue(RegularIndexListKernel.Distance(kernel_0, kernel_1) < 0.00001);
            Assert.AreEqual(kernel_1, kernel_2);
            kernel = kernel_0;

            // Check resulting kernel
            Assert.AreEqual(1.0 / 81, kernel[List.Of<uint>(0, 0, 0, 0)]);
            Assert.AreEqual(9.0 / 81, kernel.Weight);
            Assert.AreEqual(1.0 / 9, kernel.Distribution[List.Of<uint>(1, 2, 1, 2)]);
            Assert.AreEqual(0, kernel.Distribution[List.Of<uint>(1, 1, 2, 2)]);
        }

        [TestMethod]
        public void TestIndexListDistributionSelect()
        {
            FiniteKernelBuilder<List<uint>> aBuilder = new FiniteKernelBuilder<List<uint>>();
            aBuilder.Add(List.Of<uint>(0, 0, 0, 0), 0.1);
            aBuilder.Add(List.Of<uint>(0, 1, 2, 3), 0.2);
            aBuilder.Add(List.Of<uint>(1, 1, 1, 1), 0.3);
            aBuilder.Add(List.Of<uint>(0, 1, 1, 0), 0.4);
            aBuilder.Add(List.Of<uint>(2, 1, 1, 0), 0.5);
            RegularIndexListKernel a = (RegularIndexListKernel)aBuilder.Finish();
            FiniteKernelBuilder<List<uint>> bBuilder = new FiniteKernelBuilder<List<uint>>();
            bBuilder.Add(List.Of<uint>(0, 0, 0, 0, 0), 0.1);
            bBuilder.Add(List.Of<uint>(2, 0, 1, 3, 1), 0.2);
            bBuilder.Add(List.Of<uint>(1, 1, 1, 1, 1), 0.3);
            bBuilder.Add(List.Of<uint>(1, 0, 1, 0, 1), 0.4);
            bBuilder.Add(List.Of<uint>(1, 2, 1, 0, 1), 0.5);
            RegularIndexListKernel b = (RegularIndexListKernel)bBuilder.Finish();
            RegularIndexListKernel c = a.Select(List.Of<uint>(2, 0, 1, 3, 1));
            Assert.IsTrue(RegularIndexListKernel.Distance(b, c) < 0.00001);
        }

        /* [TestMethod]
        public void TestIndexListDistributionDecompose()
        {
            FiniteKernelBuilder<List<uint>> builder = new FiniteKernelBuilder<List<uint>>();
            builder.Add(List.Of<uint>(0, 0, 0, 0), 1);
            builder.Add(List.Of<uint>(0, 0, 0, 1), 1);
            builder.Add(List.Of<uint>(0, 0, 1, 0), 1);
            builder.Add(List.Of<uint>(0, 0, 1, 1), 1);
            builder.Add(List.Of<uint>(0, 1, 0, 0), 1);
            builder.Add(List.Of<uint>(0, 1, 0, 1), 1);
            builder.Add(List.Of<uint>(1, 0, 0, 0), 1);
            builder.Add(List.Of<uint>(1, 0, 0, 1), 1);
            builder.Add(List.Of<uint>(1, 0, 1, 0), 1);
            builder.Add(List.Of<uint>(1, 0, 1, 1), 1);
            builder.Add(List.Of<uint>(1, 1, 0, 0), 1);
            builder.Add(List.Of<uint>(1, 1, 0, 1), 1);
            RegularIndexListKernel kernel = (RegularIndexListKernel)builder.Finish();
            var decomposition = kernel.Distribution.Decompose(0.01, List.Of<uint>(1, 2, 1), out var comps);
            Assert.IsTrue(decomposition.IsSingleton(out List<uint> items));
            Assert.AreEqual(List.Of<uint>(0, 0, 0), items);
            // TODO: Reconstruction test with concatenation
        } */

        [TestMethod]
        public void TestIndexListDistance()
        {
            FiniteKernelBuilder<List<uint>> aBuilder = new FiniteKernelBuilder<List<uint>>();
            aBuilder.Add(List.Of<uint>(0, 0, 0), 0.1);
            aBuilder.Add(List.Of<uint>(0, 1, 2), 0.5);
            aBuilder.Add(List.Of<uint>(1, 1, 1), 0.3);
            aBuilder.Add(List.Of<uint>(0, 1, 1), 0.2);
            RegularIndexListKernel a = (RegularIndexListKernel)aBuilder.Finish();
            FiniteKernelBuilder<List<uint>> bBuilder = new FiniteKernelBuilder<List<uint>>();
            bBuilder.Add(List.Of<uint>(0, 0, 0), 0.2);
            bBuilder.Add(List.Of<uint>(0, 1, 2), 0.4);
            bBuilder.Add(List.Of<uint>(1, 1, 1), 0.4);
            bBuilder.Add(List.Of<uint>(0, 1, 1), 0.7);
            RegularIndexListKernel b = (RegularIndexListKernel)bBuilder.Finish();
            Scalar expDist_a_b = Scalar.Sqrt(_sqrDist(0.1, 0.2) + _sqrDist(0.5, 0.4) + _sqrDist(0.3, 0.4) + _sqrDist(0.2, 0.7));
            Scalar dist_a_b = RegularIndexListKernel.Distance(a, b);
            Assert.IsTrue(Scalar.Abs(expDist_a_b - dist_a_b) < 0.0001);
        }

        private static Scalar _sqrDist(Scalar x, Scalar y)
        {
            Scalar dist = Scalar.Ln(x) - Scalar.Ln(y);
            return dist * dist;
        }
    }
}
