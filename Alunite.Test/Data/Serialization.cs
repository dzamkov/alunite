﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Alunite.Data.Serialization;
using Alunite.Data.Geometry;
using Alunite.Graphics;

namespace Alunite.Data
{
    [TestClass]
    public class SerializationTests
    {
        [TestMethod]
        public void TestStruct()
        {
            // Construct test data
            Struct value = new Struct
            {
                A = 12,
                B = 482112
            };

            // Test serialization with buffer
            EncodingStrategy strat = EncodingStrategy.Balanced;
            Encoding<Struct> encoding = Encoding.Struct<Struct>(false, EncodingResolver.Default(strat), strat);
            byte[] buffer = new byte[1024];
            Span<byte> writeBuffer = buffer;
            Binary.Write(ref writeBuffer, encoding, in value);
            ReadOnlySpan<byte> readBuffer = buffer;
            Binary.Read(ref readBuffer, encoding, out Struct testValue);
            Assert.AreEqual(value, testValue);
        }
        
        /// <summary>
        /// Plain old data struct used to test serialization of various field types.
        /// </summary>
        public struct Struct
        {
            public byte A;
            public uint B;
            /* public float C;
            public ulong D;
            public ushort E;
            public double F;
            public Axis3 G;
            public Vector2 H;
            public DateTime I; */
        }
        
        [TestMethod]
        public void TestColor()
        {
            byte[] buffer = new byte[1024];
            buffer[0] = 64;
            buffer[1] = 128;
            buffer[2] = 192;
            buffer[3] = 64;
            buffer[4] = 64;
            buffer[5] = 64;
            Encoding<Paint> encoding = Paint.Encoding.B8_G8_R8;
            ReadOnlySpan<byte> readBuffer = buffer;
            Binary.Read(ref readBuffer, encoding, out Paint test_0);
            Binary.Read(ref readBuffer, encoding, out Paint test_1);
            Assert.AreEqual(new Paint(192 / 255.0, 128 / 255.0, 64 / 255.0, 1), test_0);
            Assert.AreEqual(new Paint(64 / 255.0, 64 / 255.0, 64 / 255.0, 1), test_1);
        }
    }
}
