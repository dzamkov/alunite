﻿using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Alunite.Data
{
    [TestClass]
    public class BagTests
    {
        [TestMethod]
        public void TestFilter()
        {
            BagBuilder<uint> bag = BagBuilder<uint>.CreateEmpty();
            for (uint i = 1; i < 100; i++)
                bag.Add(i);
            using (var it = bag.Iterate())
            {
                while (it.TryNext(out uint value))
                    if (value % 3 == 1)
                        it.Remove();
            }

            Assert.AreEqual<uint>(66, bag.Size);
            uint sum = 0;
            using (var it = bag.Iterate())
            {
                while (it.TryNext(out uint value))
                    sum += value;
            }
            Assert.AreEqual<uint>(3333, sum);
        }
    }
}
