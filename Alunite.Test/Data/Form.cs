﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Alunite.Data.Abstraction;

namespace Alunite.Data
{
    [TestClass]
    public class FormTests
    {
        private static bool _areClose(Scalar x, Scalar y)
        {
            return (x - y) < 0.001;
        }

        [TestMethod]
        public void TestSystem1()
        {
            ListBuilder<ScalarForm> vars = new ListBuilder<ScalarForm>();
            ScalarForm x = ScalarForm.Declare(vars);
            ScalarForm y = ScalarForm.Declare(vars);
            ScalarForm z = ScalarForm.Declare(vars);
            Assert.IsTrue(ScalarForm.TryEquate(vars, 3 * x + 2 * y - z, 1));
            Assert.IsTrue(ScalarForm.TryEquate(vars, 2 * x - 2 * y + 4 * z, -2));
            Assert.IsTrue(ScalarForm.TryEquate(vars, -x + 0.5 * y - z, 0));
            List<Scalar> solution = ScalarForm.Solve(vars);
            Assert.IsTrue(_areClose(1, x[solution]));
            Assert.IsTrue(_areClose(-2, y[solution]));
            Assert.IsTrue(_areClose(-2, z[solution]));
        }
    }
}
