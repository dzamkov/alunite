﻿using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Alunite.Data
{
    [TestClass]
    public class SetTests
    {
        [TestMethod]
        public void TestSet()
        {
            var set = Set.Of(1, 2, 4, 2);
            Assert.IsTrue(set.Contains(4));
            Assert.IsFalse(set.Contains(5));
            Assert.AreEqual<uint>(3, set.Size);
            Assert.AreEqual(Set.Of(1, 2, 3), Set.Of(1, 2, 2, 3, 1, 2));
        }

        [TestMethod]
        public void TestMinimalCover()
        {
            ListBuilder<IndexSet> candidates = new ListBuilder<IndexSet>();
            candidates.Append(IndexSet.Of(1, 2, 5, 6));
            candidates.Append(IndexSet.Of(2, 6, 9));
            candidates.Append(IndexSet.Of(1, 9));
            candidates.Append(IndexSet.Of(7, 8));
            candidates.Append(IndexSet.Of(0, 7, 9));
            candidates.Append(IndexSet.Of(0, 1, 2, 3, 8));
            candidates.Append(IndexSet.Of(0, 1, 3, 4, 5));
            candidates.Append(IndexSet.Of(4, 5, 9));
            candidates.Append(IndexSet.Of(10, 11));
            IndexSet solution = IndexSet.MinimalCover(12, candidates.Finish());
            Assert.AreEqual(IndexSet.Of(1, 3, 6, 8), solution);
        }

        [TestMethod]
        public void TestIndexListSetBuild()
        {
            RegularIndexListSet set = RegularIndexListSet.Build(
                List.Of<uint>(4, 4, 4),
                list => list[0] * list[1] == list[0] * list[2]);
            Assert.IsTrue(set.Contains(List.Of<uint>(0, 0, 0)));
            Assert.IsTrue(set.Contains(List.Of<uint>(1, 1, 1)));
            Assert.IsTrue(set.Contains(List.Of<uint>(0, 2, 3)));
            Assert.IsFalse(set.Contains(List.Of<uint>(1, 2, 3)));
            Assert.IsFalse(set.Contains(List.Of<uint>(3, 2, 1)));
            Assert.AreEqual(List.Of<uint>(4, 4, 4), set.Bounds);
            Assert.AreEqual<uint>(28, set.Size);
        }

        [TestMethod]
        public void TestIndexListSetSelect()
        {
            RegularIndexListSet set = RegularIndexListSet.Build(
                List.Of<uint>(12, 4, 4, 4),
                list => list[0] == list[1] * list[1] + list[2] * list[2] + list[3] * list[3]);
            RegularIndexListSet sub = set.Select(2, 0, 0, 2).Select(1, 3);
            Assert.IsTrue(sub.Contains(List.Of<uint>(11, 3)));
            Assert.IsFalse(sub.Contains(List.Of<uint>(11, 0)));
            Assert.AreEqual<uint>(24, sub.Size);
        }

        [TestMethod]
        public void TestIndexListSetFactorize1()
        {
            bool isEven(uint x) => x % 2 == 0;
            RegularIndexListSet set = RegularIndexListSet.Build(
                List.Of<uint>(4, 4, 4, 4),
                list => isEven(list[0]) ^ !isEven(list[1]) ^ isEven(list[3]));
            var factors = set.Factorize(List.Of<uint>(1, 1, 1, 1));
            Assert.AreEqual<uint>(4, factors.Size);
            Assert.AreEqual(set, _unfactorize(factors));
        }

        [TestMethod]
        public void TestIndexListSetFactorize2()
        {
            var set = (RegularIndexListSet)Set.Of<List<uint>>(
                List.Of<uint>(0, 0),
                List.Of<uint>(0, 1),
                List.Of<uint>(0, 2),
                List.Of<uint>(1, 0),
                List.Of<uint>(1, 3),
                List.Of<uint>(1, 4),
                List.Of<uint>(2, 1),
                List.Of<uint>(2, 3),
                List.Of<uint>(2, 5),
                List.Of<uint>(3, 2),
                List.Of<uint>(3, 4),
                List.Of<uint>(3, 5));
            var factors = set.Factorize(List.Of<uint>(1, 1));
            Assert.AreEqual<uint>(4, factors.Size);
            Assert.AreEqual(set, _unfactorize(factors));

            set = set.Select(1, 0);
            factors = set.Factorize(List.Of<uint>(1, 1));
            Assert.AreEqual<uint>(4, factors.Size);
            Assert.AreEqual(set, _unfactorize(factors));
        }

        private static RegularIndexListSet _unfactorize(Bag<List<RegularIndexListSet>> factors)
        {
            RegularIndexListSet res = RegularIndexListSet.None;
            foreach (List<RegularIndexListSet> factor in factors)
                res |= RegularIndexListSet.Concat(factor);
            return res;
        }

        [TestMethod]
        public void TestIndexSet()
        {
            Assert.AreEqual<uint>(64, IndexSet.Below(64).Bound);
        }
    }
}
