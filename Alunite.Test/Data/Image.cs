﻿using System;
using System.IO;
using System.Globalization;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Alunite.Data
{
    [TestClass]
    public class ImageTests
    {
        [TestMethod]
        public void TestMinimalCover()
        {
            // This is too slow to run right now
            Assert.Inconclusive();

            // Set cover problem from http://people.brunel.ac.uk/~mastjjb/jeb/orlib/scpinfo.html
            // Parse weights and options from file
            List<Scalar> costs;
            BoolImage2 options;
            using (Stream stream = System.Reflection.Assembly
                .GetAssembly(typeof(ImageTests))
                .GetManifestResourceStream("Alunite.Test.Data.Scp41.dat"))
            {
                StreamReader reader = new StreamReader(stream);
                string header = reader.ReadLine();
                string[] costsText = header.Split(' ');
                string[] linesText = reader.ReadToEnd().Split('\n');
                var costsBuilder = ListBuilder<Scalar>.CreateDefault((uint)costsText.Length);
                var optionsBuilder = new BoolImageBuilder2((uint)linesText.Length, (uint)costsText.Length);
                for (uint i = 0; i < costsBuilder.Length; i++)
                    costsBuilder[i] = uint.Parse(costsText[i], CultureInfo.InvariantCulture);
                for (uint i = 0; i < optionsBuilder.Size_X; i++)
                {
                    string[] colsText = linesText[i].Split(' ');
                    foreach (string colText in colsText)
                        optionsBuilder[i, uint.Parse(colText, CultureInfo.InvariantCulture) - 1] = true;
                }
                costs = costsBuilder.Finish();
                options = optionsBuilder.Finish();
            }

            // Find minimal cover
            BoolList factors = BoolImage2.MinimalCover(options, costs);
            Scalar cost = 0;
            for (uint i = 0; i < factors.Length; i++)
                if (factors[i])
                    cost += costs[i];
            Assert.AreEqual(429, cost);
        }
    }
}
