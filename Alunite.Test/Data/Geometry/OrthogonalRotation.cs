﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Alunite.Data.Geometry
{
    [TestClass]
    public class OrthogonalRotationTests
    {
        [TestMethod]
        public void TestComposition3()
        {
            foreach (var a in Roflection3i.All)
            {
                Assert.AreEqual(a, a.Inverse.Inverse);
                foreach (var b in Roflection3i.All)
                {
                    Assert.AreEqual(a, (a * b) * b.Inverse);
                    Assert.AreEqual(b, a.Inverse * (a * b));
                    Assert.IsTrue((a * b).IsRotation || !a.IsRotation || !b.IsRotation);
                }
            }

            foreach (var a in PlanarRoflection3i.All)
            {
                Assert.AreEqual(a, a.Inverse.Inverse);
                foreach (var b in PlanarRoflection3i.All)
                {
                    Assert.AreEqual(a, (a * b) * b.Inverse);
                    Assert.AreEqual(b, a.Inverse * (a * b));
                    Assert.IsTrue((a * b).IsRotation || !a.IsRotation || !b.IsRotation);
                }
            }
        }

        [TestMethod]
        public void TestInverse3()
        {
            foreach (var rof in Roflection3i.All)
            {
                // Test octets
                var octet = rof.Inverse.Apply(rof.Apply(Octet.Identity));
                for (uint i = 0; i < 8; i++)
                    Assert.AreEqual((Octant)i, octet[(Octant)i]);

                // Test axis quartets
                var axisQuartet = rof.Inverse.Apply(rof.Apply(AxialQuartet.Identity));
                for (uint i = 0; i < 12; i++)
                    Assert.AreEqual((AxisQuadrant)i, axisQuartet[(AxisQuadrant)i]);
            }
        }

        [TestMethod]
        public void TestToString3()
        {
            var names = new Dictionary<Dir3, string>
            {
                [Dir3.NegX] = "-x",
                [Dir3.PosX] = "+x",
                [Dir3.NegY] = "-y",
                [Dir3.PosY] = "+y",
                [Dir3.NegZ] = "-z",
                [Dir3.PosZ] = "+z"
            };
            foreach (Roflection3i rof in Roflection3i.All)
            {
                Roflection3i invRof = rof.Inverse;
                string expected = "(" +
                    names[invRof.Apply(Dir3.PosX)] + ", " +
                    names[invRof.Apply(Dir3.PosY)] + ", " +
                    names[invRof.Apply(Dir3.PosZ)] + ")";
                Assert.AreEqual(expected, rof.ToString());
            }
        }

        [TestMethod]
        public void TestTangent3()
        {
            var cube = new AxialDuet3<Dir2>(Dir2.PosX, Dir2.NegX, Dir2.NegY, Dir2.PosX, Dir2.PosY, Dir2.PosY);
            var comparer = AxialDuet3<Dir2>.GetComparer(EqualityComparer<Dir2>.Default);
            foreach (var a in Rotation3i.All)
            {
                var aCube = _apply(a, cube);
                var cube2 = _apply(a.Inverse, aCube);
                Assert.IsTrue(comparer.Equals(cube, cube2));
                foreach (var b in Rotation3i.All)
                {
                    var abCube1 = _apply(b, aCube);
                    var abCube2 = _apply(b * a, cube);
                    Assert.IsTrue(comparer.Equals(abCube1, abCube2));
                }
            }
        }

        [TestMethod]
        public void TestBurnside()
        {
            // By application of Burnside's lemma, it can be shown there are 192 distinct ways of painting an arrow on
            // each side of a cube. Let's see if we can reproduce this result using Rotation3i
            // Reference: http://www.baxterweb.com/puzzles/burnside5.pdf
            uint count = 0;
            HashSet<AxialDuet3<Dir2>> considered = new HashSet<AxialDuet3<Dir2>>(
                AxialDuet3<Dir2>.GetComparer(EqualityComparer<Dir2>.Default));
            for (uint i = 0; i < (1 << 12); i++)
            {
                AxialDuet3<Dir2> cube = new AxialDuet3<Dir2>(
                    (Dir2)((i >> 0) & 0b11),
                    (Dir2)((i >> 2) & 0b11),
                    (Dir2)((i >> 4) & 0b11),
                    (Dir2)((i >> 6) & 0b11),
                    (Dir2)((i >> 8) & 0b11),
                    (Dir2)((i >> 10) & 0b11));
                if (!considered.Contains(cube))
                {
                    count++;
                    foreach (var rot in Rotation3i.All)
                        considered.Add(_apply(rot, cube));
                }
            }
            Assert.AreEqual(192u, count);
        }

        private static AxialDuet3<Dir2> _apply(Rotation3i rot, AxialDuet3<Dir2> cube)
        {
            Dir3 negX = rot.Apply(Dir3.NegX, out Rotation2i tan_negX);
            Dir3 posX = rot.Apply(Dir3.PosX, out Rotation2i tan_posX);
            Dir3 negY = rot.Apply(Dir3.NegY, out Rotation2i tan_negY);
            Dir3 posY = rot.Apply(Dir3.PosY, out Rotation2i tan_posY);
            Dir3 negZ = rot.Apply(Dir3.NegZ, out Rotation2i tan_negZ);
            Dir3 posZ = rot.Apply(Dir3.PosZ, out Rotation2i tan_posZ);
            AxialDuet3<Dir2> nCube = default;
            nCube[negX] = tan_negX * cube.NegX;
            nCube[posX] = tan_posX * cube.PosX;
            nCube[negY] = tan_negY * cube.NegY;
            nCube[posY] = tan_posY * cube.PosY;
            nCube[negZ] = tan_negZ * cube.NegZ;
            nCube[posZ] = tan_posZ * cube.PosZ;
            return nCube;
        }

        private void _areSimilar(Vector3 a, Vector3 b)
        {
            Assert.IsTrue((b - a).SqrLength < 0.0001);
        }

        [TestMethod]
        public void TestConversion()
        {
            var str = new System.Text.StringBuilder();
            for (byte i = 0; i < Rotation3i.Count; i++)
            {
                Rotation3i rot = Rotation3i.ByCode(i);
                Matrix3x3 mat = rot;
                Rotation3 srot = rot;
                Vector3 test = new Vector3(1, 2, 3);
                _areSimilar(mat * test, srot * test);
            }
        }

        [TestMethod]
        public void TestMisc()
        {
            Assert.AreEqual(Dir3.NegZ, Rotation3i.NegY_PosX_PosZ.Apply(Dir3.NegZ, out Rotation2i local));
            Assert.AreEqual(Rotation2i.PosY_NegX, local);
            Assert.AreEqual(Dir3.NegZ, Rotation3i.PosY_PosX_NegZ.Apply(Dir3.PosZ, out local));
            Assert.AreEqual(Rotation2i.PosX_PosY, local);
            Assert.AreEqual(Dir3.NegZ, Rotation3i.NegX_PosY_NegZ.Apply(Dir3.PosZ, out local));
            Assert.AreEqual(Rotation2i.PosY_NegX, local);
            Assert.AreEqual(Roflection2i.PosY_PosX, PlanarRotation3i.PosY_PosX_NegZ.X_Y);
            Assert.AreEqual(new BoolAxisQuartet(0b0111_0100_0010), Roflection3i.Identity * new BoolAxisQuartet(0b0111_0100_0010));
            Assert.AreEqual(Roflection3i.PosX_PosY_NegZ, new Roflection3i(Roflection2i.PosX_PosY, Reflection1.Neg));
        }
    }
}