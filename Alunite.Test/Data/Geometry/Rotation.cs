﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Alunite.Data.Probability;

namespace Alunite.Data.Geometry
{
    [TestClass]
    public class RotationTests
    {
        [TestMethod]
        public void TestOn()
        {
            foreach (Scalar angle in List.Of(1, -1, 3, -3))
            {
                Rotation2 source = Rotation2.FromAngle(angle);
                Rotation3 a = Rotation3.On_X_Y(source);
                Rotation3 b = Rotation3.About(new Vector3(0, 0, 1), angle);
                Assert.IsTrue(Rotation3.AreLikelyEqual(a, b));
            }
        }

        [TestMethod]
        public void Test2()
        {
            Assert.AreEqual(Rotation2.Flip, Rotation2.Cross * Rotation2.Cross);
            Assert.AreEqual(Rotation2.Identity, Rotation2.Flip * Rotation2.Flip);
            Assert.AreEqual(-Matrix2x2.Identity, Rotation2.Flip);

            Scalar angle = 0;
            Rotation2 rot = Rotation2.Identity;
            SeedSampler sampler = new SeedSampler(Seed.Initial);
            for (uint i = 0; i < 5; i++)
            {
                Scalar addAngle = sampler.SampleUnit() * 10 - 5;
                if (i == 2) addAngle = Scalar.Pi;
                Rotation2 addAngleRot = Rotation2.FromAngle(addAngle);
                angle += addAngle;
                rot = addAngleRot * rot;
                Scalar normAngle = (angle + Scalar.Pi) % (2 * Scalar.Pi) - Scalar.Pi;
                Assert.IsTrue(Scalar.AreLikelyEqual(normAngle, rot.Angle));
                Vector2 vec = new Vector2(Scalar.Cos(angle), Scalar.Sin(angle));
                Vector2 testVec = rot * new Vector2(1, 0);
                Assert.IsTrue(Vector2.AreLikelyEqual(vec, testVec));
            }
        }
        
        [TestMethod]
        public void TestWahba()
        {
            Rotation3 rot = Rotation3i.PosY_PosZ_PosX;
            Vector3 a = new Vector3(1, 3, 2);
            Vector3 b = new Vector3(-4, 2, -3);
            Vector3 c = new Vector3(4, -4, 7);
            Vector3 nA = rot * a;
            Vector3 nB = rot * b;
            Vector3 nC = rot * c;
            Matrix3x3 cov = Matrix.Outer(a, nA) + Matrix.Outer(b, nB) + Matrix.Outer(c, nC);
            Rotation.Wahba(cov, out Rotation3 testRot, out Scalar trace);
            Assert.IsTrue(Rotation3.AreLikelyEqual(rot, testRot));
        }
    }
}
