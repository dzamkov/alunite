﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Alunite.Data.Geometry
{
    [TestClass]
    public class OctreeTests
    {
        [TestMethod]
        public void Test1()
        {
            var octrees = new OctreeFactory<bool>(EqualityComparer<bool>.Default);
            var t = octrees.Leaf(true);
            var f = octrees.Leaf(false);
            var x1 = octrees.Group(t, t, t, t, f, f, f, f);
            var x2 = octrees.Group(t, t, t, t, f, f, f, f);

            Assert.AreEqual(true, octrees.GetLeaf(x1, 1, new ShortVector3i(0, 0, 0)));
            Assert.IsTrue(x1 == x2);
        }

        [TestMethod]
        public void Test2()
        {
            const int height = 4;
            const int size = 1 << height;
            Func<Vector3i, int> value = offset => (offset.X * 313 + ((offset.Y * 77) ^ offset.Z)) % 15;

            var octrees = new OctreeFactory<int>(EqualityComparer<int>.Default);
            var octree = octrees.Build(value, size, new Vector3i(0, 0, 0));

            // Verify the octree is correct
            for (short z = 0; z < size; z++)
            {
                for (short y = 0; y < size; y++)
                {
                    for (short x = 0; x < size; x++)
                    {
                        ShortVector3i offset = new ShortVector3i(x, y, z);
                        Assert.AreEqual(value(offset), octrees.GetLeaf(octree, height, offset));
                    }
                }
            }
        }

        [TestMethod]
        public void Test3()
        {
            OctreeFactory<bool> octrees = new OctreeFactory<bool>(EqualityComparer<bool>.Default);
            Func<Vector3i, bool> value = offset => (offset.X * 313 + offset.Y * 77 + offset.Z * 21) % 53 % 2 == 0;

            // Set the octree to a really complicated pattern
            var octree = octrees.Leaf(false);
            for (short z = 0; z < 8; z++)
            {
                for (short y = 0; y < 8; y++)
                {
                    for (short x = 0; x < 8; x++)
                    {
                        ShortVector3i offset = new ShortVector3i(x, y, z);
                        Assert.AreEqual(false, octrees.GetLeaf(octree, 3, offset));
                        octrees.SetLeaf(ref octree, 3, offset, value(offset));
                        Assert.AreEqual(value(offset), octrees.GetLeaf(octree, 3, offset));
                    }
                }
            }

            // Verify the pattern is still there
            for (short z = 0; z < 8; z++)
            {
                for (short y = 0; y < 8; y++)
                {
                    for (short x = 0; x < 8; x++)
                    {
                        ShortVector3i offset = new ShortVector3i(x, y, z);
                        Assert.AreEqual(value(offset), octrees.GetLeaf(octree, 3, offset));
                    }
                }
            }
        }

        [TestMethod]
        public void TestShortOctantList()
        {
            // Test shift/unshift
            Octant octant;
            ShortOctantList list1 = ShortOctantList.Empty;
            Assert.IsTrue(ShortOctantList.TryPrepend(ref list1, Octant.PosX_NegY_PosZ));
            Assert.IsTrue(ShortOctantList.TryPrepend(ref list1, Octant.NegX_NegY_PosZ));
            Assert.IsTrue(ShortOctantList.TryPrepend(ref list1, Octant.PosX_PosY_PosZ));
            Assert.AreEqual(Octant.PosX_PosY_PosZ, list1[0]);
            Assert.AreEqual(Octant.NegX_NegY_PosZ, list1[1]);
            Assert.AreEqual(Octant.PosX_NegY_PosZ, list1[2]);
            Assert.IsTrue(ShortOctantList.TryUnprepend(ref list1, out octant));
            Assert.AreEqual(Octant.PosX_PosY_PosZ, octant);
            Assert.IsTrue(ShortOctantList.TryUnprepend(ref list1, out octant));
            Assert.AreEqual(Octant.NegX_NegY_PosZ, octant);
            Assert.IsTrue(ShortOctantList.TryUnprepend(ref list1, out octant));
            Assert.AreEqual(Octant.PosX_NegY_PosZ, octant);
            Assert.IsFalse(ShortOctantList.TryUnappend(ref list1, out _));

            // Test push/pop
            ShortOctantList list2 = ShortOctantList.Empty;
            Assert.IsTrue(ShortOctantList.TryAppend(ref list1, Octant.PosX_NegY_PosZ));
            Assert.IsTrue(ShortOctantList.TryAppend(ref list1, Octant.NegX_NegY_PosZ));
            Assert.IsTrue(ShortOctantList.TryAppend(ref list1, Octant.PosX_PosY_PosZ));
            Assert.AreEqual(Octant.PosX_NegY_PosZ, list1[0]);
            Assert.AreEqual(Octant.NegX_NegY_PosZ, list1[1]);
            Assert.AreEqual(Octant.PosX_PosY_PosZ, list1[2]);
            Assert.IsTrue(ShortOctantList.TryUnappend(ref list1, out octant));
            Assert.AreEqual(Octant.PosX_PosY_PosZ, octant);
            Assert.IsTrue(ShortOctantList.TryUnappend(ref list1, out octant));
            Assert.AreEqual(Octant.NegX_NegY_PosZ, octant);
            Assert.IsTrue(ShortOctantList.TryUnappend(ref list1, out octant));
            Assert.AreEqual(Octant.PosX_NegY_PosZ, octant);
            Assert.IsFalse(ShortOctantList.TryUnappend(ref list1, out _));

            // Test pick/enumerator
            uint scale = 5;
            ShortVector3i offset = new ShortVector3i(13, 17, 2);
            ShortOctantList list3 = ShortOctantList.Pick(scale, offset);
            list3.Locate(out uint testScale, out ShortVector3i testOffset);
            Assert.AreEqual(scale, testScale);
            Assert.AreEqual(offset, testOffset);
            Vector3i vec = new Vector3i(0, 0, 0);
            foreach (Octant oct in list3)
            {
                scale--;
                vec += oct.IfPos(1 << (int)scale);
            }
            Assert.AreEqual(offset, vec);

            // Test append/reverse
            ShortOctantList list4 = ShortOctantList.Empty;
            Assert.IsTrue(ShortOctantList.TryAppend(ref list4, Octant.PosX_NegY_PosZ));
            Assert.IsTrue(ShortOctantList.TryAppend(ref list4, Octant.NegX_NegY_PosZ));
            Assert.IsTrue(ShortOctantList.TryAppend(ref list4, Octant.PosX_PosY_PosZ));
            Assert.IsTrue(ShortOctantList.TryConcat(ref list4, list4));
            list4 = list4.Reverse;
            Assert.IsTrue(ShortOctantList.TryUnappend(ref list4, out octant));
            Assert.AreEqual(Octant.PosX_NegY_PosZ, octant);
            Assert.IsTrue(ShortOctantList.TryUnappend(ref list4, out octant));
            Assert.AreEqual(Octant.NegX_NegY_PosZ, octant);
            Assert.IsTrue(ShortOctantList.TryUnappend(ref list4, out octant));
            Assert.AreEqual(Octant.PosX_PosY_PosZ, octant);
            Assert.IsTrue(ShortOctantList.TryUnappend(ref list4, out octant));
            Assert.AreEqual(Octant.PosX_NegY_PosZ, octant);
            Assert.IsTrue(ShortOctantList.TryUnappend(ref list4, out octant));
            Assert.AreEqual(Octant.NegX_NegY_PosZ, octant);
            Assert.IsTrue(ShortOctantList.TryUnappend(ref list4, out octant));
            Assert.AreEqual(Octant.PosX_PosY_PosZ, octant);
        }
    }
}
