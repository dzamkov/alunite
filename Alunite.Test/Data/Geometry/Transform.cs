﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Alunite.Data.Probability;

namespace Alunite.Data.Geometry
{
    [TestClass]
    public class TransformTests
    {
        [TestMethod]
        public void TestMotion3()
        {
            Motion3 upZ = Motion3.Translate(0, 0, 1);
            Motion3 rotateY = Rotation3.Euler(new Vector3(0, 1, 0) * (Scalar.Pi / 2));
            Motion3 rotateZ = Rotation3.About(new Vector3(0, 0, 1), Scalar.Pi / 2);

            Assert.IsTrue(Vector3.AreLikelyEqual(new Vector3(0, 1, 0), rotateZ * new Vector3(1, 0, 0)));
            Assert.IsTrue(Vector3.AreLikelyEqual(new Vector3(-1, 0, 1), rotateZ * (upZ * new Vector3(0, 1, 0))));
            Assert.IsTrue(Vector3.AreLikelyEqual(new Vector3(0, 0, -1), rotateZ * (rotateY * new Vector3(1, 0, 0))));
            Assert.IsTrue(Vector3.AreLikelyEqual(new Vector3(0, 0, -1), (rotateZ * rotateY) * new Vector3(1, 0, 0)));
            Assert.IsTrue(Vector3.AreLikelyEqual(new Vector3(0, 1, 0), rotateY * (rotateZ * new Vector3(1, 0, 0))));
            Assert.IsTrue(Vector3.AreLikelyEqual(new Vector3(0, 1, 0), (rotateY * rotateZ) * new Vector3(1, 0, 0)));
            Assert.IsTrue(Vector3.AreLikelyEqual(new Vector3(1, 0, 0), (rotateZ * rotateY).Inverse * new Vector3(0, 0, -1)));
        }
        
        [TestMethod]
        public void TestSimilarity2()
        {
            Similarity2 upY = Similarity2.Translate(0, 1);
            Similarity2 cross = Rotation2.Cross;
            Similarity2 swap = Similarity2.Swap;
            Similarity2 expand = Similarity2.ScaleUniform(2);

            Assert.IsTrue(Vector2.AreLikelyEqual(new Vector2(1, 0), swap * upY * Vector2.Zero));
            Assert.IsTrue(Vector2.AreLikelyEqual(new Vector2(1, 1), cross * swap * cross * new Vector2(1, 1)));
            Assert.IsTrue(Vector2.AreLikelyEqual(new Vector2(1, 0), (swap * cross * expand).Inverse * new Vector2(2, 0)));
        }

        [TestMethod]
        public void TestRotationMatrix3()
        {
            for (int a = -1; a <= 1; a++)
            {
                for (int b = -1; b <= 1; b++)
                {
                    for (int c = -1; c <= 1; c++)
                    {
                        for (int d = -1; d <= 1; d++)
                        {
                            Quaternion quat = new Quaternion(a, b, c, d);
                            if (quat.SqrMagnitude > 0)
                            {
                                quat = Quaternion.Normalize(quat);
                                Rotation3 rot = new Rotation3(quat);
                                Matrix3x3 mat = rot;
                                Rotation3 nRot = (Rotation3)mat;
                                Matrix3x3 nMat = nRot;
                                Assert.IsTrue(Matrix3x3.AreEqualOdds(mat, nMat) > 1);
                            }
                        }
                    }
                }
            }
        }
    }
}