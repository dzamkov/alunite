﻿using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Alunite.Data.Probability;

namespace Alunite.Data
{
    [TestClass]
    public class ScalarTests
    {
        [TestMethod]
        public void TestAreEqualOdds()
        {
            Assert.IsTrue(Scalar.AreEqualOdds(100000.1, 100000) > 1);
            Assert.IsTrue(Scalar.AreEqualOdds(3.1, 3) < 1);
        }
    }
}
