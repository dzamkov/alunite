﻿using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Alunite.Data.Probability;

namespace Alunite.Data
{
    [TestClass]
    public class MatrixTests
    {
        [TestMethod]
        public void TestSvdGeneral()
        {
            Scalar totalSqrError = 0;
            SeedSampler sampler = new SeedSampler(Seed.Initial);
            for (uint i = 0; i < 100; i++)
            {
                Matrix3x3 mat = new Matrix3x3(
                    new Vector3(sampler.SampleUnit(), sampler.SampleUnit(), sampler.SampleUnit()),
                    new Vector3(sampler.SampleUnit(), sampler.SampleUnit(), sampler.SampleUnit()),
                    new Vector3(sampler.SampleUnit(), sampler.SampleUnit(), sampler.SampleUnit()));
                mat.Svd(out Matrix3x3 u, out Vector3 s, out Matrix3x3 v);
                _verifySvd(u, s, v);
                Matrix3x3 test = u * Matrix.Diagonal(s) * v.Transpose;
                Assert.IsTrue(Matrix3x3.AreLikelyEqual(mat, test));
                totalSqrError += (mat - test).SqrLength;
            }
            Assert.IsTrue(totalSqrError <= 1.69715249E-11);
        }

        [TestMethod]
        public void TestSvdSingular()
        {
            Matrix3x3 mat = Matrix3x3.Zero;
            mat.Y.Y = 0.001;
            mat.Svd(out Matrix3x3 u, out Vector3 s, out Matrix3x3 v);
            _verifySvd(u, s, v);
            Matrix3x3 test = u * Matrix.Diagonal(s) * v.Transpose;
            Assert.IsTrue(Matrix3x3.AreLikelyEqual(mat, test));
        }

        private static void _verifySvd(Matrix3x3 u, Vector3 s, Matrix3x3 v)
        {
            Assert.IsTrue(s.X >= 0);
            Assert.IsTrue(s.Y >= 0);
            Assert.IsTrue(s.Z >= 0);
            Assert.IsTrue(s.X >= s.Y);
            Assert.IsTrue(s.Y >= s.Z);
            Assert.IsTrue((u * u.Transpose).IsLikelyIdentity);
            Assert.IsTrue((v * v.Transpose).IsLikelyIdentity);
        }
    }
}
