﻿using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Alunite.Data
{
    [TestClass]
    public class BitwiseTests
    {
        [TestMethod]
        public void TestLog2()
        {
            for (int i = 0; i < 32; i++)
                Assert.AreEqual((uint)i, Bitwise.Log2(1u << i));
        }
    }
}
