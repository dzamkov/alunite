﻿using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Alunite.Data
{
    [TestClass]
    public class MapTests
    {
        [TestMethod]
        public void TestMap()
        {
            MapBuilder<string, uint> builder = new MapBuilder<string, uint>();
            builder.Add("Delaware", 1760);
            builder.Add("Maryland", 1788);
            builder.Add("Nebraska", 1867);
            builder.Add("South Dakota", 1889);
            Assert.IsTrue(builder.Add("Utah", 1896));
            Assert.IsFalse(builder.Add("Delaware", 1787));
            Assert.AreEqual<uint>(5, builder.Size);
            Assert.AreEqual<uint>(1788, builder["Maryland"]);
            Assert.IsFalse(builder.TryGet("Hawaii", out _));
            Map<string, uint> map = builder.Finish();
            Assert.AreEqual<uint>(5, builder.Size);
            Assert.AreEqual<uint>(1788, builder["Maryland"]);
            Assert.AreEqual<uint>(1889, builder["South Dakota"]);
            Assert.AreEqual<uint>(1787, builder["Delaware"]);
        }
    }
}