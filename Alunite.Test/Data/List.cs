﻿using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Alunite.Data
{
    [TestClass]
    public class ListTests
    {
        [TestMethod]
        public void TestSubseq()
        {
            Assert.AreEqual(
                List.Of('b', 'd', 'e', 'g'),
                List.Of('a', 'b', 'c', 'd', 'e', 'f', 'g')
                    .Subseq(IndexSet.Of(1, 3, 4, 6)));
        }
    }
}
