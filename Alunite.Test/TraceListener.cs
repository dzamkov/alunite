﻿using System;
using System.Diagnostics;

namespace Alunite.Test
{
    /// <summary>
    /// A <see cref="TraceListener"/> which throws an exception on failure.
    /// </summary>
    public class ExceptionThrowingTraceListener : TraceListener
    {
        public override void Write(string message)
        {
            Console.Write(message);
        }

        public override void WriteLine(string message)
        {
            Console.WriteLine(message);
        }

        public override void Fail(string message)
        {
            throw new Exception(message);
        }

        public override void Fail(string message, string detailMessage)
        {
            throw new Exception(message);
        }
    }
}
