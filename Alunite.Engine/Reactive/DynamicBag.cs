﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Threading;
using System.Diagnostics;

using Alunite.Data;

namespace Alunite.Reactive
{
    /// <summary>
    /// A time-varying multiset of values of type <see cref="T"/>.
    /// </summary>
    public abstract class DynamicBag<T>
    {
        /// <summary>
        /// Gets the contents of this bag in the context of the given <see cref="Observer"/>. Items will be
        /// returned in an arbitrary order.
        /// </summary>
        public Enumerable this[Observer obs]
        {
            get
            {
                return new Enumerable(this, obs);
            }
        }

        /// <summary>
        /// An <see cref="IEnumerable{T}"/> providing the contents of a <see cref="DynamicBag{T}"/> at a particular
        /// version, in an arbitrary order.
        /// </summary>
        public struct Enumerable : IEnumerable<T>
        {
            internal Enumerable(DynamicBag<T> bag, Observer obs)
            {
                Bag = bag;
                Observer = obs;
            }

            /// <summary>
            /// The <see cref="DynamicBag{T}"/> this enumerable is for.
            /// </summary>
            public DynamicBag<T> Bag { get; }

            /// <summary>
            /// The observer providing context for this enumerable.
            /// </summary>
            public Observer Observer { get; }

            /// <summary>
            /// Returns an enumerator that iterates through the bag.
            /// </summary>
            public IEnumerator<T> GetEnumerator()
            {
                return Bag.GetEnumerator(Observer);
            }
            
            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }

        /// <summary>
        /// Gets an enumerator for the items in this bag in the context of the given <see cref="Observer"/>. Items
        /// will be returned in an arbitrary order. The lifetime of the enumerator is limited to the lifetime of
        /// the observer.
        /// </summary>
        public virtual IEnumerator<T> GetEnumerator(Observer obs)
        {
            var items = new ConcurrentBag<T>();
            Iterate(item =>
            {
                if (item.IsActive(obs))
                    items.Add(item.Value);
            }).Clear(obs);
            return items.GetEnumerator();
        }

        /// <summary>
        /// Calls <paramref name="emit"/> for a subset of items in this bag, returning a <see cref="DynamicExcess"/> to
        /// represent the items for which this has not been called yet. <paramref name="emit"/> may be called at any
        /// time, in any order, and on any thread, but it may only be called at most once per item. The returned
        /// <see cref="DynamicExcess"/> can be used to control when new items are provided.
        /// </summary>
        /// <remarks>
        /// This provides an efficient means of tracking the items in a bag over time. The returned
        /// <see cref="DynamicItem{T}"/>s may be used to construct a custom data structure for organizing the items.
        /// </remarks>
        public abstract DynamicExcess Iterate(Action<DynamicItem<T>> emit);
    }

    /// <summary>
    /// Describes a potential item in <see cref="DynamicBag{T}"/>, factored into a non-reactive value component and a
    /// lifetime component.
    /// </summary>
    /// <remarks>
    /// This is useful for creating custom data structures for organizing the contents of a
    /// <see cref="DynamicBag{T}"/> as it allows accessing item values in a time-independent fashion.
    /// </remarks>
    public struct DynamicItem<T>
    {
        internal _Version _start;
        internal _Lifetime _lifetime;
        internal DynamicItem(_Version start, T value, _Lifetime lifetime)
        {
            _start = start;
            Value = value;
            _lifetime = lifetime;
        }

        /// <summary>
        /// The value of the item.
        /// </summary>
        public T Value { get; }

        /// <summary>
        /// If true, indicates that this item is not active in any observable version and may be ignored/removed.
        /// </summary>
        public bool IsDead => _lifetime.IsDead;

        /// <summary>
        /// Determines whether this is a real item under the context of the given observer, that is, whether
        /// the observation is made during the lifetime of the item.
        /// </summary>
        public bool IsActive(Observer obs)
        {
            return _lifetime.IsActive(_start, obs);
        }
    }

    /// <summary>
    /// Describes when a <see cref="DynamicItem{T}"/> is active.
    /// </summary>
    internal sealed class _Lifetime
    {
        /// <summary>
        /// Either a dependent <see cref="_Lifetime"/> or <see cref="_WeakDepend"/>, or an array of them.
        /// </summary>
        private object _depends;

        /// <summary>
        /// The first <see cref="_Version"/> where the associated item is no longer active. This may be refined,
        /// but only to an earlier version. If this is null, the item is active indefinitely.
        /// </summary>
        public _Version Cutoff;

        /// <summary>
        /// If true, indicates that the associated item is not active in any observable version.
        /// </summary>
        public bool IsDead => Cutoff != null && Cutoff.IsDead;

        /// <summary>
        /// Sets this lifetime to be inactive after the given cutoff.
        /// </summary>
        public void Remove(ref _Invalidator invalidator, _Version cutoff)
        {
            lock (this)
            {
                if (Cutoff == null || cutoff < Cutoff)
                {
                    Cutoff = cutoff;
                    if (_depends != null)
                    {
                        if (_depends is _WeakDepend)
                        {
                            invalidator.Invalidate(cutoff, (_WeakDepend)_depends);
                        }
                        else
                        {
                            foreach (var depend in (object[])_depends)
                            {
                                if (depend != null)
                                {
                                    invalidator.Invalidate(cutoff, (_WeakDepend)depend);
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Determines whether the associated item is active under the context of the given observer.
        /// </summary>
        /// <param name="start">The version where the associated item begins to be active</param>
        public bool IsActive(_Version start, Observer obs)
        {
            if (start <= obs._version)
            {
                lock (this)
                {
                    if (Cutoff == null || obs._version < Cutoff)
                    {
                        if (obs._observe(start, Cutoff))
                            _addDepend(obs._depend);
                        return true;
                    }
                    else
                    {
                        // After active period
                        if (obs._base < Cutoff)
                            obs._base = Cutoff;
                        return false;
                    }
                }
            }
            else
            {
                // Before active period
                if (obs._cutoff == null || start < obs._cutoff)
                    obs._cutoff = start;
                return false;
            }
        }

        /// <summary>
        /// Adds a dependent to this <see cref="_Lifetime"/>, to be notified when <see cref="Cutoff"/> is refined.
        /// </summary>
        private void _addDepend(_WeakDepend depend)
        {
            // TODO: Fallback to hashtable?
            if (_depends == null)
            {
                _depends = depend;
            }
            else if (_depends == depend)
            {
                return;
            }
            else if (_depends is object[])
            {
                // Add dependent to array
                var depends = (object[])_depends;
                for (int i = 0; i < depends.Length; i++)
                {
                    if (depends[i] == null)
                    {
                        depends[i] = depend;
                        return;
                    }
                    else if (depends[i] == depend)
                    {
                        return;
                    }
                }

                // Resize and add
                throw new NotImplementedException();
            }
            else
            {
                // Combine dependencies into array
                var depends = new object[4];
                depends[0] = _depends;
                depends[1] = depend;
                _depends = depends;
            }
        }
    }

    /// <summary>
    /// Represents a set of items that are not accounted for in a complementary data structure.
    /// </summary>
    public abstract class DynamicExcess
    {
        /// <summary>
        /// Ensures that there are no unaccounted active items in the complementary data structure under
        /// the context of the given observer.
        /// </summary>
        public abstract void Clear(Observer obs);
    }

    /// <summary>
    /// A <see cref="DynamicBag{T}"/> whose contents are explicitly introduced and removed within the context
    /// of a <see cref="Transactor"/>.
    /// </summary>
    public sealed class StateBag<T> : DynamicBag<T>, _IState
    {
        public StateBag(Transactor init)
        {
            _depends = _Depends.Create();
            _entries = new _Entry[8];
            _head = 0;
            _nextId = 0;
        }

        /// <summary>
        /// The set of <see cref="_IDepend"/>s to be informed of additional items being added to this bag.
        /// </summary>
        private _Depends _depends;

        /// <summary>
        /// A buffer of entries for this bag. New entries may be added to the end of the buffer, but existing entries
        /// may not be modified. Once the buffer gets full, it will be compacted and/or resized.
        /// </summary>
        private _Entry[] _entries;

        /// <summary>
        /// The index of the first unwritten entry in <see cref="_entries"/>.
        /// </summary>
        private uint _head;

        /// <summary>
        /// The identifier to be assigned to <see cref="_Entry.Id"/> for the next allocated entry.
        /// </summary>
        private uint _nextId;

        /// <summary>
        /// A storage location for an item within a <see cref="StateBag{T}"/>.
        /// </summary>
        private struct _Entry
        {
            /// <summary>
            /// A unique identifier for this entry. Entries within <see cref="_entries"/> will always be sorted in
            /// ascending order of <see cref="Id"/>. This is used by <see cref="Iterate(Action{DynamicItem{T}})"/> to
            /// track which items have been emitted.
            /// </summary>
            public uint Id;

            /// <summary>
            /// The version where the associated item begins to be active, or null if this entry does not correspond to
            /// an item.
            /// </summary>
            public _Version Start;

            /// <summary>
            /// The value of the associated item, if applicable.
            /// </summary>
            public T Value;

            /// <summary>
            /// Provides lifetime information about the associated item.
            /// </summary>
            public _Lifetime Lifetime;
        }

        /// <summary>
        /// Identifies an item in a <see cref="StateBag{T}"/>.
        /// </summary>
        public struct ItemRef : IEquatable<ItemRef>
        {
            internal ItemRef(_Lifetime lifetime)
            {
                _lifetime = lifetime;
            }

            /// <summary>
            /// The <see cref="_Lifetime"/> of the item referred to by this structure.
            /// </summary>
            internal _Lifetime _lifetime;

            /// <summary>
            /// The null <see cref="ItemRef"/>.
            /// </summary>
            public static ItemRef Null => new ItemRef(null);

            /// <summary>
            /// Indicates whether this is a null reference.
            /// </summary>
            public bool IsNull => _lifetime == null;

            public static bool operator ==(ItemRef a, ItemRef b)
            {
                return a._lifetime == b._lifetime;
            }

            public static bool operator !=(ItemRef a, ItemRef b)
            {
                return !(a == b);
            }

            public override bool Equals(object obj)
            {
                if (!(obj is ItemRef))
                    return false;
                return this == (ItemRef)obj;
            }

            bool IEquatable<ItemRef>.Equals(ItemRef other)
            {
                return this == other;
            }

            public override int GetHashCode()
            {
                return _lifetime.GetHashCode();
            }
        }

        /// <summary>
        /// Adds an item to this bag and returns an identifier which can be used to later remove it.
        /// </summary>
        public ItemRef Add(Transactor trans, T value)
        {
            // TODO: Compact without resizing to maintain ideal size
            _Lifetime lifetime = new _Lifetime();
            _Invalidator invalidator = trans._invalidator;
            Monitor.Enter(this);
            {
                // Resize/compact if needed
                if (_head >= _entries.Length)
                    _resizeCompact(ref _entries, ref _head, _entries.Length * 2);

                // Add item to entries
                ref _Entry entry = ref _entries[_head];
                entry.Id = _nextId++;
                entry.Value = value;
                entry.Lifetime = lifetime;
                entry.Start = trans._version;
                _head++;

                // Record modification
                trans._modify(this);

                // Invalidate
                _depends.Invalidate(ref invalidator, trans._version);
            }
            Monitor.Exit(this);
            trans._invalidator = invalidator;
            return new ItemRef(lifetime);
        }

        /// <summary>
        /// Resizes and compacts (removes dead items) the given entry buffer.
        /// </summary>
        private static void _resizeCompact(ref _Entry[] entries, ref uint head, int size)
        {
            _Entry[] nEntries = new _Entry[size];
            uint nHead = 0;
            for (int i = 0; i < head; i++)
            {
                ref _Entry source = ref entries[i];
                if (!source.Lifetime.IsDead)
                    nEntries[nHead++] = source;
            }
            entries = nEntries;
            head = nHead;
        }

        /// <summary>
        /// Removes an item from this bag, assuming it is currently a member in the bag.
        /// </summary>
        public void Remove(Transactor trans, ItemRef item)
        {
            var invalidator = trans._invalidator;
            item._lifetime.Remove(ref invalidator, trans._version);
            trans._invalidator = invalidator;
        }

        /// <summary>
        /// Gets the contents of this bag in the context of the given <see cref="Observer"/>. Items will be
        /// returned in an arbitrary order.
        /// </summary>
        public new Enumerable this[Observer obs]
        {
            get
            {
                return new Enumerable(this, obs);
            }
        }

        /// <summary>
        /// An <see cref="IEnumerator{T}"/> for the contents of a <see cref="StateBag{T}"/>.
        /// </summary>
        public struct Enumerator : IEnumerator<T>
        {
            private Observer _obs;
            private _Entry[] _entries;
            private uint _index;
            private uint _end;
            internal Enumerator(StateBag<T> bag, Observer obs)
            {
                _obs = obs;
                lock (bag)
                {
                    // Add a dependency to track when new entries are added
                    bag._depends.Add(obs);
                    
                    // Grab a reference to the current entries array to read through
                    _entries = bag._entries;
                    _end = bag._head;
                }
                _index = uint.MaxValue;
            }

            /// <summary>
            /// Gets the element in the bag at the current position of the enumerator.
            /// </summary>
            public T Current => _entries[_index].Value;

            /// <summary>
            /// Advances the enumerator to the next/first item in the bag or returns false if the end of the bag has
            /// been reached.
            /// </summary>
            public bool MoveNext()
            {
            next:
                unchecked { _index++; }
                if (_index < _end)
                {
                    ref _Entry entry = ref _entries[_index];
                    if (entry.Lifetime.IsActive(entry.Start, _obs))
                        return true;
                    goto next;
                }
                else
                {
                    return false;
                }
            }

            /// <summary>
            /// Indicates that this enumerator will no longer be used.
            /// </summary>
            public void Dispose()
            {
                // Nothing to do here
            }

            void System.Collections.IEnumerator.Reset()
            {
                _index = uint.MaxValue;
            }
            
            object System.Collections.IEnumerator.Current => Current;
        }

        /// <summary>
        /// An <see cref="IEnumerable{T}"/> providing the contents of a <see cref="StateBag{T}"/> at a particular
        /// version, in an arbitrary order.
        /// </summary>
        public new struct Enumerable : IEnumerable<T>
        {
            internal Enumerable(StateBag<T> bag, Observer obs)
            {
                Bag = bag;
                Observer = obs;
            }

            /// <summary>
            /// The <see cref="StateBag{T}"/> this enumerable is for.
            /// </summary>
            public StateBag<T> Bag { get; }

            /// <summary>
            /// The observer providing context for this enumerable.
            /// </summary>
            public Observer Observer { get; }

            /// <summary>
            /// Returns an enumerator that iterates through the bag.
            /// </summary>
            public Enumerator GetEnumerator()
            {
                return new Enumerator(Bag, Observer);
            }
            
            IEnumerator<T> IEnumerable<T>.GetEnumerator()
            {
                return GetEnumerator();
            }

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }
        
        public override IEnumerator<T> GetEnumerator(Observer obs)
        {
            return new Enumerator(this, obs);
        }

        public override DynamicExcess Iterate(Action<DynamicItem<T>> emit)
        {
            return new _Excess(this, emit);
        }

        /// <summary>
        /// A <see cref="DynamicExcess"/> for the iterator of the contents of a <see cref="StateBag{T}"/>.
        /// </summary>
        private sealed class _Excess : DynamicExcess
        {
            public _Excess(StateBag<T> bag, Action<DynamicItem<T>> emit)
            {
                _bag = bag;
                _emit = emit;
            }

            /// <summary>
            /// The <see cref="StateBag{T}"/> this is for.
            /// </summary>
            private StateBag<T> _bag { get; }

            /// <summary>
            /// The emit function for the corresponding iterator.
            /// </summary>
            private Action<DynamicItem<T>> _emit { get; }

            /// <summary>
            /// The <see cref="_Entry.Id"/> for the next entry to be emitted for the corresponding iterator.
            /// </summary>
            private uint _id;

            public override void Clear(Observer obs)
            {
                _Entry[] entries;
                uint head;
                uint nId;
                lock (_bag)
                {
                    // Add dependence
                    _bag._depends.Add(obs);

                    // Check if there are any new items to iterate over
                    if (_id >= _bag._nextId)
                    {
                        Debug.Assert(_id == _bag._nextId);
                        return;
                    }

                    // Grab a reference to the current entries array to read through
                    entries = _bag._entries;
                    head = _bag._head;
                    nId = _bag._nextId;
                }

                // Find the index of the first entry that has not yet been emitted
                // TODO: Seed binary search based on _id and _nextId
                uint l = 0;
                uint h = head - 1;
                while (l < h)
                {
                    uint m = (l + h) / 2;
                    if (_id <= entries[m].Id)
                        h = m;
                    else
                        l = m + 1;
                }

                // Emit additional items
                while (l < head)
                {
                    ref _Entry entry = ref entries[l];
                    _emit(new DynamicItem<T>(entry.Start, entry.Value, entry.Lifetime));
                    l++;
                }

                // Update _id so we don't admit those items again
                _id = nId;
            }
        }

        void _IState.Rollback(ref _Invalidator invalidator, _Version version)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// A <see cref="DynamicExcess"/> which represents some subset of the items in a source
    /// <see cref="DynamicExcess"/>.
    /// </summary>
    public sealed class SubsetDynamicExcess : DynamicExcess, _IDepend
    {
        private readonly _WeakDepend _weakThis;
        public SubsetDynamicExcess(DynamicExcess source)
        {
            _weakThis = new _WeakDepend(this);
            Source = source;
        }

        /// <summary>
        /// The <see cref="DynamicExcess"/> from which this <see cref="SubsetDynamicExcess"/> derives its items.
        /// </summary>
        public DynamicExcess Source { get; }

        /// <summary>
        /// Indicates that this <see cref="SubsetDynamicExcess"/> used to contain the given item from <see cref="Source"/>,
        /// but doesn't anymore.
        /// </summary>
        public void Remove<T>(DynamicItem<T> item)
        {
            throw new NotImplementedException();
        }

        public override void Clear(Observer obs)
        {
            throw new NotImplementedException();
        }
        
        _WeakDepend _IDepend.WeakThis => _weakThis;

        void _IDepend.Invalidate(ref _Invalidator invalidator, _Version start)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// A helper class for constructing a <see cref="DynamicBag{T}"/> through the incremental addition of
    /// <see cref="DynamicItem{T}"/>s. Note that unlike <see cref="StateBag{T}"/>, additions are not tracked
    /// reactively; they are not done under the context of a <see cref="Transactor"/> and will be visible in
    /// all versions of the reactive system immediately.
    /// </summary>
    public struct DynamicBagBuilder<T> : IBuilder<DynamicBag<T>>
    {
        private DynamicItem<T>[] _items;
        private uint _next;

        /// <summary>
        /// An initially-empty <see cref="DynamicBagBuilder{T}"/>.
        /// </summary>
        public static DynamicBagBuilder<T> Empty => new DynamicBagBuilder<T>
        {
            _items = Array.Empty<DynamicItem<T>>(),
            _next = 0
        };

        /// <summary>
        /// Adds an item to the resulting <see cref="DynamicBag{T}"/>.
        /// </summary>
        public void Add(DynamicItem<T> item)
        {
            // Try to find an empty spot
            if (_items.Length > 0)
            {
                uint start = _next;
                while (_items[_next]._start != null && !_items[_next].IsDead)
                {
                    _next = _items.Next(_next);
                    if (_next == start)
                    {
                        // TODO: Resize
                        throw new NotImplementedException();
                    }
                }
                _items[_next] = item;
                _next = _items.Next(_next);
            }
            else
            {
                _items = new DynamicItem<T>[4];
                _items[0] = item;
            }
        }

        /// <summary>
        /// Gets the contents of the resulting bag under the context of the given <see cref="Observer"/>.
        /// </summary>
        public VersionEnumerable this[Observer obs] => new VersionEnumerable(_items, obs);

        /// <summary>
        /// An <see cref="IEnumerable{T}"/> for the contents of a <see cref="DynamicBagBuilder{T}"/>, filtered
        /// to a specific version.
        /// </summary>
        public struct VersionEnumerable : IEnumerable<T>
        {
            private DynamicItem<T>[] _items;
            private Observer _obs;
            public VersionEnumerable(DynamicItem<T>[] items, Observer obs)
            {
                _items = items;
                _obs = obs;
            }

            IEnumerator<T> IEnumerable<T>.GetEnumerator()
            {
                return new VersionEnumerator(_items, _obs);
            }

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return new VersionEnumerator(_items, _obs);
            }
        }

        /// <summary>
        /// Gets an enumerator for the contents of the resulting bag under the context of the given <see cref="Observer"/>.
        /// </summary>
        public VersionEnumerator GetEnumerator(Observer obs)
        {
            return new VersionEnumerator(_items, obs);
        }

        /// <summary>
        /// An <see cref="IEnumerator{T}"/> for the contents of a <see cref="DynamicBagBuilder{T}"/>, filtered
        /// to a specific version.
        /// </summary>
        public struct VersionEnumerator : IEnumerator<T>
        {
            private Observer _obs;
            private DynamicItem<T>[] _items;
            private uint _index;
            internal VersionEnumerator(DynamicItem<T>[] items, Observer obs)
            {
                _obs = obs;
                _items = items;
                _index = uint.MaxValue;
            }

            /// <summary>
            /// Gets the element in the bag at the current position of the enumerator.
            /// </summary>
            public T Current => _items[_index].Value;

            /// <summary>
            /// Advances the enumerator to the next/first item in the bag or returns false if the end of the bag has
            /// been reached.
            /// </summary>
            public bool MoveNext()
            {
            next:
                unchecked { _index++; }
                if (_index < _items.Length)
                {
                    ref DynamicItem<T> item = ref _items[_index];
                    if (item._start != null && item.IsActive(_obs))
                        return true;
                    goto next;
                }
                else
                {
                    return false;
                }
            }

            /// <summary>
            /// Indicates that this enumerator will no longer be used.
            /// </summary>
            public void Dispose()
            {
                // Nothing to do here
            }

            void System.Collections.IEnumerator.Reset()
            {
                _index = uint.MaxValue;
            }

            object System.Collections.IEnumerator.Current => Current;
        }

        /// <summary>
        /// Gets the <see cref="DynamicBag{T}"/> that was constructed using this <see cref="DynamicBagBuilder{T}"/>.
        /// </summary>
        public DynamicBag<T> Finish()
        {
            throw new NotImplementedException();
        }
    }
}
