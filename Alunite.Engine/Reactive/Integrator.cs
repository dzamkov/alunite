﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;

using Alunite.Data;

namespace Alunite.Reactive
{
    /// <summary>
    /// In short, this class brings a reactive system to life. It determines the progression of the system from
    /// an initial configuration, allows the progression be observered at various points in time, and allows for
    /// external changes to be introduced to influence the progression.
    /// </summary>
    public sealed class Integrator
    {
        public Integrator(Time @base)
        {
            _base = new _Version(@base);
            _update = _nullUpdate;
            _pending = new PriorityQueue<_Task>(_Task.Comparer, 0);
        }

        /// <summary>
        /// The main <see cref="_Update"/> procedure for this integrator.
        /// </summary>
        private _Update _update;

        /// <summary>
        /// The earliest version for which the integrator has a complete record of the reactive system state. This
        /// correspond to <see cref="Base"/>.
        /// </summary>
        internal _Version _base;
        
        /// <summary>
        /// A queue of the pending tasks that need to be performed by this integrator, in order of
        /// <see cref="_Task.Start"/>.
        /// </summary>
        private PriorityQueue<_Task> _pending;

        /// <summary>
        /// An <see cref="_Update"/> which does nothing.
        /// </summary>
        private static _Update _nullUpdate = new _Update(trans => { });

        /// <summary>
        /// Queues the initial update task for this integrator.
        /// </summary>
        private void _initialize()
        {
            // Add initial update
            _UpdateVersion initial = new _UpdateVersion(_base.Time, _update);
            _update._pending = initial;
            _base._next = initial;
            _pending = new PriorityQueue<_Task>(_Task.Comparer, 128);
            _pending.Push(new _Task(initial));
        }

        /// <summary>
        /// Describes a modification that must be performed on the underlying reactive system in order to make it valid
        /// at or after a given version.
        /// </summary>
        internal struct _Task : IComparable<_Task>
        {
            public _Task(_UpdateVersion version)
            {
                Start = version;
            }

            /// <summary>
            /// The earliest version where the effects of this task are relevant.
            /// </summary>
            public _Version Start;

            /// <summary>
            /// Processes this task for the given integrator.
            /// </summary>
            public void Process(Integrator integrator)
            {
                ((_UpdateVersion)Start).Process(integrator);
            }

            /// <summary>
            /// The <see cref="IComparer{T}"/> for <see cref="_Task"/>s.
            /// </summary>
            public static IComparer<_Task> Comparer = Comparer<_Task>.Default;

            int IComparable<_Task>.CompareTo(_Task other)
            {
                return _Version.Compare(Start, other.Start);
            }
        }

        /// <summary>
        /// Queues a task to be processed by this <see cref="Integrator"/>.
        /// </summary>
        internal void _queue(_Task task)
        {
            lock (this)
            {
                _pending.Push(task);
            }
        }
        
        /// <summary>
        /// The earliest time for which the integrator has a complete record of the reactive system state, and
        /// thus the earliest time that observations and modifications can be made. This can only ever be increased,
        /// which should be done to allow memory for old states to be cleaned up when they are no longer relevant.
        /// </summary>
        public Time Base
        {
            get
            {
                return _base.Time;
            }
            set
            {
                Debug.Assert(_base.Time <= value);
                var timepoint = _getTimepoint(value);

                lock (this)
                {
                    _resolveTo(timepoint);

                    // Close versions up to timepoint
                    while (_base != timepoint)
                    {
                        var next = Volatile.Read(ref _base._next);
                        if (Interlocked.CompareExchange(ref _base._next, null, next) == next)
                        {
                            _base.Kill();
                            Volatile.Write(ref _base, next);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The update function for the underlying reactive system. This determines how the system evolves in absence of
        /// external influence. It is called every <see cref="Time"/> tick.
        /// </summary>
        public Action<Transactor> Update
        {
            get
            {
                return _update.Apply;
            }
            set
            {
                _update = new _Update(value);
                _initialize();
            }
        }

        /// <summary>
        /// Creates an <see cref="Observer"/> which allows observation of the underlying reactive system at the given time
        /// The returned <see cref="IDisposable"/> should be disposed when observation is done. Until that point, no
        /// other calls to the <see cref="Integrator"/> are allowed.
        /// </summary>
        /// <param name="time">The clock time to make the observation at. This may not be before <see cref="Base"/></param>
        public IDisposable Observe(Time time, out Observer obs)
        {
            var timepoint = _getTimepoint(time);
            lock (this)
            {
                _resolveTo(timepoint);
            }
            obs = new Observer(this, timepoint, null);
            return null;
        }

        /// <summary>
        /// Creates a <see cref="Transactor"/> to modify the underlying reactive system at <see cref="Base"/>. The
        /// returned <see cref="IDisposable"/> should be disposed after all changes have been made. Until that point, no other
        /// calls to the <see cref="Integrator"/> are allowed.
        /// </summary>
        public IDisposable Transact(out Transactor trans)
        {
            trans = new Transactor(this, _base, null);
            return null;
        }

        /// <summary>
        /// Gets the earliest version that occurs at the given time.
        /// </summary>
        internal _Version _getTimepoint(Time time)
        {
            // TODO: Skip lists for faster version search
            _Version prev = Volatile.Read(ref _base);
            Debug.Assert(prev.IsTimepoint);

            // Check if we already have a timepoint
            if (prev.Time == time)
                return prev;

        retry:
            _Version next = Volatile.Read(ref prev._next);
            if (next != null)
            {
                if (next.Time < time)
                {
                    prev = next;
                    goto retry;
                }

                // Check if we already have a timepoint
                if (next.Time == time && next.IsTimepoint)
                    return next;
            }

            // Create a new timepoint
            _Version timepoint = new _Version(time);
            timepoint._next = next;
            if (Interlocked.CompareExchange(ref prev._next, timepoint, next) != next)
                goto retry;
            return timepoint;
        }

        /// <summary>
        /// Gets the next <see cref="_UpdateVersion"/> for the given update following the given version.
        /// This will return null if the next opportunity for update is at or after <see cref="_Update._cutoff"/>.
        /// </summary>
        internal _UpdateVersion _getUpdate(_Update update, _Version after)
        {
            // Determine update time
            Time updateTime = after.Time;
            _UpdateVersion updateAfter = after as _UpdateVersion;
            if (updateAfter != null && !(updateAfter.Update < update))
                updateTime = Time.Next(updateTime);

            // Search for immediately-previous version
        retry:
            _UpdateVersion updateNext;
            _Version next = Volatile.Read(ref after._next);
            if (next != null)
            {
                if (next.Time < updateTime)
                {
                    after = next;
                    goto retry;
                }
                else if (next.Time == updateTime)
                {
                    updateNext = next as _UpdateVersion;
                    if (updateNext == null)
                    {
                        after = next;
                        goto retry;
                    }
                    else if (updateNext.Update <= update)
                    {
                        if (updateNext.Update == update)
                            return updateNext; // Update already exists
                        after = next;
                        goto retry;
                    }
                }
            }

            // Create and insert new update version
            updateNext = new _UpdateVersion(updateTime, update);
            updateNext._next = next;
            if (Interlocked.CompareExchange(ref after._next, updateNext, next) != next)
                goto retry;

            // Queue update task and return
            return updateNext;
        }
        
        /// <summary>
        /// Ensures all versions up to and including the given version have been resolved. This assumes the integrator
        /// is locked.
        /// </summary>
        private void _resolveTo(_Version version)
        {
            // TODO: Multithreaded processing
            while (_pending.Count > 0 && _pending.Top.Start <= version)
                _pending.Pop().Process(this);
        }
    }

    /// <summary>
    /// An object in a reactive system that is able to evolve and process external stimuli through internally-generated
    /// update events.
    /// </summary>
    public interface IActor
    {
        /// <summary>
        /// Updates the actor for a tick in the reactive system.
        /// </summary>
        void Update(Transactor trans);
    }
}
