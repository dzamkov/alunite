﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;

using Alunite.Data;

namespace Alunite.Reactive
{
    /// <summary>
    /// Contains functions for constructing <see cref="Dynamic{T}"/> values.
    /// </summary>
    public static class Dynamic
    {
        /// <summary>
        /// Constructs a <see cref="Dynamic{T}"/> with a constant value.
        /// </summary>
        public static Dynamic<T> Const<T>(T value)
        {
            return new ConstDynamic<T>(value);
        }

        /// <summary>
        /// A <see cref="Dynamic{T}"/> for the current time.
        /// </summary>
        public static Dynamic<Time> Time => TimeDynamic.Instance;

        /// <summary>
        /// Applies a function to the inner values of a <see cref="Dynamic{T}"/>.
        /// </summary>
        public static Dynamic<TResult> Lift<T, TResult>(
            Dynamic<T> source, Func<T, TResult> func)
        {
            if (source.TryGetConst(out T value))
                return func(value);
            return Compute(obs => func(source.Get(obs)));
        }

        /// <summary>
        /// Applies a function to the inner values of two <see cref="Dynamic{T}"/>s.
        /// </summary>
        public static Dynamic<TResult> Lift<T1, T2, TResult>(
            Dynamic<T1> source1, Dynamic<T2> source2, Func<T1, T2, TResult> func)
        {
            if (source1.TryGetConst(out T1 value1) && source2.TryGetConst(out T2 value2))
                return func(value1, value2);
            return Compute(obs => func(source1.Get(obs), source2.Get(obs)));
        }

        /// <summary>
        /// Constructs a <see cref="Dynamic{T}"/> which gets its value from an arbitrary computation
        /// in the reactive system.
        /// </summary>
        public static Dynamic<T> Compute<T>(DynamicFunc<T> func)
        {
            return new ComputeDynamic<T>(func);
        }

        /// <summary>
        /// Produces a continuously-variable value from a discretely-varying span.
        /// </summary>
        public static Dynamic<T> Animate<TSpan, T>(this Dynamic<TSpan> span)
            where TSpan : ISpan<T>
        {
            return Compute(obs => span[obs][obs.Time]);
        }
    }

    /// <summary>
    /// A time-varying value of type <see cref="T"/>.
    /// </summary>
    public abstract class Dynamic<T>
    {
        /// <summary>
        /// Gets the value of this <see cref="Dynamic{T}"/> at the given moment.
        /// </summary>
        public T this[Observer obs] => Get(obs);

        /// <summary>
        /// Gets the value of this <see cref="Dynamic{T}"/> in the context of the given <see cref="Observer"/>.
        /// </summary>
        public abstract T Get(Observer obs);

        /// <summary>
        /// Tries getting the value of this <see cref="Dynamic{T}"/>, if it is constant.
        /// </summary>
        public bool TryGetConst(out T value)
        {
            ConstDynamic<T> constDynamic = this as ConstDynamic<T>;
            if (constDynamic != null)
            {
                value = constDynamic.Value;
                return true;
            }
            value = default(T);
            return false;
        }

        /// <summary>
        /// Applies a mapping function to this dynamic value.
        /// </summary>
        public Dynamic<TResult> Map<TResult>(Func<T, TResult> func)
        {
            return Dynamic.Lift(this, func);
        }
        
        /// <summary>
        /// Casts the inner type of this dynamic value.
        /// </summary>
        public Dynamic<TResult> Cast<TResult>()
        {
            // TODO: This should probably be a new type of Dynamic
            var res = this as Dynamic<TResult>;
            if (res != null)
                return res;
            return Map(value => (TResult)(object)value);
        }

        public static implicit operator Dynamic<T>(T value)
        {
            return Dynamic.Const(value);
        }

        public static implicit operator Dynamic<T>(DynamicFunc<T> func)
        {
            return Dynamic.Compute(func);
        }
    }

    /// <summary>
    /// A <see cref="Dynamic{T}"/> whose value never changes.
    /// </summary>
    public sealed class ConstDynamic<T> : Dynamic<T>
    {
        public ConstDynamic(T value)
        {
            Value = value;
        }

        /// <summary>
        /// The value for this <see cref="Dynamic{T}"/>.
        /// </summary>
        public readonly T Value;

        public override T Get(Observer obs)
        {
            return Value;
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }

    /// <summary>
    /// A dynamic value which is maintained as state information and updated explicitly within the
    /// context of a <see cref="Transactor"/>.
    /// </summary>
    public sealed class State<T> : Dynamic<T>, _IState
    {
        public State(Transactor init, T initial)
        {
            _depends = _Depends.Create();
            _entries = new _Entry[4];
            _entries[0] = new _Entry(init._version, initial);
            _head = 0;
        }

        /// <summary>
        /// Tracks the reactive objects that depend on this value.
        /// </summary>
        private _Depends _depends;

        /// <summary>
        /// A ring buffer of <see cref="_Entry"/>s providing the value for this <see cref="State{T}"/> from
        /// <see cref="Integrator._base"/> to the latest value.
        /// </summary>
        private _Entry[] _entries;

        /// <summary>
        /// The index of the entry in <see cref="_entries"/> that holds the latest value for this
        /// <see cref="State{T}"/>.
        /// </summary>
        private uint _head;

        /// <summary>
        /// Describes a span of <see cref="Version"/>s where a <see cref="State{T}"/> has a particular value.
        /// </summary>
        private struct _Entry
        {
            public _Entry(_Version start, T value)
            {
                Start = start;
                Value = value;
            }

            /// <summary>
            /// The version where this entry begins to be applicable.
            /// </summary>
            public _Version Start;

            /// <summary>
            /// The value of the <see cref="State{T}"/> when this entry is applicable.
            /// </summary>
            public T Value;
        }

        /// <summary>
        /// Gets or sets the value of this <see cref="State{T}"/> in the context of
        /// the given transaction.
        /// </summary>
        public T this[Transactor trans]
        {
            get
            {
                return Get(trans);
            }
            set
            {
                Update(trans, value);
            }
        }

        /// <summary>
        /// Sets the value of this <see cref="State{T}"/> in the context of the given
        /// transaction.
        /// </summary>
        public void Update(Transactor trans, T value)
        {
            _Invalidator invalidator = trans._invalidator;
            _Update transUpdate = trans._update;
            lock (this)
            {
                // Find entry to insert after
                uint index = _search(trans._version, out _Version end);

                // Invalidate entries occuring after target entry
                while (_head != index)
                {
                    ref _Entry entry = ref _entries[_head];

                    // Make sure the updates owning the entries are notified of the invalidation so they
                    // can queue up new updates to replace them (if needed).
                    _UpdateVersion updateVersion = entry.Start as _UpdateVersion;
                    if (updateVersion != null && updateVersion.Update != transUpdate)
                        invalidator.Invalidate(updateVersion, updateVersion.Update.WeakThis);
                    entry = default;
                    _head = _entries.Prev(_head);
                }

                // Check if we are completely replacing the current entry, or updating a portion of it
                {
                    ref _Entry entry = ref _entries[_head];
                    if (entry.Start == trans._version)
                    {
                        // Replace entry
                        entry.Value = value;
                    }
                    else
                    {
                        // Partially invalidate the current entry, since we are cutting off a portion of it
                        // to make a new entry
                        _UpdateVersion updateVersion = entry.Start as _UpdateVersion;
                        if (updateVersion != null && updateVersion.Update != transUpdate)
                            invalidator.Invalidate(trans._version, updateVersion.Update.WeakThis);

                        // Insert new entry after head
                        uint next = _entries.Next(_head);
                        if (_entries[next].Start != null &&
                            trans._integrator.Base <= _entries[next].Start.Time)
                        {
                            // Entry is still alive, we need to expand the buffer
                            _Entry[] nEntries = new _Entry[_entries.Length * 2];
                            _head = 0;
                            while (_head < _entries.Length)
                            {
                                nEntries[_head] = _entries[next];
                                next = _entries.Next(next);
                                _head++;
                            }
                            _entries = nEntries;
                        }
                        else
                        {
                            _head = next;
                        }
                        _entries[_head] = new _Entry(trans._version, value);
                    }
                }

                // Record modification
                trans._modify(this);
                
                // Invalidate
                _depends.Invalidate(ref invalidator, trans._version);
            }
            trans._invalidator = invalidator;
        }

        public override T Get(Observer obs)
        {
            lock (this)
            {
                uint index = _search(obs._version, out _Version end);
                var entry = _entries[index];
                if (obs._observe(entry.Start, end))
                    _depends.Add(obs);
                return entry.Value;
            }
        }

        /// <summary>
        /// Gets the index of the entry in <see cref="_entries"/> that is active for the given version.
        /// </summary>
        /// <param name="end">The start version of the entry immediately following the returned, or null if
        /// the returned entry is the latest</param>
        private uint _search(_Version version, out _Version end)
        {
            // TODO: Replace with binary search
            end = null;

            _Entry entry;
            uint index = _head;
            while (version < (entry = _entries[index]).Start)
            {
                end = entry.Start;
                index = _entries.Prev(index);
            }
            return index;
        }
        
        void _IState.Rollback(ref _Invalidator invalidator, _Version version)
        {
            lock (this)
            {
                // Find entry to insert after
                uint index = _search(version, out _Version end);

                // Invalidate entries occuring after target entry
                while (_head != index)
                {
                    ref _Entry entry = ref _entries[_head];

                    // Make sure the updates owning the entries are notified of the invalidation so they
                    // can queue up new updates to replace them (if needed).
                    _UpdateVersion updateVersion = entry.Start as _UpdateVersion;
                    if (updateVersion != null)
                        invalidator.Invalidate(updateVersion, updateVersion.Update.WeakThis);
                    entry = default;
                    _head = _entries.Prev(_head);
                }

                // Retract entry
                {
                    ref _Entry entry = ref _entries[_head];
                    Debug.Assert(entry.Start == version);
                    entry = default;
                    _head = _entries.Prev(_head);
                }
            }
        }
    }
    
    /// <summary>
    /// A stateless <see cref="Dynamic{T}"/> whose value is defined by a <see cref="DynamicFunc{T}"/> and is cached
    /// between versions.
    /// </summary>
    public sealed class ComputeDynamic<T> : Dynamic<T>, _IDepend
    {
        private readonly _WeakDepend _weakThis;
        private _DynamicCache<T> _cache;
        public ComputeDynamic(DynamicFunc<T> func)
        {
            _weakThis = new _WeakDepend(this);
            Func = func;
        }

        /// <summary>
        /// The defining function for this <see cref="Dynamic{T}"/>
        /// </summary>
        public DynamicFunc<T> Func { get; }

        _WeakDepend _IDepend.WeakThis => _weakThis;

        public override T Get(Observer obs)
        {
            lock (_weakThis)
            {
                if (_cache.TryUse(obs._version, out T value, out _Version start, out _Version end))
                {
                    if (obs._observe(start, end))
                        _weakThis.Depends.Add(obs);
                    return value;
                }
                else
                {
                    // Compute new value
                    Observer innerObs = new Observer(obs._integrator, obs._version, _weakThis);
                    value = Func(innerObs);

                    // Add to cache
                    _cache.TryAdd(false, innerObs._base, innerObs._cutoff, ref value);

                    // Use value
                    if (obs._observe(innerObs._base, innerObs._cutoff))
                        _weakThis.Depends.Add(obs);
                    return value;
                }
            }
        }

        void _IDepend.Invalidate(ref _Invalidator invalidator, _Version start)
        {
            _cache.Invalidate(false, start);
        }
    }

    /// <summary>
    /// A fixed-size cache of computed <see cref="Dynamic{T}"/> values of type <typeparamref name="T"/>.
    /// </summary>
    internal struct _DynamicCache<T>
    {
        // Since there is a tendency for the difficulty to compute a dynamic value to correlate with the frequency
        // that it changes, a fixed number of cache slots allows us to drastically reduce work required for
        // hard-to-compute values without causing a memory explosion for small simple values.
        private _Entry _cache_0;
        private _Entry _cache_1;
        private _Entry _cache_2;

        /// <summary>
        /// Describes a cached value for a <see cref="Dynamic{T}"/>.
        /// </summary>
        private struct _Entry
        {
            public _Entry(_Version start, _Version end, T value)
            {
                Start = start;
                End = end;
                Value = value;
            }

            /// <summary>
            /// The earliest version where this entry begins to be applicable.
            /// </summary>
            public _Version Start;

            /// <summary>
            /// The earliest version where this entry is no longer applicable.
            /// </summary>
            public _Version End;

            /// <summary>
            /// The value of the <see cref="Dynamic{T}"/> when this entry is applicable.
            /// </summary>
            public T Value;

            /// <summary>
            /// Indicates whether this is a valid entry.
            /// </summary>
            public bool IsValid => Start != null;
        }

        /// <summary>
        /// Tries getting the value of the associated <see cref="Dynamic{T}"/> using this cache, returning
        /// false if nothing in the cache is applicable to the given version.
        /// </summary>
        /// <param name="start">The earliest version where the returned value is applicable.</param>
        /// <param name="end">The version where the returned value is no longer applicable.</param>
        public bool TryUse(_Version version, out T value, out _Version start, out _Version end)
        {
            value = default;
            start = default;
            end = default;
            if (!_cache_0.IsValid)
                return false;
            if (_tryUse(in _cache_0, version, ref value, ref start, ref end))
                return true;
            if (!_cache_1.IsValid)
                return false;
            if (_tryUse(in _cache_1, version, ref value, ref start, ref end))
            {
                _promote_1();
                return true;
            }
            if (!_cache_2.IsValid)
                return false;
            if (_tryUse(in _cache_2, version, ref value, ref start, ref end))
            {
                _promote_2();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Tries getting the value of the associated <see cref="Dynamic{T}"/> using the given cache entry, returning
        /// false if it is not applicable to the given version.
        /// </summary>
        private static bool _tryUse(in _Entry entry, _Version version, ref T value, ref _Version start, ref _Version end)
        {
            if (entry.Start <= version &&
                (entry.End == null || version < entry.End))
            {
                value = entry.Value;
                start = entry.Start;
                end = entry.End;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Attempts to add a new value to this cache, returning false if a value with the same <paramref name="start"/>
        /// already exists. If this is the case, <paramref name="value"/> will be updated to the value of the existing
        /// cache entry.
        /// </summary>
        /// <param name="dispose">Indicates whether values that will no longer be returned by the cache
        /// should be disposed.</param>
        public bool TryAdd(bool dispose, _Version start, _Version end, ref T value)
        {
            if (start == _cache_0.Start)
            {
                value = _cache_0.Value;
                _cache_0.End = end;
                return false;
            }
            else if (start == _cache_1.Start)
            {
                value = _cache_1.Value;
                _cache_1.End = end;
                _promote_1();
                return false;
            }
            else if (start == _cache_2.Start)
            {
                value = _cache_2.Value;
                _cache_2.End = end;
                _promote_2();
                return false;
            }
            else
            {
                if (dispose && _cache_2.IsValid)
                    ((IDisposable)_cache_2.Value).Dispose();
                _cache_2 = _cache_1;
                _cache_1 = _cache_0;
                _cache_0 = new _Entry(start, end, value);
                return true;
            }
        }

        /// <summary>
        /// Promotes <see cref="_cache_1"/> to <see cref="_cache_0"/>.
        /// </summary>
        private void _promote_1()
        {
            _Entry temp = _cache_1;
            _cache_1 = _cache_0;
            _cache_0 = temp;
        }

        /// <summary>
        /// Promotes <see cref="_cache_2"/> to <see cref="_cache_0"/>.
        /// </summary>
        private void _promote_2()
        {
            _Entry temp = _cache_2;
            _cache_2 = _cache_1;
            _cache_1 = _cache_0;
            _cache_0 = temp;
        }

        /// <summary>
        /// Invalidates the information in this cache starting at the given version.
        /// </summary>
        /// <param name="dispose">Indicates whether values that will no longer be returned by the cache
        /// should be disposed.</param>
        public void Invalidate(bool dispose, _Version start)
        {
            if (_invalidate(dispose, ref _cache_0, start))
            {
                if (_invalidate(dispose, ref _cache_1, start))
                {
                    if (!_invalidate(dispose, ref _cache_2, start))
                        _cache_0 = _cache_2;
                }
                else
                {
                    _cache_0 = _cache_1;
                    if (!_invalidate(dispose, ref _cache_2, start))
                        _cache_1 = _cache_2;
                }
            }
            else if (_invalidate(dispose, ref _cache_1, start))
            {
                if (!_invalidate(dispose, ref _cache_2, start))
                    _cache_1 = _cache_2;
            }
            else
            {
                _invalidate(dispose, ref _cache_2, start);
            }
        }

        /// <summary>
        /// Attempts to invalidate the given cache entry at or after the given start time. Returns true if the
        /// entire entry has been invalidated.
        /// </summary>
        private static bool _invalidate(bool dispose, ref _Entry entry, _Version start)
        {
            if (entry.IsValid)
            {
                if (entry.End == null || start < entry.End)
                {
                    if (start <= entry.Start)
                    {
                        _clear(dispose, ref entry);
                        return true;
                    }
                    else
                    {
                        entry.End = start;
                        return false;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Clears all values stored in this cache that are only applicable prior to the given version.
        /// </summary>
        /// <param name="disposed">Indicates whether cleared values should be disposed.</param>
        public void Close(bool dispose, _Version cutoff)
        {
            if (_close(dispose, ref _cache_0, cutoff))
            {
                if (_close(dispose, ref _cache_1, cutoff))
                {
                    if (!_close(dispose, ref _cache_2, cutoff))
                        _cache_0 = _cache_2;
                }
                else
                {
                    _cache_0 = _cache_1;
                    if (!_close(dispose, ref _cache_2, cutoff))
                        _cache_1 = _cache_2;
                }
            }
            else if (_close(dispose, ref _cache_1, cutoff))
            {
                if (!_close(dispose, ref _cache_2, cutoff))
                    _cache_1 = _cache_2;
            }
            else
            {
                _close(dispose, ref _cache_2, cutoff);
            }
        }

        /// <summary>
        /// Clears the given entry if it is only applicable before the given version.
        /// </summary>
        private static bool _close(bool dispose, ref _Entry entry, _Version cutoff)
        {
            if (entry.IsValid)
            {
                if (entry.End != null && entry.End <= cutoff)
                {
                    _clear(dispose, ref entry);
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Clears all values stored in this cache.
        /// </summary>
        /// <param name="disposed">Indicates whether cleared values should be disposed.</param>
        public void Clear(bool dispose)
        {
            if (!_cache_0.IsValid) return;
            _clear(dispose, ref _cache_0);
            if (!_cache_1.IsValid) return;
            _clear(dispose, ref _cache_1);
            if (!_cache_2.IsValid) return;
            _clear(dispose, ref _cache_2);
        }

        /// <summary>
        /// Clears the given cache entry.
        /// </summary>
        /// <param name="disposed">Indicates whether cleared values should be disposed.</param>
        private static void _clear(bool dispose, ref _Entry entry)
        {
            entry.Start = null;
            if (dispose) ((IDisposable)entry.Value).Dispose();
            entry.Value = default;
        }
    }

    /// <summary>
    /// A <see cref="Dynamic{T}"/> representing the current time.
    /// </summary>
    public sealed class TimeDynamic : Dynamic<Time>
    {
        private TimeDynamic() { }

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        public static readonly TimeDynamic Instance = new TimeDynamic();

        public override Time Get(Observer obs)
        {
            return obs.Time;
        }
    }

    /// <summary>
    /// A time-varying value of type <typeparamref name="TResult"/>.
    /// </summary>
    public delegate TResult DynamicFunc<TResult>(Observer obs);

    /// <summary>
    /// A time-varying function from <typeparamref name="T"/> to <typeparamref name="TResult"/>.
    /// </summary>
    public delegate TResult DynamicFunc<T, TResult>(Observer obs, T arg);

    /// <summary>
    /// A time-varying function from <typeparamref name="T1"/> and <typeparamref name="T2"/> to <typeparamref name="TResult"/>.
    /// </summary>
    public delegate TResult DynamicFunc<T1, T2, TResult>(Observer obs, T1 arg1, T2 arg2);
}