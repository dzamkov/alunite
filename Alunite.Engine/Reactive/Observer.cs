﻿using System;
using System.Collections.Generic;
using System.Threading;

using Alunite.Data;

namespace Alunite.Reactive
{
    /// <summary>
    /// A context that allows dynamic values to be read at a particular moment in game time.
    /// </summary>
    public class Observer
    {
        internal Observer(Integrator integrator, _Version version, _WeakDepend depend)
        {
            _integrator = integrator;
            _version = version;
            _depend = depend;
            _base = Volatile.Read(ref integrator._base);
            _cutoff = null;
        }

        /// <summary>
        /// The <see cref="Integrator"/> for the reactive system being observed.
        /// </summary>
        internal Integrator _integrator { get; }

        /// <summary>
        /// Identifies the <see cref="_Version"/> that this observer is reading from.
        /// </summary>
        internal _Version _version { get; }
        
        /// <summary>
        /// The <see cref="_IDepend"/> representation of the observation being made by this observer. This is used to
        /// tag all entries accessed by the observer.
        /// </summary>
        internal _WeakDepend _depend { get; }

        /// <summary>
        /// The earliest version at which the observation resulting from this observer may be valid.
        /// </summary>
        internal _Version _base;

        /// <summary>
        /// The version at which the observation resulting from this observer is no longer valid, or null if the
        /// observation is valid indefinitely.
        /// </summary>
        internal _Version _cutoff;

        /// <summary>
        /// The true absolute time the observation is being made at. This should not be used directly, as it will
        /// not inform the <see cref="Observer"/> that the resulting observation is time-dependent. Instead,
        /// <see cref="Time"/> should be used.
        /// </summary>
        internal Time _time => _version.Time;

        /// <summary>
        /// The absolute time that the observation is being made at.
        /// </summary>
        public ObserverTime Time
        {
            get
            {
                return new ObserverTime(this);
            }
        }

        /// <summary>
        /// The earliest time at which the observation resulting from this observer may be valid. The setter
        /// is only able to increase this value.
        /// </summary>
        internal Time _earliest
        {
            get
            {
                return _base.Time;
            }
            set
            {
                if (_base.Time < value)
                    _base = _integrator._getTimepoint(value);
            }
        }

        /// <summary>
        /// The latest time at which the observation resulting from this observer may be valid. The setter
        /// is only able to decrease this value.
        /// </summary>
        internal Time _latest
        {
            get
            {
                return _cutoff?.Time ?? Reactive.Time.MaxValue;
            }
            set
            {
                if (value < Reactive.Time.MaxValue)
                {
                    if (_cutoff == null || value < _cutoff.Time)
                        _cutoff = _integrator._getTimepoint(Reactive.Time.Next(value));
                }
            }
        }

        /// <summary>
        /// Indicates that the observation resulting from this observer depends on a value that is active within the
        /// given interval. Returns true if this should be tracked as a dependence.
        /// </summary>
        internal bool _observe(_Version start, _Version end)
        {
            if (_depend == null)
                return false; // External observations are never tracked
            else if (this is Transactor && _version == start)
                return false; // Transactions can not depend on values that they themselves have written
            if (_base < start)
                _base = start;
            if (_cutoff == null || (end != null && end < _cutoff))
                _cutoff = end;
            return true;
        }
    }
    
    /// <summary>
    /// Describes when an <see cref="Observer"/> is making an observation in terms of a <see cref="Time"/>.
    /// </summary>
    public struct ObserverTime
    {
        public ObserverTime(Observer observer)
        {
            Observer = observer;
        }

        /// <summary>
        /// The observer this time is for.
        /// </summary>
        public Observer Observer;

        /// <summary>
        /// The value of this time in seconds.
        /// </summary>
        public Scalar Seconds
        {
            get
            {
                return ((Time)this).Seconds;
            }
        }

        /// <summary>
        /// The number of ticks for this time.
        /// </summary>
        public ulong Ticks
        {
            get
            {
                return ((Time)this).Ticks;
            }
        }

        public static implicit operator Time(ObserverTime source)
        {
            Time time = source.Observer._time;
            source.Observer._earliest = time;
            source.Observer._latest = time;
            return time;
        }

        public static ulong operator /(ObserverTime a, Time b)
        {
            ulong res = a.Observer._time / b;
            a.Observer._earliest = res * b;
            a.Observer._latest = Time.Previous((res + 1) * b);
            return res;
        }

        public static bool operator <(ObserverTime a, Time b)
        {
            Time time = a.Observer._time;
            if (time < b)
            {
                a.Observer._latest = Time.Previous(b);
                return true;
            }
            else
            {
                a.Observer._earliest = b;
                return false;
            }
        }

        public static bool operator >(ObserverTime a, Time b)
        {
            Time time = a.Observer._time;
            if (time > b)
            {
                a.Observer._earliest = Time.Next(b);
                return true;
            }
            else
            {
                a.Observer._latest = b;
                return false;
            }
        }

        public static bool operator <=(ObserverTime a, Time b)
        {
            return !(a > b);
        }

        public static bool operator >=(ObserverTime a, Time b)
        {
            return !(a < b);
        }

        public static bool operator <(Time a, ObserverTime b)
        {
            return b > a;
        }

        public static bool operator >(Time a, ObserverTime b)
        {
            return b < a;
        }

        public static bool operator <=(Time a, ObserverTime b)
        {
            return b >= a;
        }

        public static bool operator >=(Time a, ObserverTime b)
        {
            return b <= a;
        }

        public static bool operator ==(ObserverTime a, Time b)
        {
            Time time = a.Observer._time;
            if (time < b)
            {
                a.Observer._latest = Time.Previous(b);
                return false;
            }
            else if (time > b)
            {
                a.Observer._earliest = Time.Next(b);
                return false;
            }
            else // if (time == b)
            {
                a.Observer._earliest = time;
                a.Observer._latest = time;
                return true;
            }
        }

        public static bool operator !=(ObserverTime a, Time b)
        {
            return !(a == b);
        }

        public static bool operator ==(Time a, ObserverTime b)
        {
            return b == a;
        }

        public static bool operator !=(Time a, ObserverTime b)
        {
            return b != a;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is ObserverTime))
                return false;
            return Observer == ((ObserverTime)obj).Observer;
        }

        public override int GetHashCode()
        {
            return Observer.GetHashCode();
        }
    }
}