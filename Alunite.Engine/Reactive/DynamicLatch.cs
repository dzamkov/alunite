﻿using System;
using System.Collections.Generic;

namespace Alunite.Reactive
{
    /// <summary>
    /// A time-varying boolean value subject to the constraint that, once true ("closed"), it will never
    /// become false ("open").
    /// </summary>
    public abstract class DynamicLatch : Dynamic<bool>
    {
        /// <summary>
        /// Gets a <see cref="DynamicLatch"/> that is never closed.
        /// </summary>
        public static DynamicLatch Never => ConstLatch.Never;

        /// <summary>
        /// Gets a <see cref="DynamicLatch"/> that is always closed.
        /// </summary>
        public static DynamicLatch Always => ConstLatch.Always;

        /// <summary>
        /// Constructs a <see cref="DynamicLatch"/> that is triggered at the given time.
        /// </summary>
        public static DynamicLatch At(Time time)
        {
            return new TimeDynamicLatch(time);
        }
    }

    /// <summary>
    /// A <see cref="DynamicLatch"/> with a constant triggered state.
    /// </summary>
    public sealed class ConstLatch : DynamicLatch
    {
        private ConstLatch(bool isClosed)
        {
            IsClosed = isClosed;
        }
        
        /// <summary>
        /// The <see cref="ConstLatch"/> which is always open.
        /// </summary>
        public static new ConstLatch Never { get; } = new ConstLatch(false);

        /// <summary>
        /// The <see cref="ConstLatch"/> which is always closed.
        /// </summary>
        public static new ConstLatch Always { get; } = new ConstLatch(true);

        /// <summary>
        /// The state of this latch.
        /// </summary>
        public bool IsClosed { get; }

        public override bool Get(Observer obs)
        {
            return IsClosed;
        }
    }

    /// <summary>
    /// A <see cref="DynamicLatch"/> which can only be updated (i.e. triggered) within the context of a
    /// <see cref="Transactor"/>.
    /// </summary>
    public sealed class StateLatch : DynamicLatch, _IState
    {
        /// <summary>
        /// Sets the value of this latch to true under the context of the given <see cref="Transactor"/>. If
        /// the latch is already true, this will do nothing.
        /// </summary>
        public void Trigger(Transactor trans)
        {
            throw new NotImplementedException();
        }

        public override bool Get(Observer obs)
        {
            throw new NotImplementedException();
        }

        void _IState.Rollback(ref _Invalidator invalidator, _Version version)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// A <see cref="DynamicLatch"/> that is triggered at a particular <see cref="Reactive.Time"/>.
    /// </summary>
    public sealed class TimeDynamicLatch : DynamicLatch
    {
        public TimeDynamicLatch(Time time)
        {
            Time = time;
        }

        /// <summary>
        /// The time that this latch is triggered at.
        /// </summary>
        public Time Time { get; }

        public override bool Get(Observer obs)
        {
            return Time <= obs.Time;
        }
    }
}
