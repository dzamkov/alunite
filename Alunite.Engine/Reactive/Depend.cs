﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Diagnostics;

using Alunite.Data;

namespace Alunite.Reactive
{
    /// <summary>
    /// An interface to an object that contains one or more cached reactive values that can depend
    /// on other values, and thus are subject to invalidation.
    /// </summary>
    internal interface _IDepend
    {
        /// <summary>
        /// A weak reference to this dependent. The same reference will be returned every time the property
        /// is accessed.
        /// </summary>
        _WeakDepend WeakThis { get; }

        /// <summary>
        /// Invalidates this object using the given <see cref="_Invalidator"/>. This assumes that
        /// <see cref="WeakThis"/> is locked.
        /// </summary>
        /// <param name="start">The earliest <see cref="_Version"/> affected by invalidation</param>
        void Invalidate(ref _Invalidator invalidator, _Version start);
    }

    /// <summary>
    /// Combines a weak reference to an <see cref="_IDepend"/> with references to its dependents. This is useful for
    /// invalidation propogation, as it allows the underlying <see cref="_IDepend"/> to be collected while ensuring
    /// that invalidations can still propogate through to transitive dependents that aren't yet collected.
    /// </summary>
    internal sealed class _WeakDepend
    {
        private readonly GCHandle _weakHandle;
        public _WeakDepend(_IDepend depend)
        {
            _weakHandle = GCHandle.Alloc(depend, GCHandleType.Weak);
            HashCode = System.Runtime.CompilerServices.RuntimeHelpers.GetHashCode(depend);
            Depends = _Depends.Create();
        }

        ~_WeakDepend()
        {
            _weakHandle.Free();
        }

        /// <summary>
        /// The hash code for the underlying <see cref="_IDepend"/>, stored explicitly so that it is available without
        /// dereferencing a weak reference.
        /// </summary>
        public int HashCode { get; }
        
        /// <summary>
        /// Tracks the dependents of <see cref="Target"/>, even after it has been collected.
        /// </summary>
        public _Depends Depends;
        
        /// <summary>
        /// The <see cref="_IDepend"/> this is a reference to, or null if it has been collected.
        /// </summary>
        public _IDepend Target
        {
            get
            {
                return (_IDepend)_weakHandle.Target;
            }
        }

        /// <summary>
        /// If true, indicates that invalidations directed at this <see cref="_WeakDepend"/> don't have
        /// any effect.
        /// </summary>
        public bool IsDead
        {
            get
            {
                // TODO
                return false;
            }
        }

        /// <summary>
        /// Invalidates the underlying <see cref="_IDepend"/> and all transitive dependents using
        /// the given <see cref="_Invalidator"/>.
        /// </summary>
        /// <param name="start">The earliest <see cref="_Version"/> affected by invalidation</param>
        public void Invalidate(ref _Invalidator invalidator, _Version start)
        {
            // TODO: Cleanup
            lock (this)
            {
                Depends.Invalidate(ref invalidator, start);
                var depend = Target;
                if (depend != null)
                    depend.Invalidate(ref invalidator, start);
            }
        }
    }

    /// <summary>
    /// Tracks a set of <see cref="_IDepend"/>s that depend on a particular value or source. This is not thread-safe.
    /// </summary>
    internal struct _Depends
    {
        /// <summary>
        /// A hashtable of dependents in this set.
        /// </summary>
        private _Entry[] _entries;

        /// <summary>
        /// Describes a single <see cref="_IDepend"/> in the set of dependencies.
        /// </summary>
        private struct _Entry
        {
            /// <summary>
            /// A weak reference to the <see cref="_IDepend"/> owning the cached values.
            /// </summary>
            public _WeakDepend Depend;

            /// <summary>
            /// The earliest version where it is known that this dependence no longer exists, or null if
            /// the dependence is assumed to be indefinite.
            /// </summary>
            public _Version Cutoff;

            /// <summary>
            /// If true, indicates that this entry can't produce an effective invalidation.
            /// </summary>
            public bool IsDead
            {
                get
                {
                    return (Cutoff != null && Cutoff.IsDead) || Depend.IsDead;
                }
            }
        }

        /// <summary>
        /// An initially-empty set of dependencies with no reserved space.
        /// </summary>
        public static _Depends Empty => new _Depends
        {
            _entries = Array.Empty<_Entry>()
        };

        /// <summary>
        /// Creates a new initially-empty set of dependences with space reserved for a modest amount of
        /// entries.
        /// </summary>
        public static _Depends Create()
        {
            return new _Depends
            {
                _entries = new _Entry[4]
            };
        }

        /// <summary>
        /// Adds a dependence to this set.
        /// </summary>
        /// <param name="cutoff">The earliest version where it is known that this dependence no longer exists, or null if
        /// the dependence is assumed to be indefinite.</param>
        public void Add(Integrator integrator, _WeakDepend depend, _Version cutoff)
        {
            if (_entries.Length == 0)
                goto resize;
            
        insert:
            {
                int hash = depend.HashCode;
                uint bin = Hashtable.GetBinPow2(_entries.Length, hash);
                uint endBin = bin;
                uint addBin = uint.MaxValue;
            nextBin:
                // TODO: We may be able to compact the hashtable by relocating entries into previously-identified
                // garbage bins when this doesn't break the hashtable invariant.
                ref _Entry cur = ref _entries[bin];
                if (cur.Depend == null)
                {
                    if (addBin < _entries.Length)
                    {
                        // Add to previously found garbage bin
                        ref _Entry add = ref _entries[addBin];
                        add.Depend = depend;
                        add.Cutoff = cutoff;
                    }
                    else
                    {
                        // Add to end of chain
                        cur.Depend = depend;
                        cur.Cutoff = cutoff;
                    }
                    return;
                }
                else if (cur.Depend == depend)
                {
                    // Extend the cutoff to indicate that the dependence still exists
                    if (cur.Cutoff != null)
                    {
                        if (cutoff == null || cur.Cutoff < cutoff)
                            cur.Cutoff = cutoff;
                    }
                    return;
                }
                else if (addBin == uint.MaxValue && cur.IsDead)
                {
                    // Mark this as a garbage bin that we will replace if possible
                    addBin = bin;
                }

                // Try next bin
                Hashtable.Traverse(_entries.Length, ref bin, 1);
                if (bin == endBin)
                    goto resize;
                else
                    goto nextBin;
            }

        resize:
            {
                // Create new table
                int nSize = Math.Max(4, _entries.Length * 2);
                _Entry[] nEntries = new _Entry[nSize];

                // Copy current dependents
                for (int i = 0; i < _entries.Length; i++)
                {
                    var copy = _entries[i];
                    if (!copy.IsDead)
                    {
                        int hash = copy.Depend.GetHashCode();
                        uint bin = Hashtable.GetBinPow2(nEntries.Length, hash);
                        while (nEntries[bin].Depend != null)
                            Hashtable.Traverse(nEntries.Length, ref bin, 1);
                        nEntries[bin] = copy;
                    }
                }

                // Finish up with new table
                _entries = nEntries;
                goto insert;
            }
        }

        /// <summary>
        /// Adds the observation resulting from the given <see cref="Observer"/> as a dependent
        /// in this set.
        /// </summary>
        public void Add(Observer obs)
        {
            if (obs._depend != null)
                Add(obs._integrator, obs._depend, obs._cutoff);
        }

        /// <summary>
        /// Queues all applicable dependents in this set for invalidation with the given invalidator.
        /// </summary>
        /// <param name="start">The earliest <see cref="_Version"/> affected by invalidation</param>
        public void Invalidate(ref _Invalidator invalidator, _Version start)
        {
            for (int i = 0; i < _entries.Length; i++)
            {
                ref _Entry cur = ref _entries[i];
                if (cur.Depend != null &&
                    (cur.Cutoff == null || start < cur.Cutoff))
                {
                    invalidator.Invalidate(start, cur.Depend);
                    cur.Cutoff = start;
                }
            }
        }
    }

    /// <summary>
    /// Provides necessary context to invalidate one or more <see cref="_IDepend"/>s.
    /// </summary>
    internal struct _Invalidator
    {
        private _Entry[] _stack;
        private int _count;
        private _Invalidator(Integrator integrator, _Entry[] stack, int count)
        {
            Integrator = integrator;
            _stack = stack;
            _count = count;
        }

        /// <summary>
        /// Describes a queued invalidation in an <see cref="_Invalidator"/>.
        /// </summary>
        private struct _Entry
        {
            public _Entry(_Version start, _WeakDepend depend)
            {
                Start = start;
                Depend = depend;
            }

            /// <summary>
            /// The earliest <see cref="_Version"/> affected by this invalidation.
            /// </summary>
            public _Version Start;

            /// <summary>
            /// The <see cref="_WeakDepend"/> to be invalidated.
            /// </summary>
            public _WeakDepend Depend;

            /// <summary>
            /// Performs this invalidation.
            /// </summary>
            public void Invalidate(ref _Invalidator invalidator)
            {
                Depend.Invalidate(ref invalidator, Start);
            }
        }

        /// <summary>
        /// Creates a new <see cref="_Invalidator"/> for invalidation starting at the given version.
        /// </summary>
        public static _Invalidator Create(Integrator integrator)
        {
            return new _Invalidator(integrator, new _Entry[16], 0);
        }

        /// <summary>
        /// The <see cref="Reactive.Integrator"/> for the reactive system where invalidation is being performed.
        /// </summary>
        public Integrator Integrator { get; }

        /// <summary>
        /// Queues an invalidation to be made using this invalidator. This does not perform the invalidation immediately,
        /// so is safe to use in critical sections.
        /// </summary>
        /// <param name="start">The earliest <see cref="Version"/> to invalidate</param>
        public void Invalidate(_Version start, _WeakDepend depend)
        {
            Debug.Assert(start != null);
            if (_count >= _stack.Length)
                Array.Resize(ref _stack, _stack.Length * 2);
            _stack[_count++] = new _Entry(start, depend);
        }

        /// <summary>
        /// Resolves all invalidations queued to this invalidator.
        /// </summary>
        public void Process()
        {
            while (_count > 0) _stack[--_count].Invalidate(ref this);
        }
    }

    /// <summary>
    /// An interface to a stateful reactive object which can be updated in a transaction, thus being subject
    /// to invalidation by transaction rollback.
    /// </summary>
    internal interface _IState
    {
        /// <summary>
        /// Retracts changes that were made to this stateful object in the given <see cref="_Version"/>.
        /// </summary>
        void Rollback(ref _Invalidator invalidator, _Version version);
    }
}
