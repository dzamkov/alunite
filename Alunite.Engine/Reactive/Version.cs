﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;

using Alunite.Data;

namespace Alunite.Reactive
{
    /// <summary>
    /// Identifies a configuration of a reactive system. For any particular system, there is a total ordering
    /// over all <see cref="_Version"/>s that specifies when they logically occur in time. New versions are
    /// introduced either by the explicit updates of a <see cref="Transactor"/>, or by an invalidation
    /// due to progressing clock <see cref="Time"/>.
    /// </summary>
    internal class _Version
    {
        public _Version(Time time)
        {
            _disposables = Array.Empty<_IDisposableDynamic>();
            Time = time;
        }

        /// <summary>
        /// The set of <see cref="_IDisposableDynamic"/>s which own disposable objects that are applicable up to
        /// this version.
        /// </summary>
        private _IDisposableDynamic[] _disposables;

        /// <summary>
        /// The version immediately following this version.
        /// </summary>
        internal _Version _next;
        
        /// <summary>
        /// The earliest clock time where state from this version is visible. 
        /// </summary>
        public Time Time { get; }

        /// <summary>
        /// Indicates whether this version is a timepoint, i.e. the initial version in a particular <see cref="Time"/>
        /// that represents the configuration after the previous tick's update.
        /// </summary>
        public bool IsTimepoint => !(this is _UpdateVersion);

        /// <summary>
        /// If true, indicates that it is no longer possible to access or modify system configuration at or prior to
        /// this version.
        /// </summary>
        public bool IsDead => _disposables == null;

        /// <summary>
        /// Indicates that this version will no longer be accessed or modified.
        /// </summary>
        public void Kill()
        {
            // Clear link to predecessor
            _UpdateVersion update = this as _UpdateVersion;
            if (update != null)
                update._predecessor = null;

            lock (this)
            {
                // Notify disposables
                foreach (var disposable in _disposables)
                    if (disposable != null)
                        disposable.Kill(this);
                _disposables = null;
            }
        }

        /// <summary>
        /// Informs this <see cref="_Version"/> of an <see cref="_IDisposableDynamic"/> which owns disposable objects
        /// that are applicable up to this version. <see cref="_IDisposableDynamic.Kill(_Version)"/> will be called
        /// when this version is killed.
        /// </summary>
        public void AddDisposable(_IDisposableDynamic disposable)
        {
            lock (this)
            {
                if (_disposables != null)
                    Hashtable.Insert(ref _disposables, disposable);
                else
                    disposable.Kill(this);
            }
        }
        
        /// <summary>
        /// Determines the ordering of the given versions.
        /// </summary>
        public static int Compare(_Version a, _Version b)
        {
            if (a.Time < b.Time)
                return -1;
            else if (a.Time > b.Time)
                return 1;
            else if (a == b)
                return 0;
            else
            {
                // TODO: Should still work even after versions have been closed and _next has been set to null
                Time time = a.Time;
                while (true)
                {
                    a = Volatile.Read(ref a._next);
                    if (a == b)
                        return -1;
                    else if (a == null || a.Time > time)
                        return 1;
                }
            }
        }

        /// <summary>
        /// Gets the earlier of two verions.
        /// </summary>
        public static _Version Earlier(_Version a, _Version b)
        {
            return a < b ? a : b;
        }

        /// <summary>
        /// Gets the later of two versions.
        /// </summary>
        public static _Version Later(_Version a, _Version b)
        {
            return a < b ? b : a;
        }

        /// <summary>
        /// Determines the ordering of the given versions.
        /// </summary>
        public static bool operator <(_Version a, _Version b)
        {
            return Compare(a, b) < 0;
        }

        /// <summary>
        /// Determines the ordering of the given versions.
        /// </summary>
        public static bool operator >(_Version a, _Version b)
        {
            return Compare(a, b) > 0;
        }

        /// <summary>
        /// Determines the ordering of the given versions.
        /// </summary>
        public static bool operator <=(_Version a, _Version b)
        {
            return Compare(a, b) <= 0;
        }

        /// <summary>
        /// Determines the ordering of the given versions.
        /// </summary>
        public static bool operator >=(_Version a, _Version b)
        {
            return Compare(a, b) >= 0;
        }
    }

    /// <summary>
    /// An interface to a dynamic object that holds references to disposable objects.
    /// </summary>
    internal interface _IDisposableDynamic : IDisposable
    {
        /// <summary>
        /// Disposes of all objects that may only be retreived in versions prior to the given version. This is in
        /// response to <see cref="Integrator._base"/> being advanced to that version.
        /// </summary>
        void Kill(_Version cutoff);
    }
}
