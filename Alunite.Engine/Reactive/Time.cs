﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using Alunite.Data;

namespace Alunite.Reactive
{
    /// <summary>
    /// Describes a positive real-time length between moments at which events can occur.
    /// </summary>
    /// <remarks>Implemented as a fixed-point decimal, because precision should be consistently high
    /// across the possible range of values.</remarks>
    public struct Time
    {
        public Time(ulong ticks)
        {
            Ticks = ticks;
        }

        /// <summary>
        /// The number of ticks for this time.
        /// </summary>
        public ulong Ticks;

        /// <summary>
        /// The number of ticks in a second.
        /// </summary>
        public const ulong TicksPerSecond = 1000000;

        /// <summary>
        /// The length of this time span in seconds.
        /// </summary>
        public Scalar Seconds
        {
            get
            {
                return (float)Ticks / TicksPerSecond;
            }
        }

        /// <summary>
        /// The largest possible time length.
        /// </summary>
        public static readonly Time MaxValue = new Time(ulong.MaxValue);

        /// <summary>
        /// A zero-length time.
        /// </summary>
        public static readonly Time Zero = new Time(0);

        /// <summary>
        /// An arbitrary time representing an "origin" moment.
        /// </summary>
        public static readonly Time Origin = new Time(0);

        /// <summary>
        /// When used in an event, indicates that the event should be triggered immediately.
        /// </summary>
        public static readonly Time Immediate = Zero;

        /// <summary>
        /// Constructs a time length from the given amount of seconds.
        /// </summary>
        public static Time FromSeconds(Scalar seconds)
        {
            return new Time((ulong)(seconds._mean * TicksPerSecond));
        }

        /// <summary>
        /// Gets the latest time before the given time.
        /// </summary>
        public static Time Previous(Time time)
        {
            return new Time(time.Ticks - 1);
        }

        /// <summary>
        /// Gets the earliest time after the given time.
        /// </summary>
        public static Time Next(Time time)
        {
            return new Time(time.Ticks + 1);
        }

        /// <summary>
        /// Gets the earlier/shorter of two times.
        /// </summary>
        public static Time Earlier(Time a, Time b)
        {
            return a < b ? a : b;
        }

        /// <summary>
        /// Gets the later/longer of two times.
        /// </summary>
        public static Time Later(Time a, Time b)
        {
            return a < b ? b : a;
        }

        public static Time operator +(Time a, Time b)
        {
            return new Time(a.Ticks + b.Ticks);
        }

        public static Time operator -(Time a, Time b)
        {
            Debug.Assert(a >= b);
            return new Time(a.Ticks - b.Ticks);
        }

        public static ulong operator /(Time a, Time b)
        {
            return a.Ticks / b.Ticks;
        }

        public static Time operator *(Time a, ulong b)
        {
            return new Time(a.Ticks * b);
        }

        public static Time operator *(ulong a, Time b)
        {
            return new Time(a * b.Ticks);
        }

        public static bool operator <(Time a, Time b)
        {
            return a.Ticks < b.Ticks;
        }

        public static bool operator >(Time a, Time b)
        {
            return a.Ticks > b.Ticks;
        }

        public static bool operator <=(Time a, Time b)
        {
            return a.Ticks <= b.Ticks;
        }

        public static bool operator >=(Time a, Time b)
        {
            return a.Ticks >= b.Ticks;
        }

        public static bool operator ==(Time a, Time b)
        {
            return a.Ticks == b.Ticks;
        }

        public static bool operator !=(Time a, Time b)
        {
            return a.Ticks != b.Ticks;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Time))
                return false;
            return this == (Time)obj;
        }

        public override int GetHashCode()
        {
            return Ticks.GetHashCode();
        }

        public override string ToString()
        {
            return Seconds.ToString();
        }
    }

    /// <summary>
    /// Describes an analytical progression of a value over time.
    /// </summary>
    public interface ISpan<T>
    {
        /// <summary>
        /// The time at which this span begins to be applicable.
        /// </summary>
        Time Start { get; }

        /// <summary>
        /// The value of this span at <see cref="Start"/>.
        /// </summary>
        T Initial { get; }

        /// <summary>
        /// Gets the value of this span at the given time, which must be at or after <see cref="Start"/>.
        /// </summary>
        T this[Time time] { get; }
    }
}