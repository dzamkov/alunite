﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

using Alunite.Data;

namespace Alunite.Reactive
{
    /// <summary>
    /// A context which allows dynamic values to be updated.
    /// </summary>
    public sealed class Transactor : Observer
    {
        internal Transactor(Integrator integrator, _Version version, _WeakDepend depend)
            : base(integrator, version, depend)
        {
            _modified = Array.Empty<_IState>();
        }

        /// <summary>
        /// The <see cref="_Update"/> this transactor is for, or null if this is an ad-hoc transaction.
        /// </summary>
        internal _Update _update => (_version as _UpdateVersion)?.Update;

        /// <summary>
        /// Indicates whether immediately repeating the update operations made by this transactor will
        /// yield the same reactive system configuration as just applying them once.
        /// </summary>
        internal bool _isIdempotent => true;
        
        /// <summary>
        /// The invalidator for this transactor.
        /// </summary>
        internal _Invalidator _invalidator
        {
            get
            {
                return _Invalidator.Create(_integrator);
            }
            set
            {
                value.Process();
            }
        }

        /// <summary>
        /// The set of <see cref="_IState"/>s that were modified by this transaction.
        /// </summary>
        internal _IState[] _modified;

        /// <summary>
        /// Records a given <see cref="_IState"/> as modified so it can be rolled back along with the transaction.
        /// </summary>
        internal void _modify(_IState state)
        {
            Hashtable.Insert(ref _modified, state);
        }
    }

    /// <summary>
    /// Describes a transaction that is applied to a reactive system every <see cref="Time"/> tick within a time
    /// range. There is a time-invariant ordering of <see cref="_Update"/>s which determines the order that overlapping
    /// updates are applied within a tick.
    /// </summary>
    internal sealed class _Update : _IDepend
    {
        public _Update(Action<Transactor> apply)
        {
            WeakThis = new _WeakDepend(this);
            Apply = apply;
        }

        /// <summary>
        /// The defining function for this update.
        /// </summary>
        public Action<Transactor> Apply { get; }
        
        /// <summary>
        /// The latest <see cref="_UpdateVersion"/> for this <see cref="_Update"/> that has been processed or is currently
        /// being processed.
        /// </summary>
        internal _UpdateVersion _head;

        /// <summary>
        /// The next <see cref="_UpdateVersion"/> to be processed.
        /// </summary>
        internal _UpdateVersion _pending;

        /// <summary>
        /// A version that indicates when this update should stop being applied. Since updates are applied at most once
        /// per tick, this does not need to be an exact. If this is null, the update is to be applied indefinitely.
        /// </summary>
        internal _Version _cutoff;

        /// <summary>
        /// Determines the in-tick ordering of the given updates.
        /// </summary>
        public static int Compare(_Update a, _Update b)
        {
            if (a == b)
                return 0;

            // TODO
            throw new NotImplementedException();
        }

        /// <summary>
        /// Determines the in-tick ordering of the given updates.
        /// </summary>
        public static bool operator <(_Update a, _Update b)
        {
            return Compare(a, b) < 0;
        }

        /// <summary>
        /// Determines the in-tick ordering of the given updates.
        /// </summary>
        public static bool operator >(_Update a, _Update b)
        {
            return Compare(a, b) > 0;
        }

        /// <summary>
        /// Determines the in-tick ordering of the given updates.
        /// </summary>
        public static bool operator <=(_Update a, _Update b)
        {
            return Compare(a, b) <= 0;
        }

        /// <summary>
        /// Determines the in-tick ordering of the given updates.
        /// </summary>
        public static bool operator >=(_Update a, _Update b)
        {
            return Compare(a, b) >= 0;
        }
        
        /// <summary>
        /// A weak reference to this <see cref="_Update"/>.
        /// </summary>
        public _WeakDepend WeakThis { get; }

        /// <summary>
        /// Invalidates the changes due to this <see cref="_Update"/> starting at the given version.
        /// </summary>
        public void Invalidate(ref _Invalidator invalidator, _Version start)
        {
            lock (WeakThis)
            {
                _invalidate(ref invalidator, start);
            }
        }

        void _IDepend.Invalidate(ref _Invalidator invalidator, _Version start)
        {
            // Lock on WeakThis is already held
            _invalidate(ref invalidator, start);
        }

        /// <summary>
        /// Invalidates the changes due to this <see cref="_Update"/> starting at the given version.
        /// </summary>
        private void _invalidate(ref _Invalidator invalidator, _Version start)
        {
            // NOTE: it's possible for an invalidation to be done by a currently-processing version of
            // this update. This indicates that a transaction is not idempotent, since it invalidates
            // its own initial conditions. In this case, we will schedule a pending update immediately
            // after the version

            // Rollback invalidated versions
            while (_head != null && start < _head)
            {
                _head._rollback(ref invalidator);
                _head = _head._predecessor;
            }

            // Schedule new update
            _queue(invalidator.Integrator, start);
        }

        /// <summary>
        /// Queues this update to be applied at the next available opportunity after the given version. This
        /// requires the <see cref="_Update"/> to be locked.
        /// </summary>
        internal void _queue(Integrator integrator, _Version after)
        {
            if (_pending == null || after < _pending)
            {
                _pending = integrator._getUpdate(this, after);
                Debug.Assert(_head == null || _head < _pending);
                integrator._queue(new Integrator._Task(_pending));
            }
        }
    }

    /// <summary>
    /// A <see cref="_Version"/> resulting from the application of a scheduled <see cref="_Update"/>.
    /// </summary>
    internal sealed class _UpdateVersion : _Version
    {
        public _UpdateVersion(Time time, _Update update)
            : base(time)
        {
            Update = update;
        }
        
        /// <summary>
        /// The previous <see cref="_UpdateVersion"/> for <see cref="Update"/> that has been processed.
        /// </summary>
        internal _UpdateVersion _predecessor;

        /// <summary>
        /// The set of <see cref="_IState"/>s modified in this update version, or null if the version has not
        /// been processed.
        /// </summary>
        internal _IState[] _modified;

        /// <summary>
        /// The <see cref="_Update"/> this version is associated with.
        /// </summary>
        public _Update Update { get; }

        /// <summary>
        /// Computes the effects of <see cref="Update"/> on this version and introduces them to the reactive system,
        /// if needed. The configuration of the reactive system is not valid at or after this version until this is called.
        /// </summary>
        public void Process(Integrator integrator)
        {
            lock (Update.WeakThis)
            {
                // Verify that this still needs processing
                if (Update._pending == this)
                {
                    // Add this to the chain of processing/processed updates
                    Update._pending = null;
                    _predecessor = Update._head;
                    Debug.Assert(_predecessor == null || _predecessor < this);
                    Update._head = this;

                    // Process the update
                    Transactor trans = new Transactor(integrator, this, Update.WeakThis);
                    Update.Apply(trans);
                    _modified = trans._modified;

                    // Queue next update
                    if (trans._isIdempotent)
                    {
                        // Since the transaction is idempotent, we can refrain from additional updates until
                        // something about the transaction's observations change.
                        if (trans._cutoff != null)
                            Update._queue(integrator, trans._cutoff);
                    }
                    else
                    {
                        Update._queue(integrator, this);
                    }
                }
            }
        }
        
        /// <summary>
        /// Retracts all changes made in this <see cref="_UpdateVersion"/>. This assumes the <see cref="_Update"/> is
        /// locked.
        /// </summary>
        internal void _rollback(ref _Invalidator invalidator)
        {
            foreach (_IState state in _modified)
                if (state != null)
                    state.Rollback(ref invalidator, this);
        }
    }
}
