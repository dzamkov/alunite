﻿using System;
using System.Collections.Generic;

namespace Alunite.Reactive
{
    /// <summary>
    /// An <see cref="IBuilder{T}"/> for a stateful reactive object.
    /// </summary>
    public interface IReactiveBuilder<out T> : IBuilder<T>
    {
        /// <summary>
        /// The <see cref="Transactor"/> providing the context needed to initialize stateful reactive objects.
        /// </summary>
        Delay<Transactor> Initializer { set; }
    }

    /// <summary>
    /// Contains helper functions related to <see cref="IReactiveBuilder{T}"/>.
    /// </summary>
    public static class ReactiveBuilder
    {
        /// <summary>
        /// Closes the builder and gets the object resulting from it. After this is called, there should
        /// be no further calls to the builder.
        /// </summary>
        /// <param name="init">Provides the context needed to initialize stateful reactive objects</param>
        public static T Finish<T>(this IReactiveBuilder<T> builder, Transactor init)
        {
            builder.Initializer = init;
            return builder.Finish();
        }
    }
}
