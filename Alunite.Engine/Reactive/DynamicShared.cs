﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

using Alunite.Data;
using Alunite.Resources;

namespace Alunite.Reactive
{
    /// <summary>
    /// A time-varying value of disposable type <see cref="T"/> which creates and tracks <see cref="Shared{T}"/>
    /// instances of objects.
    /// </summary>
    /// <remarks>This intentionally does not extend <see cref="Dynamic{T}"/> or provide an indexer due to a subtle
    /// caveat in the interface; whenever a reference is obtained from a <see cref="DynamicShared{T}"/>, the client
    /// is expected to dispose it when no longer needed.</remarks>
    public sealed class DynamicShared<T> : _IDisposableDynamic, _IDepend
        where T : IDisposable
    {
        // TODO: Consider using a large cache to continue tracking objects that are still referenced elsewhere
        private readonly _WeakDepend _weakThis;
        private _DynamicCache<Shared<T>> _cache;
        public DynamicShared(DynamicFunc<T> factory)
        {
            _weakThis = new _WeakDepend(this);
            Factory = factory;
        }

        /// <summary>
        /// The factory function used to create instances for this <see cref="Dynamic{T}"/>.
        /// </summary>
        public DynamicFunc<T> Factory { get; }
        
        /// <summary>
        /// Gets a reference to the object representing the value of this <see cref="DynamicShared{T}"/> under the context
        /// of the given <see cref="Observer"/>. The caller must dispose of the reference when no longer needed.
        /// </summary>
        public Shared<T> Use(Observer obs)
        {
            lock (_weakThis)
            {
                if (_cache.TryUse(obs._version, out Shared<T> value, out _Version start, out _Version end))
                {
                    if (obs._observe(start, end))
                        _weakThis.Depends.Add(obs);
                    bool isAlive = value.TryUse();
                    Debug.Assert(isAlive);
                    return value;
                }
                else
                {
                    // Create new value
                    Observer innerObs = new Observer(obs._integrator, obs._version, _weakThis);
                    T created = Factory(innerObs);

                    // Add to cache
                    value = Shared.Share(created);
                    if (_cache.TryAdd(false, innerObs._base, innerObs._cutoff, ref value))
                    {
                        // Have this dynamic be notified when the value is no longer accessible
                        if (innerObs._cutoff != null)
                            innerObs._cutoff.AddDisposable(this);
                    }
                    else
                    {
                        // If we aren't using the object we just created, dispose it
                        created.Dispose();
                    }

                    // Use value
                    if (obs._observe(innerObs._base, innerObs._cutoff))
                        _weakThis.Depends.Add(obs);
                    bool isAlive = value.TryUse();
                    Debug.Assert(isAlive);
                    return value;
                }
            }
        }

        /// <summary>
        /// Indicates that this dynamic value is no longer needed, releasing references to all cached objects.
        /// </summary>
        public void Dispose()
        {
            _cache.Clear(true);
        }

        _WeakDepend _IDepend.WeakThis => _weakThis;

        void _IDepend.Invalidate(ref _Invalidator invalidator, _Version start)
        {
            _cache.Invalidate(true, start);

            // Have this dynamic be notified when the version passes and prior values are no
            // longer accessible
            start.AddDisposable(this);
        }

        void _IDisposableDynamic.Kill(_Version cutoff)
        {
            lock (_weakThis)
            {
                _cache.Close(true, cutoff);
            }
        }
    }
}