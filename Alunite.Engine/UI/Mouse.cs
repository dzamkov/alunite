﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;

using Alunite.Data;
using Alunite.Reactive;

namespace Alunite.UI
{
    /// <summary>
    /// An interface to a mouse.
    /// </summary>
    public interface IMouse
    {
        /// <summary>
        /// The position of the mouse.
        /// </summary>
        Dynamic<Vector2i> Position { get; }
    }

    /// <summary>
    /// An interface to an object which intercepts exclusive mouse input.
    /// </summary>
    public interface IMouseListener
    {
        /// <summary>
        /// Processes a discrete mouse movement of the given amount.
        /// </summary>
        void Move(Transactor trans, Vector2 delta);

        /// <summary>
        /// Responds to a button down event.
        /// </summary>
        void ButtonDown(Transactor trans, MouseButton button);

        /// <summary>
        /// Responds to a button up event.
        /// </summary>
        /// <param name="exit">If true, indicates that mouse capture should end as a result of this
        /// event.</param>
        void ButtonUp(Transactor trans, MouseButton button, out bool exit);
    }

    /// <summary>
    /// An interface to a region of space in the UI that can accept ad-hoc input from the mouse.
    /// </summary>
    public interface IMouseSurface
    {
        /// <summary>
        /// Gets the current mouse handler for the given point.
        /// </summary>
        MouseHandler GetHandler(Observer obs, Vector2i point);
    }

    /// <summary>
    /// Describes how an area or point on an interactive surface responds to mouse input.
    /// </summary>
    public struct MouseHandler
    {
        /// <summary>
        /// The component of this <see cref="MouseHandler"/> that deals with hovering.
        /// </summary>
        public IMouseHoverHandler Hover;

        /// <summary>
        /// The component of this <see cref="MouseHandler"/> that deals with the left button.
        /// </summary>
        public MouseButtonHandler LeftButton;

        /// <summary>
        /// The component of this <see cref="MouseHandler"/> that deals with the right button.
        /// </summary>
        public MouseButtonHandler RightButton;

        /// <summary>
        /// Gets or sets the <see cref="MouseButtonHandler"/> component of this handler that deals with
        /// the given button.
        /// </summary>
        public MouseButtonHandler this[MouseButton button]
        {
            get
            {
                if (button == MouseButton.Left)
                    return LeftButton;
                else if (button == MouseButton.Right)
                    return RightButton;

                // TODO
                throw new NotImplementedException();
            }
            set
            {
                if (button == MouseButton.Left)
                    LeftButton = value;
                else if (button == MouseButton.Right)
                    RightButton = value;
            }
        }

        /// <summary>
        /// A <see cref="MouseHandler"/> which defines no interactions.
        /// </summary>
        public static MouseHandler None = default(MouseHandler);

        /// <summary>
        /// Gets the <see cref="MouseHandler"/> that results from overlaying one interactive surface
        /// in front of another. The front surface will have the first chance to respond to mouse
        /// events.
        /// </summary>
        public static MouseHandler operator +(MouseHandler back, MouseHandler front)
        {
            return new MouseHandler
            {
                Hover = front.Hover ?? back.Hover,
                LeftButton = back.LeftButton + front.LeftButton,
                RightButton = back.RightButton + front.RightButton
            };
        }
    }

    /// <summary>
    /// Describes how an area or point on an interactive surface responds to mouse hovering. The identity
    /// of a <see cref="IMouseHoverHandler"/> is especially important, since it needs to be tracked
    /// over time to determine how long the hover lasts.
    /// </summary>
    public interface IMouseHoverHandler
    {

    }

    /// <summary>
    /// Describes how an area or point on an interactive surface responds to interactions involving
    /// a particular <see cref="MouseButton"/>.
    /// </summary>
    public struct MouseButtonHandler
    {
        public MouseButtonHandler(IMouseClickHandler click, IMouseDragHandler drag)
        {
            Click = click;
            Drag = drag;
        }

        /// <summary>
        /// The component of this <see cref="MouseButtonHandler"/> that deals with clicking.
        /// </summary>
        public IMouseClickHandler Click;

        /// <summary>
        /// The component of this <see cref="IMouseDragHandler"/> that deals with dragging.
        /// </summary>
        public IMouseDragHandler Drag;

        /// <summary>
        /// A <see cref="MouseButtonHandler"/> which defines no interactions.
        /// </summary>
        public static MouseButtonHandler None = default(MouseButtonHandler);

        /// <summary>
        /// Gets the <see cref="MouseButtonHandler"/> that results from overlaying one interactive surface
        /// in front of another. The front surface will have the first chance to respond to mouse
        /// events.
        /// </summary>
        public static MouseButtonHandler operator +(MouseButtonHandler back, MouseButtonHandler front)
        {
            return new MouseButtonHandler
            {
                Click = front.Click ?? back.Click,
                Drag = front.Drag ?? back.Drag
            };
        }
    }

    /// <summary>
    /// Describes how an area or point on an interactive surface responds to a mouse click.
    /// </summary>
    public interface IMouseClickHandler
    {
        /// <summary>
        /// Handles a mouse click in the area of this <see cref="IMouseClickHandler"/>.
        /// </summary>
        /// <param name="listener">If non-null, indicates that the mouse will be captured by the click</param>
        void Click(Transactor trans,
            IMouse mouse, MouseButton button, Vector2i pos,
            out IMouseListener listener);
    }

    /// <summary>
    /// Describes how an area or point on an interactive surface responds to a drag gesture from
    /// the mouse.
    /// </summary>
    public interface IMouseDragHandler
    {
        /// <summary>
        /// Starts a mouse drag operation in the area of this <see cref="IMouseDragHandler"/>.
        /// </summary>
        /// <param name="listener">If non-null, indicates that the drag should be handled by the given mouse listener.
        /// In this case, the life cycle of the drag operation will be managed by the listener, and <paramref name="end"/>
        /// will not be called.</param>
        /// <param name="end">A callback to indicate when the drag operation has ended.</param>
        void Drag(Transactor trans,
            IMouse mouse, MouseButton button, Vector2i pos,
            out IMouseListener listener, out Action<Transactor> end);
    }

    /// <summary>
    /// Represents a user's mouse.
    /// </summary>
    public sealed class Mouse : IMouse
    {
        private readonly State<MouseState> _state;
        private readonly Dynamic<Vector2i> _position;
        public Mouse(IMouseSurface surface, Action<Transactor, bool> captureChanged, Transactor init, MouseState state)
        {
            _state = new State<MouseState>(init, state);
            _position = _state.Map(s => s.Position);
            Surface = surface;
            CaptureChanged = captureChanged;
            PositionHandler = Dynamic.Compute(obs => Surface.GetHandler(obs, Position[obs]));
        }

        public Mouse(IMouseSurface surface, Action<Transactor, bool> captureChanged, Transactor init)
            : this(surface, captureChanged, init, MouseState.Initial)
        { }

        /// <summary>
        /// The distance, in pixels, the mouse has the move with a button held down to be registered as a drag.
        /// </summary>
        public const int DragThreshold = 5;

        /// <summary>
        /// The state of this mouse at the given moment.
        /// </summary>
        public Dynamic<MouseState> State => _state;

        /// <summary>
        /// The position of the mouse pointer in window space.
        /// </summary>
        public Dynamic<Vector2i> Position => _position;

        /// <summary>
        /// The the <see cref="MouseHandler"/> for the point on <see cref="IMouseSurface"/> that is
        /// under the mouse's current position.
        /// </summary>
        public Dynamic<MouseHandler> PositionHandler { get; }
        
        /// <summary>
        /// The surface this mouse is on.
        /// </summary>
        public IMouseSurface Surface { get; }

        /// <summary>
        /// An event which indicates when the captured state of this mouse is changed.
        /// </summary>
        public Action<Transactor, bool> CaptureChanged { get; }

        /// <summary>
        /// Forcefully releases mouse capture, if applicable.
        /// </summary>
        public bool Release(Transactor trans)
        {
            MouseState state = _state[trans];
            if (state.Listener != null)
            {
                state.Listener = null;
                _state[trans] = state;
                CaptureChanged?.Invoke(trans, false);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Processes a discrete mouse movement of the given amount, which puts the mouse at the given
        /// position.
        /// </summary>
        public void Move(Transactor trans, Vector2 delta, Vector2i pos)
        {
            MouseState state = _state[trans];
            if (state.Listener != null)
            {
                Debug.Assert(state.Hold == null);
                state.Listener.Move(trans, delta);
            }
            else
            {
                state.Position = pos;
                MouseHold hold = state.Hold;
                if (hold != null)
                {
                    // Start drag if possible
                    if (hold.DragHandler != null)
                    {
                        Vector2i diff = pos - hold.Position;
                        if (diff.SqrLength >= DragThreshold * DragThreshold)
                            _startDrag(trans, ref state, hold.Button, hold.DragHandler);
                    }
                }
                _state[trans] = state;
            }
        }

        /// <summary>
        /// Processes a mouse down event.
        /// </summary>
        public void ButtonDown(Transactor trans, MouseButton button)
        {
            MouseState state = _state[trans];
            if (state.Listener != null)
            {
                Debug.Assert(state.Hold == null);
                state.Listener.ButtonDown(trans, button);
            }
            else if (state.Hold == null)
            {
                MouseButtonHandler handler = PositionHandler[trans][button];
                if (handler.Click != null)
                {
                    state.Hold = new MouseHold(button, state.Position, handler.Drag, handler.Click, null);
                }
                else if (handler.Drag != null)
                {
                    // Start drag immediately
                    _startDrag(trans, ref state, button, handler.Drag);
                }
                else
                {
                    state.Hold = new MouseHold(button, state.Position, null, null, null);
                }
                _state[trans] = state;
            }
        }

        /// <summary>
        /// Processes a mouse button up event.
        /// </summary>
        public void ButtonUp(Transactor trans, MouseButton button)
        {
            MouseState state = _state[trans];
            if (state.Listener != null)
            {
                Debug.Assert(state.Hold == null);
                state.Listener.ButtonUp(trans, button, out bool exit);
                if (exit)
                {
                    state.Listener = null;
                    _state[trans] = state;
                    CaptureChanged?.Invoke(trans, false);
                }
            }
            else if (state.Hold != null && state.Hold.Button == button)
            {
                IMouseClickHandler clickHandler = state.Hold.ClickHandler;
                if (clickHandler != null && clickHandler == PositionHandler[trans][button].Click)
                {
                    clickHandler.Click(trans, this, button, state.Position, out IMouseListener listener);
                    if (listener != null)
                    {
                        state.Listener = listener;
                        CaptureChanged?.Invoke(trans, true);
                    }
                }
                state.Hold.EndDrag?.Invoke(trans);
                state.Hold = null;
                _state[trans] = state;
            }
        }

        /// <summary>
        /// Begins a drag operation using the given handler.
        /// </summary>
        private void _startDrag(Transactor trans,
            ref MouseState state, MouseButton button,
            IMouseDragHandler handler)
        {
            handler.Drag(trans, this, button, state.Position, out IMouseListener listener, out Action<Transactor> end);
            if (listener != null)
            {
                state.Listener = listener;
                CaptureChanged?.Invoke(trans, true);
            }
            else
            {
                state.Hold = new MouseHold(button, state.Position, null, null, end);
            }
        }
    }

    /// <summary>
    /// Describes the state of a <see cref="Mouse"/> at a particular moment.
    /// </summary>
    public struct MouseState
    {
        public MouseState(Vector2i position, IMouseListener listener, MouseHold hold)
        {
            Position = position;
            Listener = listener;
            Hold = hold;
        }

        /// <summary>
        /// The position of the mouse.
        /// </summary>
        public Vector2i Position;

        /// <summary>
        /// The current mouse listener for the mouse, or null if the mouse is not captured. If this is non-null,
        /// <see cref="Hold"/> must be null.
        /// </summary>
        public IMouseListener Listener;

        /// <summary>
        /// If a button is being held down for a click/drag operation, describes additional information about
        /// that operation. If this is non-null, <see cref="Listener"/> must be null.
        /// </summary>
        public MouseHold Hold;

        /// <summary>
        /// A good initial state for a mouse.
        /// </summary>
        public static readonly MouseState Initial = new MouseState(Vector2i.Zero, null, null);
    }

    /// <summary>
    /// Describes the condition of a mouse when a mouse button is being held.
    /// </summary>
    public sealed class MouseHold
    {
        public MouseHold(
            MouseButton button, Vector2i position,
            IMouseDragHandler dragHandler,
            IMouseClickHandler clickHandler,
            Action<Transactor> endDrag)
        {
            Button = button;
            Position = position;
            DragHandler = dragHandler;
            ClickHandler = clickHandler;
            EndDrag = endDrag;
        }

        /// <summary>
        /// The button that is being held.
        /// </summary>
        public readonly MouseButton Button;

        /// <summary>
        /// The position of the mouse at the time the button was held down.
        /// </summary>
        public readonly Vector2i Position;

        /// <summary>
        /// The <see cref="IMouseDragHandler"/> at the place this hold originated from.
        /// </summary>
        public readonly IMouseDragHandler DragHandler;

        /// <summary>
        /// The <see cref="IMouseClickHandler"/> at the place this hold originated from, or null if
        /// a click is no longer an option.
        /// </summary>
        public readonly IMouseClickHandler ClickHandler;

        /// <summary>
        /// Ends the drag operation initiated with this hold, if applicable.
        /// </summary>
        public readonly Action<Transactor> EndDrag;
    }

    /// <summary>
    /// Describes the conditions under which a mouse button was held down.
    /// </summary>
    public struct MouseDown
    {
        public MouseDown(MouseButton button, Vector2i position, MouseHandler handler)
        {
            Button = button;
            Position = position;
            Handler = handler;
        }

        /// <summary>
        /// The button that was held down.
        /// </summary>
        public MouseButton Button;

        /// <summary>
        /// The position of the mouse when the button was held down.
        /// </summary>
        public Vector2i Position;

        /// <summary>
        /// The mouse handler at the position the button was held down.
        /// </summary>
        public MouseHandler Handler;
    }

    /// <summary>
    /// Identifies a mouse button.
    /// </summary>
    public struct MouseButton : IEquatable<MouseButton>
    {
        private int _id;
        public MouseButton(int id)
        {
            _id = id;
        }

        /// <summary>
        /// The left mouse button.
        /// </summary>
        public static readonly MouseButton Left = OpenTK.Input.MouseButton.Left;

        /// <summary>
        /// The right mouse button.
        /// </summary>
        public static readonly MouseButton Right = OpenTK.Input.MouseButton.Right;

        public static implicit operator MouseButton(OpenTK.Input.MouseButton key)
        {
            return new MouseButton((int)key);
        }

        public static bool operator ==(MouseButton a, MouseButton b)
        {
            return a._id == b._id;
        }

        public static bool operator !=(MouseButton a, MouseButton b)
        {
            return !(a == b);
        }

        bool IEquatable<MouseButton>.Equals(MouseButton other)
        {
            return this == other;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is MouseButton))
                return false;
            return this == (MouseButton)obj;
        }

        public override int GetHashCode()
        {
            return _id.GetHashCode();
        }
    }
}
