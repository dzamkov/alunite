﻿using System;
using System.Collections.Generic;
using System.Linq;

using Alunite.Reactive;

namespace Alunite.UI
{
    /// <summary>
    /// An interface to a keyboard.
    /// </summary>
    public interface IKeyboard
    {
        /// <summary>
        /// Tries getting a <see cref="Dynamic{T}"/> indicating when a given standard key
        /// is down, returning false if that key does not exist.
        /// </summary>
        bool TryGetStandardKeyIsDown(StandardKey id, out Dynamic<bool> isDown);
    }

    /// <summary>
    /// An interface to a keyboard.
    /// </summary>
    public interface IKeyboard<TKey> : IKeyboard
    {
        /// <summary>
        /// The equality comparer for keys in this keyboard.
        /// </summary>
        IEqualityComparer<TKey> KeyComparer { get; }

        /// <summary>
        /// Attempts to get an interface to a standard key on this keyboard, returning
        /// false if that key does not exist.
        /// </summary>
        bool TryGetKey(StandardKey id, out Key key);

        /// <summary>
        /// Indicates whether the given key is depressed at any given moment.
        /// </summary>
        Dynamic<bool> IsDown(TKey key);
    }

    /// <summary>
    /// Represents a user's keyboard.
    /// </summary>
    public sealed class Keyboard : IKeyboard<Key>
    {
        public Keyboard(IEnumerable<StandardKey> keys, Transactor init)
        {
            var standardKeys = new Dictionary<StandardKey, Key>();
            foreach (var key in keys)
                standardKeys.Add(key, new Key(init));
            StandardKeys = standardKeys;
        }

        /// <summary>
        /// A mapping from <see cref="StandardKey"/>s to <see cref="Key"/>s on this keyboard.
        /// </summary>
        public Dictionary<StandardKey, Key> StandardKeys { get; }

        /// <summary>
        /// Gets a <see cref="Key"/> representation of a standard key on this keyboard.
        /// </summary>
        public Key this[StandardKey key]
        {
            get
            {
                return StandardKeys[key];
            }
        }

        /// <summary>
        /// Tries getting a <see cref="Key"/> representation of a standard key on this keyboard, returning false if
        /// that key does not exist.
        /// </summary>
        public bool TryGetKey(StandardKey id, out Key key)
        {
            return StandardKeys.TryGetValue(id, out key);
        }

        /// <summary>
        /// Indicates whether the given key is depressed at any given moment.
        /// </summary>
        public Dynamic<bool> IsDown(Key key)
        {
            return key.IsDown;
        }

        /// <summary>
        /// Processes a key down event for the given key.
        /// </summary>
        public void KeyDown(Transactor trans, Key key)
        {
            key._isDown[trans] = true;
        }

        /// <summary>
        /// Processes a key down up for the given key.
        /// </summary>
        public void KeyUp(Transactor trans, Key key)
        {
            key._isDown[trans] = false;
        }

        bool IKeyboard.TryGetStandardKeyIsDown(StandardKey id, out Dynamic<bool> isDown)
        {
            if (TryGetKey(id, out Key key))
            {
                isDown = IsDown(key);
                return true;
            }
            isDown = default;
            return false;
        }

        IEqualityComparer<Key> IKeyboard<Key>.KeyComparer
        {
            get
            {
                return EqualityComparer<Key>.Default;
            }
        }
    }

    /// <summary>
    /// Represents a key on a keyboard.
    /// </summary>
    public sealed class Key
    {
        internal readonly State<bool> _isDown;
        public Key(Transactor init)
        {
            _isDown = new State<bool>(init, false);
        }

        /// <summary>
        /// Indicates whether this key is depressed.
        /// </summary>
        public Dynamic<bool> IsDown => _isDown;
    }

    /// <summary>
    /// Identifies a common key that exists on many keyboards. 
    /// </summary>
    public struct StandardKey
    {
        private int _id;
        private StandardKey(int id)
        {
            _id = id;
        }

        /// <summary>
        /// Identifies the up arrow key.
        /// </summary>
        public static readonly StandardKey Up = OpenTK.Input.Key.Up;

        /// <summary>
        /// Identifies the left arrow key.
        /// </summary>
        public static readonly StandardKey Down = OpenTK.Input.Key.Down;

        /// <summary>
        /// Identifies the up arrow key.
        /// </summary>
        public static readonly StandardKey Left = OpenTK.Input.Key.Left;

        /// <summary>
        /// Identifies the up arrow key.
        /// </summary>
        public static readonly StandardKey Right = OpenTK.Input.Key.Right;

        /// <summary>
        /// Identifies the left shift key.
        /// </summary>
        public static readonly StandardKey LShift = OpenTK.Input.Key.LShift;

        /// <summary>
        /// Identifies the left control key.
        /// </summary>
        public static readonly StandardKey LControl = OpenTK.Input.Key.LControl;

        /// <summary>
        /// Identifies the space key.
        /// </summary>
        public static readonly StandardKey Space = OpenTK.Input.Key.Space;

        /// <summary>
        /// Gets the key corresponding to the given character key on a standard QWERTY keyboard.
        /// </summary>
        public static StandardKey Fixed(char ch)
        {
            // TODO: Respect different keyboard layouts
            if (ch >= 'A' && ch <= 'Z')
                return new StandardKey((int)OpenTK.Input.Key.A + (ch - 'A'));
            if (ch >= 'a' && ch <= 'z')
                return new StandardKey((int)OpenTK.Input.Key.A + (ch - 'a'));
            if (ch >= '0' && ch <= '9')
                return new StandardKey((int)OpenTK.Input.Key.Number0 + (ch - '0'));
            throw new ArgumentException("Unrecognized character");
        }

        /// <summary>
        /// The set of all standard keys.
        /// </summary>
        public static readonly StandardKey[] All = _getAll().ToArray();

        /// <summary>
        /// Enumerates all standard keys.
        /// </summary>
        private static IEnumerable<StandardKey> _getAll()
        {
            for (int i = 1; i < (int)OpenTK.Input.Key.LastKey; i++)
                yield return new StandardKey(i);
        }

        public static implicit operator StandardKey(OpenTK.Input.Key key)
        {
            return new StandardKey((int)key);
        }
    }
}
