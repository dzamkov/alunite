﻿using System;

using Alunite.Data;
using Alunite.Data.Geometry;
using Alunite.Reactive;
using Alunite.Graphics;

namespace Alunite.UI
{
    /// <summary>
    /// A rectangular region on the screen that the user can interact with.
    /// </summary>
    public interface IControl : IMouseSurface, IActor
    {
        /// <summary>
        /// The location of the top-left corner of the control in screen space.
        /// </summary>
        Dynamic<Vector2i> TopLeft { get; }

        /// <summary>
        /// The width of the control in screen space.
        /// </summary>
        Dynamic<int> Width { get; }

        /// <summary>
        /// The height of the control in screen space.
        /// </summary>
        Dynamic<int> Height { get; }

        /// <summary>
        /// Draws the contents of this control to the given <see cref="IDrawer2"/>.
        /// </summary>
        void Draw(Observer obs, IDrawer2 drawer);
    }

    /// <summary>
    /// A helper class for constructing a <see cref="IControl"/>.
    /// </summary>
    public interface IControlBuilder : IReactiveBuilder<IControl>
    {
        /// <summary>
        /// The user interface for the control.
        /// </summary>
        Delay<IInterface> Interface { set; }

        /// <summary>
        /// The location of the top-left corner of the control in screen space.
        /// </summary>
        Delay<Dynamic<Vector2i>> TopLeft { set; }

        /// <summary>
        /// The width of the control in screen space. For some controls, this may be pre-defined and not
        /// adjustable.
        /// </summary>
        Delay<Dynamic<int>> Width { get; set; }

        /// <summary>
        /// The height of the control in screen space. For some controls, this may be pre-defined and not
        /// adjustable.
        /// </summary>
        Delay<Dynamic<int>> Height { get; set; }
    }

    /// <summary>
    /// Contains information about the user interface as a whole.
    /// </summary>
    public interface IInterface
    {
        /// <summary>
        /// The keyboard for the user interface.
        /// </summary>
        IKeyboard Keyboard { get; }
    }
}