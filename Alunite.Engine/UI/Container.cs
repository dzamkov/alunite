﻿using System;

using Alunite.Data;
using Alunite.Data.Geometry;

namespace Alunite.UI
{
    /// <summary>
    /// Describes an amount of padding that can be added around a <see cref="Box2i"/>.
    /// </summary>
    public struct Padding
    {
        public int Left;
        public int Top;
        public int Right;
        public int Bottom;
        public Padding(int left, int top, int right, int bottom)
        {
            Left = left;
            Top = top;
            Right = right;
            Bottom = bottom;
        }

        public Padding(int amount)
        {
            Left = amount;
            Top = amount;
            Right = amount;
            Bottom = amount;
        }

        /// <summary>
        /// Gets the offset of the top-left corner of a box with this padding applied.
        /// </summary>
        public Vector2i Offset
        {
            get
            {
                return new Vector2i(Left, Top);
            }
        }

        /// <summary>
        /// Gets the extra size added by this padding.
        /// </summary>
        public Vector2i Size
        {
            get
            {
                return new Vector2i(Left + Right, Top + Bottom);
            }
        }

        /// <summary>
        /// Applies this padding to a box.
        /// </summary>
        public Box2i Apply(Box2i box)
        {
            return new Box2i(
                box.Min.X - Left,
                box.Min.Y - Top,
                box.Max.X + Right,
                box.Max.Y + Bottom);
        }

        public static implicit operator Padding(int amount)
        {
            return new Padding(amount);
        }
    }
}
