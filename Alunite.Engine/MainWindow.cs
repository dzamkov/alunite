﻿using System;
using System.Collections.Generic;

using Alunite.Data;
using Alunite.Graphics;
using Alunite.Graphics.Bindings.OpenGL;
using Alunite.Reactive;
using Alunite.UI;
using Keyboard = Alunite.UI.Keyboard;
using Mouse = Alunite.UI.Mouse;
using Key = Alunite.UI.Key;
using Vector2 = Alunite.Data.Vector2;

using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Input;
using OpenTK;

namespace Alunite
{
    /// <summary>
    /// A window which displays the view for a game.
    /// </summary>
    public sealed class MainWindow : OpenTK.GameWindow, IInterface
    {
        private DateTime _start;
        private Integrator _integrator;
        private State<Vector2i> _size;
        private bool _mouseCaptured;
        private Vector2i _lastMousePos;
        private Instance _inst;
        private DynamicShared<DynamicDrawJob> _drawJob;
        public MainWindow(IControlBuilder control)
            : base(1024, 768, new GraphicsMode(new ColorFormat(8, 8, 8, 0), 16, 0, 8), "Alunite")
        {
            // Initialize window content
            _integrator = new Integrator(Time.Zero);
            using (_integrator.Transact(out Transactor init))
            {
                _size = new State<Vector2i>(init, new Vector2i(Width, Height));
                Keyboard = new Keyboard(StandardKey.All, init);
                control.Interface = this;
                control.TopLeft = Dynamic.Const(Vector2i.Zero);
                control.Width = _size.Map(size => size.X);
                control.Height = _size.Map(size => size.Y);
                Control = control.Finish(init);
                Mouse = new Mouse(Control, MouseCaptureChanged, init);
            }
            _integrator.Update = Control.Update;
            _inst = new Instance();
            _drawJob = new DynamicShared<DynamicDrawJob>(obs =>
            {
                DynamicDrawer2 drawer = _inst.CreateDrawer(obs, _size[obs], new Color(0.5, 0.7, 0.8));
                Control.Draw(obs, drawer);
                return drawer.Finish();
            });
        }

        /// <summary>
        /// The keyboard for this window.
        /// </summary>
        public Keyboard Keyboard { get; }

        /// <summary>
        /// The mouse for this window.
        /// </summary>
        public Mouse Mouse { get; }

        /// <summary>
        /// The control being displayed by this window.
        /// </summary>
        public IControl Control { get; }

        protected override void OnLoad(EventArgs e)
        {
            _start = DateTime.Now;
        }

        protected override void OnResize(EventArgs e)
        {
            using (_integrator.Transact(out Transactor trans))
            {
                _size[trans] = new Vector2i(Width, Height);
            }
        }

        protected override void OnKeyDown(KeyboardKeyEventArgs e)
        {
            if (Keyboard.TryGetKey(e.Key, out Key key))
            {
                using (_integrator.Transact(out Transactor trans))
                {
                    Keyboard.KeyDown(trans, key);
                    if (e.Key == OpenTK.Input.Key.Escape)
                        Mouse.Release(trans);
                }
            }
        }

        protected override void OnKeyUp(KeyboardKeyEventArgs e)
        {
            if (Keyboard.TryGetKey(e.Key, out Key key))
            {
                using (_integrator.Transact(out Transactor trans))
                {
                    Keyboard.KeyUp(trans, key);
                }
            }
        }

        protected override void OnMouseMove(MouseMoveEventArgs e)
        {
            if (!_mouseCaptured)
            {
                using (_integrator.Transact(out Transactor trans))
                {
                    Mouse.Move(trans, new Vector2(e.XDelta, e.YDelta), new Vector2i(e.X, e.Y));
                }
            }
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            using (_integrator.Transact(out Transactor trans))
            {
                Mouse.ButtonDown(trans, e.Button);
            }
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            using (_integrator.Transact(out Transactor trans))
            {
                Mouse.ButtonUp(trans, e.Button);
            }
        }

        private DateTime _lastFpsUpdate;
        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            if ((DateTime.Now - _lastFpsUpdate).TotalSeconds > 0.5)
            {
                Title = $"Alunite {RenderFrequency.ToString("0.00")}";
                _lastFpsUpdate = DateTime.Now;
            }
            if (_mouseCaptured)
            {
                var mouseState = OpenTK.Input.Mouse.GetState();
                var delta = new Vector2i(mouseState.X, mouseState.Y) - _lastMousePos;
                _resetMouse();
                using (_integrator.Transact(out Transactor trans))
                {
                    Mouse.Move(trans, delta, Mouse.Position[trans]);
                }
            }
        }
        
        protected override void OnRenderFrame(OpenTK.FrameEventArgs e)
        {
            GL.Enable(EnableCap.TextureCubeMapSeamless);
            GL.Viewport(0, 0, Width, Height);
            GL.Clear(ClearBufferMask.DepthBufferBit);
            _integrator.Base = Time.FromSeconds((DateTime.Now - _start).TotalSeconds);
            using (_integrator.Observe(_integrator.Base, out Observer obs))
            {
                using (var job = _drawJob.Use(obs))
                {
                    job.Target.Draw(obs);
                }
            }
            SwapBuffers();
        }

        private void _resetMouse()
        {
            OpenTK.Input.Mouse.SetPosition(Bounds.Left + Bounds.Width / 2, Bounds.Top + Bounds.Height / 2);
            var mouseState = OpenTK.Input.Mouse.GetState();
            _lastMousePos = new Vector2i(mouseState.X, mouseState.Y);
        }

        public void MouseCaptureChanged(Transactor trans, bool state)
        {
            if (state)
            {
                _resetMouse();
                _mouseCaptured = true;
                CursorVisible = false;
            }
            else
            {
                var pos = Mouse.Position[trans];
                OpenTK.Input.Mouse.SetPosition(Bounds.Left + pos.X, Bounds.Top + pos.Y);
                _mouseCaptured = false;
                CursorVisible = true;
            }
        }

        IKeyboard IInterface.Keyboard => Keyboard;
    }
}