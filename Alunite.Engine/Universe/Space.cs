﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using Alunite.Reactive;
using Alunite.Data;
using Alunite.Data.Geometry;
using Alunite.Graphics;
using Alunite.Universe.Grids;

namespace Alunite.Universe
{
    /// <summary>
    /// Represents an arbitrarily-shaped connected volume of space within a world whose material contents move around freely and
    /// continuously (e.g. fluids, particles, free-moving entities). This also establishes a frame of reference (not necessarily
    /// inertial) which can be used to address locations within the space. Within a world, there is typically a hiearchy
    /// of <see cref="Space"/>s of varying resolution, as well as significant overlap between adjacent spaces.
    /// </summary>
    public abstract class Space
    {
        /// <summary>
        /// The bounding area for the <see cref="Space"/>, defined within its local coordinate system. All space entities
        /// must be fully contained within this area.
        /// </summary>
        // TODO: Does not necessarily have to be a box
        public abstract Box3 Bounds { get; }

        /// <summary>
        /// Gets the acceleration experienced by a point mass at the given position with the given velocity in this space.
        /// </summary>
        public Vector3 GetAcceleration(Vector3 pos, Vector3 vel)
        {
            // TODO: Non-inertial frames of reference, e.g. gravity
            return Vector3.Zero;
        }

        /// <summary>
        /// Determines the most appropriate <see cref="Space"/> for an entity with the given positive, size and velocity,
        /// attempting to maximize the time that the entity can stay in that space while minimizing the number of
        /// interactions with other entities.
        /// </summary>
        /// <param name="pos">The current position of the entity within this space.</param>
        /// <param name="radius">The radius of the bounding sphere for the entity.</param>
        /// <param name="trans">The transform from this frame of reference to the returned frame of
        /// reference.</param>
        public abstract Space Transfer(Observer obs, Vector3 pos, Scalar radius, out SpaceTransform trans);

        /// <summary>
        /// Finds the smallest argument such that the given ray intersects a surface on the boundary of
        /// same <see cref="Space"/>, returning false if no such argument exists, or it is greater than
        /// <paramref name="maxArg"/>.
        /// </summary>
        public abstract bool Trace(Observer obs, Ray3 ray, Scalar maxArg, out Scalar arg, out SurfacePoint point);

        /// <summary>
        /// Renders the contents of the world outside this <see cref="Space"/> from the perspective of a viewer within it.
        /// </summary>
        public abstract void Render(Observer obs, IRenderer renderer);
    }

    /// <summary>
    /// Describes the relationship between two spaces of reference at a particular moment.
    /// </summary>
    public struct SpaceTransform
    {
        public SpaceTransform(Motion3 spatial)
        {
            Spatial = spatial;
        }

        /// <summary>
        /// The identity transform.
        /// </summary>
        public static SpaceTransform Identity => new SpaceTransform(Motion3.Identity);

        /// <summary>
        /// The component of this transform that applies to spatial coordinates, such as positions.
        /// </summary>
        public Motion3 Spatial;
    }

    /// <summary>
    /// A space which covers a block of 8 equal-sized regions within a <see cref="World"/>, with
    /// the origin of the coordinate system being the central vertex of the regions.
    /// </summary>
    public sealed class WorldSpace : Space
    {
        private readonly _KeyWorldRegion _posX_posY_posZ;
        internal WorldSpace(_KeyWorldRegion posX_posY_posZ)
        {
            _posX_posY_posZ = posX_posY_posZ;
        }

        /// <summary>
        /// The world where this space is defined.
        /// </summary>
        public World World => _posX_posY_posZ.World;

        /// <summary>
        /// The <see cref="Scale"/> of any region in <see cref="Regions"/>.
        /// </summary>
        public int Scale => _posX_posY_posZ.Scale;

        /// <summary>
        /// Gets the octet of regions this space consists of.
        /// </summary>
        public Octet<WorldRegion> Regions
        {
            get
            {
                WorldRegion posX_posY_posZ = _posX_posY_posZ;
                WorldRegion negX_posY_posZ = posX_posY_posZ.GetNeighbor(Dir3.NegX);
                WorldRegion posX_negY_posZ = posX_posY_posZ.GetNeighbor(Dir3.NegY);
                WorldRegion posX_posY_negZ = posX_posY_posZ.GetNeighbor(Dir3.NegZ);
                WorldRegion negX_negY_posZ = negX_posY_posZ.GetNeighbor(Dir3.NegY);
                WorldRegion negX_posY_negZ = negX_posY_posZ.GetNeighbor(Dir3.NegZ);
                WorldRegion posX_negY_negZ = posX_negY_posZ.GetNeighbor(Dir3.NegZ);
                WorldRegion negX_negY_negZ = negX_negY_posZ.GetNeighbor(Dir3.NegZ);
                return new Octet<WorldRegion>(
                    negX_negY_negZ, posX_negY_negZ,
                    negX_posY_negZ, posX_posY_negZ,
                    negX_negY_posZ, posX_negY_posZ,
                    negX_posY_posZ, posX_posY_posZ);
            }
        }

        public override Box3 Bounds
        {
            get
            {
                Scalar size = 1L << _posX_posY_posZ.Scale;

                // Offset space by 0.5 so that they are aligned with chunks.
                Scalar min = -size - 0.5;
                Scalar max = size - 0.5;
                return new Box3(min, max, min, max, min, max);
            }
        }

        public override Space Transfer(Observer obs, Vector3 pos, Scalar radius, out SpaceTransform trans)
        {
            _Transferer transferer = new _Transferer(this, pos);
            transferer.Improve();
            trans = transferer.Transform;
            return transferer.Current;
        }

        /// <summary>
        /// A helper for implementing <see cref="Transfer"/>.
        /// </summary>
        private struct _Transferer
        {
            public _Transferer(WorldSpace space, Vector3 pos)
            {
                Offset_X = 0;
                Offset_Y = 0;
                Offset_Z = 0;
                Region = space._posX_posY_posZ;
                HalfSize = (1L << Region.Scale) / 2.0;
                Position = pos + new Vector3(0.5, 0.5, 0.5);
            }

            /// <summary>
            /// The offset of <see cref="Current"/> from the initial space along the X axis.
            /// </summary>
            public long Offset_X;

            /// <summary>
            /// The offset of <see cref="Current"/> from the initial space along the Y axis.
            /// </summary>
            public long Offset_Y;

            /// <summary>
            /// The offset of <see cref="Current"/> from the initial space along the Z axis.
            /// </summary>
            public long Offset_Z;

            /// <summary>
            /// The defining region for <see cref="Current"/>.
            /// </summary>
            public _KeyWorldRegion Region;

            /// <summary>
            /// The current best space for the transfer.
            /// </summary>
            public WorldSpace Current => Region._space;

            /// <summary>
            /// Half the length of the defining region for <see cref="Current"/>.
            /// </summary>
            public Scalar HalfSize;

            /// <summary>
            /// The position of the transfering object within <see cref="Current"/>, plus a
            /// 0.5 offset for centering.
            /// </summary>
            public Vector3 Position;

            /// <summary>
            /// The transform from the initial space to <see cref="Current"/>.
            /// </summary>
            public SpaceTransform Transform => new SpaceTransform(Motion3.Translate(-Offset_X, -Offset_Y, -Offset_Z));

            /// <summary>
            /// Replaces <see cref="Current"/> with an equal-sized or larger space immediately in
            /// the given direction.
            /// </summary>
            public void Move(Dir3 dir)
            {
                // Keep track of where we're moving
                long dOffset_x = 0;
                long dOffset_y = 0;
                long dOffset_z = 0;
                var axis = dir.GetAxis(out Dir1 adir);
                long d = 1L << Region.Scale;
                if (adir == Dir1.Neg) d = -d;
                if (axis == Axis3.X) dOffset_x = d;
                else if (axis == Axis3.Y) dOffset_y = d;
                else dOffset_z = d;

                // Move to space
                var nRegion = Region.GetNeighbor(dir);
                Region = nRegion._ancestor;
                HalfSize = (1L << Region.Scale) / 2.0;

                // Path correction to offset
                nRegion._path.Locate(out uint pathDepth, out ShortVector3i pathOffset);
                dOffset_x -= pathOffset.X << nRegion.Scale;
                dOffset_y -= pathOffset.Y << nRegion.Scale;
                dOffset_z -= pathOffset.Z << nRegion.Scale;

                // Apply offset
                Offset_X += dOffset_x;
                Offset_Y += dOffset_y;
                Offset_Z += dOffset_z;
                Position.X -= dOffset_x;
                Position.Y -= dOffset_y;
                Position.Z -= dOffset_z;
            }

            /// <summary>
            /// Moves to a space along the X axis which is at least the size of the final space and is
            /// the most suitable space of its scale.
            /// </summary>
            public void Improve_X()
            {
                if (Position.X < -HalfSize)
                {
                    do Move(Dir3.NegX);
                    while (Position.X < -HalfSize);
                }
                else
                {
                    while (Position.X > HalfSize)
                        Move(Dir3.PosX);
                }
            }

            /// <summary>
            /// Moves to a space along the Y axis which is at least the size of the final space and is
            /// the most suitable space of its scale.
            /// </summary>
            public void Improve_Y()
            {
                if (Position.Y < -HalfSize)
                {
                    do Move(Dir3.NegY);
                    while (Position.Y < -HalfSize);
                }
                else
                {
                    while (Position.Y > HalfSize)
                        Move(Dir3.PosY);
                }
            }

            /// <summary>
            /// Moves to a space along the Z axis which is at least the size of the final space and is
            /// the most suitable space of its scale.
            /// </summary>
            public void Improve_Z()
            {
                if (Position.Z < -HalfSize)
                {
                    do Move(Dir3.NegZ);
                    while (Position.Z < -HalfSize);
                }
                else
                {
                    while (Position.Z > HalfSize)
                        Move(Dir3.PosZ);
                }
            }

            /// <summary>
            /// Moves to a space which is at least the size of the final space and is the most suitable
            /// space of its scale.
            /// </summary>
            public void Improve()
            {
                Improve_X();
                Improve_Y();
                Improve_Z();
            }
        }

        public override bool Trace(Observer obs, Ray3 ray, Scalar maxArg, out Scalar arg, out SurfacePoint point)
        {
            World world = World;

            // Find chunk and offset of ray origin
            Vector3i chunkOffset = Vector3i.Zero;
            WorldRegion region = Regions.PosX_PosY_PosZ;
            ShortVector3i offset = (ShortVector3i)Vector3i.Floor(ray.Origin + new Vector3(0.5, 0.5, 0.5));
            chunkOffset += offset;
            short size = (short)(1 << region.Scale);
            if (offset.X < 0)
            {
                region = region.GetNeighbor(Dir3.NegX);
                offset.X += size;
            }
            if (offset.Y < 0)
            {
                region = region.GetNeighbor(Dir3.NegY);
                offset.Y += size;
            }
            if (offset.Z < 0)
            {
                region = region.GetNeighbor(Dir3.NegZ);
                offset.Z += size;
            }
            WorldChunk chunk = world.GetChunk(obs, region, ref offset);
            chunkOffset -= offset;

        traverse:
            // Trace within chunk
            Ray3 chunkRay = ray - chunkOffset;
            if (true)
            {
                throw new NotImplementedException();
            }
            else
            {
                // Traverse to the appropriate neighbor
                size = (short)(1 << chunk._region.Scale);
                Scalar min = -0.5;
                Scalar max = size - 0.5;
                Scalar exitArg = chunkRay.IntersectExit(new Box3(min, max, min, max, min, max), out Dir3 exitDir);
                if (exitArg < maxArg)
                {
                    offset = (ShortVector3i)Vector3i.Floor(chunkRay[exitArg] + new Vector3(0.5, 0.5, 0.5));
                    if (exitDir == Dir3.NegX)
                        offset.X = -1;
                    else if (exitDir == Dir3.PosX)
                        offset.X = size;
                    else if (exitDir == Dir3.NegY)
                        offset.Y = -1;
                    else if (exitDir == Dir3.PosY)
                        offset.Y = size;
                    else if (exitDir == Dir3.NegZ)
                        offset.Z = -1;
                    else // if (exitDir == Dir3.PosZ)
                        offset.Z = size;
                    chunkOffset += offset;
                    offset -= (ShortVector3i)(size * (Vector3i)exitDir);
                    region = chunk.Region.GetNeighbor(exitDir);
                    chunk = world.GetChunk(obs, region, ref offset);
                    chunkOffset -= offset;
                    goto traverse;
                }
            }
            point = default;
            return false;
        }

        /// <summary>
        /// Gets a region the region that contains the given position within this frame, as well as the offset of the
        /// block within the region that contains it.
        /// </summary>
        private WorldRegion _getRegion(Vector3 pos, out ShortVector3i offset)
        {
            WorldRegion region = Regions.PosX_PosY_PosZ;
            offset = (ShortVector3i)Vector3i.Floor(pos + new Vector3(0.5, 0.5, 0.5));
            short size = (short)(1 << (int)region.Scale);
            if (offset.X < 0)
            {
                region = region.GetNeighbor(Dir3.NegX);
                offset.X += size;
            }
            if (offset.Y < 0)
            {
                region = region.GetNeighbor(Dir3.NegY);
                offset.Y += size;
            }
            if (offset.Z < 0)
            {
                region = region.GetNeighbor(Dir3.NegZ);
                offset.Z += size;
            }
            return region;
        }

        public override void Render(Observer obs, IRenderer renderer)
        {
            long size = 1L << Scale;
            var chunkRenderer = new _ChunkRenderer
            {
                World = World,
                Observer = obs,
                Handled = new HashSet<_ChunkRenderer.Region>(),
                Queue = new Queue<_ChunkRenderer.Region_Appearance>(),
                CameraBounds = size
            };
            var regions = Regions;
            chunkRenderer.Enqueue(-size, -size, -size, regions.NegX_NegY_NegZ);
            chunkRenderer.Enqueue(0, -size, -size, regions.PosX_NegY_NegZ);
            chunkRenderer.Enqueue(-size, 0, -size, regions.NegX_PosY_NegZ);
            chunkRenderer.Enqueue(0, 0, -size, regions.PosX_PosY_NegZ);
            chunkRenderer.Enqueue(-size, -size, 0, regions.NegX_NegY_PosZ);
            chunkRenderer.Enqueue(0, -size, 0, regions.PosX_NegY_PosZ);
            chunkRenderer.Enqueue(-size, 0, 0, regions.NegX_PosY_PosZ);
            chunkRenderer.Enqueue(0, 0, 0, regions.PosX_PosY_PosZ);
            chunkRenderer.Render(obs, renderer);
        }

        /// <summary>
        /// A helper for implementing <see cref="WorldSpace.Render"/>.
        /// </summary>
        private struct _ChunkRenderer
        {
            /// <summary>
            /// The world being rendered.
            /// </summary>
            public World World;

            /// <summary>
            /// The observer for the render.
            /// </summary>
            public Observer Observer;

            /// <summary>
            /// The set of <see cref="Region"/>s that have been rendered or queued for rendering.
            /// </summary>
            public HashSet<Region> Handled;

            /// <summary>
            /// A queue of <see cref="Region"/> to be rendered.
            /// </summary>
            public Queue<Region_Appearance> Queue;

            /// <summary>
            /// The maximum distance, along any axis, that the camera can be placed from the origin for this render.
            /// </summary>
            public long CameraBounds;

            /// <summary>
            /// The square of the maximum camera distance allowed for rendered regions.
            /// </summary>
            public const long MaxSquareDistance = 120 * 120;

            /// <summary>
            /// Queues the <see cref="Region"/>s that need to be handled in order to render
            /// the given region, assuming it is visible.
            /// </summary>
            public void Enqueue(
                long offset_x, long offset_y, long offset_z,
                WorldRegion region)
            {
                long size = 1L << region.Scale;
                long dist_x = Math.Max(Math.Max(-offset_x, offset_x + size) - (size + CameraBounds), 0);
                long dist_y = Math.Max(Math.Max(-offset_y, offset_y + size) - (size + CameraBounds), 0);
                long dist_z = Math.Max(Math.Max(-offset_z, offset_z + size) - (size + CameraBounds), 0);
                long sqrDist = dist_x * dist_x + dist_y * dist_y + dist_z * dist_z;
                if (sqrDist < MaxSquareDistance)
                {
                    if (World.TryGetChunk(Observer, region, out WorldChunk chunk, out ShortOctantList path))
                    {
                        path.Locate(out uint depth, out ShortVector3i offset);
                        var renderRegion = new Region
                        {
                            Offset_X = offset_x - ((long)offset.X << region.Scale),
                            Offset_Y = offset_y - ((long)offset.Y << region.Scale),
                            Offset_Z = offset_z - ((long)offset.Z << region.Scale),
                            Source = chunk.Region
                        };
                        if (Handled.Add(renderRegion))
                            Queue.Enqueue(new Region_Appearance(renderRegion, chunk));
                    }
                    else
                    {
                        // Queue child regions
                        throw new NotImplementedException();
                    }
                }
            }
            
            /// <summary>
            /// Processes all queued <see cref="Region"/>s until there are none left.
            /// </summary>
            public void Render(Observer obs, IRenderer renderer)
            {
                while (Queue.Count > 0)
                {
                    var region_appr = Queue.Dequeue();
                    var region = region_appr.Region;

                    // Render region
                    region_appr.Render(obs, renderer);

                    // Queue visible neighboring regions
                    long size = 1L << region.Source.Scale;
                    if (region.Offset_X < CameraBounds)
                    {
                        var neighbor = region.Source.GetNeighbor(Dir3.NegX);
                        Enqueue(region.Offset_X - size, region.Offset_Y, region.Offset_Z, neighbor);
                    }
                    if (region.Offset_X + size > 0)
                    {
                        var neighbor = region.Source.GetNeighbor(Dir3.PosX);
                        Enqueue(region.Offset_X + size, region.Offset_Y, region.Offset_Z, neighbor);
                    }
                    if (region.Offset_Y < CameraBounds)
                    {
                        var neighbor = region.Source.GetNeighbor(Dir3.NegY);
                        Enqueue(region.Offset_X, region.Offset_Y - size, region.Offset_Z, neighbor);
                    }
                    if (region.Offset_Y + size > 0)
                    {
                        var neighbor = region.Source.GetNeighbor(Dir3.PosY);
                        Enqueue(region.Offset_X, region.Offset_Y + size, region.Offset_Z, neighbor);
                    }
                    if (region.Offset_Z < CameraBounds)
                    {
                        var neighbor = region.Source.GetNeighbor(Dir3.NegZ);
                        Enqueue(region.Offset_X, region.Offset_Y, region.Offset_Z - size, neighbor);
                    }
                    if (region.Offset_Z + size > 0)
                    {
                        var neighbor = region.Source.GetNeighbor(Dir3.PosZ);
                        Enqueue(region.Offset_X, region.Offset_Y, region.Offset_Z + size, neighbor);
                    }
                }
            }

            /// <summary>
            /// Identifies a <see cref="WorldRegion"/> to be rendered, along with its location in the
            /// render space. Note that a single <see cref="WorldRegion"/> can be involved in
            /// a render multiple times when non-Euclidean world topologies are involved.
            /// </summary>
            public struct Region : IEquatable<Region>
            {
                /// <summary>
                /// The apparent offset of the negative corner of this region from the negative corner of the bounds
                /// of the camera for the render, along the X axis.
                /// </summary>
                public long Offset_X;

                /// <summary>
                /// The apparent offset of the negative corner of this region from the negative corner of the bounds
                /// of the camera for the render, along the X axis.
                /// </summary>
                public long Offset_Y;

                /// <summary>
                /// The apparent offset of the negative corner of this region from the negative corner of the bounds
                /// of the camera for the render, along the X axis.
                /// </summary>
                public long Offset_Z;

                /// <summary>
                /// The <see cref="WorldRegion"/> to be rendered.
                /// </summary>
                public WorldRegion Source;

                public static bool operator ==(Region a, Region b)
                {
                    return
                        a.Source == b.Source &&
                        a.Offset_X == b.Offset_X &&
                        a.Offset_Y == b.Offset_Y &&
                        a.Offset_Z == b.Offset_Z;
                }

                public static bool operator !=(Region a, Region b)
                {
                    return !(a == b);
                }

                public override bool Equals(object obj)
                {
                    if (!(obj is Region))
                        return false;
                    return this == (Region)obj;
                }

                bool IEquatable<Region>.Equals(Region other)
                {
                    return this == other;
                }

                public override int GetHashCode()
                {
                    return HashCodeHelper.Combine(
                        Source.GetHashCode(),
                        Offset_X.GetHashCode(),
                        Offset_Y.GetHashCode(),
                        Offset_Z.GetHashCode());
                }
            }

            /// <summary>
            /// Combines a <see cref="Region"/> with information about how it appears in a render.
            /// </summary>
            public struct Region_Appearance
            {
                public Region_Appearance(Region region, WorldChunk chunk)
                {
                    Region = region;
                    Chunk = chunk;
                }

                /// <summary>
                /// The region component of this <see cref="Region_Appearance"/>.
                /// </summary>
                public Region Region;

                /// <summary>
                /// The chunk that <see cref="Region"/> is displayed as.
                /// </summary>
                public WorldChunk Chunk;

                /// <summary>
                /// Renders this region.
                /// </summary>
                public void Render(Observer obs, IRenderer renderer)
                {
                    if (Chunk != null)
                    {
                        int scale = Region.Source.Scale;
                        Vector3i offset = new Vector3i(
                            (int)Region.Offset_X,
                            (int)Region.Offset_Y,
                            (int)Region.Offset_Z);
                        Chunk.Render(obs, renderer, Motion3.Translate(offset));
                        /* new Annotator3(drawer, camera)
                            .Box(new Color(1, 0, 0), 2, offset + new Box3i(
                                0, 1 << scale,
                                0, 1 << scale,
                                0, 1 << scale)); */
                    }
                }
            }
        }
    }
}