﻿using System;
using System.Runtime.InteropServices;

using Alunite.Data;
using Alunite.Data.Geometry;

namespace Alunite.Universe
{
    /// <summary>
    /// Encapsulates the ubiquitous and immutable facts about a <see cref="World"/>.
    /// </summary>
    public sealed class WorldCharter
    {
        public WorldCharter(WorldTopology topology, int scale)
        {
            Topology = topology;
            Scale = scale;
        }

        /// <summary>
        /// The topology of the world.
        /// </summary>
        public WorldTopology Topology { get; }

        /// <summary>
        /// The log-base-2 of the length of a <see cref="WorldTopology.Region"/> with respect to the world's base unit.
        /// </summary>
        public int Scale { get; }
    }

    /// <summary>
    /// Describes the topology of a <see cref="World"/> by partitioning it into equal-sized cubical regions of
    /// Euclidean space and specifying how these regions are connected.
    /// </summary>
    public struct WorldTopology
    {
        public WorldTopology(params AxialDuet3<Region>[] neighbors)
            : this(List.Of(neighbors))
        { }

        public WorldTopology(List<AxialDuet3<Region>> neighbors)
        {
            // TODO: Verify connectivity
            Neighbors = neighbors;
        }

        /// <summary>
        /// The connectivity table for this topology. This specifies the neighbors of each region in the origin
        /// "basic unit" of the topology. The basic unit may be repeated in the X, Y and/or Z directions.
        /// </summary>
        public List<AxialDuet3<Region>> Neighbors { get; }

        /// <summary>
        /// Identifies one of the root regions in a <see cref="WorldTopology"/>.
        /// </summary>
        [StructLayout(LayoutKind.Explicit)]
        public struct Region : IEquatable<Region>
        {
            [FieldOffset(0)] private ulong _def;
            public Region(byte index, short x, short y, short z)
            {
                _def = default;
                Index = index;
                X = x;
                Y = y;
                Z = z;
            }

            /// <summary>
            /// Identifies which of the regions in the basic unit of the topology this region corresponds to.
            /// </summary>
            [FieldOffset(0)] public byte Index;

            /// <summary>
            /// Identifies which X-repetition of the basic unit this region belongs to.
            /// </summary>
            [FieldOffset(2)] public short X;

            /// <summary>
            /// Identifies which Y-repetition of the basic unit this region belongs to.
            /// </summary>
            [FieldOffset(4)] public short Y;

            /// <summary>
            /// Identifies which Z-repetition of the basic unit this region belongs to.
            /// </summary>
            [FieldOffset(6)] public short Z;

            public static bool operator ==(Region a, Region b)
            {
                return a._def == b._def;
            }

            public static bool operator !=(Region a, Region b)
            {
                return !(a == b);
            }

            public override bool Equals(object obj)
            {
                if (!(obj is Region))
                    return false;
                return this == (Region)obj;
            }
            
            bool IEquatable<Region>.Equals(Region other)
            {
                return this == other;
            }

            public override int GetHashCode()
            {
                return _def.GetHashCode();
            }
        }

        /// <summary>
        /// Gets the region adjacent to the given region in the given direction.
        /// </summary>
        public Region GetNeighbor(Region region, Dir3 dir)
        {
            Region basicNeighbor = Neighbors[region.Index][dir];
            return new Region(basicNeighbor.Index,
                (short)(region.X + basicNeighbor.X),
                (short)(region.Y + basicNeighbor.Y),
                (short)(region.Z + basicNeighbor.Z));
        }
    }
}
