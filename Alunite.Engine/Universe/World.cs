﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;

using Alunite.Data;
using Alunite.Data.Geometry;
using Alunite.Data.Probability;
using Alunite.Reactive;
using Alunite.Universe.Grids;
using Alunite.Universe.Generation;

namespace Alunite.Universe
{
    /// <summary>
    /// Describes the shape and composition of matter covering an entire <see cref="WorldTopology"/> over time.
    /// </summary>
    public sealed class World : IActor
    {
        internal StateBag<WorldChunk> _chunks;
        public World(Transactor init, WorldPrototype prototype)
        {
            Charter = prototype.Charter;
            Assembly = prototype.Assembly;
            Seed = prototype.Seed;
            _chunks = new StateBag<WorldChunk>(init);
            _rootRegions = new LookupDictionary<WorldTopology.Region, _KeyWorldRegion>();
            _rootRegionFactory = source => new _KeyWorldRegion(this, source, Assembly.Enter(Seed, source));
            _excessChunks = Chunks.Iterate(_putChunk);
        }

        /// <summary>
        /// Encapsulates the ubiquitous and immutable facts about this <see cref="World"/>. 
        /// </summary>
        public WorldCharter Charter { get; }

        /// <summary>
        /// The <see cref="UglAssembly"/> used to generate the contents of this <see cref="World"/>. This may be null if
        /// the contents of the world are already fully specified by <see cref="Chunks"/>.
        /// </summary>
        public UglAssembly Assembly { get; }

        /// <summary>
        /// The random seed for the world. This is only applicable if <see cref="Assembly"/> is not null.
        /// </summary>
        public Seed Seed { get; }

        /// <summary>
        /// The set of explicitly-defined chunks for this world. These describe where and how the world differs from its
        /// implied "baseline" configuration.
        /// </summary>
        public DynamicBag<WorldChunk> Chunks => _chunks;

        /// <summary>
        /// The set of <see cref="_KeyWorldRegion"/>s corresponding to the root regions of the
        /// <see cref="WorldTopology"/> for the world.
        /// </summary>
        internal readonly LookupDictionary<WorldTopology.Region, _KeyWorldRegion> _rootRegions;

        /// <summary>
        /// The factory function for <see cref="_rootRegions"/>.
        /// </summary>
        internal readonly Func<WorldTopology.Region, _KeyWorldRegion> _rootRegionFactory;

        /// <summary>
        /// Represents the set of non-prototype <see cref="WorldChunk"/>s in <see cref="World.Chunks"/>
        /// that are not yet accounted for in the <see cref="WorldRegion"/>s for this world.
        /// </summary>
        private readonly DynamicExcess _excessChunks;
        
        /// <summary>
        /// Gets the <see cref="WorldRegion"/> representation of the given topology region.
        /// </summary>
        public WorldRegion GetRegion(WorldTopology.Region source)
        {
            return _getRegion(source);
        }

        /// <summary>
        /// Gets the <see cref="_KeyWorldRegion"/> representation of the given topology region.
        /// </summary>
        internal _KeyWorldRegion _getRegion(WorldTopology.Region source)
        {
            return _rootRegions.GetOrAdd(source, _rootRegionFactory);
        }

        /// <summary>
        /// Gets a <see cref="WorldSpace"/> which contains the given region.
        /// </summary>
        public WorldSpace GetSpace(WorldRegion region)
        {
            // TODO: Deprecate, arbitrary caller shouldn't be able to force the creation of key regions
            var keyRegion = region._toKeyRegion();
            Debug.Assert(keyRegion._space.World == this);
            return keyRegion._space;
        }

        /// <summary>
        /// Tries to get a <see cref="WorldChunk"/> describing the static grid-aligned contents of the
        /// given region, returning false if such content is defined at a lower level in the region hiearchy
        /// (i.e. descendants of this region). Note that this may return a chunk at a higher level in the
        /// region hiearchy.
        /// </summary>
        /// <param name="request">The region where content is requested.</param>
        /// <param name="path">The path from <paramref name="chunk"/>'s region to <paramref name="request"/>.</param>
        public bool TryGetChunk(Observer obs, 
            WorldRegion request,
            out WorldChunk chunk,
            out ShortOctantList path)
        {
            // TODO: Move to WorldRegion

            // TODO: Clear at region level using SubsetDynamicExcess, for performance
            // Make sure there are no new chunks that have not yet been accounted for
            _excessChunks.Clear(obs);

            _KeyWorldRegion keyRequest = request._ancestor;
            path = request._path;
        retry:
            Octant octant;
            chunk = Volatile.Read(ref keyRequest._prototypeChunk);
            if (chunk == null)
            {
                // Check if content is available from parent
                if (keyRequest.TryGetParent(out _KeyWorldRegion nKeyRequest, out octant))
                {
                    keyRequest = nKeyRequest;
                    bool hasUnshifted = ShortOctantList.TryPrepend(ref path, octant);
                    Debug.Assert(hasUnshifted);
                    goto retry;
                }

                // Build region content
                goto build;
            }
            
        traverse:
            // Traverse down to the requested region
            Debug.Assert(chunk != null);
            foreach (var stateChunk in keyRequest._chunks[obs]) // TODO: Thread safety
            {
                chunk = stateChunk;
                break;
            }
            if (chunk != WorldChunk._down)
                return true;
            if (ShortOctantList.TryUnprepend(ref path, out octant))
            {
                keyRequest = keyRequest._getChildStrict(octant);
                chunk = Volatile.Read(ref keyRequest._prototypeChunk);
                if (chunk == null)
                    goto build;
                else
                    goto traverse;
            }
            return false;

        build:
            // Build content at this level
            // TODO: This assumes all chunks are the same scale. Generalize to other cases
            Octet<_KeyWorldRegion> regions = default;
            regions.PosX_PosY_PosZ = keyRequest;
            regions.NegX_PosY_PosZ = regions.PosX_PosY_PosZ.GetNeighbor(Dir3.NegX)._toKeyRegion();
            regions.PosX_NegY_PosZ = regions.PosX_PosY_PosZ.GetNeighbor(Dir3.NegY)._toKeyRegion();
            regions.NegX_NegY_PosZ = regions.PosX_NegY_PosZ.GetNeighbor(Dir3.NegX)._toKeyRegion();
            regions.PosX_PosY_NegZ = regions.PosX_PosY_PosZ.GetNeighbor(Dir3.NegZ)._toKeyRegion();
            regions.NegX_PosY_NegZ = regions.PosX_PosY_NegZ.GetNeighbor(Dir3.NegX)._toKeyRegion();
            regions.PosX_NegY_NegZ = regions.PosX_PosY_NegZ.GetNeighbor(Dir3.NegY)._toKeyRegion();
            regions.NegX_NegY_NegZ = regions.PosX_NegY_NegZ.GetNeighbor(Dir3.NegX)._toKeyRegion();
            var sourceChunk = Assembly.Chunk(new Octet<UglAssembly.Frame>(
                regions.NegX_NegY_NegZ._frame,
                regions.PosX_NegY_NegZ._frame,
                regions.NegX_PosY_NegZ._frame,
                regions.PosX_PosY_NegZ._frame,
                regions.NegX_NegY_PosZ._frame,
                regions.PosX_NegY_PosZ._frame,
                regions.NegX_PosY_PosZ._frame,
                regions.PosX_PosY_PosZ._frame));
            chunk = new WorldChunk(keyRequest, sourceChunk);
            keyRequest._prototypeChunk = chunk; // TODO: Thread safety
            return true;
        }

        /// <summary>
        /// Gets a <see cref="WorldChunk"/> describing the static grid-aligned contents of a block within the
        /// given region.
        /// </summary>
        /// <param name="region">The region in which <paramref name="offset"/> is defined.</param>
        /// <param name="offset">Initially set to the offset of the block within <paramref name="region"/>. Returns the
        /// offset of the block within the returned chunk.</param>
        public WorldChunk GetChunk(Observer obs, WorldRegion region, ref ShortVector3i offset)
        {
            uint depth = (uint)region.Scale;
            region = region.GetDescendant(ShortOctantList.Pick(depth, offset));
            bool hasChunk = TryGetChunk(obs, region, out var chunk, out ShortOctantList path);
            Debug.Assert(hasChunk);
            path.Locate(out depth, out offset);
            Debug.Assert(chunk.Scale == depth);
            return chunk;
        }

        /// <summary>
        /// Adds a chunk definition from <see cref="Chunks"/> to the appropriate region(s), allowing it to
        /// be found using <see cref="TryGetChunk"/>.
        /// </summary>
        internal void _putChunk(DynamicItem<WorldChunk> item)
        {
            // TODO: Thread safety
            // TODO: Inform ancestor and descendant regions of new chunk
            var chunk = item.Value;
            var region = chunk._region;
            region._chunks.Add(item);
        }

        /// <summary>
        /// Updates this world in response to the passage of time.
        /// </summary>
        public void Update(Transactor trans)
        {

        }
    }

    /// <summary>
    /// Describes the shape and composition of matter covering an entire <see cref="WorldTopology"/> at
    /// a particular moment.
    /// </summary>
    public sealed class WorldPrototype
    {
        public WorldPrototype(WorldCharter charter, UglAssembly assem, Seed seed)
        {
            Charter = charter;
            Assembly = assem;
            Seed = seed;
        }

        /// <summary>
        /// Encapsulates the ubiquitous and immutable facts about the generated <see cref="World"/>.
        /// </summary>
        public WorldCharter Charter { get; }

        /// <summary>
        /// The <see cref="UglAssembly"/> used to generate the contents of this world.
        /// </summary>
        public UglAssembly Assembly { get; }

        /// <summary>
        /// The random seed for the world.
        /// </summary>
        public Seed Seed { get; }

        /// <summary>
        /// Constructs a new <see cref="World"/> whose initial contents are described by this
        /// prototype.
        /// </summary>
        public World Instantiate(Transactor init)
        {
            return new World(init, this);
        }
    }
}