﻿using System;
using System.Collections.Generic;

namespace Alunite.Universe
{
    /// <summary>
    /// Identifies a point on a material surface that forms the boundary of a <see cref="Space"/>.
    /// </summary>
    public abstract class SurfacePoint
    {

    }
}
