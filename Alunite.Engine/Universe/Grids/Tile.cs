﻿using System;
using System.Collections.Generic;

using Alunite.Data;
using Alunite.Data.Geometry;

namespace Alunite.Universe.Grids
{
    /// <summary>
    /// Describes the shape and composition of grid-aligned matter in a cubic volume of length 2 (grid units) centered
    /// around a grid vertex, excluding the matter in the vicinity of the faces of the cube. This is the primitive
    /// unit of  grid-aligned matter; the complete contents of a grid can be described by a tessellation of
    /// <see cref="Tile"/>s. Since there is some overlap between adjacent tiles, only tiles that agree on content
    /// in overlapping regions (i.e. are "compatible") may be placed next to each other.
    /// </summary>
    public struct Tile
    {
        public static bool operator ==(Tile a, Tile b)
        {
            return true;
        }

        public static bool operator !=(Tile a, Tile b)
        {
            return false;
        }
    }
}
