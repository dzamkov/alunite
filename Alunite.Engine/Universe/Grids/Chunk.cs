﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using Alunite.Data;
using Alunite.Data.Geometry;
using Alunite.Reactive;

namespace Alunite.Universe.Grids
{
    /// <summary>
    /// Represents a collection of static grid-aligned matter occupying a cubic region of a power-of-two size
    /// (in terms of grid units).
    /// </summary>
    public abstract class Chunk
    {
        public Chunk(ChunkPrototype prototype)
        {
            Prototype = prototype;
        }

        /// <summary>
        /// The <see cref="ChunkPrototype"/> describing the contents of this chunk for its entire lifetime.
        /// </summary>
        public ChunkPrototype Prototype { get; }

        /// <summary>
        /// The log-base-2 of the length of this chunk along any axis.
        /// </summary>
        public uint Scale => Prototype.Scale;

        /// <summary>
        /// The maximum possible value of <see cref="Scale"/> for any chunk.
        /// </summary>
        public const uint MaxScale = 14;
    }
    
    /// <summary>
    /// Describes the shape and composition of matter within a <see cref="Chunk"/> at a particular moment.
    /// </summary>
    public struct ChunkPrototype
    {
        public ChunkPrototype(OctreeGrid<Tile> tiles)
        {
            Tiles = tiles;
        }

        /// <summary>
        /// The tile grid defining the chunk.
        /// </summary>
        public OctreeGrid<Tile> Tiles { get; }

        /// <summary>
        /// The log-base-2 of the length of this chunk along any axis.
        /// </summary>
        public uint Scale => Tiles.Scale;
    }
}