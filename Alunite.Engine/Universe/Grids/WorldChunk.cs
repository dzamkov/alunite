﻿using System;
using System.Diagnostics;
using System.Threading;

using Alunite.Data;
using Alunite.Data.Geometry;
using Alunite.Reactive;
using Alunite.Graphics;

namespace Alunite.Universe.Grids
{
    /// <summary>
    /// A <see cref="Chunk"/> occupying a region within a <see cref="World"/>.
    /// </summary>
    public sealed class WorldChunk : Chunk
    {
        internal readonly _KeyWorldRegion _region;
        internal StateBag<WorldChunk>.ItemRef _itemRef;
        private WorldChunk() : base(default) { }
        internal WorldChunk(_KeyWorldRegion region, ChunkPrototype source)
            : base(source)
        {
            _region = region;
        }

        /// <summary>
        /// The region this chunk is for.
        /// </summary>
        public WorldRegion Region => _region;

        /// <summary>
        /// A sentinel value of <see cref="WorldChunk"/> which indicates that content is defined at
        /// a lower level in the region hiearchy.
        /// </summary>
        internal static readonly WorldChunk _down = new WorldChunk();

        /// <summary>
        /// Renders the static chunk described by this <see cref="WorldChunk"/>.
        /// </summary>
        public void Render(Observer obs, IRenderer renderer, Motion3 transform)
        {
            throw new NotImplementedException();
        }
    }
}
