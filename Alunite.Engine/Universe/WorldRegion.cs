﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Runtime.InteropServices;

using Alunite.Data;
using Alunite.Data.Geometry;
using Alunite.Reactive;
using Alunite.Universe.Grids;
using Alunite.Universe.Generation;

namespace Alunite.Universe
{
    /// <summary>
    /// Identifies a region within a <see cref="Universe.World"/> while also organizing the contents within it.
    /// </summary>
    public struct WorldRegion : IEquatable<WorldRegion>
    {
        internal WorldRegion(_KeyWorldRegion ancestor, ShortOctantList path)
        {
            _ancestor = ancestor;
            _path = path;
        }

        /// <summary>
        /// Some ancestor <see cref="_KeyWorldRegion"/> of this <see cref="WorldRegion"/>.
        /// </summary>
        internal _KeyWorldRegion _ancestor;

        /// <summary>
        /// The octant selection path from <see cref="_ancestor"/> to this region.
        /// </summary>
        internal ShortOctantList _path;

        /// <summary>
        /// The log-base-2 of the size of this region.
        /// </summary>
        public int Scale => _ancestor.Scale - (int)_path.Size;

        /// <summary>
        /// The <see cref="World"/> this region belongs to.
        /// </summary>
        public World World => _ancestor.World;

        /// <summary>
        /// Gets the equal-size neighboring region of this region in the given direction.
        /// </summary>
        public WorldRegion GetNeighbor(Dir3 dir)
        {
            // Attempt to update the path for the region
            _path.Locate(out uint depth, out ShortVector3i offset);
            short size = (short)(1 << (int)depth);
            switch (dir)
            {
                case Dir3.NegX:
                    offset.X--;
                    if (offset.X < 0)
                    {
                        offset.X += size;
                        goto traverse;
                    }
                    break;
                case Dir3.PosX:
                    offset.X++;
                    if (offset.X >= size)
                    {
                        offset.X -= size;
                        goto traverse;
                    }
                    break;
                case Dir3.NegY:
                    offset.Y--;
                    if (offset.Y < 0)
                    {
                        offset.Y += size;
                        goto traverse;
                    }
                    break;
                case Dir3.PosY:
                    offset.Y++;
                    if (offset.Y >= size)
                    {
                        offset.Y -= size;
                        goto traverse;
                    }
                    break;
                case Dir3.NegZ:
                    offset.Z--;
                    if (offset.Z < 0)
                    {
                        offset.Z += size;
                        goto traverse;
                    }
                    break;
                case Dir3.PosZ:
                    offset.Z++;
                    if (offset.Z >= size)
                    {
                        offset.Z -= size;
                        goto traverse;
                    }
                    break;
            }
            return new WorldRegion(_ancestor, ShortOctantList.Pick(depth, offset));

        traverse:
            // Traverses key world regions
            WorldRegion nRegion = _ancestor.GetNeighbor(dir);
            return nRegion.GetDescendant(ShortOctantList.Pick(depth, offset));
        }

        /// <summary>
        /// Gets the child of the this region occupying the given octant.
        /// </summary>
        public WorldRegion GetChild(Octant octant)
        {
            _KeyWorldRegion ancestor = _ancestor;
            ShortOctantList path = _path;
            while (!ShortOctantList.TryAppend(ref path, octant))
                ancestor = ancestor._getChildStrict(ShortOctantList.Unprepend(ref path));
            return new WorldRegion(_ancestor, path);
        }

        /// <summary>
        /// Gets the descendant of this region corresponding to the given octant path.
        /// </summary>
        public WorldRegion GetDescendant(ShortOctantList path)
        {
            _KeyWorldRegion ancestor = _ancestor;
            ShortOctantList nPath = _path;
            while (!ShortOctantList.TryConcat(ref nPath, path))
                ancestor = ancestor._getChildStrict(ShortOctantList.Unprepend(ref nPath));
            return new WorldRegion(ancestor, nPath);
        }

        /// <summary>
        /// Gets the <paramref name="depth"/>-level descendant of this region at the given offset (in terms of
        /// descendant-sized regions) from the descendant in the negative corner of the region.
        /// </summary>
        public WorldRegion GetDescendant(uint depth, ShortVector3i offset)
        {
            return GetDescendant(ShortOctantList.Pick(depth, offset));
        }

        /// <summary>
        /// Determines whether this region is part of a larger region of Euclidean space, and if so,
        /// returns that region, along with the relationship this region has to that region. This
        /// is the inverse of <see cref="GetChild"/>.
        /// </summary>
        public bool TryGetParent(out WorldRegion parent, out Octant octant)
        {
            ShortOctantList path = _path;
            if (ShortOctantList.TryUnprepend(ref path, out octant))
                parent = new WorldRegion(_ancestor, path);
            if (_ancestor.TryGetParent(out parent._ancestor, out octant))
            {
                parent._path = ShortOctantList.Empty;
                return true;
            }
            parent._path = default;
            return false;
        }

        /// <summary>
        /// Determines whether there is a <see cref="WorldRegion"/> which fully contains both of the given regions,
        /// and if so, returns that region.
        /// </summary>
        public static bool TryGetCommonAncestor(
            WorldRegion a, WorldRegion b,
            out WorldRegion parent)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets or creates the <see cref="_KeyWorldRegion"/> corresponding to this region.
        /// </summary>
        internal _KeyWorldRegion _toKeyRegion()
        {
            if (_path.IsEmpty)
                return _ancestor;
            _KeyWorldRegion keyRegion = _ancestor;
            foreach (Octant octant in _path)
                keyRegion = keyRegion._getChildStrict(octant);
            return keyRegion;
        }

        /// <summary>
        /// Gets the normalized form of this <see cref="WorldRegion"/>. The returned ancestor will have the
        /// maximimum possible scale.
        /// </summary>
        internal void _normalize(out _KeyWorldRegion ancestor, out ShortOctantList path)
        {
            ancestor = _ancestor;
            path = _path;
            while (ancestor.TryGetParent(out _KeyWorldRegion nAncestor, out Octant octant))
            {
                if (ShortOctantList.TryPrepend(ref path, octant))
                    ancestor = nAncestor;
                else
                    break;
            }
        }

        public static bool operator ==(WorldRegion a, WorldRegion b)
        {
            a._normalize(out _KeyWorldRegion aAncestor, out ShortOctantList aPath);
            b._normalize(out _KeyWorldRegion bAncestor, out ShortOctantList bPath);
            return aAncestor == bAncestor && aPath == bPath;
        }

        public static bool operator !=(WorldRegion a, WorldRegion b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is WorldRegion))
                return false;
            return this == (WorldRegion)obj;
        }

        bool IEquatable<WorldRegion>.Equals(WorldRegion other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            _normalize(out _KeyWorldRegion ancestor, out ShortOctantList path);
            return HashCodeHelper.Combine(ancestor.GetHashCode(), path.GetHashCode());
        }
    }

    /// <summary>
    /// A <see cref="WorldRegion"/> in which organizational data may be stored.
    /// </summary>
    /// <remarks>Some <see cref="WorldRegion"/>s used for space traversal do not have any content, so it would
    /// be wasteful to allocate an object for them.</remarks>
    internal sealed class _KeyWorldRegion
    {
        internal readonly WeakReference<_KeyWorldRegion> _weakThis;
        internal readonly WorldSpace _space;
        private readonly _Location _loc;
        private Octet<WeakReference<_KeyWorldRegion>> _children;
        private AxialDuet3<WeakReference<_KeyWorldRegion>> _neighbors;
        internal UglAssembly.Frame _frame;
        private _KeyWorldRegion(int scale, _Location loc)
        {
            _weakThis = new WeakReference<_KeyWorldRegion>(this);
            _space = new WorldSpace(this);
            _loc = loc;
            Scale = scale;
            if (scale > Chunk.MaxScale)
                _prototypeChunk = WorldChunk._down;
            _chunks = DynamicBagBuilder<WorldChunk>.Empty;
        }

        internal _KeyWorldRegion(World world, WorldTopology.Region source, UglAssembly.Frame frame)
            : this(world.Charter.Scale, new _Location(world, source))
        {
            _frame = frame;
        }

        internal _KeyWorldRegion(_KeyWorldRegion parent, Octant octant)
            : this(parent.Scale - 1, new _Location(parent, octant))
        { }

        /// <summary>
        /// Describes the contents of this region in the baseline configuration of the world (as defined by
        /// <see cref="World.Prototype"/>), if available.  For any given spatial area, this will only be defined
        /// in exactly one of the <see cref="_KeyWorldRegion"/>s that overlap it. A value of null indicates that
        /// either this hasn't been computed yet, or it is available at some higher level in the region hiearchy
        /// (i.e. the ancestors of this region). A value of <see cref="WorldChunk._down"/> indicates that chunk information
        /// is defined at a lower level in the region hiearchy.
        /// </summary>
        internal WorldChunk _prototypeChunk;

        /// <summary>
        /// A bag of chunks that describe the contents of this region for their respective lifetimes.
        /// </summary>
        internal DynamicBagBuilder<WorldChunk> _chunks;

        /// <summary>
        /// The <see cref="Universe.World"/> this region belongs to.
        /// </summary>
        public World World => _loc.World; // TODO: Make into a backed property

        /// <summary>
        /// The log-base-2 of the size of this region.
        /// </summary>
        public int Scale { get; }

        /// <summary>
        /// Describes the location of a <see cref="_KeyWorldRegion"/>.
        /// </summary>
        [StructLayout(LayoutKind.Explicit)]
        private struct _Location
        {
            [FieldOffset(0)] private Octant _octant;
            [FieldOffset(0)] private WorldTopology.Region _root;
            [FieldOffset(8)] private object _parent;
            public _Location(World world, WorldTopology.Region root)
            {
                _octant = default;
                _root = root;
                _parent = world;
            }

            public _Location(_KeyWorldRegion parent, Octant octant)
            {
                _root = default;
                _octant = octant;
                _parent = parent;
            }

            /// <summary>
            /// Constructs a <see cref="_Location"/> for a root region.
            /// </summary>
            public static _Location Root(World world, WorldTopology.Region root)
            {
                return new _Location
                {
                    _parent = world,
                    _root = root
                };
            }

            /// <summary>
            /// Constructs a <see cref="_Location"/> for a child region.
            /// </summary>
            public static _Location Child(_KeyWorldRegion parent, Octant octant)
            {
                return new _Location
                {
                    _parent = parent,
                    _octant = octant
                };
            }

            /// <summary>
            /// Determines whether this is a <see cref="Root"/> location.
            /// </summary>
            public bool IsRoot(out World world, out WorldTopology.Region root)
            {
                world = _parent as World;
                root = _root;
                return world != null;
            }

            /// <summary>
            /// Determines whether this is a <see cref="Child"/> location.
            /// </summary>
            public bool IsChild(out _KeyWorldRegion parent, out Octant octant)
            {
                parent = _parent as _KeyWorldRegion;
                octant = _octant;
                return parent != null;
            }

            /// <summary>
            /// The world the region is in.
            /// </summary>
            public World World
            {
                get
                {
                    object cur = _parent;
                    while (cur is _KeyWorldRegion region)
                        cur = region._loc._parent;
                    return (World)cur;
                }
            }
        }

        /// <summary>
        /// Gets a reference to the location used to store the given child of this region.
        /// </summary>
        internal ref WeakReference<_KeyWorldRegion> _child(Octant octant)
        {
            switch (octant)
            {
                case Octant.NegX_NegY_NegZ: return ref _children.NegX_NegY_NegZ;
                case Octant.PosX_NegY_NegZ: return ref _children.PosX_NegY_NegZ;
                case Octant.NegX_PosY_NegZ: return ref _children.NegX_PosY_NegZ;
                case Octant.PosX_PosY_NegZ: return ref _children.PosX_PosY_NegZ;
                case Octant.NegX_NegY_PosZ: return ref _children.NegX_NegY_PosZ;
                case Octant.PosX_NegY_PosZ: return ref _children.PosX_NegY_PosZ;
                case Octant.NegX_PosY_PosZ: return ref _children.NegX_PosY_PosZ;
                case Octant.PosX_PosY_PosZ: return ref _children.PosX_PosY_PosZ;
                default: throw new ArgumentException();
            }
        }

        /// <summary>
        /// Gets a reference to the location used to store the given neighbor of this region.
        /// </summary>
        internal ref WeakReference<_KeyWorldRegion> _neighbor(Dir3 dir)
        {
            switch (dir)
            {
                case Dir3.NegX: return ref _neighbors.NegX;
                case Dir3.PosX: return ref _neighbors.PosX;
                case Dir3.NegY: return ref _neighbors.NegY;
                case Dir3.PosY: return ref _neighbors.PosY;
                case Dir3.NegZ: return ref _neighbors.NegZ;
                case Dir3.PosZ: return ref _neighbors.PosZ;
                default: throw new ArgumentException();
            }
        }

        /// <summary>
        /// Gets a specific child of this region, creating it if necessary.
        /// </summary>
        internal _KeyWorldRegion _getChildStrict(Octant octant)
        {
        retry:
            ref WeakReference<_KeyWorldRegion> weakChildSlot = ref _child(octant);
            WeakReference<_KeyWorldRegion> weakChild = Volatile.Read(ref weakChildSlot);
            if (weakChild == null || !weakChild.TryGetTarget(out _KeyWorldRegion child))
            {
                child = new _KeyWorldRegion(this, octant);
                if (Interlocked.CompareExchange(ref weakChildSlot, child._weakThis, weakChild) != weakChild)
                    goto retry;
            }
            return child;
        }

        /// <summary>
        /// Determines whether this region has a particular child key region at the current moment.
        /// </summary>
        private bool _hasChild(Octant octant, out _KeyWorldRegion child)
        {
            ref WeakReference<_KeyWorldRegion> weakChildSlot = ref _child(octant);
            WeakReference<_KeyWorldRegion> weakChild = Volatile.Read(ref weakChildSlot);
            if (weakChild != null)
                return weakChild.TryGetTarget(out child);
            child = null;
            return false;
        }

        /// <summary>
        /// Determines whether this region is part of a larger region of Euclidean space, and if so,
        /// returns that region, along with the relationship this region has to that region. This
        /// is the inverse of <see cref="GetChild"/>.
        /// </summary>
        public bool TryGetParent(out _KeyWorldRegion parent, out Octant octant)
        {
            return _loc.IsChild(out parent, out octant);
        }

        /// <summary>
        /// Gets the equal-size neighboring region of this region in the given direction.
        /// </summary>
        public WorldRegion GetNeighbor(Dir3 dir)
        {
            // Check cache
            ref WeakReference<_KeyWorldRegion> weakNeighborSlot = ref _neighbor(dir);
            WeakReference<_KeyWorldRegion> weakNeighbor = weakNeighborSlot;
            if (weakNeighbor != null && weakNeighbor.TryGetTarget(out _KeyWorldRegion keyNeighbor))
                return keyNeighbor;

            // Use parent to find neighbor
            if (_loc.IsChild(out _KeyWorldRegion parent, out Octant octant))
            {
                var axis = dir.GetAxis(out Dir1 adir);
                var resOctant = octant.Flip(axis);
                if (octant.GetDir(axis) == adir)
                {
                    // Traverse to parent's neighbor
                    WorldRegion neighborParent = parent.GetNeighbor(dir);
                    if (neighborParent._path.IsEmpty && neighborParent._ancestor._hasChild(resOctant, out keyNeighbor))
                    {
                        weakNeighborSlot = keyNeighbor._weakThis;
                        return keyNeighbor;
                    }
                    else
                    {
                        return neighborParent.GetChild(resOctant);
                    }
                }
                else
                {
                    // Traverse to other child of parent
                    if (parent._hasChild(resOctant, out keyNeighbor))
                    {
                        weakNeighborSlot = keyNeighbor._weakThis;
                        return keyNeighbor;
                    }
                    else
                    {
                        return ((WorldRegion)parent).GetChild(resOctant);
                    }
                }
            }
            else
            {
                // Get neighbor of root region
                bool isRoot = _loc.IsRoot(out World world, out WorldTopology.Region source);
                Debug.Assert(isRoot);
                return world.GetRegion(world.Charter.Topology.GetNeighbor(source, dir));
            }
        }

        public static implicit operator WorldRegion(_KeyWorldRegion region)
        {
            return new WorldRegion(region, ShortOctantList.Empty);
        }
    }
}
