﻿using System;
using System.Diagnostics;

using Alunite.Data;
using Alunite.Data.Serialization;
using Alunite.Data.Geometry;
using Alunite.Universe.Grids;

namespace Alunite.Universe.Generation
{

    /// <summary>
    /// A helper class for computing various metrics and estimates for a set of <see cref="UglProcedure"/>s.
    /// </summary>
    /// <remarks>The metrics produced by the surveyor are important for deciding where to place
    /// <see cref="UglOpCode.Chunk"/> suggestions in an assembly.</remarks>
    internal sealed class _UglSurveyor
    {
        public _UglSurveyor(PermutationBuilder<Tile> tiles, uint complexityLimit)
        {
            Tiles = tiles;
            ComplexityLimit = complexityLimit;
            _cache = new MapBuilder<_UglSplitProcedure, Metrics>();
            _groups = new MapBuilder<Octet<_Node>, _Node>();
        }

        /// <summary>
        /// Defines the mapping from <see cref="Tile"/>s to scale-0 <see cref="_Node"/>s.
        /// </summary>
        public PermutationBuilder<Tile> Tiles { get; }

        /// <summary>
        /// The highest complexity bound that is considered by this surveyor.
        /// </summary>
        public uint ComplexityLimit { get; }

        /// <summary>
        /// A cache of the <see cref="Metrics"/>s for <see cref="_UglSplitProcedure"/>s.
        /// </summary>
        private MapBuilder<_UglSplitProcedure, Metrics> _cache;

        /// <summary>
        /// A mapping of <see cref="Octet{T}"/>s of <see cref="_Node"/>s to the groups they form.
        /// </summary>
        private MapBuilder<Octet<_Node>, _Node> _groups;

        /// <summary>
        /// The index to be used for the next group <see cref="_Node"/> to be defined.
        /// </summary>
        private uint _nextGroupIndex;

        /// <summary>
        /// Describes a possible octree node that can be produced by a <see cref="UglProcedure"/>.
        /// </summary>
        internal struct _Node : IEquatable<_Node>
        {
            private uint _def;
            public _Node(byte scale, uint index)
            {
                _def = (uint)(scale << 24) | index;
            }

            /// <summary>
            /// The scale of the octree for this node.
            /// </summary>
            public byte Scale => unchecked((byte)(_def >> 24));

            /// <summary>
            /// The index of this node.
            /// </summary>
            public uint Index => _def & 0x00FFFFFF;

            public static bool operator ==(_Node a, _Node b)
            {
                return a._def == b._def;
            }

            public static bool operator !=(_Node a, _Node b)
            {
                return !(a == b);
            }

            public override bool Equals(object obj)
            {
                if (!(obj is _Node))
                    return false;
                return this == (_Node)obj;
            }

            bool IEquatable<_Node>.Equals(_Node other)
            {
                return this == other;
            }

            public override int GetHashCode()
            {
                return _def.GetHashCode();
            }
        }

        /// <summary>
        /// Encapsulates various measurements for a particular <see cref="UglProcedure"/>.
        /// </summary>
        public struct Metrics
        {
            /// <summary>
            /// The potential nodes that can appear in an octree produced by the <see cref="UglProcedure"/>, excluding
            /// those whose scale is at or above <see cref="_scale"/> or <see cref="_limitScale"/>.
            /// </summary>
            internal Set<_Node> _subnodes;

            /// <summary>
            /// The scale at which the <see cref="UglProcedure"/> produces more octrees than can be tracked, according
            /// to <see cref="ComplexityLimit"/>. This is <see cref="byte.MaxValue"/> if the limit has not yet been
            /// reached.
            /// </summary>
            internal byte _limitScale;

            /// <summary>
            /// The scale of the associated <see cref="UglProcedure"/>.
            /// </summary>
            internal byte _scale;

            /// <summary>
            /// The potential nodes the <see cref="UglProcedure"/> can produce at <see cref="_scale"/>. This is only
            /// applicable if <see cref="_limitScale"/> is <see cref="byte.MaxValue"/>.
            /// </summary>
            internal Set<_Node> _nodes;
        }

        /// <summary>
        /// Gets a <see cref="_Node"/> for an individual tile.
        /// </summary>
        internal _Node _leafNode(Tile tile)
        {
            return new _Node(0, Tiles.Include(tile));
        }

        /// <summary>
        /// Attempts to construct a <see cref="_Node"/> for an octet consisting of the given <see cref="_Node"/>s of
        /// the previous scale. Returns false if the resulting node would be non-realizable (i.e. the nodes are
        /// not compatible).
        /// </summary>
        internal bool _tryGroupNode(Octet<_Node> octet, out _Node node)
        {
            // TODO: Check for node compatibility
            if (_groups.Allocate(octet, out var entry))
                entry.Value = node = new _Node((byte)(octet.NegX_NegY_NegZ.Scale + 1), _nextGroupIndex++);
            else
                node = entry.Value;
            return true;
        }

        /// <summary>
        /// Gets the <see cref="Metrics"/>s for a given procedure.
        /// </summary>
        public Metrics GetMetrics(_UglSplitProcedure proc)
        {
            if (_cache.TryGet(proc, out Metrics metrics))
                return metrics;
            metrics = GroupMetrics(new Octet<Metrics>(
                GetMetrics(proc.Targets.NegX_NegY_NegZ),
                GetMetrics(proc.Targets.PosX_NegY_NegZ),
                GetMetrics(proc.Targets.NegX_PosY_NegZ),
                GetMetrics(proc.Targets.PosX_PosY_NegZ),
                GetMetrics(proc.Targets.NegX_NegY_PosZ),
                GetMetrics(proc.Targets.PosX_NegY_PosZ),
                GetMetrics(proc.Targets.NegX_PosY_PosZ),
                GetMetrics(proc.Targets.PosX_PosY_PosZ)));
            _cache.Add(proc, metrics);
            return metrics;
        }

        /// <summary>
        /// Gets the <see cref="Metrics"/>s for a given procedure.
        /// </summary>
        public Metrics GetMetrics(UglProcedure proc)
        {
            if (proc is _UglSplitProcedure splitProc)
                return GetMetrics(splitProc);
            var considered = new SetBuilder<UglProcedure>();
            if (proc.Scale == 0)
            {
                var leafProcs = new ListBuilder<_UglLeafProcedure>();
                proc._getFillings(leafProcs, considered);

                // Collect possible tiles
                var subnodes = new SetBuilder<_Node>();
                foreach (_UglLeafProcedure leafProc in leafProcs)
                    subnodes.Include(_leafNode(leafProc.Tile));
                return new Metrics
                {
                    _subnodes = Set.None<_Node>(),
                    _limitScale = byte.MaxValue,
                    _scale = 0,
                    _nodes = subnodes.Finish()
                };
            }
            else
            {
                var splitProcs = new ListBuilder<_UglSplitProcedure>();
                proc._getFillings(splitProcs, considered);

                // Collect possible metrics
                var metricss = new Metrics[splitProcs.Length];
                byte limitScale = byte.MaxValue;
                for (uint i = 0; i < splitProcs.Length; i++)
                {
                    Metrics metrics = GetMetrics(splitProcs[i]);
                    if (metrics._limitScale < limitScale)
                        limitScale = metrics._limitScale;
                    metricss[i] = metrics;
                }

                // Combine metrics into one
                var subnodes = new SetBuilder<_Node>();
                foreach (Metrics metrics in metricss)
                {
                    foreach (_Node node in metrics._subnodes)
                        if (node.Scale < limitScale)
                            subnodes.Include(node);
                }
                if (limitScale == byte.MaxValue)
                {
                    var nodes = new SetBuilder<_Node>();
                    foreach (Metrics metrics in metricss)
                        nodes.Union(metrics._nodes);
                    return new Metrics
                    {
                        _subnodes = subnodes.Finish(),
                        _limitScale = byte.MaxValue,
                        _scale = proc.Scale,
                        _nodes = nodes.Finish()
                    };
                }
                else
                {
                    return new Metrics
                    {
                        _subnodes = subnodes.Finish(),
                        _limitScale = limitScale,
                        _scale = proc.Scale
                    };
                }
            }
        }

        /// <summary>
        /// Constructs the <see cref="Metrics"/> for an octet of procedures with the given metrics.
        /// </summary>
        public Metrics GroupMetrics(Octet<Metrics> metrics)
        {
            // Determine limit scale
            byte scale = (byte)(metrics.NegX_NegY_NegZ._scale + 1);
            byte limitScale = byte.MaxValue;
            void consider(Metrics octantMetrics)
            {
                if (octantMetrics._limitScale < limitScale)
                    limitScale = octantMetrics._limitScale;
            }
            consider(metrics.NegX_NegY_NegZ);
            consider(metrics.PosX_NegY_NegZ);
            consider(metrics.NegX_PosY_NegZ);
            consider(metrics.PosX_PosY_NegZ);
            consider(metrics.NegX_NegY_PosZ);
            consider(metrics.PosX_NegY_PosZ);
            consider(metrics.NegX_PosY_PosZ);
            consider(metrics.PosX_PosY_PosZ);

            // Combine subnodes
            var subnodes = new SetBuilder<_Node>();
            void unionSubnodes(Metrics octantMetrics)
            {
                foreach (_Node node in octantMetrics._subnodes)
                    if (node.Scale < limitScale)
                        subnodes.Include(node);
            }
            unionSubnodes(metrics.NegX_NegY_NegZ);
            unionSubnodes(metrics.PosX_NegY_NegZ);
            unionSubnodes(metrics.NegX_PosY_NegZ);
            unionSubnodes(metrics.PosX_PosY_NegZ);
            unionSubnodes(metrics.NegX_NegY_PosZ);
            unionSubnodes(metrics.PosX_NegY_PosZ);
            unionSubnodes(metrics.NegX_PosY_PosZ);
            unionSubnodes(metrics.PosX_PosY_PosZ);

            // Have we reached the maximum complexity limit yet?
            if (limitScale == byte.MaxValue)
            {
                // Add previous level root nodes as subnodes
                subnodes.Union(metrics.NegX_NegY_NegZ._nodes);
                subnodes.Union(metrics.PosX_NegY_NegZ._nodes);
                subnodes.Union(metrics.NegX_PosY_NegZ._nodes);
                subnodes.Union(metrics.PosX_PosY_NegZ._nodes);
                subnodes.Union(metrics.NegX_NegY_PosZ._nodes);
                subnodes.Union(metrics.PosX_NegY_PosZ._nodes);
                subnodes.Union(metrics.NegX_PosY_PosZ._nodes);
                subnodes.Union(metrics.PosX_PosY_PosZ._nodes);

                // Construct new root nodes
                SetBuilder<_Node> nodes = new SetBuilder<_Node>();
                uint numNodesLeft = ComplexityLimit; // TODO: We can probably reduce this limit
                foreach (_Node node_negX_negY_negZ in metrics.NegX_NegY_NegZ._nodes)
                {
                    foreach (_Node node_posX_negY_negZ in metrics.PosX_NegY_NegZ._nodes)
                    {
                        foreach (_Node node_negX_posY_negZ in metrics.NegX_PosY_NegZ._nodes)
                        {
                            foreach (_Node node_posX_posY_negZ in metrics.PosX_PosY_NegZ._nodes)
                            {
                                foreach (_Node node_negX_negY_posZ in metrics.NegX_NegY_PosZ._nodes)
                                {
                                    foreach (_Node node_posX_negY_posZ in metrics.PosX_NegY_PosZ._nodes)
                                    {
                                        foreach (_Node node_negX_posY_posZ in metrics.NegX_PosY_PosZ._nodes)
                                        {
                                            foreach (_Node node_posX_posY_posZ in metrics.PosX_PosY_PosZ._nodes)
                                            {
                                                Octet<_Node> octet = new Octet<_Node>(
                                                    node_negX_negY_negZ, node_posX_negY_negZ,
                                                    node_negX_posY_negZ, node_posX_posY_negZ,
                                                    node_negX_negY_posZ, node_posX_negY_posZ,
                                                    node_negX_posY_posZ, node_posX_posY_posZ);
                                                if (_tryGroupNode(octet, out _Node node))
                                                {
                                                    if (numNodesLeft == 0)
                                                        goto tooComplex;
                                                    nodes.Include(node);
                                                    numNodesLeft--;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // Finalize metrics
                return new Metrics
                {
                    _subnodes = subnodes.Finish(),
                    _limitScale = byte.MaxValue,
                    _scale = scale,
                    _nodes = nodes.Finish()
                };

            tooComplex:
                return new Metrics
                {
                    _subnodes = subnodes.Finish(),
                    _limitScale = scale,
                    _scale = scale
                };
            }
            else
            {
                return new Metrics
                {
                    _subnodes = subnodes.Finish(),
                    _limitScale = limitScale,
                    _scale = scale
                };
            }
        }

        /// <summary>
        /// Gets an upper bound for the complexity of the octrees generated by a procedure with the given metrics.
        /// </summary>
        public uint GetComplexityBound(Metrics metrics)
        {
            // Count the number of nodes at each scale
            byte limitScale = metrics._limitScale;
            if (limitScale > metrics._scale)
                limitScale = metrics._scale;
            Span<uint> numScaleSubnodes = stackalloc uint[limitScale];
            foreach (_Node subnode in metrics._subnodes)
                numScaleSubnodes[subnode.Scale]++;

            // Add numbers together, respecting the maximum number of nodes possible at each scale
            uint complexity = 1;
            uint maxScaleSubnodes = 8;
            byte scale = metrics._scale;
            while (scale > 0)
            {
                --scale;
                if (scale < numScaleSubnodes.Length)
                {
                    if (maxScaleSubnodes == 0)
                        complexity += numScaleSubnodes[scale];
                    else
                        complexity += Math.Min(maxScaleSubnodes, numScaleSubnodes[scale]);
                }
                else
                {
                    complexity += maxScaleSubnodes;
                }
                if (complexity > ComplexityLimit)
                    return ComplexityLimit;
                unchecked { maxScaleSubnodes *= 8; }
            }
            return complexity;
        }
    }

    /// <summary>
    /// A helper class for converting a <see cref="UglProcedure"/> into a list of interlinked operations, as required
    /// for creating a <see cref="UglAssembly"/>.
    /// </summary>
    internal sealed class _UglCompiler
    {
        public _UglCompiler(_UglSurveyor surveyor)
        {
            Surveyor = surveyor;
            _ops = new ListBuilder<_UglOperation>();
            _ops.Append(null); // Placeholder for null procedure
            _procs = new MapBuilder<UglProcedure, _ProcedureInfo>();
        }

        /// <summary>
        /// The surveyor used to produce metrics which determine where <see cref="_UglChunkOperation"/>s should be
        /// placed.
        /// </summary>
        public _UglSurveyor Surveyor { get; }

        /// <summary>
        /// The tiles defined so far for the assembly.
        /// </summary>
        public PermutationBuilder<Tile> Tiles => Surveyor.Tiles;

        /// <summary>
        /// The operations to be included in the assembly, in topological order (dependencies before dependents).
        /// </summary>
        private ListBuilder<_UglOperation> _ops;

        /// <summary>
        /// Identifies which operations in <see cref="_ops"/> implement the corresponding <see cref="UglProcedure"/>s
        /// required for the assembly.
        /// </summary>
        private MapBuilder<UglProcedure, _ProcedureInfo> _procs;

        /// <summary>
        /// Provides information about a procedure within a <see cref="_UglCompiler"/>.
        /// </summary>
        private struct _ProcedureInfo
        {
            /// <summary>
            /// The operation in <see cref="_ops"/> which implements this procedure when called within a chunk,
            /// or 0 if not defined.
            /// </summary>
            public uint InChunkTarget;

            /// <summary>
            /// The operation in <see cref="_ops"/> which implements this procedure when not called within a chunk,
            /// or 0 if not yet defined.
            /// </summary>
            public uint NotInChunkTarget;
        }

        /// <summary>
        /// Includes the given procedure in the resulting assembly, returning the index of the operation
        /// in <see cref="Operations"/> which implements it.
        /// </summary>
        /// <param name="inChunk">Indicates whether a <see cref="_UglChunkOperation"/> should be executed
        /// before the returned operation is executed. This may be set from <see cref="false"/> to <see cref="true"/> if
        /// the procedure demands to be marked with a chunk operation.</param>
        public uint Link(UglProcedure proc, ref bool inChunk, out _UglProcedureStats stats)
        {
            if (proc is null)
            {
                stats = _UglProcedureStats.Leaf;
                return 0;
            }
            uint res = _link(proc, ref inChunk);
            stats = _ops[res].Stats;
            return res;
        }

        /// <summary>
        /// Includes the given procedure in the resulting assembly, returning the index of the
        /// operation which implements it.
        /// </summary>
        /// <param name="inChunk">Indicates whether a <see cref="_UglChunkOperation"/> should be executed
        /// before the returned operation is executed.</param>
        public uint Link(UglProcedure proc, bool inChunk, out _UglProcedureStats stats)
        {
            if (proc is null)
            {
                stats = _UglProcedureStats.Leaf;
                return 0;
            }
            uint res = _link(proc, inChunk);
            stats = _ops[res].Stats;
            return res;
        }

        /// <summary>
        /// Includes the given procedure in the resulting assembly, returning the index of the operation
        /// in <see cref="Operations"/> which implements it.
        /// </summary>
        private uint _link(UglProcedure proc, ref bool inChunk)
        {
            if (!_procs.TryGet(proc, out _ProcedureInfo procInfo))
                procInfo = default;
            if (inChunk)
            {
                if (procInfo.InChunkTarget > 0)
                    return procInfo.InChunkTarget;
                uint target = _ops.Append(proc._compile(this, ref inChunk));
                Debug.Assert(inChunk);
                procInfo.InChunkTarget = target;
                _procs.Add(proc, procInfo);
                return target;
            }
            else
            {
                if (procInfo.NotInChunkTarget > 0)
                    return procInfo.NotInChunkTarget;
                if (procInfo.InChunkTarget > 0)
                {
                    inChunk = true;
                    return procInfo.InChunkTarget;
                }
                uint target = _ops.Append(proc._compile(this, ref inChunk));
                if (inChunk)
                    procInfo.InChunkTarget = target;
                else
                    procInfo.NotInChunkTarget = target;
                _procs.Add(proc, procInfo);
                return target;
            }
        }

        /// <summary>
        /// Includes the given procedure in the resulting assembly, returning the index of the
        /// operation which implements it.
        /// </summary>
        private uint _link(UglProcedure proc, bool inChunk)
        {
            if (!_procs.TryGet(proc, out _ProcedureInfo procInfo))
                procInfo = default;
            if (inChunk)
            {
                // TODO: It's kind of awkward that we might have to recompile the procedure for inChunk
                // depending on what order the Link requests come in
                if (procInfo.InChunkTarget > 0)
                    return procInfo.InChunkTarget;
                uint target = _ops.Append(proc._compile(this, ref inChunk));
                Debug.Assert(inChunk);
                procInfo.InChunkTarget = target;
                _procs.Add(proc, procInfo);
                return target;
            }
            else
            {
                if (procInfo.NotInChunkTarget > 0)
                    return procInfo.NotInChunkTarget;
                uint target;
                if (procInfo.InChunkTarget > 0)
                {
                    target = _chunk(proc.Scale, procInfo.InChunkTarget);
                }
                else
                {
                    target = _ops.Append(proc._compile(this, ref inChunk));
                    if (inChunk)
                    {
                        procInfo.InChunkTarget = target;
                        target = _chunk(proc.Scale, target);
                    }
                }
                procInfo.NotInChunkTarget = target;
                _procs.Add(proc, procInfo);
                return target;
            }
        }

        /// <summary>
        /// Constructs a <see cref="_UglChunkOperation"/> wrapper over the given target.
        /// </summary>
        private uint _chunk(byte scale, uint target)
        {
            return _ops.Append(new _UglChunkOperation(_ops[target].Stats, scale, target));
        }

        /// <summary>
        /// Gets the list of operations compiled so far, in topological order.
        /// </summary>
        public List<_UglOperation> Finish()
        {
            return _ops.Finish();
        }
    }

    /// <summary>
    /// Describes a primitive operation in the assembly. This may reference other <see cref="_UglOperation"/>s and/or metadata.
    /// </summary>
    internal abstract class _UglOperation
    {
        public _UglOperation(_UglProcedureStats stats)
        {
            Stats = stats;
        }

        /// <summary>
        /// Provides high-level information about the procedure implemented by this operation.
        /// </summary>
        public _UglProcedureStats Stats { get; }

        /// <summary>
        /// Informs the given <see cref="_UglStaticMarker"/> that this operation can be reached by the given canonical
        /// path through a chunk.
        /// </summary>
        public abstract void Reach(_UglStaticMarker marker, ShortOctantList path);

        /// <summary>
        /// Writes this operation to the start of the given assembler.
        /// </summary>
        public abstract void Write(_UglAssembler assem);
    }

    /// <summary>
    /// An operation which appends a byte constant to the belt.
    /// </summary>
    internal sealed class _UglPutOperation : _UglOperation
    {
        public _UglPutOperation(_UglProcedureStats stats, byte value, uint cont)
            : base(stats)
        {
            Value = value;
            Continuation = cont;
        }

        /// <summary>
        /// The value appended to the belt.
        /// </summary>
        public byte Value { get; }

        /// <summary>
        /// The operation executed after putting.
        /// </summary>
        public uint Continuation { get; }

        public override void Reach(_UglStaticMarker marker, ShortOctantList path)
        {
            marker.Reach(path, Continuation);
        }

        public override void Write(_UglAssembler assem)
        {
            assem.Jump(Continuation);
            if (Value < 16)
            {
                Span<byte> blob = assem.Write(1);
                Blob.WriteByte(ref blob, (byte)(UglOpCode.Put_0 + Value));
            }
            else
            {
                Span<byte> blob = assem.Write(1 + 1);
                Blob.WriteByte(ref blob, (byte)UglOpCode.Put);
                Blob.WriteByte(ref blob, Value);
            }
        }
    }

    /// <summary>
    /// An operation which selects the item at the given offset from the end of the belt and
    /// appends a copy of it to the belt.
    /// </summary>
    internal sealed class _UglDupOperation : _UglOperation
    {
        public _UglDupOperation(_UglProcedureStats stats, byte offset, uint cont)
            : base(stats)
        {
            Offset = offset;
            Continuation = cont;
        }

        /// <summary>
        /// The offset of the item to duplicate.
        /// </summary>
        public byte Offset { get; }

        /// <summary>
        /// The operation executed after duplicating.
        /// </summary>
        public uint Continuation { get; }

        public override void Reach(_UglStaticMarker marker, ShortOctantList path)
        {
            marker.Reach(path, Continuation);
        }

        public override void Write(_UglAssembler assem)
        {
            assem.Jump(Continuation);
            if (Offset < 16)
            {
                Span<byte> blob = assem.Write(1);
                Blob.WriteByte(ref blob, (byte)(UglOpCode.Dup_M1 + Offset));
            }
            else
            {
                Span<byte> blob = assem.Write(1 + 1);
                Blob.WriteByte(ref blob, (byte)UglOpCode.Dup);
                Blob.WriteByte(ref blob, Offset);
            }
        }
    }

    /// <summary>
    /// An operation which appends a byte constant to the belt.
    /// </summary>
    internal sealed class _UglYieldOperation : _UglOperation
    {
        public _UglYieldOperation(_UglProcedureStats stats, Axis3 axis, byte numOut, byte numIn, uint cont)
            : base(stats)
        {
            Axis = axis;
            NumOut = numOut;
            NumIn = numIn;
            Continuation = cont;
        }

        /// <summary>
        /// The axis on which information is transmitted and received.
        /// </summary>
        public Axis3 Axis { get; }

        /// <summary>
        /// The number of belt items transmitted.
        /// </summary>
        public byte NumOut { get; }

        /// <summary>
        /// The number of belt items to be received.
        /// </summary>
        public byte NumIn { get; }

        /// <summary>
        /// The operation executed after yielding.
        /// </summary>
        public uint Continuation { get; }

        public override void Reach(_UglStaticMarker marker, ShortOctantList path)
        {
            marker.Reach(path, Continuation);
        }

        public override void Write(_UglAssembler assem)
        {
            assem.Jump(Continuation);
            Span<byte> blob = assem.Write(1 + 1 + 1);
            Blob.WriteByte(ref blob, (byte)(UglOpCode.Yield_X + (byte)Axis));
            Blob.WriteByte(ref blob, NumOut);
            Blob.WriteByte(ref blob, NumIn);
        }
    }

    /// <summary>
    /// An operation which decorates another operation with a <see cref="UglOpCode.Chunk"/> suggestion.
    /// </summary>
    internal sealed class _UglChunkOperation : _UglOperation
    {
        public _UglChunkOperation(_UglProcedureStats stats, byte scale, uint target)
            : base(stats)
        {
            Scale = scale;
            Target = target;
        }

        /// <summary>
        /// The log base-2 of the length of the chunk.
        /// </summary>
        public byte Scale { get; }

        /// <summary>
        /// The target to be decorated.
        /// </summary>
        public uint Target { get; }

        /// <summary>
        /// The metadata for the chunk.
        /// </summary>
        public UglChunkMetadata Metadata => new UglChunkMetadata
        {
            Scale = Scale,
            BeltBound = Stats.BeltBound,

            // TODO: In order to compute the actual bandwidth, we need to know the bandwidth of neighboring
            // regions that are needed to "prime" the data for this chunk. Right now, we're using a fixed
            // fudge factor to account for those
            Bandwidth = new Axial3<ushort>(
                (ushort)(Stats.Bandwidth.X + 64),
                (ushort)(Stats.Bandwidth.Y + 64),
                (ushort)(Stats.Bandwidth.Z + 64))
        };

        public override void Reach(_UglStaticMarker marker, ShortOctantList path)
        {
            // Reachability should only ever be tested within a chunk, so we should never see a chunk operation.
            throw new InvalidOperationException();
        }

        public override void Write(_UglAssembler assem)
        {
            assem.Jump(Target);
            Span<byte> blob = assem.Write(1 + UglChunkMetadata.BlobSize);
            Blob.WriteByte(ref blob, (byte)UglOpCode.Chunk);
            UglChunkMetadata.Write(ref blob, Metadata);
        }
    }

    /// <summary>
    /// An operation which jumps to one of a given list of operations depending on the value of the item at a
    /// given offset from the end of the belt.
    /// </summary>
    internal sealed class _UglSwitchOperation : _UglOperation
    {
        public _UglSwitchOperation(_UglProcedureStats stats, byte offset, List<uint> targets)
            : base(stats)
        {
            Offset = offset;
            Targets = targets;
        }

        /// <summary>
        /// The offset of the item used to select the switch target.
        /// </summary>
        public byte Offset { get; }

        /// <summary>
        /// The potential targets for the switch.
        /// </summary>
        public List<uint> Targets { get; }

        public override void Reach(_UglStaticMarker marker, ShortOctantList path)
        {
            foreach (uint target in Targets)
                marker.Reach(path, target);
        }

        public override void Write(_UglAssembler assem)
        {
            if (Offset < 15)
            {
                Span<byte> offsets = stackalloc byte[(int)Targets.Length];
                if (assem.TrySwitch(Targets, offsets))
                {
                    Span<byte> blob = assem.Write(1 + 1 + Targets.Length);
                    Blob.WriteByte(ref blob, (byte)(UglOpCode.Switch_M1_S + Offset));
                    Blob.WriteByte(ref blob, (byte)(Targets.Length - 1));
                    foreach (byte offset in offsets)
                        Blob.WriteByte(ref blob, offset);
                }
                else
                {
                    throw new NotImplementedException();
                }
            }
        }
    }

    /// <summary>
    /// An operation which jumps to one of a pair of operations depending on whether a randomly generated
    /// 32-bit integer is greater than the given value.
    /// </summary>
    internal sealed class _UglCoinOperation : _UglOperation
    {
        public _UglCoinOperation(_UglProcedureStats stats, uint pivot, uint lt, uint ge)
            : base(stats)
        {
            Pivot = pivot;
            Lt = lt;
            Ge = ge;
        }
        /// <summary>
        /// The decision pivot for the operation.
        /// </summary>
        public uint Pivot { get; }

        /// <summary>
        /// The target if the randomly chosen value is less than <see cref="Pivot"/>.
        /// </summary>
        public uint Lt { get; }

        /// <summary>
        /// The target if the randomly chosen value is greater than or equal to <see cref="Pivot"/>.
        /// </summary>
        public uint Ge { get; }

        public override void Reach(_UglStaticMarker marker, ShortOctantList path)
        {
            marker.Reach(path, Lt);
            marker.Reach(path, Ge);
        }

        public override void Write(_UglAssembler assem)
        {
            uint pivot32 = Pivot;
            assem.Include(Lt);
            assem.Jump(Ge);
            if (!assem.TryGetOffset(Lt, out byte offset))
            {
                // Swap the cases and add an extra branch
                pivot32 = unchecked(~pivot32 + 1);
                assem.Jump(Lt);
                assem.TryGetOffset(Ge, out offset);
            }
            ushort pivot16 = (ushort)(pivot32 >> 16);
            byte pivot8 = (byte)(pivot32 >> 24);
            if (pivot32 == (uint)pivot8 << 24)
            {
                Span<byte> blob = assem.Write(1 + 1 + 1);
                Blob.WriteByte(ref blob, (byte)(UglOpCode.Coin_U1_S));
                Blob.WriteByte(ref blob, pivot8);
                Blob.WriteByte(ref blob, offset);
            }
            else if (pivot32 == (uint)pivot16 << 16)
            {
                Span<byte> blob = assem.Write(1 + 2 + 1);
                Blob.WriteByte(ref blob, (byte)(UglOpCode.Coin_U2_S));
                Blob.WriteUInt16(ref blob, pivot16);
                Blob.WriteByte(ref blob, offset);
            }
            else
            {
                Span<byte> blob = assem.Write(1 + 4 + 1);
                Blob.WriteByte(ref blob, (byte)(UglOpCode.Coin_U4_S));
                Blob.WriteUInt32(ref blob, pivot32);
                Blob.WriteByte(ref blob, offset);
            }
        }
    }

    /// <summary>
    /// An operation which jumps to one of the operations in a table, chosen randomly and uniformly.
    /// </summary>
    internal sealed class _UglDieOperation : _UglOperation
    {
        public _UglDieOperation(_UglProcedureStats stats, uint[] targets)
            : base(stats)
        {
            Debug.Assert(Bitwise.UpperPow2((uint)targets.Length) == (uint)targets.Length);
            Targets = targets;
        }

        /// <summary>
        /// The possible targets for the operation. This table must have a power-of-two size.
        /// </summary>
        public uint[] Targets { get; }

        public override void Reach(_UglStaticMarker marker, ShortOctantList path)
        {
            for (int i = 0; i < Targets.Length; i++)
                marker.Reach(path, Targets[i]);
        }

        public override void Write(_UglAssembler assem)
        {
            if (Targets.Length <= 64)
            {
                Span<byte> offsets = stackalloc byte[Targets.Length];
                bool hasSwitch = assem.TrySwitch(Targets, offsets);
                Debug.Assert(hasSwitch);
                Span<byte> blob = assem.Write(1 + (uint)Targets.Length);
                Blob.WriteByte(ref blob, (byte)((uint)UglOpCode.Die_2_S - 1 + Bitwise.Log2((uint)Targets.Length)));
                for (int i = 0; i < offsets.Length; i++)
                    Blob.WriteByte(ref blob, offsets[i]);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
    }

    /// <summary>
    /// A filling operation which splits the current region into <see cref="Octant"/> subregions
    /// and jumps to a corresponding operation to populate each one.
    /// </summary>
    internal sealed class _UglSplitOperation : _UglOperation
    {
        public _UglSplitOperation(_UglProcedureStats stats, Octet<uint> targets)
            : base(stats)
        {
            Targets = targets;
        }

        /// <summary>
        /// Identifies the operations used to populate each subregion created by the split.
        /// </summary>
        public Octet<uint> Targets { get; }

        public override void Reach(_UglStaticMarker marker, ShortOctantList path)
        {
            marker.Reach(path.Append(Octant.NegX_NegY_NegZ), Targets.NegX_NegY_NegZ);
            marker.Reach(path.Append(Octant.PosX_NegY_NegZ), Targets.PosX_NegY_NegZ);
            marker.Reach(path.Append(Octant.NegX_PosY_NegZ), Targets.NegX_PosY_NegZ);
            marker.Reach(path.Append(Octant.PosX_PosY_NegZ), Targets.PosX_PosY_NegZ);
            marker.Reach(path.Append(Octant.NegX_NegY_PosZ), Targets.NegX_NegY_PosZ);
            marker.Reach(path.Append(Octant.PosX_NegY_PosZ), Targets.PosX_NegY_PosZ);
            marker.Reach(path.Append(Octant.NegX_PosY_PosZ), Targets.NegX_PosY_PosZ);
            marker.Reach(path.Append(Octant.PosX_PosY_PosZ), Targets.PosX_PosY_PosZ);
        }

        public override void Write(_UglAssembler assem)
        {
            Span<uint> targets = stackalloc uint[8];
            targets[0] = Targets.NegX_NegY_NegZ;
            targets[1] = Targets.PosX_NegY_NegZ;
            targets[2] = Targets.NegX_PosY_NegZ;
            targets[3] = Targets.PosX_PosY_NegZ;
            targets[4] = Targets.NegX_NegY_PosZ;
            targets[5] = Targets.PosX_NegY_PosZ;
            targets[6] = Targets.NegX_PosY_PosZ;
            targets[7] = Targets.PosX_PosY_PosZ;
            Span<byte> offsets = stackalloc byte[8];
            bool hasSwitch = assem.TrySwitch(targets, offsets);
            Debug.Assert(hasSwitch);
            Span<byte> blob = assem.Write(1 + 8);
            Blob.WriteByte(ref blob, (byte)UglOpCode.Split);
            Blob.WriteByte(ref blob, offsets[0]);
            Blob.WriteByte(ref blob, offsets[1]);
            Blob.WriteByte(ref blob, offsets[2]);
            Blob.WriteByte(ref blob, offsets[3]);
            Blob.WriteByte(ref blob, offsets[4]);
            Blob.WriteByte(ref blob, offsets[5]);
            Blob.WriteByte(ref blob, offsets[6]);
            Blob.WriteByte(ref blob, offsets[7]);
        }
    }

    /// <summary>
    /// A filling operation which populates the current scale-0 region with a given <see cref="Tile"/> identified
    /// by its token.
    /// </summary>
    internal sealed class _UglLeafOperation : _UglOperation
    {
        public _UglLeafOperation(uint tileToken)
            : base(_UglProcedureStats.Leaf)
        {
            TileToken = tileToken;
        }

        /// <summary>
        /// The token for the <see cref="Tile"/> used by this operation.
        /// </summary>
        public uint TileToken { get; }

        public override void Reach(_UglStaticMarker marker, ShortOctantList path)
        {
            // Nothing to do here
        }

        public override void Write(_UglAssembler assem)
        {
            Span<byte> blob = assem.Write(5);
            Blob.WriteByte(ref blob, (byte)UglOpCode.Leaf);
            Blob.WriteUInt32(ref blob, TileToken);
        }
    }

    /// <summary>
    /// A helper class for determining where <see cref="UglOpCode.Static"/> hints should be placed in a list of
    /// operations compiled by a <see cref="_UglCompiler"/>.
    /// </summary>
    internal sealed class _UglStaticMarker
    {
        public _UglStaticMarker(List<_UglOperation> ops)
        {
            Operations = ops;
            MarkedStatic = BoolListBuilder.CreateReplicateFalse(ops.Length);
            _info = new _OperationInfo[ops.Length];
        }

        /// <summary>
        /// The operations to be considered by this <see cref="_UglCompiler"/>.
        /// </summary>
        public List<_UglOperation> Operations { get; }

        /// <summary>
        /// Identifies which operations in <see cref="Operations"/> have been marked as static so far.
        /// </summary>
        public BoolListBuilder MarkedStatic { get; }

        /// <summary>
        /// Contains marker-specific information for <see cref="Operations"/>.
        /// </summary>
        private readonly _OperationInfo[] _info;

        /// <summary>
        /// Provides information about an operation within a <see cref="_UglStaticMarker"/>.
        /// </summary>
        private struct _OperationInfo
        {
            /// <summary>
            /// Indicates whether some path was found to reach this operation from the start of the chunk.
            /// </summary>
            public bool WasReached;

            /// <summary>
            /// Indicates whether at least two paths were found to reach this operation from the start of the chunk.
            /// </summary>
            public bool WasReachedTwice;

            /// <summary>
            /// Some path from the start of the chunk to this operation, assuming <see cref="WasReached"/>.
            /// </summary>
            public ShortOctantList Path;
        }

        /// <summary>
        /// Indicates that the operation with the given index can be reached by the given path from the start
        /// of a chunk.
        /// </summary>
        public void Reach(ShortOctantList path, uint target)
        {
            if (target > 0)
            {
                ref _OperationInfo info = ref _info[target];
                if (info.WasReached)
                {
                    // It only matters if the operation was reached by a different path than before
                    if (!info.WasReachedTwice && info.Path != path)
                    {
                        _UglOperation op = Operations[target];
                        info.WasReachedTwice = true;
                        if (op.Stats.IsStatic)
                        {
                            // The operation is eligible to be marked static, and is reached by at least two paths,
                            // so mark it static.
                            MarkedStatic[target] = true;
                        }
                        else
                        {
                            // Continue reachability checks with new path
                            op.Reach(this, path);
                        }
                    }
                }
                else
                {
                    // Mark operation as reached
                    info.WasReached = true;
                    info.Path = path;

                    // Continue reachability checks with operations
                    Operations[target].Reach(this, path);
                }
            }
        }

        /// <summary>
        /// Processes the chunk for the given operation, marking static all operations that are used
        /// in at least two different places in the chunk.
        /// </summary>
        public void Chunk(_UglChunkOperation op)
        {
            // Clear info
            for (int i = 0; i < _info.Length; i++)
                _info[i] = default;

            // Perform reachability checks starting from the chunk root
            Reach(ShortOctantList.Empty, op.Target);
        }
    }

    /// <summary>
    /// A helper class for generating a <see cref="UglAssembly"/> incrementally by prepending code. The assembly is
    /// built in reverse to simplify branching to relative addresses.
    /// </summary>
    internal sealed class _UglAssembler : IBuilder<UglAssembly>
    {
        public _UglAssembler(List<Tile> tiles, List<_UglOperation> ops, BoolList markedStatic)
        {
            Debug.Assert(ops.Length == markedStatic.Length);
            _code = new byte[64];
            _len = 0;
            Tiles = tiles;
            Operations = ops;
            MarkedStatic = markedStatic;
            _opInfos = new _OperationInfo[ops.Length];
            _jumpInfos = new ListBuilder<_JumpInfo>();
        }

        /// <summary>
        /// The array of assembly code written so far, along with a section at the beginning to allow for more code.
        /// </summary>
        private byte[] _code;

        /// <summary>
        /// The number of code bytes written so far to <see cref="_code"/>. These are written towards the end of the
        /// array, with the beginning of the array being reserved for additional code.
        /// </summary>
        private uint _len;

        /// <summary>
        /// The tiles defined for the assembly.
        /// </summary>
        public List<Tile> Tiles { get; }

        /// <summary>
        /// The operations to be included in the assembler (though not necessarily in the order they will be added). The
        /// first of these is a "void" operation that should never be executed.
        /// </summary>
        public List<_UglOperation> Operations { get; }

        /// <summary>
        /// Identifies which operations in <see cref="Operations"/> should be marked with a <see cref="UglOpCode.Static"/>
        /// hint when written.
        /// </summary>
        public BoolList MarkedStatic { get; }

        /// <summary>
        /// The assembly-specific information associated with <see cref="Operations"/>.
        /// </summary>
        private _OperationInfo[] _opInfos;

        /// <summary>
        /// Maintains the list of jumps that need to be corrected during finalization of the assembly.
        /// </summary>
        private ListBuilder<_JumpInfo> _jumpInfos;

        /// <summary>
        /// Provides information about an operations placement in the assembly.
        /// </summary>
        /// <remarks>5-byte jumps may only jump directly to an operation. 3-byte branches may only branch directly
        /// to the operation, or a jump. 2-byte branches may branch to the operation, a jump, or a 3-byte
        /// branch. This hierarchy ensures a bounded (i.e. 3) number of steps before the actual operation is reached.</remarks>
        private struct _OperationInfo
        {
            /// <summary>
            /// The position where the operation is written from the end of <see cref="_code"/>, or 0 if the operation
            /// has not yet been added to the assembly.
            /// </summary>
            public uint Direct;

            /// <summary>
            /// The most recent position that a 3-byte branch can use to execute this operation.
            /// </summary>
            public uint Indirect_Br;

            /// <summary>
            /// The most recent position that a 2-byte branch can use to execute this operation.
            /// </summary>
            public uint Indirect_Br_S;
        }

        /// <summary>
        /// Provides information about a jump instruction written to the assembly. Since the absolute position of the jump
        /// target won't be known until the entire assembly is written, the jump targets have to be updated at the very end
        /// of the process.
        /// </summary>
        private struct _JumpInfo
        {
            public _JumpInfo(uint pos, uint target)
            {
                Position = pos;
                Target = target;
            }

            /// <summary>
            /// The position of the jump instruction (or rather, its 4-byte argument) relative to the end of the assembly.
            /// </summary>
            public uint Position;

            /// <summary>
            /// The position of the jump target relative to the end of the assembly.
            /// </summary>
            public uint Target;
        }

        /// <summary>
        /// Writes the minimal code to the start of the assembly such that it will execute the operation
        /// with the given index. This may be accomplished by either writing the operation directly, or
        /// by inserting a jump to a previous instance of the operation.
        /// </summary>
        public void Jump(uint target)
        {
            if (target == 0)
                return; // We don't need to do anything for the void target. It should never be executed anyway
            ref _OperationInfo opInfo = ref _opInfos[target];
            if (opInfo.Direct == 0)
            {
                // If the operation has never been written, we need to write it now
                Operations[target].Write(this);
                if (MarkedStatic[target])
                    WriteStatic();
                opInfo.Direct = _len;
                opInfo.Indirect_Br = _len;
                opInfo.Indirect_Br_S = _len;
            }
            else
            {
                uint offset = _len - opInfo.Indirect_Br_S;
                if (offset == 0)
                    return; // We're already at the target
                if (offset <= byte.MaxValue)
                {
                    Span<byte> blob = Write(1 + 1);
                    Blob.WriteByte(ref blob, (byte)UglOpCode.Br_S);
                    Blob.WriteByte(ref blob, (byte)offset);
                }
                else
                {
                    offset = _len - opInfo.Indirect_Br;
                    if (offset <= ushort.MaxValue)
                    {
                        Span<byte> blob = Write(1 + 2);
                        Blob.WriteByte(ref blob, (byte)UglOpCode.Br);
                        Blob.WriteUInt16(ref blob, (ushort)offset);
                        opInfo.Indirect_Br_S = _len;
                    }
                    else
                    {
                        Span<byte> blob = Write(1 + 5);
                        Blob.WriteByte(ref blob, (byte)UglOpCode.Jmp);
                        _jumpInfos.Append(new _JumpInfo(_len - 1, opInfo.Direct));
                        opInfo.Indirect_Br_S = _len;
                        opInfo.Indirect_Br = _len;
                    }
                }
            }
        }

        /// <summary>
        /// Ensures that the given target has been written out to the assembly at some point.
        /// </summary>
        public void Include(uint target)
        {
            if (target == 0)
                return; // We don't need to do anything for the void target. It should never be executed anyway
            ref _OperationInfo opInfo = ref _opInfos[target];
            if (opInfo.Direct == 0)
            {
                Operations[target].Write(this);
                if (MarkedStatic[target])
                    WriteStatic();
                opInfo.Direct = _len;
                opInfo.Indirect_Br = _len;
                opInfo.Indirect_Br_S = _len;
            }
        }

        /// <summary>
        /// Attempts to get a 1-byte branch offset from the current point in the assembly to the given target, returning
        /// false if this is not possible without modifying the assembly.
        /// </summary>
        public bool TryGetOffset(uint target, out byte offset)
        {
            // The void target may use any offset, since it won't be executed anyway
            if (target == 0)
            {
                offset = 0;
                return true;
            }

            // Attempt to get byte offset
            _OperationInfo opInfo = _opInfos[target];
            if (opInfo.Indirect_Br_S > 0)
            {
                uint bigOffset = _len - opInfo.Indirect_Br_S;
                if (bigOffset < 256)
                {
                    offset = (byte)bigOffset;
                    return true;
                }
            }
            offset = default;
            return false;
        }

        /// <summary>
        /// Attempts to create a switch table of byte-length branch offsets corresponding to the given targets,
        /// returning false if this is not possible. The offsets are only applicable from the current point in
        /// the assembly.
        /// </summary>
        public bool TrySwitch(ReadOnlySpan<uint> targets, Span<byte> offsets)
        {
            // Make sure all targets have been written out
            int i = targets.Length;
            while (i > 0)
            {
                i--;
                Include(targets[i]);
            }

            // Attempt to gather byte offsets for all targets
            for (i = 0; i < offsets.Length; i++)
            {
                uint target = targets[i];
                if (target == 0)
                {
                    // Any offset may be used for the void target, since it won't be executed anyway
                    offsets[i] = 0;
                }
                else
                {
                    uint offset = _len - _opInfos[target].Indirect_Br_S;
                    if (offset <= byte.MaxValue)
                        offsets[i] = (byte)offset;
                    else
                        goto beyondByte;
                }
            }
            return true;

        beyondByte:
            // Estimate how many extra bytes worth of branches we would need to build the switch table
            uint numExtraBytesBound = 0;
        retryBeyondByte:
            uint numExtraBytes = 0;
            for (i = 0; i < offsets.Length; i++)
            {
                uint target = targets[i];
                if (target > 0)
                {
                    _OperationInfo opInfo = _opInfos[target];
                    uint offset = _len - opInfo.Indirect_Br_S + numExtraBytesBound;
                    if (offset > byte.MaxValue)
                    {
                        offset = _len - opInfo.Indirect_Br + numExtraBytesBound;
                        if (offset > ushort.MaxValue)
                        {
                            // Plan for an extra jump
                            numExtraBytes += 1 + 4;
                        }
                        else
                        {
                            // Plan for an extra branch
                            numExtraBytes += 1 + 2;
                        }
                    }
                }
            }
            if (numExtraBytes > byte.MaxValue)
            {
                // If the extra bytes would start pushing the branches out of range, we can't make
                // the switch table
                return false;
            }
            else if (numExtraBytes > numExtraBytesBound)
            {
                // Repeat until we converge on an estimate, or find that the switch is impossible
                numExtraBytesBound = numExtraBytes;
                goto retryBeyondByte;
            }
            else
            {
                // Try to build the switch table again, this time calling Jump to bring faraway targets closer
                bool needRetry = true;
                while (needRetry)
                {
                    needRetry = false;
                    for (i = 0; i < offsets.Length; i++)
                    {
                        uint target = targets[i];
                        if (target == 0)
                        {
                            // Any offset may be used for the void target, since it won't be executed anyway
                            offsets[i] = 0;
                        }
                        else
                        {
                            uint offset = _len - _opInfos[target].Indirect_Br_S;
                            if (offset <= byte.MaxValue)
                            {
                                offsets[i] = (byte)offset;
                            }
                            else
                            {
                                Jump(target);
                                needRetry = true;
                            }
                        }
                    }
                }
                return true;
            }
        }

        /// <summary>
        /// Allocates a block of the given length at the start of the assembly, and returns a <see cref="Span{T}"/>
        /// to allow writing to it.
        /// </summary>
        public Span<byte> Write(uint len)
        {
            // Ensure we have enough capacity
            while (len + _len >= (uint)_code.Length)
            {
                uint pos = (uint)_code.Length - _len;
                byte[] nCode = new byte[_code.Length * 2];
                uint nPos = (uint)nCode.Length - _len;
                for (uint i = 0; i < _len; i++)
                    nCode[nPos + i] = _code[pos + i];
                _code = nCode;
            }

            // Allocate bytes
            _len += len;
            return _code.AsSpan((int)(_code.Length - _len), (int)len);
        }

        /// <summary>
        /// Writes a <see cref="UglOpCode.Static"/> hint at the start of the assembly.
        /// </summary>
        public void WriteStatic()
        {
            Span<byte> blob = Write(1);
            Blob.WriteByte(ref blob, (byte)UglOpCode.Static);
        }

        /// <summary>
        /// Gets the <see cref="UglAssembly"/> for the code written by this assembler.
        /// </summary>
        public UglAssembly Finish()
        {
            byte[] nCode = new byte[_len];
            uint offset = (uint)_code.Length - _len;
            for (uint i = 0; i < nCode.Length; i++)
                nCode[i] = _code[offset + i];
            foreach (_JumpInfo jumpInfo in _jumpInfos)
            {
                uint head = _len - jumpInfo.Position;
                Blob.WriteUInt32(nCode, ref head, _len - jumpInfo.Target);
            }
            return new UglAssembly(Tiles, nCode);
        }
    }
}
