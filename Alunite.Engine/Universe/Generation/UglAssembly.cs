﻿using System;
using System.Diagnostics;

using Alunite.Data;
using Alunite.Data.Serialization;
using Alunite.Data.Geometry;
using Alunite.Data.Probability;
using Alunite.Universe.Grids;

namespace Alunite.Universe.Generation
{
    /// <summary>
    /// A compact, self-contained sequence of code in the "Universe Generation Language", including definitions for
    /// non-code entities. The assembly may have an implicit entry-point which populates the root regions of
    /// a <see cref="WorldTopology"/>.
    /// </summary>
    public sealed class UglAssembly
    {
        private List<Tile> _tiles;
        private byte[] _code;
        internal UglAssembly(List<Tile> tiles, byte[] code)
        {
            _tiles = tiles;
            _code = code;
        }
        
        /// <summary>
        /// Identifies a point into a <see cref="UglAssembly"/> which can be used to populate the
        /// contents of a world region.
        /// </summary>
        public struct Procedure : IEquatable<Procedure>
        {
            internal Procedure(uint pos)
            {
                _pos = pos;
            }

            /// <summary>
            /// The position of the instruction at the beginning of this procedure.
            /// </summary>
            internal uint _pos;

            /// <summary>
            /// The typical entry point for a <see cref="UglAssembly"/>.
            /// </summary>
            public static Procedure Entry => new Procedure(0);

            public static bool operator ==(Procedure a, Procedure b)
            {
                return a._pos == b._pos;
            }

            public static bool operator !=(Procedure a, Procedure b)
            {
                return !(a == b);
            }

            public override bool Equals(object obj)
            {
                if (!(obj is Procedure))
                    return false;
                return this == (Procedure)obj;
            }

            bool IEquatable<Procedure>.Equals(Procedure other)
            {
                return this == other;
            }

            public override int GetHashCode()
            {
                return _pos.GetHashCode();
            }
        }

        /// <summary>
        /// Encapsulates the suspended state of a thread executing "Universe Generation Language" code.
        /// </summary>
        public sealed class Frame
        {
            internal Frame(
                FrameState state, Procedure proc, Seed seed, 
                byte[] data, byte beltLen, byte outLen_x, byte outLen_y)
            {
                State = state;
                Procedure = proc;
                Seed = seed;
                _data = data;
                _beltLen = beltLen;
                _outLen_x = outLen_x;
                _outLen_y = outLen_y;
            }

            /// <summary>
            /// Specifies how far along this frame is in processing its region.
            /// </summary>
            public FrameState State { get; }

            /// <summary>
            /// The procedure that the frame is currently paused at.
            /// </summary>
            public Procedure Procedure { get; }

            /// <summary>
            /// The random seed for the suspended thread.
            /// </summary>
            public Seed Seed { get; }

            /// <summary>
            /// Indicates whether this frame has finished processing its region.
            /// </summary>
            public bool IsTerminal => State >= FrameState.Chunk;
            
            /// <summary>
            /// Stores the data for the frame, including the current belt, and the data "yielded" to
            /// the X, Y and Z directions.
            /// </summary>
            private readonly byte[] _data;

            /// <summary>
            /// The length of the belt stored in <see cref="_data"/>.
            /// </summary>
            internal readonly byte _beltLen;

            /// <summary>
            /// The length of X data stored in <see cref="_data"/>.
            /// </summary>
            private readonly byte _outLen_x;

            /// <summary>
            /// The length of Y data stored in <see cref="_data"/>.
            /// </summary>
            private readonly byte _outLen_y;

            /// <summary>
            /// The "belt" for the suspended thread.
            /// </summary>
            public SliceList<byte> Belt => new SliceList<byte>(_data, 0, _beltLen);

            /// <summary>
            /// The data transmitted from this frame towards the positive X direction.
            /// </summary>
            public SliceList<byte> Out_X => new SliceList<byte>(_data, _beltLen, _outLen_x);

            /// <summary>
            /// The data transmitted from this frame towards the positive Y direction. This is only available if
            /// <see cref="State"/> is <see cref="FrameState.Yield_Y"/> or later.
            /// </summary>
            public SliceList<byte> Out_Y => new SliceList<byte>(_data, (uint)(_beltLen + _outLen_x), _outLen_y);

            /// <summary>
            /// The data transmitted from this frame towards the positive Z direction. This is only available if
            /// <see cref="State"/> is <see cref="FrameState.Yield_Z"/> or later.
            /// </summary>
            public SliceList<byte> Out_Z
            {
                get
                {
                    uint offset = (uint)(_beltLen + _outLen_x + _outLen_y);
                    return new SliceList<byte>(_data, offset, (uint)_data.Length - offset);
                }
            }
        }

        /// <summary>
        /// Specifies how far along a <see cref="Frame"/> is in processing its region.
        /// </summary>
        public enum FrameState : byte
        {
            /// <summary>
            /// A sentinel value of <see cref="FrameState"/> used to indicate that a region has begun processing, but
            /// hasn't reached a pause yet. This will never be visible in <see cref="Frame.State"/>.
            /// </summary>
            _Entry,

            /// <summary>
            /// The thread is paused at a <see cref="UglOpCode.Yield_X"/> instruction, and is waiting for data from the
            /// the negative X direction. Data for the positive X direction is available.
            /// </summary>
            Yield_X,

            /// <summary>
            /// The thread is paused at a <see cref="UglOpCode.Yield_Y"/> instruction, and is waiting for data from the
            /// the negative Y direction. Data for the positive X and Y directions is available.
            /// </summary>
            Yield_Y,

            /// <summary>
            /// The thread is paused at a <see cref="UglOpCode.Yield_Z"/> instruction, and is waiting for data from the
            /// the negative Z direction. Data for the positive X, Y and Z directions is available.
            /// </summary>
            Yield_Z,

            /// <summary>
            /// The thread is paused at a <see cref="UglOpCode.Chunk"/> instruction, indicating that it should now be executed
            /// with a high-performance chunk generator, with no more pausing.
            /// </summary>
            Chunk,

            /// <summary>
            /// The thread has terminated at a <see cref="UglOpCode.Split"/> instruction. The entry points for the subregion
            /// frames, as well as data for the positive X, Y and Z directions, are available.
            /// </summary>
            Split
        }

        /// <summary>
        /// Exports a procedure defined in this assembly.
        /// </summary>
        public UglProcedure this[Procedure proc]
        {
            get
            {
                _Disassembler disassem = new _Disassembler(this, new MapBuilder<Procedure, UglProcedure>());
                return disassem.Disassemble(proc);
            }
        }

        /// <summary>
        /// Constructs a <see cref="Frame"/> to populate a root region in a world.
        /// </summary>
        /// <param name="seed">The global seed for the world.</param>
        public Frame Enter(Seed seed, WorldTopology.Region region)
        {
            // Allocate belt and output buffers
            Span<byte> buffers = stackalloc byte[1024];
            Span<byte> beltBuffer = buffers.Slice(0, 256);
            Span<byte> outBuffer = buffers.Slice(256, 768);
            _Belt belt = new _Belt(beltBuffer, 0);
            _Output output = new _Output(outBuffer);

            // Initialize belt
            _Belt.Put(ref belt, region.Index);
            _Belt.Put(ref belt, _summarize(region.X));
            _Belt.Put(ref belt, _summarize(region.Y));
            _Belt.Put(ref belt, _summarize(region.Z));

            // Create local seed
            Seed.AbsorbUInt16(ref seed, region.Index);
            Seed.AbsorbInt32(ref seed, region.X);
            Seed.AbsorbInt32(ref seed, region.Y);
            Seed.AbsorbInt32(ref seed, region.Z);

            // Run until the thread needs to be suspended
            return _run(Procedure.Entry, belt, seed, output);
        }

        /// <summary>
        /// Encodes an arbitrary coordinate value as a <see cref="byte"/> which indicates whether it is
        /// negative, zero or positive.
        /// </summary>
        private static byte _summarize(short coord)
        {
            if (coord < 0)
                return 0;
            else if (coord > 0)
                return 2;
            else
                return 1;
        }

        /// <summary>
        /// Continues execution of a frame in a <see cref="FrameState.Yield_X"/> state by providing the
        /// data received from the negative X direction.
        /// </summary>
        public Frame Continue_X(Frame frame, ReadOnlySpan<byte> in_x)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Continues execution of a frame in a <see cref="FrameState.Yield_Y"/> state by providing the
        /// data received from the negative X direction.
        /// </summary>
        public Frame Continue_Y(Frame frame, ReadOnlySpan<byte> in_y)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Continues execution of a frame in a <see cref="FrameState.Yield_Z"/> state by providing the
        /// data received from the negative X direction.
        /// </summary>
        public Frame Continue_Z(Frame frame, ReadOnlySpan<byte> in_z)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Produces a <see cref="ChunkPrototype"/> for the positive octant in an octet of regions whose contents
        /// are described by the given <see cref="Frame"/>s. All frames must be in a terminal (<see cref="FrameState.Chunk"/>
        /// or <see cref="FrameState.Split"/>) state. The frame in the positive octant must be in a
        /// <see cref="FrameState.Chunk"/> state.
        /// </summary>
        /// <remarks>Even though this only generates a chunk for one octant, the entire octet must be supplied to provide
        /// the data transmitted to the octant.</remarks>
        public unsafe ChunkPrototype Chunk(Octet<Frame> frames)
        {
            if (!frames.NegX_NegY_NegZ.IsTerminal ||
                !frames.PosX_NegY_NegZ.IsTerminal ||
                !frames.NegX_PosY_NegZ.IsTerminal ||
                !frames.PosX_PosY_NegZ.IsTerminal ||
                !frames.NegX_NegY_PosZ.IsTerminal ||
                !frames.PosX_NegY_PosZ.IsTerminal ||
                !frames.NegX_PosY_PosZ.IsTerminal)
                throw new ArgumentException("All frames must be in a terminal state", nameof(frames));
            if (frames.PosX_PosY_PosZ.State != FrameState.Chunk)
                throw new ArgumentException("Positive frame must be in a chunk state", nameof(frames));

            // Get information about the chunk
            uint head = frames.PosX_PosY_PosZ.Procedure._pos;
            UglOpCode op = (UglOpCode)Blob.ReadByte(_code, ref head);
            Debug.Assert(op == UglOpCode.Chunk);
            UglChunkMetadata metadata = UglChunkMetadata.Read(_code, ref head);

            // Determine the maximum belt length among the passed frames
            uint beltLen = frames.NegX_NegY_NegZ._beltLen;
            if (beltLen < frames.PosX_NegY_NegZ._beltLen)
                beltLen = frames.PosX_NegY_NegZ._beltLen;
            if (beltLen < frames.NegX_PosY_NegZ._beltLen)
                beltLen = frames.NegX_PosY_NegZ._beltLen;
            if (beltLen < frames.PosX_PosY_NegZ._beltLen)
                beltLen = frames.PosX_PosY_NegZ._beltLen;
            if (beltLen < frames.NegX_NegY_PosZ._beltLen)
                beltLen = frames.NegX_NegY_PosZ._beltLen;
            if (beltLen < frames.PosX_NegY_PosZ._beltLen)
                beltLen = frames.PosX_NegY_PosZ._beltLen;
            if (beltLen < frames.NegX_PosY_PosZ._beltLen)
                beltLen = frames.NegX_PosY_PosZ._beltLen;
            if (beltLen < frames.PosX_PosY_PosZ._beltLen)
                beltLen = frames.PosX_PosY_PosZ._beltLen;
            
            // Allocate workspace for belt and transmitted values
            byte* workspace = stackalloc byte[(int)(
                beltLen + metadata.BeltBound + 
                metadata.Bandwidth.X + 
                metadata.Bandwidth.Y + 
                metadata.Bandwidth.Z)];
            byte* belt = workspace + beltLen;
            byte* buffer_x = belt + metadata.BeltBound;
            byte* buffer_y = buffer_x + metadata.Bandwidth.X;
            byte* buffer_z = buffer_y + metadata.Bandwidth.Y;
            byte* end = buffer_z + metadata.Bandwidth.Z;

            // Set up wires
            Axial3<_Wire> wires = default;
            _Wire.Region null_x = new _Wire.Region(buffer_y, buffer_y, null);
            _Wire.Region null_y = new _Wire.Region(buffer_z, buffer_z, null);
            _Wire.Region null_z = new _Wire.Region(end, end, null);

            // Begin by "priming" the data transmitted to the positive chunk
            wires.Z.In = null_z;
            wires.Z.OutHead = buffer_z;
            {
                wires.Y.In = null_y;
                wires.Y.OutHead = buffer_y;
                {
                    wires.X.In = null_x;
                    wires.X.OutHead = buffer_x;
                    _populateBelt(belt, frames.NegX_NegY_NegZ.Belt);
                    new _Generator(this).Generate(
                        _GeneratorMode.Interior_NegX_NegY_NegZ,
                        frames.NegX_NegY_NegZ.Procedure, belt,
                        frames.NegX_NegY_NegZ.Seed,
                        ref wires);
                    wires.X.In = new _Wire.Region(buffer_x, wires.X.OutHead, &null_x);
                    wires.X.OutHead = buffer_x;
                    _populateBelt(belt, frames.PosX_NegY_NegZ.Belt);
                    new _Generator(this).Generate(
                        _GeneratorMode.Interior_PosX_NegY_NegZ,
                        frames.PosX_NegY_NegZ.Procedure, belt,
                        frames.PosX_NegY_NegZ.Seed,
                        ref wires);
                }
                wires.Y.In = new _Wire.Region(buffer_y, wires.Y.OutHead, &null_y);
                wires.Y.OutHead = buffer_y;
                {
                    wires.X.In = null_x;
                    wires.X.OutHead = buffer_x;
                    _populateBelt(belt, frames.NegX_PosY_NegZ.Belt);
                    new _Generator(this).Generate(
                        _GeneratorMode.Interior_NegX_PosY_NegZ,
                        frames.NegX_PosY_NegZ.Procedure, belt,
                        frames.NegX_PosY_NegZ.Seed,
                        ref wires);
                    wires.X.In = new _Wire.Region(buffer_x, wires.X.OutHead, &null_x);
                    wires.X.OutHead = buffer_x;
                    _populateBelt(belt, frames.PosX_PosY_NegZ.Belt);
                    new _Generator(this).Generate(
                        _GeneratorMode.Interior_PosX_PosY_NegZ,
                        frames.PosX_PosY_NegZ.Procedure, belt,
                        frames.PosX_PosY_NegZ.Seed,
                        ref wires);
                }
            }
            wires.Z.In = new _Wire.Region(buffer_z, wires.Z.OutHead, &null_z);
            wires.Z.OutHead = buffer_z;
            {
                wires.Y.In = null_y;
                wires.Y.OutHead = buffer_y;
                {
                    wires.X.In = null_x;
                    wires.X.OutHead = buffer_x;
                    _populateBelt(belt, frames.NegX_NegY_PosZ.Belt);
                    new _Generator(this).Generate(
                        _GeneratorMode.Interior_NegX_NegY_PosZ,
                        frames.NegX_NegY_PosZ.Procedure, belt,
                        frames.NegX_NegY_PosZ.Seed,
                        ref wires);
                    wires.X.In = new _Wire.Region(buffer_x, wires.X.OutHead, &null_x);
                    wires.X.OutHead = buffer_x;
                    _populateBelt(belt, frames.PosX_NegY_PosZ.Belt);
                    new _Generator(this).Generate(
                        _GeneratorMode.Interior_PosX_NegY_PosZ,
                        frames.PosX_NegY_PosZ.Procedure, belt,
                        frames.PosX_NegY_PosZ.Seed,
                        ref wires);
                }
                wires.Y.In = new _Wire.Region(buffer_y, wires.Y.OutHead, &null_y);
                wires.Y.OutHead = buffer_y;
                {
                    wires.X.In = null_x;
                    wires.X.OutHead = buffer_x;
                    _populateBelt(belt, frames.NegX_PosY_PosZ.Belt);
                    new _Generator(this).Generate(
                        _GeneratorMode.Interior_NegX_PosY_PosZ,
                        frames.NegX_PosY_PosZ.Procedure, belt,
                        frames.NegX_PosY_PosZ.Seed,
                        ref wires);
                    wires.X.In = new _Wire.Region(buffer_x, wires.X.OutHead, &null_x);
                    wires.X.OutHead = buffer_x;
                }
            }

            // Now that all the input data is set up, do the actual chunk generation
            OctreeFactory<Tile> octrees = new OctreeFactory<Tile>();
            _populateBelt(belt, frames.PosX_PosY_PosZ.Belt);
            OctreeDictionary.Octree octree = new _Generator(this, octrees).Generate(
                _GeneratorMode.Interior_PosX_PosY_PosZ,
                frames.PosX_PosY_PosZ.Procedure, belt,
                frames.PosX_PosY_PosZ.Seed,
                ref wires);
            return new ChunkPrototype(new OctreeGrid<Tile>(octrees, octree, metadata.Scale));
        }

        /// <summary>
        /// Given the head pointer for a belt, populates the prior contents of the belt with the given data.
        /// </summary>
        private static unsafe void _populateBelt(byte* belt, ReadOnlySpan<byte> data)
        {
            Binary.Copy(data, belt - data.Length);
        }

        /// <summary>
        /// Gets the frames for the subregions of a frame in a <see cref="FrameState.Chunk"/> or
        /// <see cref="FrameState.Split"/> state.
        /// </summary>
        public Octet<Frame> Split(Frame frame)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Executes a <see cref="Procedure"/> until it hits one of the paused states identified
        /// by <see cref="FrameState"/>. This constructs a <see cref="Frame"/> to encapsulate the suspended state
        /// of the thread.
        /// </summary>
        private Frame _run(Procedure entry, _Belt belt, Seed seed, _Output output)
        {
            byte[] code = _code;
            uint head = entry._pos;
        next:
            // Interpret and execute next instruction
            UglOpCode op = (UglOpCode)Blob.ReadByte(code, ref head);
            switch(op)
            {
                case UglOpCode.Put:
                    {
                        byte value = Blob.ReadByte(code, ref head);
                        _Belt.Put(ref belt, value);
                        goto next;
                    }
                case UglOpCode.Br:
                    {
                        ushort target = Blob.ReadUInt16(code, ref head);
                        head += target;
                        goto next;
                    }
                case UglOpCode.Coin_U1_S:
                    {
                        uint pivot = Blob.ReadByte(code, ref head);
                        byte offset = Blob.ReadByte(code, ref head);
                        if (Seed.SampleByte(ref seed) < pivot)
                            head += offset;
                        goto next;
                    }
                case UglOpCode.Coin_U2_S:
                    {
                        uint pivot = Blob.ReadUInt16(code, ref head);
                        byte offset = Blob.ReadByte(code, ref head);
                        if (Seed.SampleUInt16(ref seed) < pivot)
                            head += offset;
                        goto next;
                    }
                case UglOpCode.Coin_U4_S:
                    {
                        uint pivot = Blob.ReadUInt32(code, ref head);
                        byte offset = Blob.ReadByte(code, ref head);
                        if (Seed.SampleUInt32(ref seed) < pivot)
                            head += offset;
                        goto next;
                    }
                case UglOpCode.Die_2_S:
                    {
                        uint value = Seed.SampleByte(ref seed);
                        byte target = code[head + (value % 2)];
                        head += 2;
                        head += target;
                        goto next;
                    }
                case UglOpCode.Die_4_S:
                    {
                        uint value = Seed.SampleByte(ref seed);
                        byte target = code[head + (value % 4)];
                        head += 4;
                        head += target;
                        goto next;
                    }
                case UglOpCode.Die_8_S:
                    {
                        uint value = Seed.SampleByte(ref seed);
                        byte target = code[head + (value % 8)];
                        head += 8;
                        head += target;
                        goto next;
                    }
                case UglOpCode.Die_16_S:
                    {
                        uint value = Seed.SampleByte(ref seed);
                        byte target = code[head + (value % 16)];
                        head += 16;
                        head += target;
                        goto next;
                    }
                case UglOpCode.Die_32_S:
                    {
                        uint value = Seed.SampleByte(ref seed);
                        byte target = code[head + (value % 32)];
                        head += 32;
                        head += target;
                        goto next;
                    }
                case UglOpCode.Chunk:
                    {
                        output.State = FrameState.Chunk;
                        head--; // Rewind to beginning of chunk instruction
                        goto suspend;
                    }
                case UglOpCode.Put_0:
                case UglOpCode.Put_1:
                case UglOpCode.Put_2:
                case UglOpCode.Put_3:
                case UglOpCode.Put_4:
                case UglOpCode.Put_5:
                case UglOpCode.Put_6:
                case UglOpCode.Put_7:
                case UglOpCode.Put_8:
                case UglOpCode.Put_9:
                case UglOpCode.Put_10:
                case UglOpCode.Put_11:
                case UglOpCode.Put_12:
                case UglOpCode.Put_13:
                case UglOpCode.Put_14:
                case UglOpCode.Put_15:
                    {
                        byte value = op - UglOpCode.Put_0;
                        _Belt.Put(ref belt, value);
                        goto next;
                    }
                case UglOpCode.Dup_M1:
                case UglOpCode.Dup_M2:
                case UglOpCode.Dup_M3:
                case UglOpCode.Dup_M4:
                case UglOpCode.Dup_M5:
                case UglOpCode.Dup_M6:
                case UglOpCode.Dup_M7:
                case UglOpCode.Dup_M8:
                case UglOpCode.Dup_M9:
                case UglOpCode.Dup_M10:
                case UglOpCode.Dup_M11:
                case UglOpCode.Dup_M12:
                case UglOpCode.Dup_M13:
                case UglOpCode.Dup_M14:
                case UglOpCode.Dup_M15:
                case UglOpCode.Dup_M16:
                    {
                        byte offset = (op - UglOpCode.Dup_M1);
                        byte value = belt.Read(offset);
                        _Belt.Put(ref belt, value);
                        goto next;
                    }
                case UglOpCode.Br_S:
                    {
                        byte target = Blob.ReadByte(code, ref head);
                        head += target;
                        goto next;
                    }
                case UglOpCode.Switch_M1_S:
                case UglOpCode.Switch_M2_S:
                case UglOpCode.Switch_M3_S:
                case UglOpCode.Switch_M4_S:
                case UglOpCode.Switch_M5_S:
                case UglOpCode.Switch_M6_S:
                case UglOpCode.Switch_M7_S:
                case UglOpCode.Switch_M8_S:
                case UglOpCode.Switch_M9_S:
                case UglOpCode.Switch_M10_S:
                case UglOpCode.Switch_M11_S:
                case UglOpCode.Switch_M12_S:
                case UglOpCode.Switch_M13_S:
                case UglOpCode.Switch_M14_S:
                case UglOpCode.Switch_M15_S:
                    {
                        byte offset = op - UglOpCode.Switch_M1_S;
                        byte value = belt.Read(offset);
                        byte bound = Blob.ReadByte(code, ref head);
                        if (!(value <= bound))
                            throw new Exception("Switch table overflow");
                        byte brs = code[head + value];
                        head += (uint)bound + 1;
                        head += brs;
                        goto next;
                    }
                default:
                    throw new NotImplementedException();
            }

        suspend:
            // Concatenate belt and output data
            byte beltLen = belt.Length;
            byte outLen_x = output.Length_X;
            byte outLen_y = output.Length_Y;
            byte outLen_z = output.Length_Z;
            byte[] data = new byte[beltLen + outLen_x + outLen_y + outLen_z];
            belt.CopyTo(data);
            output.Buffer.Slice(0, outLen_x).CopyTo(data.AsSpan(beltLen));
            output.Buffer.Slice(outLen_x, outLen_y).CopyTo(data.AsSpan(beltLen + outLen_x));
            output.Buffer.Slice(outLen_x + outLen_y, outLen_z).CopyTo(data.AsSpan(beltLen + outLen_x + outLen_y));

            // Suspend thread as a Frame
            return new Frame(
                output.State, new Procedure(head), seed,
                data, beltLen, outLen_x, outLen_y);
        }

        /// <summary>
        /// A circular buffer which maintains the belt during execution of a procedure.
        /// </summary>
        private ref struct _Belt
        {
            private uint _head;
            public _Belt(Span<byte> buffer, byte len)
            {
                Buffer = buffer;
                _head = len;
            }

            /// <summary>
            /// A 256 byte buffer used to store the belt.
            /// </summary>
            public Span<byte> Buffer { get; }

            /// <summary>
            /// The length of the belt.
            /// </summary>
            public byte Length => (byte)Math.Min(_head, 255);

            /// <summary>
            /// Appends a value to the belt.
            /// </summary>
            public static void Put(ref _Belt belt, byte value)
            {
                belt.Buffer[(int)(belt._head++ % 256)] = value;
            }

            /// <summary>
            /// Gets the item at the given offset from the end of the belt.
            /// </summary>
            public byte Read(byte offset)
            {
                if (!(offset < _head))
                    throw new Exception("Belt read out of bounds");
                return Buffer[(int)(_head - offset - 1) % 256];
            }

            /// <summary>
            /// Copies the contents of the belt to the given span.
            /// </summary>
            public void CopyTo(Span<byte> span)
            {
                if (_head > 256)
                {
                    // Copy first part of circular buffer (after head)
                    throw new NotImplementedException();
                }

                // Copy second part of circular buffer (before head)
                Buffer.Slice(0, (int)(_head % 256)).CopyTo(span);
            }
        }

        /// <summary>
        /// Encapsulates the data transmitted by a <see cref="Procedure"/> up to a given point.
        /// </summary>
        private ref struct _Output
        {
            public _Output(Span<byte> buffer)
            {
                Buffer = buffer;
                State = FrameState._Entry;
                Length_X = default;
                Length_Y = default;
                Length_Z = default;
            }

            /// <summary>
            /// The buffer containing the concatenation of all the data transmitted by the procedure so far,
            /// in X, Y, Z order. There should be extra space at the end of the buffer to allow additional
            /// data to be added.
            /// </summary>
            public Span<byte> Buffer;

            /// <summary>
            /// Specifies which data has been transmitted so far. This may be <see cref="FrameState._Entry"/> for
            /// none, <see cref="FrameState.Yield_X"/>, <see cref="FrameState.Yield_Y"/> or
            /// <see cref="FrameState.Yield_Z"/>.
            /// </summary>
            public FrameState State;

            /// <summary>
            /// The amount of data transmitted in the X direction, if applicable.
            /// </summary>
            public byte Length_X;

            /// <summary>
            /// The amount of data transmitted in the Y direction, if applicable.
            /// </summary>
            public byte Length_Y;

            /// <summary>
            /// The amount of data transmitted in the Z direction, if applicable.
            /// </summary>
            public byte Length_Z;
        }

        /// <summary>
        /// A helper class for executing the UGL code for an entire region, without pausing, either to produce a
        /// <see cref="ChunkPrototype"/> or to prime data for a neighboring chunk.
        /// </summary>
        private struct _Generator
        {
            public _Generator(UglAssembly assem, OctreeFactory<Tile> octrees)
            {
                Assembly = assem;
                Octrees = octrees;
                _cache = new TempCache<(_GeneratorMode, Procedure), _Static>(64);
            }

            public _Generator(UglAssembly assem)
            {
                Assembly = assem;
                Octrees = null;
                _cache = new TempCache<(_GeneratorMode, Procedure), _Static>(32);
            }

            /// <summary>
            /// The assembly containing the UGL code to be executed.
            /// </summary>
            public UglAssembly Assembly { get; }

            /// <summary>
            /// The factory in which the octrees for the chunk are defined, or <see cref="null"/> if this generator is
            /// working in a mode that does not produce octree data.
            /// </summary>
            public OctreeFactory<Tile> Octrees { get; }

            /// <summary>
            /// A cache of the results of the static procedures executed by this generator.
            /// </summary>
            private readonly TempCache<(_GeneratorMode, Procedure), _Static> _cache;

            /// <summary>
            /// Provides information about the content generated by a static procedure.
            /// </summary>
            private struct _Static
            {
                public _Static(OctreeDictionary.Octree node,
                    ushort numIn_x, ushort numIn_y, ushort numIn_z,
                    byte[] data, ushort numOut_x, ushort numOut_y)
                {
                    Node = node;
                    NumIn_X = numIn_x;
                    NumIn_Y = numIn_y;
                    NumIn_Z = numIn_z;
                    _data = data;
                    NumOut_X = numOut_x;
                    NumOut_Y = numOut_y;
                }
                
                /// <summary>
                /// Stores the data for <see cref="Out_X"/>, <see cref="Out_Y"/> and <see cref="Out_Z"/>.
                /// </summary>
                private readonly byte[] _data;

                /// <summary>
                /// The octree node generated by the procedure.
                /// </summary>
                public OctreeDictionary.Octree Node { get; }

                /// <summary>
                /// The number of input items read (and ignored) from the X direction.
                /// </summary>
                public ushort NumIn_X { get; }

                /// <summary>
                /// The number of input items read (and ignored) from the Y direction.
                /// </summary>
                public ushort NumIn_Y { get; }

                /// <summary>
                /// The number of input items read (and ignored) from the Z direction.
                /// </summary>
                public ushort NumIn_Z { get; }

                /// <summary>
                /// The number of input items written towards the X direction.
                /// </summary>
                public ushort NumOut_X { get; }

                /// <summary>
                /// The number of input items written towards the Y direction.
                /// </summary>
                public ushort NumOut_Y { get; }

                /// <summary>
                /// The number of input items written towards the Z direction.
                /// </summary>
                public ushort NumOut_Z => (ushort)(_data.Length - (NumOut_X + NumOut_Y));

                /// <summary>
                /// The output items written towards the X direction.
                /// </summary>
                public SliceList<byte> Out_X => new SliceList<byte>(_data, 0, NumOut_X);

                /// <summary>
                /// The output items written towards the X direction.
                /// </summary>
                public SliceList<byte> Out_Y => new SliceList<byte>(_data, NumOut_X, NumOut_Y);

                /// <summary>
                /// The output items written towards the X direction.
                /// </summary>
                public SliceList<byte> Out_Z => new SliceList<byte>(_data, (uint)(NumOut_X + NumOut_Y), NumOut_Z);
            }

            /// <summary>
            /// Executes UGL code in <see cref="Assembly"/> to populate a region in the resulting chunk.
            /// </summary>
            /// <param name="belt">A pointer to the head of the "belt" at the current point.</param>
            /// <param name="wires">A collection of <see cref="_Wire"/>s used to receive and transmit data along
            /// a given axis.</param>
            public unsafe OctreeDictionary.Octree Generate(_GeneratorMode mode,
                Procedure proc, byte* belt, Seed seed,
                ref Axial3<_Wire> wires)
            {
                byte[] code = Assembly._code;
                uint head = proc._pos;
            next:
                // Interpret and execute next instruction
                UglOpCode op = (UglOpCode)Blob.ReadByte(code, ref head);
                switch (op)
                {
                    case UglOpCode.Put:
                        {
                            byte value = Blob.ReadByte(code, ref head);
                            *belt++ = value;
                            goto next;
                        }
                    case UglOpCode.Br:
                        {
                            ushort target = Blob.ReadUInt16(code, ref head);
                            head += target;
                            goto next;
                        }
                    case UglOpCode.Coin_U1_S:
                        {
                            uint pivot = Blob.ReadByte(code, ref head);
                            byte offset = Blob.ReadByte(code, ref head);
                            if (Seed.SampleByte(ref seed) < pivot)
                                head += offset;
                            goto next;
                        }
                    case UglOpCode.Coin_U2_S:
                        {
                            uint pivot = Blob.ReadUInt16(code, ref head);
                            byte offset = Blob.ReadByte(code, ref head);
                            if (Seed.SampleUInt16(ref seed) < pivot)
                                head += offset;
                            goto next;
                        }
                    case UglOpCode.Coin_U4_S:
                        {
                            uint pivot = Blob.ReadUInt32(code, ref head);
                            byte offset = Blob.ReadByte(code, ref head);
                            if (Seed.SampleUInt32(ref seed) < pivot)
                                head += offset;
                            goto next;
                        }
                    case UglOpCode.Die_2_S:
                        {
                            uint value = Seed.SampleByte(ref seed);
                            byte target = code[head + (value % 2)];
                            head += 2;
                            head += target;
                            goto next;
                        }
                    case UglOpCode.Die_4_S:
                        {
                            uint value = Seed.SampleByte(ref seed);
                            byte target = code[head + (value % 4)];
                            head += 4;
                            head += target;
                            goto next;
                        }
                    case UglOpCode.Die_8_S:
                        {
                            uint value = Seed.SampleByte(ref seed);
                            byte target = code[head + (value % 8)];
                            head += 8;
                            head += target;
                            goto next;
                        }
                    case UglOpCode.Die_16_S:
                        {
                            uint value = Seed.SampleByte(ref seed);
                            byte target = code[head + (value % 16)];
                            head += 16;
                            head += target;
                            goto next;
                        }
                    case UglOpCode.Die_32_S:
                        {
                            uint value = Seed.SampleByte(ref seed);
                            byte target = code[head + (value % 32)];
                            head += 32;
                            head += target;
                            goto next;
                        }
                    case UglOpCode.Yield_X:
                        {
                            byte numOut = Blob.ReadByte(code, ref head);
                            byte numIn = Blob.ReadByte(code, ref head);
                            if (mode.IsBoundary_X)
                            {
                                _Wire.SendBytes(ref wires.X, new ReadOnlySpan<byte>(belt - numOut, numOut));
                                return default;
                            }
                            else
                            {
                                byte* beltStart = belt;
                                Blob.WriteRaw(ref belt, _Wire.ReceiveBytes(ref wires.X, numIn));
                                _Wire.SendBytes(ref wires.X, new ReadOnlySpan<byte>(beltStart - numOut, numOut));
                                goto next;
                            }
                        }
                    case UglOpCode.Yield_Y:
                        {
                            byte numOut = Blob.ReadByte(code, ref head);
                            byte numIn = Blob.ReadByte(code, ref head);
                            if (mode.IsBoundary_Y)
                            {
                                _Wire.SendBytes(ref wires.Y, new ReadOnlySpan<byte>(belt - numOut, numOut));
                                return default;
                            }
                            else if (!mode.IsBoundary_X)
                            {
                                byte* beltStart = belt;
                                Blob.WriteRaw(ref belt, _Wire.ReceiveBytes(ref wires.Y, numIn));
                                _Wire.SendBytes(ref wires.Y, new ReadOnlySpan<byte>(beltStart - numOut, numOut));
                                goto next;
                            }
                            else
                            {
                                return default;
                            }
                        }
                    case UglOpCode.Yield_Z:
                        {
                            byte numOut = Blob.ReadByte(code, ref head);
                            byte numIn = Blob.ReadByte(code, ref head);
                            if (mode.IsBoundary_Z)
                            {
                                _Wire.SendBytes(ref wires.Z, new ReadOnlySpan<byte>(belt - numOut, numOut));
                                return default;
                            }
                            else if (mode.IsInterior)
                            {
                                byte* beltStart = belt;
                                Blob.WriteRaw(ref belt, _Wire.ReceiveBytes(ref wires.Z, numIn));
                                _Wire.SendBytes(ref wires.Z, new ReadOnlySpan<byte>(beltStart - numOut, numOut));
                                goto next;
                            }
                            else
                            {
                                return default;
                            }
                        }
                    case UglOpCode.Chunk:
                        head += UglChunkMetadata.BlobSize;
                        goto next;
                    case UglOpCode.Split:
                        {
                            // Processing of splits is only required for interior modes
                            if (!mode.IsInterior)
                                return default;

                            // Get offsets for subregions
                            byte target_negX_negY_negZ = Blob.ReadByte(code, ref head);
                            byte target_posX_negY_negZ = Blob.ReadByte(code, ref head);
                            byte target_negX_posY_negZ = Blob.ReadByte(code, ref head);
                            byte target_posX_posY_negZ = Blob.ReadByte(code, ref head);
                            byte target_negX_negY_posZ = Blob.ReadByte(code, ref head);
                            byte target_posX_negY_posZ = Blob.ReadByte(code, ref head);
                            byte target_negX_posY_posZ = Blob.ReadByte(code, ref head);
                            byte target_posX_posY_posZ = Blob.ReadByte(code, ref head);

                            // Generate octrees for children. We have to do a bit of clever shuffling
                            // of wires to ensure all the data is routed between the octants correctly.
                            OctreeDictionary.Octet children = default;
                            byte* outStart_z = wires.Z.OutHead;
                            {
                                byte* outStart_y = wires.Y.OutHead;
                                {
                                    byte* outStart_x = wires.X.OutHead;
                                    children.NegX_NegY_NegZ = Generate(
                                        mode.Child(Octant.NegX_NegY_NegZ),
                                        new Procedure(head + target_negX_negY_negZ), belt,
                                        _seed_negX_negY_negZ(seed), ref wires);
                                    _Wire.Region remIn_x = wires.X.In;
                                    wires.X.In = new _Wire.Region(outStart_x, wires.X.OutHead, &remIn_x);
                                    wires.X.OutHead = outStart_x;
                                    children.PosX_NegY_NegZ = Generate(
                                        mode.Child(Octant.PosX_NegY_NegZ),
                                        new Procedure(head + target_posX_negY_negZ), belt,
                                        _seed_posX_negY_negZ(seed), ref wires);
                                    wires.X.In = remIn_x;
                                }
                                _Wire.Region remIn_y = wires.Y.In;
                                wires.Y.In = new _Wire.Region(outStart_y, wires.Y.OutHead, &remIn_y);
                                wires.Y.OutHead = outStart_y;
                                {
                                    byte* outStart_x = wires.X.OutHead;
                                    children.NegX_PosY_NegZ = Generate(
                                        mode.Child(Octant.NegX_PosY_NegZ),
                                        new Procedure(head + target_negX_posY_negZ), belt,
                                        _seed_negX_posY_negZ(seed), ref wires);
                                    _Wire.Region remIn_x = wires.X.In;
                                    wires.X.In = new _Wire.Region(outStart_x, wires.X.OutHead, &remIn_x);
                                    wires.X.OutHead = outStart_x;
                                    children.PosX_PosY_NegZ = Generate(
                                        mode.Child(Octant.PosX_PosY_NegZ),
                                        new Procedure(head + target_posX_posY_negZ), belt,
                                        _seed_posX_posY_negZ(seed), ref wires);
                                    wires.X.In = remIn_x;
                                    outStart_x = wires.X.OutHead;
                                }
                                wires.Y.In = remIn_y;
                            }
                            _Wire.Region remIn_z = wires.Z.In;
                            wires.Z.In = new _Wire.Region(outStart_z, wires.Z.OutHead, &remIn_z);
                            wires.Z.OutHead = outStart_z;
                            {
                                byte* outStart_y = wires.Y.OutHead;
                                {
                                    byte* outStart_x = wires.X.OutHead;
                                    children.NegX_NegY_PosZ = Generate(
                                        mode.Child(Octant.NegX_NegY_PosZ),
                                        new Procedure(head + target_negX_negY_posZ), belt,
                                        _seed_negX_negY_posZ(seed), ref wires);
                                    _Wire.Region remIn_x = wires.X.In;
                                    wires.X.In = new _Wire.Region(outStart_x, wires.X.OutHead, &remIn_x);
                                    wires.X.OutHead = outStart_x;
                                    children.PosX_NegY_PosZ = Generate(
                                        mode.Child(Octant.PosX_NegY_PosZ),
                                        new Procedure(head + target_posX_negY_posZ), belt,
                                        _seed_posX_negY_posZ(seed), ref wires);
                                    wires.X.In = remIn_x;
                                }
                                _Wire.Region remIn_y = wires.Y.In;
                                wires.Y.In = new _Wire.Region(outStart_y, wires.Y.OutHead, &remIn_y);
                                wires.Y.OutHead = outStart_y;
                                {
                                    byte* outStart_x = wires.X.OutHead;
                                    children.NegX_PosY_PosZ = Generate(
                                        mode.Child(Octant.NegX_PosY_PosZ),
                                        new Procedure(head + target_negX_posY_posZ), belt,
                                        _seed_negX_posY_posZ(seed), ref wires);
                                    _Wire.Region remIn_x = wires.X.In;
                                    wires.X.In = new _Wire.Region(outStart_x, wires.X.OutHead, &remIn_x);
                                    wires.X.OutHead = outStart_x;
                                    children.PosX_PosY_PosZ = Generate(
                                        mode.Child(Octant.PosX_PosY_PosZ),
                                        new Procedure(head + target_posX_posY_posZ), belt,
                                        _seed_posX_posY_posZ(seed), ref wires);
                                    wires.X.In = remIn_x;
                                }
                                wires.Y.In = remIn_y;
                            }
                            wires.Z.In = remIn_z;

                            // Assemble grouped octree
                            if (Octrees != null)
                                return Octrees.Group(children);
                            else
                                return default;
                        }
                    case UglOpCode.Leaf:
                        {
                            if (Octrees != null)
                            {
                                uint tileToken = Blob.ReadUInt32(code, ref head);
                                Tile tile = Assembly._tiles[tileToken];
                                return Octrees.Leaf(tile);
                            }
                            else
                            {
                                return default;
                            }
                        }
                    case UglOpCode.Static:
                        {
                            var key = (mode, new Procedure(head - 1));
                            if (_cache.TryGet(key, out _Static @static))
                            {
                                // Repeat operation in cache
                                _Wire.ReceiveBytes(ref wires.X, @static.NumIn_X);
                                _Wire.ReceiveBytes(ref wires.Y, @static.NumIn_Y);
                                _Wire.ReceiveBytes(ref wires.Z, @static.NumIn_Z);
                                _Wire.SendBytes(ref wires.X, @static.Out_X);
                                _Wire.SendBytes(ref wires.Y, @static.Out_Y);
                                _Wire.SendBytes(ref wires.Z, @static.Out_Z);
                                return @static.Node;
                            }
                            else
                            {
                                // Generate the static content for the first time
                                ushort startNumIn_x = wires.X.In.Num;
                                ushort startNumIn_y = wires.Y.In.Num;
                                ushort startNumIn_z = wires.Z.In.Num;
                                byte* startOut_x = wires.X.OutHead;
                                byte* startOut_y = wires.Y.OutHead;
                                byte* startOut_z = wires.Z.OutHead;
                                OctreeDictionary.Octree node = Generate(mode, new Procedure(head), belt, seed, ref wires);

                                // Build a cache for the content
                                byte[] data;
                                ushort inLen_x = (ushort)(startNumIn_x - wires.X.In.Num);
                                ushort inLen_y = (ushort)(startNumIn_y - wires.Y.In.Num);
                                ushort inLen_z = (ushort)(startNumIn_z - wires.Z.In.Num);
                                ushort outLen_x = (ushort)(wires.X.OutHead - startOut_x);
                                ushort outLen_y = (ushort)(wires.Y.OutHead - startOut_y);
                                ushort outLen_z = (ushort)(wires.Z.OutHead - startOut_z);
                                if (outLen_x + outLen_y + outLen_z == 0)
                                {
                                    data = Array.Empty<byte>();
                                }
                                else
                                {
                                    data = new byte[outLen_x + outLen_y + outLen_z];
                                    throw new NotImplementedException();
                                }
                                @static = new _Static(node, inLen_x, inLen_y, inLen_z, data, outLen_x, outLen_y);

                                // Add to cache and return
                                _cache.Add(key, @static);
                                return node;
                            }
                        }
                    case UglOpCode.Put_0:
                    case UglOpCode.Put_1:
                    case UglOpCode.Put_2:
                    case UglOpCode.Put_3:
                    case UglOpCode.Put_4:
                    case UglOpCode.Put_5:
                    case UglOpCode.Put_6:
                    case UglOpCode.Put_7:
                    case UglOpCode.Put_8:
                    case UglOpCode.Put_9:
                    case UglOpCode.Put_10:
                    case UglOpCode.Put_11:
                    case UglOpCode.Put_12:
                    case UglOpCode.Put_13:
                    case UglOpCode.Put_14:
                    case UglOpCode.Put_15:
                        {
                            byte value = op - UglOpCode.Put_0;
                            *belt++ = value;
                            goto next;
                        }
                    case UglOpCode.Dup_M1:
                    case UglOpCode.Dup_M2:
                    case UglOpCode.Dup_M3:
                    case UglOpCode.Dup_M4:
                    case UglOpCode.Dup_M5:
                    case UglOpCode.Dup_M6:
                    case UglOpCode.Dup_M7:
                    case UglOpCode.Dup_M8:
                    case UglOpCode.Dup_M9:
                    case UglOpCode.Dup_M10:
                    case UglOpCode.Dup_M11:
                    case UglOpCode.Dup_M12:
                    case UglOpCode.Dup_M13:
                    case UglOpCode.Dup_M14:
                    case UglOpCode.Dup_M15:
                    case UglOpCode.Dup_M16:
                        {
                            byte offset = op - UglOpCode.Dup_M1;
                            byte value = *(belt - offset - 1);
                            *belt++ = value;
                            goto next;
                        }
                    case UglOpCode.Br_S:
                        {
                            byte target = Blob.ReadByte(code, ref head);
                            head += target;
                            goto next;
                        }
                    case UglOpCode.Switch_M1_S:
                    case UglOpCode.Switch_M2_S:
                    case UglOpCode.Switch_M3_S:
                    case UglOpCode.Switch_M4_S:
                    case UglOpCode.Switch_M5_S:
                    case UglOpCode.Switch_M6_S:
                    case UglOpCode.Switch_M7_S:
                    case UglOpCode.Switch_M8_S:
                    case UglOpCode.Switch_M9_S:
                    case UglOpCode.Switch_M10_S:
                    case UglOpCode.Switch_M11_S:
                    case UglOpCode.Switch_M12_S:
                    case UglOpCode.Switch_M13_S:
                    case UglOpCode.Switch_M14_S:
                    case UglOpCode.Switch_M15_S:
                        {
                            byte offset = op - UglOpCode.Switch_M1_S;
                            byte value = *(belt - offset - 1);
                            byte bound = Blob.ReadByte(code, ref head);
                            if (!(value <= bound))
                                throw new Exception("Switch table overflow");
                            byte target = code[head + value];
                            head += (uint)bound + 1;
                            head += target;
                            goto next;
                        }
                    default:
                        throw new NotImplementedException();
                }
            }
        }

        /// <summary>
        /// When executing code using a <see cref="_Generator"/>, specifies what data is available as input, and consequently,
        /// what data will be generated as output.
        /// </summary>
        private struct _GeneratorMode
        {
            private byte _boundary;
            private Octant _interior;
            private _GeneratorMode(byte boundary, Octant interior)
            {
                _boundary = boundary;
                _interior = interior;
            }
            
            /// <summary>
            /// Gets the <see cref="_GeneratorMode"/> used to process a subregion of a region with 
            /// this <see cref="_GeneratorMode"/>. 
            /// </summary>
            public _GeneratorMode Child(Octant octant)
            {
                return new _GeneratorMode((byte)(_interior | octant), _interior);
            }

            #region Values
            public static _GeneratorMode Boundary_X => new _GeneratorMode(0b000, default);
            public static _GeneratorMode Boundary_Y => new _GeneratorMode(0b001, default);
            public static _GeneratorMode Boundary_Z => new _GeneratorMode(0b011, default);
            public static _GeneratorMode Interior_NegX_NegY_NegZ => new _GeneratorMode(0b111, Octant.NegX_NegY_NegZ);
            public static _GeneratorMode Interior_PosX_NegY_NegZ => new _GeneratorMode(0b111, Octant.PosX_NegY_NegZ);
            public static _GeneratorMode Interior_NegX_PosY_NegZ => new _GeneratorMode(0b111, Octant.NegX_PosY_NegZ);
            public static _GeneratorMode Interior_PosX_PosY_NegZ => new _GeneratorMode(0b111, Octant.PosX_PosY_NegZ);
            public static _GeneratorMode Interior_NegX_NegY_PosZ => new _GeneratorMode(0b111, Octant.NegX_NegY_PosZ);
            public static _GeneratorMode Interior_PosX_NegY_PosZ => new _GeneratorMode(0b111, Octant.PosX_NegY_PosZ);
            public static _GeneratorMode Interior_NegX_PosY_PosZ => new _GeneratorMode(0b111, Octant.NegX_PosY_PosZ);
            public static _GeneratorMode Interior_PosX_PosY_PosZ => new _GeneratorMode(0b111, Octant.PosX_PosY_PosZ);
            public bool IsBoundary_X => (_boundary & 0b1) == 0b0;
            public bool IsBoundary_Y => (_boundary & 0b11) == 0b01;
            public bool IsBoundary_Z => _boundary == 0b011;
            public bool IsInterior => _boundary == 0b111;
            #endregion
        }

        /// <summary>
        /// A buffer which maintains the data transmitted between regions along a certain axis during generation. This
        /// supports both reading to receive data from negative direction and writing to transmit data towards the
        /// positive direction. Reading and writing is done to the same buffer, with measures to ensure that data
        /// is not overwritten before it is read.
        /// </summary>
        private unsafe struct _Wire
        {
            public _Wire(byte* outHead, byte* inHead, byte* end)
            {
                OutHead = outHead;
                In.Head = inHead;
                In.End = end;
                In.Next = null;
            }

            public _Wire(byte* outHead, byte* end)
            {
                OutHead = outHead;
                In.Head = end;
                In.End = end;
                In.Next = null;
            }
            
            /// <summary>
            /// A pointer to the next byte to be written to the wire. This must at or before the start
            /// of <see cref="In"/>.
            /// </summary>
            public byte* OutHead;
            
            /// <summary>
            /// The buffer region being read by this <see cref="_Wire"/>.
            /// </summary>
            public Region In;
            
            /// <summary>
            /// Describes a potential input region for a <see cref="_Wire"/> buffer.
            /// </summary>
            public unsafe struct Region
            {
                public Region(byte* head, byte* end, Region* next)
                {
                    Head = head;
                    End = end;
                    Next = next;
                }

                /// <summary>
                /// A pointer to the next byte to be read from this source.
                /// </summary>
                public byte* Head;

                /// <summary>
                /// A pointer to the end of this source.
                /// </summary>
                public byte* End;
                
                /// <summary>
                /// The input source after this one, or <see cref="null"/> if this region can not be relocated (i.e. there
                /// is no free space anywhere after this region).
                /// </summary>
                public Region* Next;

                /// <summary>
                /// The number of bytes yet to be received for this region.
                /// </summary>
                public ushort Num => (ushort)(End - Head);

                /// <summary>
                /// Relocates all input sources at or after <paramref name="region"/> to the end of the buffer. This is used
                /// to make room for writing near the beginning of the buffer.
                /// </summary>
                /// <returns>The new <see cref="Head"/> for the region.</returns>
                public static byte* Relocate(ref Region region)
                {
                    if (region.Next != null)
                    {
                        // Relocate next region
                        byte* nEnd = Relocate(ref *region.Next);

                        // Move this region flush to the beginning of the next region
                        ulong num = (ulong)(region.End - region.Head);
                        byte* nHead = nEnd - num;
                        Binary.Copy(region.Head, nHead, num);
                        region.Head = nHead;
                        region.End = nEnd;
                        region.Next = null;
                        return nHead;
                    }
                    else
                    {
                        // The region is already flush with the end of the buffer
                        return region.Head;
                    }
                }

                public static implicit operator ReadOnlySpan<byte>(Region source)
                {
                    return new ReadOnlySpan<byte>(source.Head, (int)(source.End - source.Head));
                }
            }

            /// <summary>
            /// Receives the given number of bytes from the wire. The returned <see cref="ReadOnlySpan{T}"/> is only
            /// valid to the next send operation.
            /// </summary>
            public static ReadOnlySpan<byte> ReceiveBytes(ref _Wire wire, ushort num)
            {
                ReadOnlySpan<byte> res = new ReadOnlySpan<byte>(wire.In.Head, num);
                wire.In.Head += num;
                Debug.Assert(wire.In.Head <= wire.In.End);
                return res;
            }

            /// <summary>
            /// Sends the given bytes to the wire.
            /// </summary>
            public static void SendBytes(ref _Wire wire, ReadOnlySpan<byte> data)
            {
                // If there is a risk of overflow, relocate the input region to the end of the buffer
                byte* nOutHead = wire.OutHead + data.Length;
                if (nOutHead > wire.In.Head)
                {
                    byte* nInHead = Region.Relocate(ref wire.In);

                    // Ensure that the relocation was sufficient
                    Debug.Assert(nOutHead <= nInHead);
                }
                Binary.Copy(data, wire.OutHead);
                wire.OutHead = nOutHead;
            }
        }

        /// <summary>
        /// A helper class for decoding a <see cref="Procedure"/> into a <see cref="UglProcedure"/>.
        /// </summary>
        private struct _Disassembler
        {
            public _Disassembler(UglAssembly assem, MapBuilder<Procedure, UglProcedure> cache)
            {
                Assembly = assem;
                _cache = cache;
            }

            /// <summary>
            /// The assembly in which the <see cref="Procedure"/>s are defined.
            /// </summary>
            public UglAssembly Assembly { get; }

            /// <summary>
            /// A cache of procedures decoded so far.
            /// </summary>
            private MapBuilder<Procedure, UglProcedure> _cache;

            /// <summary>
            /// Gets the <see cref="UglProcedure"/> corresponding to the given procedure.
            /// </summary>
            public UglProcedure Disassemble(Procedure proc)
            {
                if (_cache.TryGet(proc, out UglProcedure res))
                    return res;
                res = _disassembleNoCache(proc);
                _cache.Add(proc, res);
                return res;
            }

            /// <summary>
            /// Disassembles the given procedure without checking the cache.
            /// </summary>
            private UglProcedure _disassembleNoCache(Procedure proc)
            {
                byte[] code = Assembly._code;
                uint head = proc._pos;
            next:
                // Interpret and execute next instruction
                UglOpCode op = (UglOpCode)Blob.ReadByte(code, ref head);
                switch (op)
                {
                    case UglOpCode.Put:
                        {
                            byte value = Blob.ReadByte(code, ref head);
                            return UglProcedure.Put(value, Disassemble(new Procedure(head)));
                        }
                    case UglOpCode.Br:
                        {
                            ushort target = Blob.ReadUInt16(code, ref head);
                            head += target;
                            goto next;
                        }
                    case UglOpCode.Coin_U1_S:
                    case UglOpCode.Coin_U2_S:
                    case UglOpCode.Coin_U4_S:
                    case UglOpCode.Die_2_S:
                    case UglOpCode.Die_4_S:
                    case UglOpCode.Die_8_S:
                    case UglOpCode.Die_16_S:
                    case UglOpCode.Die_32_S:
                        throw new NotImplementedException();
                    case UglOpCode.Yield_X:
                    case UglOpCode.Yield_Y:
                    case UglOpCode.Yield_Z:
                        {
                            byte numOut = Blob.ReadByte(code, ref head);
                            byte numIn = Blob.ReadByte(code, ref head);
                            UglProcedure cont = Disassemble(new Procedure(head));
                            return UglProcedure.Yield((Axis3)(op - UglOpCode.Yield_X), numOut, numIn, cont);
                        }
                    case UglOpCode.Chunk:
                        head += UglChunkMetadata.BlobSize;
                        goto next;
                    case UglOpCode.Split:
                        {
                            byte target_negX_negY_negZ = Blob.ReadByte(code, ref head);
                            byte target_posX_negY_negZ = Blob.ReadByte(code, ref head);
                            byte target_negX_posY_negZ = Blob.ReadByte(code, ref head);
                            byte target_posX_posY_negZ = Blob.ReadByte(code, ref head);
                            byte target_negX_negY_posZ = Blob.ReadByte(code, ref head);
                            byte target_posX_negY_posZ = Blob.ReadByte(code, ref head);
                            byte target_negX_posY_posZ = Blob.ReadByte(code, ref head);
                            byte target_posX_posY_posZ = Blob.ReadByte(code, ref head);
                            return UglProcedure.Split(new Octet<UglProcedure>(
                                Disassemble(new Procedure(head + target_negX_negY_negZ)),
                                Disassemble(new Procedure(head + target_posX_negY_negZ)),
                                Disassemble(new Procedure(head + target_negX_posY_negZ)),
                                Disassemble(new Procedure(head + target_posX_posY_negZ)),
                                Disassemble(new Procedure(head + target_negX_negY_posZ)),
                                Disassemble(new Procedure(head + target_posX_negY_posZ)),
                                Disassemble(new Procedure(head + target_negX_posY_posZ)),
                                Disassemble(new Procedure(head + target_posX_posY_posZ))));
                        }
                    case UglOpCode.Leaf:
                        {
                            uint tileToken = Blob.ReadUInt32(code, ref head);
                            Tile tile = Assembly._tiles[tileToken];
                            return UglProcedure.Leaf(tile);
                        }
                    case UglOpCode.Static:
                        goto next;
                    case UglOpCode.Put_0:
                    case UglOpCode.Put_1:
                    case UglOpCode.Put_2:
                    case UglOpCode.Put_3:
                    case UglOpCode.Put_4:
                    case UglOpCode.Put_5:
                    case UglOpCode.Put_6:
                    case UglOpCode.Put_7:
                    case UglOpCode.Put_8:
                    case UglOpCode.Put_9:
                    case UglOpCode.Put_10:
                    case UglOpCode.Put_11:
                    case UglOpCode.Put_12:
                    case UglOpCode.Put_13:
                    case UglOpCode.Put_14:
                    case UglOpCode.Put_15:
                        {
                            byte value = op - UglOpCode.Put_0;
                            return UglProcedure.Put(value, Disassemble(new Procedure(head)));
                        }
                    case UglOpCode.Dup_M1:
                    case UglOpCode.Dup_M2:
                    case UglOpCode.Dup_M3:
                    case UglOpCode.Dup_M4:
                    case UglOpCode.Dup_M5:
                    case UglOpCode.Dup_M6:
                    case UglOpCode.Dup_M7:
                    case UglOpCode.Dup_M8:
                    case UglOpCode.Dup_M9:
                    case UglOpCode.Dup_M10:
                    case UglOpCode.Dup_M11:
                    case UglOpCode.Dup_M12:
                    case UglOpCode.Dup_M13:
                    case UglOpCode.Dup_M14:
                    case UglOpCode.Dup_M15:
                    case UglOpCode.Dup_M16:
                        {
                            byte offset = op - UglOpCode.Dup_M1;
                            return UglProcedure.Dup(offset, Disassemble(new Procedure(head)));
                        }
                    case UglOpCode.Br_S:
                        {
                            byte target = Blob.ReadByte(code, ref head);
                            head += target;
                            goto next;
                        }
                    case UglOpCode.Switch_M1_S:
                    case UglOpCode.Switch_M2_S:
                    case UglOpCode.Switch_M3_S:
                    case UglOpCode.Switch_M4_S:
                    case UglOpCode.Switch_M5_S:
                    case UglOpCode.Switch_M6_S:
                    case UglOpCode.Switch_M7_S:
                    case UglOpCode.Switch_M8_S:
                    case UglOpCode.Switch_M9_S:
                    case UglOpCode.Switch_M10_S:
                    case UglOpCode.Switch_M11_S:
                    case UglOpCode.Switch_M12_S:
                    case UglOpCode.Switch_M13_S:
                    case UglOpCode.Switch_M14_S:
                    case UglOpCode.Switch_M15_S:
                        throw new NotImplementedException();
                    default:
                        throw new NotImplementedException();
                }
            }
        }

        /// <summary>
        /// Calculates the initial seed for a subregion of a split region.
        /// </summary>
        private static Seed _seed_negX_negY_negZ(Seed seed)
        {
            Seed.AbsorbUInt64(ref seed, 0x3fb68afba021ddfd);
            return seed;
        }

        /// <summary>
        /// Calculates the initial seed for a subregion of a split region.
        /// </summary>
        private static Seed _seed_posX_negY_negZ(Seed seed)
        {
            Seed.AbsorbUInt64(ref seed, 0xbb231c47e30fbf52);
            return seed;
        }

        /// <summary>
        /// Calculates the initial seed for a subregion of a split region.
        /// </summary>
        private static Seed _seed_negX_posY_negZ(Seed seed)
        {
            Seed.AbsorbUInt64(ref seed, 0x4a977e358fa881a3);
            return seed;
        }

        /// <summary>
        /// Calculates the initial seed for a subregion of a split region.
        /// </summary>
        private static Seed _seed_posX_posY_negZ(Seed seed)
        {
            Seed.AbsorbUInt64(ref seed, 0xb22d659c1fc30321);
            return seed;
        }

        /// <summary>
        /// Calculates the initial seed for a subregion of a split region.
        /// </summary>
        private static Seed _seed_negX_negY_posZ(Seed seed)
        {
            Seed.AbsorbUInt64(ref seed, 0xa2b0a06be78ae45f);
            return seed;
        }

        /// <summary>
        /// Calculates the initial seed for a subregion of a split region.
        /// </summary>
        private static Seed _seed_posX_negY_posZ(Seed seed)
        {
            Seed.AbsorbUInt64(ref seed, 0xf3eaaa7c8a00d7bc);
            return seed;
        }

        /// <summary>
        /// Calculates the initial seed for a subregion of a split region.
        /// </summary>
        private static Seed _seed_negX_posY_posZ(Seed seed)
        {
            Seed.AbsorbUInt64(ref seed, 0x27ab0925e0a288e4);
            return seed;
        }

        /// <summary>
        /// Calculates the initial seed for a subregion of a split region.
        /// </summary>
        private static Seed _seed_posX_posY_posZ(Seed seed)
        {
            Seed.AbsorbUInt64(ref seed, 0xc81889dd24fc4bd5);
            return seed;
        }
    }
}