﻿using System;
using System.Diagnostics;

using Alunite.Data;
using Alunite.Data.Geometry;
using Alunite.Data.Probability;
using Alunite.Universe.Grids;

#pragma warning disable CS0659
#pragma warning disable CS0660
#pragma warning disable CS0661

namespace Alunite.Universe.Generation
{
    /// <summary>
    /// A reusable snippet of "Universe Generation Language" code which can populate the contents of a world region.
    /// </summary>
    public abstract class UglProcedure
    {
        private readonly int _hash;
        internal UglProcedure(int hash, byte scale)
        {
            _hash = hash;
            Scale = scale;
        }

        internal UglProcedure(byte scale)
        {
            _hash = base.GetHashCode();
            Scale = scale;
        }

        /// <summary>
        /// The scale of the region populated by this <see cref="UglProcedure"/>.
        /// </summary>
        public byte Scale { get; }

        /// <summary>
        /// Constructs a <see cref="UglProcedure"/> which appends a byte constant to the belt and then executes
        /// the given procedure.
        /// </summary>
        public static UglProcedure Put(byte value, UglProcedure cont)
        {
            if (cont == null)
                return null;
            return new _UglPutProcedure(value, cont);
        }

        /// <summary>
        /// Constructs a <see cref="UglProcedure"/> which selects the item at the given offset from the end of the
        /// belt, appends a copy of it to the belt, and then executes the given procedure.
        /// </summary>
        public static UglProcedure Dup(byte offset, UglProcedure cont)
        {
            if (cont == null)
                return null;
            return new _UglDupProcedure(offset, cont);
        }

        /// <summary>
        /// Constructs a <see cref="UglProcedure"/> which executes one of the given procedures depending on the value
        /// of the item at the given offset from the end of the belt.
        /// </summary>
        public static UglProcedure Switch(byte offset, params UglProcedure[] targets)
        {
            return Switch(offset, (Span<UglProcedure>)targets);
        }

        /// <summary>
        /// Constructs a <see cref="UglProcedure"/> which executes one of the given procedures depending on the value
        /// of the item at the given offset from the end of the belt. <see cref="null"/> may be used as a placeholder
        /// for targets that will never be executed.
        /// </summary>
        public static UglProcedure Switch(byte offset, ReadOnlySpan<UglProcedure> targets)
        {
            // Search for last non-null target
            uint last = (uint)targets.Length;
            while (last > 0)
            {
                if (!(targets[(int)--last] is null))
                    goto foundLast;
            }
            return null;

        foundLast:
            uint len = last + 1;
            UglProcedure lastTarget = targets[(int)last];
            byte scale = lastTarget.Scale;

            // Check if all targets are the same. If so, we can elide the switch entirely
            for (int i = 0; i < last; i++)
            {
                UglProcedure target = targets[i];
                if (!(target is null) && target != lastTarget)
                    goto notAllSame;
            }
            return lastTarget;

        notAllSame:
            // Build array of targets
            UglProcedure[] nTargets = new UglProcedure[len];
            for (int i = 0; i < targets.Length; i++)
            {
                UglProcedure target = targets[i];
                nTargets[i] = target;
                if (!(target is null) && target.Scale != scale)
                    throw new ArgumentException("All targets must be of the same scale", nameof(targets));
            }
            return new _UglSwitchProcedure(scale, offset, targets.ToArray());
        }

        /// <summary>
        /// Constructs a <see cref="UglProcedure"/> which executes one of the given procedures depending on whether a
        /// boolean chosen with the given probability is true or false.
        /// </summary>
        public static UglProcedure Coin(LogScalar prob, UglProcedure onTrue, UglProcedure onFalse)
        {
            return Coin(BoolDistribution.FromTrue(prob), onTrue, onFalse);
        }

        /// <summary>
        /// Constructs a <see cref="UglProcedure"/> which executes one of the given procedures depending on whether a
        /// boolean chosen according to the given distribution is true or false. 
        /// </summary>
        public static UglProcedure Coin(BoolDistribution cond, UglProcedure onTrue, UglProcedure onFalse)
        {
            // Normalize
            if (cond.IsAlwaysTrue)
                return onTrue;
            if (cond.IsAlwaysFalse)
                return onFalse;
            if (onTrue is null || onFalse is null)
                return null;
            if (onTrue == onFalse)
                return onTrue;

            // Verify scale
            if (onTrue.Scale != onFalse.Scale)
                throw new ArgumentException("All targets must be of the same scale");

            // Attempt to rewrite as choice if there are multiple coin flips involved
            if (onTrue.IsChoice(out var onTrueDist))
            {
                if (onFalse.IsChoice(out var onFalseDist))
                    return new _UglChoiceProcedure(onTrue.Scale, 
                        (FiniteDistribution<UglProcedure>)(
                            cond.OfTrue * onTrueDist +
                            cond.OfFalse * onFalseDist));
                else
                    return new _UglChoiceProcedure(onTrue.Scale,
                        (FiniteDistribution<UglProcedure>)(
                            new PointKernel<UglProcedure>(onFalse, cond.OfFalse) +
                            cond.OfTrue * onTrueDist));
            }
            else if (onFalse.IsChoice(out var onFalseDist))
            {
                return new _UglChoiceProcedure(onTrue.Scale,
                    (FiniteDistribution<UglProcedure>)(
                        new PointKernel<UglProcedure>(onTrue, cond.OfTrue) +
                        cond.OfFalse * onFalseDist));
            }

            // Build procedure
            if (!cond.IsUnlikely(out UnlikelyBoolDistribution unlikelyCond))
                Util.Swap(ref onTrue, ref onFalse);
            return new _UglCoinProcedure(unlikelyCond, onTrue, onFalse);
        }

        /// <summary>
        /// Constructs a <see cref="UglProcedure"/> which executes one of the given procedures, chosen randomly from
        /// the given distribution.
        /// </summary>
        public static UglProcedure Choice(FiniteDistribution<UglProcedure> dist)
        {
            // Normalize
            var en = dist.GetEnumerator();
            bool hasItem = en.MoveNext();
            Debug.Assert(hasItem);
            PointKernel<UglProcedure> point_0 = en.Current;
            byte scale = point_0.Value.Scale;
            if (!en.MoveNext())
                return point_0.Value;
            PointKernel<UglProcedure> point_1 = en.Current;
            if (point_0.Value.Scale != point_1.Value.Scale)
                throw new ArgumentException("All targets must be of the same scale");
            if (!en.MoveNext())
            {
                // Two point choices should be rewritten as coin selections
                Debug.Assert(Scalar.Abs((point_0.Weight + point_1.Weight).Ln) < 0.001);
                if (point_0.Weight < point_1.Weight)
                    return new _UglCoinProcedure(
                        UnlikelyBoolDistribution.FromTrue(point_0.Weight),
                        point_0.Value, point_1.Value);
                else
                    return new _UglCoinProcedure(
                        UnlikelyBoolDistribution.FromTrue(point_1.Weight),
                        point_1.Value, point_0.Value);
            }


            // TODO: Normalization of nested choices
            return new _UglChoiceProcedure(scale, dist);
        }
        
        /// <summary>
        /// Constructs a <see cref="UglProcedure"/> which splits the current region into <see cref="Octant"/> subregions
        /// and executes the corresponding procedure to populate each one.
        /// </summary>
        public static UglProcedure Split(Octet<UglProcedure> targets)
        {
            // If any targets are null, the whole procedure is unreachable
            if (targets.NegX_NegY_NegZ is null)
                return null;
            if (targets.PosX_NegY_NegZ is null)
                return null;
            if (targets.NegX_PosY_NegZ is null)
                return null;
            if (targets.PosX_PosY_NegZ is null)
                return null;
            if (targets.NegX_NegY_PosZ is null)
                return null;
            if (targets.PosX_NegY_PosZ is null)
                return null;
            if (targets.NegX_PosY_PosZ is null)
                return null;
            if (targets.PosX_PosY_PosZ is null)
                return null;

            // Verify all targets have the same scale
            uint scale = targets.NegX_NegY_NegZ.Scale;
            if (targets.PosX_NegY_NegZ.Scale != scale ||
                targets.NegX_PosY_NegZ.Scale != scale ||
                targets.PosX_PosY_NegZ.Scale != scale ||
                targets.NegX_NegY_PosZ.Scale != scale ||
                targets.PosX_NegY_PosZ.Scale != scale ||
                targets.NegX_PosY_PosZ.Scale != scale ||
                targets.PosX_PosY_PosZ.Scale != scale)
            {
                throw new ArgumentException("All targets must be of the same scale");
            }

            // Build procedure
            return new _UglSplitProcedure((byte)(scale + 1), targets);
        }

        /// <summary>
        /// Constructs a <see cref="UglProcedure"/> which populates the current scale-0 region with the
        /// given <see cref="Tile"/>.
        /// </summary>
        public static UglProcedure Leaf(Tile tile)
        {
            return new _UglLeafProcedure(tile);
        }

        /// <summary>
        /// Transmits a fixed number of items to the region neighboring the current region towards the positive
        /// direction along the X axis, while simultaneously receiving analogous information from the
        /// negative direction along the same axis. If any items are transmitted by this call, the neighboring
        /// region must call yield to receive them. Yields within a region must occur in X, Y, Z order.
        /// </summary>
        /// <param name="numOut">The number of belt items transmitted.</param>
        /// <param name="numIn">The number of belt items to be received.</param>
        /// <param name="cont">The procedure executed after yielding.</param>
        public static UglProcedure Yield_X(byte numOut, byte numIn, UglProcedure cont)
        {
            return Yield(Axis3.X, numOut, numIn, cont);
        }

        /// <summary>
        /// Transmits a fixed number of items to the region neighboring the current region towards the positive
        /// direction along the Y axis, while simultaneously receiving analogous information from the
        /// negative direction along the same axis. If any items are transmitted by this call, the neighboring
        /// region must call yield to receive them. Yields within a region must occur in X, Y, Z order.
        /// </summary>
        /// <param name="numOut">The number of belt items transmitted.</param>
        /// <param name="numIn">The number of belt items to be received.</param>
        /// <param name="cont">The procedure executed after yielding.</param>
        public static UglProcedure Yield_Y(byte numOut, byte numIn, UglProcedure cont)
        {
            return Yield(Axis3.Y, numOut, numIn, cont);
        }

        /// <summary>
        /// Transmits a fixed number of items to the region neighboring the current region towards the positive
        /// direction along the Z axis, while simultaneously receiving analogous information from the
        /// negative direction along the same axis. If any items are transmitted by this call, the neighboring
        /// region must call yield to receive them. Yields within a region must occur in X, Y, Z order.
        /// </summary>
        /// <param name="numOut">The number of belt items transmitted.</param>
        /// <param name="numIn">The number of belt items to be received.</param>
        /// <param name="cont">The procedure executed after yielding.</param>
        public static UglProcedure Yield_Z(byte numOut, byte numIn, UglProcedure cont)
        {
            return Yield(Axis3.Z, numOut, numIn, cont);
        }

        /// <summary>
        /// Transmits a fixed number of items to the region neighboring the current region towards the positive
        /// direction along <paramref name="axis"/>, while simultaneously receiving analogous information from the
        /// negative direction along the same axis. If any items are transmitted by this call, the neighboring
        /// region must call yield to receive them. Yields within a region must occur in X, Y, Z order.
        /// </summary>
        /// <param name="numOut">The number of belt items transmitted.</param>
        /// <param name="numIn">The number of belt items to be received.</param>
        /// <param name="cont">The procedure executed after yielding.</param>
        public static UglProcedure Yield(Axis3 axis, byte numOut, byte numIn, UglProcedure cont)
        {
            // Normalize
            if (cont == null)
                return null;
            if (numOut == 0 && numIn == 0)
                return cont;

            // Build procedure
            return new _UglYieldProcedure(axis, numOut, numIn, cont);
        }
        
        /// <summary>
        /// Determines whether this procedure can be interpreted as a <see cref="Choice"/>.
        /// </summary>
        public bool IsChoice(out FiniteDistribution<UglProcedure> dist)
        {
            if (this is _UglCoinProcedure coin)
            {
                var distBuilder = new FiniteKernelBuilder<UglProcedure>();
                distBuilder.Add(coin.OnTrue, coin.Condition.OfTrue);
                distBuilder.Add(coin.OnFalse, coin.Condition.OfFalse);
                dist = (FiniteDistribution<UglProcedure>)distBuilder.Finish();
                return true;
            }
            else if (this is _UglChoiceProcedure choice)
            {
                dist = choice.Distribution;
                return true;
            }
            dist = default;
            return false;
        }

        /// <summary>
        /// Constructs a compact <see cref="UglAssembly"/> whose main entry point implements this procedure.
        /// </summary>
        public UglAssembly Compile()
        {
            // Compile procedure graph into a list of operations
            var tiles = PermutationBuilder<Tile>.CreateEmpty();
            _UglCompiler compiler = new _UglCompiler(new _UglSurveyor(tiles, 4097));
            uint entryIndex = compiler.Link(this, false, out _);
            List<_UglOperation> ops = compiler.Finish();
            
            // Determine placement of 'static' hints
            _UglStaticMarker marker = new _UglStaticMarker(ops);
            foreach (_UglOperation op in ops)
                if (op is _UglChunkOperation chunkOp)
                    marker.Chunk(chunkOp);
            BoolListBuilder markedStatic = marker.MarkedStatic;

            // Don't mark leaf operations as static even if it's recommended. They're just not worth caching
            for (uint i = 1; i < ops.Length; i++)
                if (ops[i] is _UglLeafOperation)
                    markedStatic[i] = false;

            // Finalize assembly
            _UglAssembler assembler = new _UglAssembler(tiles.Finish(), ops, markedStatic.Finish());
            assembler.Jump(entryIndex);
            return assembler.Finish();
        }
        
        /// <summary>
        /// Builds the list of <see cref="_UglFillingProcedure"/>s that may terminate the region for this procedure.
        /// </summary>
        /// <param name="considered">The set of procedures that have already been considered, and thus do not
        /// need to be explored.</param>
        internal abstract void _getFillings<T>(ListBuilder<T> fillings, SetBuilder<UglProcedure> considered)
            where T : _UglFillingProcedure;

        /// <summary>
        /// Constructs a <see cref="_UglOperation"/> which implements this procedure within the
        /// given <see cref="_UglCompiler"/>.
        /// </summary>
        /// <param name="inChunk">Indicates whether a <see cref="_UglChunkOperation"/> should be executed
        /// before the returned operation is executed. This may be set from <see cref="false"/> to <see cref="true"/> if
        /// the procedure demands to be marked with a chunk operation.</param>
        internal abstract _UglOperation _compile(_UglCompiler compiler, ref bool inChunk);

        /// <summary>
        /// Determines whether two <see cref="UglProcedure"/>s are equivalent, coalescing them if so.
        /// </summary>
        public static bool AreEqual(ref UglProcedure a, ref UglProcedure b)
        {
            if (ReferenceEquals(a, b))
                return true;
            if (a is null)
                return b is null;
            if (b is null)
                return false;
            if (a._hash != b._hash)
                return false;
            if (a.Equals(b))
            {
                Util.Coalesce(ref a, ref b);
                return true;
            }
            return false;
        }

        public static bool operator ==(UglProcedure a, UglProcedure b)
        {
            if (ReferenceEquals(a, b))
                return true;
            if (a is null)
                return b is null;
            if (b is null)
                return false;
            if (a._hash != b._hash)
                return false;
            return a.Equals(b);
        }

        public static bool operator !=(UglProcedure a, UglProcedure b)
        {
            return !(a == b);
        }

        public sealed override int GetHashCode()
        {
            return _hash;
        }
    }

    /// <summary>
    /// A <see cref="UglProcedure"/> which appends a byte constant to the belt.
    /// </summary>
    internal sealed class _UglPutProcedure : UglProcedure
    {
        public _UglPutProcedure(byte value, UglProcedure cont)
            : base(_hash(value, cont), cont.Scale)
        {
            Value = value;
            Continuation = cont;
        }

        /// <summary>
        /// The value added to the belt.
        /// </summary>
        public byte Value { get; }

        /// <summary>
        /// The <see cref="UglProcedure"/> executed after putting.
        /// </summary>
        public UglProcedure Continuation;

        /// <summary>
        /// Computes the hash of a <see cref="_UglPutProcedure"/> with the given properties.
        /// </summary>
        private static int _hash(byte value, UglProcedure cont)
        {
            return HashCodeHelper.Combine(
                0x6e099e25,
                value.GetHashCode(),
                cont.GetHashCode());
        }

        internal override void _getFillings<T>(ListBuilder<T> fillings, SetBuilder<UglProcedure> considered)
        {
            Continuation._getFillings(fillings, considered);
        }

        internal override _UglOperation _compile(_UglCompiler compiler, ref bool inChunk)
        {
            uint cont = compiler.Link(Continuation, ref inChunk, out _UglProcedureStats stats);
            stats.BeltBound++;
            if (stats.BeltDependence > 0)
                stats.BeltDependence--;
            return new _UglPutOperation(stats, Value, cont);
        }

        public static bool operator ==(_UglPutProcedure a, _UglPutProcedure b)
        {
            return a.Value == b.Value && AreEqual(ref a.Continuation, ref b.Continuation);
        }

        public static bool operator !=(_UglPutProcedure a, _UglPutProcedure b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is _UglPutProcedure))
                return false;
            return this == (_UglPutProcedure)obj;
        }
    }

    /// <summary>
    /// A <see cref="UglProcedure"/> which selects the item at the given offset from the end of the belt and
    /// appends a copy of it to the belt.
    /// </summary>
    internal sealed class _UglDupProcedure : UglProcedure
    {
        public _UglDupProcedure(byte offset, UglProcedure cont)
            : base(_hash(offset, cont), cont.Scale)
        {
            Offset = offset;
            Continuation = cont;
        }

        /// <summary>
        /// The offset of the item to duplicate.
        /// </summary>
        public byte Offset { get; }

        /// <summary>
        /// The <see cref="UglProcedure"/> executed after duplicating.
        /// </summary>
        public UglProcedure Continuation;

        /// <summary>
        /// Computes the hash of a <see cref="_UglPutProcedure"/> with the given properties.
        /// </summary>
        private static int _hash(byte offset, UglProcedure cont)
        {
            return HashCodeHelper.Combine(
                0x149881da,
                offset.GetHashCode(),
                cont.GetHashCode());
        }

        internal override void _getFillings<T>(ListBuilder<T> fillings, SetBuilder<UglProcedure> considered)
        {
            Continuation._getFillings(fillings, considered);
        }

        internal override _UglOperation _compile(_UglCompiler compiler, ref bool inChunk)
        {
            uint cont = compiler.Link(Continuation, ref inChunk, out _UglProcedureStats stats);
            stats.BeltBound++;
            if (stats.BeltDependence > 0)
                stats.BeltDependence--;
            if (stats.BeltDependence <= Offset)
                stats.BeltDependence = (byte)(Offset + 1);
            return new _UglDupOperation(stats, Offset, cont);
        }

        public static bool operator ==(_UglDupProcedure a, _UglDupProcedure b)
        {
            return a.Offset == b.Offset && AreEqual(ref a.Continuation, ref b.Continuation);
        }

        public static bool operator !=(_UglDupProcedure a, _UglDupProcedure b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is _UglPutProcedure))
                return false;
            return this == (_UglPutProcedure)obj;
        }
    }

    /// <summary>
    /// A <see cref="UglProcedure"/> which executes one of a given list of procedures depending on the value
    /// of the item at a given offset from the end of the belt.
    /// </summary>
    internal sealed class _UglSwitchProcedure : UglProcedure
    {
        public _UglSwitchProcedure(byte scale, byte offset, UglProcedure[] targets)
            : base(_hash(offset, targets), scale)
        {
            Debug.Assert(offset < 255);
            Debug.Assert(targets.Length < 256);
            Offset = offset;
            Targets = targets;
        }

        /// <summary>
        /// The offset of the item used to select the switch target.
        /// </summary>
        public byte Offset { get; }

        /// <summary>
        /// The potential targets for the switch.
        /// </summary>
        public UglProcedure[] Targets { get; }

        /// <summary>
        /// Computes the hash of a <see cref="_UglSwitchProcedure"/> with the given properties.
        /// </summary>
        private static int _hash(byte offset, UglProcedure[] targets)
        {
            int hash = 0x2c8ef014;
            hash = HashCodeHelper.Combine(hash, offset.GetHashCode());
            for (int i = 0; i < targets.Length; i++)
                hash = HashCodeHelper.Combine(hash, targets[i]?.GetHashCode() ?? 0);
            return hash;
        }

        internal override _UglOperation _compile(_UglCompiler compiler, ref bool inChunk)
        {
            _UglProcedureStats stats = _UglProcedureStats.Leaf;
            stats.BeltDependence = (byte)(Offset + 1);
            uint[] nTargets = new uint[Targets.Length];
            for (int i = 0; i < Targets.Length; i++)
            {
                nTargets[i] = compiler.Link(Targets[i], inChunk, out _UglProcedureStats targetStats);
                stats |= targetStats;
            }
            return new _UglSwitchOperation(stats, Offset, new List<uint>(nTargets));
        }

        internal override void _getFillings<T>(ListBuilder<T> fillings, SetBuilder<UglProcedure> considered)
        {
            if (considered.Include(this))
                foreach (UglProcedure target in Targets)
                    target?._getFillings(fillings, considered);
        }

        public static bool operator ==(_UglSwitchProcedure a, _UglSwitchProcedure b)
        {
            if (a.Offset != b.Offset)
                return false;
            if (a.Targets.Length != b.Targets.Length)
                return false;
            for (int i = 0; i < a.Targets.Length; i++)
                if (!AreEqual(ref a.Targets[i], ref b.Targets[i]))
                    return false;
            return true;
        }

        public static bool operator !=(_UglSwitchProcedure a, _UglSwitchProcedure b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is _UglSwitchProcedure))
                return false;
            return this == (_UglSwitchProcedure)obj;
        }
    }

    /// <summary>
    /// A <see cref="UglProcedure"/> which executes one of a pair of procedures depending on the value of
    /// a randomly chosen boolean.
    /// </summary>
    internal sealed class _UglCoinProcedure : UglProcedure
    {
        public _UglCoinProcedure(UnlikelyBoolDistribution cond, UglProcedure onTrue, UglProcedure onFalse)
            : base(_hash(cond, onTrue, onFalse), onTrue.Scale)
        {
            Condition = cond;
            OnTrue = onTrue;
            OnFalse = onFalse;
        }

        /// <summary>
        /// The distribution used to decide whether <see cref="OnTrue"/> or <see cref="OnFalse"/> will be executed.
        /// </summary>
        public UnlikelyBoolDistribution Condition { get; }

        /// <summary>
        /// The procedure executed if <see cref="Condition"/> is true.
        /// </summary>
        public UglProcedure OnTrue;

        /// <summary>
        /// The procedure executed if <see cref="Condition"/> is false.
        /// </summary>
        public UglProcedure OnFalse;
        
        /// <summary>
        /// Computes the hash of a <see cref="_UglCoinProcedure"/> with the given properties.
        /// </summary>
        private static int _hash(UnlikelyBoolDistribution cond, UglProcedure lt, UglProcedure ge)
        {
            return HashCodeHelper.Combine(cond.GetHashCode(), lt.GetHashCode(), ge.GetHashCode());
        }
        
        internal override void _getFillings<T>(ListBuilder<T> fillings, SetBuilder<UglProcedure> considered)
        {
            if (considered.Include(this))
            {
                OnTrue._getFillings(fillings, considered);
                OnFalse._getFillings(fillings, considered);
            }
        }

        internal override _UglOperation _compile(_UglCompiler compiler, ref bool inChunk)
        {
            float lnProb = Condition.OfTrue._ln;
            if (lnProb < -UnlikelyBoolDistribution._ln2_16 - BoolDistribution._ln2)
            {
                // The condition is too unlikely for a single coin operation
                // add a check beforehand to reduce the probability
                uint pivot;
                if (lnProb < -UnlikelyBoolDistribution._ln2_32)
                {
                    lnProb += UnlikelyBoolDistribution._ln2_32;
                    pivot = 1;
                }
                else
                {
                    lnProb += UnlikelyBoolDistribution._ln2_16;
                    pivot = 1 << 16;
                }
                var inner = new _UglCoinProcedure(new UnlikelyBoolDistribution(lnProb), OnTrue, OnFalse);
                uint lt = compiler.Link(inner, inChunk, out _UglProcedureStats ltStats);
                uint ge = compiler.Link(OnFalse, inChunk, out _UglProcedureStats geStats);
                _UglProcedureStats stats = ltStats | geStats;
                stats.IsDeterministic = false;
                return new _UglCoinOperation(stats, pivot, lt, ge);
            }
            else
            {
                // Write out as single coin operation
                // TODO: Check if the pivot can be simplified to 2 or 1 bytes
                uint pivot = (uint)(Math.Exp(lnProb) * uint.MaxValue);
                uint lt = compiler.Link(OnTrue, inChunk, out _UglProcedureStats ltStats);
                uint ge = compiler.Link(OnFalse, inChunk, out _UglProcedureStats geStats);
                _UglProcedureStats stats = ltStats | geStats;
                stats.IsDeterministic = false;
                return new _UglCoinOperation(stats, pivot, lt, ge);
            }
        }

        public static bool operator ==(_UglCoinProcedure a, _UglCoinProcedure b)
        {
            return a.Condition == b.Condition && 
                AreEqual(ref a.OnTrue, ref b.OnTrue) && 
                AreEqual(ref a.OnFalse, ref b.OnFalse);
        }

        public static bool operator !=(_UglCoinProcedure a, _UglCoinProcedure b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is _UglCoinProcedure))
                return false;
            return this == (_UglCoinProcedure)obj;
        }
    }

    /// <summary>
    /// A <see cref="UglProcedure"/> which randomly selects a procedure according to a distribution, and then executes it.
    /// </summary>
    internal sealed class _UglChoiceProcedure : UglProcedure
    {
        public _UglChoiceProcedure(byte scale, FiniteDistribution<UglProcedure> dist) // TODO: Equality
            : base(scale)
        {
            Debug.Assert(dist.SupportSize > 2);
            Distribution = dist;
        }

        /// <summary>
        /// The distribution from which the continuation procedure is selected.
        /// </summary>
        public FiniteDistribution<UglProcedure> Distribution { get; }

        internal override void _getFillings<T>(ListBuilder<T> fillings, SetBuilder<UglProcedure> considered)
        {
            if (considered.Include(this))
                foreach (PointKernel<UglProcedure> point in Distribution)
                    point.Value._getFillings(fillings, considered);
        }

        internal override _UglOperation _compile(_UglCompiler compiler, ref bool inChunk)
        {
            // TODO: Don't use alias method when there are only 3 entries
            // TODO: Check if we exceed the maximum size for a die table
            FastFiniteDistribution<UglProcedure> fastDist = Distribution.Optimize();
            uint[] targets = new uint[fastDist._table.Length];
            _UglProcedureStats stats = _UglProcedureStats.Leaf;
            stats.IsDeterministic = false;
            for (int i = 0; i < fastDist._table.Length; i++)
            {
                UglProcedure entryProc;
                var entry = fastDist._table[i];
                if (entry.Condition.IsAlwaysFalse)
                    entryProc = entry.OnFalse;
                else
                    entryProc = new _UglCoinProcedure(entry.Condition, entry.OnTrue, entry.OnFalse);
                targets[i] = compiler.Link(entryProc, inChunk, out _UglProcedureStats entryStats);
                stats |= entryStats;
            }
            return new _UglDieOperation(stats, targets);
        }
    }

    /// <summary>
    /// A <see cref="UglProcedure"/> which transmits a fixed number of items to the region neighboring the current
    /// region towards the positive direction along an axis, while simultaneously receiving analogous information
    /// from the negative direction along the same axis.
    /// </summary>
    internal sealed class _UglYieldProcedure : UglProcedure
    {
        public _UglYieldProcedure(Axis3 axis, byte numOut, byte numIn, UglProcedure cont)
            : base(_hash(axis, numOut, numIn, cont), cont.Scale)
        {
            Axis = axis;
            NumOut = numOut;
            NumIn = numIn;
            Continuation = cont;
        }

        /// <summary>
        /// The axis on which information is transmitted and received.
        /// </summary>
        public Axis3 Axis { get; }

        /// <summary>
        /// The number of belt items transmitted.
        /// </summary>
        public byte NumOut { get; }

        /// <summary>
        /// The number of belt items to be received.
        /// </summary>
        public byte NumIn { get; }

        /// <summary>
        /// The <see cref="UglProcedure"/> executed after yielding.
        /// </summary>
        public UglProcedure Continuation;

        /// <summary>
        /// Computes the hash of a <see cref="_UglYieldProcedure"/> with the given properties.
        /// </summary>
        private static int _hash(Axis3 axis, byte numOut, byte numIn, UglProcedure cont)
        {
            return HashCodeHelper.Combine(
                0x577fc078,
                axis.GetHashCode(), 
                numOut.GetHashCode(),
                numIn.GetHashCode(),
                cont.GetHashCode());
        }

        internal override void _getFillings<T>(ListBuilder<T> fillings, SetBuilder<UglProcedure> considered)
        {
            Continuation._getFillings(fillings, considered);
        }

        internal override _UglOperation _compile(_UglCompiler compiler, ref bool inChunk)
        {
            uint cont = compiler.Link(Continuation, ref inChunk, out _UglProcedureStats stats);
            stats.IsIsolatedAlong[Axis] = false;
            stats.BeltBound += NumIn;
            stats.BeltDependence = (byte)Math.Max(stats.BeltDependence - NumIn, NumOut);
            stats.Bandwidth[Axis] = (ushort)(stats.Bandwidth[Axis] + Math.Max(NumOut, NumIn));
            return new _UglYieldOperation(stats, Axis, NumOut, NumIn, cont);
        }

        public static bool operator ==(_UglYieldProcedure a, _UglYieldProcedure b)
        {
            return a.Axis == b.Axis && 
                a.NumOut == b.NumOut && 
                a.NumIn == b.NumIn && 
                AreEqual(ref a.Continuation, ref b.Continuation);

        }

        public static bool operator !=(_UglYieldProcedure a, _UglYieldProcedure b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is _UglYieldProcedure))
                return false;
            return this == (_UglYieldProcedure)obj;
        }
    }

    /// <summary>
    /// A <see cref="UglProcedure"/> which directly specifies the contents of an entire region. i.e. the execution
    /// thread for any region will be terminated with a <see cref="_UglFillingProcedure"/>.
    /// </summary>
    internal abstract class _UglFillingProcedure : UglProcedure
    {
        public _UglFillingProcedure(int hash, byte scale)
            : base(hash, scale)
        { }

        internal sealed override void _getFillings<T>(ListBuilder<T> fillings, SetBuilder<UglProcedure> considered)
        {
            if (considered.Include(this))
                if (this is T filling)
                    fillings.Append(filling);
        }
    }

    /// <summary>
    /// A <see cref="UglProcedure"/> which splits the current region into <see cref="Octant"/> subregions
    /// and executes a corresponding procedure to populate each one.
    /// </summary>
    internal sealed class _UglSplitProcedure : _UglFillingProcedure
    {
        public _UglSplitProcedure(byte scale, Octet<UglProcedure> targets)
            : base(targets.GetHashCode(), scale)
        {
            Targets = targets;
        }

        /// <summary>
        /// The procedures used to populate each subregion created by the split.
        /// </summary>
        public Octet<UglProcedure> Targets;

        internal override _UglOperation _compile(_UglCompiler compiler, ref bool inChunk)
        {
            // If this split is eligible to be in a chunk, request that it be in one
            if (!inChunk)
            {
                var metrics = compiler.Surveyor.GetMetrics(this);
                if (compiler.Surveyor.GetComplexityBound(metrics) < compiler.Surveyor.ComplexityLimit)
                    inChunk = true;
            }

            // Link targets
            Octet<uint> nTargets = new Octet<uint>(
                compiler.Link(Targets.NegX_NegY_NegZ, inChunk, out var stats_negX_negY_negZ),
                compiler.Link(Targets.PosX_NegY_NegZ, inChunk, out var stats_posX_negY_negZ),
                compiler.Link(Targets.NegX_PosY_NegZ, inChunk, out var stats_negX_posY_negZ),
                compiler.Link(Targets.PosX_PosY_NegZ, inChunk, out var stats_posX_posY_negZ),
                compiler.Link(Targets.NegX_NegY_PosZ, inChunk, out var stats_negX_negY_posZ),
                compiler.Link(Targets.PosX_NegY_PosZ, inChunk, out var stats_posX_negY_posZ),
                compiler.Link(Targets.NegX_PosY_PosZ, inChunk, out var stats_negX_posY_posZ),
                compiler.Link(Targets.PosX_PosY_PosZ, inChunk, out var stats_posX_posY_posZ));

            // Compute stats
            _UglProcedureStats stats = default;
            stats.IsDeterministic =
                stats_negX_negY_negZ.IsDeterministic &&
                stats_posX_negY_negZ.IsDeterministic &&
                stats_negX_posY_negZ.IsDeterministic &&
                stats_posX_posY_negZ.IsDeterministic &&
                stats_negX_negY_posZ.IsDeterministic &&
                stats_posX_negY_posZ.IsDeterministic &&
                stats_negX_posY_posZ.IsDeterministic &&
                stats_posX_posY_posZ.IsDeterministic;
            stats.IsIsolatedAlong.X =
                stats_negX_negY_negZ.IsIsolatedAlong.X &&
                stats_negX_posY_negZ.IsIsolatedAlong.X &&
                stats_negX_negY_posZ.IsIsolatedAlong.X &&
                stats_negX_posY_posZ.IsIsolatedAlong.X;
            stats.IsIsolatedAlong.Y =
                stats_negX_negY_negZ.IsIsolatedAlong.Y &&
                stats_posX_negY_negZ.IsIsolatedAlong.Y &&
                stats_negX_negY_posZ.IsIsolatedAlong.Y &&
                stats_posX_negY_posZ.IsIsolatedAlong.Y;
            stats.IsIsolatedAlong.Z =
                stats_negX_negY_negZ.IsIsolatedAlong.Z &&
                stats_posX_negY_negZ.IsIsolatedAlong.Z &&
                stats_negX_posY_negZ.IsIsolatedAlong.Z &&
                stats_posX_posY_negZ.IsIsolatedAlong.Z;
            stats.BeltDependence = stats_negX_negY_negZ.BeltDependence;
            if (stats.BeltDependence < stats_posX_negY_negZ.BeltDependence)
                stats.BeltDependence = stats_posX_negY_negZ.BeltDependence;
            if (stats.BeltDependence < stats_negX_posY_negZ.BeltDependence)
                stats.BeltDependence = stats_negX_posY_negZ.BeltDependence;
            if (stats.BeltDependence < stats_posX_posY_negZ.BeltDependence)
                stats.BeltDependence = stats_posX_posY_negZ.BeltDependence;
            if (stats.BeltDependence < stats_negX_negY_posZ.BeltDependence)
                stats.BeltDependence = stats_negX_negY_posZ.BeltDependence;
            if (stats.BeltDependence < stats_posX_negY_posZ.BeltDependence)
                stats.BeltDependence = stats_posX_negY_posZ.BeltDependence;
            if (stats.BeltDependence < stats_negX_posY_posZ.BeltDependence)
                stats.BeltDependence = stats_negX_posY_posZ.BeltDependence;
            if (stats.BeltDependence < stats_posX_posY_posZ.BeltDependence)
                stats.BeltDependence = stats_posX_posY_posZ.BeltDependence;
            stats.BeltBound = stats_negX_negY_negZ.BeltBound;
            if (stats.BeltBound < stats_posX_negY_negZ.BeltBound)
                stats.BeltBound = stats_posX_negY_negZ.BeltBound;
            if (stats.BeltBound < stats_negX_posY_negZ.BeltBound)
                stats.BeltBound = stats_negX_posY_negZ.BeltBound;
            if (stats.BeltBound < stats_posX_posY_negZ.BeltBound)
                stats.BeltBound = stats_posX_posY_negZ.BeltBound;
            if (stats.BeltBound < stats_negX_negY_posZ.BeltBound)
                stats.BeltBound = stats_negX_negY_posZ.BeltBound;
            if (stats.BeltBound < stats_posX_negY_posZ.BeltBound)
                stats.BeltBound = stats_posX_negY_posZ.BeltBound;
            if (stats.BeltBound < stats_negX_posY_posZ.BeltBound)
                stats.BeltBound = stats_negX_posY_posZ.BeltBound;
            if (stats.BeltBound < stats_posX_posY_posZ.BeltBound)
                stats.BeltBound = stats_posX_posY_posZ.BeltBound;
            // TODO: Check bandwidth overflow
            stats.Bandwidth.X = (ushort)Math.Max(
                stats_negX_negY_negZ.Bandwidth.X +
                stats_negX_posY_negZ.Bandwidth.X +
                stats_negX_negY_posZ.Bandwidth.X +
                stats_negX_posY_posZ.Bandwidth.X,
                stats_posX_negY_negZ.Bandwidth.X +
                stats_posX_posY_negZ.Bandwidth.X +
                stats_posX_negY_posZ.Bandwidth.X +
                stats_posX_posY_posZ.Bandwidth.X);
            stats.Bandwidth.Y = (ushort)Math.Max(
                stats_negX_negY_negZ.Bandwidth.Y +
                stats_negX_negY_posZ.Bandwidth.Y +
                stats_posX_negY_negZ.Bandwidth.Y +
                stats_posX_negY_posZ.Bandwidth.Y,
                stats_negX_posY_negZ.Bandwidth.Y +
                stats_negX_posY_posZ.Bandwidth.Y +
                stats_posX_posY_negZ.Bandwidth.Y +
                stats_posX_posY_posZ.Bandwidth.Y);
            stats.Bandwidth.Z = (ushort)Math.Max(
                stats_negX_negY_negZ.Bandwidth.Z +
                stats_posX_negY_negZ.Bandwidth.Z +
                stats_negX_posY_negZ.Bandwidth.Z +
                stats_posX_posY_negZ.Bandwidth.Z,
                stats_negX_negY_posZ.Bandwidth.Z +
                stats_posX_negY_posZ.Bandwidth.Z +
                stats_negX_posY_posZ.Bandwidth.Z +
                stats_posX_posY_posZ.Bandwidth.Z);
            return new _UglSplitOperation(stats, nTargets);
        }

        public static bool operator ==(_UglSplitProcedure a, _UglSplitProcedure b)
        {
            return
                AreEqual(ref a.Targets.NegX_NegY_NegZ, ref b.Targets.NegX_NegY_NegZ) &&
                AreEqual(ref a.Targets.PosX_NegY_NegZ, ref b.Targets.PosX_NegY_NegZ) &&
                AreEqual(ref a.Targets.NegX_PosY_NegZ, ref b.Targets.NegX_PosY_NegZ) &&
                AreEqual(ref a.Targets.PosX_PosY_NegZ, ref b.Targets.PosX_PosY_NegZ) &&
                AreEqual(ref a.Targets.NegX_NegY_PosZ, ref b.Targets.NegX_NegY_PosZ) &&
                AreEqual(ref a.Targets.PosX_NegY_PosZ, ref b.Targets.PosX_NegY_PosZ) &&
                AreEqual(ref a.Targets.NegX_PosY_PosZ, ref b.Targets.NegX_PosY_PosZ) &&
                AreEqual(ref a.Targets.PosX_PosY_PosZ, ref b.Targets.PosX_PosY_PosZ);

        }

        public static bool operator !=(_UglSplitProcedure a, _UglSplitProcedure b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is _UglSplitProcedure))
                return false;
            return this == (_UglSplitProcedure)obj;
        }
    }

    /// <summary>
    /// A <see cref="UglProcedure"/> which populates the current scale-0 region with a given <see cref="Tile"/>.
    /// </summary>
    internal sealed class _UglLeafProcedure : _UglFillingProcedure
    {
        public _UglLeafProcedure(Tile tile)
            : base(tile.GetHashCode(), 0)
        {
            Tile = tile;
        }

        /// <summary>
        /// The tile applied by this procedure.
        /// </summary>
        public Tile Tile { get; }

        internal override _UglOperation _compile(_UglCompiler compiler, ref bool inChunk)
        {
            Debug.Assert(inChunk);
            return new _UglLeafOperation(compiler.Tiles.Include(Tile));
        }

        public static bool operator ==(_UglLeafProcedure a, _UglLeafProcedure b)
        {
            return a.Tile == b.Tile;
        }

        public static bool operator !=(_UglLeafProcedure a, _UglLeafProcedure b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is _UglLeafProcedure))
                return false;
            return this == (_UglLeafProcedure)obj;
        }
    }

    /// <summary>
    /// Encapsulates high-level information about a <see cref="UglProcedure"/>.
    /// </summary>
    internal struct _UglProcedureStats
    {
        /// <summary>
        /// If true, indicates that the procedure does not depend on any randomly-generated values.
        /// </summary>
        public bool IsDeterministic;

        /// <summary>
        /// If true, indicates that the procedure does not depend on any values obtained using 'yield' along a given axis.
        /// </summary>
        public Axial3<bool> IsIsolatedAlong;

        /// <summary>
        /// The number of items at the end of the belt that this procedure may depend on.
        /// </summary>
        public byte BeltDependence;

        /// <summary>
        /// The maximum number of extra belt items added in any single path through the procedure.
        /// </summary>
        public ushort BeltBound;

        /// <summary>
        /// The maximum number of yielded items "in flight" at a time along a given axis.
        /// </summary>
        public Axial3<ushort> Bandwidth;

        /// <summary>
        /// If true, indicates that the procedure does not depend on any values obtained using 'yield'.
        /// </summary>
        public bool IsIsolated => IsIsolatedAlong.X && IsIsolatedAlong.Y && IsIsolatedAlong.Z;

        /// <summary>
        /// If true, indicates that the procedure does not depend on any randomly-generated values,
        /// values obtained using 'yield', or values on the belt. Effectively, the procedure will always
        /// execute the same content instructions regardless of any other context.
        /// </summary>
        public bool IsStatic => IsDeterministic && IsIsolated && BeltDependence == 0;

        /// <summary>
        /// The stats for a <see cref="_UglLeafProcedure"/>.
        /// </summary>
        public static _UglProcedureStats Leaf => new _UglProcedureStats
        {
            IsDeterministic = true,
            IsIsolatedAlong = Axial3.Uniform(true),
            BeltDependence = 0,
            BeltBound = 0,
            Bandwidth = Axial3.Uniform((ushort)0)
        };

        /// <summary>
        /// Constructs the stats for a procedure that executes one of the given procedures.
        /// </summary>
        public static _UglProcedureStats operator |(_UglProcedureStats a, _UglProcedureStats b)
        {
            return new _UglProcedureStats
            {
                IsDeterministic = a.IsDeterministic & b.IsDeterministic,
                IsIsolatedAlong = new Axial3<bool>(
                    a.IsIsolatedAlong.X & b.IsIsolatedAlong.X,
                    a.IsIsolatedAlong.Y & b.IsIsolatedAlong.Y,
                    a.IsIsolatedAlong.Z & b.IsIsolatedAlong.Z),
                BeltDependence = Math.Max(a.BeltDependence, b.BeltDependence),
                BeltBound = Math.Max(a.BeltBound, b.BeltBound),
                Bandwidth = new Axial3<ushort>(
                    Math.Max(a.Bandwidth.X, b.Bandwidth.X),
                    Math.Max(a.Bandwidth.Y, b.Bandwidth.Y),
                    Math.Max(a.Bandwidth.Z, b.Bandwidth.Z))
            };
        }
    }
}