﻿using System;

using Alunite.Data.Geometry;
using Alunite.Data.Serialization;
using Alunite.Universe.Grids;

namespace Alunite.Universe.Generation
{
    /// <summary>
    /// Identifies a primitive operation in the "Universe Generation Language". These operations manipulate a belt
    /// (append-only list) and a PRNG to specify the contents of an <see cref="IRegion"/>.
    /// </summary>
    /// <remarks>UGL uses a belt, instead of a stack, to allow for very quick forking. Since items already on the
    /// belt can't be manipulated, it's not necessary to create a copy of the belt when forking threads.</remarks>
    public enum UglOpCode : byte
    {
        /// <summary>
        /// Does nothing, moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>nop</code>
        /// </remarks>
        Nop = 0x00,

        /// <summary>
        /// Appends a byte constant to the belt, then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>put [value:uint8]</code>
        /// </remarks>
        Put = 0x01,

        /// <summary>
        /// Selects an item at a specific offset from the end of the belt, appends a copy of it
        /// to the belt, then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>dup [offset:uint8]</code>
        /// </remarks>
        Dup = 0x02,

        /// <summary>
        /// Unconditionally jumps forward to a given target, specified as an offset from the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>br [target:uint16]</code>
        /// </remarks>
        Br = 0x03,

        /// <summary>
        /// Jumps to the instruction at the given absolute position.
        /// </summary>
        /// <remarks>
        /// <code>jmp [target:uint32]</code>
        /// </remarks>
        Jmp = 0x04,

        /// <summary>
        /// Selects an item at a specific offset from the end of the belt, then branches to a target based on its
        /// value. All targets are specified as offsets from the next instruction. If the value exceeds the
        /// bounds of the switch table, undefined behavior will result.
        /// </summary>
        /// <remarks>
        /// <code>switch [offset:uint8] [(N-1):uint8] [target:uint16]^N</code>
        /// </remarks>
        Switch = 0x05,

        /// <summary>
        /// Conditionally branches to a target depending on whether a randomly generated 8-bit integer is less than a
        /// given value. The target is specified as an offset from the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>coin.u1.s [pivot:uint8] [target_lt:uint8]</code>
        /// </remarks>
        Coin_U1_S = 0x06,

        /// <summary>
        /// Conditionally branches to a target depending on whether a randomly generated 16-bit integer is less than a
        /// given value. The target is specified as an offset from the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>coin.u2.s [pivot:uint16] [target_lt:uint8]</code>
        /// </remarks>
        Coin_U2_S = 0x07,

        /// <summary>
        /// Conditionally branches to a target depending on whether a randomly generated 32-bit integer is less than a
        /// given value. The target is specified as an offset from the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>coin.u4.s [pivot:uint32] [target_lt:uint8]</code>
        /// </remarks>
        Coin_U4_S = 0x08,

        /// <summary>
        /// Branches to a target based on the value of a random integer chosen uniformly in the range [0 .. 2). All targets
        /// are specified as an offset from the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>die.2.s [target:uint8]^2</code>
        /// </remarks>
        Die_2_S = 0x09,

        /// <summary>
        /// Branches to a target based on the value of a random integer chosen uniformly in the range [0 .. 4). All targets
        /// are specified as an offset from the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>die.4.s [target:uint8]^4</code>
        /// </remarks>
        Die_4_S = 0x0A,

        /// <summary>
        /// Branches to a target based on the value of a random integer chosen uniformly in the range [0 .. 8). All targets
        /// are specified as an offset from the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>die.8.s [target:uint8]^8</code>
        /// </remarks>
        Die_8_S = 0x0B,

        /// <summary>
        /// Branches to a target based on the value of a random integer chosen uniformly in the range [0 .. 16). All targets
        /// are specified as an offset from the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>die.16.s [target:uint8]^16</code>
        /// </remarks>
        Die_16_S = 0x0C,

        /// <summary>
        /// Branches to a target based on the value of a random integer chosen uniformly in the range [0 .. 32). All targets
        /// are specified as an offset from the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>die.32.s [target:uint8]^32</code>
        /// </remarks>
        Die_32_S = 0x0D,

        /// <summary>
        /// Branches to a target based on the value of a random integer chosen uniformly in the range [0 .. 64). All targets
        /// are specified as an offset from the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>die.64.s [target:uint8]^64</code>
        /// </remarks>
        Die_64_S = 0x0E,

        /// <summary>
        /// Branches to a target based on the value of a random integer chosen uniformly in the range [0 .. 128). All targets
        /// are specified as an offset from the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>die.128 [target:uint16]^128</code>
        /// </remarks>
        Die_128 = 0x0F,

        /// <summary>
        /// Branches to a target based on the value of a random integer chosen uniformly in the range [0 .. 256). All targets
        /// are specified as an offset from the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>die.256 [target:uint16]^256</code>
        /// </remarks>
        Die_256 = 0x10,

        /// <summary>
        /// Transmits the last N_out items on the belt to the region neighboring this region towards the positive X
        /// direction, while simultaneously receiving analogous information from the neighboring region towards
        /// the negative X direction. The N_in items from that region will be appended to the belt. If any items are
        /// transmitted by this call, the neighboring region towards the positive X direction must yield and receive
        /// them. This may be called at most once per region.
        /// </summary>
        /// <remarks>
        /// <code>yield.x [N_out:uint8] [N_in:uint8]</code>
        /// </remarks>
        Yield_X = 0x11,

        /// <summary>
        /// Transmits the last N_out items on the belt to the region neighboring this region towards the positive Y
        /// direction, while simultaneously receiving analogous information from the neighboring region towards
        /// the negative Y direction. The N_in items from that region will be appended to the belt. If any items are
        /// transmitted by this call, the neighboring region towards the positive Y direction must yield and receive
        /// them. This may be called at most once per region.
        /// </summary>
        /// <remarks>
        /// <code>yield.y [N_out:uint8] [N_in:uint8]</code>
        /// </remarks>
        Yield_Y = 0x12,

        /// <summary>
        /// Transmits the last N_out items on the belt to the region neighboring this region towards the positive Z
        /// direction, while simultaneously receiving analogous information from the neighboring region towards
        /// the negative Z direction. The N_in items from that region will be appended to the belt. If any items are
        /// transmitted by this call, the neighboring region towards the positive Z direction must yield and receive
        /// them. This may be called at most once per region.
        /// </summary>
        /// <remarks>
        /// <code>yield.z [N_out:uint8] [N_in:uint8]</code>
        /// </remarks>
        Yield_Z = 0x13,

        /// <summary>
        /// Indicates that the current region is a good candidate to be a <see cref="WorldChunk"/>. This should be
        /// executed exactly once in every path from a root region to a <see cref="Leaf"/>. All chunks within a world
        /// should have roughly the same complexity. Additionally, there are hard complexity and scale limits that must
        /// be respected.
        /// </summary>
        /// <remarks>
        /// <code>chunk [metadata:<see cref="UglChunkMetadata"/>]</code>
        /// </remarks>
        Chunk = 0x14,

        /// <summary>
        /// Splits the current region into <see cref="Octant"/> subregions, initializing a thread for each
        /// of these subregions. The entry points for each thread are specified by offsets from the next instruction.
        /// Each thread gets a copy of the current belt.
        /// </summary>
        /// <remarks>
        /// <code>split [target:uint8]^8</code>
        /// </remarks>
        Split = 0x15,

        /// <summary>
        /// Specifies the <see cref="Tile"/> occupying the current scale-0 region. This terminates the
        /// current thread.
        /// </summary>
        /// <remarks>
        /// <code>leaf [tile_token:uint32]</code>
        /// </remarks>
        Leaf = 0x16,

        /// <summary>
        /// Indicates that the remaining code to be executed is static. This asserts the exact same sequence of content
        /// instructions will be executed regardless of the current belt, the random sampler or any values obtained using
        /// 'yield'. This is a hint that the engine should attempt to cache and reuse the content starting at
        /// this instruction.
        /// </summary>
        /// <remarks>
        /// <code>static</code>
        /// </remarks>
        Static = 0x17,

        // Reserved for additional content instructions

        /// <summary>
        /// Appends the byte constant 0 to the belt, then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>put.0</code>
        /// </remarks>
        Put_0 = 0x20,

        /// <summary>
        /// Appends the byte constant 1 to the belt, then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>put.1</code>
        /// </remarks>
        Put_1 = 0x21,

        /// <summary>
        /// Appends the byte constant 2 to the belt, then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>put.2</code>
        /// </remarks>
        Put_2 = 0x22,

        /// <summary>
        /// Appends the byte constant 3 to the belt, then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>put.3</code>
        /// </remarks>
        Put_3 = 0x23,

        /// <summary>
        /// Appends the byte constant 4 to the belt, then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>put.4</code>
        /// </remarks>
        Put_4 = 0x24,

        /// <summary>
        /// Appends the byte constant 5 to the belt, then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>put.5</code>
        /// </remarks>
        Put_5 = 0x25,

        /// <summary>
        /// Appends the byte constant 6 to the belt, then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>put.6</code>
        /// </remarks>
        Put_6 = 0x26,

        /// <summary>
        /// Appends the byte constant 7 to the belt, then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>put.7</code>
        /// </remarks>
        Put_7 = 0x27,

        /// <summary>
        /// Appends the byte constant 8 to the belt, then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>put.8</code>
        /// </remarks>
        Put_8 = 0x28,

        /// <summary>
        /// Appends the byte constant 9 to the belt, then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>put.9</code>
        /// </remarks>
        Put_9 = 0x29,

        /// <summary>
        /// Appends the byte constant 10 to the belt, then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>put.10</code>
        /// </remarks>
        Put_10 = 0x2A,

        /// <summary>
        /// Appends the byte constant 11 to the belt, then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>put.11</code>
        /// </remarks>
        Put_11 = 0x2B,

        /// <summary>
        /// Appends the byte constant 12 to the belt, then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>put.12</code>
        /// </remarks>
        Put_12 = 0x2C,

        /// <summary>
        /// Appends the byte constant 13 to the belt, then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>put.13</code>
        /// </remarks>
        Put_13 = 0x2D,

        /// <summary>
        /// Appends the byte constant 14 to the belt, then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>put.14</code>
        /// </remarks>
        Put_14 = 0x2E,

        /// <summary>
        /// Appends the byte constant 15 to the belt, then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>put.15</code>
        /// </remarks>
        Put_15 = 0x2F,
        
        /// <summary>
        /// Duplicates the last item on the belt, then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>dup.m1</code>
        /// </remarks>
        Dup_M1 = 0x30,

        /// <summary>
        /// Duplicates the second to last item on the belt, appending a copy of it to the end of the belt,
        /// then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>dup.m2</code>
        /// </remarks>
        Dup_M2 = 0x31,

        /// <summary>
        /// Duplicates the third to last item on the belt, appending a copy of it to the end of the belt,
        /// then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>dup.m3</code>
        /// </remarks>
        Dup_M3 = 0x32,

        /// <summary>
        /// Duplicates the 4th to last item on the belt, appending a copy of it to the end of the belt,
        /// then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>dup.m4</code>
        /// </remarks>
        Dup_M4 = 0x33,

        /// <summary>
        /// Duplicates the 5th to last item on the belt, appending a copy of it to the end of the belt,
        /// then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>dup.m5</code>
        /// </remarks>
        Dup_M5 = 0x34,

        /// <summary>
        /// Duplicates the 6th to last item on the belt, appending a copy of it to the end of the belt,
        /// then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>dup.m6</code>
        /// </remarks>
        Dup_M6 = 0x35,

        /// <summary>
        /// Duplicates the 7th to last item on the belt, appending a copy of it to the end of the belt,
        /// then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>dup.m7</code>
        /// </remarks>
        Dup_M7 = 0x36,

        /// <summary>
        /// Duplicates the 8th to last item on the belt, appending a copy of it to the end of the belt,
        /// then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>dup.m8</code>
        /// </remarks>
        Dup_M8 = 0x37,

        /// <summary>
        /// Duplicates the 9th to last item on the belt, appending a copy of it to the end of the belt,
        /// then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>dup.m9</code>
        /// </remarks>
        Dup_M9 = 0x38,

        /// <summary>
        /// Duplicates the 10th to last item on the belt, appending a copy of it to the end of the belt,
        /// then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>dup.m10</code>
        /// </remarks>
        Dup_M10 = 0x39,

        /// <summary>
        /// Duplicates the 11th to last item on the belt, appending a copy of it to the end of the belt,
        /// then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>dup.m11</code>
        /// </remarks>
        Dup_M11 = 0x3A,

        /// <summary>
        /// Duplicates the 12th to last item on the belt, appending a copy of it to the end of the belt,
        /// then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>dup.m12</code>
        /// </remarks>
        Dup_M12 = 0x3B,

        /// <summary>
        /// Duplicates the 13th to last item on the belt, appending a copy of it to the end of the belt,
        /// then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>dup.m13</code>
        /// </remarks>
        Dup_M13 = 0x3C,

        /// <summary>
        /// Duplicates the 14th to last item on the belt, appending a copy of it to the end of the belt,
        /// then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>dup.m14</code>
        /// </remarks>
        Dup_M14 = 0x3D,

        /// <summary>
        /// Duplicates the 15th to last item on the belt, appending a copy of it to the end of the belt,
        /// then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>dup.m15</code>
        /// </remarks>
        Dup_M15 = 0x3E,

        /// <summary>
        /// Duplicates the 16th to last item on the belt, appending a copy of it to the end of the belt,
        /// then moves onto the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>dup.m16</code>
        /// </remarks>
        Dup_M16 = 0x3F,

        /// <summary>
        /// Unconditionally jumps forward to a given target, specified as an offset from the next instruction.
        /// </summary>
        /// <remarks>
        /// <code>br.s [target:uint8]</code>
        /// </remarks>
        Br_S = 0x40,

        /// <summary>
        /// Branches to a target based on the value of the last item on the belt. All targets are specified as
        /// offsets from the next instruction. If the value exceeds the bounds of the switch table, undefined
        /// behavior will result.
        /// </summary>
        /// <remarks>
        /// <code>switch.m1.s [(N-1):uint8] [target:uint8]^N </code>
        /// </remarks>
        Switch_M1_S = 0x41,

        /// <summary>
        /// Branches to a target based on the value of the 2nd to last item on the belt. All targets are specified as
        /// offsets from the next instruction. If the value exceeds the bounds of the switch table, undefined
        /// behavior will result.
        /// </summary>
        /// <remarks>
        /// <code>switch.m2.s [(N-1):uint8] [target:uint8]^N </code>
        /// </remarks>
        Switch_M2_S = 0x42,

        /// <summary>
        /// Branches to a target based on the value of the 3rd to last item on the belt. All targets are specified as
        /// offsets from the next instruction. If the value exceeds the bounds of the switch table, undefined
        /// behavior will result.
        /// </summary>
        /// <remarks>
        /// <code>switch.m3.s [(N-1):uint8] [target:uint8]^N </code>
        /// </remarks>
        Switch_M3_S = 0x43,

        /// <summary>
        /// Branches to a target based on the value of the 4th to last item on the belt. All targets are specified as
        /// offsets from the next instruction. If the value exceeds the bounds of the switch table, undefined
        /// behavior will result.
        /// </summary>
        /// <remarks>
        /// <code>switch.m4.s [(N-1):uint8] [target:uint8]^N </code>
        /// </remarks>
        Switch_M4_S = 0x44,

        /// <summary>
        /// Branches to a target based on the value of the 5th to last item on the belt. All targets are specified as
        /// offsets from the next instruction. If the value exceeds the bounds of the switch table, undefined
        /// behavior will result.
        /// </summary>
        /// <remarks>
        /// <code>switch.m5.s [(N-1):uint8] [target:uint8]^N </code>
        /// </remarks>
        Switch_M5_S = 0x45,

        /// <summary>
        /// Branches to a target based on the value of the 6th to last item on the belt. All targets are specified as
        /// offsets from the next instruction. If the value exceeds the bounds of the switch table, undefined
        /// behavior will result.
        /// </summary>
        /// <remarks>
        /// <code>switch.m6.s [(N-1):uint8] [target:uint8]^N </code>
        /// </remarks>
        Switch_M6_S = 0x46,

        /// <summary>
        /// Branches to a target based on the value of the 7th to last item on the belt. All targets are specified as
        /// offsets from the next instruction. If the value exceeds the bounds of the switch table, undefined
        /// behavior will result.
        /// </summary>
        /// <remarks>
        /// <code>switch.m7.s [(N-1):uint8] [target:uint8]^N </code>
        /// </remarks>
        Switch_M7_S = 0x47,

        /// <summary>
        /// Branches to a target based on the value of the 8th to last item on the belt. All targets are specified as
        /// offsets from the next instruction. If the value exceeds the bounds of the switch table, undefined
        /// behavior will result.
        /// </summary>
        /// <remarks>
        /// <code>switch.m8.s [(N-1):uint8] [target:uint8]^N </code>
        /// </remarks>
        Switch_M8_S = 0x48,

        /// <summary>
        /// Branches to a target based on the value of the 9th to last item on the belt. All targets are specified as
        /// offsets from the next instruction. If the value exceeds the bounds of the switch table, undefined
        /// behavior will result.
        /// </summary>
        /// <remarks>
        /// <code>switch.m9.s [(N-1):uint8] [target:uint8]^N </code>
        /// </remarks>
        Switch_M9_S = 0x49,

        /// <summary>
        /// Branches to a target based on the value of the 10th to last item on the belt. All targets are specified as
        /// offsets from the next instruction. If the value exceeds the bounds of the switch table, undefined
        /// behavior will result.
        /// </summary>
        /// <remarks>
        /// <code>switch.m10.s [(N-1):uint8] [target:uint8]^N </code>
        /// </remarks>
        Switch_M10_S = 0x4A,

        /// <summary>
        /// Branches to a target based on the value of the 11th to last item on the belt. All targets are specified as
        /// offsets from the next instruction. If the value exceeds the bounds of the switch table, undefined
        /// behavior will result.
        /// </summary>
        /// <remarks>
        /// <code>switch.m11.s [(N-1):uint8] [target:uint8]^N </code>
        /// </remarks>
        Switch_M11_S = 0x4B,

        /// <summary>
        /// Branches to a target based on the value of the 12th to last item on the belt. All targets are specified as
        /// offsets from the next instruction. If the value exceeds the bounds of the switch table, undefined
        /// behavior will result.
        /// </summary>
        /// <remarks>
        /// <code>switch.m12.s [(N-1):uint8] [target:uint8]^N </code>
        /// </remarks>
        Switch_M12_S = 0x4C,

        /// <summary>
        /// Branches to a target based on the value of the 13th to last item on the belt. All targets are specified as
        /// offsets from the next instruction. If the value exceeds the bounds of the switch table, undefined
        /// behavior will result.
        /// </summary>
        /// <remarks>
        /// <code>switch.m13.s [(N-1):uint8] [target:uint8]^N </code>
        /// </remarks>
        Switch_M13_S = 0x4D,

        /// <summary>
        /// Branches to a target based on the value of the 14th to last item on the belt. All targets are specified as
        /// offsets from the next instruction. If the value exceeds the bounds of the switch table, undefined
        /// behavior will result.
        /// </summary>
        /// <remarks>
        /// <code>switch.m14.s [(N-1):uint8] [target:uint8]^N </code>
        /// </remarks>
        Switch_M14_S = 0x4E,

        /// <summary>
        /// Branches to a target based on the value of the 15th to last item on the belt. All targets are specified as
        /// offsets from the next instruction. If the value exceeds the bounds of the switch table, undefined
        /// behavior will result.
        /// </summary>
        /// <remarks>
        /// <code>switch.m15.s [(N-1):uint8] [target:uint8]^N </code>
        /// </remarks>
        Switch_M15_S = 0x4F,
    }

    /// <summary>
    /// Provides high-level information about a chunk generated using UGL.
    /// </summary>
    public struct UglChunkMetadata
    {
        /// <summary>
        /// The log-base-2 of the length of the chunk along any axis.
        /// </summary>
        public byte Scale;

        /// <summary>
        /// The maximum number of extra belt items added in any single path when processing the chunk.
        /// </summary>
        public ushort BeltBound;

        /// <summary>
        /// The maximum number of yielded items "in flight" at a time along a given axis when processing this chunk.
        /// </summary>
        public Axial3<ushort> Bandwidth;

        /// <summary>
        /// The size of this structure when serialized to a <see cref="Blob"/>.
        /// </summary>
        public const uint BlobSize = 1 + 2 + 2 * 3;

        /// <summary>
        /// Writes a <see cref="UglChunkMetadata"/> to a <see cref="Blob"/>.
        /// </summary>
        public static void Write(ref Span<byte> blob, UglChunkMetadata metadata)
        {
            Blob.WriteByte(ref blob, metadata.Scale);
            Blob.WriteUInt16(ref blob, metadata.BeltBound);
            Blob.WriteUInt16(ref blob, metadata.Bandwidth.X);
            Blob.WriteUInt16(ref blob, metadata.Bandwidth.Y);
            Blob.WriteUInt16(ref blob, metadata.Bandwidth.Z);
        }

        /// <summary>
        /// Reads a <see cref="UglChunkMetadata"/> from a <see cref="Blob"/>.
        /// </summary>
        public static UglChunkMetadata Read(Span<byte> blob, ref uint head)
        {
            return new UglChunkMetadata
            {
                Scale = Blob.ReadByte(blob, ref head),
                BeltBound = Blob.ReadUInt16(blob, ref head),
                Bandwidth = new Axial3<ushort>(
                    Blob.ReadUInt16(blob, ref head),
                    Blob.ReadUInt16(blob, ref head),
                    Blob.ReadUInt16(blob, ref head))
            };
        }
    }
}