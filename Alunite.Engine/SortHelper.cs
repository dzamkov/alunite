﻿using System;
using System.Runtime.CompilerServices;
using System.Collections.Generic;

namespace Alunite
{
    /// <summary>
    /// Contains helper functions related to sorting.
    /// </summary>
    public static class SortHelper
    {
        /// <summary>
        /// Sorts the items of the given span using the given <see cref="IComparer{T}"/>. This is a stable sort.
        /// </summary>
        public static void Sort<T, TComparer>(this Span<T> span, TComparer comparer)
            where TComparer : IComparer<T>
        {
            if (span.Length < 2)
            {
                // Nothing to do here
            }
            else if (span.Length < 32)
            {
                // Use insertion sort
                for (int i = 1; i < span.Length; i++)
                {
                    T item = span[i];
                    int j = i - 1;
                    T cand = span[j];
                    if (comparer.Compare(item, cand) < 0)
                    {
                        span[i] = cand;
                        while (j > 0)
                        {
                            int nj = j - 1;
                            cand = span[nj];
                            if (comparer.Compare(item, cand) < 0)
                            {
                                span[j] = cand;
                                j = nj;
                            }
                            else
                            {
                                break;
                            }
                        }
                        span[j] = item;
                    }
                }
            }
            else
            {
                // TODO: Replace with something with good performance
                T[] items = new T[span.Length];
                span.CopyTo(items);
                Array.Sort(items, comparer);
                items.CopyTo(span);
            }
        }

        /// <summary>
        /// Sorts the items of the given span. This is a stable sort.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Sort<T>(this Span<T> span)
            where T : IComparable<T>
        {
            span.Sort(_ComparableComparer<T>.Instance);
        }

        /// <summary>
        /// An <see cref="IComparer{T}"/> implemented using <see cref="IComparable{T}"/>.
        /// </summary>
        private struct _ComparableComparer<T> : IComparer<T>
            where T : IComparable<T>
        {
            /// <summary>
            /// The only instance of this class.
            /// </summary>
            public static _ComparableComparer<T> Instance => new _ComparableComparer<T>();

            int IComparer<T>.Compare(T x, T y)
            {
                return x.CompareTo(y);
            }
        }
    }
}