﻿using System;
using System.IO;
using System.Reflection;
using System.Diagnostics;

using Alunite.Data.Serialization;

namespace Alunite.Resources
{
    /// <summary>
    /// Identifies and describes a binary data source which need not be explicitly represented in memory.
    /// </summary>
    public abstract class BigBinary
    {
        public BigBinary(ulong size)
        {
            Size = size;
        }

        /// <summary>
        /// The number of bytes in this <see cref="BigBinary"/>.
        /// </summary>
        public ulong Size { get; }

        /// <summary>
        /// Creates an <see cref="IBinaryReader"/> to read from this binary starting at the given offset.
        /// </summary>
        public virtual IDisposable Read(ulong offset, out ISeekableBinaryReader reader)
        {
            Stream stream = OpenStream();
            stream.Position = (long)offset;
            reader = new StreamBinaryReader(stream, 4096);
            return stream;
        }

        /// <summary>
        /// Creates a <see cref="Stream"/> to read from this <see cref="BigBinary"/>.
        /// </summary>
        public abstract Stream OpenStream();

        /// <summary>
        /// Constructs a <see cref="BigBinary"/> for a slice of this binary.
        /// </summary>
        public BigBinary Slice(ulong offset, ulong size)
        {
            // TODO: Copy small binaries
            if (!(offset + size <= Size))
                throw new IndexOutOfRangeException();
            return new SliceBinary(this, offset, size);
        }

        /// <summary>
        /// Reads the entire contents of this <see cref="BigBinary"/> as a string.
        /// </summary>
        public string AsString()
        {
            // TODO: Encoding considerations
            using (Stream stream = OpenStream())
            {
                return new StreamReader(stream).ReadToEnd();
            }
        }
    }

    /// <summary>
    /// A <see cref="BigBinary"/> corresponding to the contents of a manifest resource of an
    /// <see cref="System.Reflection.Assembly"/>.
    /// </summary>
    public sealed class ManifestBinary : BigBinary
    {
        internal ManifestBinary(Assembly assem, string name, ulong size)
            : base(size)
        {
            Assembly = assem;
            Name = name;
        }

        /// <summary>
        /// The assembly in which this resource is found.
        /// </summary>
        public Assembly Assembly { get; }

        /// <summary>
        /// The name of this manifest resource.
        /// </summary>
        public string Name { get; }

        public override Stream OpenStream()
        {
            return Assembly.GetManifestResourceStream(Name);
        }
    }

    /// <summary>
    /// A <see cref="BigBinary"/> representing a slice of a source <see cref="BigBinary"/>.
    /// </summary>
    public sealed class SliceBinary : BigBinary
    {
        internal SliceBinary(BigBinary source, ulong offset, ulong size)
            : base(size)
        {
            Source = source;
            Offset = offset;
        }

        /// <summary>
        /// The <see cref="BigBinary"/> from which this binary gets its data.
        /// </summary>
        public BigBinary Source { get; }

        /// <summary>
        /// The offset of this binary within <see cref="Source"/>.
        /// </summary>
        public ulong Offset { get; }

        public override IDisposable Read(ulong offset, out ISeekableBinaryReader reader)
        {
            return Source.Read(Offset + offset, out reader);
        }

        public override Stream OpenStream()
        {
            throw new NotImplementedException();
        }
    }
}
