﻿using System;
using IO = System.IO;

using Alunite.Data;

namespace Alunite.Resources
{
    /// <summary>
    /// A <see cref="RepositoryViewer"/> which navigates through a file system directory.
    /// </summary>
    public sealed class SystemDirectoryViewer : IDirectoryViewer
    {
        internal SystemDirectoryViewer(string loc)
        {
            Location = loc;
        }

        /// <summary>
        /// The location of this directory.
        /// </summary>
        public string Location { get; }

        /// <summary>
        /// The names of the valid child repositories in this directory.
        /// </summary>
        public Set<string> Children
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Attempts to get a child repository within this directory, returning false if the name is not valid.
        /// </summary>
        public bool TryResolve(string name, out RepositoryViewer repo)
        {
            string nLoc = IO.Path.Combine(Location, name);
            if (IO.Directory.Exists(nLoc))
            {
                repo = new SystemDirectoryViewer(nLoc);
                return true;
            }
            if (IO.File.Exists(nLoc))
            {
                repo = RepositoryViewer.File(new SystemFileBinary(new IO.FileInfo(nLoc)));
                return true;
            }
            repo = default;
            return false;
        }

        public static implicit operator DirectoryViewer(SystemDirectoryViewer dir)
        {
            return new DirectoryViewer(dir);
        }

        public static implicit operator RepositoryViewer(SystemDirectoryViewer dir)
        {
            return (DirectoryViewer)dir;
        }
    }
    
    /// <summary>
    /// A <see cref="BigBinary"/> corresponding to the contents of a file on the local file system.
    /// </summary>
    public sealed class SystemFileBinary : BigBinary
    {
        internal SystemFileBinary(IO.FileInfo info)
            : base((ulong)info.Length)
        {
            Info = info;
        }

        /// <summary>
        /// The <see cref="IO.FileInfo"/> for the file accessed by this <see cref="SystemFileBinary"/>.
        /// </summary>
        public IO.FileInfo Info { get; }

        public override IO.Stream OpenStream()
        {
            return Info.OpenRead();
        }
    }

    /// <summary>
    /// An exception thrown when there is an attempt to access a resource whose source has expired.
    /// </summary>
    public sealed class ResourceExpiredException : Exception
    {

    }
}
