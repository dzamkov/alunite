﻿using System;
using System.Diagnostics;

using Alunite.Data;
using Alunite.Data.Serialization;

namespace Alunite.Resources
{
    /// <summary>
    /// Identifies and describes a <see cref="List{T}"/> which need not be explicitly represented in memory.
    /// </summary>
    public abstract class BigList<T>
    {
        public BigList(uint length)
        {
            Length = length;
        }

        /// <summary>
        /// The length of this list.
        /// </summary>
        public uint Length { get; }

        /// <summary>
        /// The empty list.
        /// </summary>
        public static BigList<T> Empty => MemoryList<T>.Empty;
        
        /// <summary>
        /// Creates an <see cref="IListReader{T}"/> for reading items from this list.
        /// </summary>
        public abstract IDisposable Read(out IListReader<T> reader);

        public static implicit operator List<T>(BigList<T> source)
        {
            if (source is MemoryList<T> memory)
                return memory.Source;
            // TODO: Caching
            T[] items = new T[source.Length];
            using (source.Read(out IListReader<T> reader))
                for (uint i = 0; i < (uint)items.Length; i++)
                    items[i] = reader.ReadItem();
            return new List<T>(items);
        }

        public static implicit operator BigList<T>(List<T> source)
        {
            if (source.Length == 0)
                return Empty;
            return new MemoryList<T>(source);
        }
    }

    /// <summary>
    /// Contains functions related to <see cref="BigList{T}"/>.
    /// </summary>
    public static class BigList
    {
        /// <summary>
        /// Constructs a <see cref="BigList{T}"/> which reads regularly-spaced items from the given binary, assuming
        /// the given encoding.
        /// </summary>
        /// <param name="offset">The byte offset of the start of the first item in <paramref name="binary"/>.</param>
        /// <param name="skip">The number of bytes between consecutive items in <paramref name="binary"/>.</param>
        /// <param name="len">The number of items to be read.</param>
        public static BigList<T> FromBinary<T>(BigBinary binary, Encoding<T> encoding, ulong offset, uint skip, uint len)
        {
            // TODO: Lazy reading
            T[] items = new T[len];
            using (binary.Read(offset, out ISeekableBinaryReader reader))
            {
                for (uint i = 0; i < len; i++)
                {
                    reader.Read(encoding, out items[i]);
                    reader.Skip(skip);
                }
            }
            return new List<T>(items);
        }

        /// <summary>
        /// Constructs a <see cref="BigList{T}"/> which consists of a specified number of copies of the given item.
        /// </summary>
        public static BigList<T> Replicate<T>(uint len, T item)
        {
            // TODO: Specialized implementation
            return List.Replicate(len, item);
        }

        /// <summary>
        /// Constructs a list by applying a set of deviations to <see cref="BigList{T}"/>.
        /// </summary>
        /// <param name="devIndices">A sorted list of indices where the list deviates from <paramref name="baseline"/>.</param>
        /// <param name="devValues">The values associated with the deviations in <paramref name="devIndices"/>.</param>
        // TODO: Combine indices and values into one list
        public static BigList<T> Sparse<T>(BigList<T> baseline, BigList<uint> devIndices, BigList<T> devValues)
        {
            if (devIndices.Length != devValues.Length)
                throw new ArgumentException("Deviation lists must have the same length");
            return new SparseList<T>(baseline, devIndices, devValues);
        }
    }

    /// <summary>
    /// A <see cref="BigList{T}"/> which is stored in memory.
    /// </summary>
    public sealed class MemoryList<T> : BigList<T>
    {
        internal MemoryList(List<T> source)
            : base(source.Length)
        {
            Source = source;
        }

        /// <summary>
        /// The <see cref="MemoryList{T}"/> for the empty list.
        /// </summary>
        public static new MemoryList<T> Empty { get; } = new MemoryList<T>(List.Empty<T>());

        /// <summary>
        /// The <see cref="List{T}"/> source for this <see cref="MemoryList{T}"/>.
        /// </summary>
        public List<T> Source { get; }

        public override IDisposable Read(out IListReader<T> reader)
        {
            reader = new ListReader<T>(Source);
            return null;
        }
    }

    /// <summary>
    /// A <see cref="BigList{T}"/> defined in terms of its deviations from a baseline <see cref="BigList{T}"/>.
    /// </summary>
    public sealed class SparseList<T> : BigList<T>
    {
        internal SparseList(BigList<T> baseline, BigList<uint> devIndices, BigList<T> devValues)
            : base(baseline.Length)
        {
            Debug.Assert(devIndices.Length == devValues.Length);
            Baseline = baseline;
            DeviationIndices = devIndices;
            DeviationValues = devValues;
        }

        /// <summary>
        /// The <see cref="BigList{T}"/> which provides the values for items that are not overridden by a deviation.
        /// </summary>
        public BigList<T> Baseline { get; }

        /// <summary>
        /// A sorted list of indices where this list deviates from <see cref="Baseline"/>.
        /// </summary>
        public BigList<uint> DeviationIndices { get; }

        /// <summary>
        /// The values associated with the deviations in <see cref="DeviationIndices"/>.
        /// </summary>
        public BigList<T> DeviationValues { get; }
        
        public override IDisposable Read(out IListReader<T> reader)
        {
            var res = DisposableHelper.Group(
                Baseline.Read(out var baseline),
                DeviationIndices.Read(out var devIndices),
                DeviationValues.Read(out var devValues));
            reader = new _Reader(baseline, devIndices, devValues);
            return res;
        }

        /// <summary>
        /// An <see cref="IListReader{T}"/> for a <see cref="SparseList{T}"/>.
        /// </summary>
        internal sealed class _Reader : IListReader<T>
        {
            private uint _index;
            private IListReader<T> _baseline;
            private IListReader<uint> _devIndices;
            private IListReader<T> _devValues;
            private uint _nextDevIndex;
            private T _nextDevValue;
            public _Reader(
                IListReader<T> baseline,
                IListReader<uint> devIndices,
                IListReader<T> devValues)
            {
                _index = 0;
                _baseline = baseline;
                _devIndices = devIndices;
                _devValues = devValues;
                if (_devIndices.TryReadItem(out _nextDevIndex))
                    _nextDevValue = _devValues.ReadItem();
                else
                    _nextDevIndex = uint.MaxValue;
            }

            uint IListReader<T>.Read(Span<T> buffer)
            {
                // Read baseline
                uint numRead = _baseline.Read(buffer);

                // Apply deviations
                // TODO: Allow for multiple leftover deviations
                while (_nextDevIndex < _index + numRead)
                {
                    buffer[(int)(_nextDevIndex - _index)] = _nextDevValue;
                    if (_devIndices.TryReadItem(out _nextDevIndex))
                        _nextDevValue = _devValues.ReadItem();
                    else
                        _nextDevIndex = uint.MaxValue;
                }

                // Advance index
                _index += numRead;
                return numRead;
            }

            void IListReader<T>.Skip(uint num)
            {
                throw new NotImplementedException();
            }
        }
    }
}
