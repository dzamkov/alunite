﻿using System;
using System.IO;
using System.Reflection;

using Alunite.Data;

namespace Alunite.Resources
{
    /// <summary>
    /// Contains functions related to assets.
    /// </summary>
    public static class Asset
    {
        /// <summary>
        /// Registers an assembly which may be associated with an assets directory.
        /// </summary>
        public static void Register(Assembly assem)
        {
            // TODO: Package management
            DirectoryInfo dir = System.IO.Directory.GetParent(assem.Location);
            string assemName = assem.GetName().Name;
            while (dir != null)
            {
                string path = System.IO.Path.Combine(dir.FullName, assemName, "Assets");
                if (System.IO.Directory.Exists(path))
                    Register(path);
                dir = dir.Parent;
            }
        }

        /// <summary>
        /// Registers an assets directory from which assets may be loaded.
        /// </summary>
        public static void Register(string path)
        {
            lock (_locs)
            {
                _locs.Append(path);
            }
        }

        /// <summary>
        /// The list of paths from which assets may be loaded.
        /// </summary>
        private static ListBuilder<string> _locs = new ListBuilder<string>();

        /// <summary>
        /// Constructs a <see cref="RepositoryViewer"/> for the asset repository.
        /// </summary>
        public static RepositoryViewer View()
        {
            DirectoryViewer dir = DirectoryViewer.Empty;
            foreach (string loc in _locs)
                dir |= new SystemDirectoryViewer(loc);
            return dir;
        }
        
        /// <summary>
        /// Gets the <see cref="BigBinary"/> for the contents of the asset at the given path.
        /// </summary>
        public static BigBinary Load(Path path)
        {
            if (!(View() / path).TryResolveAsFile(out BigBinary res))
                throw new Exception("Asset not found");
            return res;
        }
    }
}
