﻿using System;

using Alunite.Data;

namespace Alunite.Resources
{
    /// <summary>
    /// A specialization of <see cref="Bag{T}"/> for <see cref="IDisposable"/>s, which is itself disposable.
    /// </summary>
    public struct DisposableBag : IDisposable
    {
        internal IDisposable[] _items;
        internal DisposableBag(IDisposable[] items)
        {
            _items = items;
        }

        /// <summary>
        /// Disposes all of the contents of this bag.
        /// </summary>
        public void Dispose()
        {
            foreach (IDisposable item in _items)
                item.Dispose();
        }

        public static implicit operator DisposableBag(Bag<IDisposable> source)
        {
            return new DisposableBag(source._items);
        }
    }
}
