﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;

using Alunite.Data;
using Alunite.Diagnostics;

namespace Alunite.Resources
{
    /// <summary>
    /// Either a <see cref="BigBinary"/> file, or a directory of named <see cref="Repository"/>s. This describes a
    /// snapshot of a file system rooted at a particular path.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct Repository : IPrintable
    {
        internal object _def;
        internal Repository(object def)
        {
            _def = def;
        }
        
        /// <summary>
        /// Constructs a file <see cref="Repository"/> with the given binary contents.
        /// </summary>
        public static Repository File(BigBinary binary)
        {
            return new Repository(binary);
        }

        /// <summary>
        /// Constructs a <see cref="Repository"/> which captures the manifest resources for the given assembly.
        /// </summary>
        public static Repository Manifest(Assembly assem)
        {
            return Directory.Manifest(assem);
        }

        /// <summary>
        /// Indicates whether this <see cref="Repository"/> is a directory.
        /// </summary>
        public bool IsDirectory(out Directory dir)
        {
            dir._children._bins = _def as Map<string, Repository>._Bin[];
            return !(dir._children._bins is null);
        }

        /// <summary>
        /// Indicates whether this <see cref="Repository"/> is a file, and if so, gets the binary
        /// contents of that file.
        /// </summary>
        public bool IsFile(out BigBinary binary)
        {
            binary = _def as BigBinary;
            return !(binary is null);
        }
        
        /// <summary>
        /// Attempts to get the <see cref="Repository"/> at the given path within this <see cref="Repository"/>,
        /// returning false if the path is not valid, or if this is a file.
        /// </summary>
        public bool TryResolve(Path path, out Repository repo)
        {
            if (IsDirectory(out Directory dir))
                return dir.TryResolve(path, out repo);
            repo = default;
            return false;
        }
        
        public static RepositoryPath operator /(Repository repo, string name)
        {
            return new RepositoryPath(repo, Path.Base / name);
        }
        
        public static RepositoryPath operator /(Repository repo, Path path)
        {
            return new RepositoryPath(repo, path);
        }

        public static implicit operator RepositoryViewer(Repository repo)
        {
            if (repo.IsDirectory(out var dir))
                return dir;
            return new RepositoryViewer(repo._def);
        }

        public override string ToString()
        {
            return this.ToShortString();
        }

        void IPrintable.Print(IPrinter printer, Precedence prec)
        {
            if (IsDirectory(out _))
                printer.Write("Directory(...)");
            else if (IsFile(out _))
                printer.Write("File(...)");
            else
                printer.Write("<invalid>");
        }
    }

    /// <summary>
    /// A collection of named <see cref="Repository"/>s, which is itself a <see cref="Repository"/>.
    /// </summary>
    public struct Directory : IDirectoryViewer
    {
        internal Map<string, Repository> _children;
        public Directory(Map<string, Repository> children)
        {
            _children = children;
        }

        /// <summary>
        /// The contents of this directory.
        /// </summary>
        public Map<string, Repository> Children => _children;

        /// <summary>
        /// The empty <see cref="Directory"/>.
        /// </summary>
        public static Directory Empty => new Directory(Map<string, Repository>.Empty);

        /// <summary>
        /// Constructs a <see cref="Directory"/> which captures the manifest resources for the given assembly.
        /// </summary>
        public static Directory Manifest(Assembly assem)
        {
            // TODO: Cache repo
            // Get sorted list of resource names
            string[] paths = assem.GetManifestResourceNames();
            Array.Sort(paths, StringComparer.InvariantCulture);

            // Build directories from names
            return _buildManifestDirectory(assem, paths, 0);
        }

        /// <summary>
        /// Builds a <see cref="Directory"/> for a directory of manifest resources.
        /// </summary>
        /// <param name="resNames">The full names of the resources in the directory, sorted in some lexicographic order.</param>
        /// <param name="descIndex">The index of the first character in <paramref name="paths"/> to be considered part of
        /// the descendant path of the returned <see cref="Repository"/>.</param>
        private static Directory _buildManifestDirectory(Assembly assem, ReadOnlySpan<string> paths, int descIndex)
        {
            var children = new MapBuilder<string, Repository>();
            int pathIndex = 0;
            while (pathIndex < paths.Length)
            {
                string path = paths[pathIndex];

                // Delimit paths at dots
                int dotIndex = path.IndexOf('.', descIndex);
            nextDot:
                if (dotIndex >= 0)
                {
                    // Trailing lowercase names are considered to be extensions instead of children
                    int innerDescIndex = dotIndex + 1;
                    if (char.IsLower(path[innerDescIndex]))
                    {
                        dotIndex = path.IndexOf('.', innerDescIndex);
                        goto nextDot;
                    }
                    string name = path.Substring(descIndex, dotIndex - descIndex);

                    // Find the first path in paths that does not share name
                    int endPathIndex = pathIndex + 1;
                    while (endPathIndex < paths.Length)
                    {
                        string endPath = paths[endPathIndex];
                        if (endPath.Length < innerDescIndex)
                            goto foundEnd;
                        for (int i = descIndex; i < innerDescIndex; i++)
                            if (path[i] != endPath[i])
                                goto foundEnd;
                        endPathIndex++;
                    }
                foundEnd:

                    // Build directory from slice of paths that share name
                    ReadOnlySpan<string> innerPaths = paths.Slice(pathIndex, endPathIndex - pathIndex);
                    children.Add(name, _buildManifestDirectory(assem, innerPaths, innerDescIndex));
                    pathIndex = endPathIndex;
                }
                else
                {
                    // TODO: Get binary size without opening the stream?
                    string name = path.Substring(descIndex);
                    using (var stream = assem.GetManifestResourceStream(path))
                    {
                        children.Add(name, Repository.File(new ManifestBinary(assem, path, (ulong)stream.Length)));
                    }
                    pathIndex++;
                }
            }
            return new Directory(children.Finish());
        }

        /// <summary>
        /// Indicates whether this is <see cref="Empty"/>.
        /// </summary>
        public bool IsEmpty => Children.IsEmpty;
        
        /// <summary>
        /// Attempts to get a child repository within this directory, returning false if the name is not valid.
        /// </summary>
        public bool TryResolve(string name, out Repository repo)
        {
            return Children.TryGet(name, out repo);
        }

        /// <summary>
        /// Attempts to get the <see cref="Repository"/> at the given path within this directory,
        /// returning false if the path is not valid.
        /// </summary>
        public bool TryResolve(Path path, out Repository repo)
        {
            if (path.IsChild(out Path parentPath, out string name))
            {
                return TryResolve(path, out repo) &&
                    repo.IsDirectory(out var dir) &&
                    dir.TryResolve(name, out repo);
            }
            else
            {
                repo = this;
                return path.IsBase;
            }
        }

        public static RepositoryPath operator /(Directory dir, string name)
        {
            return (Repository)dir / name;
        }

        public static RepositoryPath operator /(Directory dir, Path path)
        {
            return (Repository)dir / path;
        }

        public static implicit operator Repository(Directory dir)
        {
            return new Repository(dir._children._bins);
        }

        public static implicit operator DirectoryViewer(Directory dir)
        {
            return new DirectoryViewer(dir);
        }

        public static implicit operator RepositoryViewer(Directory dir)
        {
            return (DirectoryViewer)dir;
        }

        Set<string> IDirectoryViewer.Children => Children.Keys;
        
        bool IDirectoryViewer.TryResolve(string name, out RepositoryViewer repo)
        {
            if (TryResolve(name, out Repository source))
            {
                repo = source;
                return true;
            }
            repo = default;
            return false;
        }
    }

    /// <summary>
    /// A path from a directory <see cref="Repository"/> to some descendant <see cref="Repository"/>.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct Path : IPrintable
    {
        internal _Internal _def;
        internal Path(_Internal def)
        {
            _def = def;
        }
        
        /// <summary>
        /// A path which refers to the base directory.
        /// </summary>
        public static Path Base => new Path(_Internal.Base);

        /// <summary>
        /// A special path which is always invalid, and from which no valid path can be reached.
        /// </summary>
        public static Path Void => default;

        /// <summary>
        /// The parent for this path, or <see cref="Void"/> if such path doesn't exist.
        /// </summary>
        public Path Parent => new Path(_def?.Parent);

        /// <summary>
        /// The name of the last child in this path, or <see cref="null"/> if this is not
        /// a child path.
        /// </summary>
        public string Name => _def?.Name;

        /// <summary>
        /// The contents of this path after the last dot in <see cref="Name"/>, or <see cref="null"/> if this path
        /// does not have an extension.
        /// </summary>
        public string Extension
        {
            get
            {
                string name = Name;
                if (name is null)
                    return null;
                int index = name.LastIndexOf('.');
                if (index < 0)
                    return null;
                return name.Substring(index + 1);
            }
        }

        /// <summary>
        /// Indicates whether this is <see cref="Base"/>.
        /// </summary>
        public bool IsBase => _def == _Internal.Base;

        /// <summary>
        /// Indicates whether this is <see cref="Void"/>.
        /// </summary>
        public bool IsVoid => _def is null;

        /// <summary>
        /// Indicates whether this path refers to a child of some parent <see cref="Path"/>.
        /// </summary>
        public bool IsChild(out Path parent, out string name)
        {
            if (!(_def is null))
            {
                parent = new Path(_def.Parent);
                name = _def.Name;
                return !(_def.Parent is null);
            }
            parent = default;
            name = default;
            return false;
        }
        
        /// <summary>
        /// The internal definition for <see cref="Path"/>.
        /// </summary>
        internal sealed class _Internal
        {
            public _Internal(_Internal parent, string name)
            {
                Parent = parent;
                Name = name;
            }

            /// <summary>
            /// The <see cref="_Internal"/> for <see cref="Path.Base"/>.
            /// </summary>
            public static _Internal Base { get; } = new _Internal(null, null);

            /// <summary>
            /// The path to the parent of this path, or null if the parent is <see cref="Base"/>.
            /// </summary>
            public _Internal Parent { get; }

            /// <summary>
            /// The name of the last descendant of this path.
            /// </summary>
            public string Name { get; }

            /// <summary>
            /// Gets a child of this path.
            /// </summary>
            internal _Internal _child(string name)
            {
                return new _Internal(this, name);
            }
            
            /// <summary>
            /// Gets a descendant of this path.
            /// </summary>
            internal _Internal _descendant(_Internal path)
            {
                if (path == Base)
                    return this;
                return _descendant(path.Parent)._child(path.Name);
            }
        }

        public static Path operator /(Path path, string name)
        {
            if (path.IsVoid)
                return Void;
            return new Path(new _Internal(path._def, name));
        }

        public static Path operator /(Path a, Path b)
        {
            if (a.IsVoid || b.IsVoid)
                return Void;
            if (a.IsBase)
                return b;
            return new Path(a._def._descendant(b._def));
        }

        public static Path operator +(Path path, string ext)
        {
            if (path.IsVoid || path.IsBase)
                return Void;
            return new Path(path._def.Parent._child(path._def.Name + ext));
        }

        public override string ToString()
        {
            return this.ToShortString();
        }

        void IPrintable.Print(IPrinter printer, Precedence prec)
        {
            ((IPrintable)(RelativePath)this).Print(printer, prec);
        }
    }

    /// <summary>
    /// An extension of <see cref="Path"/> that allows references to siblings, ancestors and the
    /// root <see cref="Repository"/> in addition to descendant <see cref="Repository"/>s.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct RelativePath : IPrintable
    {
        internal uint _up;
        internal Path._Internal _def;
        internal RelativePath(uint up, Path._Internal def)
        {
            _up = up;
            _def = def;
        }

        /// <summary>
        /// A relative path which refers to the root directory.
        /// </summary>
        public static RelativePath Root => new RelativePath(uint.MaxValue, Path._Internal.Base);

        /// <summary>
        /// A relative path which refers to the base directory.
        /// </summary>
        public static RelativePath Base => new RelativePath(0, Path._Internal.Base);

        /// <summary>
        /// A special path which is always invalid, and from which no valid path can be reached.
        /// </summary>
        public static RelativePath Void => default;

        /// <summary>
        /// Constructs a <see cref="RelativePath"/> which refers to the <paramref name="index"/>th ancestor of
        /// the base directory.
        /// </summary>
        public static RelativePath Up(uint index)
        {
            return new RelativePath(index, Path._Internal.Base);
        }

        /// <summary>
        /// Parses a relative path from the given path string.
        /// </summary>
        public static RelativePath Parse(string path)
        {
            int index = 0;

            // Determine whether this is a relative or absolute path based on presence of leading slash
            RelativePath res;
            if (path[index] == '/')
            {
                res = Root;
                index++;
            }
            else
            {
                res = Base;
            }

        next:
            // Parse path components
            int nIndex = path.IndexOf('/', index);
            if (nIndex >= 0)
            {
                if (index == nIndex)
                    throw new Exception("Bad path format");

                // Check for parent delimiter
                for (int i = index; i < nIndex; i++)
                {
                    if (path[i] != '.')
                    {
                        // Navigate down
                        res /= path.Substring(index, nIndex - index);
                        index = nIndex;
                        goto next;
                    }
                }

                // Navigate up by an amount corresponding to the number of dots
                res /= Up((uint)(nIndex - index - 1));
                goto next;
            }
            else if (index == path.Length)
            {
                // Path ends at directory
                return res;
            }
            else
            {
                // Path ends with trailing file or directory
                return res / path.Substring(index);
            }
        }

        /// <summary>
        /// Indicates whether this is <see cref="Root"/>.
        /// </summary>
        public bool IsRoot => _def == Path._Internal.Base && _up == uint.MaxValue;

        /// <summary>
        /// Indicates whether this is <see cref="Base"/>.
        /// </summary>
        public bool IsBase => _def == Path._Internal.Base && _up == 0;

        /// <summary>
        /// Indicates whether this is <see cref="Void"/>.
        /// </summary>
        public bool IsVoid => _def is null;

        /// <summary>
        /// Indicates whether this path refers to a child of some parent <see cref="RelativePath"/>.
        /// </summary>
        public bool IsChild(out RelativePath parent, out string name)
        {
            if (!(_def is null))
            {
                parent = new RelativePath(_up, _def.Parent);
                name = _def.Name;
                return !(_def.Parent is null);
            }
            parent = default;
            name = default;
            return false;
        }

        /// <summary>
        /// A <see cref="RelativePath"/> which refers to the parent of this path, or <see cref="Void"/> if
        /// such path doesn't exist.
        /// </summary>
        public RelativePath Parent => throw new NotImplementedException();

        public static implicit operator RelativePath(Path path)
        {
            return new RelativePath(0, path._def);
        }

        public static RelativePath operator /(RelativePath path, string name)
        {
            if (path.IsVoid)
                return Void;
            return new RelativePath(path._up, new Path._Internal(path._def, name));
        }

        public static RelativePath operator /(RelativePath a, RelativePath b)
        {
            if (a.IsVoid || b.IsVoid)
                return Void;
            if (b._up == uint.MaxValue)
                return b;
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return this.ToShortString();
        }

        void IPrintable.Print(IPrinter printer, Precedence prec)
        {
            if (_def == Path._Internal.Base)
            {
                if (_up == uint.MaxValue)
                {
                    printer.Write("Root");
                }
                else if (_up == 0)
                {
                    printer.Write("Base");
                }
                else
                {
                    printer.Write("Up(");
                    printer.Print(Precedence.Free, _up);
                    printer.WriteItem(')');
                }
            }
            else if (IsChild(out RelativePath parent, out string name))
            {
                printer.Print(Precedence.Multiplicative, parent);
                printer.Write(" / ");
                printer.Print(Precedence.Unary, name);
            }
            else
            {
                printer.Write("Void");
            }
        }
    }

    /// <summary>
    /// Identifies a <see cref="Resources.Path"/> within a <see cref="Repository"/>. The path may or may not be valid.
    /// </summary>
    public struct RepositoryPath
    {
        public RepositoryPath(Repository @base, Path path)
        {
            Base = path.IsVoid ? default : @base;
            Path = path;
        }

        /// <summary>
        /// The <see cref="Repository"/> from which this <see cref="RepositoryPath"/> is based, assuming this
        /// is not <see cref="Void"/>.
        /// </summary>
        public Repository Base { get; }

        /// <summary>
        /// The path component of this <see cref="RepositoryPath"/>.
        /// </summary>
        public Path Path { get; }

        /// <summary>
        /// A special <see cref="RepositoryPath"/> from which no valid path can be reached.
        /// </summary>
        public static RepositoryPath Void => default;

        /// <summary>
        /// Indicates whether this is <see cref="Void"/>.
        /// </summary>
        public bool IsVoid => Path.IsVoid;

        /// <summary>
        /// Attempts to get the <see cref="Repository"/> at this path, returning false if the path is not valid.
        /// </summary>
        public bool TryResolve(out Repository repo)
        {
            if (!IsVoid)
                return Base.TryResolve(Path, out repo);
            repo = default;
            return false;
        }

        /// <summary>
        /// Attempts to get the file at this path, returning false if the path is invalid, or does not lead to a file.
        /// </summary>
        public bool TryResolveAsFile(out BigBinary binary)
        {
            if (!IsVoid && Base.TryResolve(Path, out Repository repo))
                return repo.IsFile(out binary);
            binary = default;
            return false;
        }

        public static RepositoryPath operator /(RepositoryPath repo, string name)
        {
            return new RepositoryPath(repo.Base, repo.Path / name);
        }

        public static RepositoryPath operator /(RepositoryPath repo, Path path)
        {
            return new RepositoryPath(repo.Base, repo.Path / path);
        }

        public static RepositoryPath operator +(RepositoryPath repo, string ext)
        {
            return new RepositoryPath(repo.Base, repo.Path + ext);
        }

        public static implicit operator RepositoryPath(Repository repo)
        {
            return new RepositoryPath(repo, Path.Base);
        }

        public static explicit operator Repository(RepositoryPath repo)
        {
            if (!repo.TryResolve(out Repository nRepo))
                throw new Exception("Path is not valid");
            return nRepo;
        }

        public static implicit operator RepositoryPathViewer(RepositoryPath repo)
        {
            if (repo.IsVoid)
                return RepositoryPathViewer.Void;
            return (RepositoryViewer)repo.Base / repo.Path;
        }
    }

    /// <summary>
    /// A helper class for navigating a <see cref="Repository"/> which need not be explicitly represented in memory.
    /// </summary>
    public struct RepositoryViewer
    {
        private object _def;
        internal RepositoryViewer(object def)
        {
            _def = def;
        }

        /// <summary>
        /// A sentinel value for <see cref="RepositoryViewer"/> used to mark an invalid repository.
        /// </summary>
        public static RepositoryViewer Invalid => default;

        /// <summary>
        /// Constructs a <see cref="RepositoryViewer"/> for a file with the given contents.
        /// </summary>
        public static RepositoryViewer File(BigBinary binary)
        {
            return new RepositoryViewer(binary);
        }

        /// <summary>
        /// Indicates whether this is <see cref="Invalid"/>.
        /// </summary>
        public bool IsInvalid => _def is null;

        /// <summary>
        /// Indicates whether this repository is a file, and if so, gets the binary contents of that file.
        /// </summary>
        public bool IsFile(out BigBinary binary)
        {
            binary = _def as BigBinary;
            return !(binary is null);
        }

        /// <summary>
        /// Indicates whether this repository is a directory.
        /// </summary>
        public bool IsDirectory(out DirectoryViewer dir)
        {
            dir._def = _def as IDirectoryViewer;
            return !(dir._def is null);
        }

        /// <summary>
        /// Attempts to get the repository at the given path within this repository, returning false if
        /// the path is not valid, or if this is a file.
        /// </summary>
        public bool TryResolve(Path path, out RepositoryViewer repo)
        {
            if (path.IsChild(out Path parentPath, out string name))
            {
                return TryResolve(path, out repo) &&
                    repo.IsDirectory(out DirectoryViewer dir) &&
                    dir.TryResolve(name, out repo);
            }
            else
            {
                repo = this;
                return true;
            }
        }
        
        /// <summary>
        /// Merges the contents of two <see cref="RepositoryViewer"/>s, preferring the content of the second when there
        /// is a conflict.
        /// </summary>
        public static RepositoryViewer operator |(RepositoryViewer secondary, RepositoryViewer primary)
        {
            if (!(primary._def is IDirectoryViewer primaryDirDef))
                return primary;
            if (!(secondary._def is IDirectoryViewer secondaryDirDef))
                return primary;
            return new DirectoryViewer(secondaryDirDef) | new DirectoryViewer(primaryDirDef);
        }

        public static RepositoryPathViewer operator /(RepositoryViewer repo, string name)
        {
            return (RepositoryPathViewer)repo / name;
        }

        public static RepositoryPathViewer operator /(RepositoryViewer repo, Path path)
        {
            return (RepositoryPathViewer)repo / path;
        }
    }

    /// <summary>
    /// A helper class for navigating a directory <see cref="Repository"/> which need not be
    /// explicitly represented in memory.
    /// </summary>
    public struct DirectoryViewer
    {
        internal IDirectoryViewer _def;
        public DirectoryViewer(IDirectoryViewer def)
        {
            _def = def;
        }

        /// <summary>
        /// The definition for this <see cref="DirectoryViewer"/>.
        /// </summary>
        public IDirectoryViewer Definition => _def;

        /// <summary>
        /// A <see cref="DirectoryViewer"/> for the empty directory.
        /// </summary>
        public static DirectoryViewer Empty { get; } = Directory.Empty;

        /// <summary>
        /// The names of the valid child repositories in this directory.
        /// </summary>
        public Set<string> Children => Definition.Children;

        /// <summary>
        /// Attempts to get a child repository within this directory, returning false if the name is not
        /// valid, or if this is a file.
        /// </summary>
        public bool TryResolve(string name, out RepositoryViewer repo)
        {
            return Definition.TryResolve(name, out repo);
        }

        /// <summary>
        /// Merges the contents of two <see cref="DirectoryViewer"/>s, preferring the content of the second when there
        /// is a conflict.
        /// </summary>
        public static DirectoryViewer operator |(DirectoryViewer secondary, DirectoryViewer primary)
        {
            if (secondary._def == Empty._def)
                return primary;
            if (primary._def == Empty._def)
                return secondary;
            return new MergeDirectoryViewer(primary, secondary);
        }

        public static RepositoryPathViewer operator /(DirectoryViewer dir, string name)
        {
            return (RepositoryPathViewer)dir / name;
        }

        public static RepositoryPathViewer operator /(DirectoryViewer dir, Path path)
        {
            return (RepositoryPathViewer)dir / path;
        }

        public static implicit operator RepositoryViewer(DirectoryViewer dir)
        {
            return new RepositoryViewer(dir._def);
        }
    }

    /// <summary>
    /// A <see cref="DirectoryViewer"/> which merges the contents of two <see cref="DirectoryViewer"/>s.
    /// </summary>
    public sealed class MergeDirectoryViewer : IDirectoryViewer
    {
        public MergeDirectoryViewer(DirectoryViewer primary, DirectoryViewer secondary)
        {
            Primary = primary;
            Secondary = secondary;
        }

        /// <summary>
        /// The source <see cref="DirectoryViewer"/> which takes priority.
        /// </summary>
        public DirectoryViewer Primary { get; }

        /// <summary>
        /// The source <see cref="DirectoryViewer"/> which is consulted after <see cref="Primary"/>.
        /// </summary>
        public DirectoryViewer Secondary { get; }

        /// <summary>
        /// The names of the valid child repositories in this directory.
        /// </summary>
        public Set<string> Children => Primary.Children | Secondary.Children;

        /// <summary>
        /// Attempts to get a child repository within this directory, returning false if the name is not
        /// valid, or if this is a file.
        /// </summary>
        public bool TryResolve(string name, out RepositoryViewer repo)
        {
            if (Primary.TryResolve(name, out repo))
            {
                if (repo.IsDirectory(out DirectoryViewer primaryDir))
                {
                    if (Secondary.TryResolve(name, out RepositoryViewer secondaryRepo))
                    {
                        if (secondaryRepo.IsDirectory(out DirectoryViewer secondaryDir))
                        {
                            // If this resolves to a directory in both sources, merge them
                            repo = secondaryDir | primaryDir;
                            return true;
                        }
                    }
                }
                return true;
            }
            return Secondary.TryResolve(name, out repo);
        }

        public static implicit operator DirectoryViewer(MergeDirectoryViewer dir)
        {
            return new DirectoryViewer(dir);
        }
    }

    /// <summary>
    /// A definition for a <see cref="DirectoryViewer"/>.
    /// </summary>
    public interface IDirectoryViewer
    {
        /// <summary>
        /// The names of the valid child repositories in this directory.
        /// </summary>
        Set<string> Children { get; }

        /// <summary>
        /// Attempts to get a child repository within this directory, returning false if the name is not valid.
        /// </summary>
        bool TryResolve(string name, out RepositoryViewer repo);
    }

    /// <summary>
    /// A helper class for navigating a <see cref="Repository"/> from a particular <see cref="Path"/> within it.
    /// </summary>
    public struct RepositoryPathViewer
    {
        private _Internal _def;
        private RepositoryPathViewer(_Internal def)
        {
            _def = def;
        }

        /// <summary>
        /// A special <see cref="RepositoryPathViewer"/> from which no valid path can be reached.
        /// </summary>
        public static RepositoryPathViewer Void => default;

        /// <summary>
        /// Indicates whether this is <see cref="Void"/>.
        /// </summary>
        public bool IsVoid => _def is null;
        
        /// <summary>
        /// A <see cref="RepositoryPathViewer"/> for the parent of the current path, or <see cref="Void"/> if such
        /// path doesn't exist.
        /// </summary>
        public RepositoryPathViewer Parent => new RepositoryPathViewer(_def?.Parent);

        /// <summary>
        /// The name of the last child in this path, or <see cref="null"/> if this is not a child path.
        /// </summary>
        public string Name => _def?.Name;

        /// <summary>
        /// The contents of this path after the last dot in <see cref="Name"/>, or <see cref="null"/> if this path
        /// does not have an extension.
        /// </summary>
        public string Extension
        {
            get
            {
                string name = Name;
                if (name is null)
                    return null;
                int index = name.LastIndexOf('.');
                if (index < 0)
                    return null;
                return name.Substring(index + 1);
            }
        }

        /// <summary>
        /// Attempts to get the repository at this path, returning false if the path is invalid.
        /// </summary>
        public bool TryResolve(out RepositoryViewer repo)
        {
            if (!IsVoid && !_def.Repository.IsInvalid)
            {
                repo = _def.Repository;
                return true;
            }
            repo = default;
            return false;
        }

        /// <summary>
        /// Attempts to get the file at this path, returning false if the path is invalid, or does not lead to a file.
        /// </summary>
        public bool TryResolveAsFile(out BigBinary binary)
        {
            if (!IsVoid && !_def.Repository.IsInvalid)
                return _def.Repository.IsFile(out binary);
            binary = default;
            return false;
        }

        /// <summary>
        /// The internal definition for a <see cref="RepositoryPathViewer"/>.
        /// </summary>
        private sealed class _Internal
        {
            public _Internal(_Internal parent, string name, RepositoryViewer repo)
            {
                Parent = parent;
                Name = name;
                Repository = repo;
            }

            /// <summary>
            /// The path to the parent of this path, or null if this is the root path.
            /// </summary>
            public _Internal Parent { get; }

            /// <summary>
            /// The name of the last descendant of this path, or null if this is the root path.
            /// </summary>
            public string Name { get; }

            /// <summary>
            /// The <see cref="RepositoryViewer"/> for the repository at this path or
            /// <see cref="RepositoryViewer.Invalid"/> if the path is not valid.
            /// </summary>
            public RepositoryViewer Repository { get; }

            /// <summary>
            /// Gets a child of this path.
            /// </summary>
            internal _Internal _child(string name)
            {
                if (!Repository.IsInvalid &&
                    Repository.IsDirectory(out DirectoryViewer dir) &&
                    dir.TryResolve(name, out var repo))
                {
                    return new _Internal(this, name, repo);
                }
                else
                {
                    return new _Internal(this, name, RepositoryViewer.Invalid);
                }
            }

            /// <summary>
            /// Gets a descendant of the <paramref name="index"/>th ancestor of this path.
            /// </summary>
            internal _Internal _ancestorDescendant(uint index, Path._Internal path)
            {
                _Internal cur = this;
                while (index > 0)
                {
                    cur = cur.Parent;
                    if (cur == null)
                        return null;
                    index--;
                }
                return cur._descendant(path);
            }

            /// <summary>
            /// Gets a descendant of this path.
            /// </summary>
            internal _Internal _descendant(Path._Internal path)
            {
                if (path == Path._Internal.Base)
                    return this;
                return _descendant(path.Parent)._child(path.Name);
            }
        }

        public static RepositoryPathViewer operator /(RepositoryPathViewer repo, string name)
        {
            if (repo.IsVoid)
                return Void;
            return new RepositoryPathViewer(repo._def._child(name));
        }

        public static RepositoryPathViewer operator /(RepositoryPathViewer repo, RelativePath path)
        {
            if (repo.IsVoid || path.IsVoid)
                return Void;
            return new RepositoryPathViewer(repo._def._ancestorDescendant(path._up, path._def));
        }

        public static RepositoryPathViewer operator +(RepositoryPathViewer repo, string ext)
        {
            if (repo.IsVoid || repo._def.Parent is null)
                return Void;
            return new RepositoryPathViewer(repo._def.Parent._child(repo._def.Name + ext));
        }

        public static implicit operator RepositoryPathViewer(Repository repo)
        {
            return new RepositoryPathViewer(new _Internal(null, null, repo));
        }

        public static implicit operator RepositoryPathViewer(RepositoryViewer repo)
        {
            return new RepositoryPathViewer(new _Internal(null, null, repo));
        }

        public static implicit operator RepositoryPathViewer(DirectoryViewer dir)
        {
            return (RepositoryViewer)dir;
        }
    }
}
