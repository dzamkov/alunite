﻿using System;
using System.Diagnostics;
using System.Threading;

using Alunite.Data;

namespace Alunite.Resources
{
    /// <summary>
    /// A mutable, thread-safe collection of resources of type <typeparamref name="TResource"/> indexed
    /// by keys of type <typeparamref name="TKey"/>. There may be multiple resources per key.
    /// Resources may only be removed when they are "dead", and dead resources may be removed at any time.
    /// </summary>
    public sealed class Registry<TKey, TResource>
        where TResource : ITransient
    {
        private _Table _table;
        private Registry()
        {
            uint[] hashtable = new uint[8];
            for (uint i = 0; i < hashtable.Length; i++)
                hashtable[i] = uint.MaxValue;
            _KeyEntry[] entries = new _KeyEntry[7];
            _table = new _Table(hashtable, entries, 0);
        }

        /// <summary>
        /// Tracks the arrays that define the contents <see cref="Registry{TKey, TResource}"/>. When these arrays
        /// need to be expanded, the entire <see cref="_Table"/> is swapped out (an atomic operation).
        /// </summary>
        private sealed class _Table
        {
            public _Table(uint[] hashtable, _KeyEntry[] entries, uint nextFreeEntryIndex)
            {
                Hashtable = hashtable;
                Entries = entries;
                NextFreeEntryIndex = nextFreeEntryIndex;
            }

            /// <summary>
            /// The hashtable used to index <see cref="Entries"/>. This is a simple linear probing hashtable whose bins
            /// store an index in <see cref="Entries"/>, or <see cref="uint.MaxValue"/> if empty.
            /// </summary>
            public uint[] Hashtable { get; }

            /// <summary>
            /// The entries stored in the table, organized by key.
            /// </summary>
            public _KeyEntry[] Entries { get; }

            /// <summary>
            /// The index of the next unused entry in <see cref="Entries"/>.
            /// </summary>
            public uint NextFreeEntryIndex;
        }

        /// <summary>
        /// Tracks the set of entries associated with a particular key.
        /// </summary>
        private struct _KeyEntry
        {
            /// <summary>
            /// The key for this bag.
            /// </summary>
            public TKey Key;

            /// <summary>
            /// The <see cref="Bag{T}"/> of resources with this key. This must be locked for writing, but not necessarily
            /// for reading.
            /// </summary>
            public _RegistryBag<TResource> Bag;
        }

        /// <summary>
        /// Creates a new initially-empty <see cref="Registry{TKey, TResource}"/>.
        /// </summary>
        public static Registry<TKey, TResource> CreateEmpty()
        {
            return new Registry<TKey, TResource>();
        }

        /// <summary>
        /// Adds a resource to the registry.
        /// </summary>
        public void Add(TKey key, TResource resource)
        {
            _RegistryBag<TResource> bag = _getBagLocked(key);
            Volatile.Write(ref bag.Next, new _RegistryBagNode<TResource>(resource, bag.Next));
            Monitor.Exit(bag);
        }

        /// <summary>
        /// Creates an <see cref="IIterator{T}"/> to iterate over the resources with the given key.
        /// </summary>
        public RegistryBagIterator<TResource> Iterate(TKey key)
        {
            if (_tryGetBag(key, out _RegistryBag<TResource> bag))
                return new RegistryBagIterator<TResource>(bag, Volatile.Read(ref bag.Next));
            else
                return RegistryBagIterator<TResource>.CreateEmpty();
        }

        /// <summary>
        /// Creates a <see cref="RegistryBagViewer{TResource}"/> for inspecting the bag of resources associated with
        /// the given key. The viewer can potentially be upgraded to a <see cref="RegistryBagBrowser{TResource}"/>
        /// if needed.
        /// </summary>
        public RegistryBagViewer<TResource> View(TKey key)
        {
            if (_tryGetBag(key, out _RegistryBag<TResource> bag))
                return new RegistryBagViewer<TResource>(bag, Volatile.Read(ref bag.Next));
            else
                return RegistryBagViewer<TResource>.CreateEmpty();
        }

        /// <summary>
        /// Creates a <see cref="RegistryBagBrowser{TResource}"/> for inspecting and adding to the bag
        /// of resources associated with the given key. All operations are done as an atomic transaction.
        /// The browser must be disposed whem finished.
        /// </summary>
        public RegistryBagBrowser<TResource> Browse(TKey key)
        {
            return new RegistryBagBrowser<TResource>(_getBagLocked(key));
        }

        /// <summary>
        /// Gets the bag associated with the given key, returning it locked.
        /// </summary>
        private _RegistryBag<TResource> _getBagLocked(TKey key)
        {
        retry:
            _RegistryBag<TResource> bag = _getBag(key);
            Monitor.Enter(bag);
            if (!bag.IsAlive)
            {
                Monitor.Exit(bag);
                goto retry;
            }
            return bag;
        }

        /// <summary>
        /// Gets the bag associated with the given key, or returns <see cref="false"/> if no entry exists for the key.
        /// </summary>
        private bool _tryGetBag(TKey key, out _RegistryBag<TResource> bag)
        {
            uint keyIndex;
            _Table table = Volatile.Read(ref _table);
            int hash = EqualityHelper.GetHashCode(key);
            uint bin = table.Hashtable.GetBinPow2(hash);
            while ((keyIndex = Volatile.Read(ref table.Hashtable[bin])) != uint.MaxValue)
            {
                if (EqualityHelper.Equals(key, table.Entries[keyIndex].Key))
                {
                    bag = table.Entries[keyIndex].Bag;
                    return true;
                }
                bin = table.Hashtable.Next(bin);
            }
            bag = default;
            return false;
        }

        /// <summary>
        /// Gets the bag associated with the given key. This will allocate the bag if it doesn't exist.
        /// </summary>
        private _RegistryBag<TResource> _getBag(TKey key)
        {
        retry:
            // Check for existing key entry
            uint keyIndex;
            _Table table = Volatile.Read(ref _table);
            int hash = EqualityHelper.GetHashCode(key);
            uint bin = table.Hashtable.GetBinPow2(hash);
            while ((keyIndex = Volatile.Read(ref table.Hashtable[bin])) != uint.MaxValue)
            {
                if (EqualityHelper.Equals(key, table.Entries[keyIndex].Key))
                    return table.Entries[keyIndex].Bag;
                bin = table.Hashtable.Next(bin);
            }

            // Add new entry
            lock (this)
            {
                // Ensure that there have been no changes since the search above
                if (table == _table)
                {
                    keyIndex = table.Hashtable[bin];
                    if (keyIndex == uint.MaxValue)
                    {
                        // Rebuild table if needed
                        if (!(table.NextFreeEntryIndex < (uint)table.Entries.Length))
                        {
                            _rebuild();
                            table = _table;
                            bin = table.Hashtable.GetBinPow2(hash);
                            while (table.Hashtable[bin] != uint.MaxValue)
                                bin = table.Hashtable.Next(bin);
                        }

                        // Insert the new entry into bin
                        keyIndex = table.NextFreeEntryIndex++;
                        _RegistryBag<TResource> bag = new _RegistryBag<TResource>();
                        bag.IsAlive = true;
                        ref _KeyEntry entry = ref table.Entries[keyIndex];
                        entry.Key = key;
                        entry.Bag = bag;
                        Volatile.Write(ref table.Hashtable[bin], keyIndex);
                        return bag;
                    }
                }

                // If there were changes while we were grabbing the lock, try again
                goto retry;
            }
        }

        /// <summary>
        /// Rebuilds the table for this <see cref="Registry{TKey, TResource}"/> to allow for insertion of at
        /// least one additional entry. This assumes the registry is locked.
        /// </summary>
        private void _rebuild()
        {
            Debug.Assert(Monitor.IsEntered(this));
            _Table table = _table;

            // Remove empty entries and count remaining non-empty entries
            uint numNonEmpty = 0;
            for (uint i = 0; i < table.NextFreeEntryIndex; i++)
            {
                _RegistryBag<TResource> bag = table.Entries[i].Bag;
                Monitor.Enter(bag);
                _RegistryBagNode<TResource> cur = bag.Next;
                while (cur != null)
                {
                    if (cur.Resource.IsAlive)
                    {
                        numNonEmpty++;
                        goto nextBag;
                    }
                    else
                    {
                        // Remove dead resource from bag
                        cur = cur.Next;
                        bag.Next = cur;
                    }
                }
                    
                // The bag contains no live resources, so remove it from the registry
                bag.IsAlive = false;

            nextBag:;
                Monitor.Exit(bag);
            }

            // Determine appropriate size for new hashtable and entry table
            uint nHashtableSize = Bitwise.UpperPow2((numNonEmpty * 10 / 3) + 8);
            uint nEntriesSize = (nHashtableSize * 3 / 5) + 3;

            // Rebuild table
            uint nextFreeEntryIndex = 0;
            uint[] nHashtable = new uint[nHashtableSize];
            for (uint i = 0; i < (uint)nHashtable.Length; i++)
                nHashtable[i] = uint.MaxValue;
            _KeyEntry[] nEntries = new _KeyEntry[nEntriesSize];
            for (uint i = 0; i < table.NextFreeEntryIndex; i++)
            {
                _KeyEntry entry = table.Entries[i];
                if (entry.Bag.IsAlive)
                {
                    uint entryIndex = nextFreeEntryIndex++;
                    nEntries[entryIndex] = entry;
                    int hash = EqualityHelper.GetHashCode(entry.Key);
                    uint bin = nHashtable.GetBinPow2(hash);
                    while (nHashtable[bin] != uint.MaxValue)
                        bin = nHashtable.Next(bin);
                    nHashtable[bin] = entryIndex;
                }
            }

            // Update table
            Volatile.Write(ref _table, new _Table(nHashtable, nEntries, nextFreeEntryIndex));
        }
    }

    /// <summary>
    /// A mutable <see cref="Bag{T}"/> of resources within a <see cref="Registry{TKey, TResource}"/>.
    /// </summary>
    internal sealed class _RegistryBag<TResource> : _RegistryBagParent<TResource>
    {
        /// <summary>
        /// Indicates whether this bag is still usable. This may only be set or checked while the bag is
        /// locked.
        /// </summary>
        public bool IsAlive;
    }

    /// <summary>
    /// A node within a singly-linked list defining a <see cref="_RegistryBag{TResource}"/>.
    /// </summary>
    internal sealed class _RegistryBagNode<TResource> : _RegistryBagParent<TResource>
    {
        public _RegistryBagNode(TResource resource, _RegistryBagNode<TResource> next)
        {
            Resource = resource;
            Next = next;
        }

        /// <summary>
        /// The resource for this node.
        /// </summary>
        public TResource Resource;
    }

    /// <summary>
    /// An object which points to a <see cref="_RegistryBagNode{TResource}"/>.
    /// </summary>
    internal class _RegistryBagParent<TResource>
    {
        /// <summary>
        /// The node following this <see cref="_RegistryBagParent{TResource}"/>.
        /// </summary>
        public _RegistryBagNode<TResource> Next;
    }

    /// <summary>
    /// An <see cref="IIterator{T}"/> for the <see cref="Bag{T}"/> of resources in a
    /// <see cref="Registry{TKey, TResource}"/> associated with a particular key.
    /// </summary>
    public struct RegistryBagIterator<TResource> : IIterator<TResource>
        where TResource : ITransient
    {
        private _RegistryBagParent<TResource> _prev;
        private _RegistryBagParent<TResource> _cur;
        private _RegistryBagNode<TResource> _next;
        internal RegistryBagIterator(_RegistryBag<TResource> bag, _RegistryBagNode<TResource> first)
        {
            _prev = null;
            _cur = bag;
            _next = first;
        }

        /// <summary>
        /// Creates a <see cref="RegistryBagIterator{TResource}"/> which iterates over an empty bag.
        /// </summary>
        public static RegistryBagIterator<TResource> CreateEmpty()
        {
            return new RegistryBagIterator<TResource>(null, null);
        }

        /// <summary>
        /// Attempts to get the next item from the iterator, returning false if the end of the bag
        /// has been reached.
        /// </summary>
        public bool TryNext(out TResource resource)
        {
            if (!(_next is null))
            {
                resource = _next.Resource;
                _prev = _cur;
                _cur = _next;
                _next = _next.Next;
                return true;
            }
            resource = default;
            return false;
        }

        /// <summary>
        /// Indicates that the resource returned by the previous call to <see cref="TryNext(out TResource)"/>
        /// is "dead", and can safely be removed.
        /// </summary>
        public void Clean()
        {
            _RegistryBagNode<TResource> curNode = (_RegistryBagNode<TResource>)_cur;
            Debug.Assert(!curNode.Resource.IsAlive);
            if (Interlocked.CompareExchange(ref _prev.Next, _next, curNode) == curNode)
                _cur = _prev;
        }

        void IDisposable.Dispose()
        {
            // Nothing to do here
        }
    }

    /// <summary>
    /// A helper class for inspecting the <see cref="Bag{T}"/> of resources in a
    /// <see cref="Registry{TKey, TResource}"/> associated with a particular key.
    /// </summary>
    public struct RegistryBagViewer<TResource> : IBagViewer<TResource>
        where TResource : ITransient
    {
        private _RegistryBag<TResource> _bag;
        private _RegistryBagNode<TResource> _first;
        internal RegistryBagViewer(_RegistryBag<TResource> bag, _RegistryBagNode<TResource> first)
        {
            _bag = bag;
            _first = first;
        }

        /// <summary>
        /// Creates a <see cref="RegistryBagViewer{TResource}"/> which views an empty bag. <see cref="TryBrowse"/>
        /// will always fail for this viewer.
        /// </summary>
        public static RegistryBagViewer<TResource> CreateEmpty()
        {
            return new RegistryBagViewer<TResource>(null, null);
        }

        /// <summary>
        /// Indicates whether this is a viewer for the empty bag.
        /// </summary>
        public bool IsEmpty => _first is null;

        /// <summary>
        /// Creates an <see cref="IIterator{T}"/> to iterate over the items in this bag. No other
        /// methods may be called until the iterator is disposed.
        /// </summary>
        public RegistryBagIterator<TResource> Iterate()
        {
            return new RegistryBagIterator<TResource>(_bag, _first);
        }

        /// <summary>
        /// Attempts to create a <see cref="RegistryBagBrowser{TResource}"/> to add new resources to the bag
        /// referenced by this <see cref="RegistryBagViewer{TResource}"/>. This will return <see cref="false"/>
        /// if not possible (e.g. the bag has already been modified on another thread). If a browser is returned,
        /// it must be disposed when finished.
        /// </summary>
        public bool TryBrowse(out RegistryBagBrowser<TResource> browser)
        {
            if (!(_bag is null))
            {
                Monitor.Enter(_bag);
                if (_bag.IsAlive && _bag.Next == _first)
                {
                    browser = new RegistryBagBrowser<TResource>(_bag);
                    return true;
                }
                Monitor.Exit(_bag);
            }
            browser = default;
            return false;
        }

        IIterator<TResource> IBagViewer<TResource>.Iterate()
        {
            return Iterate();
        }
    }
    
    /// <summary>
    /// A helper class for inspecting and adding to the <see cref="Bag{T}"/> of resources in
    /// a <see cref="Registry{TKey, TResource}"/> associated with a particular key.
    /// </summary>
    public struct RegistryBagBrowser<TResource> : IBagBrowser<TResource>, IDisposable
        where TResource : ITransient
    {
        private _RegistryBag<TResource> _bag;
        internal RegistryBagBrowser(_RegistryBag<TResource> bag)
        {
            Debug.Assert(Monitor.IsEntered(bag));
            _bag = bag;
        }

        /// <summary>
        /// Adds an item to this bag.
        /// </summary>
        public void Add(TResource resource)
        {
            Volatile.Write(ref _bag.Next, new _RegistryBagNode<TResource>(resource, _bag.Next));
        }
        
        /// <summary>
        /// Creates an <see cref="IIterator{T}"/> to iterate over the items in this bag. No other
        /// methods may be called until the iterator is disposed.
        /// </summary>
        public RegistryBagIterator<TResource> Iterate()
        {
            return new RegistryBagIterator<TResource>(_bag, _bag.Next);
        }

        /// <summary>
        /// Indicates that this interface will no longer be used, releasing the lock on the associated
        /// <see cref="Registry{TKey, TResource}"/>.
        /// </summary>
        public void Dispose()
        {
            Monitor.Exit(_bag);
        }
        
        IIterator<TResource> IBagViewer<TResource>.Iterate()
        {
            return Iterate();
        }
    }

    /// <summary>
    /// Identifies a resource with a limited lifespan, not controlled by the garbage collector.
    /// </summary>
    public interface ITransient
    {
        /// <summary>
        /// Indicates whether this resource is still useable. Once this becomes <see cref="false"/>, it
        /// will remain so forever.
        /// </summary>
        bool IsAlive { get; }
    }
}
