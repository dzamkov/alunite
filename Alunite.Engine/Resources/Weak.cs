﻿using System;

namespace Alunite.Resources
{
    /// <summary>
    /// A weak reference to an object of type <typeparamref name="T"/>.
    /// </summary>
    /// <remarks>This is a wrapper over <see cref="WeakReference{T}"/> which prevents switching targets. This allows
    /// instances of <see cref="WeakReference{T}"/> for the same target to be reused.</remarks>
    public struct Weak<T> : ITransient
        where T : class
    {
        private WeakReference<T> _source;
        private Weak(WeakReference<T> source)
        {
            _source = source;
        }

        /// <summary>
        /// A weak reference which never has a valid target.
        /// </summary>
        public static Weak<T> Null { get; } = new Weak<T>(new WeakReference<T>(null));

        /// <summary>
        /// Gets a weak reference for the given object.
        /// </summary>
        public static Weak<T> Build(T target)
        {
            if (target is null)
                return Null;
            return new Weak<T>(new WeakReference<T>(target));
        }

        /// <summary>
        /// The target for this weak reference, or <see cref="null"/> if it has been collected.
        /// </summary>
        public T Target
        {
            get
            {
                _source.TryGetTarget(out T target);
                return target;
            }
        }

        /// <summary>
        /// Indicates whether this weak reference still has a valid target. Once this becomes <see cref="false"/>, it
        /// will remain so forever.
        /// </summary>
        public bool IsAlive => _source.TryGetTarget(out _);
    }
}
