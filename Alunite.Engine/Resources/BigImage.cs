﻿using System;
using System.Diagnostics;

using Alunite.Data;
using Alunite.Data.Serialization;
using Alunite.Graphics;

namespace Alunite.Resources
{
    /// <summary>
    /// Identifies and describes an <see cref="Image2{T}"/> which need not be explicitly represented in memory.
    /// </summary>
    public abstract class BigImage2<T> : _IAnyBigImage2
    {
        public BigImage2(Index2 size, Encoding<T> encoding)
        {
            Size = size;
            Encoding = encoding;
        }
        
        /// <summary>
        /// The dimensions of this image. Both components must be non-zero.
        /// </summary>
        public Index2 Size { get; }

        /// <summary>
        /// The native pixel encoding for this image. This is the encoding used when reading image data using
        /// <see cref="Read(out IImageRowReader)"/>.
        /// </summary>
        public Encoding<T> Encoding { get; }

        /// <summary>
        /// Creates a <see cref="IImageRowReader"/> to read the binary data for this image, using <see cref="Encoding"/>
        /// to encode the pixel data.
        /// </summary>
        public abstract IDisposable Read(out IImageRowReader reader);

        Type _IAnyBigImage2.PixelType => typeof(T);
        
        public static implicit operator BigImage2<T>(Image2<T> source)
        {
            return new MemoryImage2<T>(source);
        }

        public static explicit operator BigImage2<T>(AnyBigImage2 source)
        {
            if (!(source._source is BigImage2<T>))
                throw new InvalidCastException("Pixel type mismatch");
            return (BigImage2<T>)source._source;
        }

        public static implicit operator Image2<T>(BigImage2<T> source)
        {
            // TODO: Force loading of the image
            throw new NotImplementedException();
        }

        public static implicit operator AnyBigImage2(BigImage2<T> source)
        {
            return new AnyBigImage2(source);
        }
    }

    /// <summary>
    /// A <see cref="BigImage2{T}"/> of some pixel type.
    /// </summary>
    public struct AnyBigImage2
    {
        internal _IAnyBigImage2 _source;
        internal AnyBigImage2(_IAnyBigImage2 source)
        {
            _source = source;
        }

        /// <summary>
        /// The type of pixels in this image.
        /// </summary>
        public Type PixelType => _source.PixelType;

        /// <summary>
        /// The dimensions of this image. Both components must be non-zero.
        /// </summary>
        public Index2 Size => _source.Size;

        /// <summary>
        /// Tries interpreting this image as an image of <see cref="Scalar"/>s. This will convert color
        /// images to grayscale, if needed.
        /// </summary>
        public BigImage2<Scalar> AsScalar
        {
            get
            {
                if (_source is BigImage2<Scalar> scalarSource)
                    return scalarSource;
                if (_source is BigImage2<Color> colorSource)
                    return colorSource.Channel(ColorChannel.R);
                if (_source is BigImage2<Paint> paintSource)
                    return paintSource.Channel(PaintChannel.R);
                throw new InvalidCastException("Could not convert pixel type");
            }
        }

        /// <summary>
        /// Tries interpreting this image as an image of <see cref="Color"/>s. This will remove opacity information
        /// from <see cref="Paint"/> images and apply tonemapping to <see cref="Radiance"/> images.
        /// </summary>
        public BigImage2<Color> AsColor
        {
            get
            {
                if (_source is BigImage2<Color> colorSource)
                    return colorSource;
                if (_source is BigImage2<Paint> paintSource)
                    return paintSource.StripAlpha();
                throw new InvalidCastException("Could not convert pixel type");
            }
        }

        /// <summary>
        /// Tries interpreting this image as an image of <see cref="Paint"/>s.
        /// </summary>
        public BigImage2<Paint> AsPaint
        {
            get
            {
                if (_source is BigImage2<Paint> paintSource)
                    return paintSource;
                throw new InvalidCastException("Could not convert pixel type");
            }
        }

        /// <summary>
        /// Tries interpreting this image as an image of <see cref="Radiance"/>s.
        /// </summary>
        public BigImage2<Radiance> AsRadiance
        {
            get
            {
                if (_source is BigImage2<Radiance> radianceSource)
                    return radianceSource;
                throw new InvalidCastException("Could not convert pixel type");
            }
        }
    }

    /// <summary>
    /// The interface version of <see cref="AnyBigImage2"/>.
    /// </summary>
    internal interface _IAnyBigImage2
    {
        /// <summary>
        /// The type of pixels in this image.
        /// </summary>
        Type PixelType { get; }

        /// <summary>
        /// The dimensions of this image. Both components must be non-zero.
        /// </summary>
        Index2 Size { get; }
    }

    /// <summary>
    /// Contains helper functions related to <see cref="BigImage2{T}"/>.
    /// </summary>
    public static class BigImage2
    {
        /// <summary>
        /// Loads the image asset at the given path.
        /// </summary>
        public static AnyBigImage2 FromAsset(Path assetPath)
        {
            return _fromFile(Asset.View() / assetPath, null);
        }

        /// <summary>
        /// Loads the image asset at the given path.
        /// </summary>
        /// <param name="isSrgb">If true, indicates that colors values should be interpreted as sRGB, rather than
        /// linearly.</param>
        public static AnyBigImage2 FromAsset(Path assetPath, bool isSrgb)
        {
            return _fromFile(Asset.View() / assetPath, isSrgb);
        }

        /// <summary>
        /// Loads an image from the file at the given path.
        /// </summary>
        public static AnyBigImage2 FromFile(RepositoryPathViewer path)
        {
            return _fromFile(path, null);
        }

        /// <summary>
        /// Loads an image from the file at the given path.
        /// </summary>
        /// <param name="isSrgb">If true, indicates that colors values should be interpreted as sRGB, rather than
        /// linearly.</param>
        public static AnyBigImage2 FromFile(RepositoryPathViewer path, bool isSrgb)
        {
            return _fromFile(path, isSrgb);
        }
        
        /// <summary>
        /// Loads an image from the file at the given path.
        /// </summary>
        /// <param name="isSrgb">If true, indicates that colors values should be interpreted as sRGB, rather than
        /// linearly. If <see cref="null"/>, uses the default for the file extension.</param>
        private static AnyBigImage2 _fromFile(RepositoryPathViewer path, bool? isSrgb)
        {
            if (!path.TryResolveAsFile(out BigBinary binary))
                throw new Exception("File does not exist");
            string ext = path.Extension;
            switch (ext)
            {
                case "png":
                    return Png.Load(binary, isSrgb ?? true);
                case "hdr":
                    return Hdr.Load(binary);
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Isolates a particular channel from this color image.
        /// </summary>
        public static BigImage2<Scalar> Channel(this BigImage2<Color> source, ColorChannel channel)
        {
            // TODO: Cache as product
            if (source.Encoding == Color.Encoding.B8_G8_R8)
                return BitwiseImage.Channel_B8_G8_R8(source, channel);
            throw new NotImplementedException();
        }

        /// <summary>
        /// Isolates a particular channel from this <see cref="Paint"/> image. Note that color values will be
        /// post-multiplied by alpha. Ues <see cref="StripAlpha"/> if this is not appropriate.
        /// </summary>
        public static BigImage2<Scalar> Channel(this BigImage2<Paint> source, PaintChannel channel)
        {
            // TODO: Account for post-multiplication of alpha
            if (channel != PaintChannel.A)
                return source.StripAlpha().Channel((ColorChannel)channel);
            throw new NotImplementedException();
        }

        /// <summary>
        /// Removes the alpha component from this source image.
        /// </summary>
        public static BigImage2<Color> StripAlpha(this BigImage2<Paint> source)
        {
            // TODO: Cache as product
            if (Paint.Encoding.IsOpaque(source.Encoding, out Encoding<Color> sourceEncoding))
                return source.Reinterpret(sourceEncoding);
            else if (source.Encoding == Paint.Encoding.B8_G8_R8_A8)
                return BitwiseImage.StripAlpha_B8_G8_R8_A8(source);
            throw new NotImplementedException();
        }

        /// <summary>
        /// Interprets this image as a normal map.
        /// </summary>
        /// <param name="flipY">If true, indicates that the green channel of <paramref name="flipY"/> corresponds
        /// to -Y, rather than +Y.</param>
        public static BigImage2<Vector3> ToNormal(this BigImage2<Color> source, bool flipY)
        {
            // TODO: Cache as product
            if (source.Encoding == Color.Encoding.B8_G8_R8)
                return BitwiseImage.ToNormal_B8_G8_R8(source, flipY);
            throw new NotImplementedException();
        }

        /// <summary>
        /// Reinterprets the pixel data in this image using a different encoding.
        /// </summary>
        public static BigImage2<T> Reinterpret<TSource, T>(this BigImage2<TSource> source, Encoding<T> encoding)
        {
            // TODO: Cache as product
            return new ReinterpretImage2<TSource, T>(source, encoding);
        }
    }
    
    /// <summary>
    /// Identifies one of the channels of a <see cref="Color"/> image.
    /// </summary>
    public enum ColorChannel : byte { R, G, B }

    /// <summary>
    /// Identifies one of the channels of a <see cref="Paint"/> image.
    /// </summary>
    public enum PaintChannel : byte { R, G, B, A }

    /// <summary>
    /// A <see cref="BigImage2{T}"/> which is stored in memory.
    /// </summary>
    public sealed class MemoryImage2<T> : BigImage2<T>
    {
        internal MemoryImage2(Image2<T> source)
            : base(source.Size, Data.Serialization.Encoding.Default<T>(EncodingStrategy.Fast))
        {
            Source = source;
        }

        /// <summary>
        /// The <see cref="Image2{T}"/> source for this <see cref="MemoryImage2{T}"/>.
        /// </summary>
        public Image2<T> Source { get; }

        public override IDisposable Read(out IImageRowReader reader)
        {
            if (Encoding.Size.Multiplicity == 0)
            {
                uint stride = Encoding.Size.Min * Size.X;
                reader = new BinaryRowReader(Encoding.Encode(new List<T>(Source._data)), stride);
                return null;
            }
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// An <see cref="Image2{T}"/> constructed by using bitwise operations to combine and process the data from one
    /// or more source <see cref="Image2{T}"/>s of the same dimensions.
    /// </summary>
    public sealed class BitwiseImage2<T> : BigImage2<T>
    {
        internal BitwiseImage2(
            Index2 size, Encoding<T> encoding,
            List<uint> chunkInitial, Bag<BitwiseImage.Source> sources)
            : base(size, encoding)
        {
            ChunkInitial = chunkInitial;
            Sources = sources;
        }

        /// <summary>
        /// The number of chunks for each row of this <see cref="BitwiseImage2{T}"/>. A chunk is a fixed-length
        /// span of pixels which are processed as a group.
        /// </summary>
        public uint NumChunks
        {
            get
            {
                uint rowSize = Size.X * Encoding.Size.Max;
                uint chunkSize = ChunkInitial.Length * sizeof(uint);
                return (rowSize + chunkSize - 1) / chunkSize;
            }
        }

        /// <summary>
        /// The initial binary data for a chunk of pixels in the resulting image. This is repeated to initialize the
        /// row buffers for the <see cref="IImageRowReader"/>. Data from <see cref="Sources"/> is then XORed onto
        /// this buffer.
        /// </summary>
        public List<uint> ChunkInitial { get; }

        /// <summary>
        /// The source images for this <see cref="BitwiseImage2{T}"/>, along with the procedures used to transfer their
        /// data to the resulting image.
        /// </summary>
        public Bag<BitwiseImage.Source> Sources { get; }

        public override IDisposable Read(out IImageRowReader reader)
        {
            uint index = 0;
            IDisposable disposable = null;
            var sourcesItems = new (IImageRowReader, BitwiseImage.Procedure)[Sources.Size];
            foreach (BitwiseImage.Source source in Sources)
            {
                disposable = DisposableHelper.Group(disposable, source.Read(out IImageRowReader sourceReader));
                sourcesItems[index++] = (sourceReader, source.Procedure);
            }
            var sources = new Bag<(IImageRowReader, BitwiseImage.Procedure)>(sourcesItems);
            reader = new BitwiseRowReader(NumChunks, ChunkInitial, sources);
            return disposable;
        }
    }
    
    /// <summary>
    /// Contains functions related to <see cref="BitwiseImage2{T}"/>.
    /// </summary>
    public static class BitwiseImage
    {
        /// <summary>
        /// Constructs a <see cref="BitwiseImage2{T}"/> of the given size and encoding which applies a given
        /// operation to a set of source images.
        /// </summary>
        public static BitwiseImage2<T> Build<T>(
            Index2 size, Encoding<T> encoding,
            List<uint> chunkInitial, params Source[] sources)
        {
            return Build(size, encoding, chunkInitial, Bag.Of(sources));
        }

        /// <summary>
        /// Constructs a <see cref="BitwiseImage2{T}"/> of the given size and encoding which applies a given
        /// operation to a set of source images.
        /// </summary>
        public static BitwiseImage2<T> Build<T>(
            Index2 size, Encoding<T> encoding,
            List<uint> chunkInitial, Bag<Source> sources)
        {
            // TODO: Normalization
            return new BitwiseImage2<T>(size, encoding, chunkInitial, sources);
        }

        /// <summary>
        /// Constructs a <see cref="BitwiseImage2{T}"/> which extracts a particular color channel from a source image
        /// interpreted using the <see cref="Color.Encoding.B8_G8_R8"/> encoding.
        /// </summary>
        public static BitwiseImage2<Scalar> Channel_B8_G8_R8(BigImage2<Color> source, ColorChannel channel)
        {
            return Build(source.Size, Scalar.Encoding.U8, List.Of<uint>(0),
                new Source<Color>(source,
                    Color.Encoding.B8_G8_R8,
                    procsChannel_B8_G8_R8[(uint)channel]));
        }

        /// <summary>
        /// The table of procedures used to implement <see cref="Channel(BigImage2{Color}, ColorChannel)"/>.
        /// </summary>
        private static readonly Procedure[] procsChannel_B8_G8_R8 = new Procedure[]
        {
            Procedure.Build(3,
                new Operation(0, 0, 0x00FF0000, 16),
                new Operation(1, 0, 0x0000FF00, 0),
                new Operation(2, 0, 0x000000FF, 16),
                new Operation(2, 0, 0xFF000000, 0)),
            Procedure.Build(3,
                new Operation(0, 0, 0x0000FF00, 24),
                new Operation(1, 0, 0x000000FF, 8),
                new Operation(1, 0, 0xFF000000, 24),
                new Operation(2, 0, 0x00FF0000, 8)),
            Procedure.Build(3,
                new Operation(0, 0, 0x000000FF, 0),
                new Operation(0, 0, 0xFF000000, 16),
                new Operation(1, 0, 0x00FF0000, 0),
                new Operation(2, 0, 0x0000FF00, 16))
        };

        /// <summary>
        /// Constructs a <see cref="BitwiseImage2{T}"/> which removes the alpha channel from a source image
        /// interpreted using the <see cref="Paint.Encoding.B8_G8_R8_A8"/> encoding.
        /// </summary>
        public static BitwiseImage2<Color> StripAlpha_B8_G8_R8_A8(BigImage2<Paint> source)
        {
            return Build(source.Size, Color.Encoding.B8_G8_R8, List.Of<uint>(0, 0, 0),
                new Source<Paint>(source,
                    Paint.Encoding.B8_G8_R8_A8,
                    procStripAlpha_B8_G8_R8_A8));
        }

        /// <summary>
        /// The procedure used the implement <see cref="StripAlpha_B8_G8_R8_A8"/>.
        /// </summary>
        private static readonly Procedure procStripAlpha_B8_G8_R8_A8 =
            Procedure.Build(4,
                new Operation(0, 0, 0x00FFFFFF, 0),
                new Operation(1, 0, 0x000000FF, 24),
                new Operation(1, 1, 0x00FFFF00, 24),
                new Operation(2, 1, 0x0000FFFF, 16),
                new Operation(2, 2, 0x00FF0000, 16),
                new Operation(3, 2, 0x00FFFFFF, 8));

        /// <summary>
        /// Interprests an image as a normal map using the <see cref="Color.Encoding.B8_G8_R8"/> encoding.
        /// </summary>
        /// <param name="flipY">If true, indicates that the green channel of <paramref name="flipY"/> corresponds
        /// to -Y, rather than +Y.</param>
        public static BitwiseImage2<Vector3> ToNormal_B8_G8_R8(BigImage2<Color> source, bool flipY)
        {
            List<uint> chunkInitial = flipY ?
                List.Of<uint>(0x80807F80, 0x7F80807F, 0x807F8080) :
                List.Of<uint>(0x80808080, 0x80808080, 0x80808080);
            return Build(source.Size, Vector3.Encoding.Zs8_Ys8_Xs8, chunkInitial,
                new Source<Color>(source, Color.Encoding.B8_G8_R8, Procedure.Xor(3)));
        }

        /// <summary>
        /// Identifies a possible source image for a <see cref="BitwiseImage2{T}"/> and describes the
        /// <see cref="BitwiseImage.Procedure"/> used to transfer its data to the resulting image.
        /// </summary>
        public abstract class Source
        {
            internal Source(Procedure proc)
            {
                Procedure = proc;
            }

            /// <summary>
            /// The procedure used to transfer this source's data to the resulting image.
            /// </summary>
            public Procedure Procedure { get; }

            /// <summary>
            /// Creates an <see cref="IImageRowReader"/> to read from this source.
            /// </summary>
            public abstract IDisposable Read(out IImageRowReader reader);
        }

        /// <summary>
        /// A <see cref="Source"/> of a particular pixel type.
        /// </summary>
        public sealed class Source<T> : Source
        {
            public Source(BigImage2<T> image, Encoding<T> encoding, Procedure proc)
                : base(proc)
            {
                Image = image;
                Encoding = encoding;
            }

            /// <summary>
            /// The <see cref="BigImage2{T}"/> for this source.
            /// </summary>
            public BigImage2<T> Image { get; }

            /// <summary>
            /// The encoding used to load data from this source.
            /// </summary>
            public Encoding<T> Encoding { get; }

            public override IDisposable Read(out IImageRowReader reader)
            {
                IDisposable disposable = Image.Read(out reader);
                reader = reader.Reencode(Image.Encoding, Encoding);
                return disposable;
            }
        }

        /// <summary>
        /// Describes a procedure for transferring data from an input buffer to a result buffer.
        /// </summary>
        public sealed class Procedure
        {
            private Procedure(uint chunkSize, List<Operation> ops)
            {
                ChunkSize = chunkSize;
                Operations = ops;
            }

            /// <summary>
            /// Constructs a procedure from the given <see cref="ChunkSize"/> and operations.
            /// </summary>
            public static Procedure Build(uint chunkSize, params Operation[] ops)
            {
                return Build(chunkSize, (ReadOnlySpan<Operation>)ops);
            }

            /// <summary>
            /// Constructs a procedure from the given <see cref="ChunkSize"/> and operations.
            /// </summary>
            public static Procedure Build(uint chunkSize, ReadOnlySpan<Operation> ops)
            {
                // TODO: Normalize
                return new Procedure(chunkSize, List.Of(ops));
            }

            /// <summary>
            /// Constructs a procedure which XORs all data from the input to the output, provided the
            /// number of words per chunk for both.
            /// </summary>
            public static Procedure Xor(uint chunkSize)
            {
                Operation[] ops = new Operation[chunkSize];
                for (uint i = 0; i < chunkSize; i++)
                    ops[i] = new Operation(i, i, 0xFFFFFFFF, 0);
                return new Procedure(chunkSize, new List<Operation>(ops));
            }

            /// <summary>
            /// The number of <see cref="uint"/> words per chunk in the procedure input. The operations of
            /// the procedure will be repeated for every chunk.
            /// </summary>
            public uint ChunkSize { get; }

            /// <summary>
            /// The set of operations for this procedure, ordered first by <see cref="Operation.TargetWordIndex"/>,
            /// then by <see cref="Operation.SourceWordIndex"/>, then by <see cref="Operation.BitRotate"/>. This
            /// should be normalized such that there is only one operation for each combination of the three.
            /// </summary>
            public List<Operation> Operations { get; }
        }

        /// <summary>
        /// Describes an operation which transfers a <see cref="uint"/> word from a source chunk to a target chunk,
        /// merging using XOR. All operations are commutative with each other.
        /// </summary>
        public struct Operation
        {
            public Operation(uint sourceWordIndex, uint targetWordIndex, uint mask, byte bitRotate)
            {
                SourceWordIndex = sourceWordIndex;
                TargetWordIndex = targetWordIndex;
                Mask = mask;
                BitRotate = bitRotate;
            }

            /// <summary>
            /// The index of the word in the source chunk from which this operation gets its data.
            /// </summary>
            public uint SourceWordIndex { get; }

            /// <summary>
            /// The index of the word in the target chunk onto which the data for this <see cref="Operation"/>
            /// is transferred.
            /// </summary>
            public uint TargetWordIndex { get; }

            /// <summary>
            /// A little endian bit mask specifying which bits of the source word are considered by
            /// this operation.
            /// </summary>
            public uint Mask { get; }

            /// <summary>
            /// The number of bits to left-rotate the source word during the transfer.
            /// </summary>
            public byte BitRotate { get; }
        }
    }

    /// <summary>
    /// An <see cref="Image2{T}"/> constructed by reinterpreting the binary data of a source image using
    /// a different encoding.
    /// </summary>
    public sealed class ReinterpretImage2<TSource, T> : BigImage2<T>
    {
        internal ReinterpretImage2(BigImage2<TSource> source, Encoding<T> encoding)
            : base(source.Size, encoding)
        {
            Source = source;
        }

        /// <summary>
        /// The source image for this reinterpreted image.
        /// </summary>
        public BigImage2<TSource> Source { get; }

        public override IDisposable Read(out IImageRowReader reader)
        {
            return Source.Read(out reader);
        }
    }
}