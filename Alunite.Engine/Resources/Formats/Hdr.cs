﻿using System;

using Alunite.Data;
using Alunite.Data.Serialization;
using Alunite.Graphics;

namespace Alunite.Resources
{
    /// <summary>
    /// An <see cref="Image2{T}"/> loaded from a HDR file.
    /// </summary>
    public sealed class HdrImage2 : BigImage2<Radiance>
    {
        internal HdrImage2(Index2 size, BigBinary body)
            : base(size, Radiance.Encoding.R8_G8_B8_e8)
        {
            Body = body;
        }

        /// <summary>
        /// The binary data for the body of this <see cref="HdrImage2"/>.
        /// </summary>
        public BigBinary Body { get; }

        public override IDisposable Read(out IImageRowReader reader)
        {
            IDisposable res = Body.Read(0, out ISeekableBinaryReader bodyReader);
            reader = new _RowReader
            {
                Width = Size.X,
                Source = bodyReader
            };
            return res;
        }

        /// <summary>
        /// An <see cref="IImageRowReader"/> for an <see cref="HdrImage2"/>.
        /// </summary>
        private struct _RowReader : IImageRowReader
        {
            public uint Width;
            public IBinaryReader Source;
            uint IImageRowReader.MinBufferSize => Width * 4;
            void IImageRowReader.ReadRow(Span<byte> buffer)
            {
                // Is this row run-length-encoded?
                Source.Prepare(4);
                var sourceBuffer = Source.Buffer;
                if (Width >= 8 && Width < 32768 &&
                    sourceBuffer[0] == 2 &&
                    sourceBuffer[1] == 2 &&
                    sourceBuffer[2] < 0x80)
                {
                    // Verify scan width
                    if (((sourceBuffer[2] << 8) | sourceBuffer[3]) != Width)
                        throw new Exception("Bad HDR data");
                    uint offset = 4;

                    // Decode pixel components separately
                    for (uint i = 0; i < 4; i++)
                    {
                        uint x = 0;
                        while (x < Width)
                        {
                            // Refresh source buffer if needed
                            if (!(offset + 1 < (uint)sourceBuffer.Length))
                            {
                                Source.Skip(offset);
                                Source.Prepare(2);
                                sourceBuffer = Source.Buffer;
                                offset = 0;
                            }

                            // Decode next instruction
                            byte count = sourceBuffer[(int)offset++];
                            if (count > 128)
                            {
                                // Write run of same value
                                count -= 128;
                                byte value = sourceBuffer[(int)offset++];
                                while (count > 0)
                                {
                                    count--;
                                    buffer[(int)(x++ * 4 + i)] = value;
                                }
                            }
                            else
                            {
                                // Copy block of data
                                while (count > 0)
                                {
                                    // Refresh source buffer if needed
                                    if (!(offset < (uint)sourceBuffer.Length))
                                    {
                                        Source.Skip(offset);
                                        Source.Prepare(count);
                                        sourceBuffer = Source.Buffer;
                                        offset = 0;
                                    }

                                    // Copy byte
                                    count--;
                                    buffer[(int)(x++ * 4 + i)] = sourceBuffer[(int)offset++];
                                }
                            }
                        }
                    }

                    // Mark final bytes as read
                    Source.Skip(offset);
                }
                else
                {
                    throw new NotImplementedException();
                }
            }
        }
    }

    /// <summary>
    /// Contains functions related to the HDR image format.
    /// </summary>
    public static class Hdr
    {
        /// <summary>
        /// Loads an image from the given binary using the HDR format.
        /// </summary>
        public static HdrImage2 Load(BigBinary binary)
        {
            using (binary.Read(0, out var reader))
            {
                ulong startPos = reader.Position;
                Index2 size; HdrLayout layout;
                Scalar exposure = 1;
                Scalar gamma = 1;
                HdrFormat format = HdrFormat.R8_G8_B8_E8;
                using (reader.DecodeUtf8(out var decoder))
                using (decoder.Buffered(out var input))
                {
                    // Read header
                    if (input.AcceptString("#?RADIANCE"))
                    {
                    nextLine:
                        input.AcceptWhitespace();
                        var lineStart = input.Mark();
                        if (input.AcceptString("#"))
                        {
                            // Skip line comment
                            while (input.TryReadItem(out char ch) && ch != '\n') ;
                            goto nextLine;
                        }
                        else if (input.Reset(lineStart) && input.AcceptClass(CharSet.Alphabetic, out string var))
                        {
                            if (input.AcceptString("="))
                            {
                                // Read variable value
                                input.AcceptWhitespace();
                                switch (var)
                                {
                                    case "EXPOSURE":
                                        if (input.AcceptScalarLiteral(out exposure))
                                            goto nextLine;
                                        break;
                                    case "GAMMA":
                                        if (input.AcceptScalarLiteral(out gamma))
                                            goto nextLine;
                                        break;
                                    case "PRIMARIES":
                                        if (!input.AcceptScalarLiteral(out _))
                                            break;
                                        input.AcceptWhitespace();
                                        if (!input.AcceptScalarLiteral(out _))
                                            break;
                                        input.AcceptWhitespace();
                                        if (!input.AcceptScalarLiteral(out _))
                                            break;
                                        input.AcceptWhitespace();
                                        if (!input.AcceptScalarLiteral(out _))
                                            break;
                                        input.AcceptWhitespace();
                                        if (!input.AcceptScalarLiteral(out _))
                                            break;
                                        input.AcceptWhitespace();
                                        if (!input.AcceptScalarLiteral(out _))
                                            break;
                                        input.AcceptWhitespace();
                                        if (!input.AcceptScalarLiteral(out _))
                                            break;
                                        input.AcceptWhitespace();
                                        if (!input.AcceptScalarLiteral(out _))
                                            break;
                                        goto nextLine;
                                    case "FORMAT":
                                        var valueStart = input.Mark();
                                        if (input.AcceptString("32-bit_rle_rgbe"))
                                        {
                                            format = HdrFormat.R8_G8_B8_E8;
                                            goto nextLine;
                                        }
                                        else if (input.Reset(valueStart) && input.AcceptString("32-bit_rle_xyze"))
                                        {
                                            format = HdrFormat.X8_Y8_Z8_E8;
                                            goto nextLine;
                                        }
                                        break;
                                }
                            }
                        }
                        else if (input.Reset(lineStart) &&
                            input.TryReadItem(out char firstPrefixCh) &&
                            _plusOrMinus.Contains(firstPrefixCh) &&
                            input.TryReadItem(out char firstCh) &&
                            _xOrY.Contains(firstCh) &&
                            input.AcceptWhitespace() &&
                            input.AcceptIntegerLiteral(out long sizeFirst) &&
                            input.AcceptWhitespace() &&
                            input.TryReadItem(out char secondPrefixCh) &&
                            _plusOrMinus.Contains(secondPrefixCh) &&
                            input.TryReadItem(out char secondCh) &&
                            _xOrY.Contains(firstCh) &&
                            input.AcceptWhitespace() &&
                            input.AcceptIntegerLiteral(out long sizeSecond) &&
                            input.AcceptString("\n"))
                        {
                            // Interpret resolution string
                            if (firstCh == 'Y' && secondCh == 'X')
                            {
                                size.Y = (uint)sizeFirst;
                                size.X = (uint)sizeSecond;
                                if (firstPrefixCh == '-' && secondPrefixCh == '-')
                                    layout = HdrLayout.NegY_NegX;
                                else if (firstPrefixCh == '+' && secondPrefixCh == '-')
                                    layout = HdrLayout.PosY_NegX;
                                else if (firstPrefixCh == '-' && secondPrefixCh == '+')
                                    layout = HdrLayout.NegY_PosX;
                                else
                                    layout = HdrLayout.PosY_PosX;
                                goto afterHeader;
                            }
                            else if (firstCh == 'X' && secondCh == 'Y')
                            {
                                size.X = (uint)sizeFirst;
                                size.Y = (uint)sizeSecond;
                                if (firstPrefixCh == '-' && secondPrefixCh == '-')
                                    layout = HdrLayout.NegX_NegY;
                                else if (firstPrefixCh == '+' && secondPrefixCh == '-')
                                    layout = HdrLayout.PosX_NegY;
                                else if (firstPrefixCh == '-' && secondPrefixCh == '+')
                                    layout = HdrLayout.NegX_PosY;
                                else
                                    layout = HdrLayout.PosX_PosY;
                                goto afterHeader;
                            }
                        }
                    }
                    throw new Exception("Invalid HDR file");
                afterHeader:;
                }

                // Verify header
                if (format != HdrFormat.R8_G8_B8_E8)
                    throw new NotSupportedException("Formats other than \"32-bit_rle_rgbe\" are not supported");
                if (layout != HdrLayout.NegY_PosX)
                    throw new NotSupportedException("Layouts other than \"-Y N +X M\" are not supported");

                // Construct image
                // TODO: Handle exposure and gamma
                ulong offset = reader.Position - startPos;
                return new HdrImage2(size, binary.Slice(offset, binary.Size - offset));
            }
        }

        /// <summary>
        /// The <see cref="CharSet"/> consisting of "+" and "-".
        /// </summary>
        private static readonly CharSet _plusOrMinus = CharSet.Of('+', '-');

        /// <summary>
        /// The <see cref="CharSet"/> consisting of "X" and "Y".
        /// </summary>
        private static readonly CharSet _xOrY = CharSet.Of('X', 'Y');
    }

    /// <summary>
    /// Identifies one of the possible pixel formats for an HDR file.
    /// </summary>
    public enum HdrFormat
    {
        R8_G8_B8_E8,
        X8_Y8_Z8_E8
    }

    /// <summary>
    /// Identifies one of the possible pixel layouts for an HDR file.
    /// </summary>
    public enum HdrLayout
    {
        NegY_PosX,
        NegY_NegX,
        PosY_NegX,
        PosY_PosX,
        PosX_PosY,
        NegX_PosY,
        NegX_NegY,
        PosX_NegY
    }
}
