﻿using System;

using Alunite.Data;
using Alunite.Data.Serialization;
using Alunite.Graphics;

namespace Alunite.Resources
{
    /// <summary>
    /// An <see cref="Image2{T}"/> loaded from a PNG file.
    /// </summary>
    public sealed class PngImage2 : BigImage2<Paint>
    {
        internal PngImage2(Index2 size, Encoding<Paint> encoding,
            BigBinary binary, bool isSrgb,
            PngColorType colorType,
            byte bitDepth)
            : base(size, encoding)
        {
            Binary = binary;
            IsSrgb = isSrgb;
            ColorType = colorType;
            BitDepth = bitDepth;
        }

        /// <summary>
        /// The binary for the PNG file.
        /// </summary>
        public BigBinary Binary { get; }

        /// <summary>
        /// If true, indicates that colors values should be interpreted as sRGB, rather than
        /// linearly.
        /// </summary>
        public bool IsSrgb { get; }

        /// <summary>
        /// The type of color data in this <see cref="PngImage2"/>.
        /// </summary>
        public PngColorType ColorType { get; }

        /// <summary>
        /// The number of bits per pixel component in this <see cref="PngImage2"/>.
        /// </summary>
        public byte BitDepth { get; }
        
        public override unsafe IDisposable Read(out IImageRowReader reader)
        {
            // TODO: Allow multiple simultaneous readers
            System.Drawing.Bitmap bitmap;
            using (var stream = Binary.OpenStream())
            {
                bitmap = new System.Drawing.Bitmap(stream);
            }
            int width = bitmap.Width;
            int height = bitmap.Height;
            var format = _format;
            var bd = bitmap.LockBits(
                new System.Drawing.Rectangle(0, 0, width, height),
                System.Drawing.Imaging.ImageLockMode.ReadOnly, format);
            byte* start = (byte*)bd.Scan0;
            var bufferReader = new PointerReader(start, start + height * bd.Stride);
            reader = new BinaryRowReader(bufferReader, (uint)bd.Stride);
            return new _BitmapDataDisposable(bitmap, bd);
        }

        /// <summary>
        /// The format used to load data for this image.
        /// </summary>
        private System.Drawing.Imaging.PixelFormat _format
        {
            get
            {
                if (Encoding == Paint.Encoding.B8_G8_R8 || Encoding == Paint.Encoding.Sb8_Sg8_Sr8)
                    return System.Drawing.Imaging.PixelFormat.Format24bppRgb;
                else if (Encoding == Paint.Encoding.B8_G8_R8_A8 || Encoding == Paint.Encoding.Sb8_Sg8_Sr8_A8)
                    return System.Drawing.Imaging.PixelFormat.Format32bppArgb;
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// An <see cref="IDisposable"/> wrapper over a <see cref="System.Drawing.Bitmap"/> and its locked data.
        /// </summary>
        private struct _BitmapDataDisposable : IDisposable
        {
            public _BitmapDataDisposable(System.Drawing.Bitmap bitmap, System.Drawing.Imaging.BitmapData data)
            {
                Bitmap = bitmap;
                Data = data;
            }

            /// <summary>
            /// The bitmap covered by this <see cref="_BitmapDataDisposable"/>.
            /// </summary>
            public System.Drawing.Bitmap Bitmap { get; }

            /// <summary>
            /// The bitmap data covered by this <see cref="_BitmapDataDisposable"/>.
            /// </summary>
            public System.Drawing.Imaging.BitmapData Data { get; }

            /// <summary>
            /// Releases the resources held by this <see cref="_BitmapDataDisposable"/>.
            /// </summary>
            public void Dispose()
            {
                Bitmap.UnlockBits(Data);
                Bitmap.Dispose();
            }
        }
    }

    /// <summary>
    /// Contains functions related to the PNG image format.
    /// </summary>
    public static class Png
    {
        /// <summary>
        /// Loads an image from the given binary using the PNG format.
        /// </summary>
        /// <param name="isSrgb">If true, indicates that colors values should be interpreted as sRGB, rather than
        /// linearly.</param>
        public static PngImage2 Load(BigBinary binary, bool isSrgb)
        {
            using (binary.Read(0, out ISeekableBinaryReader reader))
            {
                // Verify file header
                if (reader.ReadUInt64() != Signature)
                    throw new Exception("Invalid PNG file");

                // Read metadata from header chunk
                uint headerChunkSize = reader.ReadNetworkUInt32();
                reader.Prepare(headerChunkSize);
                if (reader.ReadUInt32() != (uint)PngChunkType.IHDR)
                    throw new Exception("Invalid PNG file");
                uint width = reader.ReadNetworkUInt32();
                uint height = reader.ReadNetworkUInt32();
                byte bitDepth = reader.ReadByte();
                PngColorType colorType = (PngColorType)reader.ReadByte();
                byte compressionMethod = reader.ReadByte();
                byte filterMethod = reader.ReadByte();
                byte interlaceMethod = reader.ReadByte();
                return new PngImage2(
                    new Index2(width, height),
                    GetEncoding(colorType, bitDepth, isSrgb),
                    binary, isSrgb, colorType, bitDepth);
            }
        }

        /// <summary>
        /// The 8-byte signature which begins a PNG file, in little endian byte order.
        /// </summary>
        public const ulong Signature = 0x0A1A0A0D474E5089;

        /// <summary>
        /// Gets the preferred encoding to load image data of the given <see cref="ColorType"/> and bit depth.
        /// </summary>
        public static Encoding<Paint> GetEncoding(PngColorType colorType, byte bitDepth, bool isSrgb)
        {
            if (!_encodings.TryGet((colorType, bitDepth, isSrgb), out var res))
                throw new NotSupportedException("Unsupported PNG format");
            return res;
        }

        /// <summary>
        /// A table of possible encodings for <see cref="GetEncoding(ColorType, byte, bool)"/>.
        /// </summary>
        private static readonly Map<(PngColorType, byte, bool), Encoding<Paint>> _encodings = Map.Of(
            ((PngColorType.Grayscale, (byte)8, false), Paint.Encoding.B8_G8_R8),
            ((PngColorType.Rgb, (byte)8, false), Paint.Encoding.B8_G8_R8),
            ((PngColorType.Rgb, (byte)8, true), Paint.Encoding.Sb8_Sg8_Sr8),
            ((PngColorType.RgbAlpha, (byte)8, false), Paint.Encoding.B8_G8_R8_A8),
            ((PngColorType.RgbAlpha, (byte)8, true), Paint.Encoding.Sb8_Sg8_Sr8_A8));
    }

    /// <summary>
    /// Identifies a type of PNG chunk. The value of a <see cref="ChunkType"/> is equivalent to the 4-byte marker for
    /// the chunk, in little endian byte order.
    /// </summary>
    public enum PngChunkType : uint
    {
        IHDR = 0x52444849
    }

    /// <summary>
    /// Identifies a possible color format for a PNG image.
    /// </summary>
    public enum PngColorType : byte
    {
        Grayscale = 0,
        Rgb = 2,
        Palette = 3,
        GrayscaleAlpha = 4,
        RgbAlpha = 6
    }
}