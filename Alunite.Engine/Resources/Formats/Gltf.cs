﻿using System;
using System.Diagnostics;
using System.Json;
using System.Globalization;

using Alunite.Data;
using Alunite.Data.Serialization;
using Alunite.Data.Geometry;
using Alunite.Graphics;

namespace Alunite.Resources
{
    /// <summary>
    /// Describes the addressable contents of a glTF asset, abstracing out storage details.
    /// </summary>
    public sealed class GltfAsset
    {
        private GltfAsset(
            GltfScene defaultScene,
            Map<string, GltfScene> scenes,
            Map<string, GltfNode> nodes,
            Map<string, GltfMesh> meshes,
            Map<string, GltfMaterial> mats)
        {
            DefaultScene = defaultScene;
            Scenes = scenes;
            Nodes = nodes;
            Meshes = meshes;
            Materials = mats;
        }

        /// <summary>
        /// The default scene for this asset, or <see cref="null"/> if not defined.
        /// </summary>
        public GltfScene DefaultScene { get; }

        /// <summary>
        /// The named scenes in the asset. If there are duplicate names, this will contain just one representative for
        /// each name.
        /// </summary>
        public Map<string, GltfScene> Scenes { get; }

        /// <summary>
        /// The named nodes in the asset. If there are duplicate names, this will contain just one representative for
        /// each name.
        /// </summary>
        public Map<string, GltfNode> Nodes { get; }

        /// <summary>
        /// The named meshes in the asset. If there are duplicate names, this will contain just one representative for
        /// each name.
        /// </summary>
        public Map<string, GltfMesh> Meshes { get; }

        /// <summary>
        /// The named materials in the asset. If there are duplicate names, this will contain just one representative for
        /// each name.
        /// </summary>
        public Map<string, GltfMaterial> Materials { get; }

        /// <summary>
        /// Loads the glTF asset at the given asset path.
        /// </summary>
        public static GltfAsset FromAsset(Path assetPath)
        {
            return FromFile(Asset.View() / assetPath);
        }

        /// <summary>
        /// Loads a glTF asset from the file at the given path.
        /// </summary>
        public static GltfAsset FromFile(RepositoryPathViewer path)
        {
            var loader = _GltfLoader.Create(path);
            var scenes = MapBuilder<string, GltfScene>.CreateEmpty();
            var nodes = MapBuilder<string, GltfNode>.CreateEmpty();
            var meshes = MapBuilder<string, GltfMesh>.CreateEmpty();
            var mats = MapBuilder<string, GltfMaterial>.CreateEmpty();
            foreach (GltfScene scene in loader.Scenes)
                if (!(scene.Name is null))
                    scenes.Add(scene.Name, scene);
            foreach (GltfNode node in loader.Nodes)
                if (!(node.Name is null))
                    nodes.Add(node.Name, node);
            foreach (GltfMesh mesh in loader.Meshes)
                if (!(mesh.Name is null))
                    meshes.Add(mesh.Name, mesh);
            foreach (GltfMaterial mat in loader.Materials)
                if (!(mat.Name is null))
                    mats.Add(mat.Name, mat);
            return new GltfAsset(
                loader.DefaultScene,
                scenes.Finish(),
                nodes.Finish(),
                meshes.Finish(),
                mats.Finish());
        }
    }

    /// <summary>
    /// Describes a glTF "scene" object, abstracting out storage details.
    /// </summary>
    public sealed class GltfScene
    {
        public GltfScene(string name, Bag<GltfNode> nodes)
        {
            Name = name;
            Nodes = nodes;
        }

        /// <summary>
        /// The name of this node, or <see cref="null"/> if not defined.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// The nodes included in this scene.
        /// </summary>
        public Bag<GltfNode> Nodes { get; }

        /// <summary>
        /// Loads the glTF scene at the given asset path.
        /// </summary>
        public static GltfScene FromAsset(Path assetPath)
        {
            return FromFile(Asset.View() / assetPath);
        }

        /// <summary>
        /// Loads a glTF scene from the file at the given path.
        /// </summary>
        public static GltfScene FromFile(RepositoryPathViewer path)
        {
            return _GltfLoader.Create(path).DefaultScene;
        }

        /// <summary>
        /// Constructs a <see cref="ModelForm"/> consisting of the visual contents of this <see cref="GltfScene"/>.
        /// </summary>
        public ModelForm Import()
        {
            ModelForm res = ModelForm.Empty;
            foreach (GltfNode node in Nodes)
                res += node.ImportParent();
            return res;
        }
    }

    /// <summary>
    /// Describes a glTF "node" object, abstracting out storage details.
    /// </summary>
    public sealed class GltfNode
    {
        // TODO: Skinning
        public GltfNode(string name, Affine3 trans, Bag<GltfNode> children, GltfMesh mesh)
        {
            Name = name;
            Transform = trans;
            Children = children;
            Mesh = mesh;
        }

        /// <summary>
        /// The name of this node, or <see cref="null"/> if not defined.
        /// </summary>
        public string Name { get; }
        
        /// <summary>
        /// The transform from the node's space to its parent space.
        /// </summary>
        public Affine3 Transform { get; }
        
        /// <summary>
        /// The children of this node.
        /// </summary>
        public Bag<GltfNode> Children { get; }

        /// <summary>
        /// The <see cref="GltfMesh"/> at this node, or <see cref="null"/> if not applicable.
        /// </summary>
        public GltfMesh Mesh { get; }

        /// <summary>
        /// Constructs a <see cref="ModelForm"/> consisting of the visual contents of this <see cref="GltfNode"/> in
        /// its parent space.
        /// </summary>
        public ModelForm ImportParent()
        {
            return Import(Transform);
        }

        /// <summary>
        /// Constructs a <see cref="ModelForm"/> consisting of the visual contents of this <see cref="GltfNode"/> in
        /// its local space.
        /// </summary>
        public ModelForm ImportLocal()
        {
            return Import(Affine3.Identity);
        }

        /// <summary>
        /// Constructs a <see cref="ModelForm"/> consisting of the visual contents of this <see cref="GltfNode"/>.
        /// </summary>
        /// <param name="trans">The transform from the node space <em>(not the parent space)</em> to model space.</param>
        public ModelForm Import(Affine3 trans)
        {
            // TODO: Caching?
            ModelForm res = ModelForm.Empty;
            foreach (GltfNode child in Children)
                res += child.Import(trans * child.Transform);
            if (!(Mesh is null))
                res += Mesh.Import(trans);
            return res;
        }
    }

    /// <summary>
    /// Describes a glTF "mesh" object, abstracting out storage details.
    /// </summary>
    public sealed class GltfMesh
    {
        public GltfMesh(string name, List<GltfPrimitive> prims)
        {
            Name = name;
            Primitives = prims;
        }

        /// <summary>
        /// The name of this mesh, or <see cref="null"/> if not defined.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// The primitives that make up this mesh.
        /// </summary>
        public List<GltfPrimitive> Primitives { get; }

        /// <summary>
        /// Constructs a <see cref="ModelForm"/> based on this <see cref="GltfMesh"/>.
        /// </summary>
        /// <param name="trans">The transform from the mesh space to model space.</param>
        public ModelForm Import(Affine3 trans)
        {
            bool shouldOverlay = true; // TODO: Select overlay mode
            if (shouldOverlay)
            {
                return GltfPrimitive.ImportOverlay(Primitives, trans);
            }
            else
            {
                ModelForm res = ModelForm.Empty;
                foreach (GltfPrimitive prim in Primitives)
                    res += prim.Import(trans);
                return res;
            }
        }
    }
    
    /// <summary>
    /// Describes a glTF "primitive" object, abstracting out storage details.
    /// </summary>
    public struct GltfPrimitive
    {
        /// <summary>
        /// The material applied to this primitive.
        /// </summary>
        public GltfMaterial Material;

        /// <summary>
        /// The render mode of this primitive.
        /// </summary>
        public GltfPrimitiveMode Mode;

        /// <summary>
        /// The positions of the vertices for this primitive.
        /// </summary>
        public BigList<Vector3> Positions;

        /// <summary>
        /// The normals of the vertices for this primitive, or <see cref="null"/> if not available.
        /// </summary>
        public BigList<Vector3> Normals;

        /// <summary>
        /// The tangents of the vertices, with the last component defining UV orientation, or
        /// <see cref="null"/> if not available.
        /// </summary>
        public BigList<Vector4> Tangents;

        /// <summary>
        /// The texture coordinates of the vertices, or <see cref="null"/> if not available.
        /// </summary>
        public BigList<Vector2> Coords;

        /// <summary>
        /// The colors for the vertices, or <see cref="null"/> if not applicable.
        /// </summary>
        public BigList<Paint> Colors;

        /// <summary>
        /// The indices for the primitive.
        /// </summary>
        public BigList<uint> Indices;

        /// <summary>
        /// The indices for the "mesh ties" of the primitive.
        /// </summary>
        public List<IndexSet> TieIndices;
        
        /// <summary>
        /// Constructs a <see cref="ModelForm"/> based on this primitive.
        /// </summary>
        /// <param name="trans">A transform applied to all coordinates in the primitive. Since <see cref="Model"/>s can
        /// only be transformed by <see cref="Motion3"/>s, the primitive needs to be properly scaled before conversion to
        /// a model.</param>
        public ModelForm Import(Affine3 trans)
        {
            // TODO: Handle mesh ties
            var verts = new MaterialVertexList(trans, Positions, Normals, Tangents, Coords);
            MaterialUsage mat = Material.Import();
            switch (Mode)
            {
                case GltfPrimitiveMode.Triangles:
                    return ModelForm.Solid(mat, verts, Indices);
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Constructs a <see cref="ModelForm"/> based on a set of primitives, assuming that all overlapping geometry
        /// between the primitives should be interpreted as multiple-material blended geometry.
        /// </summary>
        /// <param name="trans">A transform applied to all coordinates in the primitive. Since <see cref="Model"/>s can
        /// only be transformed by <see cref="Motion3"/>s, the primitive needs to be properly scaled before conversion to
        /// a model.</param>
        public static ModelForm ImportOverlay(SpanList<GltfPrimitive> prims, Affine3 trans)
        {
            // Identify common vertex positions across primitives
            var poss = PermutationBuilder<Vector3>.CreateEmpty();
            uint numVerts = 0;
            for (uint i = 0; i < prims.Length; i++)
                numVerts += prims[i].Positions.Length;
            var posIndices = ListBuilder<uint>.CreateEmpty(numVerts);
            var primStartIndices = ListBuilder<uint>.CreateDefault(prims.Length);
            for (uint i = 0; i < prims.Length; i++)
            {
                primStartIndices[i] = posIndices.Length;
                using (prims[i].Positions.Read(out var posReader))
                    while (posReader.TryReadItem(out Vector3 pos))
                        posIndices.Append(poss.Include(pos));
            }
            
            // Convert vertices to blend factors
            var blendFactors = ListBuilder<VertexTextureFactor>.CreateEmpty(numVerts);
            for (uint i = 0; i < prims.Length; i++)
            {
                GltfPrimitive prim = prims[i];
                uint blendFactorIndex = blendFactors.Length;

                // Read all but colors, assuming alpha is 1
                using (prim.Normals.Read(out var normalReader))
                using (prim.Tangents.Read(out var tangentReader))
                using (prim.Coords.Read(out var coordReader))
                {
                    while (normalReader.TryReadItem(out Vector3 normal))
                    {
                        Vector4 tangent = tangentReader.ReadItem();
                        Vector2 coord = coordReader.ReadItem();
                        blendFactors.Append(VertexTextureFactor.Build(i, coord,
                            TransformTbn(trans, normal, tangent),
                            1, false));
                    }
                }

                // Read alphas from color data
                if (!(prim.Colors is null))
                    using (prim.Colors.Read(out var colorReader))
                        while (colorReader.TryReadItem(out Paint color))
                            blendFactors[blendFactorIndex++].Alpha = color.A;
            }
            
            // Count number of triangles across primitives
            uint numTris = 0;
            for (uint i = 0; i < prims.Length; i++)
            {
                GltfPrimitive prim = prims[i];
                if (prim.Mode == GltfPrimitiveMode.Triangles)
                    numTris += prim.Indices.Length / 3;
                else
                    throw new NotImplementedException();
            }

            // Build lists of overlayed triangles
            var tris = ListBuilder<_OverlayTriangle>.CreateEmpty(numTris);
            var posTriMap = MapBuilder<_IndexTriangle, uint>.CreateEmpty();
            for (uint i = 0; i < prims.Length; i++)
            {
                GltfPrimitive prim = prims[i];
                uint primStartIndex = primStartIndices[i];
                if (prim.Mode == GltfPrimitiveMode.Triangles)
                {
                    using (prim.Indices.Read(out IListReader<uint> indexReader))
                    {
                        while (indexReader.TryReadItem(out uint index_0))
                        {
                            index_0 = primStartIndex + index_0;
                            uint index_1 = primStartIndex + indexReader.ReadItem();
                            uint index_2 = primStartIndex + indexReader.ReadItem();
                            uint posIndex_0 = posIndices[index_0];
                            uint posIndex_1 = posIndices[index_1];
                            uint posIndex_2 = posIndices[index_2];
                            if (posTriMap.TryGet(_tri(posIndex_0, posIndex_1, posIndex_2), out uint belowIndex))
                            {
                                uint triIndex = tris.Append(new _OverlayTriangle(belowIndex, _tri(index_0, index_1, index_2)));
                                tris[belowIndex].HasAbove = true;
                                posTriMap[_tri(posIndex_0, posIndex_1, posIndex_2)] = triIndex;
                            }
                            else if (posTriMap.TryGet(_tri(posIndex_1, posIndex_2, posIndex_0), out belowIndex))
                            {
                                uint triIndex = tris.Append(new _OverlayTriangle(belowIndex, _tri(index_1, index_2, index_0)));
                                tris[belowIndex].HasAbove = true;
                                posTriMap[_tri(posIndex_1, posIndex_2, posIndex_0)] = triIndex;
                            }
                            else if (posTriMap.TryGet(_tri(posIndex_2, posIndex_0, posIndex_1), out belowIndex))
                            {
                                uint triIndex = tris.Append(new _OverlayTriangle(belowIndex, _tri(index_2, index_0, index_1)));
                                tris[belowIndex].HasAbove = true;
                                posTriMap[_tri(posIndex_2, posIndex_0, posIndex_1)] = triIndex;
                            }
                            else
                            {
                                belowIndex = _OverlayTriangle.NoneBelowIndex;
                                uint triIndex = tris.Append(new _OverlayTriangle(belowIndex, _tri(index_0, index_1, index_2)));
                                posTriMap.Add(_tri(posIndex_0, posIndex_1, posIndex_2), triIndex);
                            }
                        }
                    }
                }
                else
                {
                    throw new NotImplementedException();
                }
            }

            // Build mesh
            var mesh = MeshBuilder.CreateEmpty();
            var sourceIndices_0 = ListBuilder<uint>.CreateEmpty(4);
            var sourceIndices_1 = ListBuilder<uint>.CreateEmpty(4);
            var sourceIndices_2 = ListBuilder<uint>.CreateEmpty(4);
            var vertBlendFactors = ListBuilder<VertexTextureFactor>.CreateEmpty(4);
            var fVertIndices = MapBuilder<List<uint>, MeshBuilder.Index>.CreateEmpty(); // TODO: Specialized structure to avoid list allocation
            for (uint i = 0; i < tris.Length; i++)
            {
                ref _OverlayTriangle tri = ref tris[i];
                if (!tri.HasAbove)
                {
                    // Determine indices involved for each triangle endpoint. Note that indices will be added backwards,
                    // from top to bottom. This is desirable because it matches the priority of blend factors within
                    // a multiple-material blended vertex.
                    uint curTriIndex = i;
                    _OverlayTriangle curTri = tri;
                next:
                    sourceIndices_0.Append(curTri.Indices.Index_0);
                    sourceIndices_1.Append(curTri.Indices.Index_1);
                    sourceIndices_2.Append(curTri.Indices.Index_2);
                    curTriIndex = curTri.BelowIndex;
                    if (curTriIndex < tris.Length)
                    {
                        curTri = tris[curTriIndex];
                        goto next;
                    }

                    // Add or reuse vertices with blend factors
                    if (fVertIndices.Allocate(sourceIndices_0, out var fVertEntry_0))
                        fVertEntry_0.Value = addVert(sourceIndices_0);
                    if (fVertIndices.Allocate(sourceIndices_1, out var fVertEntry_1))
                        fVertEntry_1.Value = addVert(sourceIndices_1);
                    if (fVertIndices.Allocate(sourceIndices_2, out var fVertEntry_2))
                        fVertEntry_2.Value = addVert(sourceIndices_2);

                    // Add triangle
                    mesh.AddTriangle(fVertEntry_0.Value, fVertEntry_1.Value, fVertEntry_2.Value);

                    // Clear lists for next triangle
                    sourceIndices_0.Clear();
                    sourceIndices_1.Clear();
                    sourceIndices_2.Clear();
                }
            }

            // Adds a blended vertex to the resulting mesh
            MeshBuilder.Index addVert(SpanList<uint> sourceIndices)
            {
                // Convert alphas to account for additive blend function used for rendering
                Scalar weight = 0;
                foreach (uint sourceIndex in sourceIndices)
                {
                    VertexTextureFactor blendFactor = blendFactors[sourceIndex];
                    Scalar alpha = blendFactor.Alpha * (1 - weight);
                    weight += alpha;
                    blendFactor.Alpha = alpha;
                    vertBlendFactors.Append(blendFactor);
                }
                Vector3 pos = trans * poss[posIndices[sourceIndices[0]]];
                MeshBuilder.Index res = mesh.BuildVertex(pos, vertBlendFactors);
                vertBlendFactors.Clear();
                return res;
            }

            // Mark mesh ties
            uint numTies = 0;
            for (uint i = 0; i < prims.Length; i++)
                numTies = Math.Max(numTies, prims[i].TieIndices.Length);
            var tieIndices = IndexSetBuilder.CreateEmpty();
            for (uint i = 0; i < numTies; i++)
            {
                // Identify indices which belong to this tie
                for (uint j = 0; j < prims.Length; j++)
                {
                    GltfPrimitive prim = prims[j];
                    if (i < prim.TieIndices.Length)
                    {
                        uint primStartIndex = primStartIndices[j];
                        tieIndices.UnionAdd(primStartIndex, prim.TieIndices[i]);
                    }
                }

                // Check if there are any indices in this tie
                if (tieIndices.Bound > 0)
                {
                    var meshTieIndices = SetBuilder<MeshBuilder.Index>.CreateEmpty();
                    foreach (var kvp in fVertIndices)
                    {
                        List<uint> sourceIndices = kvp.Key;
                        bool inTie = tieIndices.Contains(sourceIndices[0]);
                        for (uint j = 1; j < sourceIndices.Length; j++)
                            if (inTie != tieIndices.Contains(sourceIndices[j]))
                                throw new Exception("Inconsistent mesh tie membership");
                        if (inTie)
                            meshTieIndices.Include(kvp.Value);
                    }

                    // Add to mesh
                    mesh.AddTie(meshTieIndices.Finish());

                    // Clear indices for next tie
                    tieIndices.Clear();
                }
            }

            // Prepare materials
            var mats = ListBuilder<MaterialUsage>.CreateDefault(prims.Length);
            for (uint i = 0; i < prims.Length; i++)
                mats[i] = prims[i].Material.Import();

            // Finalize model
            return ModelForm.FromMesh(mesh.Finish(), mats.Finish());
        }

        /// <summary>
        /// Constructs an <see cref="_IndexTriangle"/> with the given indices.
        /// </summary>
        private static _IndexTriangle _tri(uint index_0, uint index_1, uint index_2)
        {
            return new _IndexTriangle(index_0, index_1, index_2);
        }

        /// <summary>
        /// The default material for importation purposes.
        /// </summary>
        private static Material _defaultMat => throw new NotImplementedException();
        
        /// <summary>
        /// Describes a triangle belonging to some primitive in the process of importing an overlay model.
        /// </summary>
        private struct _OverlayTriangle
        {
            private uint _belowIndexDef;
            public _OverlayTriangle(uint belowIndex, _IndexTriangle indices)
            {
                _belowIndexDef = belowIndex;
                Indices = indices;
            }

            /// <summary>
            /// Identifies the <see cref="_OverlayTriangle"/> that is overlayed below this triangle, or
            /// <see cref="NoneBelowIndex"/> if not applicable. This establishes a singly-linked list
            /// of primitives influencing the triangle.
            /// </summary>
            public uint BelowIndex
            {
                get
                {
                    return _belowIndexDef & 0x7FFFFFFF;
                }
                set
                {
                    _belowIndexDef &= 0x80000000;
                    _belowIndexDef |= 0x7FFFFFFF & value;
                }
            }

            /// <summary>
            /// Indicates whether there is an <see cref="_OverlayTriangle"/> overlayed above this triangle.
            /// </summary>
            public bool HasAbove
            {
                get
                {
                    return (_belowIndexDef & 0x80000000) != 0;
                }
                set
                {
                    if (value) _belowIndexDef |= 0x80000000;
                    else _belowIndexDef &= ~0x80000000;
                }
            }
            
            /// <summary>
            /// A sentinel value for <see cref="BelowIndex"/> used to indicate the absence of an overlayed triangle.
            /// </summary>
            public const uint NoneBelowIndex = 0x7FFFFFFF;

            /// <summary>
            /// The vertex indices for the triangle.
            /// </summary>
            public _IndexTriangle Indices;
        }

        /// <summary>
        /// A triangle consisting of three indices.
        /// </summary>
        private struct _IndexTriangle : IEquatable<_IndexTriangle>
        {
            public uint Index_0;
            public uint Index_1;
            public uint Index_2;
            public _IndexTriangle(uint index_0, uint index_1, uint index_2)
            {
                Index_0 = index_0;
                Index_1 = index_1;
                Index_2 = index_2;
            }

            public static bool operator ==(_IndexTriangle a, _IndexTriangle b)
            {
                return a.Index_0 == b.Index_0 && a.Index_1 == b.Index_1 && a.Index_2 == b.Index_2;
            }

            public static bool operator !=(_IndexTriangle a, _IndexTriangle b)
            {
                return !(a == b);
            }

            public override bool Equals(object obj)
            {
                if (!(obj is _IndexTriangle))
                    return false;
                return this == (_IndexTriangle)obj;
            }
            
            bool IEquatable<_IndexTriangle>.Equals(_IndexTriangle other)
            {
                return this == other;
            }

            public override int GetHashCode()
            {
                return HashCodeHelper.Combine(
                    Index_0.GetHashCode(),
                    Index_1.GetHashCode(),
                    Index_2.GetHashCode());
            }
        }

        /// <summary>
        /// Computes the TBN transform for a primitive vertex with the given properties.
        /// </summary>
        public static Rotation3 Tbn(Vector3 normal, Vector4 tangent)
        {
            // TODO: Account for tangent orientation
            throw new NotImplementedException();
        }

        /// <summary>
        /// Computes the TBN transform for a primitive vertex with the given properties, then applies a transform
        /// to it.
        /// </summary>
        public static Roflection3 TransformTbn(Affine3 trans, Vector3 normal, Vector4 tangent)
        {
            normal = Vector3.Normalize(trans.Linear * normal);
            tangent.X_Y_Z = Vector3.Normalize(trans.Linear * tangent.X_Y_Z);
            Vector3 bitangent = Vector3.Cross(normal, tangent.X_Y_Z);
            tangent.X_Y_Z = Vector3.Cross(bitangent, normal);
            Rotation3 rot = (Rotation3)new Matrix3x3(tangent.X_Y_Z, bitangent, normal);

            // Note that the convention for normals in this engine differs from glTF. The G component corresponds to
            // +Y rather than -Y. We invert G values when importing an image, so we also need to flip the bitangent.
            if (tangent.W > 0)
            {
                // Shortcut for: rot = rot * Rotation3i.NegX_PosY_NegZ
                rot._quat = new Quaternion(
                    -rot._quat.C,
                    -rot._quat.D,
                    rot._quat.A,
                    rot._quat.B);
                return new Roflection3(rot, true);
            }
            else
            {
                return rot;
            }
        }
    }

    /// <summary>
    /// The render mode of a <see cref="GltfPrimitive"/>.
    /// </summary>
    public enum GltfPrimitiveMode : byte
    {
        Points = 0,
        Lines = 1,
        LinesLoop = 2,
        LinesStrip = 3,
        Triangles = 4,
        TriangleStrip = 5,
        TriangleFan = 6
    }

    /// <summary>
    /// Describes a glTF "material" object, abstracting out storage details.
    /// </summary>
    public sealed class GltfMaterial
    {
        public GltfMaterial(
            string name,
            MaterialKind paramKind,
            uint paramIndex,
            TextureLaxity laxity,
            BigImage2<Paint> albedo,
            BigImage2<Vector3> normal,
            BigImage2<Scalar> metalness,
            BigImage2<Scalar> roughness)
        {
            Name = name;
            ParameterKind = paramKind;
            ParameterIndex = paramIndex;
            Laxity = laxity;
            AlbedoImage = albedo;
            NormalImage = normal;
            MetalnessImage = metalness;
            RoughnessImage = roughness;
        }

        /// <summary>
        /// The name of this material, or <see cref="null"/> if not defined.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Indicates whether this is a parameteric material.
        /// </summary>
        public bool IsParameter => !ParameterKind.IsNone;

        /// <summary>
        /// The <see cref="MaterialKind"/> of this material, or <see cref="MaterialKind.None"/> if this is
        /// not a parameteric material.
        /// </summary>
        public MaterialKind ParameterKind { get; }

        /// <summary>
        /// The index of this parameteric material, if applicable.
        /// </summary>
        public uint ParameterIndex { get; }

        /// <summary>
        /// The <see cref="TextureLaxity"/> applied to material coordinates referencing this material.
        /// </summary>
        public TextureLaxity Laxity { get; }

        /// <summary>
        /// The image providing albedo and alpha information for this material, if applicable.
        /// </summary>
        public BigImage2<Paint> AlbedoImage { get; }

        /// <summary>
        /// The image providing normal information for this material, if applicable, using the convention where +X is
        /// towards +U, +Y is towards +V, and +Z is away from the material.
        /// </summary>
        public BigImage2<Vector3> NormalImage { get; }

        /// <summary>
        /// The image providing metalness information for this material, if applicable. A value of 1 means the material
        /// is a metal. A value of 0 means the material is a dielectric. Values in between are for blending between
        /// metals and dielectrics such as dirty metallic surfaces.
        /// </summary>
        public BigImage2<Scalar> MetalnessImage { get; }

        /// <summary>
        /// The image providing roughness information for this material, if applicable. A value of 1 means the material is
        /// completely rough. A value of 0 means the material is completely smooth. 
        /// </summary>
        public BigImage2<Scalar> RoughnessImage { get; }

        /// <summary>
        /// Constructs a <see cref="MaterialUsage"/> based on this <see cref="GltfMaterial"/>.
        /// </summary>
        public MaterialUsage Import()
        {
            // TODO: Caching
            if (IsParameter)
            {
                return MaterialUsage.Parameter(ParameterIndex, ParameterKind, Laxity);
            }
            else
            {
                // TODO: Is opaque?
                return MaterialUsage.Build(
                    new _ReferenceMaterial(
                        new MaterialTexture2(
                            true, true, true,
                            AlbedoImage, NormalImage,
                            MetalnessImage, RoughnessImage),
                        Vector2.Zero, new Vector2(1, 1),
                        Rotation2.Identity,  MaterialKind.Square),
                    Laxity);
            }
        }
    }
    
    /// <summary>
    /// A list of <see cref="SolidVertex"/>s constructed from seperated lists of vertex properties,
    /// as well as a possible transformation.
    /// </summary>
    public sealed class MaterialVertexList : BigList<SolidVertex>
    {
        internal MaterialVertexList(
            Affine3 trans,
            BigList<Vector3> positions,
            BigList<Vector3> normals,
            BigList<Vector4> tangents,
            BigList<Vector2> coords)
            : base(positions.Length)
        {
            Transform = trans;
            Positions = positions;
            Normals = normals;
            Tangents = tangents;
            Coords = coords;
        }

        /// <summary>
        /// The transform applied to <see cref="Positions"/>, <see cref="Normals"/> and <see cref="Tangents"/>.
        /// </summary>
        public Affine3 Transform { get; }

        /// <summary>
        /// The positions of the vertices.
        /// </summary>
        public BigList<Vector3> Positions { get; }

        /// <summary>
        /// The normals of the vertices.
        /// </summary>
        public BigList<Vector3> Normals { get; }

        /// <summary>
        /// The tangents of the vertices, with the last component defining UV orientation.
        /// </summary>
        public BigList<Vector4> Tangents { get; }

        /// <summary>
        /// The texture coordinates of the vertices.
        /// </summary>
        public BigList<Vector2> Coords { get; }

        public override IDisposable Read(out IListReader<SolidVertex> reader)
        {
            _Reader res = new _Reader();
            reader = res;
            res._trans = Transform;
            return DisposableHelper.Group(
                Positions.Read(out res._poss),
                Normals.Read(out res._normals),
                Tangents.Read(out res._tangents),
                Coords.Read(out res._coords));
        }

        /// <summary>
        /// A reader for a <see cref="MaterialVertexList"/>.
        /// </summary>
        public sealed class _Reader : IListReader<SolidVertex>
        {
            internal Affine3 _trans;
            internal IListReader<Vector3> _poss;
            internal IListReader<Vector3> _normals;
            internal IListReader<Vector4> _tangents;
            internal IListReader<Vector2> _coords;

            uint IListReader<SolidVertex>.Read(Span<SolidVertex> buffer)
            {
                throw new NotImplementedException();
            }

            void IListReader<SolidVertex>.Skip(uint num)
            {
                throw new NotImplementedException();
            }
        }
    }
    
    /// <summary>
    /// A helper class for reading and loading data from a glTF file.
    /// </summary>
    internal sealed class _GltfLoader
    {
        private GltfScene[] _scenes;
        private GltfNode[] _nodes;
        private GltfMesh[] _meshes;
        private GltfMaterial[] _mats;
        private BigImage2<Paint>[] _images;
        private BigBinary[] _buffers;
        private _GltfLoader(RepositoryPathViewer path, JsonObject source, BigBinary binary)
        {
            Path = path;
            Source = source;
            Binary = binary;

            // Allocate caches
            if (Source.TryGetValue("scenes", out JsonArray jScenes))
                _scenes = new GltfScene[jScenes.Count];
            if (Source.TryGetValue("nodes", out JsonArray jNodes))
                _nodes = new GltfNode[jNodes.Count];
            if (Source.TryGetValue("meshes", out JsonArray jMeshes))
                _meshes = new GltfMesh[jMeshes.Count];
            if (Source.TryGetValue("materials", out JsonArray jMaterials))
                _mats = new GltfMaterial[jMaterials.Count];
            if (Source.TryGetValue("images", out JsonArray jImages))
                _images = new BigImage2<Paint>[jImages.Count * 2];
            if (source.TryGetValue("buffers", out JsonArray jBuffers))
                _buffers = new BigBinary[jBuffers.Count];
        }

        /// <summary>
        /// The path at which the glTF file is found. This is used as the base path for referencing external assets.
        /// </summary>
        public RepositoryPathViewer Path { get; }

        /// <summary>
        /// The root <see cref="JsonObject"/> from which all glTF data is loaded.
        /// </summary>
        public JsonObject Source { get; }

        /// <summary>
        /// The embedded buffer for the GLB file, if applicable (<see cref="null"/> otherwise).
        /// </summary>
        public BigBinary Binary { get; }

        /// <summary>
        /// Creates a <see cref="_GltfLoader"/> to read content from the file at the given path.
        /// </summary>
        public static unsafe _GltfLoader Create(RepositoryPathViewer path)
        {
            if (!path.TryResolveAsFile(out BigBinary binary))
                throw new Exception("File not found");
            string ext = path.Extension.ToLowerInvariant();
            if (ext == "gltf")
            {
                using (var stream = binary.OpenStream())
                {
                    using (var reader = new System.IO.StreamReader(stream, System.Text.Encoding.UTF8, false))
                    {
                        var root = (JsonObject)JsonValue.Load(reader);
                        return new _GltfLoader(path, root, null);
                    }
                }
            }
            else if (ext == "glb")
            {
                using (binary.Read(0, out ISeekableBinaryReader reader))
                {
                    ulong startPos = reader.Position;
                    if (reader.ReadUInt32() != Signature)
                        throw new Exception("Invalid GLB file");
                    uint version = reader.ReadUInt32();
                    uint len = reader.ReadUInt32();

                    // Read JSON chunk
                    // TODO: Streaming read
                    uint chunkLen = reader.ReadUInt32();
                    _GlbChunkType chunkType = (_GlbChunkType)reader.ReadUInt32();
                    if (chunkType != _GlbChunkType.JSON)
                        throw new Exception("Invalid GLB file");
                    string json;
                    fixed (byte* jsonBytes = reader.ReadRaw(chunkLen))
                        json = System.Text.Encoding.UTF8.GetString(jsonBytes, (int)chunkLen);
                    var root = (JsonObject)JsonValue.Parse(json);

                    // Read remaining chunks
                    BigBinary embedded = null;
                    while ((reader.Position - startPos) < len)
                    {
                        chunkLen = reader.ReadUInt32();
                        chunkType = (_GlbChunkType)reader.ReadUInt32();
                        switch (chunkType)
                        {
                            case _GlbChunkType.BIN:
                                embedded = binary.Slice(reader.Position - startPos, (uint)chunkLen);
                                reader.Skip(chunkLen);
                                break;
                        }
                    }

                    // Create reader
                    return new _GltfLoader(path, root, embedded);
                }
            }
            else
            {
                throw new Exception("Unrecognized file extension");
            }
        }

        /// <summary>
        /// The 4-byte signature which begins a GLB file, in little endian byte order.
        /// </summary>
        public const ulong Signature = 0x46546C67;
        
        /// <summary>
        /// The default scene in the file, or <see cref="null"/> if no such scene exists.
        /// </summary>
        public GltfScene DefaultScene
        {
            get
            {
                if (Source.TryGetValue("scene", out int index))
                    return _scene((uint)index);
                return null;
            }
        }

        /// <summary>
        /// The list of scenes in the file.
        /// </summary>
        public List<GltfScene> Scenes
        {
            get
            {
                if (_scenes is null)
                    return List.Empty<GltfScene>();
                for (uint i = 0; i < _scenes.Length; i++)
                    _scene(i);
                return new List<GltfScene>(_scenes);
            }
        }

        /// <summary>
        /// The list of nodes in the file.
        /// </summary>
        public List<GltfNode> Nodes
        {
            get
            {
                if (_nodes is null)
                    return List.Empty<GltfNode>();
                for (uint i = 0; i < _nodes.Length; i++)
                    _node(i);
                return new List<GltfNode>(_nodes);
            }
        }

        /// <summary>
        /// The list of meshes in the file.
        /// </summary>
        public List<GltfMesh> Meshes
        {
            get
            {
                if (_meshes is null)
                    return List.Empty<GltfMesh>();
                for (uint i = 0; i < _meshes.Length; i++)
                    _mesh(i);
                return new List<GltfMesh>(_meshes);
            }
        }

        /// <summary>
        /// The list of materials in the file.
        /// </summary>
        public List<GltfMaterial> Materials
        {
            get
            {
                if (_mats is null)
                    return List.Empty<GltfMaterial>();
                for (uint i = 0; i < _mats.Length; i++)
                    _mat(i);
                return new List<GltfMaterial>(_mats);
            }
        }

        /// <summary>
        /// Loads the <see cref="GltfScene"/> at the given index.
        /// </summary>
        private GltfScene _scene(uint index)
        {
            ref GltfScene res = ref _scenes[index];
            if (res is null)
            {
                JsonObject jScene = (JsonObject)Source["scenes"][(int)index];
                if (!jScene.TryGetValue("name", out string name))
                    name = null;
                var nodes = BagBuilder<GltfNode>.CreateEmpty();
                if (jScene.TryGetValue("nodes", out JsonArray jNodes))
                    foreach (JsonValue jNode in jNodes)
                        nodes.Add(_node(jNode));
                res = new GltfScene(name, nodes.Finish());
            }
            return res;
        }

        /// <summary>
        /// Loads the <see cref="GltfNode"/> at the given index.
        /// </summary>
        private GltfNode _node(uint index)
        {
            ref GltfNode res = ref _nodes[index];
            if (res is null)
            {
                JsonObject jNode = (JsonObject)Source["nodes"][(int)index];
                if (!jNode.TryGetValue("name", out string name))
                    name = null;

                // Load baseline transform
                Affine3 trans;
                if (jNode.TryGetValue("matrix", out JsonArray jMatrix))
                {
                    throw new NotImplementedException();
                }
                else
                {
                    trans = Affine3.Identity;
                    if (jNode.TryGetValue("translation", out JsonArray jTranslation))
                    {
                        trans *= Affine3.Translation(
                            (double)jTranslation[0],
                            (double)jTranslation[1],
                            (double)jTranslation[2]);
                    }
                    if (jNode.TryGetValue("rotation", out JsonArray jRotation))
                    {
                        trans *= new Rotation3(
                            (double)jRotation[3],
                            (double)jRotation[0],
                            (double)jRotation[1],
                            (double)jRotation[2]);
                    }
                    if (jNode.TryGetValue("scale", out JsonArray jScale))
                    {
                        trans *= Affine3.Scale(
                            (double)jScale[0],
                            (double)jScale[1],
                            (double)jScale[2]);
                    }
                }

                // Load mesh
                GltfMesh mesh = null;
                if (jNode.TryGetValue("mesh", out uint meshIndex))
                    mesh = _mesh(meshIndex);

                // Load children
                var children = BagBuilder<GltfNode>.CreateEmpty();
                if (jNode.TryGetValue("children", out JsonArray jChildren))
                    foreach (JsonValue jChild in jChildren)
                        children.Add(_node(jChild));

                // Build node
                res = new GltfNode(name, trans, children.Finish(), mesh);
            }
            return res;
        }

        /// <summary>
        /// Loads the <see cref="GltfMesh"/> at the given index.
        /// </summary>
        private GltfMesh _mesh(uint index)
        {
            ref GltfMesh res = ref _meshes[index];
            if (res is null)
            {
                JsonObject jMesh = (JsonObject)Source["meshes"][(int)index];
                if (!jMesh.TryGetValue("name", out string name))
                    name = null;
                JsonArray jPrimitives = (JsonArray)jMesh["primitives"];
                var prims = ListBuilder<GltfPrimitive>.CreateEmpty((uint)jPrimitives.Count);
                foreach (JsonObject jPrimitive in jPrimitives)
                    prims.Append(_primitive(jPrimitive));
                res = new GltfMesh(name, prims);
            }
            return res;
        }

        /// <summary>
        /// Loads a <see cref="GltfPrimitive"/> from a <see cref="JsonObject"/>.
        /// </summary>
        private GltfPrimitive _primitive(JsonObject jPrimitive)
        {
            GltfPrimitive prim = default;
            var ties = ListBuilder<IndexSet>.CreateEmpty();

            // Get material
            if (jPrimitive.TryGetValue("material", out uint matIndex))
                prim.Material = _mat(matIndex);

            // Get mode
            if (jPrimitive.TryGetValue("mode", out uint mode))
                prim.Mode = (GltfPrimitiveMode)mode;
            else
                prim.Mode = GltfPrimitiveMode.Triangles;

            // Load vertex attributes
            JsonObject jAttrs = (JsonObject)jPrimitive["attributes"];
            foreach (var attr in jAttrs)
            {
                uint attrIndex = attr.Value;
                switch (attr.Key)
                {
                    case "POSITION":
                        prim.Positions = _accessorVector3(attrIndex);
                        break;
                    case "NORMAL":
                        prim.Normals = _accessorVector3(attrIndex);
                        break;
                    case "TANGENT":
                        prim.Tangents = _accessorVector4(attrIndex);
                        break;
                    case "TEXCOORD_0":
                        prim.Coords = _accessorVector2(attrIndex);
                        break;
                    case "COLOR_0":
                        prim.Colors = _accessorPaint(attrIndex);
                        break;
                    default:
                        if (attr.Key.StartsWith("_TIE_"))
                        {
                            uint tieIndex = uint.Parse(attr.Key.Substring(5), CultureInfo.InvariantCulture);
                            while (ties.Length <= tieIndex) ties.Append(IndexSet.None);

                            // Load mesh tie indices
                            IndexSetBuilder indices = IndexSetBuilder.CreateEmpty();
                            List<Scalar> weights = _accessorScalar(attrIndex);
                            for (uint i = 0; i < weights.Length; i++)
                                if (weights[i] > 0)
                                    indices.Include(i);

                            // Add mesh tie
                            ties[tieIndex] = indices.Finish();
                        }
                        else
                        {
                            // Unrecognized attribute
                            throw new NotImplementedException();
                        }
                        break;
                }
            }

            // Get indices
            if (jPrimitive.TryGetValue("indices", out uint indicesIndex))
                prim.Indices = _accessorUInt32(indicesIndex);

            // Finalize primitive
            prim.TieIndices = ties.Finish();
            return prim;
        }

        /// <summary>
        /// Loads the <see cref="GltfMaterial"/> at the given index.
        /// </summary>
        private GltfMaterial _mat(uint index)
        {
            // TODO: Transluceny considerations
            // TODO: Emissive texture
            // TODO: Sampler considerations
            ref GltfMaterial res = ref _mats[index];
            if (res is null)
            {
                // Load material properties
                JsonObject jMat = (JsonObject)Source["materials"][(int)index];
                if (!jMat.TryGetValue("name", out string name))
                    name = null;
                BigImage2<Paint> albedo = null;
                BigImage2<Vector3> normal = null;
                BigImage2<Scalar> metalness = null;
                BigImage2<Scalar> roughness = null;
                if (jMat.TryGetValue("normalTexture", out JsonObject jNormalTextureInfo))
                {
                    PaintTexture2 normalTexture = _texture(jNormalTextureInfo, false);
                    normal = normalTexture.Image.StripAlpha().ToNormal(true);
                }
                if (jMat.TryGetValue("pbrMetallicRoughness", out JsonObject jPbrMetallicRoughness))
                {
                    // TODO: Constant factors
                    if (jPbrMetallicRoughness.TryGetValue("baseColorTexture", out JsonObject jAlbedoTextureInfo))
                    {
                        PaintTexture2 albedoTexture = _texture(jAlbedoTextureInfo, true);
                        albedo = albedoTexture.Image;
                    }
                    if (jPbrMetallicRoughness.TryGetValue("metallicRoughnessTexture", out JsonObject jMetaTextureInfo))
                    {
                        PaintTexture2 metallicRoughnessTexture = _texture(jMetaTextureInfo, false);
                        metalness = metallicRoughnessTexture.Image.Channel(PaintChannel.B);
                        roughness = metallicRoughnessTexture.Image.Channel(PaintChannel.G);
                    }
                }

                // Load extra properties
                uint paramIndex = default;
                MaterialKind paramKind = MaterialKind.None;
                TextureLaxity laxity = TextureLaxity.None;
                if (jMat.TryGetValue("extras", out JsonObject jExtras))
                {
                    // Load material kind if available
                    MaterialKind kind = MaterialKind.Square;
                    if (jExtras.TryGetValue("kind", out string kindStr))
                    {
                        kind = MaterialKind.Any;
                        foreach (string kindPartStr in kindStr.Split(' '))
                        {
                            if (!_kinds.TryGet(kindPartStr, out MaterialKind kindPart))
                                throw new Exception("Unrecognized material kind");
                            kind &= kindPart;
                        }
                        if (kind.IsNone)
                            throw new Exception("Invalid material kind");
                        laxity = laxity.Upon(kind);
                    }

                    // If a parameter index is available, treat the material as parameteric
                    if (jExtras.TryGetValue("parameterIndex", out paramIndex))
                        paramKind = kind;
                }

                // Construct material
                res = new GltfMaterial(name, paramKind, paramIndex, laxity, albedo, normal, metalness, roughness);
            }
            return res;
        }

        /// <summary>
        /// A table of <see cref="MaterialKind"/>'s indexed by their name.
        /// </summary>
        private static readonly Map<string, MaterialKind> _kinds = Map.Of(
            ("square", MaterialKind.Square),
            ("band", MaterialKind.Band),
            ("plane", MaterialKind.Plane),
            ("isotropic", MaterialKind.Isotropic),
            ("opaque", MaterialKind.Opaque));

        /// <summary>
        /// Loads a texture based on the given "textureInfo". This requires a texture coordinate index of 0. 
        /// </summary>
        private PaintTexture2 _texture(JsonObject jTextureInfo, bool isSrgb)
        {
            if (jTextureInfo.TryGetValue("texCoord", out int texCoord))
                if (texCoord != 0)
                    throw new NotSupportedException("Multiple texture coordinates not supported");
            return _texture(jTextureInfo["index"], isSrgb);
        }

        /// <summary>
        /// Loads the <see cref="PaintTexture2"/> at the given index.
        /// </summary>
        private PaintTexture2 _texture(uint index, bool isSrgb)
        {
            JsonObject jTexture = (JsonObject)Source["textures"][(int)index];
            Sampler2 sampler = _defaultSampler;
            if (jTexture.TryGetValue("sampler", out uint samplerIndex))
                sampler = _sampler(samplerIndex);
            BigImage2<Paint> image = _image(jTexture["source"], isSrgb);
            return new PaintTexture2(sampler, image);
        }

        /// <summary>
        /// Loads the <see cref="Graphics.Sampler2"/> at the given index.
        /// </summary>
        private Graphics.Sampler2 _sampler(uint index)
        {
            throw new NotImplementedException();
        }

        private static readonly Graphics.Sampler2 _defaultSampler = new Sampler2
        {
            Wrapping_X = SamplerWrapping.Repeat,
            Wrapping_Y = SamplerWrapping.Repeat,
            Filtering = SamplerFiltering.Linear
        };

        /// <summary>
        /// Loads the <see cref="Image2{T}"/> at the given index.
        /// </summary>
        /// <param name="isSrgb">If true, indicates that colors values should be interpreted as sRGB, rather than
        /// linearly.</param>
        private BigImage2<Paint> _image(uint index, bool isSrgb)
        {
            ref BigImage2<Paint> res = ref _images[index * 2 + (isSrgb ? 1 : 0)];
            if (res is null)
            {
                JsonObject jImage = (JsonObject)Source["images"][(int)index];
                if (jImage.TryGetValue("uri", out string uri))
                {
                    RelativePath uriPath = RelativePath.Parse(uri);
                    RepositoryPathViewer uriViewer = Path.Parent / uriPath;
                    if (!uriViewer.TryResolveAsFile(out BigBinary binary))
                        throw new Exception("External image not found");
                    return BigImage2.FromFile(uriViewer, isSrgb).AsPaint;
                }
                else
                {
                    throw new NotImplementedException();
                }
            }
            return res;
        }

        /// <summary>
        /// Loads an accessor as a list of <see cref="uint"/>, throwing an exception if this is not
        /// the correct type. This will automatically extend smaller unsigned integers into <see cref="uint"/>s.
        /// </summary>
        private BigList<uint> _accessorUInt32(uint index)
        {
            return _accessor(index, "SCALAR", uint32Encodings);
        }

        /// <summary>
        /// Loads an accessor as a list of <see cref="Scalar"/>, throwing an exception if this is not
        /// the correct type.
        /// </summary>
        private BigList<Scalar> _accessorScalar(uint index)
        {
            return _accessor(index, "SCALAR", scalarEncodings);
        }

        /// <summary>
        /// Loads an accessor as a list of <see cref="Vector2"/>s, throwing an exception if this
        /// is not the correct type.
        /// </summary>
        private BigList<Vector2> _accessorVector2(uint index)
        {
            return _accessor(index, "VEC2", vec2Encodings);
        }

        /// <summary>
        /// Loads an accessor as a list of <see cref="Vector3"/>s, throwing an exception if this
        /// is not the correct type.
        /// </summary>
        private BigList<Vector3> _accessorVector3(uint index)
        {
            return _accessor(index, "VEC3", vec3Encodings);
        }

        /// <summary>
        /// Loads an accessor as a list of <see cref="Vector4"/>s, throwing an exception if this
        /// is not the correct type.
        /// </summary>
        private BigList<Vector4> _accessorVector4(uint index)
        {
            return _accessor(index, "VEC4", vec4Encodings);
        }

        /// <summary>
        /// Loads an accessor as a list of <see cref="Paint"/>s, throwing an exception if this
        /// is not the correct type.
        /// </summary>
        private BigList<Paint> _accessorPaint(uint index)
        {
            // TODO: Opaque colors from VEC3
            return _accessor(index, "VEC4", paintEncodings);
        }

        /// <summary>
        /// The table of possible encodings for <see cref="uint"/> accessors.
        /// </summary>
        private readonly Encoding<uint>[] uint32Encodings = new Encoding<uint>[]
        {
            Encoding.UInt32.I8,  // BYTE
            null,                // NORMALIZED BYTE
            Encoding.UInt32.I8,  // UNSIGNED_BYTE
            null,                // NORMALIZED UNSIGNED_BYTE
            Encoding.UInt32.I16, // SHORT
            null,                // NORMALIZED SHORT
            Encoding.UInt32.I16, // UNSIGNED_SHORT
            null,                // NORMALIZED UNSIGNED_SHORT
            Encoding.UInt32.I32, // INT
            null,                // NORMALIZED INT
            Encoding.UInt32.I32, // UNSIGNED_INT
            null,                // NORMALIZED UNSIGNED_INT
            null,                // FLOAT
        };

        /// <summary>
        /// The table of possible encodings for <see cref="Scalar"/> accessors.
        /// </summary>
        private readonly Encoding<Scalar>[] scalarEncodings = new Encoding<Scalar>[]
        {
            null,                // BYTE
            Scalar.Encoding.S8,  // NORMALIZED BYTE
            null,                // UNSIGNED_BYTE
            Scalar.Encoding.U8,  // NORMALIZED UNSIGNED_BYTE
            null,                // SHORT
            null,                // NORMALIZED SHORT
            null,                // UNSIGNED_SHORT
            null,                // NORMALIZED UNSIGNED_SHORT
            null,                // INT
            null,                // NORMALIZED INT
            null,                // UNSIGNED_INT
            null,                // NORMALIZED UNSIGNED_INT
            Scalar.Encoding.F32, // FLOAT
        };

        /// <summary>
        /// The table of possible encodings for <see cref="Vector2"/> accessors.
        /// </summary>
        private readonly Encoding<Vector2>[] vec2Encodings = new Encoding<Vector2>[]
        {
            null,                       // BYTE
            null,                       // NORMALIZED BYTE
            null,                       // UNSIGNED_BYTE
            null,                       // NORMALIZED UNSIGNED_BYTE
            null,                       // SHORT
            null,                       // NORMALIZED SHORT
            null,                       // UNSIGNED_SHORT
            null,                       // NORMALIZED UNSIGNED_SHORT
            null,                       // INT
            null,                       // NORMALIZED INT
            null,                       // UNSIGNED_INT
            null,                       // NORMALIZED UNSIGNED_INT
            Vector2.Encoding.Xf32_Yf32, // FLOAT
        };

        /// <summary>
        /// The table of possible encodings for <see cref="Vector3"/> accessors.
        /// </summary>
        private readonly Encoding<Vector3>[] vec3Encodings = new Encoding<Vector3>[]
        {
            null,                            // BYTE
            null,                            // NORMALIZED BYTE
            null,                            // UNSIGNED_BYTE
            null,                            // NORMALIZED UNSIGNED_BYTE
            null,                            // SHORT
            null,                            // NORMALIZED SHORT
            null,                            // UNSIGNED_SHORT
            null,                            // NORMALIZED UNSIGNED_SHORT
            null,                            // INT
            null,                            // NORMALIZED INT
            null,                            // UNSIGNED_INT
            null,                            // NORMALIZED UNSIGNED_INT
            Vector3.Encoding.Xf32_Yf32_Zf32, // FLOAT
        };

        /// <summary>
        /// The table of possible encodings for <see cref="Vector4"/> accessors.
        /// </summary>
        private readonly Encoding<Vector4>[] vec4Encodings = new Encoding<Vector4>[]
        {
            null,                                 // BYTE
            null,                                 // NORMALIZED BYTE
            null,                                 // UNSIGNED_BYTE
            null,                                 // NORMALIZED UNSIGNED_BYTE
            null,                                 // SHORT
            null,                                 // NORMALIZED SHORT
            null,                                 // UNSIGNED_SHORT
            null,                                 // NORMALIZED UNSIGNED_SHORT
            null,                                 // INT
            null,                                 // NORMALIZED INT
            null,                                 // UNSIGNED_INT
            null,                                 // NORMALIZED UNSIGNED_INT
            Vector4.Encoding.Xf32_Yf32_Zf32_Wf32, // FLOAT
        };

        /// <summary>
        /// The table of possible encodings for <see cref="Paint"/> accessors.
        /// </summary>
        private readonly Encoding<Paint>[] paintEncodings = new Encoding<Paint>[]
        {
            null,                                 // BYTE
            null,                                 // NORMALIZED BYTE
            null,                                 // UNSIGNED_BYTE
            null,                                 // NORMALIZED UNSIGNED_BYTE
            null,                                 // SHORT
            null,                                 // NORMALIZED SHORT
            null,                                 // UNSIGNED_SHORT
            Paint.Encoding.R16_G16_B16_A16,       // NORMALIZED UNSIGNED_SHORT
            null,                                 // INT
            null,                                 // NORMALIZED INT
            null,                                 // UNSIGNED_INT
            null,                                 // NORMALIZED UNSIGNED_INT
            null,                                 // FLOAT
        };

        /// <summary>
        /// Loads an accessor as a list of values with a particular encoding.
        /// </summary>
        /// <param name="type">The expected value for "type".</param>
        /// <param name="encodings">The table of potential encodings to use for loading, organized by
        /// "normalized", then by "componentType".</param>
        private BigList<T> _accessor<T>(uint index, string type, Encoding<T>[] encodings)
        {
            JsonObject jAccessor = (JsonObject)Source["accessors"][(int)index];
            if (jAccessor["type"] != type)
                throw new Exception("Wrong accessor type");
            uint len = jAccessor["count"];
            int normalized = 0;
            if (jAccessor.TryGetValue("normalized", out JsonValue jNormalized) && jNormalized)
                normalized = 1;
            Encoding<T> encoding = encodings[(jAccessor["componentType"] - 5120) * 2 + normalized];
            if (encoding is null)
                throw new Exception("Unrecognized component type");
            BigList<T> res;
            if (jAccessor.TryGetValue("bufferView", out uint bufferViewIndex))
            {
                if (!jAccessor.TryGetValue("offset", out uint addOffset))
                    addOffset = 0;
                res = _bufferView(encoding, bufferViewIndex, len, addOffset);
            }
            else
            {
                // Zero initialize
                ReadOnlySpan<byte> zeroBuffer = stackalloc byte[(int)encoding.Size.Max];
                Data.Serialization.Binary.Read(ref zeroBuffer, encoding, out T zeroValue);
                res = BigList.Replicate(len, zeroValue);
            }

            // Check for sparse deviations
            if (jAccessor.TryGetValue("sparse", out JsonObject jSparse))
            {
                uint sparseLen = jSparse["count"];
                JsonObject jIndices = (JsonObject)jSparse["indices"];
                JsonObject jValues = (JsonObject)jSparse["values"];
                Encoding<uint> indicesEncoding = uint32Encodings[(jIndices["componentType"] - 5120) * 2];
                if (!jIndices.TryGetValue("byteOffset", out uint addOffset))
                    addOffset = 0;
                BigList<uint> indices = _bufferView(indicesEncoding, jIndices["bufferView"], sparseLen, addOffset);
                if (!jValues.TryGetValue("byteOffset", out addOffset))
                    addOffset = 0;
                BigList<T> values = _bufferView(encoding, jValues["bufferView"], sparseLen, addOffset);
                res = BigList.Sparse(res, indices, values);
            }
            return res;
        }

        /// <summary>
        /// Interprets the contents of a buffer view as a list, using the given <see cref="Encoding{T}"/>.
        /// </summary>
        private BigList<T> _bufferView<T>(Encoding<T> encoding, uint index, uint len, uint addOffset)
        {
            BigBinary buffer = _bufferView(index, out uint offset, out uint size, out uint? stride);
            offset += addOffset;
            Debug.Assert(encoding.Size.Min == encoding.Size.Max);
            int skip = (int?)stride - (int)encoding.Size.Min ?? 0;
            if (skip < 0)
                throw new Exception("Values may not overlap");
            return BigList.FromBinary(
                buffer, encoding,
                offset, (uint)skip, len);
        }

        /// <summary>
        /// Gets the <see cref="BigBinary"/> for the contents of the buffer for the given view, as well as the
        /// parameters of the view.
        /// </summary>
        private BigBinary _bufferView(uint index, out uint offset, out uint size, out uint? stride)
        {
            JsonObject jBufferView = (JsonObject)Source["bufferViews"][(int)index];
            BigBinary buffer = _buffer(jBufferView["buffer"]);
            if (!jBufferView.TryGetValue("byteOffset", out offset))
                offset = 0;
            size = jBufferView["byteLength"];
            stride = null;
            if (jBufferView.TryGetValue("byteStride", out uint strideValue))
                stride = strideValue;
            return buffer;
        }

        /// <summary>
        /// Constructs a <see cref="BigBinary"/> for the contents of the given buffer.
        /// </summary>
        private BigBinary _buffer(uint index)
        {
            ref BigBinary res = ref _buffers[index];
            if (res is null)
            {
                JsonObject jBuffer = (JsonObject)Source["buffers"][(int)index];
                ulong size = jBuffer["byteLength"];
                if (jBuffer.TryGetValue("uri", out string uri))
                {
                    RelativePath uriPath = RelativePath.Parse(uri);
                    RepositoryPathViewer uriViewer = Path.Parent / uriPath;
                    if (!uriViewer.TryResolveAsFile(out res))
                        throw new Exception("External buffer not found");
                    if (size != res.Size)
                        throw new Exception("Buffer size mismatch");
                }
                else if (index == 0 && !(Binary is null))
                {
                    res = Binary;
                }
                else
                {
                    throw new Exception("Invalid buffer definition");
                }
            }
            return res;
        }
    }

    /// <summary>
    /// Identifies a type of GLB chunk. The value of a <see cref="_GlbChunkType"/> is equivalent to the 4-byte marker for
    /// the chunk, in little endian byte order.
    /// </summary>
    internal enum _GlbChunkType : uint
    {
        JSON = 0x4E4F534A,
        BIN = 0x004E4942
    }
}