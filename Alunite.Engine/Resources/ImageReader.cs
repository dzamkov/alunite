﻿using System;
using System.Runtime.InteropServices;

using Alunite.Data;
using Alunite.Data.Serialization;

namespace Alunite.Resources
{
    /// <summary>
    /// An interface for reading the contents of an image row by row, using some binary encoding for pixels.
    /// </summary>
    public interface IImageRowReader
    {
        /// <summary>
        /// The minimum size for the buffer that can be provided to <see cref="ReadRow(Span{byte})"/>. This is the
        /// maximum size for the row data produced by this reader.
        /// </summary>
        uint MinBufferSize { get; }

        /// <summary>
        /// Populates the given buffer with the next row of data.
        /// </summary>
        void ReadRow(Span<byte> buffer);
    }

    /// <summary>
    /// Contains helper functions related to <see cref="IImageRowReader"/>.
    /// </summary>
    public static class RowReader
    {
        /// <summary>
        /// Creates a <see cref="IImageRowReader"/> which reads pixel data from the given reader and re-encodes
        /// it using a different encoding.
        /// </summary>
        public static IImageRowReader Reencode<T>(this IImageRowReader reader, Encoding<T> from, Encoding<T> to)
        {
            // If the encodings are the same, we don't need to do anything
            if (from == to)
                return reader;
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a buffer with the next <paramref name="numRows"/> rows from this reader, where each
        /// row is allocated <paramref name="stride"/> bytes. The buffer is valid only until the next call
        /// is made to the reader.
        /// </summary>
        /// <param name="stride">The number of bytes allocated for each row in the returned buffer. This must
        /// be at least <see cref="IImageRowReader.MinBufferSize"/>.</param>
        public static ReadOnlySpan<byte> ReadBlock(this IImageRowReader reader, uint numRows, uint stride)
        {
            if (stride < reader.MinBufferSize)
                throw new ArgumentException("Stride too small", nameof(stride));

            // Shortcut for binary row reader
            if (reader is BinaryRowReader binaryReader && binaryReader.Stride == stride)
                return binaryReader.Source.ReadRaw(numRows * stride);

            // Generate the data into a new buffer and return that
            byte[] res = new byte[numRows * stride];
            uint offset = 0;
            for (uint y = 0; y < numRows; y++)
            {
                reader.ReadRow(res.AsSpan((int)offset, (int)stride));
                offset += stride;
            }
            return res;
        }
    }

    /// <summary>
    /// An <see cref="IImageRowReader"/> which simply reads serialized row data from an <see cref="IBinaryReader"/>.
    /// </summary>
    public sealed class BinaryRowReader : IImageRowReader
    {
        public BinaryRowReader(IBinaryReader source, uint stride)
        {
            Source = source;
            Stride = stride;
        }

        /// <summary>
        /// The source <see cref="IBinaryReader"/> for this <see cref="IImageRowReader"/>.
        /// </summary>
        public IBinaryReader Source { get; }

        /// <summary>
        /// The number of bytes per row in <see cref="Source"/>.
        /// </summary>
        public uint Stride { get; }

        /// <summary>
        /// Populates the given buffer with the next row of data.
        /// </summary>
        public void ReadRow(Span<byte> buffer)
        {
            Source.ReadRaw(buffer.Slice(0, (int)Stride));
        }

        uint IImageRowReader.MinBufferSize => Stride;
    }

    /// <summary>
    /// A <see cref="IImageRowReader"/> which uses bitwise operations to combine and process data from
    /// one or more source <see cref="IImageRowReader"/>s.
    /// </summary>
    public sealed class BitwiseRowReader : IImageRowReader
    {
        internal BitwiseRowReader(
            uint numChunks, List<uint> resultChunkInitial,
            Bag<(IImageRowReader, BitwiseImage.Procedure)> sources)
        {
            NumChunks = numChunks;
            ChunkInitial = resultChunkInitial;
            Sources = sources;
        }

        /// <summary>
        /// The number of chunks for each row of this <see cref="BitwiseRowReader"/>. A chunk is a fixed-length
        /// span of pixels which are processed as a group.
        /// </summary>
        public uint NumChunks { get; }

        /// <summary>
        /// The initial binary data for a chunk of pixels in the result row. This is repeated to initialize the result
        /// buffer for this <see cref="BitwiseRowReader"/>. Data from <see cref="Sources"/> is XORed onto this buffer.
        /// </summary>
        public List<uint> ChunkInitial { get; }

        /// <summary>
        /// The number of <see cref="uint"/> words per chunk in the result.
        /// </summary>
        public uint ChunkSize => ChunkInitial.Length;

        /// <summary>
        /// The set of input <see cref="IImageRowReader"/>s which contribute to the results of this
        /// <see cref="BitwiseRowReader"/>, along with the <see cref="Procedure"/> applied for each.
        /// </summary>
        public Bag<(IImageRowReader, BitwiseImage.Procedure)> Sources { get; }

        /// <summary>
        /// Populates the given buffer with the next row of data.
        /// </summary>
        public void ReadRow(Span<byte> buffer)
        {
            Span<uint> wordBuffer = MemoryMarshal.Cast<byte, uint>(buffer);
            if (!BitConverter.IsLittleEndian)
                throw new NotImplementedException();

            // Initialize result
            // TODO: Special cases depending on ResultChunkInitial.Length
            uint resultWordIndex = 0;
            for (uint i = 0; i < NumChunks; i++)
                for (uint j = 0; j < ChunkInitial.Length; j++)
                    wordBuffer[(int)resultWordIndex++] = ChunkInitial[j];

            // Apply sources
            foreach ((IImageRowReader source, BitwiseImage.Procedure proc) in Sources)
            {
                // TODO: Improve buffer allocation
                uint minSourceBufferSize = Math.Max(NumChunks * proc.ChunkSize * sizeof(uint), source.MinBufferSize);
                Span<byte> sourceBuffer = stackalloc byte[(int)minSourceBufferSize];
                source.ReadRow(sourceBuffer);

                // Apply procedure to transfer data from source buffer to result buffer
                Span<uint> sourceWordBuffer = MemoryMarshal.Cast<byte, uint>(sourceBuffer);
                foreach (BitwiseImage.Operation op in proc.Operations)
                {
                    // TODO: Apply several operations in one pass
                    uint sourceWordIndex = 0;
                    resultWordIndex = 0;
                    for (uint i = 0; i < NumChunks; i++)
                    {
                        uint word = sourceWordBuffer[(int)(sourceWordIndex + op.SourceWordIndex)] & op.Mask;
                        word = Bitwise.RotateLeft(word, op.BitRotate);
                        wordBuffer[(int)(resultWordIndex + op.TargetWordIndex)] ^= word;
                        sourceWordIndex += proc.ChunkSize;
                        resultWordIndex += ChunkInitial.Length;
                    }
                }
            }
        }

        uint IImageRowReader.MinBufferSize => NumChunks * ChunkSize * sizeof(uint);
    }

    /// <summary>
    /// A <see cref="IImageRowReader"/> which applies a pixel mapping function to the values produced
    /// by a source <see cref="IImageRowReader"/>.
    /// </summary>
    public sealed class MapRowReader<T, TResult> : IImageRowReader
    {
        public MapRowReader(
            Func<T, TResult> func,
            Encoding<T> sourceEncoding,
            Encoding<TResult> resultEncoding,
            IImageRowReader source,
            uint size_x)
        {
            if (resultEncoding.Size.Max == uint.MaxValue)
                throw new ArgumentException("Result encoding must have a bounded size", nameof(ResultEncoding));
            Func = func;
            SourceEncoding = sourceEncoding;
            ResultEncoding = resultEncoding;
            Source = source;
            Length = size_x;
        }

        /// <summary>
        /// The mapping function applied by this <see cref="MapRowReader{T, TResult}"/>.
        /// </summary>
        public Func<T, TResult> Func { get; }

        /// <summary>
        /// The encoding of the pixel data produced by <see cref="Source"/>.
        /// </summary>
        public Encoding<T> SourceEncoding { get; }

        /// <summary>
        /// The encoding of the pixel data produced by this reader.
        /// </summary>
        public Encoding<TResult> ResultEncoding { get; }

        /// <summary>
        /// The <see cref="IImageRowReader"/> producing the source data to be mapped.
        /// </summary>
        public IImageRowReader Source { get; }

        /// <summary>
        /// The number of pixels in each row.
        /// </summary>
        public uint Length { get; }

        /// <summary>
        /// The minimum size for the buffer that can be provided to <see cref="ReadRow(Span{byte})"/>. This is the
        /// maximum size for the row data produced by this reader.
        /// </summary>
        public uint MinBufferSize => Length * ResultEncoding.Size.Max;

        /// <summary>
        /// Populates the given buffer with the next row of data.
        /// </summary>
        public void ReadRow(Span<byte> buffer)
        {
            // TODO: Do more to avoid bounds checking
            Span<byte> sourceBuffer = stackalloc byte[(int)Source.MinBufferSize];
            Source.ReadRow(sourceBuffer);
            ReadOnlySpan<byte> readSourceBuffer = sourceBuffer;
            for (uint i = 0; i < Length; i++)
            {
                Binary.Read(ref readSourceBuffer, SourceEncoding, out T source);
                TResult res = Func(source);
                Binary.Write(ref buffer, ResultEncoding, in res);
            }
        }
    }
}
