﻿using System;
using System.Diagnostics;
using System.Threading;

namespace Alunite.Resources
{
    /// <summary>
    /// A wrapper over a disposable resource given by type <see cref="T"/> whose ownership is shared across
    /// multiple users. The resource will be disposed once all users dispose their reference to it.
    /// </summary>
    public sealed class Shared<T> : ITransient, IDisposable
        where T : IDisposable
    {
        private int _references;
        private Shared(T target)
        {
            _references = 1;
            Target = target;
        }

        /// <summary>
        /// The resource this shared wrapper is for.
        /// </summary>
        public T Target { get; }

        /// <summary>
        /// If true, indicates that the underlying resource has not yet been disposed.
        /// </summary>
        public bool IsAlive => _references > 0;

        /// <summary>
        /// Attempts to add a usage to the shared resource, returning false if it has already been disposed. If
        /// this returns true, <see cref="Dispose"/> must be called one extra time.
        /// </summary>
        public bool TryUse()
        {
        retry:
            int cur = _references;
            if (cur > 0)
            {
                if (Interlocked.CompareExchange(ref _references, cur + 1, cur) != cur)
                    goto retry;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Creates a shared wrapper over the given un-disposed resource that is currently owned by
        /// the calling thread.
        /// </summary>
        public static Shared<T> Share(T resource)
        {
            return new Shared<T>(resource);
        }

        /// <summary>
        /// Removes a user from the shared resource, possibly disposing the underlying resource.
        /// </summary>
        public void Dispose()
        {
            int nRefs = Interlocked.Decrement(ref _references);
            if (nRefs <= 0)
            {
                if (nRefs < 0)
                    throw new InvalidOperationException("Shared wrapper disposed too many times");
                Target.Dispose();
            }
        }
    }

    /// <summary>
    /// Contains functions related to <see cref="Shared{T}"/>.
    /// </summary>
    public static class Shared
    {
        /// <summary>
        /// Creates a shared wrapper over the given un-disposed resource that is currently owned by
        /// the calling thread.
        /// </summary>
        public static Shared<T> Share<T>(T resource)
            where T : IDisposable
        {
            return Shared<T>.Share(resource);
        }
    }
}
