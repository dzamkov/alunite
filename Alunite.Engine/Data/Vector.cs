﻿using System;
using System.Diagnostics;

using Numerics = System.Numerics;

using Alunite.Data.Serialization;
using Alunite.Data.Probability;

namespace Alunite.Data
{
    /// <summary>
    /// A vector consisting of 2 real components.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct Vector2 : IEquatable<Vector2>, IComparable<Vector2>
    {
        internal Numerics.Vector2 _mean;
        internal Vector2(Numerics.Vector2 _mean)
        {
            this._mean = _mean;
        }
        
        public Vector2(Scalar x, Scalar y)
        {
            _mean = new Numerics.Vector2(x._mean, y._mean);
        }
        
        /// <summary>
        /// The zero vector
        /// </summary>
        public static Vector2 Zero => new Vector2(0, 0);
        
        public Scalar X
        {
            get { return _mean.X; }
            set { _mean.X = value._mean; }
        }
        
        public Scalar Y
        {
            get { return _mean.Y; }
            set { _mean.Y = value._mean; }
        }
        
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Scalar SqrLength
        {
            get
            {
                return new Scalar(_mean.LengthSquared());
            }
        }
        
        public Scalar Length
        {
            get
            {
                return new Scalar(_mean.Length());
            }
        }

        public Vector2 Cross
        {
            get
            {
                return new Vector2(-Y, X);
            }
        }

        public static Vector2 Normalize(Vector2 a)
        {
            return new Vector2(Numerics.Vector2.Normalize(a._mean));
        }

        public static Scalar Dot(Vector2 a, Vector2 b)
        {
            return new Scalar(Numerics.Vector2.Dot(a._mean, b._mean));
        }

        public static Vector2 Abs(Vector2 vec)
        {
            return new Vector2(Numerics.Vector2.Abs(vec._mean));
        }

        public static int Compare(Vector2 a, Vector2 b)
        {
            if (a.Y < b.Y)
                return -1;
            else if (a.Y > b.Y)
                return 1;
            else if (a.X < b.X)
                return -1;
            else if (a.X > b.X)
                return 1;
            else
                return 0;
        }

        /// <summary>
        /// Determines whether the given <see cref="Vector2"/>s likely have the same true value
        /// after compensating for representation error.
        /// </summary>
        public static bool AreLikelyEqual(Vector2 a, Vector2 b)
        {
            return AreEqualOdds(a, b) > 1;
        }

        /// <summary>
        /// Determines the relative probability that the given <see cref="Vector2"/>s have the same true value, over
        /// the probability that their true values are independent. This is based on the apriori distribution of
        /// vector values, and individual value distributions of <paramref name="a"/> and <paramref name="b"/>.
        /// </summary>
        public static LogScalar AreEqualOdds(Vector2 a, Vector2 b)
        {
            // See Scalar.AreEqualOdds for explanation
            float aSqrMean = Numerics.Vector2.Dot(a._mean, a._mean);
            float bSqrMean = Numerics.Vector2.Dot(b._mean, b._mean);
            float aVar = Scalar._beta + Scalar._gamma * aSqrMean;
            float bVar = Scalar._beta + Scalar._gamma * bSqrMean;
            float aPriorFactor = Scalar._alpha + aSqrMean;
            float bPriorFactor = Scalar._alpha + bSqrMean;
            Numerics.Vector2 diff = a._mean - b._mean;
            float sqrDiff = Numerics.Vector2.Dot(diff, diff);
            return new LogScalar(
                2f * (Scalar._lnNormalNorm - Scalar._lnPriorNorm)
                + sqrDiff / (-2f * (aVar + bVar))
                - Scalar._ln((aVar + bVar) / (aPriorFactor * bPriorFactor)) * (2f / 2f));
        }

        /// <summary>
        /// Contains potential encodings for <see cref="Vector2"/> values.
        /// </summary>
        public static class Encoding
        {
            /// <summary>
            /// An encoding for <see cref="Vector2"/>s where components are 32-bit little endian floats in XY order.
            /// </summary>
            [StandardEncoding]
            public static Encoding<Vector2> Xf32_Yf32 =>
                BitConverter.IsLittleEndian ? Xfn32_Yfn32 : throw new NotImplementedException();

            /// <summary>
            /// An encoding for <see cref="Vector2"/>s where components are 32-bit native endian floats in XY order.
            /// </summary>
            [DefaultEncoding]
            public static Encoding<Vector2> Xfn32_Yfn32 { get; } = new BlittableEncoding<Vector2>(8, 4);
        }

        public static Vector2 operator -(Vector2 a)
        {
            return new Vector2(-a.X, -a.Y);
        }
        
        public static Vector2 operator +(Vector2 a, Vector2 b)
        {
            return new Vector2(a.X + b.X, a.Y + b.Y);
        }
        
        public static Vector2 operator -(Vector2 a, Vector2 b)
        {
            return new Vector2(a.X - b.X, a.Y - b.Y);
        }
        
        public static Vector2 operator *(Vector2 a, Scalar b)
        {
            return new Vector2(a._mean * b._mean);
        }
        
        public static Vector2 operator *(Scalar a, Vector2 b)
        {
            return b * a;
        }
        
        public static Vector2 operator /(Vector2 a, Scalar b)
        {
            return a * (1.0 / b);
        }
        
        public static bool operator ==(Vector2 a, Vector2 b)
        {
            return a._mean == b._mean;
        }
        
        public static bool operator !=(Vector2 a, Vector2 b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Vector2))
                return false;
            return this == (Vector2)obj;
        }

        bool IEquatable<Vector2>.Equals(Vector2 other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return HashCodeHelper.Combine(X.GetHashCode(), Y.GetHashCode());
        }
        
        public override string ToString()
        {
            return $"({X}, {Y})";
        }

        int IComparable<Vector2>.CompareTo(Vector2 other)
        {
            return Compare(this, other);
        }
    }

    /// <summary>
    /// A vector consisting of 3 real components.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct Vector3 : IEquatable<Vector3>, IComparable<Vector3>
    {
        internal Numerics.Vector3 _mean;
        internal Vector3(Numerics.Vector3 mean)
        {
            _mean = mean;
        }
        
        public Vector3(Scalar x, Scalar y, Scalar z)
        {
            _mean = new Numerics.Vector3(x._mean, y._mean, z._mean);
        }
        
        public Vector3(Vector2 x_y, Scalar z)
            : this(x_y.X, x_y.Y, z)
        { }
        
        public Vector3(Scalar x, Vector2 y_z)
            : this(x, y_z.X, y_z.Y)
        { }
        
        /// <summary>
        /// The zero vector
        /// </summary>
        public static Vector3 Zero => new Vector3(0, 0, 0);
        
        public Scalar X
        {
            get { return _mean.X; }
            set { _mean.X = value._mean; }
        }
        
        public Scalar Y
        {
            get { return _mean.Y; }
            set { _mean.Y = value._mean; }
        }
        
        public Scalar Z
        {
            get { return _mean.Z; }
            set { _mean.Z = value._mean; }
        }
        
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Vector2 X_Y
        {
            get
            {
                return new Vector2(X, Y);
            }
            set
            {
                X = value.X;
                Y = value.Y;
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Vector2 Y_Z
        {
            get
            {
                return new Vector2(Y, Z);
            }
            set
            {
                Y = value.X;
                Z = value.Y;
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Vector2 X_Z
        {
            get
            {
                return new Vector2(X, Z);
            }
            set
            {
                X = value.X;
                Z = value.Y;
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Vector2 Z_X
        {
            get
            {
                return new Vector2(Z, X);
            }
            set
            {
                Z = value.X;
                X = value.Y;
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Vector3 Y_Z_X
        {
            get
            {
                return new Vector3(Y, Z, X);
            }
            set
            {
                Y = value.X;
                Z = value.Y;
                X = value.Z;
            }
        }
        
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Scalar SqrLength
        {
            get
            {
                return new Scalar(_mean.LengthSquared());
            }
        }
        
        public Scalar Length
        {
            get
            {
                return new Scalar(_mean.Length());
            }
        }

        public static Vector3 Normalize(Vector3 a)
        {
            return new Vector3(Numerics.Vector3.Normalize(a._mean));
        }
        
        public static Vector3 Multiply(Vector3 a, Vector3 b)
        {
            return new Vector3(Numerics.Vector3.Multiply(a._mean, b._mean));
        }
        
        public static Scalar Dot(Vector3 a, Vector3 b)
        {
            return new Scalar(Numerics.Vector3.Dot(a._mean, b._mean));
        }
        
        public static Vector3 Cross(Vector3 a, Vector3 b)
        {
            return new Vector3(Numerics.Vector3.Cross(a._mean, b._mean));
        }
        
        public static Vector3 Lerp(Vector3 x, Vector3 y, Scalar a)
        {
            return y * a + x * (1 - a);
        }

        /// <summary>
        /// Gets the vector representing the outgoing direction of light after hitting a reflective surface with
        /// the given normal.
        /// </summary>
        /// <param name="i">The incident vector specifying the incoming direction of light.</param>
        public static Vector3 Reflect(Vector3 i, Vector3 n)
        {
            return i - 2 * Dot(i, n) * n;
        }
        
        public static Vector3 Scale(Vector3 a, Vector3 b)
        {
            return new Vector3(Numerics.Vector3.Multiply(a._mean, b._mean));
        }
        
        public static Vector3 Inverse(Vector3 a)
        {
            return new Vector3(Numerics.Vector3.Divide(new Numerics.Vector3(1f, 1f, 1f), a._mean));
        }

        public static int Compare(Vector3 a, Vector3 b)
        {
            if (a.Z < b.Z)
                return -1;
            else if (a.Z > b.Z)
                return 1;
            else if (a.Y < b.Y)
                return -1;
            else if (a.Y > b.Y)
                return 1;
            else if (a.X < b.X)
                return -1;
            else if (a.X > b.X)
                return 1;
            else
                return 0;
        }

        /// <summary>
        /// Determines whether the given <see cref="Vector3"/>s likely have the same true value
        /// after compensating for representation error.
        /// </summary>
        public static bool AreLikelyEqual(Vector3 a, Vector3 b)
        {
            return AreEqualOdds(a, b) > 1;
        }

        /// <summary>
        /// Determines the relative probability that the given <see cref="Vector3"/>s have the same true value, over
        /// the probability that their true values are independent. This is based on the apriori distribution of
        /// vector values, and individual value distributions of <paramref name="a"/> and <paramref name="b"/>.
        /// </summary>
        public static LogScalar AreEqualOdds(Vector3 a, Vector3 b)
        {
            // See Scalar.AreEqualOdds for explanation
            float aSqrMean = Numerics.Vector3.Dot(a._mean, a._mean);
            float bSqrMean = Numerics.Vector3.Dot(b._mean, b._mean);
            float aVar = Scalar._beta + Scalar._gamma * aSqrMean;
            float bVar = Scalar._beta + Scalar._gamma * bSqrMean;
            float aPriorFactor = Scalar._alpha + aSqrMean;
            float bPriorFactor = Scalar._alpha + bSqrMean;
            Numerics.Vector3 diff = a._mean - b._mean;
            float sqrDiff = Numerics.Vector3.Dot(diff, diff);
            return new LogScalar(
                3f * (Scalar._lnNormalNorm - Scalar._lnPriorNorm)
                + sqrDiff / (-2f * (aVar + bVar))
                - Scalar._ln((aVar + bVar) / (aPriorFactor * bPriorFactor)) * (3f / 2f));
        }

        /// <summary>
        /// Contains potential encodings for <see cref="Vector3"/> values.
        /// </summary>
        public static class Encoding
        {
            /// <summary>
            /// An encoding for <see cref="Vector3"/>s where components are bytes in ZYX order and
            /// each component is scaled to the range [0, 1].
            /// </summary>
            public static Encoding<Vector3> Zu8_Yu8_Xu8 => _Zu8_Yu8_Xu8.Instance;
            
            /// <summary>
            /// An encoding for <see cref="Vector3"/>s where components are signed bytes in ZYX order and
            /// each component is scaled to the range [-1, 1].
            /// </summary>
            public static Encoding<Vector3> Zs8_Ys8_Xs8 => _Zs8_Ys8_Xs8.Instance;
            
            /// <summary>
            /// An encoding for <see cref="Vector3"/>s where components are 32-bit little endian floats in XYZ order.
            /// </summary>
            [StandardEncoding]
            public static Encoding<Vector3> Xf32_Yf32_Zf32 => 
                BitConverter.IsLittleEndian ? Xfn32_Yfn32_Zfn32 : throw new NotImplementedException();

            /// <summary>
            /// An encoding for <see cref="Vector3"/>s where components are 32-bit native endian floats in XYZ order.
            /// </summary>
            [DefaultEncoding]
            public static Encoding<Vector3> Xfn32_Yfn32_Zfn32 { get; } = new BlittableEncoding<Vector3>(12, 4);
            
            /// <summary>
            /// The implementation for <see cref="Zu8_Yu8_Xu8"/>.
            /// </summary>
            private sealed class _Zu8_Yu8_Xu8 : Encoding<Vector3>
            {
                public _Zu8_Yu8_Xu8() : base(3, 1) { }

                /// <summary>
                /// The only instance of this class.
                /// </summary>
                public static _Zu8_Yu8_Xu8 Instance { get; } = new _Zu8_Yu8_Xu8();

                public override ref byte FastRead(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out Vector3 value)
                {
                    return ref FastReadAligned(ref head, ref buffer, reader, out value);
                }

                public override ref byte FastReadAligned(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out Vector3 value)
                {
                    throw new NotImplementedException();
                }

                public override ref byte FastWrite(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in Vector3 value)
                {
                    return ref FastWriteAligned(ref head, ref buffer, writer, in value);
                }

                public override ref byte FastWriteAligned(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in Vector3 value)
                {
                    throw new NotImplementedException();
                }
            }

            /// <summary>
            /// The implementation for <see cref="Zs8_Ys8_Xs8"/>.
            /// </summary>
            private sealed class _Zs8_Ys8_Xs8 : Encoding<Vector3>
            {
                public _Zs8_Ys8_Xs8() : base(3, 1) { }

                /// <summary>
                /// The only instance of this class.
                /// </summary>
                public static _Zs8_Ys8_Xs8 Instance { get; } = new _Zs8_Ys8_Xs8();

                public override ref byte FastRead(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out Vector3 value)
                {
                    return ref FastReadAligned(ref head, ref buffer, reader, out value);
                }

                public override ref byte FastReadAligned(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out Vector3 value)
                {
                    throw new NotImplementedException();
                }

                public override ref byte FastWrite(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in Vector3 value)
                {
                    return ref FastWriteAligned(ref head, ref buffer, writer, in value);
                }

                public override ref byte FastWriteAligned(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in Vector3 value)
                {
                    throw new NotImplementedException();
                }
            }
        }
        
        public static Vector3 operator -(Vector3 a)
        {
            return new Vector3(Numerics.Vector3.Negate(a._mean));
        }
        
        public static Vector3 operator +(Vector3 a, Vector3 b)
        {
            return new Vector3(Numerics.Vector3.Add(a._mean, b._mean));
        }
        
        public static Vector3 operator -(Vector3 a, Vector3 b)
        {
            return new Vector3(Numerics.Vector3.Subtract(a._mean, b._mean));
        }
        
        public static Vector3 operator *(Vector3 a, Scalar b)
        {
            return new Vector3(Numerics.Vector3.Multiply(a._mean, b._mean));
        }
        
        public static Vector3 operator *(Scalar a, Vector3 b)
        {
            return b * a;
        }
        
        public static Vector3 operator /(Vector3 a, Scalar b)
        {
            return a * (1.0 / b);
        }
        
        public static bool operator ==(Vector3 a, Vector3 b)
        {
            return a.X == b.X && a.Y == b.Y && a.Z == b.Z;
        }
        
        public static bool operator !=(Vector3 a, Vector3 b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Vector3))
                return false;
            return this == (Vector3)obj;
        }

        bool IEquatable<Vector3>.Equals(Vector3 other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return HashCodeHelper.Combine(X.GetHashCode(), Y.GetHashCode(), Z.GetHashCode());
        }

        public override string ToString()
        {
            return $"({X}, {Y}, {Z})";
        }

        int IComparable<Vector3>.CompareTo(Vector3 other)
        {
            return Compare(this, other);
        }
    }

    /// <summary>
    /// A vector consisting of 4 real components.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct Vector4
    {
        internal Numerics.Vector4 _mean;
        internal Vector4(Numerics.Vector4 mean)
        {
            _mean = mean;
        }
        
        public Vector4(Scalar x, Scalar y, Scalar z, Scalar w)
        {
            _mean = new Numerics.Vector4(x._mean, y._mean, z._mean, w._mean);
        }

        public Vector4(Vector2 x_y, Scalar z, Scalar w)
            : this(x_y.X, x_y.Y, z, w)
        { }
        
        public Vector4(Vector3 x_y_z, Scalar w)
            : this(x_y_z.X, x_y_z.Y, x_y_z.Z, w)
        { }
        
        /// <summary>
        /// The zero vector.
        /// </summary>
        public static Vector4 Zero => new Vector4(0, 0, 0, 0);
        
        public Scalar X
        {
            get { return _mean.X; }
            set { _mean.X = value._mean; }
        }
        
        public Scalar Y
        {
            get { return _mean.Y; }
            set { _mean.Y = value._mean; }
        }
        
        public Scalar Z
        {
            get { return _mean.Z; }
            set { _mean.Z = value._mean; }
        }
        
        public Scalar W
        {
            get { return _mean.W; }
            set { _mean.W = value._mean; }
        }

        public Vector2 X_Y
        {
            get { return new Vector2(X, Y); }
            set { X = value.X; Y = value.Y; }
        }

        public Vector3 X_Y_Z
        {
            get { return new Vector3(X, Y, Z); }
            set { X = value.X; Y = value.Y; Z = value.Z; }
        }
        
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Scalar SqrLength
        {
            get
            {
                return new Scalar(_mean.LengthSquared());
            }
        }
        
        public Scalar Length
        {
            get
            {
                return new Scalar(_mean.Length());
            }
        }

        public static Vector4 Normalize(Vector4 a)
        {
            return new Vector4(Numerics.Vector4.Normalize(a._mean));
        }

        public static Scalar Dot(Vector4 a, Vector4 b)
        {
            return new Scalar(Numerics.Vector4.Dot(a._mean, b._mean));
        }

        /// <summary>
        /// Determines whether the given <see cref="Vector4"/>s likely have the same true value
        /// after compensating for representation error.
        /// </summary>
        public static bool AreLikelyEqual(Vector4 a, Vector4 b)
        {
            return AreEqualOdds(a, b) > 1;
        }

        /// <summary>
        /// Determines the relative probability that the given <see cref="Vector4"/>s have the same true value, over
        /// the probability that their true values are independent. This is based on the apriori distribution of
        /// vector values, and individual value distributions of <paramref name="a"/> and <paramref name="b"/>.
        /// </summary>
        public static LogScalar AreEqualOdds(Vector4 a, Vector4 b)
        {
            // See Scalar.AreEqualOdds for explanation
            float aSqrMean = Numerics.Vector4.Dot(a._mean, a._mean);
            float bSqrMean = Numerics.Vector4.Dot(b._mean, b._mean);
            float aVar = Scalar._beta + Scalar._gamma * aSqrMean;
            float bVar = Scalar._beta + Scalar._gamma * bSqrMean;
            float aPriorFactor = Scalar._alpha + aSqrMean;
            float bPriorFactor = Scalar._alpha + bSqrMean;
            Numerics.Vector4 diff = a._mean - b._mean;
            float sqrDiff = Numerics.Vector4.Dot(diff, diff);
            return new LogScalar(
                4f * (Scalar._lnNormalNorm - Scalar._lnPriorNorm)
                + sqrDiff / (-2f * (aVar + bVar))
                - Scalar._ln((aVar + bVar) / (aPriorFactor * bPriorFactor)) * (4f / 2f));
        }

        /// <summary>
        /// Contains potential encodings for <see cref="Vector4"/> values.
        /// </summary>
        public static class Encoding
        {
            /// <summary>
            /// An encoding for <see cref="Vector4"/>s where components are 32-bit little endian floats in XYZW order.
            /// </summary>
            [StandardEncoding]
            public static Encoding<Vector4> Xf32_Yf32_Zf32_Wf32 =>
                BitConverter.IsLittleEndian ? Xfn32_Yfn32_Zfn32_Wfn32 : throw new NotImplementedException();

            /// <summary>
            /// An encoding for <see cref="Vector4"/>s where components are 32-bit native endian floats in XYZW order.
            /// </summary>
            [DefaultEncoding]
            public static Encoding<Vector4> Xfn32_Yfn32_Zfn32_Wfn32 { get; } = new BlittableEncoding<Vector4>(16, 4);
        }

        public static Vector4 operator -(Vector4 a)
        {
            return new Vector4(-a.X, -a.Y, -a.Z, -a.W);
        }
        
        public static Vector4 operator +(Vector4 a, Vector4 b)
        {
            return new Vector4(a.X + b.X, a.Y + b.Y, a.Z + b.Z, a.W + b.W);
        }
        
        public static Vector4 operator -(Vector4 a, Vector4 b)
        {
            return new Vector4(a.X - b.X, a.Y - b.Y, a.Z + b.Z, a.W - b.W);
        }
        
        public static Vector4 operator *(Vector4 a, Scalar b)
        {
            return new Vector4(a._mean * b._mean);
        }
        
        public static Vector4 operator *(Scalar a, Vector4 b)
        {
            return b * a;
        }
        
        public static Vector4 operator /(Vector4 a, Scalar b)
        {
            return a * (1.0 / b);
        }

        public override string ToString()
        {
            return $"({X}, {Y}, {Z}, {W})";
        }
    }

    /// <summary>
    /// A vector consisting of 2 integral components.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct Vector2i : IEquatable<Vector2i>
    {
        public int X;
        public int Y;
        public Vector2i(int x, int y)
        {
            X = x;
            Y = y;
        }

        /// <summary>
        /// The zero vector.
        /// </summary>
        public static Vector2i Zero => new Vector2i(0, 0);
        
        public int SqrLength
        {
            get
            {
                return X * X + Y * Y;
            }
        }

        public static Vector2i Floor(Vector2 a)
        {
            return new Vector2i(Scalar.Floor(a.X), Scalar.Floor(a.Y));
        }

        public static int Compare(Vector2i a, Vector2i b)
        {
            if (a.Y < b.Y)
                return -1;
            else if (a.Y > b.Y)
                return 1;
            else if (a.X < b.X)
                return -1;
            else if (a.X > b.X)
                return 1;
            else
                return 0;
        }

        public static Vector2i operator -(Vector2i a)
        {
            return new Vector2i(-a.X, -a.Y);
        }

        public static Vector2i operator +(Vector2i a, Vector2i b)
        {
            return new Vector2i(a.X + b.X, a.Y + b.Y);
        }

        public static Vector2i operator -(Vector2i a, Vector2i b)
        {
            return new Vector2i(a.X - b.X, a.Y - b.Y);
        }

        public static implicit operator Vector2(Vector2i source)
        {
            return new Vector2(source.X, source.Y);
        }

        public static implicit operator Vector2i(Geometry.Dir2 dir)
        {
            return _dirs[(int)dir];
        }

        /// <summary>
        /// The table used to implement the conversion from <see cref="Dir2"/>.
        /// </summary>
        private static Vector2i[] _dirs = new Vector2i[]
        {
            new Vector2i(-1, 0),
            new Vector2i(1, 0),
            new Vector2i(0, -1),
            new Vector2i(0, 1)
        };

        public static bool operator ==(Vector2i a, Vector2i b)
        {
            return a.X == b.X && a.Y == b.Y;
        }

        public static bool operator !=(Vector2i a, Vector2i b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Vector2i))
                return false;
            return this == (Vector2i)obj;
        }

        public override int GetHashCode()
        {
            return HashCodeHelper.Combine(X.GetHashCode(), Y.GetHashCode());
        }

        bool IEquatable<Vector2i>.Equals(Vector2i other)
        {
            return this == other;
        }

        public override string ToString()
        {
            return $"({X}, {Y})";
        }
    }

    /// <summary>
    /// A vector consisting of 3 integral components.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct Vector3i : IEquatable<Vector3i>, IComparable<Vector3i>
    {
        public int X;
        public int Y;
        public int Z;
        public Vector3i(int x, int y, int z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public Vector3i(Vector2i x_y, int z)
            : this(x_y.X, x_y.Y, z)
        { }

        public Vector3i(int x, Vector2i y_z)
            : this(x, y_z.X, y_z.Y)
        { }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Vector2i X_Y
        {
            get
            {
                return new Vector2i(X, Y);
            }
            set
            {
                X = value.X;
                Y = value.Y;
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Vector2i X_Z
        {
            get
            {
                return new Vector2i(X, Z);
            }
            set
            {
                X = value.X;
                Z = value.Y;
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Vector2i Y_Z
        {
            get
            {
                return new Vector2i(Y, Z);
            }
            set
            {
                Y = value.X;
                Z = value.Y;
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Vector2i Z_X
        {
            get
            {
                return new Vector2i(Z, X);
            }
            set
            {
                Z = value.X;
                X = value.Y;
            }
        }

        /// <summary>
        /// The zero vector.
        /// </summary>
        public static Vector3i Zero => new Vector3i(0, 0, 0);

        public static Vector3i Floor(Vector3 a)
        {
            return new Vector3i(Scalar.Floor(a.X), Scalar.Floor(a.Y), Scalar.Floor(a.Z));
        }

        public static int Compare(Vector3i a, Vector3i b)
        {
            if (a.Z < b.Z)
                return -1;
            else if (a.Z > b.Z)
                return 1;
            else if (a.Y < b.Y)
                return -1;
            else if (a.Y > b.Y)
                return 1;
            else if (a.X < b.X)
                return -1;
            else if (a.X > b.X)
                return 1;
            else
                return 0;
        }

        public static Vector3i operator -(Vector3i a)
        {
            return new Vector3i(-a.X, -a.Y, -a.Z);
        }

        public static Vector3i operator +(Vector3i a, Vector3i b)
        {
            return new Vector3i(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        }

        public static Vector3i operator -(Vector3i a, Vector3i b)
        {
            return new Vector3i(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        }

        public static Vector3i operator *(Vector3i a, int b)
        {
            return new Vector3i(a.X * b, a.Y * b, a.Z * b);
        }

        public static Vector3i operator *(int a, Vector3i b)
        {
            return new Vector3i(a * b.X, a * b.Y, a * b.Z);
        }

        public static implicit operator Vector3(Vector3i source)
        {
            return new Vector3(source.X, source.Y, source.Z);
        }

        public static implicit operator Vector3i(Geometry.Dir3 dir)
        {
            return _dirs[(int)dir];
        }

        /// <summary>
        /// The table used to implement the conversion from <see cref="Dir3"/>.
        /// </summary>
        private static Vector3i[] _dirs = new Vector3i[]
        {
            new Vector3i(-1, 0, 0),
            new Vector3i(1, 0, 0),
            new Vector3i(0, -1, 0),
            new Vector3i(0, 1, 0),
            new Vector3i(0, 0, -1),
            new Vector3i(0, 0, 1)
        };

        public static bool operator ==(Vector3i a, Vector3i b)
        {
            return a.X == b.X & a.Y == b.Y & a.Z == b.Z;
        }

        public static bool operator !=(Vector3i a, Vector3i b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Vector3i))
                return false;
            return this == (Vector3i)obj;
        }

        public override int GetHashCode()
        {
            return HashCodeHelper.Combine(
                X.GetHashCode(),
                Y.GetHashCode(),
                Z.GetHashCode());
        }

        bool IEquatable<Vector3i>.Equals(Vector3i other)
        {
            return this == other;
        }

        public override string ToString()
        {
            return $"({X}, {Y}, {Z})";
        }

        int IComparable<Vector3i>.CompareTo(Vector3i other)
        {
            return Compare(this, other);
        }
    }

    /// <summary>
    /// A <see cref="Vector3i"/> where each component is a 2-byte signed integer.
    /// </summary>
    public struct ShortVector3i : IEquatable<ShortVector3i>
    {
        public short X;
        public short Y;
        public short Z;
        public ShortVector3i(short x, short y, short z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        /// <summary>
        /// The zero vector.
        /// </summary>
        public static ShortVector3i Zero => new ShortVector3i(0, 0, 0);

        public static ShortVector3i operator -(ShortVector3i a)
        {
            return new ShortVector3i((short)-a.X, (short)-a.Y, (short)-a.Z);
        }

        public static ShortVector3i operator +(ShortVector3i a, ShortVector3i b)
        {
            return new ShortVector3i((short)(a.X + b.X), (short)(a.Y + b.Y), (short)(a.Z + b.Z));
        }

        public static ShortVector3i operator -(ShortVector3i a, ShortVector3i b)
        {
            return new ShortVector3i((short)(a.X - b.X), (short)(a.Y - b.Y), (short)(a.Z - b.Z));
        }

        public static implicit operator Vector3i(ShortVector3i vec)
        {
            return new Vector3i(vec.X, vec.Y, vec.Z);
        }

        public static explicit operator ShortVector3i(Vector3i vec)
        {
            return new ShortVector3i((short)vec.X, (short)vec.Y, (short)vec.Z);
        }

        public static implicit operator ShortVector3i(Geometry.Dir3 dir)
        {
            return _dirs[(int)dir];
        }

        /// <summary>
        /// The table used to implement the conversion from <see cref="Dir3"/>.
        /// </summary>
        private static ShortVector3i[] _dirs = new ShortVector3i[]
        {
            new ShortVector3i(-1, 0, 0),
            new ShortVector3i(1, 0, 0),
            new ShortVector3i(0, -1, 0),
            new ShortVector3i(0, 1, 0),
            new ShortVector3i(0, 0, -1),
            new ShortVector3i(0, 0, 1)
        };

        public static bool operator ==(ShortVector3i a, ShortVector3i b)
        {
            return a.X == b.X & a.Y == b.Y & a.Z == b.Z;
        }

        public static bool operator !=(ShortVector3i a, ShortVector3i b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is ShortVector3i))
                return false;
            return this == (ShortVector3i)obj;
        }

        public override int GetHashCode()
        {
            return HashCodeHelper.Combine(
                X.GetHashCode(),
                Y.GetHashCode(),
                Z.GetHashCode());
        }

        bool IEquatable<ShortVector3i>.Equals(ShortVector3i other)
        {
            return this == other;
        }

        public override string ToString()
        {
            return $"({X}, {Y}, {Z})";
        }
    }
}