﻿using System;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;

namespace Alunite.Data
{
    /// <summary>
    /// A finite unordered collection of values of type <typeparamref name="T"/>.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    [DebuggerTypeProxy(typeof(EnumerableDebugView<>))]
    public struct Bag<T> : IEquatable<Bag<T>>, IEnumerable<T>
    {
        internal T[] _items;
        internal Bag(T[] items)
        {
            _items = items;
        }

        /// <summary>
        /// The empty <see cref="Bag{T}"/>.
        /// </summary>
        public static Bag<T> Empty => new Bag<T>(Array.Empty<T>());

        /// <summary>
        /// Indicates whether this is the empty bag.
        /// </summary>
        public bool IsEmpty => _items.Length == 0;

        /// <summary>
        /// Determines whether this bag consists of a single member, and if so, gets its value.
        /// </summary>
        public bool IsSingleton(out T value)
        {
            if (_items.Length == 1)
            {
                value = _items[0];
                return true;
            }
            value = default;
            return false;
        }

        /// <summary>
        /// The number of members in this bag, including duplicates.
        /// </summary>
        public uint Size => (uint)_items.Length;

        /// <summary>
        /// Constructs a list consisting of the members of this bag in some arbitrary order. Note that this doesn't
        /// even guarantee that duplicates will occur consecutively.
        /// </summary>
        public List<T> SortArbitrary()
        {
            return new List<T>(_items);
        }

        /// <summary>
        /// Creates an <see cref="IIterator{T}"/> to iterate over the items in this bag.
        /// </summary>
        public Iterator Iterate()
        {
            return new Iterator
            {
                _items = _items,
                _index = 0
            };
        }

        /// <summary>
        /// An <see cref="IIterator{T}"/> for a <see cref="Bag{T}"/>.
        /// </summary>
        public struct Iterator : IIterator<T>
        {
            internal T[] _items;
            internal uint _index;

            /// <summary>
            /// Attempts to get the next item from the iterator, returning false if the end of the collection
            /// has been reached.
            /// </summary>
            public bool TryNext(out T value)
            {
                if (_index < (uint)_items.Length)
                {
                    value = _items[_index++];
                    return true;
                }
                value = default;
                return false;
            }

            void IDisposable.Dispose()
            {
                // Nothing to do here
            }
        }

        /// <summary>
        /// Gets an enumerator for this bag.
        /// </summary>
        public IteratorEnumerator<Iterator, T> GetEnumerator()
        {
            return new IteratorEnumerator<Iterator, T>(Iterate());
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public static bool operator ==(Bag<T> a, Bag<T> b)
        {
            throw new NotImplementedException();
        }

        public static bool operator !=(Bag<T> a, Bag<T> b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Bag<T>))
                return false;
            return this == (Bag<T>)obj;
        }

        bool IEquatable<Bag<T>>.Equals(Bag<T> other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.WriteItem('{');
            bool isFirst = true;
            foreach (var item in this)
            {
                if (isFirst)
                    isFirst = false;
                else
                    str.Write(", ");
                str.Write(item.ToString());
            }
            str.WriteItem('}');
            return str.ToString();
        }
    }

    /// <summary>
    /// Contains functions for constructing and manipulating <see cref="Bag{T}"/>s.
    /// </summary>
    public static class Bag
    {
        /// <summary>
        /// Gets the empty bag of the given type.
        /// </summary>
        public static Bag<T> Empty<T>()
        {
            return Bag<T>.Empty;
        }
        /// <summary>
        /// Constructs a bag consisting of the given items.
        /// </summary>
        public static Bag<T> Of<T>(params T[] items)
        {
            return Of((Span<T>)items);
        }

        /// <summary>
        /// Constructs a bag consisting of the given items.
        /// </summary>
        public static Bag<T> Of<T>(Span<T> items)
        {
            return Of((ReadOnlySpan<T>)items);
        }

        /// <summary>
        /// Constructs a bag consisting of the given items.
        /// </summary>
        public static Bag<T> Of<T>(ReadOnlySpan<T> items)
        {
            if (items.Length == 0)
                return Empty<T>();
            T[] nItems = new T[items.Length];
            for (int i = 0; i < items.Length; i++)
                nItems[i] = items[i];
            return new Bag<T>(nItems);
        }
    }

    /// <summary>
    /// An interface for inspecting the contents of a <see cref="Bag{T}"/>.
    /// </summary>
    public interface IBagViewer<T>
    {
        /// <summary>
        /// Creates an <see cref="IIterator{T}"/> to iterate over the items in this bag. No other
        /// methods may be called until the iterator is disposed.
        /// </summary>
        IIterator<T> Iterate();
    }

    /// <summary>
    /// An interface for inspecting and adding to the contents of a <see cref="Bag{T}"/>. This does not allow removal
    /// or modification of existing items.
    /// </summary>
    public interface IBagBrowser<T> : IBagViewer<T>
    {
        /// <summary>
        /// Adds an item to the bag.
        /// </summary>
        void Add(T item);
    }

    /// <summary>
    /// An interface for inspecting and modifying the contents of a <see cref="Bag{T}"/>.
    /// </summary>
    public interface IBagEditor<T> : IBagBrowser<T>
    {
        /// <summary>
        /// Creates an <see cref="IFilterIterator{T}"/> to iterate over the items in the bag, with the ability
        /// to selectively remove them. No other methods may be called until the iterator is disposed.
        /// </summary>
        IFilterIterator<T> FilterIterate();
    }

    /// <summary>
    /// Contains helper functions related to <see cref="IBagEditor{T}"/>.
    /// </summary>
    public static class BagEditor
    {
        /// <summary>
        /// Removes all items from this bag.
        /// </summary>
        public static void Clear<T>(this IBagEditor<T> editor)
        {
            using (var it = editor.FilterIterate())
            {
                while (it.TryNext(out _))
                    it.Remove();
            }
        }
    }

    /// <summary>
    /// A helper class for constructing a <see cref="Bag{T}"/> by incrementally adding items.
    /// </summary>
    [DebuggerTypeProxy(typeof(EnumerableDebugView<>))]
    public sealed class BagBuilder<T> : IBagEditor<T>, IBuilder<Bag<T>>, IEnumerable<T>
    {
        internal T[] _buffer;
        internal uint _size;
        private BagBuilder()
        {
            _buffer = Array.Empty<T>();
            _size = 0;
        }

        /// <summary>
        /// The current size of the bag under construction.
        /// </summary>
        public uint Size => _size;

        /// <summary>
        /// Creates a new initially-empty <see cref="BagBuilder{T}"/>.
        /// </summary>
        public static BagBuilder<T> CreateEmpty()
        {
            return new BagBuilder<T>();
        }
        
        /// <summary>
        /// Adds an item to this bag.
        /// </summary>
        public void Add(T item)
        {
            uint index = _size++;
            Table.Allocate(ref _buffer, index, 1);
            _buffer[index] = item;
        }
        
        /// <summary>
        /// Removes all items from this bag.
        /// </summary>
        public void Clear()
        {
            _size = 0;
        }
        
        /// <summary>
        /// Gets the <see cref="Bag{T}"/> resulting from this builder.
        /// </summary>
        public Bag<T> Finish()
        {
            if (_size == _buffer.Length)
                return new Bag<T>(_buffer);
            T[] array = new T[_size];
            Array.Copy(_buffer, array, (int)_size);
            return new Bag<T>(array);
        }

        /// <summary>
        /// Creates an <see cref="IIterator{T}"/> to iterate over the items in the bag, with the ability
        /// to selectively remove them. No other methods may be called while the iterator is active.
        /// </summary>
        public Iterator Iterate()
        {
            return new Iterator
            {
                _bag = this,
                _index = 0
            };
        }

        /// <summary>
        /// An <see cref="IFilterIterator{T}"/> for a <see cref="Bag{T}"/>.
        /// </summary>
        public struct Iterator : IFilterIterator<T>
        {
            internal BagBuilder<T> _bag;
            internal uint _index;

            /// <summary>
            /// Attempts to get the next item from the iterator, returning false if the end of the collection
            /// has been reached.
            /// </summary>
            public bool TryNext(out T value)
            {
                if (_index < _bag._size)
                {
                    value = _bag._buffer[_index++];
                    return true;
                }
                value = default;
                return false;
            }

            /// <summary>
            /// Removes the current item from the collection. This may only be called if the previous call to
            /// <see cref="IIterator{T}.TryNext(out T)"/> returned true. This may be called at most once
            /// per item.
            /// </summary>
            public void Remove()
            {
                _bag._buffer[--_index] = _bag._buffer[--_bag._size];
            }

            void IDisposable.Dispose()
            {
                // Nothing to do here
            }
        }

        /// <summary>
        /// Gets an enumerator for this bag.
        /// </summary>
        public IteratorEnumerator<Iterator, T> GetEnumerator()
        {
            return new IteratorEnumerator<Iterator, T>(Iterate());
        }

        IIterator<T> IBagViewer<T>.Iterate()
        {
            return Iterate();
        }

        IFilterIterator<T> IBagEditor<T>.FilterIterate()
        {
            return Iterate();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
