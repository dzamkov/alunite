﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Alunite.Data.Geometry
{
    /// <summary>
    /// A three-dimensional grid defined by an <see cref="Octree{TLeaf}"/>.
    /// </summary>
    public struct OctreeGrid<T> : IGrid3<T>
    {
        public OctreeGrid(OctreeDictionary<T> dict, OctreeDictionary.Octree octree, uint scale)
        {
            Octree = new Octree<T>(dict, octree);
            Scale = scale;
        }

        public OctreeGrid(Octree<T> octree, uint scale)
        {
            Octree = octree;
            Scale = scale;
        }
        
        /// <summary>
        /// The <see cref="Octree{TLeaf}"/> defining this grid.
        /// </summary>
        public Octree<T> Octree { get; }

        /// <summary>
        /// The scale of this grid, that is, the log base-2 of its length on any axis.
        /// </summary>
        public uint Scale { get; }

        /// <summary>
        /// The length of this grid on any axis.
        /// </summary>
        public uint Length => 1u << (int)Scale;

        /// <summary>
        /// The size of this grid.
        /// </summary>
        public Vector3i Size
        {
            get
            {
                int len = (int)Length;
                return new Vector3i(len, len, len);
            }
        }

        /// <summary>
        /// Gets a cell in this grid.
        /// </summary>
        public T this[ShortVector3i cell]
        {
            get
            {
                return Octree.Dictionary.GetLeaf(Octree.Id, Scale, cell);
            }
        }

        /// <summary>
        /// Constructs an <see cref="OctreeGrid{T}"/> copy of the given source grid. The size of the octree
        /// grid will be extended to the next higher power of two. When this happens, the extra space will be
        /// filled with <paramref name="overflow"/>
        /// </summary>
        public static OctreeGrid<T> Create<TGrid>(IEqualityComparer<T> comparer, T overflow, TGrid source)
            where TGrid : IGrid3<T>
        {
            OctreeFactory<T> octrees = new OctreeFactory<T>(comparer);
            Vector3i sourceSize = source.Size;
            uint size = Bitwise.UpperPow2((uint)Math.Max(Math.Max(sourceSize.X, sourceSize.Y), sourceSize.Z));
            uint scale = Bitwise.Log2(size);
            return new OctreeGrid<T>(octrees, octrees.Build(source, size, Vector3i.Zero), scale);
        }

        /// <summary>
        /// Gets the list of cells that meet the given criteria, in sorted order.
        /// </summary>
        public OctreeGrid.SearchResult<TN> Search<TN>(FuncPredicate<T, TN> pred)
        {
            uint numLeaves = (uint)Octree.Dictionary._leaves.Length;
            if (Octree.Dictionary is OctreeFactory<T>)
                numLeaves = ((OctreeFactory<T>)Octree.Dictionary)._nextLeafIndex;
            TN[] mappedLeaves = new TN[numLeaves];
            BitArray leaves = new BitArray((int)numLeaves);
            for (int i = 0; i < numLeaves; i++)
                leaves[i] = pred(Octree.Dictionary._leaves[i], out mappedLeaves[i]);
            var items = new OctreeGrid._Searcher<T>(Octree.Dictionary, leaves).Search(Octree.Id, (ushort)Length);
            return new OctreeGrid.SearchResult<TN>(items, mappedLeaves);
        }

        /// <summary>
        /// Creates a <see cref="OctreeBuilder{TLeaf}"/> that is initially set to this octree grid. This allows a modified
        /// copy of the grid to be created.
        /// </summary>
        public OctreeGridBuilder<T> Edit()
        {
            return new OctreeGridBuilder<T>(Octree.Edit(), Scale);
        }
        
        T IGrid3<T>.this[Vector3i cell] => this[(ShortVector3i)cell];
    }

    /// <summary>
    /// A helper class for constructing and manipulating <see cref="OctreeGrid{T}"/>s through incremental modifications.
    /// </summary>
    public struct OctreeGridBuilder<T> : IGrid3<T>, IBuilder<OctreeGrid<T>>
    {
        public OctreeGridBuilder(OctreeBuilder<T> octree, uint scale)
        {
            Octree = octree;
            Scale = scale;
        }

        /// <summary>
        /// The <see cref="OctreeBuilder{TLeaf}"/> defining this grid.
        /// </summary>
        public OctreeBuilder<T> Octree { get; }

        /// <summary>
        /// The scale of this grid, that is, the log base-2 of its length on any axis.
        /// </summary>
        public uint Scale { get; }

        /// <summary>
        /// The length of the resulting grid on any axis.
        /// </summary>
        public uint Length => 1u << (int)Scale;

        /// <summary>
        /// The size of this grid.
        /// </summary>
        public Vector3i Size
        {
            get
            {
                uint len = Length;
                return new Vector3i((int)len, (int)len, (int)len);
            }
        }

        /// <summary>
        /// Gets or sets a cell in this grid.
        /// </summary>
        public T this[ShortVector3i cell]
        {
            get
            {
                return Octree.GetLeaf(Scale, cell);
            }
            set
            {
                Octree.SetLeaf(Scale, cell, value);
            }
        }

        /// <summary>
        /// Gets the <see cref="OctreeGrid{T}"/> resulting from this builder.
        /// </summary>
        public OctreeGrid<T> Finish()
        {
            return new OctreeGrid<T>(Octree.Finish(), Scale);
        }
        
        T IGrid3<T>.this[Vector3i cell] => this[(ShortVector3i)cell];
    }

    /// <summary>
    /// Contains types and helper functions related to <see cref="OctreeGrid{T}"/>.
    /// </summary>
    public static class OctreeGrid
    {
        /// <summary>
        /// An <see cref="IEnumerable{T}"/> providing the results of <see cref="OctreeGrid{T}.Search{TN}(FuncPredicate{T, TN})"/>.
        /// </summary>
        public struct SearchResult<T> : IEnumerable<Position3_Cell<T>>
        {
            private _Offset_Cell[] _items;
            private T[] _mappedLeaves;
            internal SearchResult(_Offset_Cell[] items, T[] mappedLeaves)
            {
                _items = items;
                _mappedLeaves = mappedLeaves;
            }

            /// <summary>
            /// The number of items in the results list.
            /// </summary>
            public uint Count => (uint)_items.Length;

            /// <summary>
            /// Returns an enumerator for this list.
            /// </summary>
            public SearchEnumerator<T> GetEnumerator()
            {
                return new SearchEnumerator<T>(_items, _mappedLeaves);
            }

            IEnumerator<Position3_Cell<T>> IEnumerable<Position3_Cell<T>>.GetEnumerator()
            {
                return GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }

        /// <summary>
        /// An <see cref="IEnumerator{T}"/> for a <see cref="SearchResult{TN}"/>.
        /// </summary>
        public struct SearchEnumerator<T> : IEnumerator<Position3_Cell<T>>
        {
            private _Offset_Cell[] _items;
            private T[] _mappedLeaves;
            private uint _index;
            internal SearchEnumerator(_Offset_Cell[] items, T[] mappedLeaves)
            {
                _items = items;
                _mappedLeaves = mappedLeaves;
                _index = uint.MaxValue;
            }

            /// <summary>
            /// Gets the element for the current position of the enumerator.
            /// </summary>
            public Position3_Cell<T> Current
            {
                get
                {
                    var item = _items[_index];
                    return new Position3_Cell<T>(item.Offset, _mappedLeaves[item.LeafIndex]);
                }
            }

            /// <summary>
            /// Advances the enumerator to the next/first item or returns false if the end of the list has
            /// been reached.
            /// </summary>
            public bool MoveNext()
            {
                unchecked { _index++; }
                return _index < _items.Length;
            }

            /// <summary>
            /// Indicates that this enumerator will no longer be used.
            /// </summary>
            public void Dispose()
            {
                // Nothing to do here
            }

            void IEnumerator.Reset()
            {
                _index = uint.MaxValue;
            }

            object IEnumerator.Current => Current;
        }

        /// <summary>
        /// A helper for implementing <see cref="OctreeGrid{T}.Search"/>
        /// </summary>
        internal struct _Searcher<T>
        {
            private TempCache<OctreeDictionary.Octree_Size, _Offset_Cell[]> _cache;
            public _Searcher(OctreeDictionary<T> octrees, BitArray leaves)
            {
                Octrees = octrees;
                Leaves = leaves;
                _cache = new TempCache<OctreeDictionary.Octree_Size, _Offset_Cell[]>(128);
            }

            /// <summary>
            /// The <see cref="OctreeDictionary{TLeaf}"/> in which the octrees to be searched are defined.
            /// </summary>
            public OctreeDictionary<T> Octrees { get; }

            /// <summary>
            /// A bit array representing the set of leaves of <see cref="Octrees"/> which are included in the search.
            /// </summary>
            public BitArray Leaves { get; }

            /// <summary>
            /// Searches for included leaves in the given octree node of the given size.
            /// </summary>
            public _Offset_Cell[] Search(OctreeDictionary.Octree octree, ushort size)
            {
                if (size == 1 && Octrees._isLeaf(octree, out uint leafIndex))
                {
                    if (Leaves[(int)leafIndex])
                        return new[] { _Offset_Cell.AtZero(leafIndex) };
                    else
                        return Array.Empty<_Offset_Cell>();
                }
                else
                {
                    var key = new OctreeDictionary.Octree_Size(octree, size);
                    if (!_cache.TryGet(key, out _Offset_Cell[] res))
                    {
                        // Perform search on octets
                        ushort hsize = (ushort)(size >> 1);
                        var octet = Octrees.AsGroup(octree);
                        var partials = new Octet<_Offset_Cell[]>(
                            Search(octet.NegX_NegY_NegZ, hsize),
                            Search(octet.PosX_NegY_NegZ, hsize),
                            Search(octet.NegX_PosY_NegZ, hsize),
                            Search(octet.PosX_PosY_NegZ, hsize),
                            Search(octet.NegX_NegY_PosZ, hsize),
                            Search(octet.PosX_NegY_PosZ, hsize),
                            Search(octet.NegX_PosY_PosZ, hsize),
                            Search(octet.PosX_PosY_PosZ, hsize));

                        // Merge partial results into one array
                        int negLength =
                            partials.NegX_NegY_NegZ.Length +
                            partials.PosX_NegY_NegZ.Length +
                            partials.NegX_PosY_NegZ.Length +
                            partials.PosX_PosY_NegZ.Length;
                        int posLength =
                            partials.NegX_NegY_PosZ.Length +
                            partials.PosX_NegY_PosZ.Length +
                            partials.NegX_PosY_PosZ.Length +
                            partials.PosX_PosY_PosZ.Length;
                        int resLength = negLength + posLength;
                        if (resLength > 0)
                        {
                            res = new _Offset_Cell[resLength];
                            _merge(new Span<_Offset_Cell>(res, 0, negLength),
                                partials.NegX_NegY_NegZ, new Offset(0, 0, 0),
                                partials.PosX_NegY_NegZ, new Offset(hsize, 0, 0),
                                partials.NegX_PosY_NegZ, new Offset(0, hsize, 0),
                                partials.PosX_PosY_NegZ, new Offset(hsize, hsize, 0));
                            _merge(new Span<_Offset_Cell>(res, negLength, posLength),
                                partials.NegX_NegY_PosZ, new Offset(0, 0, hsize),
                                partials.PosX_NegY_PosZ, new Offset(hsize, 0, hsize),
                                partials.NegX_PosY_PosZ, new Offset(0, hsize, hsize),
                                partials.PosX_PosY_PosZ, new Offset(hsize, hsize, hsize));
                        }
                        else
                        {
                            res = Array.Empty<_Offset_Cell>();
                        }

                        // Add cache entry
                        _cache.Add(key, res);
                    }
                    return res;
                }
            }

            /// <summary>
            /// Merges the given sorted cell arrays into a single sorted cell array.
            /// </summary>
            private static void _merge(Span<_Offset_Cell> res,
                _Offset_Cell[] a, Offset offset_a,
                _Offset_Cell[] b, Offset offset_b,
                _Offset_Cell[] c, Offset offset_c,
                _Offset_Cell[] d, Offset offset_d)
            {
                uint index_a = 0;
                uint index_b = 0;
                uint index_c = 0;
                uint index_d = 0;
                _Offset_Cell next_a = _read(a, ref index_a, offset_a);
                _Offset_Cell next_b = _read(b, ref index_b, offset_b);
                _Offset_Cell next_c = _read(c, ref index_c, offset_c);
                _Offset_Cell next_d = _read(d, ref index_d, offset_d);
                bool source_ab = next_a < next_b;
                _Offset_Cell next_ab = source_ab ? next_a : next_b;
                bool source_cd = next_c < next_d;
                _Offset_Cell next_cd = source_cd ? next_c : next_d;
                for (int i = 0; i < res.Length; i++)
                {
                    if (next_ab < next_cd)
                    {
                        res[i] = next_ab;
                        if (source_ab) next_a = _read(a, ref index_a, offset_a);
                        else next_b = _read(b, ref index_b, offset_b);
                        source_ab = next_a < next_b;
                        next_ab = source_ab ? next_a : next_b;
                    }
                    else
                    {
                        res[i] = next_cd;
                        if (source_cd) next_c = _read(c, ref index_c, offset_c);
                        else next_d = _read(d, ref index_d, offset_d);
                        source_cd = next_c < next_d;
                        next_cd = source_cd ? next_c : next_d;
                    }
                }
            }

            /// <summary>
            /// Reads the next <see cref="_Offset_Cell"/> from the given array, or returns
            /// <see cref="_Offset_Cell.Invalid"/> if the end of the array has been reached.
            /// </summary>
            private static _Offset_Cell _read(_Offset_Cell[] array, ref uint index, Offset offset)
            {
                if (index < array.Length)
                    return array[index++] + offset;
                return _Offset_Cell.Invalid;
            }
        }

        /// <summary>
        /// Combines a leaf index within a <see cref="OctreeDictionary{TLeaf}"/> with the three-dimensional
        /// offset it occurs at. This intermediate representation used to identify an item to be returned from a
        /// call to <see cref="OctreeGrid{T}.Search"/>.
        /// </summary>
        internal struct _Offset_Cell
        {
            private ulong _def;
            public _Offset_Cell(ulong def)
            {
                _def = def;
            }

            /// <summary>
            /// Constructs a <see cref="_Offset_Cell"/> whose offset component is zero.
            /// </summary>
            public static _Offset_Cell AtZero(uint leafIndex)
            {
                return new _Offset_Cell((ushort)leafIndex);
            }

            /// <summary>
            /// A sentinel <see cref="_Offset_Cell"/> which marks an invalid value. When comparing
            /// <see cref="_Offset_Cell"/>s, this is the greatest.
            /// </summary>
            public static _Offset_Cell Invalid => new _Offset_Cell(0xFFFFFFFFFFFFFFFF);

            /// <summary>
            /// The offset component of this <see cref="_Offset_Cell"/>.
            /// </summary>
            public Offset Offset => new Offset(_def & 0xFFFFFFFFFFFF0000);

            /// <summary>
            /// The leaf index for the cell component of this <see cref="_Offset_Cell"/>.
            /// </summary>
            public uint LeafIndex => unchecked((ushort)_def);

            /// <summary>
            /// Compares two <see cref="_Offset_Cell"/>s first by offset, then by cell.
            /// </summary>
            public static bool operator <(_Offset_Cell a, _Offset_Cell b)
            {
                return a._def < b._def;
            }

            /// <summary>
            /// Compares two <see cref="_Offset_Cell"/>s first by offset, then by cell.
            /// </summary>
            public static bool operator >(_Offset_Cell a, _Offset_Cell b)
            {
                return a._def > b._def;
            }

            /// <summary>
            /// Adds the given offset to the offset component of this <see cref="_Offset_Cell"/>.
            /// </summary>
            public static _Offset_Cell operator +(_Offset_Cell a, Offset b)
            {
                return new _Offset_Cell(a._def + b._def);
            }
        }

        // TODO: Replace with ShortVector3i
        /// <summary>
        /// A limited-range <see cref="Vector3i"/> which can identify a cell in an <see cref="OctreeGrid{T}"/>.
        /// </summary>
        public struct Offset
        {
            internal ulong _def;
            internal Offset(ulong def)
            {
                _def = def;
            }

            public Offset(ushort x, ushort y, ushort z)
            {
                _def = ((ulong)x << 16) | ((ulong)y << 32) | ((ulong)z << 48);
            }

            /// <summary>
            /// The zero offset.
            /// </summary>
            public static Offset Zero = new Offset(0);

            public static implicit operator Vector3i(Offset offset)
            {
                return new Vector3i(
                    unchecked((ushort)(offset._def >> 16)),
                    unchecked((ushort)(offset._def >> 32)),
                    unchecked((ushort)(offset._def >> 48)));
            }

            public static bool operator <(Offset a, Offset b)
            {
                return a._def < b._def;
            }
            
            public static bool operator >(Offset a, Offset b)
            {
                return a._def > b._def;
            }

            public static Offset operator +(Offset a, Offset b)
            {
                return new Offset(a._def + b._def);
            }
        }
    }
}