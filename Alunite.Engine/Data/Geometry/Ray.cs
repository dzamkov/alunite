﻿using System;
using System.Collections.Generic;

namespace Alunite.Data.Geometry
{
    /// <summary>
    /// Describes a ray in three-dimensional space. This can also be used to represent a parameterized line.
    /// </summary>
    public struct Ray3
    {
        public Ray3(Vector3 origin, Vector3 dir)
        {
            Origin = origin;
            Direction = dir;
        }

        /// <summary>
        /// The origin of the ray, where the argument is zero.
        /// </summary>
        public Vector3 Origin;

        /// <summary>
        /// The direction of the ray.
        /// </summary>
        public Vector3 Direction;

        /// <summary>
        /// Evaluates this ray at a given argument.
        /// </summary>
        public Vector3 this[Scalar arg]
        {
            get
            {
                return Origin + Direction * arg;
            }
        }

        /// <summary>
        /// Determines whether this ray intersects the given plane for some non-negative argument, and if so, returns
        /// that argument.
        /// </summary>
        public bool TryIntersect(Plane plane, out Scalar arg)
        {
            Scalar origin = Vector3.Dot(Origin, plane.Normal);
            Scalar diff = plane.Offset - origin;
            if (diff != Scalar.Zero)
            {
                Scalar dir = Vector3.Dot(Direction, plane.Normal);
                if (dir != Scalar.Zero)
                {
                    arg = diff / dir;
                    return arg >= Scalar.Zero;
                }
                else
                {
                    arg = Scalar.Inf;
                    return false;
                }
            }
            else
            {
                arg = Scalar.Zero;
                return true;
            }
        }

        /// <summary>
        /// Finds the smallest argument where this ray intersects a plane of the given box going outward and returns the direction
        /// of that plane. This will return a negative argument if the ray is outside the box and pointing away from it.
        /// </summary>
        public Scalar IntersectExit(Box3 box, out Dir3 dir)
        {
            dir = default;
            Scalar arg = Scalar.Inf;
            _intersect(Origin.X, Direction.X, box.Min.X, box.Max.X, ref arg, Axis3.X, ref dir);
            _intersect(Origin.Y, Direction.Y, box.Min.Y, box.Max.Y, ref arg, Axis3.Y, ref dir);
            _intersect(Origin.Z, Direction.Z, box.Min.Z, box.Max.Z, ref arg, Axis3.Z, ref dir);
            return arg;
        }

        // Helper function for implementing IntersectExit
        private static void _intersect(
            Scalar origin, Scalar delta, Scalar min, Scalar max,
            ref Scalar arg, Axis3 axis, ref Dir3 dir)
        {
            if (delta < 0)
            {
                Scalar nArg = (origin - min) / -delta;
                if (nArg < arg)
                {
                    arg = nArg;
                    dir = axis.ToDir(Dir1.Neg);
                }
            }
            else if (delta > 0)
            {
                Scalar nArg = (max - origin) / delta;
                if (nArg < arg)
                {
                    arg = nArg;
                    dir = axis.ToDir(Dir1.Pos);
                }
            }
        }

        public static Ray3 operator +(Ray3 a, Ray3 b)
        {
            return new Ray3(a.Origin + b.Origin, a.Direction + b.Direction);
        }

        public static Ray3 operator +(Ray3 a, Vector3 b)
        {
            return new Ray3(a.Origin + b, a.Direction);
        }
        
        public static Ray3 operator +(Vector3 a, Ray3 b)
        {
            return new Ray3(a + b.Origin, b.Direction);
        }

        public static Ray3 operator -(Ray3 a, Vector3 b)
        {
            return new Ray3(a.Origin - b, a.Direction);
        }
    }
}
