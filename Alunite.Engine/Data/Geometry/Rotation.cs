﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

using Alunite.Data.Probability;

namespace Alunite.Data.Geometry
{
    /// <summary>
    /// Describes a rotation in two-dimensional space.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct Rotation2 : IEquatable<Rotation2>
    {
        // Rotations are represented by the tangent of half their angle. This representation has several
        // benefits: it only uses one scalar value, it has roughly uniform precision over all rotations,
        // it does not require any expensive operations (e.g. sin, cos, sqrt) to apply or compose, and there
        // is no need to normalize after composing.
        internal float _tanHalfAngle;
        internal Rotation2(float tanHalfAngle)
        {
            _tanHalfAngle = tanHalfAngle;
        }

        /// <summary>
        /// The identity rotation.
        /// </summary>
        public static Rotation2 Identity => new Rotation2(0f);

        /// <summary>
        /// A 90 degrees counter-clockwise rotation.
        /// </summary>
        public static Rotation2 Cross => new Rotation2(1f);

        /// <summary>
        /// A 180 degree rotation.
        /// </summary>
        public static Rotation2 Flip => new Rotation2(float.PositiveInfinity);

        /// <summary>
        /// Indicates whether this rotation is close enough to <see cref="Identity"/> such that it is likely
        /// represent it after accounting for rounding error incurred during arithmetic operations.
        /// </summary>
        public bool IsLikelyIdentity => AreLikelyEqual(this, Identity);

        /// <summary>
        /// The inverse of this rotation.
        /// </summary>
        public Rotation2 Inverse => new Rotation2(-_tanHalfAngle);

        /// <summary>
        /// The angle, in counter-clockwise radians, of this rotation.
        /// </summary>
        public Scalar Angle => Scalar.Atan(_tanHalfAngle) * 2;

        /// <summary>
        /// Constructs a rotation which rotates counter-clockwise by the given angle, in radians.
        /// </summary>
        public static Rotation2 FromAngle(Scalar angle)
        {
            return new Rotation2((float)Scalar.Tan(angle * 0.5));
        }

        /// <summary>
        /// Constructs a rotation which orients the positive X direction towards the given vector.
        /// </summary>
        /// <param name="len">The length of <paramref name="forward"/>.</param>
        public static Rotation2 Orient(Vector2 forward, out Scalar len)
        {
            float rCosAngle = forward.X._mean;
            float rSinAngle = forward.Y._mean;
            float r = Scalar._sqrt(rCosAngle * rCosAngle + rSinAngle * rSinAngle);
            len = new Scalar(r);
            if (r == 0f)
                return Identity;
            if (rCosAngle >= 0f)
                return new Rotation2(rSinAngle / (r + rCosAngle));
            else
                return new Rotation2((r - rCosAngle) / rSinAngle);
        }

        /// <summary>
        /// Composes two rotations.
        /// </summary>
        public static Rotation2 Compose(Rotation2 a, Rotation2 b)
        {
            float x = a._tanHalfAngle;
            float y = b._tanHalfAngle;
            if (Math.Abs(x) <= _normalThreshold)
            {
                if (Math.Abs(y) <= _normalThreshold)
                    return new Rotation2((x + y) / (1f - x * y));
                else
                    return new Rotation2(1f / (1f / y - x));
            }
            else
            {
                if (Math.Abs(y) <= _normalThreshold)
                    return new Rotation2(1f / (1f / x - y));
                else
                    return new Rotation2((-1f / x) + (-1f / y));
            }
        }

        /// <summary>
        /// The threshold for the absolute value of <see cref="_tanHalfAngle"/> above which special handling is needed
        /// to ensure numerical stability. 
        /// </summary>
        internal const float _normalThreshold = 1e18f;

        /// <summary>
        /// Applies this rotation to a position or direction vector.
        /// </summary>
        public Vector2 Apply(Vector2 pos)
        {
            return (Matrix2x2)this * pos;
        }
        
        /// <summary>
        /// Determines whether the given <see cref="Rotation2"/>s likely have the same true value
        /// after compensating for representation error.
        /// </summary>
        public static bool AreLikelyEqual(Rotation2 a, Rotation2 b)
        {
            return AreEqualOdds(a, b) > 1;
        }

        /// <summary>
        /// Determines the relative probability that the given <see cref="Rotation3"/>s have the same
        /// true value, over the probability that their true values are independent.
        /// </summary>
        public static LogScalar AreEqualOdds(Rotation2 a, Rotation2 b)
        {
            Rotation2 between = b.Inverse * a;
            float sqrMean = between._tanHalfAngle * between._tanHalfAngle;
            float var = Scalar._beta + Scalar._gamma * sqrMean;
            float priorFactor = Scalar._alpha + sqrMean;
            return new LogScalar(
                (Scalar._lnNormalNorm - Scalar._lnPriorNorm)
                + sqrMean / (-2f * (var + Scalar._beta))
                - Scalar._ln((var + Scalar._beta) / (priorFactor * Scalar._alpha)) / 2f);
        }

        public static Rotation2 operator *(Rotation2 a, Rotation2 b)
        {
            return Compose(a, b);
        }
        
        public static Vector2 operator *(Rotation2 a, Vector2 b)
        {
            return a.Apply(b);
        }

        public static implicit operator Matrix2x2(Rotation2 source)
        {
            Scalar sin, cos;
            float x = source._tanHalfAngle;
            if (Math.Abs(x) < _normalThreshold)
            {
                float xSqr = x * x;
                float y = 1f / (1f + xSqr);
                sin = 2f * x * y;
                cos = (1f - xSqr) * y;
            }
            else
            {
                sin = 2f / x;
                cos = -1f;
            }
            return new Matrix2x2(
                new Vector2(cos, sin),
                new Vector2(-sin, cos));
        }

        public static explicit operator Rotation2(Matrix2x2 source)
        {
            float rCosAngle = (source.X.X._mean + source.Y.Y._mean) / 2f;
            float rSinAngle = (source.X.Y._mean - source.Y.X._mean) / 2f;
            if (rCosAngle >= 0f)
                return new Rotation2(rSinAngle / (1f + rCosAngle));
            else
                return new Rotation2((1f - rCosAngle) / rSinAngle);
        }

        public static bool operator ==(Rotation2 a, Rotation2 b)
        {
            return a._tanHalfAngle == b._tanHalfAngle;
        }

        public static bool operator !=(Rotation2 a, Rotation2 b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Rotation2))
                return false;
            return this == (Rotation2)obj;
        }

        bool IEquatable<Rotation2>.Equals(Rotation2 other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return _tanHalfAngle.GetHashCode();
        }

        public override string ToString()
        {
            Scalar degs = Scalar.Atan(_tanHalfAngle) * 2 * 180 / Scalar.Pi;
            return "Rotation(" + degs.ToString() + "°)";
        }
    }

    /// <summary>
    /// Describes an orthogonal transformation in two-dimensional space. This is a transformation consisting of
    /// rotations and reflections.
    /// </summary>
    public struct Roflection2
    {
        internal Rotation2 _rot;
        internal bool _reflects;
        internal Roflection2(Rotation2 rot, bool reflects)
        {
            _rot = rot;
            _reflects = reflects;
        }

        /// <summary>
        /// Indicates whether this <see cref="Roflection2"/> is a <see cref="Rotation2"/>.
        /// </summary>
        public bool IsRotation => !_reflects;
    }

    /// <summary>
    /// Describes a rotation in three-dimensional space.
    /// </summary>
    public struct Rotation3
    {
        internal Quaternion _quat;
        public Rotation3(Quaternion quat)
        {
            _quat = quat;
        }

        public Rotation3(Scalar a, Scalar b, Scalar c, Scalar d)
            : this(new Quaternion(a, b, c, d))
        { }

        /// <summary>
        /// The identity rotation.
        /// </summary>
        public static Rotation3 Identity => new Rotation3(new Quaternion(1, 0, 0, 0));

        /// <summary>
        /// The inverse of this rotation.
        /// </summary>
        public Rotation3 Inverse
        {
            get
            {
                return new Rotation3(new Quaternion(_quat.A, -_quat.B_C_D));
            }
        }

        /// <summary>
        /// Constructs a rotation of the given angle about the given axis, following the right-hand rule.
        /// </summary>
        public static Rotation3 About(Vector3 axis, Scalar angle)
        {
            Scalar halfAngle = angle * 0.5;
            return new Rotation3(new Quaternion(Scalar.Cos(halfAngle), axis * Scalar.Sin(halfAngle)));
        }

        /// <summary>
        /// Constructs a rotation about the given vector with an angle which is equal to its magnitude, in radians,
        /// following the right-hand rule.
        /// </summary>
        public static Rotation3 Euler(Vector3 euler)
        {
            Scalar len = euler.Length;
            return About(euler / len, len);
        }

        /// <summary>
        /// Constructs a rotation representing the world-to-view transform for the situation where the camera is
        /// looking in a given direction.
        /// </summary>
        public static Rotation3 LookAt(Vector3 forward, Vector3 up)
        {
            Vector3 x = forward;
            Vector3 y = Vector3.Normalize(Vector3.Cross(up, x));
            Vector3 z = Vector3.Cross(x, y);
            Matrix3x3 mat = new Matrix3x3(x, y, z);
            return ((Rotation3)mat).Inverse;
        }

        /// <summary>
        /// Composes two rotations, first applying the right, then the left.
        /// </summary>
        public static Rotation3 Compose(Rotation3 a, Rotation3 b)
        {
            Quaternion quat = a._quat * b._quat;

            // Use a rational approximation for square root centered around 1, since a full square root
            // for normalization is overkill
            Scalar sqrMag = quat.SqrMagnitude;
            return new Rotation3(quat * (2 / (1 + sqrMag)));
        }

        /// <summary>
        /// Constructs the three-dimensional rotation which applies the given <see cref="Rotation2"/> on the X/Y
        /// plane while leaving the Z axis untouched.
        /// </summary>
        public static Rotation3 On_X_Y(Rotation2 rot)
        {
            float x = rot._tanHalfAngle;
            Scalar sinHalfAngle, cosHalfAngle;
            if (Math.Abs(x) < Rotation2._normalThreshold)
            {
                Scalar tanHalfAngle = rot._tanHalfAngle;
                cosHalfAngle = 1 / Scalar.Sqrt(1 + tanHalfAngle * tanHalfAngle);
                sinHalfAngle = tanHalfAngle * cosHalfAngle;
            }
            else
            {
                cosHalfAngle = 1f / rot._tanHalfAngle;
                sinHalfAngle = 1;
            }
            return new Rotation3(new Quaternion(cosHalfAngle, 0, 0, sinHalfAngle));
        }

        /// <summary>
        /// Applies this rotation to a position or direction vector.
        /// </summary>
        public Vector3 Apply(Vector3 pos)
        {
            // TODO: Faster way to do this
            return (Matrix3x3)this * pos;
        }

        /// <summary>
        /// Determines whether the given <see cref="Rotation3"/>s likely have the same true value
        /// after compensating for representation error.
        /// </summary>
        public static bool AreLikelyEqual(Rotation3 a, Rotation3 b)
        {
            return AreEqualOdds(a, b) > 1;
        }

        /// <summary>
        /// Determines the relative probability that the given <see cref="Rotation3"/>s have the same
        /// true value, over the probability that their true values are independent.
        /// </summary>
        public static LogScalar AreEqualOdds(Rotation3 a, Rotation3 b)
        {
            // TODO: Specialize for rotation
            if (Vector4.Dot(a._quat.B_C_D_A, b._quat.B_C_D_A) > 0)
                return Vector4.AreEqualOdds(a._quat.B_C_D_A, b._quat.B_C_D_A);
            else
                return Vector4.AreEqualOdds(a._quat.B_C_D_A, -b._quat.B_C_D_A);
        }

        public static Rotation3 operator *(Rotation3 a, Rotation3 b)
        {
            return Compose(a, b);
        }

        public static Vector3 operator *(Rotation3 a, Vector3 b)
        {
            return a.Apply(b);
        }

        public static implicit operator Matrix3x3(Rotation3 rot)
        {
            Quaternion quat = rot._quat;
            Vector3 ab2_ac2_ad2 = 2 * quat.A * quat.B_C_D;
            Vector3 bb2_cc2_dd2 = 2 * Vector3.Multiply(quat.B_C_D, quat.B_C_D);
            Vector3 bc2_cd2_bd2 = 2 * Vector3.Multiply(quat.B_C_D, quat.B_C_D.Y_Z_X);
            Scalar bb2 = bb2_cc2_dd2.X;
            Scalar cc2 = bb2_cc2_dd2.Y;
            Scalar dd2 = bb2_cc2_dd2.Z;
            Scalar ab2 = ab2_ac2_ad2.X;
            Scalar ac2 = ab2_ac2_ad2.Y;
            Scalar ad2 = ab2_ac2_ad2.Z;
            Scalar bc2 = bc2_cd2_bd2.X;
            Scalar cd2 = bc2_cd2_bd2.Y;
            Scalar bd2 = bc2_cd2_bd2.Z;
            return new Matrix3x3(
                new Vector3(1 - cc2 - dd2, bc2 + ad2, bd2 - ac2),
                new Vector3(bc2 - ad2, 1 - dd2 - bb2, cd2 + ab2),
                new Vector3(bd2 + ac2, cd2 - ab2, 1 - cc2 - bb2));
        }

        public static explicit operator Rotation3(Matrix3x3 source)
        {
            Scalar trace = source.X.X + source.Y.Y + source.Z.Z;
            if (trace > 0)
            {
                Scalar s = Scalar.Sqrt(trace + 1);
                Scalar invS = 0.5 / s;
                return new Rotation3(new Quaternion(s * 0.5,
                    (source.Y.Z - source.Z.Y) * invS,
                    (source.Z.X - source.X.Z) * invS,
                    (source.X.Y - source.Y.X) * invS));
            }
            else if (source.X.X >= source.Y.Y && source.X.X >= source.Z.Z)
            {
                Scalar s = Scalar.Sqrt(1 + source.X.X - source.Y.Y - source.Z.Z);
                Scalar invS = 0.5 / s;
                return new Rotation3(new Quaternion(
                    (source.Y.Z - source.Z.Y) * invS,
                    s * 0.5,
                    (source.X.Y + source.Y.X) * invS,
                    (source.Z.X + source.X.Z) * invS));
            }
            else if (source.Y.Y > source.Z.Z)
            {
                Scalar s = Scalar.Sqrt(1 + source.Y.Y - source.Z.Z - source.X.X);
                Scalar invS = 0.5 / s;
                return new Rotation3(new Quaternion(
                    (source.Z.X - source.X.Z) * invS,
                    (source.X.Y + source.Y.X) * invS,
                    0.5 * s,
                    (source.Y.Z + source.Z.Y) * invS));
            }
            else
            {
                Scalar s = Scalar.Sqrt(1 + source.Z.Z - source.X.X - source.Y.Y);
                Scalar invS = 0.5 / s;
                return new Rotation3(new Quaternion(
                    (source.X.Y - source.Y.X) * invS,
                    (source.Z.X + source.X.Z) * invS,
                    (source.Y.Z + source.Z.Y) * invS,
                    0.5 * s));
            }
        }
    }

    /// <summary>
    /// Describes an orthogonal transformation in three-dimensional space. This is a transformation consisting of
    /// rotations and reflections.
    /// </summary>
    public struct Roflection3
    {
        private Roflection3(Quaternion quat)
        {
            _quat = quat;
        }

        internal Roflection3(Rotation3 rot, bool reflects)
        {
            _quat = rot._quat;
            if (_quat.A < -Scalar._normalEps)
                reflects = !reflects;
            else if (_quat.A < Scalar._normalEps)
                _quat.A = Scalar._normalEps; // Make sure reflection component is unambiguous
            if (reflects)
                _quat = -_quat;
        }

        /// <summary>
        /// The quaternion representation of this <see cref="Roflection3"/>. Since <see cref="Quaternion"/>s double-cover
        /// rotations, the reflection component of the roflection can be encoded directly in the quaternion.
        /// Specifically, the sign of <see cref="Quaternion.A"/> specifies a scaling factor that is applied alongside
        /// the rotation.
        /// </summary>
        private Quaternion _quat;

        /// <summary>
        /// The identity <see cref="Roflection3"/>.
        /// </summary>
        public static Roflection3 Identity => new Roflection3(new Quaternion(1, 0, 0, 0));

        /// <summary>
        /// Indicates whether this <see cref="Roflection3"/> is a <see cref="Rotation3"/>.
        /// </summary>
        public bool IsRotation => !_reflects;
        
        /// <summary>
        /// The inverse of this roflection.
        /// </summary>
        public Roflection3 Inverse
        {
            get
            {
                Quaternion quat = _quat;
                return new Roflection3(new Quaternion(quat.A, -quat.B_C_D));
            }
        }

        /// <summary>
        /// The rotation component of this <see cref="Roflection3"/>.
        /// </summary>
        private Rotation3 _rot => new Rotation3(_quat);

        /// <summary>
        /// Indicates whether this <see cref="Roflection3"/> involves a reflection.
        /// </summary>
        private bool _reflects => _quat.A < 0;

        /// <summary>
        /// Composes two rotations, first applying the right, then the left.
        /// </summary>
        public static Roflection3 Compose(Roflection3 a, Roflection3 b)
        {
            Quaternion quat = a._quat * b._quat;
            bool reflects = a._reflects ^ b._reflects;
            if (quat.A._mean < -Scalar._normalEps)
                reflects = !reflects;
            else if (quat.A < Scalar._normalEps)
                quat.A = Scalar._normalEps; // Make sure reflection component is unambiguous
            Scalar sqrMag = quat.SqrMagnitude;
            Scalar scale = 2 / (1 + sqrMag);
            if (reflects)
                scale = -scale;
            return new Roflection3(quat * scale);
        }
        
        /// <summary>
        /// Applies this roflection to a position or direction vector.
        /// </summary>
        public Vector3 Apply(Vector3 pos)
        {
            Vector3 res = _rot.Apply(pos);
            if (_reflects)
                res = -res;
            return res;
        }

        public static Roflection3 operator *(Roflection3 a, Roflection3 b)
        {
            return Compose(a, b);
        }

        public static Vector3 operator *(Roflection3 a, Vector3 b)
        {
            return a.Apply(b);
        }

        public static implicit operator Roflection3(Rotation3 rot)
        {
            return new Roflection3(rot, false);
        }

        public static explicit operator Rotation3(Roflection3 rof)
        {
            if (!rof.IsRotation)
                throw new InvalidCastException("Roflection is not a rotation");
            return rof._rot;
        }

        public static implicit operator Matrix3x3(Roflection3 rof)
        {
            Matrix3x3 mat = rof._rot;
            if (rof._reflects)
            {
                mat.X = -mat.X;
                mat.Y = -mat.Y;
                mat.Z = -mat.Z;
            }
            return mat;
        }
    }

    /// <summary>
    /// Contains functions related to rotations.
    /// </summary>
    public static class Rotation
    {
        /// <summary>
        /// Finds a rotation which, when applied to <paramref name="cov"/>, maximizes its trace.
        /// </summary>
        public static void Wahba(Matrix2x2 cov, out Rotation2 rot, out Scalar trace)
        {
            // Reduce the problem to maximizing the X component of a vector
            Vector2 vec = new Vector2(cov.X.X + cov.Y.Y, cov.Y.X - cov.X.Y);
            rot = Rotation2.Orient(vec, out trace);
        }
        
        /// <summary>
        /// Finds a rotation which, when applied to <paramref name="cov"/>, maximizes its trace.
        /// </summary>
        /// <remarks>This is a necessary step for solving <a href="https://en.wikipedia.org/wiki/Wahba%27s_problem">
        /// Wahba's problem</a>.</remarks>
        /// <param name="rot">The rotation which maximizes the trace of <paramref name="cov"/>.</param>
        /// <param name="trace">The trace of <paramref name="rot"/> * <paramref name="cov"/>.</param>
        public static void Wahba(Matrix3x3 cov, out Rotation3 rot, out Scalar trace)
        {
            cov.Svd(out Matrix3x3 u, out Vector3 s, out Matrix3x3 v);
            if (u.Determinant * v.Determinant < 0)
            {
                s.Z = -s.Z;
                u.Z = -u.Z;
            }
            rot = (Rotation3)(v * u.Transpose);
            trace = s.X + s.Y + s.Z;
        }
    }
}