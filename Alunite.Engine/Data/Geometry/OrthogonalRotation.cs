﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace Alunite.Data.Geometry
{
    /// <summary>
    /// Identifies one of the two possible reflection isometries in one-dimensional space. This can either be the
    /// identity transform, or a negation.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct Reflection1 : IEquatable<Reflection1>
    {
        private Reflection1(byte code)
        {
            Code = code;
        }

        /// <summary>
        /// The unique code used to identify this reflection transform, guaranteed to
        /// be less than <see cref="Count"/>.
        /// </summary>
        public byte Code { get; }

        /// <summary>
        /// The number of distinct <see cref="Reflection1"/>s.
        /// </summary>
        public const byte Count = 2;

        /// <summary>
        /// The identity <see cref="Reflection1"/>.
        /// </summary>
        public static Reflection1 Identity => Pos;

        /// <summary>
        /// The <see cref="Reflection1"/> which negates coordinate values.
        /// </summary>
        public static Reflection1 Flip => Neg;

        /// <summary>
        /// The inverse of this <see cref="Reflection1"/>.
        /// </summary>
        public Reflection1 Inverse => this;

        /// <summary>
        /// Gets the <see cref="Reflection1"/> with the given <see cref="Code"/>.
        /// </summary>
        public static Reflection1 ByCode(byte code)
        {
            Debug.Assert(code < Count);
            return new Reflection1(code);
        }

        /// <summary>
        /// Composes two <see cref="Reflection1"/>s.
        /// </summary>
        public static Reflection1 Compose(Reflection1 a, Reflection1 b)
        {
            return new Reflection1((byte)(a.Code ^ b.Code));
        }

        /// <summary>
        /// Applies this <see cref="Reflection1"/> to a scalar.
        /// </summary>
        public Scalar Apply(Scalar scalar)
        {
            if (Code == 0) return scalar;
            else return -scalar;
        }

        /// <summary>
        /// Applies this <see cref="Reflection1"/> to an integer.
        /// </summary>
        public int Apply(int i)
        {
            if (Code == 0) return i;
            else return -i;
        }

        /// <summary>
        /// Applies this <see cref="Reflection1"/> to a direction.
        /// </summary>
        public Dir1 Apply(Dir1 dir)
        {
            return (Dir1)((byte)dir ^ Code);
        }

        /// <summary>
        /// Applies this <see cref="Reflection1"/> to a <see cref="Duet{T}"/>, assuming elements are not affected by
        /// the reflection.
        /// </summary>
        public Duet<T> Apply<T>(Duet<T> duet)
        {
            if (Code == 0) return duet;
            else return duet.Flip();
        }

        #region Elements
        public static Reflection1 Pos => new Reflection1(0);
        public static Reflection1 Neg => new Reflection1(1);
        #endregion

        public static explicit operator Reflection1(Dir1 dir)
        {
            return new Reflection1((byte)dir.Flip());
        }

        public static Reflection1 operator *(Reflection1 a, Reflection1 b)
        {
            return Compose(a, b);
        }

        public static Scalar operator *(Reflection1 a, Scalar b)
        {
            return a.Apply(b);
        }

        public static Vector2 operator *(Reflection1 a, Vector2 b)
        {
            return new Vector2(a * b.X, a * b.Y);
        }

        public static Vector2i operator *(Reflection1 a, Vector2i b)
        {
            return new Vector2i(a * b.X, a * b.Y);
        }

        public static Vector3 operator *(Reflection1 a, Vector3 b)
        {
            return new Vector3(a * b.X, a * b.Y, a * b.Z);
        }

        public static Vector3i operator *(Reflection1 a, Vector3i b)
        {
            return new Vector3i(a * b.X, a * b.Y, a * b.Z);
        }

        public static int operator *(Reflection1 a, int b)
        {
            return a.Apply(b);
        }

        public static Dir1 operator *(Reflection1 a, Dir1 b)
        {
            return a.Apply(b);
        }

        public static bool operator ==(Reflection1 a, Reflection1 b)
        {
            return a.Code == b.Code;
        }

        public static bool operator !=(Reflection1 a, Reflection1 b)
        {
            return a.Code != b.Code;
        }

        public override bool Equals(object obj)
        {
            if (obj is Reflection1)
                return this == (Reflection1)obj;
            return false;
        }

        bool IEquatable<Reflection1>.Equals(Reflection1 other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return Code;
        }

        public override string ToString()
        {
            return _stringTable[Code];
        }

        /// <summary>
        /// The table used to implement <see cref="object.ToString"/>
        /// </summary>
        private static readonly string[] _stringTable = new string[]
        {
            "+",
            "-"
        };
    }

    /// <summary>
    /// Identifies one of the 4 rotations isometries in discrete two-dimensional space.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct Rotation2i : IEquatable<Rotation2i>
    {
        private Rotation2i(byte code)
        {
            Code = code;
        }

        /// <summary>
        /// The unique code used to identify this <see cref="Rotation2i"/>, guaranteed to
        /// be less than <see cref="Count"/>.
        /// </summary>
        public byte Code { get; }

        /// <summary>
        /// The number of distinct <see cref="Rotation2i"/>s.
        /// </summary>
        public const byte Count = 4;

        /// <summary>
        /// The identity <see cref="Rotation2i"/>.
        /// </summary>
        public static Rotation2i Identity => PosX_PosY;

        /// <summary>
        /// The <see cref="Roflection2i"/> which rotates 90 degrees counter-clockwise.
        /// </summary>
        public static Rotation2i Rotate => NegY_PosX;

        /// <summary>
        /// The inverse of this <see cref="Rotation2i"/>.
        /// </summary>
        public Rotation2i Inverse => new Rotation2i((byte)((4 - Code) & 0b11));

        /// <summary>
        /// Gets the <see cref="Rotation2i"/> with the given <see cref="Code"/>.
        /// </summary>
        public static Rotation2i ByCode(byte code)
        {
            Debug.Assert(code < Count);
            return new Rotation2i(code);
        }

        /// <summary>
        /// Composes two <see cref="Reflection1"/>s.
        /// </summary>
        public static Rotation2i Compose(Rotation2i a, Rotation2i b)
        {
            return new Rotation2i((byte)((a.Code + b.Code) & 0b11));
        }

        /// <summary>
        /// Applies this <see cref="Rotation2i"/> to a vector.
        /// </summary>
        public Vector2 Apply(Vector2 vec)
        {
            switch (Code)
            {
                case 0: return new Vector2(vec.X, vec.Y);
                case 1: return new Vector2(-vec.Y, vec.X);
                case 2: return new Vector2(-vec.X, -vec.Y);
                case 3: return new Vector2(vec.Y, -vec.X);
                default: throw new ArgumentException();
            }
        }

        /// <summary>
        /// Applies this <see cref="Rotation2i"/> to a vector.
        /// </summary>
        public Vector2i Apply(Vector2i vec)
        {
            switch (Code)
            {
                case 0: return new Vector2i(vec.X, vec.Y);
                case 1: return new Vector2i(-vec.Y, vec.X);
                case 2: return new Vector2i(-vec.X, -vec.Y);
                case 3: return new Vector2i(vec.Y, -vec.X);
                default: throw new ArgumentException();
            }
        }

        /// <summary>
        /// Applies this <see cref="Rotation2i"/> to an axis.
        /// </summary>
        public Axis2 Apply(Axis2 axis)
        {
            return Apply(axis, out _);
        }

        /// <summary>
        /// Applies this <see cref="Rotation2i"/> to an axis.
        /// </summary>
        /// <param name="local">The transform from the canonical local space of the given axis to
        /// the canonical local space of the returned axis.</param>
        public Axis2 Apply(Axis2 axis, out PlanarRoflection2i local)
        {
            Axis2 nAxis = ((Roflection2i)this).Apply(axis, out local);
            return nAxis;
        }

        /// <summary>
        /// Applies this <see cref="Rotation2i"/> to a direction.
        /// </summary>
        public Dir2 Apply(Dir2 dir)
        {
            Axis2 axis = dir.GetAxis(out Dir1 adir);
            Axis2 nAxis = Apply(axis, out PlanarRoflection2i local);
            return nAxis.ToDir(local.Y * adir);
        }

        /// <summary>
        /// Applies this <see cref="Rotation2i"/> to a <see cref="Quadrant"/>.
        /// </summary>
        public Quadrant Apply(Quadrant quad)
        {
            return ((Roflection2i)this).Apply(quad);
        }

        #region Elements
        public static Rotation2i PosX_PosY => new Rotation2i(0);
        public static Rotation2i NegY_PosX => new Rotation2i(1);
        public static Rotation2i NegX_NegY => new Rotation2i(2);
        public static Rotation2i PosY_NegX => new Rotation2i(3);
        #endregion

        public static Rotation2i operator *(Rotation2i a, Rotation2i b)
        {
            return Compose(a, b);
        }

        public static Vector2 operator *(Rotation2i a, Vector2 b)
        {
            return a.Apply(b);
        }

        public static Vector2i operator *(Rotation2i a, Vector2i b)
        {
            return a.Apply(b);
        }

        public static Dir2 operator *(Rotation2i a, Dir2 b)
        {
            return a.Apply(b);
        }

        public static Quadrant operator *(Rotation2i a, Quadrant b)
        {
            return a.Apply(b);
        }

        public static implicit operator Rotation2(Rotation2i rot)
        {
            return _rotTable[rot.Code];
        }

        /// <summary>
        /// The table used to implement the conversion to <see cref="Rotation2"/>.
        /// </summary>
        private static readonly Rotation2[] _rotTable = new Rotation2[]
        {
            Rotation2.Identity,
            Rotation2.Cross,
            Rotation2.Flip,
            Rotation2.Cross.Inverse
        };

        public static implicit operator Matrix2x2(Rotation2i rot)
        {
            return new Matrix2x2(
                rot.Apply(new Vector2(1, 0)),
                rot.Apply(new Vector2(0, 1)));
        }

        public static bool operator ==(Rotation2i a, Rotation2i b)
        {
            return a.Code == b.Code;
        }

        public static bool operator !=(Rotation2i a, Rotation2i b)
        {
            return a.Code != b.Code;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Rotation2i))
                return false;
            return this == (Rotation2i)obj;
        }

        bool IEquatable<Rotation2i>.Equals(Rotation2i other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return Code.GetHashCode();
        }

        public override string ToString()
        {
            return ((Roflection2i)this).ToString();
        }
    }

    /// <summary>
    /// Identifies one of the 8 rotation/reflection isometries in discrete two-dimensional space.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct Roflection2i : IEquatable<Roflection2i>
    {
        private Roflection2i(byte code)
        {
            Code = code;
        }

        public Roflection2i(Reflection1 x, Reflection1 y)
        {
            this = new PlanarRoflection2i(x, y);
        }

        /// <summary>
        /// The unique code used to identify this <see cref="Roflection2i"/>, guaranteed to
        /// be less than <see cref="Count"/>.
        /// </summary>
        public byte Code { get; }

        /// <summary>
        /// The number of distinct <see cref="Roflection2i"/>s.
        /// </summary>
        public const byte Count = 8;

        /// <summary>
        /// The identity <see cref="Roflection2i"/>.
        /// </summary>
        public static Roflection2i Identity => PosX_PosY;

        /// <summary>
        /// A 90 degrees counter-clockwise rotation.
        /// </summary>
        public static Roflection2i Cross => NegY_PosX;

        /// <summary>
        /// A 180 degree rotation.
        /// </summary>
        public static Roflection2i Flip => NegX_NegY;

        /// <summary>
        /// The <see cref="Roflection2i"/> which swaps the X and Y axis.
        /// </summary>
        public static Roflection2i Swap => PosY_PosX;

        /// <summary>
        /// The reflection component of this <see cref="Roflection2i"/>.
        /// </summary>
        public Reflection1 Reflection => Reflection1.ByCode((byte)(Code & 0b1));

        /// <summary>
        /// Indicates whether this <see cref="Roflection2i"/> is a <see cref="Rotation2i"/>.
        /// </summary>
        public bool IsRotation => Reflection == Reflection1.Identity;

        /// <summary>
        /// The inverse of this <see cref="Roflection2i"/>.
        /// </summary>
        public Roflection2i Inverse => new Roflection2i((byte)((0x72543610u >> (Code << 2)) & 0b111));

        /// <summary>
        /// Gets the <see cref="Roflection2i"/> with the given <see cref="Code"/>.
        /// </summary>
        public static Roflection2i ByCode(byte code)
        {
            Debug.Assert(code < Count);
            return new Roflection2i(code);
        }

        /// <summary>
        /// Composes two <see cref="Roflection2i"/>, first applying the right, then the left.
        /// </summary>
        public static Roflection2i Compose(Roflection2i a, Roflection2i b)
        {
            // Taking advantage of the fact that the first 8 Rotation3i's correspond to Roflection2i's
            return new Roflection2i((Rotation3i.ByCode(a.Code) * Rotation3i.ByCode(b.Code)).Code);
        }

        /// <summary>
        /// Applies this <see cref="Roflection2i"/> to a vector.
        /// </summary>
        public Vector2 Apply(Vector2 vec)
        {
            switch (Code)
            {
                case 0: return new Vector2(vec.X, vec.Y);
                case 1: return new Vector2(vec.Y, vec.X);
                case 2: return new Vector2(-vec.Y, vec.X);
                case 3: return new Vector2(-vec.X, vec.Y);
                case 4: return new Vector2(-vec.X, -vec.Y);
                case 5: return new Vector2(-vec.Y, -vec.X);
                case 6: return new Vector2(vec.Y, -vec.X);
                case 7: return new Vector2(vec.X, -vec.Y);
                default: throw new ArgumentException();
            }
        }

        /// <summary>
        /// Applies this <see cref="Roflection2i"/> to a vector.
        /// </summary>
        public Vector2i Apply(Vector2i vec)
        {
            switch (Code)
            {
                case 0: return new Vector2i(vec.X, vec.Y);
                case 1: return new Vector2i(vec.Y, vec.X);
                case 2: return new Vector2i(-vec.Y, vec.X);
                case 3: return new Vector2i(-vec.X, vec.Y);
                case 4: return new Vector2i(-vec.X, -vec.Y);
                case 5: return new Vector2i(-vec.Y, -vec.X);
                case 6: return new Vector2i(vec.Y, -vec.X);
                case 7: return new Vector2i(vec.X, -vec.Y);
                default: throw new ArgumentException();
            }
        }

        /// <summary>
        /// Applies this <see cref="Roflection2i"/> to an axis.
        /// </summary>
        public Axis2 Apply(Axis2 axis)
        {
            return Apply(axis, out _);
        }

        /// <summary>
        /// Applies this <see cref="Roflection2i"/> to an axis.
        /// </summary>
        /// <param name="local">The transform from the canonical local space of the given axis to
        /// the canonical local space of the returned axis.</param>
        public Axis2 Apply(Axis2 axis, out PlanarRoflection2i local)
        {
            (axis, local) = _axisTable[Code * 2 + (int)axis];
            return axis;
        }

        /// <summary>
        /// The table used to implement <see cref="Apply(Axis2, out PlanarRoflection2i)"/>.
        /// </summary>
        private static readonly (Axis2, PlanarRoflection2i)[] _axisTable = new[]
        {
            (Axis2.X, PlanarRoflection2i.PosX_PosY),
            (Axis2.Y, PlanarRoflection2i.PosX_PosY),
            (Axis2.Y, PlanarRoflection2i.PosX_PosY),
            (Axis2.X, PlanarRoflection2i.PosX_PosY),
            (Axis2.Y, PlanarRoflection2i.NegX_PosY),
            (Axis2.X, PlanarRoflection2i.PosX_NegY),
            (Axis2.X, PlanarRoflection2i.PosX_NegY),
            (Axis2.Y, PlanarRoflection2i.NegX_PosY),
            (Axis2.X, PlanarRoflection2i.NegX_NegY),
            (Axis2.Y, PlanarRoflection2i.NegX_NegY),
            (Axis2.Y, PlanarRoflection2i.NegX_NegY),
            (Axis2.X, PlanarRoflection2i.NegX_NegY),
            (Axis2.Y, PlanarRoflection2i.PosX_NegY),
            (Axis2.X, PlanarRoflection2i.NegX_PosY),
            (Axis2.X, PlanarRoflection2i.NegX_PosY),
            (Axis2.Y, PlanarRoflection2i.PosX_NegY)
        };

        /// <summary>
        /// Applies this <see cref="Roflection2i"/> to a direction.
        /// </summary>
        public Dir2 Apply(Dir2 dir)
        {
            return Apply(dir, out _);
        }

        /// <summary>
        /// Applies this <see cref="Roflection2i"/> to a direction.
        /// </summary>
        /// <param name="tan">The transform from the canonical tangent space of the given direction
        /// to the canonical tangent space of the returned direction.</param>
        public Dir2 Apply(Dir2 dir, out Reflection1 tan)
        {
            Axis2 axis = dir.GetAxis(out Dir1 adir);
            Axis2 nAxis = Apply(axis, out PlanarRoflection2i local);
            tan = Reflection1.ByCode((byte)(local.X.Code ^ (int)axis ^ (int)nAxis));
            return nAxis.ToDir(local.Y * adir);
        }

        /// <summary>
        /// Applies this <see cref="Roflection2i"/> to an <see cref="Quadrant"/>.
        /// </summary>
        public Quadrant Apply(Quadrant quad)
        {
            // Taking advantage of the fact that the first 8 Rotation3i's correspond to Roflection2i's
            return (Quadrant)((int)Rotation3i.ByCode(Code).Apply((Octant)quad) & 0b11);
        }

        /// <summary>
        /// Applies this <see cref="Roflection2i"/> to a quartet, assuming elements are not affected by the transform.
        /// </summary>
        public Quartet<T> Apply<T>(Quartet<T> quartet)
        {
            // TODO: Convert to switch statement and inline for performance
            Quartet<T> nQuartet = new Quartet<T>();
            for (int i = 0; i < 4; i++)
            {
                var quad = (Quadrant)i;
                nQuartet[this * quad] = quartet[quad];
            }
            return nQuartet;
        }

        #region Elements
        public static Roflection2i PosX_PosY => new Roflection2i(0);
        public static Roflection2i PosY_PosX => new Roflection2i(1);
        public static Roflection2i NegY_PosX => new Roflection2i(2);
        public static Roflection2i NegX_PosY => new Roflection2i(3);
        public static Roflection2i NegX_NegY => new Roflection2i(4);
        public static Roflection2i NegY_NegX => new Roflection2i(5);
        public static Roflection2i PosY_NegX => new Roflection2i(6);
        public static Roflection2i PosX_NegY => new Roflection2i(7);
        #endregion

        public static Vector2 operator *(Roflection2i a, Vector2 b)
        {
            return a.Apply(b);
        }

        public static Vector2i operator *(Roflection2i a, Vector2i b)
        {
            return a.Apply(b);
        }

        public static Dir2 operator *(Roflection2i a, Dir2 b)
        {
            return a.Apply(b);
        }

        public static Quadrant operator *(Roflection2i a, Quadrant b)
        {
            return a.Apply(b);
        }

        public static Roflection2i operator *(Roflection2i a, Roflection2i b)
        {
            return Compose(a, b);
        }

        public static explicit operator Rotation2i(Roflection2i rof)
        {
            Debug.Assert(rof.IsRotation);
            return Rotation2i.ByCode((byte)(rof.Code >> 1));
        }

        public static implicit operator Roflection2i(Rotation2i rot)
        {
            return new Roflection2i((byte)(rot.Code << 1));
        }

        public static implicit operator Matrix2x2(Roflection2i rof)
        {
            return new Matrix2x2(
                rof.Apply(new Vector2(1, 0)),
                rof.Apply(new Vector2(0, 1)));
        }

        public static bool operator ==(Roflection2i a, Roflection2i b)
        {
            return a.Code == b.Code;
        }

        public static bool operator !=(Roflection2i a, Roflection2i b)
        {
            return a.Code != b.Code;
        }

        public override bool Equals(object obj)
        {
            if (obj is Roflection2i)
                return this == (Roflection2i)obj;
            return false;
        }

        bool IEquatable<Roflection2i>.Equals(Roflection2i other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return Code;
        }

        public override string ToString()
        {
            return _stringTable[Code];
        }

        /// <summary>
        /// The table used to implement <see cref="object.ToString"/>
        /// </summary>
        private static readonly string[] _stringTable = new string[]
        {
            "(+x, +y)",
            "(+y, +x)",
            "(-y, +x)",
            "(-x, +y)",
            "(-x, -y)",
            "(-y, -x)",
            "(+y, -x)",
            "(+x, -y)"
        };
    }

    /// <summary>
    /// Identifies one of the 24 rotation isometries in discrete three-dimensional space.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct Rotation3i : IEquatable<Rotation3i>
    {
        private Rotation3i(byte code)
        {
            Code = code;
        }

        /// <summary>
        /// The unique code used to identify this <see cref="Rotation3i"/>.
        /// </summary>
        public byte Code { get; }

        /// <summary>
        /// The number of distinct <see cref="Rotation3i"/>s.
        /// </summary>
        public const byte Count = 24;

        /// <summary>
        /// The inverse of this <see cref="Rotation3i"/>.
        /// </summary>
        public Rotation3i Inverse => _inverseTable[Code];

        /// <summary>
        /// The identity <see cref="Rotation3i"/>.
        /// </summary>
        public static Rotation3i Identity => PosX_PosY_PosZ;

        /// <summary>
        /// The set of all <see cref="Rotation3i"/>s.
        /// </summary>
        public static IEnumerable<Rotation3i> All => RotationSet3i.All;

        /// <summary>
        /// Gets the <see cref="Rotation3i"/> which applies the given <see cref="Rotation2i"/> on
        /// the given oriented axis.
        /// </summary>
        public static Rotation3i About(Dir3 dir, Rotation2i rot)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the unique <see cref="Rotation3i"/> which transforms the canonical local space of the given
        /// <see cref="Axis3"/> by the given <see cref="Roflection2i"/>, without transforming the axis
        /// itself.
        /// </summary>
        public static Rotation3i About(Axis3 axis, Roflection2i rof)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the <see cref="Rotation3i"/> with the given <see cref="Code"/>.
        /// </summary>
        public static Rotation3i ByCode(byte code)
        {
            Debug.Assert(code < Count);
            return new Rotation3i(code);
        }

        /// <summary>
        /// Composes two <see cref="Rotation3i"/>, first applying the right, then the left.
        /// </summary>
        public static Rotation3i Compose(Rotation3i a, Rotation3i b)
        {
            return _composeTable[a.Code * Count + b.Code];
        }

        /// <summary>
        /// Applies this <see cref="Rotation3i"/> to a vector.
        /// </summary>
        public Vector3 Apply(Vector3 vec)
        {
            switch (Code)
            {
                case 0: return new Vector3(vec.X, vec.Y, vec.Z);
                case 1: return new Vector3(vec.Y, vec.X, -vec.Z);
                case 2: return new Vector3(-vec.Y, vec.X, vec.Z);
                case 3: return new Vector3(-vec.X, vec.Y, -vec.Z);
                case 4: return new Vector3(-vec.X, -vec.Y, vec.Z);
                case 5: return new Vector3(-vec.Y, -vec.X, -vec.Z);
                case 6: return new Vector3(vec.Y, -vec.X, vec.Z);
                case 7: return new Vector3(vec.X, -vec.Y, -vec.Z);
                case 8: return new Vector3(vec.Z, vec.X, vec.Y);
                case 9: return new Vector3(vec.X, vec.Z, -vec.Y);
                case 10: return new Vector3(-vec.X, vec.Z, vec.Y);
                case 11: return new Vector3(-vec.Z, vec.X, -vec.Y);
                case 12: return new Vector3(-vec.Z, -vec.X, vec.Y);
                case 13: return new Vector3(-vec.X, -vec.Z, -vec.Y);
                case 14: return new Vector3(vec.X, -vec.Z, vec.Y);
                case 15: return new Vector3(vec.Z, -vec.X, -vec.Y);
                case 16: return new Vector3(vec.Y, vec.Z, vec.X);
                case 17: return new Vector3(vec.Z, vec.Y, -vec.X);
                case 18: return new Vector3(-vec.Z, vec.Y, vec.X);
                case 19: return new Vector3(-vec.Y, vec.Z, -vec.X);
                case 20: return new Vector3(-vec.Y, -vec.Z, vec.X);
                case 21: return new Vector3(-vec.Z, -vec.Y, -vec.X);
                case 22: return new Vector3(vec.Z, -vec.Y, vec.X);
                case 23: return new Vector3(vec.Y, -vec.Z, -vec.X);
                default: throw new ArgumentException();
            }
        }

        /// <summary>
        /// Applies this <see cref="Rotation3i"/> to a vector.
        /// </summary>
        public Vector3i Apply(Vector3i vec)
        {
            switch (Code)
            {
                case 0: return new Vector3i(vec.X, vec.Y, vec.Z);
                case 1: return new Vector3i(vec.Y, vec.X, -vec.Z);
                case 2: return new Vector3i(-vec.Y, vec.X, vec.Z);
                case 3: return new Vector3i(-vec.X, vec.Y, -vec.Z);
                case 4: return new Vector3i(-vec.X, -vec.Y, vec.Z);
                case 5: return new Vector3i(-vec.Y, -vec.X, -vec.Z);
                case 6: return new Vector3i(vec.Y, -vec.X, vec.Z);
                case 7: return new Vector3i(vec.X, -vec.Y, -vec.Z);
                case 8: return new Vector3i(vec.Z, vec.X, vec.Y);
                case 9: return new Vector3i(vec.X, vec.Z, -vec.Y);
                case 10: return new Vector3i(-vec.X, vec.Z, vec.Y);
                case 11: return new Vector3i(-vec.Z, vec.X, -vec.Y);
                case 12: return new Vector3i(-vec.Z, -vec.X, vec.Y);
                case 13: return new Vector3i(-vec.X, -vec.Z, -vec.Y);
                case 14: return new Vector3i(vec.X, -vec.Z, vec.Y);
                case 15: return new Vector3i(vec.Z, -vec.X, -vec.Y);
                case 16: return new Vector3i(vec.Y, vec.Z, vec.X);
                case 17: return new Vector3i(vec.Z, vec.Y, -vec.X);
                case 18: return new Vector3i(-vec.Z, vec.Y, vec.X);
                case 19: return new Vector3i(-vec.Y, vec.Z, -vec.X);
                case 20: return new Vector3i(-vec.Y, -vec.Z, vec.X);
                case 21: return new Vector3i(-vec.Z, -vec.Y, -vec.X);
                case 22: return new Vector3i(vec.Z, -vec.Y, vec.X);
                case 23: return new Vector3i(vec.Y, -vec.Z, -vec.X);
                default: throw new ArgumentException();
            }
        }

        /// <summary>
        /// Applies this <see cref="Rotation3i"/> to a vector.
        /// </summary>
        public ShortVector3i Apply(ShortVector3i vec)
        {
            switch (Code)
            {
                case 0: return new ShortVector3i(vec.X, vec.Y, vec.Z);
                case 1: return new ShortVector3i(vec.Y, vec.X, (short)-vec.Z);
                case 2: return new ShortVector3i((short)-vec.Y, vec.X, vec.Z);
                case 3: return new ShortVector3i((short)-vec.X, vec.Y, (short)-vec.Z);
                case 4: return new ShortVector3i((short)-vec.X, (short)-vec.Y, vec.Z);
                case 5: return new ShortVector3i((short)-vec.Y, (short)-vec.X, (short)-vec.Z);
                case 6: return new ShortVector3i(vec.Y, (short)-vec.X, vec.Z);
                case 7: return new ShortVector3i(vec.X, (short)-vec.Y, (short)-vec.Z);
                case 8: return new ShortVector3i(vec.Z, vec.X, vec.Y);
                case 9: return new ShortVector3i(vec.X, vec.Z, (short)-vec.Y);
                case 10: return new ShortVector3i((short)-vec.X, vec.Z, vec.Y);
                case 11: return new ShortVector3i((short)-vec.Z, vec.X, (short)-vec.Y);
                case 12: return new ShortVector3i((short)-vec.Z, (short)-vec.X, vec.Y);
                case 13: return new ShortVector3i((short)-vec.X, (short)-vec.Z, (short)-vec.Y);
                case 14: return new ShortVector3i(vec.X, (short)-vec.Z, vec.Y);
                case 15: return new ShortVector3i(vec.Z, (short)-vec.X, (short)-vec.Y);
                case 16: return new ShortVector3i(vec.Y, vec.Z, vec.X);
                case 17: return new ShortVector3i(vec.Z, vec.Y, (short)-vec.X);
                case 18: return new ShortVector3i((short)-vec.Z, vec.Y, vec.X);
                case 19: return new ShortVector3i((short)-vec.Y, vec.Z, (short)-vec.X);
                case 20: return new ShortVector3i((short)-vec.Y, (short)-vec.Z, vec.X);
                case 21: return new ShortVector3i((short)-vec.Z, (short)-vec.Y, (short)-vec.X);
                case 22: return new ShortVector3i(vec.Z, (short)-vec.Y, vec.X);
                case 23: return new ShortVector3i(vec.Y, (short)-vec.Z, (short)-vec.X);
                default: throw new ArgumentException();
            }
        }

        /// <summary>
        /// Applies this <see cref="Rotation3i"/> to an <see cref="Octant"/>.
        /// </summary>
        public Octant Apply(Octant octant)
        {
            return _octantTable[Code * 8 + (int)octant];
        }

        /// <summary>
        /// Applies this <see cref="Rotation3i"/> to an octet, assuming elements are not affected by the transform.
        /// </summary>
        public Octet<T> Apply<T>(Octet<T> octet)
        {
            // TODO: Convert to switch statement and inline for performance
            Octet<T> nOctet = new Octet<T>();
            for (int i = 0; i < 8; i++)
            {
                var octant = (Octant)i;
                nOctet[Apply(octant)] = octet[octant];
            }
            return nOctet;
        }

        /// <summary>
        /// Applies this <see cref="Rotation3i"/> to an axis.
        /// </summary>
        public Axis3 Apply(Axis3 axis)
        {
            return Apply(axis, out _);
        }

        /// <summary>
        /// Applies this <see cref="Rotation3i"/> to an axis.
        /// </summary>
        /// <param name="local">The transform from the canonical local space of the given axis to
        /// the canonical local space of the returned axis.</param>
        public Axis3 Apply(Axis3 axis, out PlanarRotation3i local)
        {
            (axis, local) = _axisTable[Code * 3 + (int)axis];
            return axis;
        }

        /// <summary>
        /// Applies this <see cref="Rotation3i"/> to a <see cref="Dir3"/>.
        /// </summary>
        public Dir3 Apply(Dir3 dir)
        {
            return Apply(dir, out _);
        }

        /// <summary>
        /// Applies this <see cref="Rotation3i"/> to a <see cref="Dir3"/>.
        /// </summary>
        /// <param name="tan">The transform from the canonical tangent space of the given direction
        /// to the canonical tangent space of the returned direction.</param>
        public Dir3 Apply(Dir3 dir, out Rotation2i tan)
        {
            Axis3 axis = dir.GetAxis(out Dir1 adir);
            Axis3 nAxis = Apply(axis, out PlanarRotation3i local);
            Dir1 nAdir = local.Apply_Z(adir, out tan);
            return nAxis.ToDir(nAdir);
        }

        /// <summary>
        /// Applies this <see cref="Rotation3i"/> to a <see cref="AxisQuadrant"/>.
        /// </summary>
        public AxisQuadrant Apply(AxisQuadrant axisQuad)
        {
            Axis3 axis = axisQuad.GetAxis(out Quadrant quad);
            return Apply(axis, out PlanarRotation3i local).ToAxisQuadrant(local.X_Y * quad);
        }

        /// <summary>
        /// Applies this <see cref="Roflection3i"/> to an <see cref="AxialDuet3{T}"/>, assuming elements
        /// are transformation invariant.
        /// </summary>
        public AxialDuet3<T> Apply<T>(AxialDuet3<T> axisDuet)
        {
            // TODO: Convert to switch statement and inline for performance
            AxialDuet3<T> nAxisDuet = new AxialDuet3<T>();
            for (int i = 0; i < 6; i++)
            {
                var dir = (Dir3)i;
                nAxisDuet[Apply(dir)] = axisDuet[dir];
            }
            return nAxisDuet;
        }

        /// <summary>
        /// Applies this <see cref="Roflection3i"/> to an <see cref="AxialQuartet{T}"/>, assuming elements
        /// are transformation invariant.
        /// </summary>
        public AxialQuartet<T> Apply<T>(AxialQuartet<T> axisQuartet)
        {
            // TODO: Convert to switch statement and inline for performance
            AxialQuartet<T> nAxisQuartet = new AxialQuartet<T>();
            for (int i = 0; i < 12; i++)
            {
                var axisQuad = (AxisQuadrant)i;
                nAxisQuartet[Apply(axisQuad)] = axisQuartet[axisQuad];
            }
            return nAxisQuartet;
        }

        #region Elements
        public static Rotation3i PosX_PosY_PosZ => new Rotation3i(0);
        public static Rotation3i PosY_PosX_NegZ => new Rotation3i(1);
        public static Rotation3i NegY_PosX_PosZ => new Rotation3i(2);
        public static Rotation3i NegX_PosY_NegZ => new Rotation3i(3);
        public static Rotation3i NegX_NegY_PosZ => new Rotation3i(4);
        public static Rotation3i NegY_NegX_NegZ => new Rotation3i(5);
        public static Rotation3i PosY_NegX_PosZ => new Rotation3i(6);
        public static Rotation3i PosX_NegY_NegZ => new Rotation3i(7);
        public static Rotation3i PosZ_PosX_PosY => new Rotation3i(8);
        public static Rotation3i PosX_PosZ_NegY => new Rotation3i(9);
        public static Rotation3i NegX_PosZ_PosY => new Rotation3i(10);
        public static Rotation3i NegZ_PosX_NegY => new Rotation3i(11);
        public static Rotation3i NegZ_NegX_PosY => new Rotation3i(12);
        public static Rotation3i NegX_NegZ_NegY => new Rotation3i(13);
        public static Rotation3i PosX_NegZ_PosY => new Rotation3i(14);
        public static Rotation3i PosZ_NegX_NegY => new Rotation3i(15);
        public static Rotation3i PosY_PosZ_PosX => new Rotation3i(16);
        public static Rotation3i PosZ_PosY_NegX => new Rotation3i(17);
        public static Rotation3i NegZ_PosY_PosX => new Rotation3i(18);
        public static Rotation3i NegY_PosZ_NegX => new Rotation3i(19);
        public static Rotation3i NegY_NegZ_PosX => new Rotation3i(20);
        public static Rotation3i NegZ_NegY_NegX => new Rotation3i(21);
        public static Rotation3i PosZ_NegY_PosX => new Rotation3i(22);
        public static Rotation3i PosY_NegZ_NegX => new Rotation3i(23);
        #endregion

        /// <summary>
        /// The table used to implement <see cref="Inverse"/>.
        /// </summary>
        private static readonly Rotation3i[] _inverseTable;

        /// <summary>
        /// The table used to implement <see cref="Compose"/>
        /// </summary>
        private static readonly Rotation3i[] _composeTable;

        /// <summary>
        /// The table used to implement <see cref="Apply(Octant)"/>.
        /// </summary>
        private static readonly Octant[] _octantTable;

        /// <summary>
        /// The table used to implement <see cref="Apply(Axis3, out PlanarRotation3i)"/>
        /// </summary>
        private static readonly (Axis3, PlanarRotation3i)[] _axisTable;

        static Rotation3i()
        {
            // Create a representation of rotations as vectors
            Vector3i id = new Vector3i(1, 2, 3);
            var rots = new Dictionary<Vector3i, Rotation3i>();
            for (byte code = 0; code < Rotation3i.Count; code++)
            {
                Rotation3i rot = new Rotation3i(code);
                rots.Add(rot.Apply(id), rot);
            }

            // Create a representation of octants as vectors
            var octants = new Dictionary<Vector3i, Octant>();
            for (int i = 0; i < 8; i++)
            {
                Octant octant = (Octant)i;
                octants.Add(octant.ToVector(), octant);
            }

            // Create a representation of directions as vectors
            var dirs = new Dictionary<Vector3i, Dir3>();
            for (int i = 0; i < 6; i++)
            {
                Dir3 dir = (Dir3)i;
                dirs.Add(dir, dir);
            }

            // Build operation tables
            _inverseTable = new Rotation3i[Count];
            _composeTable = new Rotation3i[Count * Count];
            _octantTable = new Octant[Count * 8];
            for (byte i = 0; i < Count; i++)
            {
                Rotation3i a = new Rotation3i(i);

                // Build compose and inverse tables
                for (byte j = 0; j < Count; j++)
                {
                    Rotation3i b = new Rotation3i(j);
                    var composed = rots[a.Apply(b.Apply(id))];
                    _composeTable[i * Count + j] = composed;
                    if (composed == Identity)
                        _inverseTable[i] = b;
                }

                // Build octant table
                for (int j = 0; j < 8; j++)
                {
                    Octant octant = (Octant)j;
                    _octantTable[i * 8 + j] = octants[a.Apply(octant.ToVector())];
                }
            }

            // Build axis table
            _axisTable = new (Axis3, PlanarRotation3i)[Count * 3];
            for (byte i = 0; i < PlanarRotation3i.Count; i++)
            {
                PlanarRotation3i local = PlanarRotation3i.ByCode(i);
                int trans_x = local.Z.Apply(1);
                int trans_y = local.Z.Apply(2);
                int trans_z = local.Z.Apply(3);
                Vector2i trans_x_y = local.X_Y.Apply(new Vector2i(1, 2));
                Vector2i trans_y_z = local.X_Y.Apply(new Vector2i(2, 3));
                Vector2i trans_z_x = local.X_Y.Apply(new Vector2i(3, 1));
                _axisTable[rots[new Vector3i(trans_x, trans_y_z)].Code * 3 + 0] = (Axis3.X, local);
                _axisTable[rots[new Vector3i(trans_y_z.Y, trans_x, trans_y_z.X)].Code * 3 + 0] = (Axis3.Y, local);
                _axisTable[rots[new Vector3i(trans_y_z, trans_x)].Code * 3 + 0] = (Axis3.Z, local);
                _axisTable[rots[new Vector3i(trans_y, trans_z_x)].Code * 3 + 1] = (Axis3.X, local);
                _axisTable[rots[new Vector3i(trans_z_x.Y, trans_y, trans_z_x.X)].Code * 3 + 1] = (Axis3.Y, local);
                _axisTable[rots[new Vector3i(trans_z_x, trans_y)].Code * 3 + 1] = (Axis3.Z, local);
                _axisTable[rots[new Vector3i(trans_z, trans_x_y)].Code * 3 + 2] = (Axis3.X, local);
                _axisTable[rots[new Vector3i(trans_x_y.Y, trans_z, trans_x_y.X)].Code * 3 + 2] = (Axis3.Y, local);
                _axisTable[rots[new Vector3i(trans_x_y, trans_z)].Code * 3 + 2] = (Axis3.Z, local);
            }
        }

        public static Rotation3i operator *(Rotation3i a, Rotation3i b)
        {
            return Compose(a, b);
        }

        public static Vector3 operator *(Rotation3i a, Vector3 b)
        {
            return a.Apply(b);
        }

        public static Vector3i operator *(Rotation3i a, Vector3i b)
        {
            return a.Apply(b);
        }

        public static ShortVector3i operator *(Rotation3i a, ShortVector3i b)
        {
            return a.Apply(b);
        }

        public static implicit operator Rotation3(Rotation3i rot)
        {
            return _rotTable[rot.Code];
        }
        
        private static readonly Scalar _sq = Scalar.Sqrt(2) / 2;

        /// <summary>
        /// The table used to implement the conversion to <see cref="Rotation3"/>.
        /// </summary>
        private static readonly Rotation3[] _rotTable = new Rotation3[]
        {
            new Rotation3(1, 0, 0, 0),
            new Rotation3(0, _sq, _sq, 0),
            new Rotation3(_sq, 0, 0, _sq),
            new Rotation3(0, 0, 1, 0),
            new Rotation3(0, 0, 0, 1),
            new Rotation3(0, _sq, -_sq, 0),
            new Rotation3(_sq, 0, 0, -_sq),
            new Rotation3(0, 1, 0, 0),
            new Rotation3(0.5, 0.5, 0.5, 0.5),
            new Rotation3(_sq, -_sq, 0, 0),
            new Rotation3(0, 0, _sq, _sq),
            new Rotation3(0.5, -0.5, -0.5, 0.5),
            new Rotation3(0.5, 0.5, -0.5, -0.5),
            new Rotation3(0, 0, -_sq, _sq),
            new Rotation3(_sq, _sq, 0, 0),
            new Rotation3(0.5, -0.5, 0.5, -0.5),
            new Rotation3(0.5, -0.5, -0.5, -0.5),
            new Rotation3(_sq, 0, _sq, 0),
            new Rotation3(_sq, 0, -_sq, 0),
            new Rotation3(0.5, -0.5, 0.5, 0.5),
            new Rotation3(0.5, 0.5, -0.5, 0.5),
            new Rotation3(0, _sq, 0, -_sq),
            new Rotation3(0, _sq, 0, _sq),
            new Rotation3(0.5, 0.5, 0.5, -0.5)
        };

        public static implicit operator Motion3(Rotation3i rot)
        {
            return (Rotation3)rot;
        }

        public static implicit operator Matrix3x3(Rotation3i rot)
        {
            return new Matrix3x3(
                rot.Apply(new Vector3(1, 0, 0)),
                rot.Apply(new Vector3(0, 1, 0)),
                rot.Apply(new Vector3(0, 0, 1)));
        }

        public static bool operator ==(Rotation3i a, Rotation3i b)
        {
            return a.Code == b.Code;
        }

        public static bool operator !=(Rotation3i a, Rotation3i b)
        {
            return a.Code != b.Code;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Rotation3i))
                return false;
            return this == (Rotation3i)obj;
        }

        bool IEquatable<Rotation3i>.Equals(Rotation3i other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return Code.GetHashCode();
        }

        public override string ToString()
        {
            return ((Roflection3i)this).ToString();
        }
    }

    /// <summary>
    /// Identifies one of the 48 rotation/reflection isometries in discrete three-dimensional space.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct Roflection3i : IEquatable<Roflection3i>
    {
        private Roflection3i(byte code)
        {
            Code = code;
        }

        public Roflection3i(Reflection1 x, Reflection1 y, Reflection1 z)
            : this(new Roflection2i(x, y), z)
        { }

        public Roflection3i(Roflection2i x_y, Reflection1 z)
        {
            this = new PlanarRoflection3i(x_y, z);
        }

        /// <summary>
        /// The unique code used to identify this <see cref="Roflection3i"/>, guaranteed to
        /// be less than <see cref="Count"/>.
        /// </summary>
        public byte Code { get; }

        /// <summary>
        /// The number of distinct <see cref="Roflection3i"/>s.
        /// </summary>
        public const byte Count = 48;

        /// <summary>
        /// The reflection component of this <see cref="Roflection3i"/>.
        /// </summary>
        public Reflection1 Reflection => Reflection1.ByCode((byte)(Code & 0b1));

        /// <summary>
        /// Indicates whether this <see cref="Roflection3i"/> is a <see cref="Rotation3i"/>.
        /// </summary>
        public bool IsRotation => Reflection == Reflection1.Identity;

        /// <summary>
        /// The rotation component of this <see cref="Roflection3i"/>.
        /// </summary>
        private Rotation3i _rot => Rotation3i.ByCode((byte)(Code >> 1));

        /// <summary>
        /// Indicates whether this <see cref="Roflection3i"/> involves a reflection.
        /// </summary>
        private bool _reflects => (Code & 0b1) != 0;

        /// <summary>
        /// The inverse of this <see cref="Roflection3i"/>.
        /// </summary>
        public Roflection3i Inverse
        {
            get
            {
                int rot = Rotation3i.ByCode((byte)(Code >> 1)).Inverse.Code;
                int refl = Code & 0b1;
                return new Roflection3i((byte)((rot << 1) | refl));
            }
        }

        /// <summary>
        /// The identity <see cref="Roflection3i"/>.
        /// </summary>
        public static Roflection3i Identity => PosX_PosY_PosZ;

        /// <summary>
        /// The set of all <see cref="Roflection3i"/>s.
        /// </summary>
        public static IEnumerable<Roflection3i> All
        {
            get
            {
                for (byte i = 0; i < Count; i++)
                    yield return new Roflection3i(i);
            }
        }

        /// <summary>
        /// Gets the <see cref="Roflection3i"/> with the given <see cref="Code"/>.
        /// </summary>
        public static Roflection3i ByCode(byte code)
        {
            Debug.Assert(code < Count);
            return new Roflection3i(code);
        }

        /// <summary>
        /// Composes two <see cref="Roflection3i"/>, first applying the right, then the left.
        /// </summary>
        public static Roflection3i Compose(Roflection3i a, Roflection3i b)
        {
            Rotation3i aRot = Rotation3i.ByCode((byte)(a.Code >> 1));
            Rotation3i bRot = Rotation3i.ByCode((byte)(b.Code >> 1));
            int rot = Rotation3i.Compose(aRot, bRot).Code;
            int refl = (a.Code ^ b.Code) & 0b1;
            return new Roflection3i((byte)((rot << 1) | refl));
        }

        /// <summary>
        /// Applies this <see cref="Roflection3i"/> to a vector.
        /// </summary>
        public Vector3 Apply(Vector3 vec)
        {
            switch (Code)
            {
                case 0: return new Vector3(vec.X, vec.Y, vec.Z);
                case 1: return new Vector3(-vec.X, -vec.Y, -vec.Z);
                case 2: return new Vector3(vec.Y, vec.X, -vec.Z);
                case 3: return new Vector3(-vec.Y, -vec.X, vec.Z);
                case 4: return new Vector3(-vec.Y, vec.X, vec.Z);
                case 5: return new Vector3(vec.Y, -vec.X, -vec.Z);
                case 6: return new Vector3(-vec.X, vec.Y, -vec.Z);
                case 7: return new Vector3(vec.X, -vec.Y, vec.Z);
                case 8: return new Vector3(-vec.X, -vec.Y, vec.Z);
                case 9: return new Vector3(vec.X, vec.Y, -vec.Z);
                case 10: return new Vector3(-vec.Y, -vec.X, -vec.Z);
                case 11: return new Vector3(vec.Y, vec.X, vec.Z);
                case 12: return new Vector3(vec.Y, -vec.X, vec.Z);
                case 13: return new Vector3(-vec.Y, vec.X, -vec.Z);
                case 14: return new Vector3(vec.X, -vec.Y, -vec.Z);
                case 15: return new Vector3(-vec.X, vec.Y, vec.Z);
                case 16: return new Vector3(vec.Z, vec.X, vec.Y);
                case 17: return new Vector3(-vec.Z, -vec.X, -vec.Y);
                case 18: return new Vector3(vec.X, vec.Z, -vec.Y);
                case 19: return new Vector3(-vec.X, -vec.Z, vec.Y);
                case 20: return new Vector3(-vec.X, vec.Z, vec.Y);
                case 21: return new Vector3(vec.X, -vec.Z, -vec.Y);
                case 22: return new Vector3(-vec.Z, vec.X, -vec.Y);
                case 23: return new Vector3(vec.Z, -vec.X, vec.Y);
                case 24: return new Vector3(-vec.Z, -vec.X, vec.Y);
                case 25: return new Vector3(vec.Z, vec.X, -vec.Y);
                case 26: return new Vector3(-vec.X, -vec.Z, -vec.Y);
                case 27: return new Vector3(vec.X, vec.Z, vec.Y);
                case 28: return new Vector3(vec.X, -vec.Z, vec.Y);
                case 29: return new Vector3(-vec.X, vec.Z, -vec.Y);
                case 30: return new Vector3(vec.Z, -vec.X, -vec.Y);
                case 31: return new Vector3(-vec.Z, vec.X, vec.Y);
                case 32: return new Vector3(vec.Y, vec.Z, vec.X);
                case 33: return new Vector3(-vec.Y, -vec.Z, -vec.X);
                case 34: return new Vector3(vec.Z, vec.Y, -vec.X);
                case 35: return new Vector3(-vec.Z, -vec.Y, vec.X);
                case 36: return new Vector3(-vec.Z, vec.Y, vec.X);
                case 37: return new Vector3(vec.Z, -vec.Y, -vec.X);
                case 38: return new Vector3(-vec.Y, vec.Z, -vec.X);
                case 39: return new Vector3(vec.Y, -vec.Z, vec.X);
                case 40: return new Vector3(-vec.Y, -vec.Z, vec.X);
                case 41: return new Vector3(vec.Y, vec.Z, -vec.X);
                case 42: return new Vector3(-vec.Z, -vec.Y, -vec.X);
                case 43: return new Vector3(vec.Z, vec.Y, vec.X);
                case 44: return new Vector3(vec.Z, -vec.Y, vec.X);
                case 45: return new Vector3(-vec.Z, vec.Y, -vec.X);
                case 46: return new Vector3(vec.Y, -vec.Z, -vec.X);
                case 47: return new Vector3(-vec.Y, vec.Z, vec.X);
                default: throw new ArgumentException();
            }
        }

        /// <summary>
        /// Applies this <see cref="Roflection3i"/> to a vector.
        /// </summary>
        public Vector3i Apply(Vector3i vec)
        {
            switch (Code)
            {
                case 0: return new Vector3i(vec.X, vec.Y, vec.Z);
                case 1: return new Vector3i(-vec.X, -vec.Y, -vec.Z);
                case 2: return new Vector3i(vec.Y, vec.X, -vec.Z);
                case 3: return new Vector3i(-vec.Y, -vec.X, vec.Z);
                case 4: return new Vector3i(-vec.Y, vec.X, vec.Z);
                case 5: return new Vector3i(vec.Y, -vec.X, -vec.Z);
                case 6: return new Vector3i(-vec.X, vec.Y, -vec.Z);
                case 7: return new Vector3i(vec.X, -vec.Y, vec.Z);
                case 8: return new Vector3i(-vec.X, -vec.Y, vec.Z);
                case 9: return new Vector3i(vec.X, vec.Y, -vec.Z);
                case 10: return new Vector3i(-vec.Y, -vec.X, -vec.Z);
                case 11: return new Vector3i(vec.Y, vec.X, vec.Z);
                case 12: return new Vector3i(vec.Y, -vec.X, vec.Z);
                case 13: return new Vector3i(-vec.Y, vec.X, -vec.Z);
                case 14: return new Vector3i(vec.X, -vec.Y, -vec.Z);
                case 15: return new Vector3i(-vec.X, vec.Y, vec.Z);
                case 16: return new Vector3i(vec.Z, vec.X, vec.Y);
                case 17: return new Vector3i(-vec.Z, -vec.X, -vec.Y);
                case 18: return new Vector3i(vec.X, vec.Z, -vec.Y);
                case 19: return new Vector3i(-vec.X, -vec.Z, vec.Y);
                case 20: return new Vector3i(-vec.X, vec.Z, vec.Y);
                case 21: return new Vector3i(vec.X, -vec.Z, -vec.Y);
                case 22: return new Vector3i(-vec.Z, vec.X, -vec.Y);
                case 23: return new Vector3i(vec.Z, -vec.X, vec.Y);
                case 24: return new Vector3i(-vec.Z, -vec.X, vec.Y);
                case 25: return new Vector3i(vec.Z, vec.X, -vec.Y);
                case 26: return new Vector3i(-vec.X, -vec.Z, -vec.Y);
                case 27: return new Vector3i(vec.X, vec.Z, vec.Y);
                case 28: return new Vector3i(vec.X, -vec.Z, vec.Y);
                case 29: return new Vector3i(-vec.X, vec.Z, -vec.Y);
                case 30: return new Vector3i(vec.Z, -vec.X, -vec.Y);
                case 31: return new Vector3i(-vec.Z, vec.X, vec.Y);
                case 32: return new Vector3i(vec.Y, vec.Z, vec.X);
                case 33: return new Vector3i(-vec.Y, -vec.Z, -vec.X);
                case 34: return new Vector3i(vec.Z, vec.Y, -vec.X);
                case 35: return new Vector3i(-vec.Z, -vec.Y, vec.X);
                case 36: return new Vector3i(-vec.Z, vec.Y, vec.X);
                case 37: return new Vector3i(vec.Z, -vec.Y, -vec.X);
                case 38: return new Vector3i(-vec.Y, vec.Z, -vec.X);
                case 39: return new Vector3i(vec.Y, -vec.Z, vec.X);
                case 40: return new Vector3i(-vec.Y, -vec.Z, vec.X);
                case 41: return new Vector3i(vec.Y, vec.Z, -vec.X);
                case 42: return new Vector3i(-vec.Z, -vec.Y, -vec.X);
                case 43: return new Vector3i(vec.Z, vec.Y, vec.X);
                case 44: return new Vector3i(vec.Z, -vec.Y, vec.X);
                case 45: return new Vector3i(-vec.Z, vec.Y, -vec.X);
                case 46: return new Vector3i(vec.Y, -vec.Z, -vec.X);
                case 47: return new Vector3i(-vec.Y, vec.Z, vec.X);
                default: throw new ArgumentException();
            }
        }

        /// <summary>
        /// Applies this <see cref="Roflection3i"/> to an <see cref="Octant"/>.
        /// </summary>
        public Octant Apply(Octant octant)
        {
            octant = Rotation3i.ByCode((byte)(Code >> 1)).Apply(octant);
            int reflMask = -(Code & 0b1) & 0b111;
            return (Octant)((int)octant ^ reflMask);
        }

        /// <summary>
        /// Applies this <see cref="Roflection3i"/> to an octet, assuming elements are not affected by the transform.
        /// </summary>
        public Octet<T> Apply<T>(Octet<T> octet)
        {
            // TODO: Convert to switch statement and inline for performance
            Octet<T> nOctet = new Octet<T>();
            for (int i = 0; i < 8; i++)
            {
                var octant = (Octant)i;
                nOctet[Apply(octant)] = octet[octant];
            }
            return nOctet;
        }

        /// <summary>
        /// Applies this <see cref="Roflection3i"/> to an axis.
        /// </summary>
        public Axis3 Apply(Axis3 axis)
        {
            return Apply(axis, out _);
        }

        /// <summary>
        /// Applies this <see cref="Roflection3i"/> to an axis.
        /// </summary>
        /// <param name="local">The transform from the canonical local space of the given axis to
        /// the canonical local space of the returned axis.</param>
        public Axis3 Apply(Axis3 axis, out PlanarRoflection3i local)
        {
            Axis3 nAxis = Rotation3i.ByCode((byte)(Code >> 1)).Apply(axis, out PlanarRotation3i rotLocal);
            int refl = Code & 0b1;
            local = PlanarRoflection3i.ByCode((byte)((rotLocal.Code << 1) | refl));
            return nAxis;
        }

        /// <summary>
        /// Applies this <see cref="Roflection3i"/> to a <see cref="Dir3"/>.
        /// </summary>
        public Dir3 Apply(Dir3 dir)
        {
            return Apply(dir, out _);
        }

        /// <summary>
        /// Applies this <see cref="Roflection3i"/> to a <see cref="Dir3"/>.
        /// </summary>
        /// <param name="tan">The transform from the canonical tangent space of the given direction
        /// to the canonical tangent space of the returned direction.</param>
        public Dir3 Apply(Dir3 dir, out Roflection2i tan)
        {
            Axis3 axis = dir.GetAxis(out Dir1 adir);
            Axis3 nAxis = Apply(axis, out PlanarRoflection3i local);
            Dir1 nAdir = local.Apply_Z(adir, out tan);
            return nAxis.ToDir(nAdir);
        }

        /// <summary>
        /// Applies this <see cref="Roflection3i"/> to a <see cref="AxisQuadrant"/>.
        /// </summary>
        public AxisQuadrant Apply(AxisQuadrant axisQuad)
        {
            Axis3 axis = axisQuad.GetAxis(out Quadrant quad);
            return Apply(axis, out PlanarRoflection3i local).ToAxisQuadrant(local.X_Y * quad);
        }

        /// <summary>
        /// Applies this <see cref="Roflection3i"/> to an <see cref="AxialDuet3{T}"/>, assuming elements
        /// are transformation invariant.
        /// </summary>
        public AxialDuet3<T> Apply<T>(AxialDuet3<T> axisDuet)
        {
            // TODO: Convert to switch statement and inline for performance
            AxialDuet3<T> nAxisDuet = new AxialDuet3<T>();
            for (int i = 0; i < 6; i++)
            {
                var dir = (Dir3)i;
                nAxisDuet[Apply(dir)] = axisDuet[dir];
            }
            return nAxisDuet;
        }

        /// <summary>
        /// Applies this <see cref="Roflection3i"/> to an <see cref="AxialQuartet{T}"/>, assuming elements
        /// are transformation invariant.
        /// </summary>
        public AxialQuartet<T> Apply<T>(AxialQuartet<T> axisQuartet)
        {
            // TODO: Convert to switch statement and inline for performance
            AxialQuartet<T> nAxisQuartet = new AxialQuartet<T>();
            for (int i = 0; i < 12; i++)
            {
                var axisQuad = (AxisQuadrant)i;
                nAxisQuartet[Apply(axisQuad)] = axisQuartet[axisQuad];
            }
            return nAxisQuartet;
        }

        #region Elements
        public static Roflection3i PosX_PosY_PosZ => new Roflection3i(0);
        public static Roflection3i NegX_NegY_NegZ => new Roflection3i(1);
        public static Roflection3i PosY_PosX_NegZ => new Roflection3i(2);
        public static Roflection3i NegY_NegX_PosZ => new Roflection3i(3);
        public static Roflection3i NegY_PosX_PosZ => new Roflection3i(4);
        public static Roflection3i PosY_NegX_NegZ => new Roflection3i(5);
        public static Roflection3i NegX_PosY_NegZ => new Roflection3i(6);
        public static Roflection3i PosX_NegY_PosZ => new Roflection3i(7);
        public static Roflection3i NegX_NegY_PosZ => new Roflection3i(8);
        public static Roflection3i PosX_PosY_NegZ => new Roflection3i(9);
        public static Roflection3i NegY_NegX_NegZ => new Roflection3i(10);
        public static Roflection3i PosY_PosX_PosZ => new Roflection3i(11);
        public static Roflection3i PosY_NegX_PosZ => new Roflection3i(12);
        public static Roflection3i NegY_PosX_NegZ => new Roflection3i(13);
        public static Roflection3i PosX_NegY_NegZ => new Roflection3i(14);
        public static Roflection3i NegX_PosY_PosZ => new Roflection3i(15);
        public static Roflection3i PosZ_PosX_PosY => new Roflection3i(16);
        public static Roflection3i NegZ_NegX_NegY => new Roflection3i(17);
        public static Roflection3i PosX_PosZ_NegY => new Roflection3i(18);
        public static Roflection3i NegX_NegZ_PosY => new Roflection3i(19);
        public static Roflection3i NegX_PosZ_PosY => new Roflection3i(20);
        public static Roflection3i PosX_NegZ_NegY => new Roflection3i(21);
        public static Roflection3i NegZ_PosX_NegY => new Roflection3i(22);
        public static Roflection3i PosZ_NegX_PosY => new Roflection3i(23);
        public static Roflection3i NegZ_NegX_PosY => new Roflection3i(24);
        public static Roflection3i PosZ_PosX_NegY => new Roflection3i(25);
        public static Roflection3i NegX_NegZ_NegY => new Roflection3i(26);
        public static Roflection3i PosX_PosZ_PosY => new Roflection3i(27);
        public static Roflection3i PosX_NegZ_PosY => new Roflection3i(28);
        public static Roflection3i NegX_PosZ_NegY => new Roflection3i(29);
        public static Roflection3i PosZ_NegX_NegY => new Roflection3i(30);
        public static Roflection3i NegZ_PosX_PosY => new Roflection3i(31);
        public static Roflection3i PosY_PosZ_PosX => new Roflection3i(32);
        public static Roflection3i NegY_NegZ_NegX => new Roflection3i(33);
        public static Roflection3i PosZ_PosY_NegX => new Roflection3i(34);
        public static Roflection3i NegZ_NegY_PosX => new Roflection3i(35);
        public static Roflection3i NegZ_PosY_PosX => new Roflection3i(36);
        public static Roflection3i PosZ_NegY_NegX => new Roflection3i(37);
        public static Roflection3i NegY_PosZ_NegX => new Roflection3i(38);
        public static Roflection3i PosY_NegZ_PosX => new Roflection3i(39);
        public static Roflection3i NegY_NegZ_PosX => new Roflection3i(40);
        public static Roflection3i PosY_PosZ_NegX => new Roflection3i(41);
        public static Roflection3i NegZ_NegY_NegX => new Roflection3i(42);
        public static Roflection3i PosZ_PosY_PosX => new Roflection3i(43);
        public static Roflection3i PosZ_NegY_PosX => new Roflection3i(44);
        public static Roflection3i NegZ_PosY_NegX => new Roflection3i(45);
        public static Roflection3i PosY_NegZ_NegX => new Roflection3i(46);
        public static Roflection3i NegY_PosZ_PosX => new Roflection3i(47);
        #endregion

        public static Vector3 operator *(Roflection3i a, Vector3 b)
        {
            return a.Apply(b);
        }

        public static Vector3i operator *(Roflection3i a, Vector3i b)
        {
            return a.Apply(b);
        }

        public static Octant operator *(Roflection3i a, Octant b)
        {
            return a.Apply(b);
        }

        public static Dir3 operator *(Roflection3i a, Dir3 b)
        {
            return a.Apply(b);
        }

        public static AxisQuadrant operator *(Roflection3i a, AxisQuadrant b)
        {
            return a.Apply(b);
        }

        public static Roflection3i operator *(Roflection3i a, Roflection3i b)
        {
            return Compose(a, b);
        }

        public static explicit operator Rotation3i(Roflection3i rof)
        {
            if (!rof.IsRotation)
                throw new InvalidCastException("Roflection is not a rotation");
            return rof._rot;
        }

        public static implicit operator Roflection3i(Rotation3i rot)
        {
            return new Roflection3i((byte)(rot.Code << 1));
        }

        public static implicit operator Roflection3(Roflection3i rof)
        {
            return new Roflection3(rof._rot, rof._reflects);
        }

        public static implicit operator Matrix3x3(Roflection3i rof)
        {
            return new Matrix3x3(
                rof.Apply(new Vector3(1, 0, 0)),
                rof.Apply(new Vector3(0, 1, 0)),
                rof.Apply(new Vector3(0, 0, 1)));
        }

        public static bool operator ==(Roflection3i a, Roflection3i b)
        {
            return a.Code == b.Code;
        }

        public static bool operator !=(Roflection3i a, Roflection3i b)
        {
            return a.Code != b.Code;
        }

        public override bool Equals(object obj)
        {
            if (obj is Roflection3i)
                return this == (Roflection3i)obj;
            return false;
        }

        bool IEquatable<Roflection3i>.Equals(Roflection3i other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return Code;
        }

        public override string ToString()
        {
            return _stringTable[Code];
        }

        /// <summary>
        /// The table used to implement <see cref="object.ToString"/>
        /// </summary>
        private static readonly string[] _stringTable = new string[]
        {
            "(+x, +y, +z)",
            "(-x, -y, -z)",
            "(+y, +x, -z)",
            "(-y, -x, +z)",
            "(-y, +x, +z)",
            "(+y, -x, -z)",
            "(-x, +y, -z)",
            "(+x, -y, +z)",
            "(-x, -y, +z)",
            "(+x, +y, -z)",
            "(-y, -x, -z)",
            "(+y, +x, +z)",
            "(+y, -x, +z)",
            "(-y, +x, -z)",
            "(+x, -y, -z)",
            "(-x, +y, +z)",
            "(+z, +x, +y)",
            "(-z, -x, -y)",
            "(+x, +z, -y)",
            "(-x, -z, +y)",
            "(-x, +z, +y)",
            "(+x, -z, -y)",
            "(-z, +x, -y)",
            "(+z, -x, +y)",
            "(-z, -x, +y)",
            "(+z, +x, -y)",
            "(-x, -z, -y)",
            "(+x, +z, +y)",
            "(+x, -z, +y)",
            "(-x, +z, -y)",
            "(+z, -x, -y)",
            "(-z, +x, +y)",
            "(+y, +z, +x)",
            "(-y, -z, -x)",
            "(+z, +y, -x)",
            "(-z, -y, +x)",
            "(-z, +y, +x)",
            "(+z, -y, -x)",
            "(-y, +z, -x)",
            "(+y, -z, +x)",
            "(-y, -z, +x)",
            "(+y, +z, -x)",
            "(-z, -y, -x)",
            "(+z, +y, +x)",
            "(+z, -y, +x)",
            "(-z, +y, -x)",
            "(+y, -z, -x)",
            "(-y, +z, +x)",
        };
    }

    /// <summary>
    /// A <see cref="Roflection2i"/> which preserves the Y axis. This allows the effect on the X component to
    /// be separated from the effect on the Y component.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct PlanarRoflection2i : IEquatable<PlanarRoflection2i>
    {
        private PlanarRoflection2i(byte code)
        {
            Code = code;
        }

        public PlanarRoflection2i(Reflection1 x, Reflection1 y)
        {
            Code = (byte)(((x.Code ^ y.Code) << 1) | y.Code);
        }

        /// <summary>
        /// The unique code used to identify this <see cref="PlanarRoflection2i"/>.
        /// </summary>
        public byte Code { get; }

        /// <summary>
        /// The number of distinct <see cref="PlanarRoflection2i"/>s.
        /// </summary>
        public const byte Count = 4;

        /// <summary>
        /// The transform applied to the X components.
        /// </summary>
        public Reflection1 X => Reflection1.ByCode((byte)(((Code >> 1) ^ Code) & 0b1));

        /// <summary>
        /// The transform applied to the Y component.
        /// </summary>
        public Reflection1 Y => Reflection1.ByCode((byte)(Code & 0b1));

        /// <summary>
        /// Indicates whether this <see cref="PlanarRoflection2i"/> is a rotation.
        /// </summary>
        public bool IsRotation => Code < 2;

        /// <summary>
        /// The inverse of this <see cref="PlanarRotation2i"/>.
        /// </summary>
        public PlanarRoflection2i Inverse => this;

        /// <summary>
        /// The identity <see cref="PlanarRotation2i"/>.
        /// </summary>
        public static PlanarRoflection2i Identity => PosX_PosY;

        /// <summary>
        /// The set of all <see cref="PlanarRotation2i"/>s.
        /// </summary>
        public static IEnumerable<PlanarRoflection2i> All
        {
            get
            {
                for (byte i = 0; i < Count; i++)
                    yield return new PlanarRoflection2i(i);
            }
        }

        /// <summary>
        /// Gets the <see cref="PlanarRotation2i"/> with the given <see cref="Code"/>.
        /// </summary>
        public static PlanarRoflection2i ByCode(byte code)
        {
            Debug.Assert(code < Count);
            return new PlanarRoflection2i(code);
        }

        /// <summary>
        /// Composes two <see cref="PlanarRotation2i"/>, first applying the right, then the left.
        /// </summary>
        public static PlanarRoflection2i Compose(PlanarRoflection2i a, PlanarRoflection2i b)
        {
            return new PlanarRoflection2i((byte)(a.Code ^ b.Code));
        }

        /// <summary>
        /// Applies this <see cref="PlanarRotation2i"/> to a vector.
        /// </summary>
        public Vector2 Apply(Vector2 vec)
        {
            return ((Roflection2i)this).Apply(vec);
        }

        /// <summary>
        /// Applies this <see cref="PlanarRotation2i"/> to a vector.
        /// </summary>
        public Vector2i Apply(Vector2i vec)
        {
            return ((Roflection2i)this).Apply(vec);
        }

        #region Elements
        public static PlanarRoflection2i PosX_PosY => new PlanarRoflection2i(0);
        public static PlanarRoflection2i NegX_NegY => new PlanarRoflection2i(1);
        public static PlanarRoflection2i NegX_PosY => new PlanarRoflection2i(2);
        public static PlanarRoflection2i PosX_NegY => new PlanarRoflection2i(3);
        #endregion

        public static PlanarRoflection2i operator *(PlanarRoflection2i a, PlanarRoflection2i b)
        {
            return Compose(a, b);
        }

        public static implicit operator Roflection2i(PlanarRoflection2i rof)
        {
            return _convTable[rof.Code];
        }

        /// <summary>
        /// The table used to implement the conversion to <see cref="Roflection2i"/>.
        /// </summary>
        private static readonly Roflection2i[] _convTable = new[]
        {
            Roflection2i.PosX_PosY,
            Roflection2i.NegX_NegY,
            Roflection2i.NegX_PosY,
            Roflection2i.PosX_NegY
        };

        public static bool operator ==(PlanarRoflection2i a, PlanarRoflection2i b)
        {
            return a.Code == b.Code;
        }

        public static bool operator !=(PlanarRoflection2i a, PlanarRoflection2i b)
        {
            return a.Code != b.Code;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is PlanarRoflection2i))
                return false;
            return this == (PlanarRoflection2i)obj;
        }

        bool IEquatable<PlanarRoflection2i>.Equals(PlanarRoflection2i other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return Code.GetHashCode();
        }

        public override string ToString()
        {
            return ((Roflection2i)this).ToString();
        }
    }

    /// <summary>
    /// A <see cref="Rotation3i"/> which preserves the Z axis. This allows the effect on the X/Y components to
    /// be separated from the effect on the Z component.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct PlanarRotation3i : IEquatable<PlanarRotation3i>
    {
        private PlanarRotation3i(byte code)
        {
            Code = code;
        }

        /// <summary>
        /// The unique code used to identify this <see cref="PlanarRotation3i"/>.
        /// </summary>
        public byte Code { get; }

        /// <summary>
        /// The number of distinct <see cref="PlanarRotation3i"/>s.
        /// </summary>
        public const byte Count = 8;

        /// <summary>
        /// The transform applied to the X and Y components.
        /// </summary>
        public Roflection2i X_Y => Roflection2i.ByCode(Code);

        /// <summary>
        /// The transform applied to the Z component.
        /// </summary>
        public Reflection1 Z => Reflection1.ByCode((byte)(Code & 0b1));

        /// <summary>
        /// The inverse of this <see cref="PlanarRotation3i"/>.
        /// </summary>
        public PlanarRotation3i Inverse => new PlanarRotation3i(Roflection2i.ByCode(Code).Inverse.Code);

        /// <summary>
        /// The identity <see cref="PlanarRotation3i"/>.
        /// </summary>
        public static PlanarRotation3i Identity => PosX_PosY_PosZ;

        /// <summary>
        /// The set of all <see cref="PlanarRotation3i"/>s.
        /// </summary>
        public static IEnumerable<PlanarRotation3i> All
        {
            get
            {
                for (byte i = 0; i < Count; i++)
                    yield return new PlanarRotation3i(i);
            }
        }

        /// <summary>
        /// Gets the <see cref="PlanarRotation3i"/> with the given <see cref="Code"/>.
        /// </summary>
        public static PlanarRotation3i ByCode(byte code)
        {
            Debug.Assert(code < Count);
            return new PlanarRotation3i(code);
        }

        /// <summary>
        /// Gets the <see cref="PlanarRotation3i"/> which performs the given rotation about the Z axis.
        /// </summary>
        public static PlanarRotation3i About_Z(Rotation2i rot)
        {
            return new PlanarRotation3i(((Roflection2i)rot).Code);
        }

        /// <summary>
        /// Composes two <see cref="PlanarRotation3i"/>, first applying the right, then the left.
        /// </summary>
        public static PlanarRotation3i Compose(PlanarRotation3i a, PlanarRotation3i b)
        {
            Roflection2i aRof = Roflection2i.ByCode(a.Code);
            Roflection2i bRof = Roflection2i.ByCode(b.Code);
            return new PlanarRotation3i(Roflection2i.Compose(aRof, bRof).Code);
        }

        /// <summary>
        /// Applies this <see cref="PlanarRotation3i"/> to a vector.
        /// </summary>
        public Vector3 Apply(Vector3 vec)
        {
            return ((Rotation3i)this).Apply(vec);
        }

        /// <summary>
        /// Applies this <see cref="PlanarRotation3i"/> to a vector.
        /// </summary>
        public Vector3i Apply(Vector3i vec)
        {
            return ((Rotation3i)this).Apply(vec);
        }

        /// <summary>
        /// Applies this <see cref="PlanarRotation3i"/> to a <see cref="Dir1"/> on the Z axis.
        /// </summary>
        /// <param name="tan">The transform from the canonical tangent space of the given direction
        /// to the canonical tangent space of the returned direction.</param>
        public Dir1 Apply_Z(Dir1 dir, out Rotation2i tan)
        {
            Roflection2i rof = Roflection2i.ByCode((byte)(Code ^ (int)dir)).Inverse;
            tan = Rotation2i.ByCode((byte)(rof.Code >> 1));
            return (Dir1)(rof.Code & 0b1);
        }

        /// <summary>
        /// The only <see cref="PlanarRotation3i"/> which flips the Z axis without transforming the
        /// canonical tangent space of the oriented X/Y plane.
        /// </summary>
        public static PlanarRotation3i Tan_Flip_Z => PosY_PosX_NegZ;

        #region Elements
        public static PlanarRotation3i PosX_PosY_PosZ => new PlanarRotation3i(0);
        public static PlanarRotation3i PosY_PosX_NegZ => new PlanarRotation3i(1);
        public static PlanarRotation3i NegY_PosX_PosZ => new PlanarRotation3i(2);
        public static PlanarRotation3i NegX_PosY_NegZ => new PlanarRotation3i(3);
        public static PlanarRotation3i NegX_NegY_PosZ => new PlanarRotation3i(4);
        public static PlanarRotation3i NegY_NegX_NegZ => new PlanarRotation3i(5);
        public static PlanarRotation3i PosY_NegX_PosZ => new PlanarRotation3i(6);
        public static PlanarRotation3i PosX_NegY_NegZ => new PlanarRotation3i(7);
        #endregion

        public static PlanarRotation3i operator *(PlanarRotation3i a, PlanarRotation3i b)
        {
            return Compose(a, b);
        }

        public static explicit operator PlanarRotation3i(Rotation3i rot)
        {
            Debug.Assert(rot.Code < Count);
            return new PlanarRotation3i(rot.Code);
        }

        public static implicit operator Rotation3i(PlanarRotation3i rot)
        {
            return Rotation3i.ByCode(rot.Code);
        }

        public static bool operator ==(PlanarRotation3i a, PlanarRotation3i b)
        {
            return a.Code == b.Code;
        }

        public static bool operator !=(PlanarRotation3i a, PlanarRotation3i b)
        {
            return a.Code != b.Code;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is PlanarRotation3i))
                return false;
            return this == (PlanarRotation3i)obj;
        }

        bool IEquatable<PlanarRotation3i>.Equals(PlanarRotation3i other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return Code.GetHashCode();
        }

        public override string ToString()
        {
            return ((Rotation3i)this).ToString();
        }
    }

    /// <summary>
    /// A <see cref="Roflection3i"/> which preserves the Z axis. This allows the effect on the X/Y components to
    /// be separated from the effect on the Z component.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct PlanarRoflection3i : IEquatable<PlanarRoflection3i>
    {
        private PlanarRoflection3i(byte code)
        {
            Code = code;
        }

        public PlanarRoflection3i(Roflection2i x_y, Reflection1 z)
        {
            this = _consTable[(x_y.Code << 1) | z.Code];
        }

        /// <summary>
        /// The unique code used to identify this <see cref="PlanarRoflection3i"/>.
        /// </summary>
        public byte Code { get; }

        /// <summary>
        /// The number of distinct <see cref="PlanarRoflection3i"/>s.
        /// </summary>
        public const byte Count = 16;

        /// <summary>
        /// The transform applied to the X and Y components.
        /// </summary>
        public Roflection2i X_Y => Roflection2i.ByCode((byte)((Code >> 1) ^ ((Code & 0b1) << 2)));

        /// <summary>
        /// The transform applied to the Z component.
        /// </summary>
        public Reflection1 Z => Reflection1.ByCode((byte)(((Code >> 1) ^ Code) & 0b1));

        /// <summary>
        /// The reflection component of this <see cref="PlanarRoflection3i"/>.
        /// </summary>
        public Reflection1 Reflection => Reflection1.ByCode((byte)(Code & 0b1));

        /// <summary>
        /// Indicates whether this <see cref="PlanarRoflection3i"/> is a <see cref="PlanarRotation3i"/>.
        /// </summary>
        public bool IsRotation => Reflection == Reflection1.Identity;

        /// <summary>
        /// The inverse of this <see cref="PlanarRoflection3i"/>.
        /// </summary>
        public PlanarRoflection3i Inverse
        {
            get
            {
                int rot = Roflection2i.ByCode((byte)(Code >> 1)).Inverse.Code;
                int refl = Code & 0b1;
                return new PlanarRoflection3i((byte)((rot << 1) | refl));
            }
        }

        /// <summary>
        /// The identity <see cref="PlanarRotation3i"/>.
        /// </summary>
        public static PlanarRoflection3i Identity => PosX_PosY_PosZ;

        /// <summary>
        /// The set of all <see cref="PlanarRoflection3i"/>s.
        /// </summary>
        public static IEnumerable<PlanarRoflection3i> All
        {
            get
            {
                for (byte i = 0; i < Count; i++)
                    yield return new PlanarRoflection3i(i);
            }
        }

        /// <summary>
        /// Gets the <see cref="PlanarRoflection3i"/> with the given <see cref="Code"/>.
        /// </summary>
        public static PlanarRoflection3i ByCode(byte code)
        {
            Debug.Assert(code < Count);
            return new PlanarRoflection3i(code);
        }

        /// <summary>
        /// Composes two <see cref="PlanarRoflection3i"/>, first applying the right, then the left.
        /// </summary>
        public static PlanarRoflection3i Compose(PlanarRoflection3i a, PlanarRoflection3i b)
        {
            Roflection2i aRof = Roflection2i.ByCode((byte)(a.Code >> 1));
            Roflection2i bRof = Roflection2i.ByCode((byte)(b.Code >> 1));
            int rot = Roflection2i.Compose(aRof, bRof).Code;
            int refl = (a.Code ^ b.Code) & 0b1;
            return new PlanarRoflection3i((byte)((rot << 1) | refl));
        }

        /// <summary>
        /// Applies this <see cref="PlanarRoflection3i"/> to a vector.
        /// </summary>
        public Vector3 Apply(Vector3 vec)
        {
            return ((Roflection3i)this).Apply(vec);
        }

        /// <summary>
        /// Applies this <see cref="PlanarRoflection3i"/> to a vector.
        /// </summary>
        public Vector3i Apply(Vector3i vec)
        {
            return ((Roflection3i)this).Apply(vec);
        }

        /// <summary>
        /// Applies this <see cref="PlanarRoflection3i"/> to a <see cref="Dir1"/> on the Z axis.
        /// </summary>
        /// <param name="tan">The transform from the canonical tangent space of the given direction
        /// to the canonical tangent space of the returned direction.</param>
        public Dir1 Apply_Z(Dir1 dir, out Roflection2i tan)
        {
            // TODO: Test if this actually works
            tan = Roflection2i.ByCode((byte)(Code >> 1));
            if (dir == Dir1.Neg) tan = tan.Inverse;
            return Z * dir;
        }

        #region Elements
        public static PlanarRoflection3i PosX_PosY_PosZ => new PlanarRoflection3i(0);
        public static PlanarRoflection3i NegX_NegY_NegZ => new PlanarRoflection3i(1);
        public static PlanarRoflection3i PosY_PosX_NegZ => new PlanarRoflection3i(2);
        public static PlanarRoflection3i NegY_NegX_PosZ => new PlanarRoflection3i(3);
        public static PlanarRoflection3i NegY_PosX_PosZ => new PlanarRoflection3i(4);
        public static PlanarRoflection3i PosY_NegX_NegZ => new PlanarRoflection3i(5);
        public static PlanarRoflection3i NegX_PosY_NegZ => new PlanarRoflection3i(6);
        public static PlanarRoflection3i PosX_NegY_PosZ => new PlanarRoflection3i(7);
        public static PlanarRoflection3i NegX_NegY_PosZ => new PlanarRoflection3i(8);
        public static PlanarRoflection3i PosX_PosY_NegZ => new PlanarRoflection3i(9);
        public static PlanarRoflection3i NegY_NegX_NegZ => new PlanarRoflection3i(10);
        public static PlanarRoflection3i PosY_PosX_PosZ => new PlanarRoflection3i(11);
        public static PlanarRoflection3i PosY_NegX_PosZ => new PlanarRoflection3i(12);
        public static PlanarRoflection3i NegY_PosX_NegZ => new PlanarRoflection3i(13);
        public static PlanarRoflection3i PosX_NegY_NegZ => new PlanarRoflection3i(14);
        public static PlanarRoflection3i NegX_PosY_PosZ => new PlanarRoflection3i(15);
        #endregion

        /// <summary>
        /// The table used to implement <see cref="PlanarRoflection3i(Roflection2i, Reflection1)"/>
        /// </summary>
        private static readonly PlanarRoflection3i[] _consTable = new PlanarRoflection3i[]
        {
            PosX_PosY_PosZ,
            PosX_PosY_NegZ,
            PosY_PosX_PosZ,
            PosY_PosX_NegZ,
            NegY_PosX_PosZ,
            NegY_PosX_NegZ,
            NegX_PosY_PosZ,
            NegX_PosY_NegZ,
            NegX_NegY_PosZ,
            NegX_NegY_NegZ,
            NegY_NegX_PosZ,
            NegY_NegX_NegZ,
            PosY_NegX_PosZ,
            PosY_NegX_NegZ,
            PosX_NegY_PosZ,
            PosX_NegY_NegZ,
        };

        public static PlanarRoflection3i operator *(PlanarRoflection3i a, PlanarRoflection3i b)
        {
            return Compose(a, b);
        }

        public static explicit operator PlanarRotation3i(PlanarRoflection3i rof)
        {
            Debug.Assert(rof.IsRotation);
            return PlanarRotation3i.ByCode((byte)(rof.Code >> 1));
        }

        public static explicit operator PlanarRoflection3i(Roflection3i rof)
        {
            Debug.Assert(rof.Code < Count);
            return new PlanarRoflection3i(rof.Code);
        }

        public static implicit operator PlanarRoflection3i(PlanarRotation3i rot)
        {
            return new PlanarRoflection3i((byte)(rot.Code << 1));
        }

        public static implicit operator Roflection3i(PlanarRoflection3i rof)
        {
            return Roflection3i.ByCode(rof.Code);
        }

        public static bool operator ==(PlanarRoflection3i a, PlanarRoflection3i b)
        {
            return a.Code == b.Code;
        }

        public static bool operator !=(PlanarRoflection3i a, PlanarRoflection3i b)
        {
            return a.Code != b.Code;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is PlanarRoflection3i))
                return false;
            return this == (PlanarRoflection3i)obj;
        }

        bool IEquatable<PlanarRoflection3i>.Equals(PlanarRoflection3i other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return Code.GetHashCode();
        }

        public override string ToString()
        {
            return ((Roflection3i)this).ToString();
        }
    }
}
