﻿using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;

namespace Alunite.Data.Geometry
{
    /// <summary>
    /// A set of <see cref="Rotation2i"/>s.
    /// </summary>
    [DebuggerDisplay("Size = {Size}")]
    [DebuggerTypeProxy(typeof(EnumerableDebugView<Rotation2i>))]
    public struct RotationSet2i : IEquatable<RotationSet2i>, IEnumerable<Rotation2i>
    {
        internal byte _members;
        internal RotationSet2i(byte members)
        {
            _members = members;
        }

        /// <summary>
        /// The empty rotation set.
        /// </summary>
        public static RotationSet2i None => new RotationSet2i(0);

        /// <summary>
        /// The rotation set consisting of all <see cref="Rotation2i"/>s.
        /// </summary>
        public static RotationSet2i All => new RotationSet2i(0b1111);

        /// <summary>
        /// Constructs a <see cref="RotationSet3i"/> containing only the given member.
        /// </summary>
        public static RotationSet2i Singleton(Rotation2i rot)
        {
            return new RotationSet2i((byte)(1 << rot.Code));
        }

        /// <summary>
        /// The number of rotations in this set.
        /// </summary>
        public uint Size => Bitwise.Count(_members);

        /// <summary>
        /// Gets an arbitrary rotation in this set, assuming it is not empty.
        /// </summary>
        public Rotation3i Any => Rotation3i.ByCode((byte)Bitwise.FindFirstOne(_members));

        /// <summary>
        /// Indicates whether this rotation group contains the given rotation.
        /// </summary>
        public bool Contains(Rotation3i rot)
        {
            return (_members & (1u << rot.Code)) != 0;
        }

        /// <summary>
        /// Gets an enumerator for this set.
        /// </summary>
        public Enumerator GetEnumerator()
        {
            return new Enumerator(this);
        }

        IEnumerator<Rotation2i> IEnumerable<Rotation2i>.GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// An enumerator for a <see cref="RotationSet2i"/>
        /// </summary>
        public struct Enumerator : IEnumerator<Rotation2i>
        {
            private byte _members;
            private uint _index;
            public Enumerator(RotationSet2i set)
            {
                _members = set._members;
                _index = uint.MaxValue;
            }

            /// <summary>
            /// Gets the current item for this enumerator.
            /// </summary>
            public Rotation2i Current
            {
                get
                {
                    Debug.Assert(_index >= 0 && _index < Rotation2i.Count);
                    return Rotation2i.ByCode((byte)_index);
                }
            }

            /// <summary>
            /// Advances to the next, or first, item.
            /// </summary>
            public bool MoveNext()
            {
                while (unchecked(++_index) < Rotation2i.Count)
                    if ((_members & (1u << (int)_index)) != 0)
                        return true;
                return false;
            }

            /// <summary>
            /// Resets the enumerator to before the beginning of the first item.
            /// </summary>
            public void Reset()
            {
                _index = uint.MaxValue;
            }

            object IEnumerator.Current => Current;

            void IDisposable.Dispose() { }
        }

        /// <summary>
        /// Gets the intersection of two rotation sets.
        /// </summary>
        public static RotationSet2i operator &(RotationSet2i a, RotationSet2i b)
        {
            return new RotationSet2i((byte)(a._members & b._members));
        }

        /// <summary>
        /// Gets the union of two rotation sets.
        /// </summary>
        public static RotationSet2i operator |(RotationSet2i a, RotationSet2i b)
        {
            return new RotationSet2i((byte)(a._members | b._members));
        }

        /// <summary>
        /// Gets the complement of a rotation set.
        /// </summary>
        public static RotationSet2i operator ~(RotationSet2i a)
        {
            return new RotationSet2i((byte)(~a._members & 0b1111));
        }

        /// <summary>
        /// Gets the set of rotations that are a composition of the given rotation and a rotation from the given set.
        /// </summary>
        public static RotationSet2i operator *(Rotation2i a, RotationSet2i b)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the set of rotations that are a composition of a rotation from the given set and the given rotation.
        /// </summary>
        public static RotationSet2i operator *(RotationSet2i a, Rotation2i b)
        {
            throw new NotImplementedException();
        }

        public static bool operator ==(RotationSet2i a, RotationSet2i b)
        {
            return a._members == b._members;
        }

        public static bool operator !=(RotationSet2i a, RotationSet2i b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is RotationSet2i))
                return false;
            return this == (RotationSet2i)obj;
        }

        bool IEquatable<RotationSet2i>.Equals(RotationSet2i other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return _members.GetHashCode();
        }
    }

    /// <summary>
    /// A set of <see cref="Rotation2i"/>s which includes the identity rotation and is closed under composition
    /// and inverse. This can be used to describe the rotational symmetries of an object in discrete
    /// three-dimensional space.
    /// </summary>
    [DebuggerDisplay("Order = {Order}")]
    [DebuggerTypeProxy(typeof(EnumerableDebugView<Rotation2i>))]
    public struct RotationGroup2i : IEquatable<RotationGroup2i>, IEnumerable<Rotation2i>
    {
        private RotationSet2i _set;
        private RotationGroup2i(RotationSet2i set)
        {
            _set = set;
        }

        /// <summary>
        /// The rotation group consisting of all <see cref="Rotation2i"/>s.
        /// </summary>
        public static RotationGroup2i All => new RotationGroup2i(RotationSet2i.All);

        /// <summary>
        /// The rotation group consisting of just <see cref="Rotation2i.Identity"/>.
        /// </summary>
        public static RotationGroup2i Trivial => new RotationGroup2i(RotationSet2i.Singleton(Rotation2i.Identity));

        /// <summary>
        /// Gets the smallest subgroup which contains all of the given generating rotations.
        /// </summary>
        public static RotationGroup2i Build(params Rotation2i[] gens)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The number of rotations in this group. 
        /// </summary>
        public uint Order => _set.Size;

        /// <summary>
        /// Gets an enumerator for this set.
        /// </summary>
        public RotationSet2i.Enumerator GetEnumerator()
        {
            return new RotationSet2i.Enumerator(_set);
        }

        IEnumerator<Rotation2i> IEnumerable<Rotation2i>.GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Gets the intersection of two rotation groups.
        /// </summary>
        public static RotationGroup2i operator &(RotationGroup2i a, RotationGroup2i b)
        {
            return new RotationGroup2i(a._set & b._set);
        }

        public static implicit operator RotationSet2i(RotationGroup2i group)
        {
            return group._set;
        }

        public static bool operator ==(RotationGroup2i a, RotationGroup2i b)
        {
            return a._set == b._set;
        }

        public static bool operator !=(RotationGroup2i a, RotationGroup2i b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is RotationGroup2i))
                return false;
            return this == (RotationGroup2i)obj;
        }

        bool IEquatable<RotationGroup2i>.Equals(RotationGroup2i other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return _set.GetHashCode();
        }
    }

    /// <summary>
    /// A set of <see cref="Rotation3i"/>s.
    /// </summary>
    [DebuggerDisplay("Size = {Size}")]
    [DebuggerTypeProxy(typeof(EnumerableDebugView<Rotation3i>))]
    public struct RotationSet3i : IEquatable<RotationSet3i>, IEnumerable<Rotation3i>
    {
        internal uint _members;
        internal RotationSet3i(uint members)
        {
            _members = members;
        }

        /// <summary>
        /// The empty rotation set.
        /// </summary>
        public static RotationSet3i None => new RotationSet3i(0);

        /// <summary>
        /// The rotation set consisting of all <see cref="Rotation3i"/>s.
        /// </summary>
        public static RotationSet3i All => new RotationSet3i(0b111111111111111111111111);

        /// <summary>
        /// Constructs a <see cref="RotationSet3i"/> containing only the given member.
        /// </summary>
        public static RotationSet3i Singleton(Rotation3i rot)
        {
            return new RotationSet3i(1u << rot.Code);
        }

        /// <summary>
        /// The number of rotations in this set.
        /// </summary>
        public uint Size => Bitwise.Count(_members);

        /// <summary>
        /// Gets an arbitrary rotation in this set, assuming it is not empty.
        /// </summary>
        public Rotation3i Any => Rotation3i.ByCode((byte)Bitwise.FindFirstOne(_members));

        /// <summary>
        /// Indicates whether this rotation group contains the given rotation.
        /// </summary>
        public bool Contains(Rotation3i rot)
        {
            return (_members & (1u << rot.Code)) != 0;
        }

        /// <summary>
        /// Gets an enumerator for this set.
        /// </summary>
        public Enumerator GetEnumerator()
        {
            return new Enumerator(this);
        }

        IEnumerator<Rotation3i> IEnumerable<Rotation3i>.GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// An enumerator for a <see cref="RotationSet3i"/>
        /// </summary>
        public struct Enumerator : IEnumerator<Rotation3i>
        {
            private uint _members;
            private uint _index;
            public Enumerator(RotationSet3i set)
            {
                _members = set._members;
                _index = uint.MaxValue;
            }

            /// <summary>
            /// Gets the current item for this enumerator.
            /// </summary>
            public Rotation3i Current
            {
                get
                {
                    Debug.Assert(_index >= 0 && _index < Rotation3i.Count);
                    return Rotation3i.ByCode((byte)_index);
                }
            }

            /// <summary>
            /// Advances to the next, or first, item.
            /// </summary>
            public bool MoveNext()
            {
                while (unchecked(++_index) < Rotation3i.Count)
                    if ((_members & (1u << (int)_index)) != 0)
                        return true;
                return false;
            }

            /// <summary>
            /// Resets the enumerator to before the beginning of the first item.
            /// </summary>
            public void Reset()
            {
                _index = uint.MaxValue;
            }

            object IEnumerator.Current => Current;

            void IDisposable.Dispose() { }
        }

        /// <summary>
        /// Gets the intersection of two rotation sets.
        /// </summary>
        public static RotationSet3i operator &(RotationSet3i a, RotationSet3i b)
        {
            return new RotationSet3i(a._members & b._members);
        }

        /// <summary>
        /// Gets the union of two rotation sets.
        /// </summary>
        public static RotationSet3i operator |(RotationSet3i a, RotationSet3i b)
        {
            return new RotationSet3i(a._members | b._members);
        }

        /// <summary>
        /// Gets the complement of a rotation set.
        /// </summary>
        public static RotationSet3i operator ~(RotationSet3i a)
        {
            return new RotationSet3i(~a._members & 0b111111111111111111111111);
        }

        /// <summary>
        /// Gets the set of rotations that are a composition of the given rotation and a rotation from the given set.
        /// </summary>
        public static RotationSet3i operator *(Rotation3i a, RotationSet3i b)
        {
            RotationSet3i res = None;
            for (byte i = 0; i < Rotation3i.Count; i++)
            {
                Rotation3i bRot = Rotation3i.ByCode(i);
                if (b.Contains(bRot))
                    res |= Singleton(a * bRot);
            }
            return res;
        }

        /// <summary>
        /// Gets the set of rotations that are a composition of a rotation from the given set and the given rotation.
        /// </summary>
        public static RotationSet3i operator *(RotationSet3i a, Rotation3i b)
        {
            RotationSet3i res = None;
            for (byte i = 0; i < Rotation3i.Count; i++)
            {
                Rotation3i aRot = Rotation3i.ByCode(i);
                if (a.Contains(aRot))
                    res |= Singleton(aRot * b);
            }
            return res;
        }

        public static bool operator ==(RotationSet3i a, RotationSet3i b)
        {
            return a._members == b._members;
        }

        public static bool operator !=(RotationSet3i a, RotationSet3i b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is RotationSet3i))
                return false;
            return this == (RotationSet3i)obj;
        }

        bool IEquatable<RotationSet3i>.Equals(RotationSet3i other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return _members.GetHashCode();
        }
    }

    /// <summary>
    /// A set of <see cref="Roflection3i"/>s.
    /// </summary>
    [DebuggerDisplay("Size = {Size}")]
    [DebuggerTypeProxy(typeof(EnumerableDebugView<Roflection3i>))]
    public struct RoflectionSet3i : IEquatable<RoflectionSet3i>, IEnumerable<Roflection3i>
    {
        internal ulong _members;
        internal RoflectionSet3i(ulong members)
        {
            _members = members;
        }

        /// <summary>
        /// The empty rotation set.
        /// </summary>
        public static RoflectionSet3i None => new RoflectionSet3i(0);

        /// <summary>
        /// The rotation set consisting of all <see cref="Roflection3i"/>s.
        /// </summary>
        public static RoflectionSet3i All => new RoflectionSet3i(0b111111111111111111111111111111111111111111111111);

        /// <summary>
        /// Constructs a <see cref="RoflectionSet3i"/> containing only the given member.
        /// </summary>
        public static RoflectionSet3i Singleton(Roflection3i rot)
        {
            return new RoflectionSet3i(1ul << rot.Code);
        }

        /// <summary>
        /// The number of rotations in this set.
        /// </summary>
        public uint Size => Bitwise.Count(_members);

        /// <summary>
        /// Gets an arbitrary rotation in this set, assuming it is not empty.
        /// </summary>
        public Roflection3i Any => Roflection3i.ByCode((byte)Bitwise.FindFirstOne(_members));

        /// <summary>
        /// Indicates whether this rotation group contains the given rotation.
        /// </summary>
        public bool Contains(Roflection3i rot)
        {
            return (_members & (1ul << rot.Code)) != 0;
        }

        /// <summary>
        /// Gets an enumerator for this set.
        /// </summary>
        public Enumerator GetEnumerator()
        {
            return new Enumerator(this);
        }

        IEnumerator<Roflection3i> IEnumerable<Roflection3i>.GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// An enumerator for a <see cref="RoflectionSet3i"/>
        /// </summary>
        public struct Enumerator : IEnumerator<Roflection3i>
        {
            private ulong _members;
            private uint _index;
            public Enumerator(RoflectionSet3i set)
            {
                _members = set._members;
                _index = uint.MaxValue;
            }

            /// <summary>
            /// Gets the current item for this enumerator.
            /// </summary>
            public Roflection3i Current
            {
                get
                {
                    Debug.Assert(_index >= 0 && _index < Roflection3i.Count);
                    return Roflection3i.ByCode((byte)_index);
                }
            }

            /// <summary>
            /// Advances to the next, or first, item.
            /// </summary>
            public bool MoveNext()
            {
                while (unchecked(++_index) < Roflection3i.Count)
                    if ((_members & (1ul << (int)_index)) != 0)
                        return true;
                return false;
            }

            /// <summary>
            /// Resets the enumerator to before the beginning of the first item.
            /// </summary>
            public void Reset()
            {
                _index = uint.MaxValue;
            }

            object IEnumerator.Current => Current;

            void IDisposable.Dispose() { }
        }

        /// <summary>
        /// Gets the intersection of two rotation sets.
        /// </summary>
        public static RoflectionSet3i operator &(RoflectionSet3i a, RoflectionSet3i b)
        {
            return new RoflectionSet3i(a._members & b._members);
        }

        /// <summary>
        /// Gets the union of two rotation sets.
        /// </summary>
        public static RoflectionSet3i operator |(RoflectionSet3i a, RoflectionSet3i b)
        {
            return new RoflectionSet3i(a._members | b._members);
        }

        /// <summary>
        /// Gets the complement of a rotation set.
        /// </summary>
        public static RoflectionSet3i operator ~(RoflectionSet3i a)
        {
            return new RoflectionSet3i(~a._members & 0b111111111111111111111111111111111111111111111111);
        }

        /// <summary>
        /// Gets the set of rotations that are a composition of the given rotation and a rotation from the given set.
        /// </summary>
        public static RoflectionSet3i operator *(Roflection3i a, RoflectionSet3i b)
        {
            RoflectionSet3i res = None;
            for (byte i = 0; i < Roflection3i.Count; i++)
            {
                Roflection3i bRot = Roflection3i.ByCode(i);
                if (b.Contains(bRot))
                    res |= Singleton(a * bRot);
            }
            return res;
        }

        /// <summary>
        /// Gets the set of rotations that are a composition of a rotation from the given set and the given rotation.
        /// </summary>
        public static RoflectionSet3i operator *(RoflectionSet3i a, Roflection3i b)
        {
            RoflectionSet3i res = None;
            for (byte i = 0; i < Roflection3i.Count; i++)
            {
                Roflection3i aRot = Roflection3i.ByCode(i);
                if (a.Contains(aRot))
                    res |= Singleton(aRot * b);
            }
            return res;
        }

        public static bool operator ==(RoflectionSet3i a, RoflectionSet3i b)
        {
            return a._members == b._members;
        }

        public static bool operator !=(RoflectionSet3i a, RoflectionSet3i b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is RoflectionSet3i))
                return false;
            return this == (RoflectionSet3i)obj;
        }

        bool IEquatable<RoflectionSet3i>.Equals(RoflectionSet3i other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return _members.GetHashCode();
        }
    }

    /// <summary>
    /// A set of <see cref="Rotation3i"/>s which includes the identity rotation and is closed under composition
    /// and inverse. This can be used to describe the rotational symmetries of an object in discrete
    /// three-dimensional space.
    /// </summary>
    [DebuggerDisplay("Order = {Order}")]
    [DebuggerTypeProxy(typeof(EnumerableDebugView<Rotation3i>))]
    public struct RotationGroup3i : IEquatable<RotationGroup3i>, IEnumerable<Rotation3i>
    {
        private RotationSet3i _set;
        private RotationGroup3i(RotationSet3i set)
        {
            _set = set;
        }

        /// <summary>
        /// The rotation group consisting of all <see cref="Rotation3i"/>s.
        /// </summary>
        public static RotationGroup3i All => new RotationGroup3i(RotationSet3i.All);

        /// <summary>
        /// The rotation group consisting of just <see cref="Rotation3i.Identity"/>.
        /// </summary>
        public static RotationGroup3i Trivial => new RotationGroup3i(RotationSet3i.Singleton(Rotation3i.Identity));

        /// <summary>
        /// Gets the smallest subgroup which contains all of the given generating rotations.
        /// </summary>
        public static RotationGroup3i Build(params Rotation3i[] gens)
        {
            uint members = 1;
            foreach (var gen in gens)
            {
                uint nMembers = members;
                while (nMembers != 0)
                {
                    uint pMembers = members;
                    for (byte i = 0; i < Rotation3i.Count; i++)
                    {
                        if ((pMembers & (1u << i)) != 0)
                        {
                            members |= 1u << (gen * Rotation3i.ByCode(i)).Code;
                            members |= 1u << (Rotation3i.ByCode(i) * gen).Code;
                        }
                    }
                    nMembers = members & ~pMembers;
                }
            }
            return new RotationGroup3i(new RotationSet3i(members));
        }

        /// <summary>
        /// The number of rotations in this group. 
        /// </summary>
        public uint Order => _set.Size;

        /// <summary>
        /// Gets a minimal set of <see cref="Rotation3i"/>s such that every rotation can be described as a composition
        /// of a member of the returned set and a member of this group.
        /// </summary>
        public RotationSet3i Dual
        {
            get
            {
                RotationSet3i dual = RotationSet3i.None;
                RotationSet3i rem = RotationSet3i.All;
                while (rem != RotationSet3i.None)
                {
                    Rotation3i rot = rem.Any;
                    dual |= RotationSet3i.Singleton(rot);
                    rem &= ~(rot * _set);
                }
                return dual;
            }
        }

        /// <summary>
        /// Gets an enumerator for this set.
        /// </summary>
        public RotationSet3i.Enumerator GetEnumerator()
        {
            return new RotationSet3i.Enumerator(_set);
        }

        IEnumerator<Rotation3i> IEnumerable<Rotation3i>.GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Gets the intersection of two rotation groups.
        /// </summary>
        public static RotationGroup3i operator &(RotationGroup3i a, RotationGroup3i b)
        {
            return new RotationGroup3i(a._set & b._set);
        }

        public static implicit operator RotationSet3i(RotationGroup3i group)
        {
            return group._set;
        }

        public static bool operator ==(RotationGroup3i a, RotationGroup3i b)
        {
            return a._set == b._set;
        }

        public static bool operator !=(RotationGroup3i a, RotationGroup3i b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is RotationGroup3i))
                return false;
            return this == (RotationGroup3i)obj;
        }

        bool IEquatable<RotationGroup3i>.Equals(RotationGroup3i other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return _set.GetHashCode();
        }
    }
}