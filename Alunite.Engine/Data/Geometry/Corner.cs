﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Alunite.Data.Geometry
{
    /// <summary>
    /// Describes the general shape of faces and edges in the vicinity of a two-dimensional corner.
    /// </summary>
    public struct CornerShape2
    {
        private byte _bitmap;
        private CornerShape2(byte bitmap)
        {
            _bitmap = bitmap;
        }

        public CornerShape2(BoolAxisDuet2 edges, BoolQuartet faces)
        {
            _bitmap = (byte)(edges.Bitmap | (faces.Bitmap << 4));
        }
        
        /// <summary>
        /// Identifies where edges emanate from this corner.
        /// </summary>
        public BoolAxisDuet2 Edges
        {
            get
            {
                return new BoolAxisDuet2((byte)(_bitmap & 0b00001111));
            }
            set
            {
                _bitmap &= 0b11110000;
                _bitmap |= value.Bitmap;
            }
        }

        /// <summary>
        /// Identifies where faces emanate from this corner. Faces must be bounded by <see cref="Edges"/>.
        /// </summary>
        public BoolQuartet Faces
        {
            get
            {
                return new BoolQuartet((byte)((_bitmap & 0b11110000) >> 4));
            }
            set
            {
                _bitmap &= 0b00001111;
                _bitmap |= (byte)(value.Bitmap << 4);
            }
        }

        /// <summary>
        /// Decomposes this shape into a set of <see cref="PrimitiveCornerShape2"/>s.
        /// </summary>
        public IEnumerable<PrimitiveCornerShape2> Components
        {
            get
            {
                var faces = Faces;
                foreach (var component in _componentsFromEdges[Edges.Bitmap])
                {
                    if (faces[component.GetArbitraryQuadrant()])
                        yield return component;
                }
            }
        }

        /// <summary>
        /// Adds an edge in every direction that is in the interior of a face.
        /// </summary>
        public CornerShape2 Split()
        {
            byte nBitmap = _bitmap;
            if ((_bitmap & 0b00010000) != 0) nBitmap |= 0b0101;
            if ((_bitmap & 0b00100000) != 0) nBitmap |= 0b0110;
            if ((_bitmap & 0b01000000) != 0) nBitmap |= 0b1001;
            if ((_bitmap & 0b10000000) != 0) nBitmap |= 0b1010;
            return new CornerShape2(nBitmap);
        }

        /// <summary>
        /// Converts from a <see cref="CornerShape2"/> to a <see cref="PrimitiveCornerShape2"/>, assuming it is primitive.
        /// </summary>
        public static explicit operator PrimitiveCornerShape2(CornerShape2 shape)
        {
            var faces = shape.Faces;
            foreach (var component in _componentsFromEdges[shape.Edges.Bitmap])
            {
                if (faces[component.GetArbitraryQuadrant()])
                    return component;
            }
            throw new Exception("Empty shape is not primitive");
        }

        /// <summary>
        /// Converts from a <see cref="PrimitiveCornerShape2"/> to a <see cref="CornerShape2"/>.
        /// </summary>
        public static implicit operator CornerShape2(PrimitiveCornerShape2 source)
        {
            return _fromPrimitive[(int)source];
        }

        /// <summary>
        /// The table used to implement <see cref="Components"/>.
        /// </summary>
        private static readonly PrimitiveCornerShape2[][] _componentsFromEdges = new PrimitiveCornerShape2[][]
        {
            new [] { PrimitiveCornerShape2.Full },
            new [] { PrimitiveCornerShape2.Broken_NegX },
            new [] { PrimitiveCornerShape2.Broken_PosX },
            new [] { PrimitiveCornerShape2.Half_NegY, PrimitiveCornerShape2.Half_PosY },
            new [] { PrimitiveCornerShape2.Broken_NegY },
            new [] { PrimitiveCornerShape2.Quarter_NegX_NegY, PrimitiveCornerShape2.AntiQuarter_NegX_NegY },
            new [] { PrimitiveCornerShape2.Quarter_PosX_NegY, PrimitiveCornerShape2.AntiQuarter_PosX_NegY },
            new [] { PrimitiveCornerShape2.Quarter_NegX_NegY, PrimitiveCornerShape2.Quarter_PosX_NegY, PrimitiveCornerShape2.Half_PosY },
            new [] { PrimitiveCornerShape2.Broken_PosY },
            new [] { PrimitiveCornerShape2.Quarter_NegX_PosY, PrimitiveCornerShape2.AntiQuarter_NegX_PosY },
            new [] { PrimitiveCornerShape2.Quarter_PosX_PosY, PrimitiveCornerShape2.AntiQuarter_PosX_PosY },
            new [] { PrimitiveCornerShape2.Quarter_NegX_PosY, PrimitiveCornerShape2.Quarter_PosX_PosY, PrimitiveCornerShape2.Half_NegY },
            new [] { PrimitiveCornerShape2.Half_NegX, PrimitiveCornerShape2.Half_PosX },
            new [] { PrimitiveCornerShape2.Quarter_NegX_NegY, PrimitiveCornerShape2.Quarter_NegX_PosY, PrimitiveCornerShape2.Half_PosX },
            new [] { PrimitiveCornerShape2.Quarter_PosX_NegY, PrimitiveCornerShape2.Quarter_PosX_PosY, PrimitiveCornerShape2.Half_NegX },
            new [] { PrimitiveCornerShape2.Quarter_NegX_NegY, PrimitiveCornerShape2.Quarter_PosX_NegY, PrimitiveCornerShape2.Quarter_NegX_PosY, PrimitiveCornerShape2.Quarter_PosX_PosY },
        };

        /// <summary>
        /// The table used to implement the conversion from <see cref="PrimitiveCornerShape2"/>.
        /// </summary>
        private static readonly CornerShape2[] _fromPrimitive = new CornerShape2[]
        {
            new CornerShape2(0b00010101),
            new CornerShape2(0b00100110),
            new CornerShape2(0b01001001),
            new CornerShape2(0b10001010),
            new CornerShape2(0b01011100),
            new CornerShape2(0b00111100),
            new CornerShape2(0b11000011),
            new CornerShape2(0b10100011),
            new CornerShape2(0b01110101),
            new CornerShape2(0b11100110),
            new CornerShape2(0b11011001),
            new CornerShape2(0b10111010),
            new CornerShape2(0b11110001),
            new CornerShape2(0b11110010),
            new CornerShape2(0b11110100),
            new CornerShape2(0b11111000),
            new CornerShape2(0b11110000)
        };
    }
    
    /// <summary>
    /// A <see cref="CornerShape2"/> that consists of exactly one face.
    /// </summary>
    /// <remarks>The strange numbering scheme is explained by the implementation of
    /// <see cref="Orthogonal.GetArbitraryQuadrant(PrimitiveCornerShape2)"/></remarks>
    public enum PrimitiveCornerShape2 : byte
    {
        /// <summary>
        /// The polygon consists of the quadrant in the negative X, negative Y direction.
        /// </summary>
        Quarter_NegX_NegY = 0,

        /// <summary>
        /// The polygon consists of the quadrant in the positive X, negative Y direction.
        /// </summary>
        Quarter_PosX_NegY = 1,

        /// <summary>
        /// The polygon consists of the quadrant in the negative X, positive Y direction.
        /// </summary>
        Quarter_NegX_PosY = 2,

        /// <summary>
        /// The polygon consists of the quadrant in the positive X, positive Y direction.
        /// </summary>
        Quarter_PosX_PosY = 3,

        /// <summary>
        /// The polygon consists of the half-space in the negative X direction.
        /// </summary>
        Half_NegX = 4,

        /// <summary>
        /// The polygon consists of the half-space in the positive X direction.
        /// </summary>
        Half_PosX = 7,

        /// <summary>
        /// The polygon consists of the half-space in the negative Y direction.
        /// </summary>
        Half_NegY = 5,

        /// <summary>
        /// The polygon consists of the half-space in the positive Y direction.
        /// </summary>
        Half_PosY = 6,

        /// <summary>
        /// The polygon consists of all but the quadrant in the negative X, negative Y direction.
        /// </summary>
        AntiQuarter_NegX_NegY = 9,

        /// <summary>
        /// The polygon consists of all but the quadrant in the positive X, negative Y direction.
        /// </summary>
        AntiQuarter_PosX_NegY = 10,

        /// <summary>
        /// The polygon consists of all but the quadrant in the negative X, positive Y direction.
        /// </summary>
        AntiQuarter_NegX_PosY = 11,

        /// <summary>
        /// The polygon consists of all but the quadrant in the positive X, positive Y direction.
        /// </summary>
        AntiQuarter_PosX_PosY = 8,

        /// <summary>
        /// The polygon consists of all space except for a ray (i.e. significant edge) in the negative X direction.
        /// </summary>
        Broken_NegX = 12,

        /// <summary>
        /// The polygon consists of all space except for a ray (i.e. significant edge) in the positive X direction.
        /// </summary>
        Broken_PosX = 13,

        /// <summary>
        /// The polygon consists of all space except for a ray (i.e. significant edge) in the negative Y direction.
        /// </summary>
        Broken_NegY = 14,

        /// <summary>
        /// The polygon consists of all space except for a ray (i.e. significant edge) in the positive Y direction.
        /// </summary>
        Broken_PosY = 15,

        /// <summary>
        /// The polygon consists of all space. i.e. the vertex is completely surronded by the polygon.
        /// </summary>
        Full = 16
    }

    /// <summary>
    /// Contains helper functions related to corner shape types.
    /// </summary>
    public static class CornerShape
    {
        /// <summary>
        /// Applies a <see cref="Roflection2i"/> to a <see cref="PrimitiveCornerShape2"/>.
        /// </summary>
        public static PrimitiveCornerShape2 Transform(this PrimitiveCornerShape2 face, Roflection2i rof)
        {
            return _transformFace[(int)face * Roflection2i.Count + rof.Code];
        }
        
        /// <summary>
        /// Swaps the X and Y coordinates of a <see cref="PrimitiveCornerShape2"/>.
        /// </summary>
        public static PrimitiveCornerShape2 Swap(this PrimitiveCornerShape2 face)
        {
            return Transform(face, Roflection2i.Swap);
        }

        /// <summary>
        /// The table used to implement <see cref="Transform(PrimitiveCornerShape2, Roflection2i)"/>.
        /// </summary>
        private static readonly PrimitiveCornerShape2[] _transformFace = _createTransformFaceTable();

        /// <summary>
        /// The initializer for <see cref="_transformFace"/>
        /// </summary>
        private static PrimitiveCornerShape2[] _createTransformFaceTable()
        {
            var rotateTable = new PrimitiveCornerShape2[]
            {
                PrimitiveCornerShape2.Quarter_PosX_NegY,
                PrimitiveCornerShape2.Quarter_PosX_PosY,
                PrimitiveCornerShape2.Quarter_NegX_NegY,
                PrimitiveCornerShape2.Quarter_NegX_PosY,
                PrimitiveCornerShape2.Half_NegY,
                PrimitiveCornerShape2.Half_PosX,
                PrimitiveCornerShape2.Half_NegX,
                PrimitiveCornerShape2.Half_PosY,
                PrimitiveCornerShape2.AntiQuarter_NegX_PosY,
                PrimitiveCornerShape2.AntiQuarter_PosX_NegY,
                PrimitiveCornerShape2.AntiQuarter_PosX_PosY,
                PrimitiveCornerShape2.AntiQuarter_NegX_NegY,
                PrimitiveCornerShape2.Broken_NegY,
                PrimitiveCornerShape2.Broken_PosY,
                PrimitiveCornerShape2.Broken_PosX,
                PrimitiveCornerShape2.Broken_NegX,
                PrimitiveCornerShape2.Full
            };
            var swapTable = new PrimitiveCornerShape2[]
            {
                PrimitiveCornerShape2.Quarter_NegX_NegY,
                PrimitiveCornerShape2.Quarter_NegX_PosY,
                PrimitiveCornerShape2.Quarter_PosX_NegY,
                PrimitiveCornerShape2.Quarter_PosX_PosY,
                PrimitiveCornerShape2.Half_NegY,
                PrimitiveCornerShape2.Half_NegX,
                PrimitiveCornerShape2.Half_PosX,
                PrimitiveCornerShape2.Half_PosY,
                PrimitiveCornerShape2.AntiQuarter_PosX_PosY,
                PrimitiveCornerShape2.AntiQuarter_NegX_NegY,
                PrimitiveCornerShape2.AntiQuarter_NegX_PosY,
                PrimitiveCornerShape2.AntiQuarter_PosX_NegY,
                PrimitiveCornerShape2.Broken_NegY,
                PrimitiveCornerShape2.Broken_PosY,
                PrimitiveCornerShape2.Broken_NegX,
                PrimitiveCornerShape2.Broken_PosX,
                PrimitiveCornerShape2.Full
            };
            var resTable = new PrimitiveCornerShape2[17 * Roflection2i.Count];
            for (uint i = 0; i < 17; i++)
            {
                var shape = (PrimitiveCornerShape2)i;
                var rof = Roflection2i.Identity;
                resTable[i * Roflection2i.Count + rof.Code] = shape;
                rof = Roflection2i.Cross * rof;
                shape = rotateTable[(int)shape];
                resTable[i * Roflection2i.Count + rof.Code] = shape;
                rof = Roflection2i.Cross * rof;
                shape = rotateTable[(int)shape];
                resTable[i * Roflection2i.Count + rof.Code] = shape;
                rof = Roflection2i.Cross * rof;
                shape = rotateTable[(int)shape];
                resTable[i * Roflection2i.Count + rof.Code] = shape;
                shape = swapTable[i];
                rof = Roflection2i.Swap;
                resTable[i * Roflection2i.Count + rof.Code] = shape;
                rof = Roflection2i.Cross * rof;
                shape = rotateTable[(int)shape];
                resTable[i * Roflection2i.Count + rof.Code] = shape;
                rof = Roflection2i.Cross * rof;
                shape = rotateTable[(int)shape];
                resTable[i * Roflection2i.Count + rof.Code] = shape;
                rof = Roflection2i.Cross * rof;
                shape = rotateTable[(int)shape];
                resTable[i * Roflection2i.Count + rof.Code] = shape;
            }
            return resTable;
        }

        /// <summary>
        /// Gets an arbitrary quadrant included in this corner face.
        /// </summary>
        public static Quadrant GetArbitraryQuadrant(this PrimitiveCornerShape2 face)
        {
            return (Quadrant)((int)face & 0b11);
        }
    }

    /// <summary>
    /// A helper class for establishing connections between corners in a three-dimensional grid.
    /// Corners are added incrementally to the linker in ascending X, Y, Z order, producing edge and face information
    /// to be transmitted to later corners while consuming similar information from earlier corners. This allows the corners
    /// to determine which edges and faces they are a part of and allows geometry to be built for them by the consuming
    /// corners.
    /// </summary>
    public sealed class CornerLinker3<TEdge, TFace>
    {
        public CornerLinker3()
        {
            // TODO: Tune initialize hashtable capacities
            _rayBins_y = new _RayBin_Y[32];
            _rayBins_z = new _RayBin_Z[128];
            _rayCapacity_y = _capacity(_rayBins_y.Length);
            _rayCapacity_z = _capacity(_rayBins_z.Length);
            _faces = new Pool<TFace>(256);

            // Initalize bins to empty
            for (int i = 0; i < _rayBins_y.Length; i++)
                _rayBins_y[i] = _RayBin_Y.Empty;
            for (int i = 0; i < _rayBins_z.Length; i++)
                _rayBins_z[i] = _RayBin_Z.Empty;
        }

        /// <summary>
        /// The ray along the X direction at the Y, Z coordinates of the last processed corner.
        /// </summary>
        private _Ray _ray_x;

        /// <summary>
        /// The rays along the Y direction at the Z coordinate of the last processed corner, stored in a hashtable indexed
        /// by X coordinate.
        /// </summary>
        private _RayBin_Y[] _rayBins_y;

        /// <summary>
        /// The number of additional rays that can be stored in <see cref="_rayBins_y"/>.
        /// </summary>
        private uint _rayCapacity_y;

        /// <summary>
        /// The rays along the Z direction, stored in a hashtable indexed by X and Y coordinate.
        /// </summary>
        private _RayBin_Z[] _rayBins_z;

        /// <summary>
        /// The number of additional rays that can be stored in <see cref="_rayBins_z"/>.
        /// </summary>
        private uint _rayCapacity_z;

        /// <summary>
        /// The storage location for the face information for the linker.
        /// </summary>
        private Pool<TFace> _faces;

        /// <summary>
        /// Constructs a <see cref="Corner"/> structure to examine and manipulate the edge and face connections available
        /// to a corner at the given position. The corner should be commited once all required information has been set.
        /// This can be done using either <see cref="Corner.Commit"/> or by disposing the corner.
        /// </summary>
        public Corner Link(Vector3i pos)
        {
            _prepareBins();
            return new Corner(this, pos);
        }

        /// <summary>
        /// An interface for consuming edge and face information available to a corner while transmitting similar information
        /// to later corners.
        /// </summary>
        public struct Corner : IDisposable
        {
            private CornerLinker3<TEdge, TFace> _linker;
            private uint _rayBinCache_y;
            private uint _rayBinCache_z;
            private _Ray _rayOut_x;
            private _Ray _rayOut_y;
            private _Ray _rayOut_z;
            private bool _write_y;
            private bool _write_z;
            private Pool<TFace>.Id _freeFace_x_y;
            private Pool<TFace>.Id _freeFace_x_z;
            private Pool<TFace>.Id _freeFace_y_z;
            internal Corner(CornerLinker3<TEdge, TFace> linker, Vector3i pos)
            {
                Position = pos;
                _linker = linker;
                _rayBinCache_y = uint.MaxValue;
                _rayBinCache_z = uint.MaxValue;
                _rayOut_x = default;
                _rayOut_y = default;
                _rayOut_z = default;
                _write_y = false;
                _write_z = false;
                _freeFace_x_y = Pool<TFace>.Id.Dummy;
                _freeFace_x_z = Pool<TFace>.Id.Dummy;
                _freeFace_y_z = Pool<TFace>.Id.Dummy;
            }

            /// <summary>
            /// The position of the corner.
            /// </summary>
            public Vector3i Position { get; }

            /// <summary>
            /// The edge information transmitted from the negative X direction.
            /// </summary>
            public TEdge Edge_NegX
            {
                get
                {
                    return _rayIn_x.Edge;
                }
            }

            /// <summary>
            /// The edge information to be transmitted in the positive X direction.
            /// </summary>
            public TEdge Edge_PosX
            {
                set
                {
                    _rayOut_x.Edge = value;
                    _write_x = true;
                }
            }

            /// <summary>
            /// The edge information transmitted from the negative Y direction.
            /// </summary>
            public TEdge Edge_NegY
            {
                get
                {
                    return _rayIn_y.Edge;
                }
            }

            /// <summary>
            /// The edge information to be transmitted in the positive Y direction.
            /// </summary>
            public TEdge Edge_PosY
            {
                set
                {
                    _rayOut_y.Edge = value;
                    _write_y = true;
                }
            }

            /// <summary>
            /// The edge information transmitted from the negative Z direction.
            /// </summary>
            public TEdge Edge_NegZ
            {
                get
                {
                    return _rayIn_z.Edge;
                }
            }

            /// <summary>
            /// The edge information to be transmitted in the positive Z direction.
            /// </summary>
            public TEdge Edge_PosZ
            {
                set
                {
                    _rayOut_z.Edge = value;
                    _write_z = true;
                }
            }

            /// <summary>
            /// The face information for the quarter of the XY plane in the negative X, negative Y direction.
            /// </summary>
            public ref TFace Quarter_NegX_NegY
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_x.Face_NegX;
                    _merge(face, _rayIn_y.Face_NegY);
                    _freeFace_x_y = face;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the quarter of the XY plane in the positive X, negative Y direction.
            /// </summary>
            public ref TFace Quarter_PosX_NegY
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_y.Face_PosY;
                    _rayOut_x.Face_NegX = face;
                    _write_x = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the quarter of the XY plane in the negative X, positive Y direction.
            /// </summary>
            public ref TFace Quarter_NegX_PosY
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_x.Face_PosX;
                    _rayOut_y.Face_NegY = face;
                    _write_y = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the quarter of the XY plane in the positive X, positive Y direction.
            /// </summary>
            public ref TFace Quarter_PosX_PosY
            {
                get
                {
                    Pool<TFace>.Id face = _linker._faces.Allocate();
                    _rayOut_x.Face_PosX = face;
                    _rayOut_y.Face_PosY = face;
                    _write_x = true;
                    _write_y = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the half of the XY plane in the negative X direction.
            /// </summary>
            public ref TFace Half_NegX_Y
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_y.Face_NegY;
                    _rayOut_y.Face_NegY = face;
                    _write_y = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the half of the XY plane in the positive X direction.
            /// </summary>
            public ref TFace Half_PosX_Y
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_y.Face_PosY;
                    _rayOut_y.Face_PosY = face;
                    _write_y = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the half of the XY plane in the negative Y direction.
            /// </summary>
            public ref TFace Half_X_NegY
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_x.Face_NegX;
                    _rayOut_x.Face_NegX = face;
                    _write_x = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the half of the XY plane in the positive Y direction.
            /// </summary>
            public ref TFace Half_X_PosY
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_x.Face_PosX;
                    _rayOut_x.Face_PosX = face;
                    _write_x = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the XY plane except for the quarter in the negative X, negative Y direction.
            /// </summary>
            public Merge AntiQuarter_NegX_NegY
            {
                get
                {
                    uint leftRayBin = _linker._getNextFaceRay_x_y(Position.X);
                    Pool<TFace>.Id leftFace = _rayIn_x.Face_PosX;
                    Pool<TFace>.Id rightFace = _linker._rayBins_y[leftRayBin].Ray.Face_NegY;
                    _merge(rightFace, _rayIn_y.Face_PosY);
                    _linker._rayBins_y[leftRayBin].Ray.Face_NegY = leftFace;
                    _freeFace_x_y = rightFace;
                    return new Merge(_linker, leftFace, rightFace);
                }
            }

            /// <summary>
            /// The face information for the XY plane except for the quarter in the positive X, negative Y direction.
            /// </summary>
            public ref TFace AntiQuarter_PosX_NegY
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_y.Face_NegY;
                    _rayOut_x.Face_PosX = face;
                    _write_x = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the XY plane except for the quarter in the negative X, positive Y direction.
            /// </summary>
            public ref TFace AntiQuarter_NegX_PosY
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_x.Face_NegX;
                    _rayOut_y.Face_PosY = face;
                    _write_y = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the XY plane except for the quarter in the positive X, positive Y direction. 
            /// </summary>
            public Split AntiQuarter_PosX_PosY
            {
                get
                {
                    uint leftRayBin = _linker._getPrevFaceRay_x_y(Position.X);
                    Pool<TFace>.Id rightFace = _linker._rayBins_y[leftRayBin].Ray.Face_PosY;
                    Pool<TFace>.Id leftFace = _linker._faces.Allocate();
                    _linker._rayBins_y[leftRayBin].Ray.Face_PosY = leftFace;
                    _rayOut_y.Face_NegY = leftFace;
                    _rayOut_x.Face_NegX = rightFace;
                    _write_x = true;
                    _write_y = true;
                    return new Split(_linker, leftFace, rightFace);
                }
            }

            /// <summary>
            /// The face information for the XY plane surronding the corner.
            /// </summary>
            public ref TFace Full_X_Y
            {
                get
                {
                    uint leftRayBin = _linker._getPrevFaceRay_x_y(Position.X);
                    Pool<TFace>.Id face = _linker._rayBins_y[leftRayBin].Ray.Face_PosY;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the quarter of the XZ plane in the negative X, negative Z direction.
            /// </summary>
            public ref TFace Quarter_NegX_NegZ
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_x.Face_NegY;
                    _merge(face, _rayIn_z.Face_NegX);
                    _freeFace_x_z = face;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the quarter of the XZ plane in the positive X, negative Z direction.
            /// </summary>
            public ref TFace Quarter_PosX_NegZ
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_z.Face_PosX;
                    _rayOut_x.Face_NegY = face;
                    _write_x = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the quarter of the XZ plane in the negative X, positive Z direction.
            /// </summary>
            public ref TFace Quarter_NegX_PosZ
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_x.Face_PosY;
                    _rayOut_z.Face_NegX = face;
                    _write_z = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the quarter of the XZ plane in the positive X, positive Z direction.
            /// </summary>
            public ref TFace Quarter_PosX_PosZ
            {
                get
                {
                    Pool<TFace>.Id face = _linker._faces.Allocate();
                    _rayOut_x.Face_PosY = face;
                    _rayOut_z.Face_PosX = face;
                    _write_x = true;
                    _write_z = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the half of the XZ plane in the negative X direction.
            /// </summary>
            public ref TFace Half_NegX_Z
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_z.Face_NegX;
                    _rayOut_z.Face_NegX = face;
                    _write_z = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the half of the XZ plane in the positive X direction.
            /// </summary>
            public ref TFace Half_PosX_Z
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_z.Face_PosX;
                    _rayOut_z.Face_PosX = face;
                    _write_z = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the half of the XZ plane in the negative Z direction.
            /// </summary>
            public ref TFace Half_X_NegZ
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_x.Face_NegY;
                    _rayOut_x.Face_NegY = face;
                    _write_x = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the half of the XZ plane in the positive Z direction.
            /// </summary>
            public ref TFace Half_X_PosZ
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_x.Face_PosY;
                    _rayOut_x.Face_PosY = face;
                    _write_x = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the XZ plane except for the quarter in the negative X, negative Z direction.
            /// </summary>
            public Merge AntiQuarter_NegX_NegZ
            {
                get
                {
                    uint leftRayBin = _linker._getNextFaceRay_x_z(Position.X, Position.Y);
                    Pool<TFace>.Id leftFace = _rayIn_x.Face_PosY;
                    Pool<TFace>.Id rightFace = _linker._rayBins_z[leftRayBin].Ray.Face_NegX;
                    _merge(rightFace, _rayIn_z.Face_PosX);
                    _linker._rayBins_z[leftRayBin].Ray.Face_NegX = leftFace;
                    _freeFace_x_z = rightFace;
                    return new Merge(_linker, leftFace, rightFace);
                }
            }

            /// <summary>
            /// The face information for the XZ plane except for the quarter in the positive X, negative Z direction.
            /// </summary>
            public ref TFace AntiQuarter_PosX_NegZ
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_z.Face_NegX;
                    _rayOut_x.Face_PosY = face;
                    _write_x = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the XZ plane except for the quarter in the negative X, positive Z direction.
            /// </summary>
            public ref TFace AntiQuarter_NegX_PosZ
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_x.Face_NegY;
                    _rayOut_z.Face_PosX = face;
                    _write_z = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the XZ plane except for the quarter in the positive X, positive Z direction. 
            /// </summary>
            public Split AntiQuarter_PosX_PosZ
            {
                get
                {
                    uint leftRayBin = _linker._getPrevFaceRay_x_z(Position.X, Position.Y);
                    Pool<TFace>.Id rightFace = _linker._rayBins_z[leftRayBin].Ray.Face_PosX;
                    Pool<TFace>.Id leftFace = _linker._faces.Allocate();
                    _linker._rayBins_z[leftRayBin].Ray.Face_PosX = leftFace;
                    _rayOut_z.Face_NegX = leftFace;
                    _rayOut_x.Face_NegY = rightFace;
                    _write_x = true;
                    _write_z = true;
                    return new Split(_linker, leftFace, rightFace);
                }
            }

            /// <summary>
            /// The face information for the quarter of the YZ plane in the negative Y, negative Z direction.
            /// </summary>
            public ref TFace Quarter_NegY_NegZ
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_y.Face_NegX;
                    _merge(face, _rayIn_z.Face_NegY);
                    _freeFace_y_z = face;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the quarter of the YZ plane in the positive Y, negative Z direction.
            /// </summary>
            public ref TFace Quarter_PosY_NegZ
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_z.Face_PosY;
                    _rayOut_y.Face_NegX = face;
                    _write_y = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the quarter of the YZ plane in the negative Y, positive Z direction.
            /// </summary>
            public ref TFace Quarter_NegY_PosZ
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_y.Face_PosX;
                    _rayOut_z.Face_NegY = face;
                    _write_z = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the quarter of the YZ plane in the positive Y, positive Z direction.
            /// </summary>
            public ref TFace Quarter_PosY_PosZ
            {
                get
                {
                    Pool<TFace>.Id face = _linker._faces.Allocate();
                    _rayOut_y.Face_PosX = face;
                    _rayOut_z.Face_PosY = face;
                    _write_y = true;
                    _write_z = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the half of the YZ plane in the negative Y direction.
            /// </summary>
            public ref TFace Half_NegY_Z
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_z.Face_NegY;
                    _rayOut_z.Face_NegY = face;
                    _write_z = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the half of the YZ plane in the positive Y direction.
            /// </summary>
            public ref TFace Half_PosY_Z
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_z.Face_PosY;
                    _rayOut_z.Face_PosY = face;
                    _write_z = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the half of the YZ plane in the negative Z direction.
            /// </summary>
            public ref TFace Half_Y_NegZ
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_y.Face_NegX;
                    _rayOut_y.Face_NegX = face;
                    _write_y = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the half of the YZ plane in the positive Z direction.
            /// </summary>
            public ref TFace Half_Y_PosZ
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_y.Face_PosX;
                    _rayOut_y.Face_PosX = face;
                    _write_y = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the YZ plane except for the quarter in the negative Y, negative Z direction.
            /// </summary>
            public Merge AntiQuarter_NegY_NegZ
            {
                get
                {
                    uint leftRayBin = _linker._getNextFaceRay_y_z(Position.X, Position.Y);
                    Pool<TFace>.Id leftFace = _rayIn_y.Face_PosX;
                    Pool<TFace>.Id rightFace = _linker._rayBins_z[leftRayBin].Ray.Face_NegY;
                    _merge(rightFace, _rayIn_z.Face_PosY);
                    _linker._rayBins_z[leftRayBin].Ray.Face_NegY = leftFace;
                    _freeFace_y_z = rightFace;
                    return new Merge(_linker, leftFace, rightFace);
                }
            }

            /// <summary>
            /// The face information for the YZ plane except for the quarter in the positive Y, negative Z direction.
            /// </summary>
            public ref TFace AntiQuarter_PosY_NegZ
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_z.Face_NegY;
                    _rayOut_y.Face_PosX = face;
                    _write_y = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the YZ plane except for the quarter in the negative Y, positive Z direction.
            /// </summary>
            public ref TFace AntiQuarter_NegY_PosZ
            {
                get
                {
                    Pool<TFace>.Id face = _rayIn_y.Face_NegX;
                    _rayOut_z.Face_PosY = face;
                    _write_z = true;
                    return ref _linker._faces[face];
                }
            }

            /// <summary>
            /// The face information for the YZ plane except for the quarter in the positive Y, positive Z direction. 
            /// </summary>
            public Split AntiQuarter_PosY_PosZ
            {
                get
                {
                    uint leftRayBin = _linker._getPrevFaceRay_y_z(Position.X, Position.Y);
                    Pool<TFace>.Id rightFace = _linker._rayBins_z[leftRayBin].Ray.Face_PosY;
                    Pool<TFace>.Id leftFace = _linker._faces.Allocate();
                    _linker._rayBins_z[leftRayBin].Ray.Face_PosY = leftFace;
                    _rayOut_z.Face_NegY = leftFace;
                    _rayOut_y.Face_NegX = rightFace;
                    _write_y = true;
                    _write_z = true;
                    return new Split(_linker, leftFace, rightFace);
                }
            }

            /// <summary>
            /// Commits the changes made to this corner.
            /// </summary>
            public void Commit()
            {
                if (!_freeFace_x_y.IsDummy) _linker._faces.Free(_freeFace_x_y);
                if (!_freeFace_x_z.IsDummy) _linker._faces.Free(_freeFace_x_z);
                if (!_freeFace_y_z.IsDummy) _linker._faces.Free(_freeFace_y_z);
                _rayIn_x = _rayOut_x;
                if (_write_y) _rayIn_y = _rayOut_y;
                else if (_rayBinCache_y != uint.MaxValue) _linker._removeRay_y(_rayBinCache_y);
                if (_write_z) _rayIn_z = _rayOut_z;
                else if (_rayBinCache_z != uint.MaxValue) _linker._removeRay_z(_rayBinCache_z);
            }

            /// <summary>
            /// Commits the changes made to this corner.
            /// </summary>
            public void Dispose()
            {
                Commit();
            }

            /// <summary>
            /// Verifies that two faces are the same.
            /// </summary>
            private void _merge(Pool<TFace>.Id a, Pool<TFace>.Id b)
            {
                Debug.Assert(a == b);
            }

            /// <summary>
            /// The ray which provides incoming information along the X axis.
            /// </summary>
            private ref _Ray _rayIn_x
            {
                get
                {
                    return ref _linker._ray_x;
                }
            }

            /// <summary>
            /// Indicates whether there is outgoing information along the X axis.
            /// </summary>
            private bool _write_x
            {
                set
                {
                    // Not actually important to track in the X direction
                }
            }

            /// <summary>
            /// The ray which provides incoming information along the Y axis.
            /// </summary>
            private ref _Ray _rayIn_y
            {
                get
                {
                    if (_rayBinCache_y == uint.MaxValue)
                        _rayBinCache_y = _linker._getRay_y(Position.X);
                    return ref _linker._rayBins_y[_rayBinCache_y].Ray;
                }
            }

            /// <summary>
            /// The ray which provides incoming information along the Z axis.
            /// </summary>
            private ref _Ray _rayIn_z
            {
                get
                {
                    if (_rayBinCache_z == uint.MaxValue)
                        _rayBinCache_z = _linker._getRay_z(Position.X_Y);
                    return ref _linker._rayBins_z[_rayBinCache_z].Ray;
                }
            }
        }

        /// <summary>
        /// Provides information about the independent face branches around a splitting corner.
        /// </summary>
        public struct Split
        {
            private CornerLinker3<TEdge, TFace> _linker;
            private Pool<TFace>.Id _left;
            private Pool<TFace>.Id _right;
            public Split(CornerLinker3<TEdge, TFace> linker, Pool<TFace>.Id left, Pool<TFace>.Id right)
            {
                _linker = linker;
                _left = left;
                _right = right;
            }

            /// <summary>
            /// The face information to be transmitted down the left (low X, high Y) branch of the split.
            /// </summary>
            public ref TFace Left
            {
                get
                {
                    return ref _linker._faces[_left];
                }
            }

            /// <summary>
            /// The face information to be transmitted down the right (high X, low Y) branch of the split. This
            /// also contains the face information prior to the split.
            /// </summary>
            public ref TFace Right
            {
                get
                {
                    return ref _linker._faces[_right];
                }
            }
        }

        /// <summary>
        /// Provides information about the independent face branches around a merging corner.
        /// </summary>
        public struct Merge
        {
            private CornerLinker3<TEdge, TFace> _linker;
            private Pool<TFace>.Id _left;
            private Pool<TFace>.Id _right;
            public Merge(CornerLinker3<TEdge, TFace> linker, Pool<TFace>.Id left, Pool<TFace>.Id right)
            {
                _linker = linker;
                _left = left;
                _right = right;
            }

            /// <summary>
            /// The face information transmitted from the left (low X, high Y) branch of the merge. This will
            /// also contain the final merged face information to be transmitted to later corners.
            /// </summary>
            public ref TFace Left
            {
                get
                {
                    return ref _linker._faces[_left];
                }
            }

            /// <summary>
            /// The face information transmitted from the right (high X, low Y) branch of the merge.
            /// </summary>
            public ref TFace Right
            {
                get
                {
                    return ref _linker._faces[_right];
                }
            }
        }

        /// <summary>
        /// Encasulates the information available along an axis-aligned ray in 3D space.
        /// </summary>
        private struct _Ray
        {
            /// <summary>
            /// The edge information from the origin of the ray.
            /// </summary>
            public TEdge Edge;

            /// <summary>
            /// The identifier for the face along the negative X direction of the ray,
            /// in the local coordinate system of the ray.
            /// </summary>
            public Pool<TFace>.Id Face_NegX;

            /// <summary>
            /// The identifier for the face along the positive X direction of the ray,
            /// in the local coordinate system of the ray.
            /// </summary>
            public Pool<TFace>.Id Face_PosX;

            /// <summary>
            /// The identifier for the face along the negative Y direction of the ray,
            /// in the local coordinate system of the ray.
            /// </summary>
            public Pool<TFace>.Id Face_NegY;

            /// <summary>
            /// The identifier for the face along the positive Y direction of the ray,
            /// in the local coordinate system of the ray.
            /// </summary>
            public Pool<TFace>.Id Face_PosY;
        }

        /// <summary>
        /// A bin in the hashtable implemented by <see cref="_rayBins_y"/>.
        /// </summary>
        private struct _RayBin_Y
        {
            public int X;
            public _Ray Ray;
            public _RayBin_Y(int x, _Ray ray)
            {
                X = x;
                Ray = ray;
            }

            /// <summary>
            /// An empty <see cref="_RayBin_Y"/>.
            /// </summary>
            public static _RayBin_Y Empty => new _RayBin_Y(_emptyCode, default);

            /// <summary>
            /// Indicates whether this bin is empty.
            /// </summary>
            public bool IsEmpty
            {
                get
                {
                    return X == _emptyCode;
                }
            }
        }

        /// <summary>
        /// A bin in the hashtable implemented by <see cref="_RayBin_Z"/>.
        /// </summary>
        private struct _RayBin_Z
        {
            public Vector2i X_Y;
            public _Ray Ray;
            public _RayBin_Z(Vector2i x_y, _Ray ray)
            {
                X_Y = x_y;
                Ray = ray;
            }

            /// <summary>
            /// The X coordinate of this ray.
            /// </summary>
            public int X => X_Y.X;

            /// <summary>
            /// The Y coordinate of this ray.
            /// </summary>
            public int Y => X_Y.Y;

            /// <summary>
            /// An empty <see cref="_RayBin_Z"/>.
            /// </summary>
            public static _RayBin_Z Empty => new _RayBin_Z(new Vector2i(_emptyCode, _emptyCode), default);

            /// <summary>
            /// Indicates whether this bin is empty.
            /// </summary>
            public bool IsEmpty
            {
                get
                {
                    return X_Y.X == _emptyCode;
                }
            }
        }

        /// <summary>
        /// A coordinate value used to indicate an associated bin is empty.
        /// </summary>
        private const int _emptyCode = int.MaxValue - 1;

        /// <summary>
        /// Finds the index of the bin in <see cref="_rayBins_y"/> containing information about the
        /// ray going in the Y direction at the given X coordinate. This will allocate the bin if
        /// it does not already exist.
        /// </summary>
        private uint _getRay_y(int x)
        {
            // Find target or empty bin
            int hash = x.GetHashCode();
            uint bin = _rayBins_y.GetBinPow2(hash);
            while (!_rayBins_y[bin].IsEmpty)
            {
                if (_rayBins_y[bin].X == x)
                    return bin;
                bin = _rayBins_y.Next(bin);
            }

            // Allocate bin
            Debug.Assert(_rayCapacity_y > 0);
            _rayCapacity_y--;
            _rayBins_y[bin] = new _RayBin_Y(x, default(_Ray));
            return bin;
        }

        /// <summary>
        /// Finds the index of the bin in <see cref="_rayBins_z"/> containing information about the
        /// ray going in the Z direction at the given X, Y coordinate. This will allocate the bin if
        /// it does not already exist.
        /// </summary>
        private uint _getRay_z(Vector2i x_y)
        {
            // Find target or empty bin
            int hash = x_y.GetHashCode();
            uint bin = _rayBins_z.GetBinPow2(hash);
            while (!_rayBins_z[bin].IsEmpty)
            {
                if (_rayBins_z[bin].X_Y == x_y)
                    return bin;
                bin = _rayBins_z.Next(bin);
            }

            // Allocate bin
            Debug.Assert(_rayCapacity_z > 0);
            _rayCapacity_z--;
            _rayBins_z[bin] = new _RayBin_Z(x_y, default(_Ray));
            return bin;
        }

        /// <summary>
        /// Finds the index of the bin in <see cref="_rayBins_y"/> whose ray's <see cref="_Ray.Face_PosY"/>
        /// covers a specific point.
        /// </summary>
        /// <param name="x">The X coordinate of the point to get the face ray for.</param>
        private uint _getPrevFaceRay_x_y(int x)
        {
            uint bestBin = 0;
            int bestX = int.MinValue;
            for (int i = 0; i < _rayBins_y.Length; i++)
            {
                _RayBin_Y ray = _rayBins_y[i];
                if (!ray.IsEmpty && ray.X < x && ray.X >= bestX)
                {
                    Debug.Assert(ray.X > bestX);
                    bestBin = (uint)i;
                    bestX = ray.X;
                }
            }
            Debug.Assert(bestX != int.MinValue);
            return bestBin;
        }

        /// <summary>
        /// Finds the index of the bin in <see cref="_rayBins_y"/> whose ray's <see cref="_Ray.Face_NegY"/>
        /// covers a specific point.
        /// </summary>
        /// <param name="x">The X coordinate of the point to get the face ray for.</param>
        private uint _getNextFaceRay_x_y(int x)
        {
            uint bestBin = 0;
            int bestX = int.MaxValue;
            for (int i = 0; i < _rayBins_y.Length; i++)
            {
                _RayBin_Y ray = _rayBins_y[i];
                if (!ray.IsEmpty && ray.X > x && ray.X <= bestX)
                {
                    Debug.Assert(ray.X < bestX);
                    bestBin = (uint)i;
                    bestX = ray.X;
                }
            }
            Debug.Assert(bestX != int.MinValue);
            return bestBin;
        }

        /// <summary>
        /// Finds the index of the bin in <see cref="_rayBins_z"/> whose ray's <see cref="_Ray.Face_PosX"/>
        /// covers a specific point.
        /// </summary>
        /// <param name="x">The X coordinate of the point to get the face ray for.</param>
        /// <param name="y">The Y coordinate of the plane.</param>
        private uint _getPrevFaceRay_x_z(int x, int y)
        {
            // TODO: Indexing for performance
            uint bestBin = 0;
            int bestX = int.MinValue;
            for (int i = 0; i < _rayBins_z.Length; i++)
            {
                _RayBin_Z ray = _rayBins_z[i];
                if (!ray.IsEmpty && ray.Y == y && ray.X < x && ray.X >= bestX)
                {
                    Debug.Assert(ray.X > bestX);
                    bestBin = (uint)i;
                    bestX = ray.X;
                }
            }
            Debug.Assert(bestX != int.MinValue);
            return bestBin;
        }

        /// <summary>
        /// Finds the index of the bin in <see cref="_rayBins_z"/> whose ray's <see cref="_Ray.Face_NegX"/>
        /// covers a specific point.
        /// </summary>
        /// <param name="x">The X coordinate of the point to get the face ray for.</param>
        /// <param name="y">The Y coordinate of the plane.</param>
        private uint _getNextFaceRay_x_z(int x, int y)
        {
            // TODO: Indexing for performance
            uint bestBin = 0;
            int bestX = int.MaxValue;
            for (int i = 0; i < _rayBins_z.Length; i++)
            {
                _RayBin_Z ray = _rayBins_z[i];
                if (!ray.IsEmpty && ray.Y == y && ray.X > x && ray.X <= bestX)
                {
                    Debug.Assert(ray.X < bestX);
                    bestBin = (uint)i;
                    bestX = ray.X;
                }
            }
            Debug.Assert(bestX != int.MinValue);
            return bestBin;
        }

        /// <summary>
        /// Finds the index of the bin in <see cref="_rayBins_z"/> whose ray's <see cref="_Ray.Face_PosY"/>
        /// covers a specific point.
        /// </summary>
        /// <param name="x">The X coordinate of the plane.</param>
        /// <param name="y">The Y coordinate of the point to get the face ray for.</param>
        private uint _getPrevFaceRay_y_z(int x, int y)
        {
            // TODO: Indexing for performance
            uint bestBin = 0;
            int bestY = int.MinValue;
            for (int i = 0; i < _rayBins_z.Length; i++)
            {
                _RayBin_Z ray = _rayBins_z[i];
                if (!ray.IsEmpty && ray.X == x && ray.Y < y && ray.Y >= bestY)
                {
                    Debug.Assert(ray.Y > bestY);
                    bestBin = (uint)i;
                    bestY = ray.Y;
                }
            }
            Debug.Assert(bestY != int.MinValue);
            return bestBin;
        }

        /// <summary>
        /// Finds the index of the bin in <see cref="_rayBins_z"/> whose ray's <see cref="_Ray.Face_NegY"/>
        /// covers a specific point.
        /// </summary>
        /// <param name="x">The X coordinate of the plane.</param>
        /// <param name="y">The Y coordinate of the point to get the face ray for.</param>
        private uint _getNextFaceRay_y_z(int x, int y)
        {
            // TODO: Indexing for performance
            uint bestBin = 0;
            int bestY = int.MaxValue;
            for (int i = 0; i < _rayBins_z.Length; i++)
            {
                _RayBin_Z ray = _rayBins_z[i];
                if (!ray.IsEmpty && ray.X == x && ray.Y > y && ray.Y <= bestY)
                {
                    Debug.Assert(ray.Y < bestY);
                    bestBin = (uint)i;
                    bestY = ray.Y;
                }
            }
            Debug.Assert(bestY != int.MinValue);
            return bestBin;
        }

        /// <summary>
        /// Removes the ray in the given location of <see cref="_rayBins_y"/>.
        /// </summary>
        private void _removeRay_y(uint bin)
        {
            _rayCapacity_y++;
            _rayBins_y[bin] = _RayBin_Y.Empty;

        fix:
            // Since this is open addressing, we need to make sure not to break a chain
            uint nBin = bin;
            while (true)
            {
                nBin = _rayBins_y.Next(nBin);
                if (_rayBins_y[nBin].IsEmpty)
                    return;
                int nHash = _rayBins_y[nBin].X.GetHashCode();
                uint ntBin = _rayBins_y.GetBinPow2(nHash);
                if (!Hashtable.BetweenMod(bin, ntBin, nBin))
                {
                    _rayBins_y[bin] = _rayBins_y[nBin];
                    _rayBins_y[nBin] = _RayBin_Y.Empty;
                    bin = nBin;
                    goto fix;
                }
            }
        }

        /// <summary>
        /// Removes the ray in the given location of <see cref="_rayBins_z"/>.
        /// </summary>
        private void _removeRay_z(uint bin)
        {
            _rayCapacity_z++;
            _rayBins_z[bin] = _RayBin_Z.Empty;

        fix:
            // Since this is open addressing, we need to make sure not to break a chain
            uint nBin = bin;
            while (true)
            {
                nBin = _rayBins_z.Next(nBin);
                if (_rayBins_z[nBin].IsEmpty)
                    return;
                int nHash = _rayBins_z[nBin].X_Y.GetHashCode();
                uint ntBin = _rayBins_z.GetBinPow2(nHash);
                if (!Hashtable.BetweenMod(bin, ntBin, nBin))
                {
                    _rayBins_z[bin] = _rayBins_z[nBin];
                    _rayBins_z[nBin] = _RayBin_Z.Empty;
                    bin = nBin;
                    goto fix;
                }
            }
        }

        /// <summary>
        /// Ensures that <see cref="_rayBins_y"/> and <see cref="_rayBins_z"/> each have the capacity to accept at
        /// least one new ray.
        /// </summary>
        private void _prepareBins()
        {
            // Resize _rayBins_y if needed
            if (_rayCapacity_y <= 1)
            {
                _RayBin_Y[] nRayBins_y = new _RayBin_Y[_rayBins_y.Length * 2];
                _rayCapacity_y = _capacity(nRayBins_y.Length);
                for (uint i = 0; i < nRayBins_y.Length; i++)
                    nRayBins_y[i] = _RayBin_Y.Empty;
                for (uint i = 0; i < _rayBins_y.Length; i++)
                {
                    var ray = _rayBins_y[i];
                    if (!ray.IsEmpty)
                    {
                        int hash = ray.X.GetHashCode();
                        uint bin = nRayBins_y.GetBinPow2(hash);
                        while (!nRayBins_y[bin].IsEmpty) bin = nRayBins_y.Next(bin);
                        nRayBins_y[bin] = ray;
                        _rayCapacity_y--;
                    }
                }
                _rayBins_y = nRayBins_y;
            }

            // Resize _rayBins_z if needed
            if (_rayCapacity_z <= 1)
            {
                _RayBin_Z[] nRayBins_z = new _RayBin_Z[_rayBins_z.Length * 2];
                _rayCapacity_z = _capacity(nRayBins_z.Length);
                for (uint i = 0; i < nRayBins_z.Length; i++)
                    nRayBins_z[i] = _RayBin_Z.Empty;
                for (uint i = 0; i < _rayBins_z.Length; i++)
                {
                    var ray = _rayBins_z[i];
                    if (!ray.IsEmpty)
                    {
                        int hash = ray.X_Y.GetHashCode();
                        uint bin = nRayBins_z.GetBinPow2(hash);
                        while (!nRayBins_z[bin].IsEmpty) bin = nRayBins_z.Next(bin);
                        nRayBins_z[bin] = ray;
                        _rayCapacity_z--;
                    }
                }
                _rayBins_z = nRayBins_z;
            }
        }

        /// <summary>
        /// Gets the capacity of a hash table with the given size.
        /// </summary>
        private static uint _capacity(int size)
        {
            return ((uint)size * 3) / 5;
        }
    }
}
