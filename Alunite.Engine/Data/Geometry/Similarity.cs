﻿using System;

using Alunite.Data.Probability;

namespace Alunite.Data.Geometry
{
    /// <summary>
    /// A shape-preserving transformation in two-dimensional space consisting of a translation,
    /// uniform scaling, a rotation and potentially a reflection.
    /// </summary>
    public struct Similarity2
    {
        private Scalar _scaling_reflects;
        private Similarity2(Scalar scaling_reflects, Rotation2 rot, Vector2 offset)
        {
            _scaling_reflects = scaling_reflects;
            Rotation = rot;
            Offset = offset;
        }

        public Similarity2(Scalar scaling, Rotation2 rot, bool reflects, Vector2 offset)
        {
            _scaling_reflects = scaling;
            if (reflects)
                _scaling_reflects = -_scaling_reflects;
            Rotation = rot;
            Offset = offset;
        }

        /// <summary>
        /// The scale factor applied by this <see cref="Similarity2"/>.
        /// </summary>
        public Scalar Scaling
        {
            get
            {
                return Scalar.Abs(_scaling_reflects);
            }
            set
            {
                bool reflects = Reflects;
                _scaling_reflects = value;
                if (reflects)
                    _scaling_reflects = -_scaling_reflects;
            }
        }

        /// <summary>
        /// The rotation component of this <see cref="Similarity2"/>.
        /// </summary>
        public Rotation2 Rotation;

        /// <summary>
        /// Indicates whether this <see cref="Similarity2"/> involves a reflection.
        /// </summary>
        public bool Reflects
        {
            get
            {
                return _scaling_reflects < 0;
            }
            set
            {
                if ((_scaling_reflects < 0) != value)
                    _scaling_reflects = -_scaling_reflects;
            }
        }

        /// <summary>
        /// The translation component of this transformation.
        /// </summary>
        public Vector2 Offset;

        /// <summary>
        /// The linear component of this transformation.
        /// </summary>
        public Matrix2x2 Linear
        {
            get
            {
                Matrix2x2 linear = Rotation;
                linear.X *= Scaling;
                linear.Y *= _scaling_reflects;
                return linear;
            }
        }
            
        /// <summary>
        /// The identity motion.
        /// </summary>
        public static Similarity2 Identity => new Similarity2(1, Rotation2.Identity, Vector2.Zero);

        /// <summary>
        /// The <see cref="Similarity2"/> which swaps the X and Y axis.
        /// </summary>
        public static Similarity2 Swap => new Similarity2(-1, Rotation2.Cross, Vector2.Zero);

        /// <summary>
        /// Constructs a similarity which translates by the given amount.
        /// </summary>
        public static Similarity2 Translate(Vector2 offset)
        {
            return new Similarity2(1, Rotation2.Identity, offset);
        }

        /// <summary>
        /// Constructs a similarity which translates by the given amount.
        /// </summary>
        public static Similarity2 Translate(Scalar x, Scalar y)
        {
            return Translate(new Vector2(x, y));
        }

        /// <summary>
        /// Constructs a similarity which applies a uniform scaling of the given amount.
        /// </summary>
        public static Similarity2 ScaleUniform(Scalar amount)
        {
            if (amount <= 0)
                throw new ArgumentException("Scale factor must be positive", nameof(amount));
            return new Similarity2(amount, Rotation2.Identity, Vector2.Zero);
        }

        /// <summary>
        /// Gets the inverse of this transformation.
        /// </summary>
        public Similarity2 Inverse
        {
            get
            {
                Scalar invScaling_reflects = 1 / _scaling_reflects;
                Rotation2 nRot = Rotation;
                if (!Reflects)
                    nRot = nRot.Inverse;
                Vector2 nOffset = -Offset;
                nOffset.X *= Scalar.Abs(invScaling_reflects);
                nOffset.Y *= invScaling_reflects;
                nOffset = nRot * nOffset;
                return new Similarity2(invScaling_reflects, nRot, nOffset);
            }
        }

        /// <summary>
        /// Applies this transformation to a position.
        /// </summary>
        public Vector2 Apply(Vector2 pos)
        {
            pos.X *= Scaling;
            pos.Y *= _scaling_reflects;
            return Rotation * pos + Offset;
        }

        /// <summary>
        /// Composes the given transformations, first applying the right, then the left.
        /// </summary>
        public static Similarity2 Compose(Similarity2 a, Similarity2 b)
        {
            Rotation2 rot = b.Rotation;
            if (a.Reflects)
                rot = rot.Inverse;
            rot = a.Rotation * rot;
            return new Similarity2(a._scaling_reflects * b._scaling_reflects, rot, a.Apply(b.Offset));
        }

        /// <summary>
        /// Determines whether the given <see cref="Similarity2"/>s likely have the same true value
        /// after compensating for representation error.
        /// </summary>
        public static bool AreLikelyEqual(Similarity2 a, Similarity2 b)
        {
            return AreEqualOdds(a, b) > 1;
        }

        /// <summary>
        /// Determines the relative probability that the given <see cref="Similarity2"/>s have the same true value, over
        /// the probability that their true values are independent. This is based on the apriori distribution of
        /// matrix values, and individual value distributions of <paramref name="a"/> and <paramref name="b"/>.
        /// </summary>
        public static LogScalar AreEqualOdds(Similarity2 a, Similarity2 b)
        {
            return Vector2.AreEqualOdds(a.Offset, b.Offset) *
                Scalar.AreEqualOdds(a._scaling_reflects, b._scaling_reflects) *
                Rotation2.AreEqualOdds(a.Rotation, b.Rotation);
        }

        public static implicit operator Similarity2(Rotation2 source)
        {
            return new Similarity2(1, source, Vector2.Zero);
        }

        public static implicit operator Similarity2(Motion2 source)
        {
            return new Similarity2(1, source.Linear, source.Offset);
        }

        public static Vector2 operator *(Similarity2 a, Vector2 b)
        {
            return a.Apply(b);
        }

        public static Similarity2 operator *(Similarity2 a, Similarity2 b)
        {
            return Compose(a, b);
        }
    }
}
