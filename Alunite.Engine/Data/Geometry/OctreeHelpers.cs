﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Alunite.Data.Geometry
{
    /// <summary>
    /// A helper for implementing mappings and transformations on octrees that result in duetrees.
    /// </summary>
    public struct OctreeTransformer1<T, TN>
    {
        private TempCache<OctreeDictionary.Octree, Wad> _cache;
        public OctreeTransformer1(
            Func<T, TN> mapping, Duet<Octant> trans,
            OctreeDictionary<T> source,
            WadDictionary<WadDuet, TN> target)
        {
            Mapping = mapping;
            Transformation = trans;
            Source = source;
            Target = target;
            _cache = new TempCache<OctreeDictionary.Octree, Wad>(32);
        }

        /// <summary>
        /// The mapping function applied to leaf values.
        /// </summary>
        public Func<T, TN> Mapping { get; }

        /// <summary>
        /// Provides the source octant for a given target direction.
        /// </summary>
        public Duet<Octant> Transformation { get; }

        /// <summary>
        /// The dictionary where source octrees are defined.
        /// </summary>
        public OctreeDictionary<T> Source { get; }

        /// <summary>
        /// The factory where target octrees are defined.
        /// </summary>
        public WadDictionary<WadDuet, TN> Target { get; }

        /// <summary>
        /// Gets the mapped and transformed form of the given source octree.
        /// </summary>
        public Wad Transform(OctreeDictionary.Octree source)
        {
            if (Source.IsLeaf(source, out T leaf))
            {
                return Target.Leaf(Mapping(leaf));
            }
            else
            {
                // Check cache
                if (!_cache.TryGet(source, out Wad res))
                {
                    OctreeDictionary.Octet octet = Source.AsGroup(source);
                    res = Target.Group(new WadDuet(
                        Transform(octet[Transformation.Neg]),
                        Transform(octet[Transformation.Pos])));
                    _cache.Add(source, res);
                }
                return res;
            }
        }
    }

    /// <summary>
    /// A helper for implementing mappings and transformations on octrees that result in <see cref="Quadtree"/>s.
    /// </summary>
    public struct OctreeTransformer2<T, TN>
    {
        private TempCache<OctreeDictionary.Octree, Wad> _cache;
        public OctreeTransformer2(
            Func<T, TN> mapping, Quartet<Octant> trans,
            OctreeDictionary<T> source,
            WadDictionary<WadQuartet, TN> target)
        {
            Mapping = mapping;
            Transformation = trans;
            Source = source;
            Target = target;
            _cache = new TempCache<OctreeDictionary.Octree, Wad>(32);
        }

        /// <summary>
        /// The mapping function applied to leaf values.
        /// </summary>
        public Func<T, TN> Mapping { get; }

        /// <summary>
        /// Provides the source octant for a given target quadrant.
        /// </summary>
        public Quartet<Octant> Transformation { get; }

        /// <summary>
        /// The dictionary where source octrees are defined.
        /// </summary>
        public OctreeDictionary<T> Source { get; }

        /// <summary>
        /// The factory where target octrees are defined.
        /// </summary>
        public WadDictionary<WadQuartet, TN> Target { get; }

        /// <summary>
        /// Gets the mapped and transformed form of the given source octree.
        /// </summary>
        public Wad Transform(OctreeDictionary.Octree source)
        {
            if (Source.IsLeaf(source, out T leaf))
            {
                return Target.Leaf(Mapping(leaf));
            }
            else
            {
                // Check cache
                if (!_cache.TryGet(source, out Wad res))
                {
                    OctreeDictionary.Octet octet = Source.AsGroup(source);
                    res = Target.Group(new WadQuartet(
                        Transform(octet[Transformation.NegX_NegY]),
                        Transform(octet[Transformation.PosX_NegY]),
                        Transform(octet[Transformation.NegX_PosY]),
                        Transform(octet[Transformation.PosX_PosY])));
                    _cache.Add(source, res);
                }
                return res;
            }
        }
    }

    /// <summary>
    /// A helper for implementing mappings and transformations on octrees.
    /// </summary>
    public struct OctreeTransformer3<T, TN>
    {
        private TempCache<OctreeDictionary.Octree, OctreeDictionary.Octree> _cache;
        public OctreeTransformer3(
            Func<T, TN> mapping, Octet<Octant> trans,
            OctreeDictionary<T> source,
            OctreeFactory<TN> target)
        {
            Mapping = mapping;
            Transformation = trans;
            Source = source;
            Target = target;
            _cache = new TempCache<OctreeDictionary.Octree, OctreeDictionary.Octree>(32);
        }

        /// <summary>
        /// The mapping function applied to leaf values.
        /// </summary>
        public Func<T, TN> Mapping { get; }

        /// <summary>
        /// Provides the source octant for a given target octant.
        /// </summary>
        public Octet<Octant> Transformation { get; }

        /// <summary>
        /// The dictionary where source octrees are defined.
        /// </summary>
        public OctreeDictionary<T> Source { get; }

        /// <summary>
        /// The factory where target octrees are defined.
        /// </summary>
        public OctreeFactory<TN> Target { get; }

        /// <summary>
        /// Gets the mapped and transformed form of the given source octree.
        /// </summary>
        public OctreeDictionary.Octree Transform(OctreeDictionary.Octree source)
        {
            if (Source.IsLeaf(source, out T leaf))
            {
                return Target.Leaf(Mapping(leaf));
            }
            else
            {
                if (!_cache.TryGet(source, out OctreeDictionary.Octree res))
                {
                    OctreeDictionary.Octet octet = Source.AsGroup(source);
                    res = Target.Group(new OctreeDictionary.Octet(
                        Transform(octet[Transformation.NegX_NegY_NegZ]),
                        Transform(octet[Transformation.PosX_NegY_NegZ]),
                        Transform(octet[Transformation.NegX_PosY_NegZ]),
                        Transform(octet[Transformation.PosX_PosY_NegZ]),
                        Transform(octet[Transformation.NegX_NegY_PosZ]),
                        Transform(octet[Transformation.PosX_NegY_PosZ]),
                        Transform(octet[Transformation.NegX_PosY_PosZ]),
                        Transform(octet[Transformation.PosX_PosY_PosZ])));
                    _cache.Add(source, res);
                }
                return res;
            }
        }
    }

    /// <summary>
    /// An extension to <see cref="OctreeTransformer3{T, TN}"/> which allows for specified octants to
    /// be set to a default value instead of being mapped.
    /// </summary>
    public struct OctreeSwizzler<T, TN>
    {
        private TempCache<OctreeDictionary.Octree, OctreeDictionary.Octree> _cache;
        public OctreeSwizzler(
            Func<T, TN> mapping, TN def, Octet<Octant?> trans,
            OctreeDictionary<T> source,
            OctreeFactory<TN> target)
        {
            Mapping = mapping;
            Default = target.Leaf(def);
            Transformation = trans;
            Source = source;
            Target = target;
            _cache = new TempCache<OctreeDictionary.Octree, OctreeDictionary.Octree>(32);
        }

        /// <summary>
        /// The mapping function applied to leaf values.
        /// </summary>
        public Func<T, TN> Mapping { get; }

        /// <summary>
        /// The default (leaf) value given to leaf values with no source specified in
        /// <see cref="Transformation"/>.
        /// </summary>
        public OctreeDictionary.Octree Default { get; }

        /// <summary>
        /// Provides the source octant for a given target octant, or null for octants that
        /// should be given the default value.
        /// </summary>
        public Octet<Octant?> Transformation { get; }

        /// <summary>
        /// The dictionary where source octrees are defined.
        /// </summary>
        public OctreeDictionary<T> Source { get; }

        /// <summary>
        /// The factory where target octrees are defined.
        /// </summary>
        public OctreeFactory<TN> Target { get; }

        /// <summary>
        /// Gets the swizzled form of the given source octree.
        /// </summary>
        public OctreeDictionary.Octree Swizzle(OctreeDictionary.Octree source)
        {
            if (Source.IsLeaf(source, out T leaf))
            {
                return Target.Leaf(Mapping(leaf));
            }
            else
            {
                if (!_cache.TryGet(source, out OctreeDictionary.Octree res))
                {
                    OctreeDictionary.Octet sourceOctet = Source.AsGroup(source);
                    OctreeDictionary.Octet resOctet = OctreeDictionary.Octet.Uniform(Default);
                    for (int i = 0; i < 8; i++)
                    {
                        Octant to = (Octant)i;
                        Octant? from = Transformation[to];
                        if (from != null)
                            resOctet[to] = Swizzle(sourceOctet[from.Value]);
                    }
                    res = Target.Group(resOctet);
                    _cache.Add(source, res);
                }
                return res;
            }
        }
    }

    /// <summary>
    /// A helper for applying a function of two inputs to the leaf values of two octrees.
    /// </summary>
    public struct OctreeLifter2<TA, TB, TN>
    {
        private TempCache<Domain, OctreeDictionary.Octree> _cache;
        public OctreeLifter2(
            Func<TA, TB, TN> func,
            OctreeDictionary<TA> source_a,
            OctreeDictionary<TB> source_b,
            OctreeFactory<TN> target)
        {
            Func = func;
            Source_A = source_a;
            Source_B = source_b;
            Target = target;
            _cache = new TempCache<Domain, OctreeDictionary.Octree>(64);
        }

        internal OctreeLifter2(
            Func<TA, TB, TN> func,
            OctreeDictionary<TA> source_a,
            OctreeDictionary<TB> source_b,
            OctreeFactory<TN> target,
            TempCache<Domain, OctreeDictionary.Octree> cache)
        {
            Func = func;
            Source_A = source_a;
            Source_B = source_b;
            Target = target;
            _cache = cache;
        }

        /// <summary>
        /// The mapping function applied to leaf values.
        /// </summary>
        public Func<TA, TB, TN> Func { get; }

        /// <summary>
        /// The dictionary where source octrees of the first input are defined.
        /// </summary>
        public OctreeDictionary<TA> Source_A { get; }

        /// <summary>
        /// The dictionary where source octrees of the second input are defined.
        /// </summary>
        public OctreeDictionary<TB> Source_B { get; }

        /// <summary>
        /// The factory where target octrees are defined.
        /// </summary>
        public OctreeFactory<TN> Target { get; }

        /// <summary>
        /// Applies <see cref="Func"/> to the given input octrees.
        /// </summary>
        public OctreeDictionary.Octree Lift(OctreeDictionary.Octree a, OctreeDictionary.Octree b)
        {
            // Base case
            TA leaf_a; TB leaf_b;
            if (Source_A.IsLeaf(a, out leaf_a) && Source_B.IsLeaf(b, out leaf_b))
                return Target.Leaf(Func(leaf_a, leaf_b));

            // General case
            var domain = new Domain(a, b);
            if (_cache.TryGet(domain, out OctreeDictionary.Octree res))
            {
                return res;
            }
            else
            {
                var octetA = Source_A.AsGroup(domain.A);
                var octetB = Source_B.AsGroup(domain.B);
                res = Target.Group(
                    Lift(octetA.NegX_NegY_NegZ, octetB.NegX_NegY_NegZ),
                    Lift(octetA.PosX_NegY_NegZ, octetB.PosX_NegY_NegZ),
                    Lift(octetA.NegX_PosY_NegZ, octetB.NegX_PosY_NegZ),
                    Lift(octetA.PosX_PosY_NegZ, octetB.PosX_PosY_NegZ),
                    Lift(octetA.NegX_NegY_PosZ, octetB.NegX_NegY_PosZ),
                    Lift(octetA.PosX_NegY_PosZ, octetB.PosX_NegY_PosZ),
                    Lift(octetA.NegX_PosY_PosZ, octetB.NegX_PosY_PosZ),
                    Lift(octetA.PosX_PosY_PosZ, octetB.PosX_PosY_PosZ));
                _cache.Add(domain, res);
                return res;
            }
        }

        /// <summary>
        /// Encapsulates all the information needed to perform a lift operation to a pair of octrees.
        /// </summary>
        public struct Domain : IEquatable<Domain>
        {
            public Domain(OctreeDictionary.Octree a, OctreeDictionary.Octree b)
            {
                A = a;
                B = b;
            }

            /// <summary>
            /// The octree providing the first input for the lift function.
            /// </summary>
            public OctreeDictionary.Octree A;

            /// <summary>
            /// The octree providing the second input for the lift function.
            /// </summary>
            public OctreeDictionary.Octree B;

            public static bool operator ==(Domain a, Domain b)
            {
                return a.A == b.A && a.B == b.B;
            }

            public static bool operator !=(Domain a, Domain b)
            {
                return !(a == b);
            }

            public override bool Equals(object obj)
            {
                if (!(obj is Domain))
                    return false;
                return this == (Domain)obj;
            }

            bool IEquatable<Domain>.Equals(Domain other)
            {
                return this == other;
            }

            public override int GetHashCode()
            {
                return HashCodeHelper.Combine(A.GetHashCode(), B.GetHashCode());
            }
        }
    }

    /// <summary>
    /// A helper for implementing translation functions on octrees.
    /// </summary>
    public struct OctreeShifter<T>
    {
        private TempCache<Domain, OctreeDictionary.Octree> _cache;
        public OctreeShifter(OctreeFactory<T> octrees)
        {
            Octrees = octrees;
            _cache = new TempCache<Domain, OctreeDictionary.Octree>(64);
        }

        /// <summary>
        /// The dictionary where source and result octrees are defined.
        /// </summary>
        public OctreeFactory<T> Octrees { get; }

        /// <summary>
        /// Translates a octree by a given positive amount.
        /// </summary>
        public OctreeDictionary.Octet Shift(T env, uint depth, ShortVector3i offset, OctreeDictionary.Octree octree)
        {
            Debug.Assert(offset.X >= 0 && offset.X < (1 << (int)depth));
            Debug.Assert(offset.Y >= 0 && offset.Y < (1 << (int)depth));
            Debug.Assert(offset.Z >= 0 && offset.Z < (1 << (int)depth));
            ShortOctantList path = ShortOctantList.Pick(depth, -offset).TrimSuffix(Octant.NegX_NegY_NegZ);
            OctreeDictionary.Octree envOctree = Octrees.Leaf(env);
            OctreeDictionary.Octet envOctet = OctreeDictionary.Octet.Uniform(envOctree);
            OctreeDictionary.Octet resOctet = default;
            for (int i = 0; i < 8; i++)
            {
                Octant oct = (Octant)i;
                OctreeDictionary.Octet octet = envOctet;
                octet[oct.Flip()] = octree;
                resOctet[oct] = Unshift(path, octet);
            }

            // Correct for zero shifting on any axis
            if (offset.X == 0)
            {
                Util.Swap(ref resOctet.NegX_NegY_NegZ, ref resOctet.PosX_NegY_NegZ);
                Util.Swap(ref resOctet.NegX_PosY_NegZ, ref resOctet.PosX_PosY_NegZ);
                Util.Swap(ref resOctet.NegX_NegY_PosZ, ref resOctet.PosX_NegY_PosZ);
                Util.Swap(ref resOctet.NegX_PosY_PosZ, ref resOctet.PosX_PosY_PosZ);
            }
            if (offset.Y == 0)
            {
                Util.Swap(ref resOctet.NegX_NegY_NegZ, ref resOctet.NegX_PosY_NegZ);
                Util.Swap(ref resOctet.PosX_NegY_NegZ, ref resOctet.PosX_PosY_NegZ);
                Util.Swap(ref resOctet.NegX_NegY_PosZ, ref resOctet.NegX_PosY_PosZ);
                Util.Swap(ref resOctet.PosX_NegY_PosZ, ref resOctet.PosX_PosY_PosZ);
            }
            if (offset.Z == 0)
            {
                Util.Swap(ref resOctet.NegX_NegY_NegZ, ref resOctet.NegX_NegY_PosZ);
                Util.Swap(ref resOctet.PosX_NegY_NegZ, ref resOctet.PosX_NegY_PosZ);
                Util.Swap(ref resOctet.NegX_PosY_NegZ, ref resOctet.NegX_PosY_PosZ);
                Util.Swap(ref resOctet.PosX_PosY_NegZ, ref resOctet.PosX_PosY_PosZ);
            }
            return resOctet;
        }

        /// <summary>
        /// Gets a octree for a shifted cubical region within the given octet. This
        /// is the inverse of <see cref="Shift(T, int, Vector3i, OctreeDictionary.Octree)"/>.
        /// </summary>
        public OctreeDictionary.Octree Unshift(uint depth, ShortVector3i offset, OctreeDictionary.Octet octet)
        {
            Debug.Assert(offset.X >= 0 && offset.X < (1 << (int)depth));
            Debug.Assert(offset.Y >= 0 && offset.Y < (1 << (int)depth));
            Debug.Assert(offset.Z >= 0 && offset.Z < (1 << (int)depth));
            return Unshift(ShortOctantList.Pick(depth, offset).TrimSuffix(Octant.NegX_NegY_NegZ), octet);
        }

        /// <summary>
        /// Gets a octree for a shifted cubical region within the given octet.
        /// </summary>
        public OctreeDictionary.Octree Unshift(ShortOctantList path, OctreeDictionary.Octet octet)
        {
            if (path.IsEmpty)
            {
                // No shift to apply
                return octet.NegX_NegY_NegZ;
            }
            else if (Octrees.IsLeaf(octet.NegX_NegY_NegZ, out T leaf) &&
                octet == OctreeDictionary.Octet.Uniform(octet.NegX_NegY_NegZ))
            {
                // Leaf nodes don't need to be shifted
                return octet.NegX_NegY_NegZ;
            }
            else
            {
                // Check cache
                Domain domain = new Domain(path, octet);
                if (!_cache.TryGet(domain, out OctreeDictionary.Octree res))
                {
                    Octant octant = ShortOctantList.Unprepend(ref path);
                    var grid = OctreeDictionary.OctreeGrid_4_4_4.FromOctet(Octrees, octet);
                    int offset = (int)(_shiftOffset >> ((int)octant << 3)) & 0xFF;
                    res = Octrees.Group(
                        _unshift(path, ref grid, offset + 0),
                        _unshift(path, ref grid, offset + 1),
                        _unshift(path, ref grid, offset + 4),
                        _unshift(path, ref grid, offset + 5),
                        _unshift(path, ref grid, offset + 16),
                        _unshift(path, ref grid, offset + 17),
                        _unshift(path, ref grid, offset + 20),
                        _unshift(path, ref grid, offset + 21));
                    _cache.Add(domain, res);
                }
                return res;
            }
        }

        /// <summary>
        /// Helper function for <see cref="Unshift(ShortOctantList, OctreeDictionary.Octet)"/>.
        /// </summary>
        private OctreeDictionary.Octree _unshift(ShortOctantList path, ref OctreeDictionary.OctreeGrid_4_4_4 grid, int offset)
        {
            return Unshift(path, new OctreeDictionary.Octet(
                grid[offset + 0],
                grid[offset + 1],
                grid[offset + 4],
                grid[offset + 5],
                grid[offset + 16],
                grid[offset + 17],
                grid[offset + 20],
                grid[offset + 21]));
        }

        /// <summary>
        /// An unconventional bitwise mapping table associating <see cref="Octant"/> to indices in
        /// a <see cref="OctreeDictionary.OctreeGrid_4_4_4"/>.
        /// </summary>
        private const ulong _shiftOffset =
            (0ul << (0 << 3)) |
            (1ul << (1 << 3)) |
            (4ul << (2 << 3)) |
            (5ul << (3 << 3)) |
            (16ul << (4 << 3)) |
            (17ul << (5 << 3)) |
            (20ul << (6 << 3)) |
            (21ul << (7 << 3));

        /// <summary>
        /// Encapsulates all the information needed to unshift an octet.
        /// </summary>
        public struct Domain : IEquatable<Domain>
        {
            public ShortOctantList Path;
            public OctreeDictionary.Octet Octet;
            public Domain(ShortOctantList path, OctreeDictionary.Octet octet)
            {
                Path = path;
                Octet = octet;
            }

            public static bool operator ==(Domain a, Domain b)
            {
                return a.Octet == b.Octet && a.Path == b.Path;
            }

            public static bool operator !=(Domain a, Domain b)
            {
                return !(a == b);
            }

            public override bool Equals(object obj)
            {
                if (!(obj is Domain))
                    return false;
                return this == (Domain)obj;
            }

            bool IEquatable<Domain>.Equals(Domain other)
            {
                return this == other;
            }

            public override int GetHashCode()
            {
                return HashCodeHelper.Combine(Octet.GetHashCode(), Path.GetHashCode());
            }
        }
    }

    /// <summary>
    /// A helper for implementing sweep functions on an octree.
    /// </summary>
    public struct OctreeSweeper<T, TN, T_X, T_Y, T_Z>
    {
        private TempCache<Domain, Result> _cache;
        public OctreeSweeper(
            SweepFunc3<T, TN, T_X, T_Y, T_Z> func,
            OctreeDictionary<T> source,
            OctreeFactory<TN> target,
            WadDictionary<WadQuartet, T_X> dict_x,
            WadDictionary<WadQuartet, T_Y> dict_y,
            WadDictionary<WadQuartet, T_Z> dict_z)
        {
            Func = func;
            Source = source;
            Target = target;
            Dictionary_X = dict_x;
            Dictionary_Y = dict_y;
            Dictionary_Z = dict_z;
            _cache = new TempCache<Domain, Result>(64);
        }

        /// <summary>
        /// The sweeping function applied per-cell.
        /// </summary>
        public SweepFunc3<T, TN, T_X, T_Y, T_Z> Func { get; }

        /// <summary>
        /// The dictionary where source octrees are defined.
        /// </summary>
        public OctreeDictionary<T> Source { get; }

        /// <summary>
        /// The dictionary where target octrees are defined.
        /// </summary>
        public OctreeFactory<TN> Target { get; }

        /// <summary>
        /// The dictionary where quadtree wads holding information transmitted along the X direction are defined.
        /// </summary>
        public WadDictionary<WadQuartet, T_X> Dictionary_X { get; }

        /// <summary>
        /// The dictionary where quadtree wads holding information transmitted along the Y direction are defined.
        /// </summary>
        public WadDictionary<WadQuartet, T_Y> Dictionary_Y { get; }

        /// <summary>
        /// The dictionary where quadtree wads holding information transmitted along the Z direction are defined.
        /// </summary>
        public WadDictionary<WadQuartet, T_Z> Dictionary_Z { get; }

        /// <summary>
        /// Applies <see cref="Func"/> to a given octree while threading information along the X, Y
        /// and Z directions.
        /// </summary>
        public OctreeDictionary.Octree Sweep(uint scale,
            OctreeDictionary.Octree input,
            ref Wad x, ref Wad y, ref Wad z)
        {
            Debug.Assert(scale >= 0);
            if (scale == 0)
            {
                // Single leaf node special case
                T leaf_input = Source.AsLeaf(input);
                T_X leaf_x = Dictionary_X.AsLeaf(x);
                T_Y leaf_y = Dictionary_Y.AsLeaf(y);
                T_Z leaf_z = Dictionary_Z.AsLeaf(z);
                TN leaf_output = Func(leaf_input, ref leaf_x, ref leaf_y, ref leaf_z);
                x = Dictionary_X.Leaf(leaf_x);
                y = Dictionary_Y.Leaf(leaf_y);
                z = Dictionary_Z.Leaf(leaf_z);
                return Target.Leaf(leaf_output);
            }
            else
            {
                return _sweepGroup(scale, input, ref x, ref y, ref z);
            }
        }

        /// <summary>
        /// Implements <see cref="Sweep"/> for a scale of at least 1.
        /// </summary>
        private OctreeDictionary.Octree _sweepGroup(uint scale,
            OctreeDictionary.Octree input,
            ref Wad x, ref Wad y, ref Wad z)
        {
            Domain domain = new Domain(scale, input, x, y, z);
            if (_cache.TryGet(domain, out Result res))
            {
                x = res.X;
                y = res.Y;
                z = res.Z;
                return res.Output;
            }
            else
            {
                // Recursively sweep across octree contents
                OctreeDictionary.Octree output;
                if (scale == 1)
                {
                    // Base case for octet of leaf nodes
                    OctreeDictionary.Octet octet_in = Source.AsGroup(input);
                    Quartet<T_X> quartet_x = Dictionary_X.AsLeafQuartet(x);
                    Quartet<T_Y> quartet_y = Dictionary_Y.AsLeafQuartet(y);
                    Quartet<T_Z> quartet_z = Dictionary_Z.AsLeafQuartet(z);
                    output = Target.Group(
                        Target.Leaf(Func(Source.AsLeaf(octet_in.NegX_NegY_NegZ),
                            ref quartet_x.NegX_NegY, ref quartet_y.NegX_NegY, ref quartet_z.NegX_NegY)),
                        Target.Leaf(Func(Source.AsLeaf(octet_in.PosX_NegY_NegZ),
                            ref quartet_x.NegX_NegY, ref quartet_y.NegX_PosY, ref quartet_z.PosX_NegY)),
                        Target.Leaf(Func(Source.AsLeaf(octet_in.NegX_PosY_NegZ),
                            ref quartet_x.PosX_NegY, ref quartet_y.NegX_NegY, ref quartet_z.NegX_PosY)),
                        Target.Leaf(Func(Source.AsLeaf(octet_in.PosX_PosY_NegZ),
                            ref quartet_x.PosX_NegY, ref quartet_y.NegX_PosY, ref quartet_z.PosX_PosY)),
                        Target.Leaf(Func(Source.AsLeaf(octet_in.NegX_NegY_PosZ),
                            ref quartet_x.NegX_PosY, ref quartet_y.PosX_NegY, ref quartet_z.NegX_NegY)),
                        Target.Leaf(Func(Source.AsLeaf(octet_in.PosX_NegY_PosZ),
                            ref quartet_x.NegX_PosY, ref quartet_y.PosX_PosY, ref quartet_z.PosX_NegY)),
                        Target.Leaf(Func(Source.AsLeaf(octet_in.NegX_PosY_PosZ),
                            ref quartet_x.PosX_PosY, ref quartet_y.PosX_NegY, ref quartet_z.NegX_PosY)),
                        Target.Leaf(Func(Source.AsLeaf(octet_in.PosX_PosY_PosZ),
                            ref quartet_x.PosX_PosY, ref quartet_y.PosX_PosY, ref quartet_z.PosX_PosY)));
                    x = Dictionary_X.LeafQuartet(quartet_x);
                    y = Dictionary_Y.LeafQuartet(quartet_y);
                    z = Dictionary_Z.LeafQuartet(quartet_z);
                }
                else
                {
                    uint scale_inner = scale - 1;
                    OctreeDictionary.Octet octet = Source.AsGroup(input);
                    WadQuartet quartet_x = Dictionary_X.AsGroup(x);
                    WadQuartet quartet_y = Dictionary_Y.AsGroup(y);
                    WadQuartet quartet_z = Dictionary_Z.AsGroup(z);
                    octet.NegX_NegY_NegZ = _sweepGroup(scale_inner, octet.NegX_NegY_NegZ,
                        ref quartet_x.NegX_NegY, ref quartet_y.NegX_NegY, ref quartet_z.NegX_NegY);
                    octet.PosX_NegY_NegZ = _sweepGroup(scale_inner, octet.PosX_NegY_NegZ,
                        ref quartet_x.NegX_NegY, ref quartet_y.NegX_PosY, ref quartet_z.PosX_NegY);
                    octet.NegX_PosY_NegZ = _sweepGroup(scale_inner, octet.NegX_PosY_NegZ,
                        ref quartet_x.PosX_NegY, ref quartet_y.NegX_NegY, ref quartet_z.NegX_PosY);
                    octet.PosX_PosY_NegZ = _sweepGroup(scale_inner, octet.PosX_PosY_NegZ,
                        ref quartet_x.PosX_NegY, ref quartet_y.NegX_PosY, ref quartet_z.PosX_PosY);
                    octet.NegX_NegY_PosZ = _sweepGroup(scale_inner, octet.NegX_NegY_PosZ,
                        ref quartet_x.NegX_PosY, ref quartet_y.PosX_NegY, ref quartet_z.NegX_NegY);
                    octet.PosX_NegY_PosZ = _sweepGroup(scale_inner, octet.PosX_NegY_PosZ,
                        ref quartet_x.NegX_PosY, ref quartet_y.PosX_PosY, ref quartet_z.PosX_NegY);
                    octet.NegX_PosY_PosZ = _sweepGroup(scale_inner, octet.NegX_PosY_PosZ,
                        ref quartet_x.PosX_PosY, ref quartet_y.PosX_NegY, ref quartet_z.NegX_PosY);
                    octet.PosX_PosY_PosZ = _sweepGroup(scale_inner, octet.PosX_PosY_PosZ,
                        ref quartet_x.PosX_PosY, ref quartet_y.PosX_PosY, ref quartet_z.PosX_PosY);
                    x = Dictionary_X.Group(quartet_x);
                    y = Dictionary_Y.Group(quartet_y);
                    z = Dictionary_Z.Group(quartet_z);
                    output = Target.Group(octet);
                }

                // Add to cache and return
                _cache.Add(domain, new Result(output, x, y, z));
                return output;
            }
        }

        /// <summary>
        /// Encapsulates all the information needed to apply a sweep function to a source octree.
        /// </summary>
        public struct Domain : IEquatable<Domain>
        {
            public Domain(uint scale, OctreeDictionary.Octree input, Wad x, Wad y, Wad z)
            {
                Scale = scale;
                Input = input;
                X = x;
                Y = y;
                Z = z;
            }

            /// <summary>
            /// The scale of all wads in the input.
            /// </summary>
            public uint Scale;

            /// <summary>
            /// The octree being mapped.
            /// </summary>
            public OctreeDictionary.Octree Input;

            /// <summary>
            /// The wad containing information received from the negative X direction.
            /// </summary>
            public Wad X;

            /// <summary>
            /// The wad containing information received from the negative Y direction.
            /// </summary>
            public Wad Y;

            /// <summary>
            /// The wad containing information received from the negative Z direction.
            /// </summary>
            public Wad Z;

            public static bool operator ==(Domain a, Domain b)
            {
                return
                    a.Scale == b.Scale &&
                    a.Input == b.Input &&
                    a.X == b.X &&
                    a.Y == b.Y &&
                    a.Z == b.Z;
            }

            public static bool operator !=(Domain a, Domain b)
            {
                return !(a == b);
            }

            public override bool Equals(object obj)
            {
                if (!(obj is Domain))
                    return false;
                return this == (Domain)obj;
            }

            bool IEquatable<Domain>.Equals(Domain other)
            {
                return this == other;
            }

            public override int GetHashCode()
            {
                return HashCodeHelper.Combine(
                    Scale.GetHashCode(),
                    Input.GetHashCode(),
                    X.GetHashCode(),
                    Y.GetHashCode(),
                    Z.GetHashCode());
            }
        }

        /// <summary>
        /// Encapsulates the result of applying a sweep function to a <see cref="Domain"/>.
        /// </summary>
        public struct Result
        {
            public Result(OctreeDictionary.Octree output, Wad x, Wad y, Wad z)
            {
                Output = output;
                X = x;
                Y = y;
                Z = z;
            }

            /// <summary>
            /// The mapped octree.
            /// </summary>
            public OctreeDictionary.Octree Output;

            /// <summary>
            /// The wad containing information transmitted towards the positive X direction.
            /// </summary>
            public Wad X;

            /// <summary>
            /// The wad containing information transmitted towards the positive Y direction.
            /// </summary>
            public Wad Y;

            /// <summary>
            /// The wad containing information transmitted towards the positive z direction.
            /// </summary>
            public Wad Z;
        }
    }

    /// <summary>
    /// A helper for performing convolutions on octrees.
    /// </summary>
    public struct OctreeConvolver
    {
        private TempCache<
            OctreeLifter2<Posit3, Posit3, Posit3>.Domain,
            OctreeDictionary.Octree> _unionCache;
        private TempCache<Domain, OctreeDictionary.Octet> _cache;
        public OctreeConvolver(OctreeDictionary<bool> source, OctreeFactory<Posit3> target)
        {
            Source = source;
            Target = target;
            None = target.Leaf(Posit3.None);
            _unionCache = new TempCache<
                OctreeLifter2<Posit3, Posit3, Posit3>.Domain,
                OctreeDictionary.Octree>(64);
            _cache = new TempCache<Domain, OctreeDictionary.Octet>(256);
        }

        /// <summary>
        /// The dictionary where source nodes are defined.
        /// </summary>
        public OctreeDictionary<bool> Source { get; }

        /// <summary>
        /// The dictionary where the resulting node will be defined.
        /// </summary>
        public OctreeFactory<Posit3> Target { get; }

        /// <summary>
        /// The leaf node in <see cref="Target"/> representing <see cref="Posit3.None"/>.
        /// </summary>
        public OctreeDictionary.Octree None { get; }

        /// <summary>
        /// Encapsulates all the information needed to convolve a pair of octrees of the same scale.
        /// </summary>
        public struct Domain : IEquatable<Domain>
        {
            public Domain(uint scale, OctreeDictionary.Octree a, OctreeDictionary.Octree b)
            {
                Scale = scale;
                A = a;
                B = b;
            }

            /// <summary>
            /// The scale of both source octrees.
            /// </summary>
            public uint Scale;

            /// <summary>
            /// The first of the octrees to be convolved.
            /// </summary>
            public OctreeDictionary.Octree A;

            /// <summary>
            /// The second of the octrees to be convolved.
            /// </summary>
            public OctreeDictionary.Octree B;

            public static bool operator ==(Domain a, Domain b)
            {
                return a.Scale == b.Scale && a.A == b.A && a.B == b.B;
            }

            public static bool operator !=(Domain a, Domain b)
            {
                return !(a == b);
            }

            public override bool Equals(object obj)
            {
                if (!(obj is Domain))
                    return false;
                return this == (Domain)obj;
            }

            bool IEquatable<Domain>.Equals(Domain other)
            {
                return this == other;
            }

            public override int GetHashCode()
            {
                return HashCodeHelper.Combine(Scale.GetHashCode(), A.GetHashCode(), B.GetHashCode());
            }
        }

        /// <summary>
        /// Convolves the given octrees of equal size, returning an octree that is twice as large representing the offsets that
        /// <paramref name="a"/> may be placed at while still overlapping with <paramref name="b"/>.
        /// </summary>
        public OctreeDictionary.Octet Convolve(uint scale, OctreeDictionary.Octree a, OctreeDictionary.Octree b)
        {
            return Convolve(new Domain(scale, a, b));
        }

        /// <summary>
        /// Performs the convolution described by the given <see cref="Domain"/>.
        /// </summary>
        public OctreeDictionary.Octet Convolve(Domain domain)
        {
            if (domain.Scale == 0)
            {
                // Overlap is only possible if both leafs are non-empty
                if (Source.AsLeaf(domain.A) && Source.AsLeaf(domain.B))
                {
                    return new OctreeDictionary.Octet(
                        Target.Leaf(Posit3.Interior),
                        Target.Leaf(Posit3.Face_Y_Z | Posit3.Interior),
                        Target.Leaf(Posit3.Face_X_Z | Posit3.Interior),
                        Target.Leaf(Posit3.Face_X_Z | Posit3.Face_Y_Z | Posit3.Edge_Z | Posit3.Interior),
                        Target.Leaf(Posit3.Face_X_Y | Posit3.Interior),
                        Target.Leaf(Posit3.Face_X_Y | Posit3.Face_Y_Z | Posit3.Edge_Y | Posit3.Interior),
                        Target.Leaf(Posit3.Face_X_Y | Posit3.Face_X_Z | Posit3.Edge_X | Posit3.Interior),
                        Target.Leaf(Posit3.All));
                }
                else
                {
                    return OctreeDictionary.Octet.Uniform(None);
                }
            }
            else
            {
                // If either source node is empty, there's no overlap and we're done
                if (Source.IsLeaf(domain.A, out bool leaf_a) && !leaf_a)
                    return OctreeDictionary.Octet.Uniform(None);
                if (Source.IsLeaf(domain.B, out bool leaf_b) && !leaf_b)
                    return OctreeDictionary.Octet.Uniform(None);

                // General case
                if (_cache.TryGet(domain, out OctreeDictionary.Octet res))
                {
                    return res;
                }
                else
                {
                    // Initialize working grid
                    var lifter = _getUnionLifter();
                    var grid = OctreeDictionary.OctreeGrid_4_4_4.Uniform(lifter.Target.Leaf(Posit3.None));

                    // Convolve each of the 8 octants in one node with each of the 8 octants in the other.
                    var octet_a = Source.AsGroup(domain.A);
                    var octet_b = Source.AsGroup(domain.B);
                    _convolve(ref grid, domain.Scale - 1, octet_a, octet_b);
                    res = grid.ToOctet(Target);
                    _cache.Add(domain, res);
                    return res;
                }
            }
        }

        /// <summary>
        /// Helper for implementing <see cref="Convolve(Domain)"/>.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void _convolve(
            ref OctreeDictionary.OctreeGrid_4_4_4 grid, uint scale,
            OctreeDictionary.Octet a, OctreeDictionary.Octet b)
        {
            var lifter = _getUnionLifter();
            _convolve(ref lifter, ref grid, scale, a.NegX_NegY_NegZ, b, 21);
            _convolve(ref lifter, ref grid, scale, a.PosX_NegY_NegZ, b, 20);
            _convolve(ref lifter, ref grid, scale, a.NegX_PosY_NegZ, b, 17);
            _convolve(ref lifter, ref grid, scale, a.PosX_PosY_NegZ, b, 16);
            _convolve(ref lifter, ref grid, scale, a.NegX_NegY_PosZ, b, 5);
            _convolve(ref lifter, ref grid, scale, a.PosX_NegY_PosZ, b, 4);
            _convolve(ref lifter, ref grid, scale, a.NegX_PosY_PosZ, b, 1);
            _convolve(ref lifter, ref grid, scale, a.PosX_PosY_PosZ, b, 0);
        }

        /// <summary>
        /// Helper for implementing <see cref="Convolve(Domain)"/>.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void _convolve(
            ref OctreeLifter2<Posit3, Posit3, Posit3> lifter,
            ref OctreeDictionary.OctreeGrid_4_4_4 grid, uint scale,
            OctreeDictionary.Octree a, OctreeDictionary.Octet b, int offset)
        {
            _convolve(ref lifter, ref grid, scale, a, b.NegX_NegY_NegZ, offset + 0);
            _convolve(ref lifter, ref grid, scale, a, b.PosX_NegY_NegZ, offset + 1);
            _convolve(ref lifter, ref grid, scale, a, b.NegX_PosY_NegZ, offset + 4);
            _convolve(ref lifter, ref grid, scale, a, b.PosX_PosY_NegZ, offset + 5);
            _convolve(ref lifter, ref grid, scale, a, b.NegX_NegY_PosZ, offset + 16);
            _convolve(ref lifter, ref grid, scale, a, b.PosX_NegY_PosZ, offset + 17);
            _convolve(ref lifter, ref grid, scale, a, b.NegX_PosY_PosZ, offset + 20);
            _convolve(ref lifter, ref grid, scale, a, b.PosX_PosY_PosZ, offset + 21);
        }

        /// <summary>
        /// Helper for implementing <see cref="Convolve(Domain)"/>.
        /// </summary>
        private void _convolve(
            ref OctreeLifter2<Posit3, Posit3, Posit3> lifter,
            ref OctreeDictionary.OctreeGrid_4_4_4 grid, uint scale,
            OctreeDictionary.Octree a, OctreeDictionary.Octree b, int offset)
        {
            var octet = Convolve(scale, a, b);
            _add(ref lifter, ref grid, octet.NegX_NegY_NegZ, offset);
            _add(ref lifter, ref grid, octet.PosX_NegY_NegZ, offset + 1);
            _add(ref lifter, ref grid, octet.NegX_PosY_NegZ, offset + 4);
            _add(ref lifter, ref grid, octet.PosX_PosY_NegZ, offset + 5);
            _add(ref lifter, ref grid, octet.NegX_NegY_PosZ, offset + 16);
            _add(ref lifter, ref grid, octet.PosX_NegY_PosZ, offset + 17);
            _add(ref lifter, ref grid, octet.NegX_PosY_PosZ, offset + 20);
            _add(ref lifter, ref grid, octet.PosX_PosY_PosZ, offset + 21);
        }

        /// <summary>
        /// Helper for implementing <see cref="Convolve(Domain)"/>.
        /// </summary>
        private void _add(
            ref OctreeLifter2<Posit3, Posit3, Posit3> lifter,
            ref OctreeDictionary.OctreeGrid_4_4_4 grid, OctreeDictionary.Octree octree, int offset)
        {
            grid[offset] = lifter.Lift(octree, grid[offset]);
        }

        /// <summary>
        /// Gets the union of the given <see cref="Posit3"/> octrees in the target octree.
        /// </summary>
        public OctreeDictionary.Octree Union(OctreeDictionary.Octree a, OctreeDictionary.Octree b)
        {
            return _getUnionLifter().Lift(a, b);
        }

        /// <summary>
        /// Gets the <see cref="Lifter2{TA, TB, TN}"/> used to implement <see cref="Union"/>.
        /// </summary>
        private OctreeLifter2<Posit3, Posit3, Posit3> _getUnionLifter()
        {
            return new OctreeLifter2<Posit3, Posit3, Posit3>((a, b) => a | b, Target, Target, Target, _unionCache);
        }
    }
}