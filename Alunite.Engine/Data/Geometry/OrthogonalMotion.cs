﻿using System;
using System.Collections.Generic;

namespace Alunite.Data.Geometry
{
    /// <summary>
    /// A coordinate transformation in discrete three-dimensional space composed of a rotation and
    /// translation.
    /// </summary>
    public struct Motion3i
    {
    }

    /// <summary>
    /// A specialization of <see cref="Motion3i"/> for use with <see cref="ShortVector3i"/>.
    /// </summary>
    public struct ShortMotion3i : IEquatable<ShortMotion3i>
    {
        public ShortMotion3i(Rotation3i linear, ShortVector3i offset)
        {
            Linear = linear;
            Offset = offset;
        }

        /// <summary>
        /// The linear component of this motion.
        /// </summary>
        public Rotation3i Linear;

        /// <summary>
        /// The translation component of this motion.
        /// </summary>
        public ShortVector3i Offset;

        /// <summary>
        /// The identity motion.
        /// </summary>
        public static ShortMotion3i Identity = new ShortMotion3i(Rotation3i.Identity, ShortVector3i.Zero);

        /// <summary>
        /// Gets the inverse of this motion.
        /// </summary>
        public ShortMotion3i Inverse
        {
            get
            {
                var linearInv = Linear.Inverse;
                return new ShortMotion3i(linearInv, linearInv.Apply(-Offset));
            }
        }

        /// <summary>
        /// Applies this motion to a vector representing a position.
        /// </summary>
        public ShortVector3i Apply(ShortVector3i pos)
        {
            return Linear.Apply(pos) + Offset;
        }

        /// <summary>
        /// Composes the given motion, first applying the right, then the left.
        /// </summary>
        public static ShortMotion3i Compose(ShortMotion3i a, ShortMotion3i b)
        {
            return new ShortMotion3i(Rotation3i.Compose(a.Linear, b.Linear), a.Apply(b.Offset));
        }

        public static ShortVector3i operator *(ShortMotion3i a, ShortVector3i b)
        {
            return a.Apply(b);
        }

        public static ShortMotion3i operator *(ShortMotion3i a, ShortMotion3i b)
        {
            return Compose(a, b);
        }

        public static implicit operator ShortMotion3i(Rotation3i rot)
        {
            return new ShortMotion3i(rot, ShortVector3i.Zero);
        }

        public static implicit operator ShortMotion3i(Motion3i source)
        {
            throw new NotImplementedException();
        }

        public static bool operator ==(ShortMotion3i a, ShortMotion3i b)
        {
            return a.Linear == b.Linear && a.Offset == b.Offset;
        }

        public static bool operator !=(ShortMotion3i a, ShortMotion3i b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is ShortMotion3i))
                return false;
            return this == (ShortMotion3i)obj;
        }

        public override int GetHashCode()
        {
            return HashCodeHelper.Combine(
                Linear.GetHashCode(),
                Offset.GetHashCode());
        }

        bool IEquatable<ShortMotion3i>.Equals(ShortMotion3i other)
        {
            return this == other;
        }
    }
}
