﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Threading;

namespace Alunite.Data.Geometry
{
    /// <summary>
    /// When paired with an appropriate <see cref="WadDictionary{TInterior, TLeaf}"/>, describes the contents of
    /// an n-dimensional grid of values of a certain type. This is a node in a sparse recursive spatial data
    /// structure, such as an octree or quadtree.
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 2)]
    public struct Wad : IEquatable<Wad>
    {
        internal Wad(ushort name)
        {
            _name = name;
        }

        /// <summary>
        /// The unique name for this wad. This identifies a location in either <see cref="WadDictionary{TInterior, TLeaf}._interiors"/>
        /// (ascending from 0) or <see cref="WadDictionary{TInterior, TLeaf}._groups"/> (descending from <see cref="ushort.MaxValue"/>).
        /// </summary>
        [FieldOffset(0)] internal ushort _name;

        /// <summary>
        /// A sentinel value for <see cref="Wad"/>.
        /// </summary>
        public static Wad Invalid => new Wad(ushort.MaxValue);

        /// <summary>
        /// Indicates whether two wads of the same <see cref="WadDictionary{TInterior, TLeaf}"/> are equivalent.
        /// </summary>
        public static bool operator ==(Wad a, Wad b)
        {
            return a._name == b._name;
        }

        /// <summary>
        /// Indicates whether two wads of the same <see cref="WadDictionary{TInterior, TLeaf}"/> are not equivalent.
        /// </summary>
        public static bool operator !=(Wad a, Wad b)
        {
            return !(a == b);
        }

        /// <summary>
        /// The index of this node in <see cref="WadDictionary{TInterior, TLeaf}._leaves"/>, if applicable.
        /// </summary>
        internal int _leafIndex
        {
            get
            {
                return ushort.MaxValue - 1 - _name;
            }
        }

        /// <summary>
        /// The index of this node in <see cref="WadDictionary{TInterior, TLeaf}._groups"/>, if applicable.
        /// </summary>
        internal int _groupIndex
        {
            get
            {
                return _name;
            }
        }

        /// <summary>
        /// Constructs a wad with the given leaf index.
        /// </summary>
        internal static Wad _leaf(int leafIndex)
        {
            return new Wad((ushort)(ushort.MaxValue - 1 - leafIndex));
        }

        /// <summary>
        /// Constructs a wad with the given group index.
        /// </summary>
        internal static Wad _group(int groupIndex)
        {
            return new Wad((ushort)groupIndex);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Wad))
                return false;
            return this == (Wad)obj;
        }

        public override int GetHashCode()
        {
            return _name.GetHashCode();
        }

        bool IEquatable<Wad>.Equals(Wad other)
        {
            return this == other;
        }

        public override string ToString()
        {
            return _name.ToString();
        }
    }

    /// <summary>
    /// Combines a <see cref="Wad"/> with its size.
    /// </summary>
    public struct Wad_Size : IEquatable<Wad_Size>
    {
        public Wad_Size(Wad wad, int size)
        {
            Wad = wad;
            Size = size;
        }

        /// <summary>
        /// The wad component of this <see cref="Node_Size"/>.
        /// </summary>
        public Wad Wad;

        /// <summary>
        /// The size component of this <see cref="Node_Size"/>.
        /// </summary>
        public int Size;

        public static bool operator ==(Wad_Size a, Wad_Size b)
        {
            return a.Wad == b.Wad && a.Size == b.Size;
        }

        public static bool operator !=(Wad_Size a, Wad_Size b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            return this == (Wad_Size)obj;
        }

        bool IEquatable<Wad_Size>.Equals(Wad_Size other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return HashCodeHelper.Combine(Wad.GetHashCode(), Size.GetHashCode());
        }
    }

    /// <summary>
    /// Provides definitions for a set of <see cref="Wad"/> values.
    /// </summary>
    public sealed class WadDictionary<TGroup, TLeaf>
        where TGroup : IWadGroup, IEquatable<TGroup>, new()
    {
        public WadDictionary(IEqualityComparer<TLeaf> leafComparer, int leafCapacity, int groupCapacity)
        {
            LeafComparer = leafComparer;
            _leaves = new TLeaf[_defaultLeafCapacity];
            _groups = new TGroup[_defaultGroupCapacity];
            _hashtable = new int[Table.UpperPow2((leafCapacity + groupCapacity) * 5 / 4)]; // 80% load factor
            for (int i = 0; i < _hashtable.Length; i++)
                _hashtable[i] = _emptyBin;
        }

        public WadDictionary(IEqualityComparer<TLeaf> leafComparer)
            : this(leafComparer, _defaultLeafCapacity, _defaultGroupCapacity)
        { }

        public WadDictionary()
            : this(EqualityComparer<TLeaf>.Default)
        { }

        /// <summary>
        /// The <see cref="IEqualityComparer{T}"/> used to compare leaf values.
        /// </summary>
        public IEqualityComparer<TLeaf> LeafComparer { get; }

        /// <summary>
        /// The default capacity of <see cref="_leaves"/>, if no capacity is explicitly
        /// specified.
        /// </summary>
        private const int _defaultLeafCapacity = 5;

        /// <summary>
        /// The default capacity of <see cref="_groups"/>, if no capacity is explicitly
        /// specified.
        /// </summary>
        private const int _defaultGroupCapacity = 60;

        /// <summary>
        /// Contains definitions for leaf nodes.
        /// </summary>
        private TLeaf[] _leaves;

        /// <summary>
        /// Contains definitions for interior nodes.
        /// </summary>
        private TGroup[] _groups;

        // TODO: See if we can squeeze two entries per int to save space
        /// <summary>
        /// A hash-table for lookup of leaves and interior nodes based on content. Collisions are resolved
        /// using linear probing.
        /// </summary>
        /// <remarks>This is using <see cref="int"/> instead of <see cref="ushort"/> so entries can
        /// be set atomically. Also, so we can use a special value to identify empty bins.</remarks>
        private int[] _hashtable;

        /// <summary>
        /// Provides a count of the number of defined leaves in <see cref="_leaves"/>.
        /// </summary>
        private int _leafCount;

        /// <summary>
        /// Provides a count of the number of defined groups in <see cref="_groups"/>.
        /// </summary>
        internal int _groupCount;

        /// <summary>
        /// A special value used in <see cref="_hashtable"/> to identify an empty bin.
        /// </summary>
        private const int _emptyBin = ushort.MaxValue;

        /// <summary>
        /// Indicates whether this dictionary is able to accept new <see cref="Wad"/> definitions. This will be false
        /// for "frozen" dictionaries, and for dictionaries that have hit the maximum definition limit. Non-expandable
        /// dictionaries are effectively read-only.
        /// </summary>
        public bool IsExpandable
        {
            get
            {
                return _hashtable != null;
            }
        }

        /// <summary>
        /// Gets or constructs a <see cref="Wad"/> for a leaf node with the given value. If this dictionary is
        /// not expandable according to <see cref="IsExpandable"/>, or the maximum definition limit has been reached,
        /// this will return <see cref="Wad.Invalid"/>.
        /// </summary>
        public Wad Leaf(TLeaf value)
        {
        retry:
            // Look up the current hashtable and verify this dictionary is still expandable
            int[] hashtable = Volatile.Read(ref _hashtable);
            if (hashtable == null)
                return Wad.Invalid;

            // Try looking up the value in the hash table
            TLeaf[] leaves = Volatile.Read(ref _leaves);
            int hash = LeafComparer.GetHashCode(value);
            uint bin = hashtable.GetBinPow2(hash);
            int testName, testLeafIndex;
            while ((testName = Volatile.Read(ref hashtable[bin])) != _emptyBin)
            {
                Wad testLeaf = new Wad((ushort)testName);
                testLeafIndex = testLeaf._leafIndex;
                if (testLeafIndex < leaves.Length)
                    if (LeafComparer.Equals(leaves[testLeafIndex], value))
                        return testLeaf;
                bin = hashtable.Next(bin);
            }

            // Lock for modification
            lock (this)
            {
                // Verify that the spot in the hashtable is still open
                if (hashtable != _hashtable || hashtable[bin] != _emptyBin)
                    goto retry;

                // Insert the leaf
                leaves = _leaves;
                int index = _leafCount;
                if (index < leaves.Length)
                {
                    _leafCount = index + 1;
                    Wad wad = Wad._leaf(index);
                    leaves[index] = value;
                    Volatile.Write(ref hashtable[bin], wad._name);
                    return wad;
                }
                else
                {
                    // Not enough space, need to allocate more
                    _expandLeaves();
                    goto retry;
                }
            }
        }

        /// <summary>
        /// Gets or constructs a <see cref="Wad"/> for an interior node with the given contents. If this dictionary is
        /// not expandable according to <see cref="IsExpandable"/>, or the maximum definition limit has been reached,
        /// this will return <see cref="Wad.Invalid"/>.
        /// </summary>
        public Wad Group(TGroup group)
        {
            // If all the children are leaf nodes, the group is that leaf node
            Wad testLeaf = group.First;
            if (testLeaf._leafIndex < _leafCount)
            {
                TGroup testGroup = new TGroup();
                testGroup.Uniform = testLeaf;
                if (group.Equals(testGroup))
                    return testLeaf;
            }

        retry:
            // Look up the current hashtable and verify this dictionary is still expandable
            int[] hashtable = Volatile.Read(ref _hashtable);
            if (hashtable == null)
                return Wad.Invalid;

            // Try looking up the value in the hash table
            TGroup[] groups = Volatile.Read(ref _groups);
            int hash = group.GetHashCode();
            uint bin = hashtable.GetBinPow2(hash);
            int testName;
            while ((testName = Volatile.Read(ref hashtable[bin])) != _emptyBin)
            {
                Wad testWad = new Wad((ushort)testName);
                int testGroupIndex = testWad._groupIndex;
                if (testGroupIndex < groups.Length)
                    if (group.Equals(groups[testGroupIndex]))
                        return testWad;
                bin = hashtable.Next(bin);
            }

            // Lock for modification
            lock (this)
            {
                // Verify that the spot in the hashtable is still open
                if (hashtable != _hashtable || hashtable[bin] != _emptyBin)
                    goto retry;

                // Insert the group
                groups = _groups;
                int index = _groupCount;
                if (index < groups.Length)
                {
                    _groupCount++;
                    Wad wad = Wad._group(index);
                    groups[index] = group;
                    Volatile.Write(ref hashtable[bin], wad._name);
                    return wad;
                }
                else
                {
                    // Not enough space, need to allocate more
                    _expandGroups();
                    goto retry;
                }
            }
        }

        /// <summary>
        /// Gets the value of a leaf node, assuming it is in fact a leaf node.
        /// </summary>
        public TLeaf AsLeaf(Wad wad)
        {
            return _leaves[wad._leafIndex];
        }

        /// <summary>
        /// Determines whether the given node is a leaf node, returning its value if so.
        /// </summary>
        public bool AsLeaf(Wad wad, out TLeaf value)
        {
            int index = wad._leafIndex;
            if (index < _leaves.Length)
            {
                Debug.Assert(index < _leafCount);
                value = _leaves[index];
                return true;
            }
            else
            {
                value = default(TLeaf);
                return false;
            }
        }

        /// <summary>
        /// Interprets the given node as an interior node, getting its children. For leaf nodes, this will be the uniform
        /// group of the leaf node.
        /// </summary>
        public TGroup AsGroup(Wad wad)
        {
            int groupIndex = wad._groupIndex;
            if (groupIndex < _groups.Length)
            {
                Debug.Assert(groupIndex < _groupCount);
                return _groups[groupIndex];
            }
            else
            {
                // Leaves can still be interpreted as blocks of 8 identical nodes
                Debug.Assert(wad._leafIndex < _leafCount);
                TGroup group = new TGroup();
                group.Uniform = wad;
                return group;
            }
        }

        /// <summary>
        /// Gets the definition of the given wad as either a leaf (returning true) or a group (returning false).
        /// </summary>
        public bool Lookup(Wad wad, out TLeaf leaf, out TGroup group)
        {
            int index = wad._leafIndex;
            if (index < _leaves.Length)
            {
                Debug.Assert(index < _leafCount);
                leaf = _leaves[index];
                group = default;
                return true;
            }
            else
            {
                index = wad._groupIndex;
                Debug.Assert(index < _groupCount);
                leaf = default;
                group = _groups[index];
                return false;
            }
        }

        /// <summary>
        /// Enlarges <see cref="_leaves"/>. The dictionary should be locked for this call.
        /// </summary>
        private void _expandLeaves()
        {
            TLeaf[] nLeaves = new TLeaf[_leaves.Length * 2];
            Array.Copy(_leaves, nLeaves, _leaves.Length);
            Volatile.Write(ref _leaves, nLeaves);
            if (_leaves.Length + _groups.Length >= ushort.MaxValue)
                throw new Exception(); // TODO
            _expandHashtable();
        }

        /// <summary>
        /// Enlarges <see cref="_groups"/>. The dictionary should be locked for this call.
        /// </summary>
        private void _expandGroups()
        {
            TGroup[] nGroups = new TGroup[_groups.Length * 2];
            Array.Copy(_groups, nGroups, _groups.Length);
            Volatile.Write(ref _groups, nGroups);
            if (_leaves.Length + _groups.Length >= ushort.MaxValue)
                throw new Exception(); // TODO
            _expandHashtable();
        }

        /// <summary>
        /// Enlarges <see cref="_hashtable"/>. The dictionary should be locked for this call.
        /// </summary>
        private void _expandHashtable()
        {
            // TODO: Only ever resize hashtable by a multiplicative factor. Anything else is inefficient
            uint bin;
            int[] nHashtable = new int[Table.UpperPow2((_leaves.Length + _groups.Length) * 5 / 4)]; // 80% load factor
            for (bin = 0; bin < nHashtable.Length; bin++)
                nHashtable[bin] = _emptyBin;

            // Rehash things
            for (int i = 0; i < _leafCount; i++)
            {
                int hash = LeafComparer.GetHashCode(_leaves[i]);
                bin = nHashtable.GetBinPow2(hash);
                while (nHashtable[bin] != _emptyBin)
                    bin = nHashtable.Next(bin);
                nHashtable[bin] = Wad._leaf(i)._name;
            }
            for (int i = 0; i < _groupCount; i += 2)
            {
                int hash = _groups[i].GetHashCode();
                bin = nHashtable.GetBinPow2(hash);
                while (nHashtable[bin] != _emptyBin)
                    bin = nHashtable.Next(bin);
                nHashtable[bin] = Wad._group(i)._name;
            }
            Volatile.Write(ref _hashtable, nHashtable);
        }
    }

    /// <summary>
    /// Describes the child nodes of an interior node in the tree formed by a <see cref="Wad"/>.
    /// </summary>
    public interface IWadGroup
    {
        /// <summary>
        /// Gets a wad from an arbitrary, but consistent, position in the group.
        /// </summary>
        Wad First { get; }

        /// <summary>
        /// Sets all children of this group to be the given wad.
        /// </summary>
        Wad Uniform { set; }
    }
}
