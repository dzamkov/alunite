﻿using System;
using System.Collections.Generic;

namespace Alunite.Data.Geometry
{
    /// <summary>
    /// A bounded discrete grid in two-dimensional space where each cell is associated with a value of type
    /// <typeparamref name="T"/>.
    /// </summary>
    public interface IGrid2<T>
    {
        /// <summary>
        /// The size of this grid.
        /// </summary>
        Vector2i Size { get; }

        /// <summary>
        /// Gets the value of a cell in this grid.
        /// </summary>
        T this[Vector2i cell] { get; }
    }

    /// <summary>
    /// Associates a <see cref="IGrid2{T}"/> cell with its position.
    /// </summary>
    public struct Position2_Cell<T> : IComparable<Position2_Cell<T>>
    {
        public Vector2i Position;
        public T Cell;
        public Position2_Cell(Vector2i pos, T cell)
        {
            Position = pos;
            Cell = cell;
        }

        int IComparable<Position2_Cell<T>>.CompareTo(Position2_Cell<T> other)
        {
            return Vector2i.Compare(Position, other.Position);
        }
    }

    /// <summary>
    /// A two-dimensional grid defined by an array.
    /// </summary>
    public struct ArrayGrid2<T> : IGrid2<T>
    {
        public ArrayGrid2(Vector2i size, T[] array)
        {
            Size = size;
            Array = array;
        }

        /// <summary>
        /// The size of the grid.
        /// </summary>
        public Vector2i Size;

        /// <summary>
        /// The array defining the grid.
        /// </summary>
        public T[] Array;

        /// <summary>
        /// Creates an <see cref="ArrayGrid2{T}"/> of the given size, with all cells initialized to the default value
        /// of <typeparamref name="T"/>.
        /// </summary>
        public static ArrayGrid2<T> Create(Vector2i size)
        {
            return new ArrayGrid2<T>(size, new T[size.X * size.Y]);
        }

        /// <summary>
        /// Creates an <see cref="ArrayGrid2{T}"/> of the given size, with all cells initialized to the default value
        /// of <typeparamref name="T"/>.
        /// </summary>
        public static ArrayGrid2<T> Create(uint sizeX, uint sizeY)
        {
            return Create(new Vector2i((int)sizeX, (int)sizeY));
        }

        /// <summary>
        /// Creates a copy of the given grid.
        /// </summary>
        public static ArrayGrid2<T> Create<TGrid>(TGrid source)
            where TGrid : IGrid2<T>
        {
            Vector2i size = source.Size;
            int index = 0;
            ArrayGrid2<T> res = Create(size);
            for (int y = 0; y < size.Y; y++)
            {
                for (int x = 0; x < size.X; x++)
                {
                    res.Array[index++] = source[new Vector2i(x, y)];
                }
            }
            return res;
        }

        /// <summary>
        /// Gets or sets a cell in this grid.
        /// </summary>
        public T this[Vector2i cell]
        {
            get
            {
                return Array[cell.Y * Size.X + cell.X];
            }
            set
            {
                Array[cell.Y * Size.X + cell.X] = value;
            }
        }

        /// <summary>
        /// Gets or sets a cell in this grid.
        /// </summary>
        public T this[uint x, uint y]
        {
            get
            {
                return Array[y * Size.X + x];
            }
            set
            {
                Array[y * Size.X + x] = value;
            }
        }

        Vector2i IGrid2<T>.Size
        {
            get
            {
                return Size;
            }
        }
    }

    /// <summary>
    /// A two-dimensional grid defined by a given sampling function.
    /// </summary>
    public struct FuncGrid2<T> : IGrid2<T>
    {
        public FuncGrid2(Vector2i size, Func<Vector2i, T> func)
        {
            Size = size;
            Func = func;
        }

        /// <summary>
        /// The size of this grid.
        /// </summary>
        public Vector2i Size;

        /// <summary>
        /// The function defining this grid.
        /// </summary>
        public Func<Vector2i, T> Func;

        /// <summary>
        /// Gets the value of a cell in this grid.
        /// </summary>
        public T this[Vector2i cell]
        {
            get
            {
                return Func(cell);
            }
        }

        Vector2i IGrid2<T>.Size
        {
            get
            {
                return Size;
            }
        }
    }

    /// <summary>
    /// A two-dimensional grid defined by an quadtree.
    /// </summary>
    public struct QuadtreeGrid<T> : IGrid2<T>
    {
        public QuadtreeGrid(WadDictionary<WadQuartet, T> dict, Wad root, uint scale)
        {
            Dictionary = dict;
            Root = root;
            Scale = scale;
        }

        /// <summary>
        /// The dictionary defining <see cref="Root"/> and subtrees it references.
        /// </summary>
        public WadDictionary<WadQuartet, T> Dictionary;

        /// <summary>
        /// The root node for the quadtree in <see cref="Dictionary"/>.
        /// </summary>
        public Wad Root;

        /// <summary>
        /// The scale of <see cref="Root"/>, that is, the log base-2 of its length on any axis.
        /// </summary>
        public uint Scale;

        /// <summary>
        /// The length of this grid on any axis.
        /// </summary>
        public int Length => 1 << (int)Scale;

        /// <summary>
        /// The size of this grid.
        /// </summary>
        public Vector2i Size
        {
            get
            {
                int len = (int)Length;
                return new Vector2i(len, len);
            }
        }

        /// <summary>
        /// Gets or sets a cell in this grid.
        /// </summary>
        public T this[Vector2i cell]
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Constructs an <see cref="QuadtreeGrid{T}"/> copy of the given source grid. The size of the quadtree
        /// grid will be extended to the next higher power of two. When this happens, the extra space will be
        /// filled with <paramref name="overflow"/>
        /// </summary>
        public static QuadtreeGrid<T> Create<TGrid>(WadDictionary<WadQuartet, T> dict, T overflow, TGrid source)
            where TGrid : IGrid2<T>
        {
            Vector2i sourceSize = source.Size;
            uint size = Bitwise.UpperPow2((uint)Math.Max(sourceSize.X, sourceSize.Y));
            uint scale = Bitwise.Log2(size);
            return new QuadtreeGrid<T>(dict, dict.Build(source, (int)size, Vector2i.Zero), scale);
        }

        /// <summary>
        /// Constructs an <see cref="QuadtreeGrid{T}"/> copy of the given source grid. The size of the quadtree
        /// grid will be extended to the next higher power of two. When this happens, the extra space will be
        /// filled with <paramref name="overflow"/>
        /// </summary>
        public static QuadtreeGrid<T> Create<TGrid>(IEqualityComparer<T> comparer, T overflow, TGrid source)
            where TGrid : IGrid2<T>
        {
            return Create(new WadDictionary<WadQuartet, T>(comparer), overflow, source);
        }

        /// <summary>
        /// Gets the list of cells that meet the given criteria, in sorted order.
        /// </summary>
        public ListBuilder<Position2_Cell<TN>> Search<TN>(FuncPredicate<T, TN> pred)
        {
            var res = new ListBuilder<Position2_Cell<TN>>();
            var cache = new Dictionary<Wad_Size, _SearchCache>();
            _search(cache, res, pred, Dictionary, Root, Length, Vector2i.Zero);
            res.Sort();
            return res;
        }

        /// <summary>
        /// Appends the cells in the given wad that meet the given criteria to the given list, in sorted order.
        /// </summary>
        private static void _search<TN>(
            Dictionary<Wad_Size, _SearchCache> cache, ListBuilder<Position2_Cell<TN>> cells,
            FuncPredicate<T, TN> pred, WadDictionary<WadQuartet, T> dict,
            Wad wad, int size, Vector2i offset)
        {
            if (dict.AsLeaf(wad, out T leaf))
            {
                // Don't bother with caching for leaf nodes. It's faster to just process them directly
                if (pred(leaf, out TN cell))
                {
                    Box2i box = new Box2i(offset, offset + new Vector2i(size, size));
                    for (int y = box.Min.Y; y < box.Max.Y; y++)
                    {
                        for (int x = box.Min.X; x < box.Max.X; x++)
                        {
                            cells.Append(new Position2_Cell<TN>(new Vector2i(x, y), cell));
                        }
                    }
                }
            }
            else
            {
                // Check cache
                var key = new Wad_Size(wad, size);
                if (cache.TryGetValue(key, out _SearchCache nodeCache))
                {
                    // Reuse cells from cache
                    Vector2i delta = offset - nodeCache.Offset;
                    for (uint i = nodeCache.StartIndex; i < nodeCache.EndIndex; i++)
                    {
                        Position2_Cell<TN> source = cells[i];
                        cells.Append(new Position2_Cell<TN>(source.Position + delta, source.Cell));
                    }
                }
                else
                {
                    uint startIndex = cells.Length;

                    // Add vertices recursively from child nodes
                    // Generate vertices in approximate X, Y order to make sorting faster
                    int hsize = size >> 1;
                    var quartet = dict.AsGroup(wad);
                    _search(cache, cells, pred, dict, quartet.NegX_NegY, hsize, offset);
                    _search(cache, cells, pred, dict, quartet.PosX_NegY, hsize, offset + new Vector2i(hsize, 0));
                    _search(cache, cells, pred, dict, quartet.NegX_PosY, hsize, offset + new Vector2i(0, hsize));
                    _search(cache, cells, pred, dict, quartet.PosX_PosY, hsize, offset + new Vector2i(hsize, hsize));

                    // Add cache entry
                    cache.Add(key, new _SearchCache(offset, startIndex, cells.Length));
                }
            }
        }

        /// <summary>
        /// Caches a result of a call to <see cref="_search"/>
        /// </summary>
        private struct _SearchCache
        {
            public _SearchCache(Vector2i offset, uint startIndex, uint endIndex)
            {
                Offset = offset;
                StartIndex = startIndex;
                EndIndex = endIndex;
            }

            /// <summary>
            /// The offset that the cells in this cache were created at.
            /// </summary>
            public Vector2i Offset;

            /// <summary>
            /// The index of the first cell in the resulting search array that belongs in this cache.
            /// </summary>
            public uint StartIndex;

            /// <summary>
            /// The index of (one after) the last cell in the resulting search array that belongs in this cache.
            /// </summary>
            public uint EndIndex;
        }
    }

    /// <summary>
    /// A function which maps cells in a <see cref="IGrid2{T}"/> while threading information from/to neighboring
    /// cells in the X and Y directions. 
    /// </summary>
    /// <typeparam name="T">The type of information stored in cells prior to mapping</typeparam>
    /// <typeparam name="TN">The type of information stored in cells after mapping</typeparam>
    /// <typeparam name="T_X">The type of information threaded along the X direction</typeparam>
    /// <typeparam name="T_Y">The type of information threaded along the Y direction</typeparam>
    /// <param name="x">The information received from the neighboring cell in the negative X direction
    /// and transmitted to neighboring cell in the positive X direction</param>
    /// <param name="y">The information received from the neighboring cell in the negative Y direction
    /// and transmitted to neighboring cell in the positive Y direction</param>
    public delegate TN SweepFunc2<T, TN, T_X, T_Y>(T cell, ref T_X x, ref T_Y y);
}