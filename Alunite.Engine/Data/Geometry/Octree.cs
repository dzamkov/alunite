﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Collections;

namespace Alunite.Data.Geometry
{
    /// <summary>
    /// A tree data structure which partitions three-dimensional space and assigns individual regions to values of
    /// type <typeparamref name="TLeaf"/>.
    /// </summary>
    public struct Octree<TLeaf>
    {
        public Octree(OctreeDictionary<TLeaf> dict, OctreeDictionary.Octree id)
        {
            Debug.Assert(!id.IsInvalid);
            Dictionary = dict;
            Id = id;
        }

        /// <summary>
        /// The <see cref="OctreeDictionary{TLeaf}"/> in which this octree is defined. This may also be an "open"
        /// <see cref="OctreeFactory{TLeaf}"/>, to which anyone can add definitions as long as a lock on
        /// the factory is acquired beforehand.
        /// </summary>
        public OctreeDictionary<TLeaf> Dictionary { get; }

        /// <summary>
        /// The identifier for this octree within <see cref="Dictionary"/>.
        /// </summary>
        public OctreeDictionary.Octree Id { get; }

        /// <summary>
        /// Gets a particular child node of the octree.
        /// </summary>
        public Octree<TLeaf> GetChild(Octant octant)
        {
            return new Octree<TLeaf>(Dictionary, Dictionary.GetChild(Id, octant));
        }

        /// <summary>
        /// Gets a particular descendant of the octree.
        /// </summary>
        public Octree<TLeaf> GetDescendant(ShortOctantList path)
        {
            return new Octree<TLeaf>(Dictionary, Dictionary.GetDescendant(Id, path));
        }

        /// <summary>
        /// Gets the content of a leaf at a particular offset and depth.
        /// </summary>
        public TLeaf GetLeaf(uint depth, ShortVector3i offset)
        {
            return Dictionary.GetLeaf(Id, depth, offset);
        }

        /// <summary>
        /// Creates a <see cref="OctreeBuilder{TLeaf}"/> that is initially set to this octree. This allows a modified
        /// copy of the octree to be created.
        /// </summary>
        public OctreeBuilder<TLeaf> Edit()
        {
            OctreeFactory<TLeaf> factory = Dictionary as OctreeFactory<TLeaf>;
            if (factory != null && factory.IsExpandable)
            {
                if (Monitor.TryEnter(factory))
                    return new OctreeBuilder<TLeaf>(factory, true, Id);
            }
            factory = new OctreeFactory<TLeaf>(Dictionary.LeafComparer);
            return new OctreeBuilder<TLeaf>(factory, false, factory.Import(Dictionary, Id));
        }
    }

    /// <summary>
    /// A helper class for incrementally constructing or manipulating a <see cref="Octree{TLeaf}"/>.
    /// </summary>
    public sealed class OctreeBuilder<TLeaf> : IBuilder<Octree<TLeaf>>
    {
        // TODO: Queue modifications, and then apply them in a single pass by sorting by ShortOctantList
        // TODO: Handle factory overflow
        internal OctreeBuilder(OctreeFactory<TLeaf> factory, bool isShared, OctreeDictionary.Octree octree)
        {
            _factory = factory;
            _isShared = isShared;
            _octree = octree;
        }

        /// <summary>
        /// The factory in which the octree is built. This may be replaced if the current factory overflows.
        /// </summary>
        private OctreeFactory<TLeaf> _factory;

        /// <summary>
        /// Indicates whether <see cref="_factory"/> has content that is being shared across threads. If so, this
        /// <see cref="OctreeBuilder{TLeaf}"/> must have a lock on it to allow for exclusive write access. Otherwise,
        /// the factory is only available on one thread and thus no locking is required.
        /// </summary>
        private bool _isShared;

        /// <summary>
        /// The currently-constructed octree in <see cref="_factory"/>.
        /// </summary>
        private OctreeDictionary.Octree _octree;

        /// <summary>
        /// Gets a particular child node of the octree.
        /// </summary>
        public Octree<TLeaf> GetChild(Octant octant)
        {
            return new Octree<TLeaf>(_factory, _factory.GetChild(_octree, octant));
        }

        /// <summary>
        /// Gets a particular descendant of the octree.
        /// </summary>
        public Octree<TLeaf> GetDescendant(ShortOctantList path)
        {
            return new Octree<TLeaf>(_factory, _factory.GetDescendant(_octree, path));
        }

        /// <summary>
        /// Gets the content of a leaf at a particular offset and depth.
        /// </summary>
        public TLeaf GetLeaf(uint depth, ShortVector3i offset)
        {
            return _factory.GetLeaf(_octree, depth, offset);
        }

        /// <summary>
        /// Sets a particular child node of the octree.
        /// </summary>
        public void SetChild(Octant octant, Octree<TLeaf> value)
        {
            _factory.SetChild(ref _octree, octant, _factory.Import(value));
        }

        /// <summary>
        /// Sets a particular descendant of the octree.
        /// </summary>
        public void SetDescendant(ShortOctantList path, Octree<TLeaf> value)
        {
            _factory.SetDescendant(ref _octree, path, _factory.Import(value));
        }

        /// <summary>
        /// Sets the content of a leaf at a particular offset and depth.
        /// </summary>
        public void SetLeaf(uint depth, ShortVector3i offset, TLeaf value)
        {
            _factory.SetLeaf(ref _octree, depth, offset, value);
        }

        /// <summary>
        /// Finishes construction of the octree.
        /// </summary>
        public Octree<TLeaf> Finish()
        {
            if (_isShared) Monitor.Exit(_factory);
            return new Octree<TLeaf>(_factory, _octree);
        }
    }

    /// <summary>
    /// Provides definitions for a set of octree nodes of type <typeparamref name="T"/>, which terminate in
    /// leaves of type <typeparamref name="TLeaf"/>.
    /// </summary>
    public interface IOctreeDictionary<T, TLeaf>
    {
        /// <summary>
        /// Determines whether the given octree node is a leaf node, and if so, returns its value.
        /// </summary>
        bool IsLeaf(T octree, out TLeaf value);

        /// <summary>
        /// Determines whether the given octree node is an interior node, formed from an octet, and if so,
        /// gets that octet.
        /// </summary>
        bool IsGroup(T octree, out Octet<T> octet);
    }

    /// <summary>
    /// Allows the construction of octrees from nodes of type <typeparamref name="T"/> and leaves of type
    /// <typeparamref name="TLeaf"/>.
    /// </summary>
    public interface IOctreeFactory<T, TLeaf>
    {
        /// <summary>
        /// Defines a leaf node with the given value.
        /// </summary>
        T Leaf(TLeaf value);

        /// <summary>
        /// Defines an interior node from the given octet.
        /// </summary>
        T Group(Octet<T> octet);
    }

    /// <summary>
    /// A compact and efficient pool for octree data with leaves of type <typeparamref name="TLeaf"/>. Octrees in the
    /// dictionary are identified by <see cref="OctreeDictionary.Octree"/>.
    /// </summary>
    public class OctreeDictionary<TLeaf> : IOctreeDictionary<OctreeDictionary.Octree, TLeaf>
    {
        public OctreeDictionary(IEqualityComparer<TLeaf> leafComparer)
        {
            LeafComparer = leafComparer;
        }

        /// <summary>
        /// The <see cref="IEqualityComparer{T}"/> for leaves referenced in this dictionary.
        /// </summary>
        public IEqualityComparer<TLeaf> LeafComparer { get; }

        /// <summary>
        /// Contains definitions for leaf nodes.
        /// </summary>
        internal TLeaf[] _leaves;

        /// <summary>
        /// Contains definitions for interior nodes.
        /// </summary>
        internal OctreeDictionary.Octet[] _groups;

        /// <summary>
        /// Determines whether the given octree node is a leaf node, and if so, returns its value.
        /// </summary>
        public bool IsLeaf(OctreeDictionary.Octree octree, out TLeaf value)
        {
            var leaves = Volatile.Read(ref _leaves);
            uint leafIndex = octree._leafIndex;
            if (leafIndex < leaves.Length)
            {
                value = leaves[leafIndex];
                return true;
            }
            value = default;
            return false;
        }

        /// <summary>
        /// Determines whether the given octree node is a leaf node, and if so, gets its leaf index.
        /// </summary>
        internal bool _isLeaf(OctreeDictionary.Octree octree, out uint leafIndex)
        {
            leafIndex = octree._leafIndex;
            return leafIndex < _leaves.Length;
        }

        /// <summary>
        /// Interprets the given octree as an octet node. Note that leaf nodes are considered to be uniform octets of
        /// themselves.
        /// </summary>
        public OctreeDictionary.Octet AsGroup(OctreeDictionary.Octree octree)
        {
            var groups = Volatile.Read(ref _groups);
            uint groupIndex = octree._groupIndex;
            if (groupIndex < groups.Length)
                return groups[groupIndex];
            else
                return OctreeDictionary.Octet.Uniform(octree);
        }
        
        bool IOctreeDictionary<OctreeDictionary.Octree, TLeaf>.IsGroup(
            OctreeDictionary.Octree octree,
            out Octet<OctreeDictionary.Octree> octet)
        {
            octet = AsGroup(octree);
            return true;
        }

        /// <summary>
        /// Gets a particular child node of an octree.
        /// </summary>
        public OctreeDictionary.Octree GetChild(OctreeDictionary.Octree octree, Octant octant)
        {
            var groups = Volatile.Read(ref _groups);
            uint groupIndex = octree._groupIndex;
            if (groupIndex < groups.Length)
                return groups[groupIndex][octant];
            else
                return octree;
        }

        /// <summary>
        /// Gets a particular descendant of an octree.
        /// </summary>
        public OctreeDictionary.Octree GetDescendant(OctreeDictionary.Octree octree, ShortOctantList path)
        {
            var groups = Volatile.Read(ref _groups);
            foreach (Octant octant in path)
            {
                uint groupIndex = octree._groupIndex;
                if (groupIndex < groups.Length)
                    octree = groups[groupIndex][octant];
                else
                    return octree;
            }
            return octree;
        }

        /// <summary>
        /// Gets the content of a leaf at a particular offset and depth.
        /// </summary>
        public TLeaf GetLeaf(OctreeDictionary.Octree octree, uint depth, ShortVector3i offset)
        {
            bool isLeaf = IsLeaf(GetDescendant(octree, ShortOctantList.Pick(depth, offset)), out TLeaf value);
            Debug.Assert(isLeaf);
            return value;
        }
    }

    /// <summary>
    /// An <see cref="OctreeDictionary{TLeaf}"/> augmented with the ability to define new octrees.
    /// </summary>
    public sealed class OctreeFactory<TLeaf> : OctreeDictionary<TLeaf>,
        IOctreeFactory<OctreeDictionary.Octree, TLeaf>
    {
        public OctreeFactory(IEqualityComparer<TLeaf> leafComparer, uint leafCapacity, uint groupCapacity)
            : base(leafComparer)
        {
            _leaves = new TLeaf[leafCapacity];
            _nextLeafIndex = 0;
            _groups = new OctreeDictionary.Octet[groupCapacity];
            _nextGroupIndex = 0;
            _hashtable = new OctreeDictionary.Octree[_getHashtableSize(leafCapacity, groupCapacity)];
            for (uint i = 0; i < _hashtable.Length; i++)
                _hashtable[i] = OctreeDictionary.Octree.Invalid;
        }

        public OctreeFactory(IEqualityComparer<TLeaf> leafComparer)
            : this(leafComparer, DefaultLeafCapacity, DefaultGroupCapacity)
        { }

        public OctreeFactory()
            : this(EqualityHelper.Comparer<TLeaf>())
        { }

        /// <summary>
        /// The default capacity for leaves in a <see cref="OctreeFactory{TLeaf}"/>.
        /// </summary>
        public const uint DefaultLeafCapacity = 8;

        /// <summary>
        /// The default capacity for groups in a <see cref="OctreeFactory{TLeaf}"/>.
        /// </summary>
        public const uint DefaultGroupCapacity = 64;

        /// <summary>
        /// The index of the next unused slot in <see cref="OctreeDictionary{TLeaf}._leaves"/>.
        /// </summary>
        internal uint _nextLeafIndex;

        /// <summary>
        /// The index of the next unused slot in <see cref="OctreeDictionary{TLeaf}._groups"/>.
        /// </summary>
        internal uint _nextGroupIndex;

        /// <summary>
        /// A hashtable for lookup of octrees that are already defined in the factory.
        /// </summary>
        private OctreeDictionary.Octree[] _hashtable;
        
        /// <summary>
        /// Gets the preferred size for the hashtable of a factory that has the given leaf and group capacity.
        /// </summary>
        private static uint _getHashtableSize(uint leafCapacity, uint groupCapacity)
        {
            // Maximum of 80% load factor
            return Bitwise.UpperPow2((leafCapacity + groupCapacity) * 5 / 4);
        }

        /// <summary>
        /// Indicates whether this factory is able to accept new definitions. Once the factory has reached its
        /// maximum capacity, this will be false and all calls to <see cref="Leaf"/>, <see cref="Group"/> and any
        /// other defining operation will yield <see cref="OctreeDictionary.Octree.Invalid"/>. Note that this being
        /// true does not gurantee that the next defining operation will succeed.
        /// </summary>
        public bool IsExpandable => _hashtable != null;

        /// <summary>
        /// Defines a leaf node with the given value.
        /// </summary>
        public OctreeDictionary.Octree Leaf(TLeaf value)
        {
            // Initial capacity check
            if (!IsExpandable)
                return OctreeDictionary.Octree.Invalid;

            // Check hashtable
            uint leafIndex;
            OctreeDictionary.Octree octree;
            int hash = LeafComparer.GetHashCode(value);
            uint bin = _hashtable.GetBinPow2(hash);
            while (!(octree = _hashtable[bin]).IsInvalid)
            {
                leafIndex = octree._leafIndex;
                if (leafIndex < _leaves.Length && LeafComparer.Equals(value, _leaves[leafIndex]))
                    return octree;
                bin = _hashtable.Next(bin);
            }

            // Add new leaf
            if (_nextLeafIndex < _leaves.Length)
            {
                leafIndex = _nextLeafIndex++;
                _leaves[leafIndex] = value;
                return _hashtable[bin] = OctreeDictionary.Octree._leaf(leafIndex);
            }
            else
            {
                // TODO: Don't expand past Octree/ushort capacity limit
                TLeaf[] nLeaves = new TLeaf[_leaves.Length * 2];
                Array.Copy(_leaves, nLeaves, _leaves.Length);
                leafIndex = _nextLeafIndex++;
                nLeaves[leafIndex] = value;
                Volatile.Write(ref _leaves, nLeaves);
                octree = _hashtable[bin] = OctreeDictionary.Octree._leaf(leafIndex);
                _resizeHashtable();
                return octree;
            }
        }

        /// <summary>
        /// Defines an interior node from the given octet.
        /// </summary>
        public OctreeDictionary.Octree Group(OctreeDictionary.Octet octet)
        {
            // If all the children are leaf nodes, the group itself is that leaf node
            OctreeDictionary.Octree testLeaf = octet.NegX_NegY_NegZ;
            if (testLeaf._leafIndex < _leaves.Length)
            {
                if (octet == OctreeDictionary.Octet.Uniform(testLeaf))
                    return testLeaf;
            }
            
            // Initial capacity check
            if (!IsExpandable)
                return OctreeDictionary.Octree.Invalid;

            // Check hashtable
            uint groupIndex;
            OctreeDictionary.Octree octree;
            int hash = octet.GetHashCode();
            uint bin = _hashtable.GetBinPow2(hash);
            while (!(octree = _hashtable[bin]).IsInvalid)
            {
                groupIndex = octree._groupIndex;
                if (groupIndex < _groups.Length && octet == _groups[groupIndex])
                    return octree;
                bin = _hashtable.Next(bin);
            }

            // Add new octet
            if (_nextGroupIndex < _groups.Length)
            {
                groupIndex = _nextGroupIndex++;
                _groups[groupIndex] = octet;
                return _hashtable[bin] = OctreeDictionary.Octree._group(groupIndex);
            }
            else
            {
                // TODO: Don't expand past Octree/ushort capacity limit
                OctreeDictionary.Octet[] nGroups = new OctreeDictionary.Octet[_groups.Length * 2];
                Array.Copy(_groups, nGroups, _groups.Length);
                groupIndex = _nextGroupIndex++;
                nGroups[groupIndex] = octet;
                Volatile.Write(ref _groups, nGroups);
                octree = _hashtable[bin] = OctreeDictionary.Octree._group(groupIndex);
                _resizeHashtable();
                return octree;
            }
        }

        OctreeDictionary.Octree IOctreeFactory<OctreeDictionary.Octree, TLeaf>.Group(Octet<OctreeDictionary.Octree> octet)
        {
            return Group(octet);
        }

        /// <summary>
        /// Recreates <see cref="_hashtable"/> with the proper size as determined by
        /// <see cref="_getHashtableSize(uint, uint)"/>.
        /// </summary>
        private void _resizeHashtable()
        {
            uint nSize = _getHashtableSize((uint)_leaves.Length, (uint)_groups.Length);
            if (_hashtable.Length < nSize)
            {
                _hashtable = new OctreeDictionary.Octree[nSize];
                for (uint i = 0; i < _hashtable.Length; i++)
                    _hashtable[i] = OctreeDictionary.Octree.Invalid;
                for (uint i = 0; i < _nextLeafIndex; i++)
                {
                    uint bin = _hashtable.GetBinPow2(LeafComparer.GetHashCode(_leaves[i]));
                    while (!_hashtable[bin].IsInvalid) bin = _hashtable.Next(bin);
                    _hashtable[bin] = OctreeDictionary.Octree._leaf(i);
                }
                for (uint i = 0; i < _nextGroupIndex; i++)
                {
                    uint bin = _hashtable.GetBinPow2(_groups[i].GetHashCode());
                    while (!_hashtable[bin].IsInvalid) bin = _hashtable.Next(bin);
                    _hashtable[bin] = OctreeDictionary.Octree._group(i);
                }
            }
        }

        /// <summary>
        /// Imports the given octree into this factory.
        /// </summary>
        public OctreeDictionary.Octree Import(Octree<TLeaf> octree)
        {
            return Import(octree.Dictionary, octree.Id);
        }
        
        /// <summary>
        /// Imports the given octree into this factory.
        /// </summary>
        public OctreeDictionary.Octree Import(OctreeDictionary<TLeaf> source, OctreeDictionary.Octree octree)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Sets a particular child node of an octree.
        /// </summary>
        public void SetChild(ref OctreeDictionary.Octree octree, Octant octant, OctreeDictionary.Octree value)
        {
            var octet = AsGroup(octree);
            octet[octant] = value;
            octree = Group(octet);
        }

        /// <summary>
        /// Sets a particular descendant of an octree.
        /// </summary>
        public void SetDescendant(ref OctreeDictionary.Octree octree, ShortOctantList path, OctreeDictionary.Octree value)
        {
            if (ShortOctantList.TryUnprepend(ref path, out Octant octant))
            {
                var octet = AsGroup(octree);
                OctreeDictionary.Octree child = octet[octant];
                SetDescendant(ref child, path, value);
                octet[octant] = child;
                octree = Group(octet);
            }
            else
            {
                octree = value;
            }
        }
        
        /// <summary>
        /// Sets the content of a leaf at a particular offset and depth.
        /// </summary>
        public void SetLeaf(ref OctreeDictionary.Octree octree, uint depth, ShortVector3i offset, TLeaf value)
        {
            SetDescendant(ref octree, ShortOctantList.Pick(depth, offset), Leaf(value));
        }
    }

    /// <summary>
    /// Contains functions and types related to <see cref="IOctreeDictionary{T, TLeaf}"/> and
    /// <see cref="OctreeDictionary{TLeaf}"/>.
    /// </summary>
    public static class OctreeDictionary
    {
        /// <summary>
        /// Identifies an octree within a <see cref="OctreeDictionary{TLeaf}"/>.
        /// </summary>
        [StructLayout(LayoutKind.Explicit, Size = 2)]
        public struct Octree : IEquatable<Octree>
        {
            internal Octree(ushort name)
            {
                _name = name;
            }

            /// <summary>
            /// The unique name for this octree. This identifies a location in either
            /// <see cref="OctreeDictionary{TLeaf}._groups"/> (ascending from 0) or
            /// <see cref="OctreeDictionary{TLeaf}._leaves"/> (descending from <see cref="ushort.MaxValue"/>, exclusive).
            /// </summary>
            [FieldOffset(0)] internal ushort _name;

            /// <summary>
            /// A sentinel value for <see cref="Octree"/>.
            /// </summary>
            public static Octree Invalid => new Octree(ushort.MaxValue);

            /// <summary>
            /// Indicates whether this is <see cref="Invalid"/>.
            /// </summary>
            public bool IsInvalid => _name == ushort.MaxValue;

            /// <summary>
            /// Indicates whether two octrees of the same <see cref="OctreeDictionary{TLeaf}"/> are the same.
            /// </summary>
            public static bool operator ==(Octree a, Octree b)
            {
                return a._name == b._name;
            }

            /// <summary>
            /// Indicates whether two octrees of the same <see cref="OctreeDictionary{TLeaf}"/> are different.
            /// </summary>
            public static bool operator !=(Octree a, Octree b)
            {
                return !(a == b);
            }

            /// <summary>
            /// The index of this node in <see cref="OctreeDictionary{TLeaf}._leaves"/>, if applicable.
            /// </summary>
            internal uint _leafIndex
            {
                get
                {
                    return unchecked((uint)(ushort.MaxValue - 1 - _name));
                }
            }

            /// <summary>
            /// The index of this node in <see cref="OctreeDictionary{TLeaf}._groups"/>, if applicable.
            /// </summary>
            internal uint _groupIndex
            {
                get
                {
                    return _name;
                }
            }

            /// <summary>
            /// Constructs a octree with the given leaf index.
            /// </summary>
            internal static Octree _leaf(uint leafIndex)
            {
                return new Octree((ushort)(ushort.MaxValue - 1 - leafIndex));
            }

            /// <summary>
            /// Constructs an octree with the given group index.
            /// </summary>
            internal static Octree _group(uint groupIndex)
            {
                return new Octree((ushort)groupIndex);
            }

            public override bool Equals(object obj)
            {
                if (!(obj is Octree))
                    return false;
                return this == (Octree)obj;
            }

            public override int GetHashCode()
            {
                return _name.GetHashCode();
            }

            bool IEquatable<Octree>.Equals(Octree other)
            {
                return this == other;
            }

            public override string ToString()
            {
                return _name.ToString();
            }
        }

        /// <summary>
        /// An efficient specialization of <see cref="Octet{T}"/> for <see cref="Octree"/>s.
        /// </summary>
        [StructLayout(LayoutKind.Explicit, Size = 16)]
        public unsafe struct Octet : IEquatable<Octet>
        {
            [FieldOffset(0)] private fixed ushort _members[8];
            [FieldOffset(0)] private ulong _low;
            [FieldOffset(8)] private ulong _high;
            [FieldOffset(0)] public Octree NegX_NegY_NegZ;
            [FieldOffset(2)] public Octree PosX_NegY_NegZ;
            [FieldOffset(4)] public Octree NegX_PosY_NegZ;
            [FieldOffset(6)] public Octree PosX_PosY_NegZ;
            [FieldOffset(8)] public Octree NegX_NegY_PosZ;
            [FieldOffset(10)] public Octree PosX_NegY_PosZ;
            [FieldOffset(12)] public Octree NegX_PosY_PosZ;
            [FieldOffset(14)] public Octree PosX_PosY_PosZ;

            private Octet(ulong low, ulong high) : this()
            {
                _low = low;
                _high = high;
            }

            public Octet(Octree a, Octree b, Octree c, Octree d, Octree e, Octree f, Octree g, Octree h) : this()
            {
                NegX_NegY_NegZ = a;
                PosX_NegY_NegZ = b;
                NegX_PosY_NegZ = c;
                PosX_PosY_NegZ = d;
                NegX_NegY_PosZ = e;
                PosX_NegY_PosZ = f;
                NegX_PosY_PosZ = g;
                PosX_PosY_PosZ = h;
            }

            /// <summary>
            /// Gets or sets a value of an octant in this octet.
            /// </summary>
            public Octree this[Octant octant]
            {
                get
                {
                    return new Octree(_members[(int)octant]);
                }
                set
                {
                    _members[(int)octant] = value._name;
                }
            }

            /// <summary>
            /// Constructs an <see cref="Octet"/> with the given value for all octants.
            /// </summary>
            public static Octet Uniform(Octree octree)
            {
                ulong val = octree._name;
                val |= val << 16;
                val |= val << 32;
                return new Octet(val, val);
            }

            /// <summary>
            /// Indicates whether this octet is <see cref="Uniform"/> for some octree.
            /// </summary>
            public bool IsUniform(out Octree octree)
            {
                octree = NegX_NegY_NegZ;
                return this == Uniform(octree);
            }

            public static implicit operator Octet(Octet<Octree> octet)
            {
                return new Octet(
                    octet.NegX_NegY_NegZ, octet.PosX_NegY_NegZ,
                    octet.NegX_PosY_NegZ, octet.PosX_PosY_NegZ,
                    octet.NegX_NegY_PosZ, octet.PosX_NegY_PosZ,
                    octet.NegX_PosY_PosZ, octet.PosX_PosY_PosZ);
            }

            public static implicit operator Octet<Octree>(Octet octet)
            {
                return new Octet<Octree>(
                    octet.NegX_NegY_NegZ, octet.PosX_NegY_NegZ,
                    octet.NegX_PosY_NegZ, octet.PosX_PosY_NegZ,
                    octet.NegX_NegY_PosZ, octet.PosX_NegY_PosZ,
                    octet.NegX_PosY_PosZ, octet.PosX_PosY_PosZ);
            }

            public static bool operator ==(Octet a, Octet b)
            {
                return a._low == b._low && a._high == b._high;
            }

            public static bool operator !=(Octet a, Octet b)
            {
                return !(a == b);
            }

            public override bool Equals(object obj)
            {
                if (!(obj is Octet))
                    return false;
                return this == (Octet)obj;
            }

            bool IEquatable<Octet>.Equals(Octet other)
            {
                return this == other;
            }

            public override int GetHashCode()
            {
                return HashCodeHelper.Combine(_low.GetHashCode(), _high.GetHashCode());
            }
        }

        /// <summary>
        /// A 4x4x4 grid of <see cref="Octree"/>s.
        /// </summary>
        [StructLayout(LayoutKind.Explicit, Pack = 8, Size = 128)]
        public unsafe struct OctreeGrid_4_4_4
        {
            [FieldOffset(0)] private fixed ushort _cells[64];
            [FieldOffset(0)] private fixed ulong _rows[16];

            /// <summary>
            /// Constructs an octree grid that is initially set to have the given octree in all cells.
            /// </summary>
            public static OctreeGrid_4_4_4 Uniform(Octree octree)
            {
                ulong row = octree._name;
                row |= row << 16;
                row |= row << 32;
                OctreeGrid_4_4_4 grid = new OctreeGrid_4_4_4();
                for (int i = 0; i < 16; i++)
                    grid._rows[i] = row;
                return grid;
            }

            /// <summary>
            /// Gets or sets an octree in this grid based on its index.
            /// </summary>
            public Octree this[int index]
            {
                get
                {
                    return new Octree(_cells[index]);
                }
                set
                {
                    _cells[index] = value._name;
                }
            }

            /// <summary>
            /// Gets or sets an octree in this grid based on its cell coordinates.
            /// </summary>
            public Octree this[Vector3i cell]
            {
                get
                {
                    return this[cell.Z * 16 + cell.Y * 4 + cell.X];
                }
                set
                {
                    this[cell.Z * 16 + cell.Y * 4 + cell.X] = value;
                }
            }

            /// <summary>
            /// Converts an octet into a grid.
            /// </summary>
            public static OctreeGrid_4_4_4 FromOctet<T>(OctreeDictionary<T> dict, Octet octet)
            {
                Octet cur;
                OctreeGrid_4_4_4 res = default;
                cur = dict.AsGroup(octet.NegX_NegY_NegZ);
                res[0] = cur.NegX_NegY_NegZ;
                res[1] = cur.PosX_NegY_NegZ;
                res[4] = cur.NegX_PosY_NegZ;
                res[5] = cur.PosX_PosY_NegZ;
                res[16] = cur.NegX_NegY_PosZ;
                res[17] = cur.PosX_NegY_PosZ;
                res[20] = cur.NegX_PosY_PosZ;
                res[21] = cur.PosX_PosY_PosZ;
                cur = dict.AsGroup(octet.PosX_NegY_NegZ);
                res[2] = cur.NegX_NegY_NegZ;
                res[3] = cur.PosX_NegY_NegZ;
                res[6] = cur.NegX_PosY_NegZ;
                res[7] = cur.PosX_PosY_NegZ;
                res[18] = cur.NegX_NegY_PosZ;
                res[19] = cur.PosX_NegY_PosZ;
                res[22] = cur.NegX_PosY_PosZ;
                res[23] = cur.PosX_PosY_PosZ;
                cur = dict.AsGroup(octet.NegX_PosY_NegZ);
                res[8] = cur.NegX_NegY_NegZ;
                res[9] = cur.PosX_NegY_NegZ;
                res[12] = cur.NegX_PosY_NegZ;
                res[13] = cur.PosX_PosY_NegZ;
                res[24] = cur.NegX_NegY_PosZ;
                res[25] = cur.PosX_NegY_PosZ;
                res[28] = cur.NegX_PosY_PosZ;
                res[29] = cur.PosX_PosY_PosZ;
                cur = dict.AsGroup(octet.PosX_PosY_NegZ);
                res[10] = cur.NegX_NegY_NegZ;
                res[11] = cur.PosX_NegY_NegZ;
                res[14] = cur.NegX_PosY_NegZ;
                res[15] = cur.PosX_PosY_NegZ;
                res[26] = cur.NegX_NegY_PosZ;
                res[27] = cur.PosX_NegY_PosZ;
                res[30] = cur.NegX_PosY_PosZ;
                res[31] = cur.PosX_PosY_PosZ;
                cur = dict.AsGroup(octet.NegX_NegY_PosZ);
                res[32] = cur.NegX_NegY_NegZ;
                res[33] = cur.PosX_NegY_NegZ;
                res[36] = cur.NegX_PosY_NegZ;
                res[37] = cur.PosX_PosY_NegZ;
                res[48] = cur.NegX_NegY_PosZ;
                res[49] = cur.PosX_NegY_PosZ;
                res[52] = cur.NegX_PosY_PosZ;
                res[53] = cur.PosX_PosY_PosZ;
                cur = dict.AsGroup(octet.PosX_NegY_PosZ);
                res[34] = cur.NegX_NegY_NegZ;
                res[35] = cur.PosX_NegY_NegZ;
                res[38] = cur.NegX_PosY_NegZ;
                res[39] = cur.PosX_PosY_NegZ;
                res[50] = cur.NegX_NegY_PosZ;
                res[51] = cur.PosX_NegY_PosZ;
                res[54] = cur.NegX_PosY_PosZ;
                res[55] = cur.PosX_PosY_PosZ;
                cur = dict.AsGroup(octet.NegX_PosY_PosZ);
                res[40] = cur.NegX_NegY_NegZ;
                res[41] = cur.PosX_NegY_NegZ;
                res[44] = cur.NegX_PosY_NegZ;
                res[45] = cur.PosX_PosY_NegZ;
                res[56] = cur.NegX_NegY_PosZ;
                res[57] = cur.PosX_NegY_PosZ;
                res[60] = cur.NegX_PosY_PosZ;
                res[61] = cur.PosX_PosY_PosZ;
                cur = dict.AsGroup(octet.PosX_PosY_PosZ);
                res[42] = cur.NegX_NegY_NegZ;
                res[43] = cur.PosX_NegY_NegZ;
                res[46] = cur.NegX_PosY_NegZ;
                res[47] = cur.PosX_PosY_NegZ;
                res[58] = cur.NegX_NegY_PosZ;
                res[59] = cur.PosX_NegY_PosZ;
                res[62] = cur.NegX_PosY_PosZ;
                res[63] = cur.PosX_PosY_PosZ;
                return res;
            }

            /// <summary>
            /// Converts this grid into a single octet of the given <see cref="Octree"/>.
            /// </summary>
            public Octet ToOctet<T>(OctreeFactory<T> octrees)
            {
                return new Octet(
                    octrees.Group(this[0], this[1], this[4], this[5], this[16], this[17], this[20], this[21]),
                    octrees.Group(this[2], this[3], this[6], this[7], this[18], this[19], this[22], this[23]),
                    octrees.Group(this[8], this[9], this[12], this[13], this[24], this[25], this[28], this[29]),
                    octrees.Group(this[10], this[11], this[14], this[15], this[26], this[27], this[30], this[31]),
                    octrees.Group(this[32], this[33], this[36], this[37], this[48], this[49], this[52], this[53]),
                    octrees.Group(this[34], this[35], this[38], this[39], this[50], this[51], this[54], this[55]),
                    octrees.Group(this[40], this[41], this[44], this[45], this[56], this[57], this[60], this[61]),
                    octrees.Group(this[42], this[43], this[46], this[47], this[58], this[59], this[62], this[63]));
            }
        }

        /// <summary>
        /// Combines an <see cref="Octree"/> with information about its size.
        /// </summary>
        public struct Octree_Size : IEquatable<Octree_Size>
        {
            public Octree_Size(Octree octree, uint size)
            {
                Octree = octree;
                Size = size;
            }

            /// <summary>
            /// The <see cref="OctreeDictionary.Octree"/> component of this <see cref="Octree_Size"/>.
            /// </summary>
            public Octree Octree;

            /// <summary>
            /// The size component of this <see cref="Octree_Size"/>.
            /// </summary>
            public uint Size;

            public static bool operator ==(Octree_Size a, Octree_Size b)
            {
                return a.Octree == b.Octree && a.Size == b.Size;
            }

            public static bool operator !=(Octree_Size a, Octree_Size b)
            {
                return !(a == b);
            }

            public override bool Equals(object obj)
            {
                return this == (Octree_Size)obj;
            }

            bool IEquatable<Octree_Size>.Equals(Octree_Size other)
            {
                return this == other;
            }

            public override int GetHashCode()
            {
                return HashCodeHelper.Combine(Octree.GetHashCode(), Size.GetHashCode());
            }
        }

        /// <summary>
        /// Asserts that the given octree is a leaf and gets its value.
        /// </summary>
        public static TLeaf AsLeaf<T, TLeaf>(this IOctreeDictionary<T, TLeaf> dict, T octree)
        {
            bool isLeaf = dict.IsLeaf(octree, out TLeaf value);
            Debug.Assert(isLeaf);
            return value;
        }

        /// <summary>
        /// Gets a particular child node of an octree, assuming it exists.
        /// </summary>
        public static T GetChild<T, TLeaf>(
            this IOctreeDictionary<T, TLeaf> dict,
            T octree, Octant octant)
        {
            bool isGroup = dict.IsGroup(octree, out Octet<T> octet);
            Debug.Assert(isGroup);
            return octet[octant];
        }

        /// <summary>
        /// Gets a particular descendant of an octree, assuming it exists.
        /// </summary>
        public static T GetDescendant<T, TLeaf>(
            this IOctreeDictionary<T, TLeaf> dict,
            T octree, ShortOctantList path)
        {
            foreach (Octant octant in path)
                octree = dict.GetChild(octree, octant);
            return octree;
        }

        /// <summary>
        /// Gets the content of a leaf at a particular offset and depth.
        /// </summary>
        public static TLeaf GetLeaf<T, TLeaf>(
            this IOctreeDictionary<T, TLeaf> dict,
            T octree, uint depth, ShortVector3i offset)
        {
            bool isLeaf = dict.IsLeaf(dict.GetDescendant(octree, ShortOctantList.Pick(depth, offset)), out TLeaf value);
            Debug.Assert(isLeaf);
            return value;
        }

        /// <summary>
        /// Constructs an octree for an interior node with the given children.
        /// </summary>
        public static T Group<T, TLeaf>(
            this IOctreeFactory<T, TLeaf> dict,
            T a, T b, T c, T d, T e, T f, T g, T h)
        {
            return dict.Group(new Octet<T>(a, b, c, d, e, f, g, h));
        }

        /// <summary>
        /// Constructs an octree representing a region of the given 3D function.
        /// </summary>
        /// <param name="size">The size of the resulting octree.</param>
        /// <param name="offset">The offset at which to start sampling <paramref name="func"/></param>
        public static T Build<T, TLeaf>(
            this IOctreeFactory<T, TLeaf> dict,
            Func<Vector3i, TLeaf> func, uint size, Vector3i offset)
        {
            var grid = new FuncGrid3<TLeaf>(default, func);
            return dict.Build(grid, size, offset);
        }

        /// <summary>
        /// Constructs a node for an octree representing a region of the given grid.
        /// </summary>
        public static T Build<TGrid, T, TLeaf>(
            this IOctreeFactory<T, TLeaf> dict,
            TGrid grid, uint size, Vector3i offset)
            where TGrid : IGrid3<TLeaf>
        {
            if (size <= 1)
            {
                return dict.Leaf(grid[offset]);
            }
            else
            {
                Debug.Assert((int)(size & 0b1) == 0);
                uint hsize = size >> 1;
                Debug.Assert(hsize > 0);
                Vector3i altset = new Vector3i(offset.X + (int)hsize, offset.Y + (int)hsize, offset.Z + (int)hsize);
                return dict.Group(
                    dict.Build(grid, hsize, new Vector3i(offset.X, offset.Y, offset.Z)),
                    dict.Build(grid, hsize, new Vector3i(altset.X, offset.Y, offset.Z)),
                    dict.Build(grid, hsize, new Vector3i(offset.X, altset.Y, offset.Z)),
                    dict.Build(grid, hsize, new Vector3i(altset.X, altset.Y, offset.Z)),
                    dict.Build(grid, hsize, new Vector3i(offset.X, offset.Y, altset.Z)),
                    dict.Build(grid, hsize, new Vector3i(altset.X, offset.Y, altset.Z)),
                    dict.Build(grid, hsize, new Vector3i(offset.X, altset.Y, altset.Z)),
                    dict.Build(grid, hsize, new Vector3i(altset.X, altset.Y, altset.Z)));
            }
        }

        /// <summary>
        /// Applies a mapping function to the leaf values of a given octree.
        /// </summary>
        public static Octree Map<T, TN>(
            this OctreeFactory<TN> target,
            Func<T, TN> func, OctreeDictionary<T> source, Octree octree)
        {
            return new OctreeTransformer3<T, TN>(func, Geometry.Octet.Identity, source, target).Transform(octree);
        }

        /// <summary>
        /// Applies a multi-input mapping function to the leaf values of the given octrees.
        /// </summary>
        public static Octree Lift<TA, TB, TN>(
            this OctreeFactory<TN> target, Func<TA, TB, TN> func,
            OctreeDictionary<TA> source_a, Octree octree_a,
            OctreeDictionary<TB> source_b, Octree octree_b)
        {
            return new OctreeLifter2<TA, TB, TN>(func, source_a, source_b, target).Lift(octree_a, octree_b);
        }

        /// <summary>
        /// Computes the leaf-wise "and" of the given octrees.
        /// </summary>
        public static Octree And(
            this OctreeFactory<bool> target,
            OctreeDictionary<bool> source_a, Octree octree_a,
            OctreeDictionary<bool> source_b, Octree octree_b)
        {
            return Lift(target, (a, b) => a & b, source_a, octree_a, source_b, octree_b);
        }

        /// <summary>
        /// Computes the leaf-wise "and" of the given octrees.
        /// </summary>
        public static Octree And(
            this OctreeFactory<bool> target,
            Octree octree_a, Octree octree_b)
        {
            return And(target, target, octree_a, target, octree_b);
        }

        /// <summary>
        /// Computes the leaf-wise "or" of the given octrees.
        /// </summary>
        public static Octree Or(
            this OctreeFactory<bool> target,
            OctreeDictionary<bool> source_a, Octree octree_a,
            OctreeDictionary<bool> source_b, Octree octree_b)
        {
            return Lift(target, (a, b) => a | b, source_a, octree_a, source_b, octree_b);
        }

        /// <summary>
        /// Computes the leaf-wise "or" of the given octrees.
        /// </summary>
        public static Octree Or(
            this OctreeFactory<bool> target,
            Octree octree_a, Octree octree_b)
        {
            return Or(target, target, octree_a, target, octree_b);
        }

        /// <summary>
        /// Translates a <see cref="Octree"/> by a positive amount.
        /// </summary>
        public static Octet Shift<T>(
            this OctreeFactory<T> target,
            T env, uint scale, ShortVector3i offset, Octree octree)
        {
            return new OctreeShifter<T>(target).Shift(env, scale, offset, octree);
        }

        /// <summary>
        /// Gets a <see cref="Octree"/> for a shifted cubical region within the given <see cref="Octet"/>. This is
        /// the inverse of <see cref="Shift"/>.
        /// </summary>
        public static Octree Unshift<T>(
            this OctreeFactory<T> target,
            uint scale, ShortVector3i offset, Octet octet)
        {
            return new OctreeShifter<T>(target).Unshift(scale, offset, octet);
        }

        /// <summary>
        /// Maps the leaf cells in a source octree while threading information from/to neighboring cells along the
        /// X, Y and Z directions.
        /// </summary>
        /// <param name="scale">The log base-2 of the size of <paramref name="octree"/>, in relation to a leaf node</param>
        public static Octree Sweep< T, TN, T_X, T_Y, T_Z>(
            this OctreeFactory<TN> target,
            SweepFunc3<T, TN, T_X, T_Y, T_Z> func,
            OctreeDictionary<T> source, uint scale, Octree octree,
            WadDictionary<WadQuartet, T_X> dict_x, ref Wad x,
            WadDictionary<WadQuartet, T_Y> dict_y, ref Wad y,
            WadDictionary<WadQuartet, T_Z> dict_z, ref Wad z)
        {
            return new OctreeSweeper<T, TN, T_X, T_Y, T_Z>(func, source, target, dict_x, dict_y, dict_z)
                .Sweep(scale, octree, ref x, ref y, ref z);
        }

        /// <summary>
        /// Constructs an octree of <see cref="Posit3"/>s representing the possible offsets that <paramref name="a"/> may
        /// be placed at to overlap with <paramref name="b"/>, essentially a Minkowski difference of <paramref name="a"/>
        /// and <paramref name="b"/>.
        /// </summary>
        public static Octree Convolve(
            this OctreeFactory<Posit3> target,
            OctreeDictionary<bool> source, uint scale, Octree a, Octree b)
        {
            return target.Group(new OctreeConvolver(source, target).Convolve(scale, a, b));
        }

        /// <summary>
        /// Gets the complement of the set defined by the given octree.
        /// </summary>
        public static Octree Complement(this OctreeFactory<Posit3> octrees, Octree octree)
        {
            return octrees.Map(p => ~p, octrees, octree);
        }
    }

    /// <summary>
    /// A list of up to 16 <see cref="Octant"/>s.
    /// </summary>
    [DebuggerDisplay("Size = {Size}")]
    [DebuggerTypeProxy(typeof(EnumerableDebugView<Octant>))]
    public struct ShortOctantList : IEquatable<ShortOctantList>, IEnumerable<Octant>
    {
        private ulong _def;
        private ShortOctantList(ulong def)
        {
            _def = def;
        }

        /// <summary>
        /// The number of <see cref="Octant"/>s in this list.
        /// </summary>
        public uint Size => (uint)(_def >> 48);

        /// <summary>
        /// Gets or sets an item in this list.
        /// </summary>
        public Octant this[uint index]
        {
            get
            {
                ulong odata = (_def >> (int)(Size - index - 1)) & 0x0000000100010001;
                return (Octant)(0x7 & ((odata >> 30) | (odata >> 15) | odata));
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// The empty list.
        /// </summary>
        public static ShortOctantList Empty => default;

        /// <summary>
        /// Indicates whether this list is empty.
        /// </summary>
        public bool IsEmpty => _def == 0;

        /// <summary>
        /// Indicates whether this list is at the maximum size for a <see cref="ShortOctantList"/>. If so,
        /// <see cref="TryPush(Octant)"/> and <see cref="TryUnshift(Octant)"/> will always fail.
        /// </summary>
        public bool IsFull => Size == 16;

        /// <summary>
        /// Constructs a list of <see cref="Octant"/>s, which, when used to traverse an <see cref="Octree{TLeaf}"/>,
        /// will yield the node at the given depth and offset.
        /// </summary>
        public static ShortOctantList Pick(uint depth, ShortVector3i offset)
        {
            ulong def = (ulong)depth << 48;
            def |= (ulong)offset.Z << 32;
            def |= (ulong)offset.Y << 16;
            def |= (ushort)offset.X;
            return new ShortOctantList(def);
        }

        /// <summary>
        /// Gets the depth and offset of the node identified by this <see cref="ShortOctantList"/> within an
        /// <see cref="Octree{TLeaf}"/>. This is the inverse of <see cref="Pick(uint, ShortVector3i)"/>.
        /// </summary>
        public void Locate(out uint depth, out ShortVector3i offset)
        {
            depth = (uint)(_def >> 48);
            offset.Z = unchecked((short)(ushort)(_def >> 32));
            offset.Y = unchecked((short)(ushort)(_def >> 16));
            offset.X = unchecked((short)(ushort)_def);
        }
        
        /// <summary>
        /// The reverse of this list.
        /// </summary>
        public ShortOctantList Reverse
        {
            get
            {
                uint size = (uint)(_def >> 48);
                ulong data = _def & 0x0000FFFFFFFFFFFF;
                data = ((data & 0x0000AAAAAAAAAAAA) >> 1) | ((data & 0x0000055555555555) << 1);
                data = ((data & 0x0000CCCCCCCCCCCC) >> 2) | ((data & 0x0000033333333333) << 2);
                data = ((data & 0x0000F0F0F0F0F0F0) >> 4) | ((data & 0x00000F0F0F0F0F0F) << 4);
                data = ((data & 0x0000FF00FF00FF00) >> 8) | ((data & 0x000000FF00FF00FF) << 8);
                data >>= (int)(16 - size);
                return new ShortOctantList(((ulong)size << 48) | data);
            }
        }

        /// <summary>
        /// Removes all occurences of the given octant at the end of this list.
        /// </summary>
        public ShortOctantList TrimSuffix(Octant octant)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Constructs a new list by appending the given item onto this list. This will throw an exception if
        /// the maximum size limit for a <see cref="ShortOctantList"/> is exceeded.
        /// </summary>
        public ShortOctantList Append(Octant octant)
        {
            ShortOctantList list = this;
            if (!TryAppend(ref list, octant))
                throw new Exception("List is too large");
            return list;
        }

        /// <summary>
        /// Constructs a new list by prepending the given item to this list. This will throw an exception if
        /// the maximum size limit for a <see cref="ShortOctantList"/> is exceeded.
        /// </summary>
        public ShortOctantList Prepend(Octant octant)
        {
            ShortOctantList list = this;
            if (!TryPrepend(ref list, octant))
                throw new Exception("List is too large");
            return list;
        }

        /// <summary>
        /// Attempts to insert an octant at the end of a list. Returns false if this would exceed the
        /// allowable size limit for a <see cref="ShortOctantList"/>.
        /// </summary>
        public static bool TryAppend(ref ShortOctantList list, Octant octant)
        {
            ulong esize = list._def & 0xFFFF000000000000;
            if (esize < 0x0010000000000000)
            {
                ulong data = (list._def & 0x0000FFFFFFFFFFFF) << 1;
                ulong odata = ((ulong)(uint)octant << 30) | ((ulong)(uint)octant << 15) | ((uint)octant);
                data |= odata & 0x0000000100010001;
                esize += 0x0001000000000000;
                list._def = esize | data;
                return true;
            }
            octant = default;
            return false;
        }

        /// <summary>
        /// Attempts to retrieve the last octant from a list. Returns false if the list is empty.
        /// </summary>
        public static bool TryUnappend(ref ShortOctantList list, out Octant octant)
        {
            ulong esize = list._def & 0xFFFF000000000000;
            if (!list.IsEmpty)
            {
                ulong odata = list._def & 0x0000000100010001;
                octant = (Octant)(0x7 & ((odata >> 30) | (odata >> 15) | odata));
                esize -= 0x0001000000000000;
                list._def = esize | ((list._def & 0x0000FFFEFFFEFFFE) >> 1);
                return true;
            }
            octant = default;
            return false;
        }

        /// <summary>
        /// Attempts to insert an octant at the beginning of a list. Returns false if this would exceed the
        /// allowable size limit for a <see cref="ShortOctantList"/>.
        /// </summary>
        public static bool TryPrepend(ref ShortOctantList list, Octant octant)
        {
            uint size = list.Size;
            if (size < 16)
            {
                ulong odata = ((ulong)(uint)octant << 30) | ((ulong)(uint)octant << 15) | ((uint)octant);
                list._def |= (odata & 0x0000000100010001) << (int)size;
                list._def += 0x0001000000000000;
                return true;
            }
            octant = default;
            return false;
        }

        /// <summary>
        /// Attempts to retrieve the first octant from a list. Returns false if the list is empty.
        /// </summary>
        public static bool TryUnprepend(ref ShortOctantList list, out Octant octant)
        {
            if (!list.IsEmpty)
            {
                list._def -= 0x0001000000000000;
                int shift = (int)(list._def >> 48);
                ulong odata = (list._def >> shift) & 0x0000000100010001;
                list._def &= ~(0x0000000100010001ul << shift);
                octant = (Octant)(0x7 & ((odata >> 30) | (odata >> 15) | odata));
                return true;
            }
            octant = default;
            return false;
        }

        /// <summary>
        /// Attempts to retrieve the first octant from a list, assuming the list is not empty.
        /// </summary>
        public static Octant Unprepend(ref ShortOctantList list)
        {
            if (!TryUnprepend(ref list, out Octant octant))
                throw new Exception("List is empty");
            return octant;
        }

        /// <summary>
        /// Tries concatenating a list onto the end of the given list, returning false if the result
        /// would exceed the size limit for a <see cref="ShortOctantList"/>.
        /// </summary>
        public static bool TryConcat(ref ShortOctantList list, ShortOctantList suffix)
        {
            uint suffixSize = suffix.Size;
            if (list.Size + suffixSize <= 16)
            {
                ulong data = list._def & 0x0000FFFFFFFFFFFF;
                data <<= (int)suffixSize;
                data |= suffix._def;
                data += list._def & 0xFFFF000000000000;
                list._def = data;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Tries concatenating the given lists, returning false if the resulting list would exceed the
        /// size limit for a <see cref="ShortOctantList"/>.
        /// </summary>
        public static bool TryConcat(ShortOctantList a, ShortOctantList b, out ShortOctantList res)
        {
            res = a;
            return TryConcat(ref res, b);
        }

        /// <summary>
        /// Gets an enumerator for this list.
        /// </summary>
        public Enumerator GetEnumerator()
        {
            return new Enumerator(this);
        }

        IEnumerator<Octant> IEnumerable<Octant>.GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// An enumerator for a <see cref="RotationSet2i"/>
        /// </summary>
        public struct Enumerator : IEnumerator<Octant>
        {
            private Octant _current;
            private uint _numRem;
            private ulong _remData;
            public Enumerator(ShortOctantList source)
            {
                _current = default;
                _numRem = source.Size;
                ulong data =
                    (Bitwise.Expand4(unchecked((ushort)source._def)) << 0) |
                    (Bitwise.Expand4(unchecked((ushort)(source._def >> 16))) << 1) |
                    (Bitwise.Expand4(unchecked((ushort)(source._def >> 32))) << 2);
                _remData = data << ((int)(16 - _numRem) * 4);
            }

            /// <summary>
            /// Gets the current item for this enumerator.
            /// </summary>
            public Octant Current => _current;

            /// <summary>
            /// Advances to the next, or first, item.
            /// </summary>
            public bool MoveNext()
            {
                if (_numRem > 0)
                {
                    _current = (Octant)(_remData >> 60);
                    _numRem--;
                    _remData <<= 4;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            
            object IEnumerator.Current => Current;
            
            void IDisposable.Dispose() { }

            void IEnumerator.Reset()
            {
                throw new NotSupportedException();
            }
        }

        public static bool operator ==(ShortOctantList a, ShortOctantList b)
        {
            return a._def == b._def;
        }

        public static bool operator !=(ShortOctantList a, ShortOctantList b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is ShortOctantList))
                return false;
            return this == (ShortOctantList)obj;
        }

        bool IEquatable<ShortOctantList>.Equals(ShortOctantList other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            // TODO: Can probably be improved
            return _def.GetHashCode();
        }
    }
}
