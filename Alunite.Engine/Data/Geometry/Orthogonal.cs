﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Alunite.Data.Geometry
{
    /// <summary>
    /// Identifies a cardinal axis in two-dimensional space.
    /// </summary>
    public enum Axis2 : byte
    {
        X = 0,
        Y = 1
    }

    /// <summary>
    /// Identifies a cardinal axis in three-dimensional space.
    /// </summary>
    public enum Axis3 : byte
    {
        X = 0,
        Y = 1,
        Z = 2
    }

    /// <summary>
    /// Identifies a direction in one-dimensional space.
    /// </summary>
    public enum Dir1 : byte
    {
        Neg = 0,
        Pos = 1
    }

    /// <summary>
    /// Identifies a direction in two-dimensional space.
    /// </summary>
    public enum Dir2 : byte
    {
        NegX = 0,
        PosX = 1,
        NegY = 2,
        PosY = 3
    }

    /// <summary>
    /// Identifies a direction in three-dimensional space.
    /// </summary>
    public enum Dir3 : byte
    {
        NegX = 0,
        PosX = 1,
        NegY = 2,
        PosY = 3,
        NegZ = 4,
        PosZ = 5
    }

    /// <summary>
    /// Identifies one of the four divisions of two-dimensional space defined by the signs of the coordinates.
    /// </summary>
    public enum Quadrant : byte
    {
        NegX_NegY = 0,
        PosX_NegY = 1,
        NegX_PosY = 2,
        PosX_PosY = 3,
    }

    /// <summary>
    /// Identifies one of the eight divisions of three-dimensional space defined by the signs of the coordinates.
    /// </summary>
    public enum Octant : byte
    {
        NegX_NegY_NegZ = 0,
        PosX_NegY_NegZ = 1,
        NegX_PosY_NegZ = 2,
        PosX_PosY_NegZ = 3,
        NegX_NegY_PosZ = 4,
        PosX_NegY_PosZ = 5,
        NegX_PosY_PosZ = 6,
        PosX_PosY_PosZ = 7
    }

    /// <summary>
    /// Identifies a quadrant of a plane perpendicular to an <see cref="Axis3"/>.
    /// </summary>
    public enum AxisQuadrant : byte
    {
        NegY_NegZ = 0,
        PosY_NegZ = 1,
        NegY_PosZ = 2,
        PosY_PosZ = 3,
        NegZ_NegX = 4,
        PosZ_NegX = 5,
        NegZ_PosX = 6,
        PosZ_PosX = 7,
        NegX_NegY = 8,
        PosX_NegY = 9,
        NegX_PosY = 10,
        PosX_PosY = 11,
    }

    /// <summary>
    /// Associated each <see cref="Dir1"/> with a value of type <typeparamref name="T"/>.
    /// </summary>
    public struct Duet<T>
    {
        public T Neg;
        public T Pos;
        public Duet(T neg, T pos)
        {
            Neg = neg;
            Pos = pos;
        }

        /// <summary>
        /// Gets or sets the value for the given direction.
        /// </summary>
        public T this[Dir1 dir]
        {
            get
            {
                if (dir == Dir1.Neg) return Neg;
                else return Pos;
            }
            set
            {
                if (dir == Dir1.Neg) Neg = value;
                else Pos = value;
            }
        }

        /// <summary>
        /// Flips the components of this duet.
        /// </summary>
        public Duet<T> Flip()
        {
            return new Duet<T>(Pos, Neg);
        }

        /// <summary>
        /// Gets an <see cref="IEqualityComparer{T}"/> for this type given a comparer for a direction.
        /// </summary>
        public static IEqualityComparer<Duet<T>> GetComparer(IEqualityComparer<T> dir)
        {
            return new _Comparer(dir);
        }

        /// <summary>
        /// Implements an <see cref="IEqualityComparer{T}"/> for quartets.
        /// </summary>
        private sealed class _Comparer : IEqualityComparer<Duet<T>>
        {
            public _Comparer(IEqualityComparer<T> dir)
            {
                Direction = dir;
            }

            /// <summary>
            /// The <see cref="IEqualityComparer{T}"/> for a direction of the duet.
            /// </summary>
            public IEqualityComparer<T> Direction { get; }

            bool IEqualityComparer<Duet<T>>.Equals(Duet<T> x, Duet<T> y)
            {
                return
                    Direction.Equals(x.Neg, y.Neg) &&
                    Direction.Equals(x.Pos, y.Pos);
            }

            int IEqualityComparer<Duet<T>>.GetHashCode(Duet<T> obj)
            {
                return HashCodeHelper.Combine(
                    Direction.GetHashCode(obj.Neg),
                    Direction.GetHashCode(obj.Pos));
            }
        }

        public static Duet<T> operator *(Reflection1 rof, Duet<T> duet)
        {
            return rof.Apply(duet);
        }
    }

    /// <summary>
    /// Associates each <see cref="Quadrant"/> with a value of type <typeparamref name="T"/>.
    /// </summary>
    public struct Quartet<T>
    {
        public T NegX_NegY;
        public T PosX_NegY;
        public T NegX_PosY;
        public T PosX_PosY;
        public Quartet(T a, T b, T c, T d)
        {
            NegX_NegY = a;
            PosX_NegY = b;
            NegX_PosY = c;
            PosX_PosY = d;
        }

        /// <summary>
        /// Constructs a <see cref="Quartet{T}"/> with the given value for all quadrants.
        /// </summary>
        public static Quartet<T> Uniform(T value)
        {
            return new Quartet<T>(value, value, value, value);
        }

        /// <summary>
        /// Gets or sets the value for the given quadrant.
        /// </summary>
        public T this[Quadrant quadrant]
        {
            get
            {
                switch (quadrant)
                {
                    case Quadrant.NegX_NegY: return NegX_NegY;
                    case Quadrant.PosX_NegY: return PosX_NegY;
                    case Quadrant.NegX_PosY: return NegX_PosY;
                    case Quadrant.PosX_PosY: return PosX_PosY;
                    default: throw new ArgumentException();
                }
            }
            set
            {
                switch (quadrant)
                {
                    case Quadrant.NegX_NegY: NegX_NegY = value; break;
                    case Quadrant.PosX_NegY: PosX_NegY = value; break;
                    case Quadrant.NegX_PosY: NegX_PosY = value; break;
                    case Quadrant.PosX_PosY: PosX_PosY = value; break;
                    default: throw new ArgumentException();
                }
            }
        }

        /// <summary>
        /// Swaps the X and Y coordinates for this quartet.
        /// </summary>
        public Quartet<T> Swap()
        {
            return new Quartet<T>(NegX_NegY, NegX_PosY, PosX_NegY, PosX_PosY);
        }

        /// <summary>
        /// Gets an <see cref="IEqualityComparer{T}"/> for this type given a comparer for an quadrant.
        /// </summary>
        public static IEqualityComparer<Quartet<T>> GetComparer(IEqualityComparer<T> quad)
        {
            return new _Comparer(quad);
        }

        /// <summary>
        /// Implements an <see cref="IEqualityComparer{T}"/> for quartets.
        /// </summary>
        private sealed class _Comparer : IEqualityComparer<Quartet<T>>
        {
            public _Comparer(IEqualityComparer<T> quad)
            {
                Quadrant = quad;
            }

            /// <summary>
            /// The <see cref="IEqualityComparer{T}"/> for octants of the octet.
            /// </summary>
            public IEqualityComparer<T> Quadrant { get; }

            bool IEqualityComparer<Quartet<T>>.Equals(Quartet<T> x, Quartet<T> y)
            {
                return
                    Quadrant.Equals(x.NegX_NegY, y.NegX_NegY) &&
                    Quadrant.Equals(x.PosX_NegY, y.PosX_NegY) &&
                    Quadrant.Equals(x.NegX_PosY, y.NegX_PosY) &&
                    Quadrant.Equals(x.PosX_PosY, y.PosX_PosY);
            }

            int IEqualityComparer<Quartet<T>>.GetHashCode(Quartet<T> obj)
            {
                return HashCodeHelper.Combine(
                    Quadrant.GetHashCode(obj.NegX_NegY),
                    Quadrant.GetHashCode(obj.PosX_NegY),
                    Quadrant.GetHashCode(obj.NegX_PosY),
                    Quadrant.GetHashCode(obj.PosX_PosY));
            }
        }

        public static Quartet<T> operator *(Roflection2i rof, Quartet<T> quartet)
        {
            return rof.Apply(quartet);
        }
    }

    /// <summary>
    /// Contains functions related to <see cref="Quartet{T}"/>.
    /// </summary>
    public static class Quartet
    {
        /// <summary>
        /// An <see cref="Quartet"/> where each quadrant contains itself.
        /// </summary>
        public static Quartet<Quadrant> Identity => new Quartet<Quadrant>(
            Quadrant.NegX_NegY,
            Quadrant.PosX_NegY,
            Quadrant.NegX_PosY,
            Quadrant.PosX_PosY);

        /// <summary>
        /// Constructs an <see cref="Quartet{T}"/> with the given value for all quadrants.
        /// </summary>
        public static Quartet<T> Uniform<T>(T value)
        {
            return new Quartet<T>(value, value, value, value);
        }
    }

    /// <summary>
    /// Associates each <see cref="Octant"/> with a value of type <typeparamref name="T"/>.
    /// </summary>
    public struct Octet<T> : IEquatable<Octet<T>>
    {
        public T NegX_NegY_NegZ;
        public T PosX_NegY_NegZ;
        public T NegX_PosY_NegZ;
        public T PosX_PosY_NegZ;
        public T NegX_NegY_PosZ;
        public T PosX_NegY_PosZ;
        public T NegX_PosY_PosZ;
        public T PosX_PosY_PosZ;
        public Octet(T a, T b, T c, T d, T e, T f, T g, T h)
        {
            NegX_NegY_NegZ = a;
            PosX_NegY_NegZ = b;
            NegX_PosY_NegZ = c;
            PosX_PosY_NegZ = d;
            NegX_NegY_PosZ = e;
            PosX_NegY_PosZ = f;
            NegX_PosY_PosZ = g;
            PosX_PosY_PosZ = h;
        }

        /// <summary>
        /// Gets or sets the value for the given octant.
        /// </summary>
        public T this[Octant octant]
        {
            get
            {
                switch (octant)
                {
                    case Octant.NegX_NegY_NegZ: return NegX_NegY_NegZ;
                    case Octant.PosX_NegY_NegZ: return PosX_NegY_NegZ;
                    case Octant.NegX_PosY_NegZ: return NegX_PosY_NegZ;
                    case Octant.PosX_PosY_NegZ: return PosX_PosY_NegZ;
                    case Octant.NegX_NegY_PosZ: return NegX_NegY_PosZ;
                    case Octant.PosX_NegY_PosZ: return PosX_NegY_PosZ;
                    case Octant.NegX_PosY_PosZ: return NegX_PosY_PosZ;
                    case Octant.PosX_PosY_PosZ: return PosX_PosY_PosZ;
                    default: throw new ArgumentException();
                }
            }
            set
            {
                switch (octant)
                {
                    case Octant.NegX_NegY_NegZ: NegX_NegY_NegZ = value; break;
                    case Octant.PosX_NegY_NegZ: PosX_NegY_NegZ = value; break;
                    case Octant.NegX_PosY_NegZ: NegX_PosY_NegZ = value; break;
                    case Octant.PosX_PosY_NegZ: PosX_PosY_NegZ = value; break;
                    case Octant.NegX_NegY_PosZ: NegX_NegY_PosZ = value; break;
                    case Octant.PosX_NegY_PosZ: PosX_NegY_PosZ = value; break;
                    case Octant.NegX_PosY_PosZ: NegX_PosY_PosZ = value; break;
                    case Octant.PosX_PosY_PosZ: PosX_PosY_PosZ = value; break;
                    default: throw new ArgumentException();
                }
            }
        }

        /// <summary>
        /// Replaces all octants for which the given <see cref="BoolOctet"/> mask is true.
        /// </summary>
        public void Replace(BoolOctet mask, T value)
        {
            if (mask[Octant.NegX_NegY_NegZ])
                NegX_NegY_NegZ = value;
            if (mask[Octant.PosX_NegY_NegZ])
                PosX_NegY_NegZ = value;
            if (mask[Octant.NegX_PosY_NegZ])
                NegX_PosY_NegZ = value;
            if (mask[Octant.PosX_PosY_NegZ])
                PosX_PosY_NegZ = value;
            if (mask[Octant.NegX_NegY_PosZ])
                NegX_NegY_PosZ = value;
            if (mask[Octant.PosX_NegY_PosZ])
                PosX_NegY_PosZ = value;
            if (mask[Octant.NegX_PosY_PosZ])
                NegX_PosY_PosZ = value;
            if (mask[Octant.PosX_PosY_PosZ])
                PosX_PosY_PosZ = value;
        }

        /// <summary>
        /// Applies a mapping function to all components of this octet.
        /// </summary>
        public Octet<TA> Map<TA>(Func<T, TA> func)
        {
            return new Octet<TA>(
                func(NegX_NegY_NegZ),
                func(PosX_NegY_NegZ),
                func(NegX_PosY_NegZ),
                func(PosX_PosY_NegZ),
                func(NegX_NegY_PosZ),
                func(PosX_NegY_PosZ),
                func(NegX_PosY_PosZ),
                func(PosX_PosY_PosZ));
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Quartet<T> NegX_Y_Z
        {
            get
            {
                return new Quartet<T>(NegX_NegY_NegZ, NegX_PosY_NegZ, NegX_NegY_PosZ, NegX_PosY_PosZ);
            }
            set
            {
                NegX_NegY_NegZ = value.NegX_NegY;
                NegX_PosY_NegZ = value.PosX_NegY;
                NegX_NegY_PosZ = value.NegX_PosY;
                NegX_PosY_PosZ = value.PosX_PosY;
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Quartet<T> PosX_Y_Z
        {
            get
            {
                return new Quartet<T>(PosX_NegY_NegZ, PosX_PosY_NegZ, PosX_NegY_PosZ, PosX_PosY_PosZ);
            }
            set
            {
                PosX_NegY_NegZ = value.NegX_NegY;
                PosX_PosY_NegZ = value.PosX_NegY;
                PosX_NegY_PosZ = value.NegX_PosY;
                PosX_PosY_PosZ = value.PosX_PosY;
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Quartet<T> NegY_Z_X
        {
            get
            {
                return new Quartet<T>(NegX_NegY_NegZ, NegX_NegY_PosZ, PosX_NegY_NegZ, PosX_NegY_PosZ);
            }
            set
            {
                NegX_NegY_NegZ = value.NegX_NegY;
                NegX_NegY_PosZ = value.PosX_NegY;
                PosX_NegY_NegZ = value.NegX_PosY;
                PosX_NegY_PosZ = value.PosX_PosY;
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Quartet<T> PosY_Z_X
        {
            get
            {
                return new Quartet<T>(NegX_PosY_NegZ, NegX_PosY_PosZ, PosX_PosY_NegZ, PosX_PosY_PosZ);
            }
            set
            {
                NegX_PosY_NegZ = value.NegX_NegY;
                NegX_PosY_PosZ = value.PosX_NegY;
                PosX_PosY_NegZ = value.NegX_PosY;
                PosX_PosY_PosZ = value.PosX_PosY;
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Quartet<T> NegZ_X_Y
        {
            get
            {
                return new Quartet<T>(NegX_NegY_NegZ, PosX_NegY_NegZ, NegX_PosY_NegZ, PosX_PosY_NegZ);
            }
            set
            {
                NegX_NegY_NegZ = value.NegX_NegY;
                PosX_NegY_NegZ = value.PosX_NegY;
                NegX_PosY_NegZ = value.NegX_PosY;
                PosX_PosY_NegZ = value.PosX_PosY;
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Quartet<T> PosZ_X_Y
        {
            get
            {
                return new Quartet<T>(NegX_NegY_PosZ, PosX_NegY_PosZ, NegX_PosY_PosZ, PosX_PosY_PosZ);
            }
            set
            {
                NegX_NegY_PosZ = value.NegX_NegY;
                PosX_NegY_PosZ = value.PosX_NegY;
                NegX_PosY_PosZ = value.NegX_PosY;
                PosX_PosY_PosZ = value.PosX_PosY;
            }
        }

        public static Octet<T> operator *(Roflection3i rof, Octet<T> octet)
        {
            return rof.Apply(octet);
        }

        public static bool operator ==(Octet<T> x, Octet<T> y)
        {
            return
                EqualityHelper.Equals(x.NegX_NegY_NegZ, y.NegX_NegY_NegZ) &&
                EqualityHelper.Equals(x.PosX_NegY_NegZ, y.PosX_NegY_NegZ) &&
                EqualityHelper.Equals(x.NegX_PosY_NegZ, y.NegX_PosY_NegZ) &&
                EqualityHelper.Equals(x.PosX_PosY_NegZ, y.PosX_PosY_NegZ) &&
                EqualityHelper.Equals(x.NegX_NegY_PosZ, y.NegX_NegY_PosZ) &&
                EqualityHelper.Equals(x.PosX_NegY_PosZ, y.PosX_NegY_PosZ) &&
                EqualityHelper.Equals(x.NegX_PosY_PosZ, y.NegX_PosY_PosZ) &&
                EqualityHelper.Equals(x.PosX_PosY_PosZ, y.PosX_PosY_PosZ);
        }

        public static bool operator !=(Octet<T> x, Octet<T> y)
        {
            return !(x == y);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Octet<T>))
                return false;
            return this == (Octet<T>)obj;
        }

        bool IEquatable<Octet<T>>.Equals(Octet<T> other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return HashCodeHelper.Combine(
                EqualityHelper.GetHashCode(NegX_NegY_NegZ),
                EqualityHelper.GetHashCode(PosX_NegY_NegZ),
                EqualityHelper.GetHashCode(NegX_PosY_NegZ),
                EqualityHelper.GetHashCode(PosX_PosY_NegZ),
                EqualityHelper.GetHashCode(NegX_NegY_PosZ),
                EqualityHelper.GetHashCode(PosX_NegY_PosZ),
                EqualityHelper.GetHashCode(NegX_PosY_PosZ),
                EqualityHelper.GetHashCode(PosX_PosY_PosZ));
        }
    }

    /// <summary>
    /// Contains functions related to <see cref="Octet{T}"/>.
    /// </summary>
    public static class Octet
    {
        /// <summary>
        /// An <see cref="Octet"/> where each octant contains itself.
        /// </summary>
        public static Octet<Octant> Identity => new Octet<Octant>(
            Octant.NegX_NegY_NegZ,
            Octant.PosX_NegY_NegZ,
            Octant.NegX_PosY_NegZ,
            Octant.PosX_PosY_NegZ,
            Octant.NegX_NegY_PosZ,
            Octant.PosX_NegY_PosZ,
            Octant.NegX_PosY_PosZ,
            Octant.PosX_PosY_PosZ);

        /// <summary>
        /// Constructs an <see cref="Octet{T}"/> with the given value for all octants.
        /// </summary>
        public static Octet<T> Uniform<T>(T value)
        {
            return new Octet<T>(value, value, value, value, value, value, value, value);
        }
    }
    
    /// <summary>
    /// Associates every <see cref="Axis2"/> with a <see cref="Duet{T}"/>.
    /// </summary>
    public struct AxialDuet2<T>
    {
        public T NegX;
        public T PosX;
        public T NegY;
        public T PosY;
        public AxialDuet2(T negX, T posX, T negY, T posY)
        {
            NegX = negX;
            PosX = posX;
            NegY = negY;
            PosY = posY;
        }

        public AxialDuet2(Duet<T> x, Duet<T> y)
        {
            NegX = x.Neg;
            PosX = x.Pos;
            NegY = y.Neg;
            PosY = y.Pos;
        }

        /// <summary>
        /// Gets or sets a component of this <see cref="AxialDuet2{T}"/>.
        /// </summary>
        public Duet<T> this[Axis2 axis]
        {
            get
            {
                if (axis == Axis2.X) return X;
                else return Y;
            }
            set
            {
                if (axis == Axis2.X) X = value;
                else Y = value;
            }
        }

        /// <summary>
        /// Gets or sets a component of this <see cref="AxialDuet2{T}"/>
        /// </summary>
        public T this[Dir2 dir]
        {
            get
            {
                switch (dir)
                {
                    case Dir2.NegX: return NegX;
                    case Dir2.PosX: return PosX;
                    case Dir2.NegY: return NegY;
                    case Dir2.PosY: return PosY;
                    default: throw new ArgumentException();
                }
            }
            set
            {
                switch (dir)
                {
                    case Dir2.NegX: NegX = value; break;
                    case Dir2.PosX: PosX = value; break;
                    case Dir2.NegY: NegY = value; break;
                    case Dir2.PosY: PosY = value; break;
                    default: throw new ArgumentException();
                }
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Duet<T> X
        {
            get
            {
                return new Duet<T>(NegX, PosX);
            }
            set
            {
                NegX = value.Neg;
                PosX = value.Pos;
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Duet<T> Y
        {
            get
            {
                return new Duet<T>(NegY, PosY);
            }
            set
            {
                NegY = value.Neg;
                PosY = value.Pos;
            }
        }

        /// <summary>
        /// Gets an <see cref="IEqualityComparer{T}"/> for this type given a comparer for a direction.
        /// </summary>
        public static IEqualityComparer<AxialDuet2<T>> GetComparer(IEqualityComparer<T> dir)
        {
            return new _Comparer(dir);
        }

        /// <summary>
        /// Implements an <see cref="IEqualityComparer{T}"/> for axis quartets.
        /// </summary>
        private sealed class _Comparer : IEqualityComparer<AxialDuet2<T>>
        {
            public _Comparer(IEqualityComparer<T> dir)
            {
                Direction = dir;
            }

            /// <summary>
            /// The <see cref="IEqualityComparer{T}"/> for directions of the axis duet.
            /// </summary>
            public IEqualityComparer<T> Direction { get; }

            bool IEqualityComparer<AxialDuet2<T>>.Equals(AxialDuet2<T> a, AxialDuet2<T> b)
            {
                return
                    Direction.Equals(a.NegX, b.NegX) &&
                    Direction.Equals(a.PosX, b.PosX) &&
                    Direction.Equals(a.NegY, b.NegY) &&
                    Direction.Equals(a.PosY, b.PosY);
            }

            int IEqualityComparer<AxialDuet2<T>>.GetHashCode(AxialDuet2<T> obj)
            {
                return HashCodeHelper.Combine(
                    Direction.GetHashCode(obj.NegX),
                    Direction.GetHashCode(obj.PosX),
                    Direction.GetHashCode(obj.NegY),
                    Direction.GetHashCode(obj.PosY));
            }
        }
    }

    /// <summary>
    /// Contains functions related to <see cref="AxialDuet2{T}"/>.
    /// </summary>
    public static class AxialDuet2
    {
        /// <summary>
        /// An <see cref="AxialDuet2"/> where each direction contains itself.
        /// </summary>
        public static AxialDuet2<Dir2> Identity => new AxialDuet2<Dir2>(
            Dir2.NegX, Dir2.PosX,
            Dir2.NegY, Dir2.PosY);
        
        /// <summary>
        /// Constructs an <see cref="AxialDuet2{T}"/> with the given value for all directions.
        /// </summary>
        public static AxialDuet2<T> Uniform<T>(T value)
        {
            return new AxialDuet2<T>(value, value, value, value);
        }
    }

    /// <summary>
    /// Associates every <see cref="Axis3"/> with a value of type <typeparamref name="T"/>.
    /// </summary>
    public struct Axial3<T>
    {
        public T X;
        public T Y;
        public T Z;
        public Axial3(T x, T y, T z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        /// <summary>
        /// Gets or sets a component of this <see cref="AxialDuet3{T}"/>.
        /// </summary>
        public T this[Axis3 axis]
        {
            get
            {
                if (axis == Axis3.X) return X;
                else if (axis == Axis3.Y) return Y;
                else return Z;
            }
            set
            {
                if (axis == Axis3.X) X = value;
                else if (axis == Axis3.Y) Y = value;
                else Z = value;
            }
        }
    }

    /// <summary>
    /// Contains helper functions related to <see cref="Axial3{T}"/>.
    /// </summary>
    public struct Axial3
    {
        /// <summary>
        /// Constructs an <see cref="Axial3{T}"/> with the given value for all axes.
        /// </summary>
        public static Axial3<T> Uniform<T>(T value)
        {
            return new Axial3<T>(value, value, value);
        }
    }

    /// <summary>
    /// Associates every <see cref="Axis3"/> with a <see cref="Duet{T}"/>. Alternatively, this can be interpreted
    /// as associating every <see cref="Dir3"/> with a <typeparamref name="T"/>.
    /// </summary>
    public struct AxialDuet3<T>
    {
        public T NegX;
        public T PosX;
        public T NegY;
        public T PosY;
        public T NegZ;
        public T PosZ;
        public AxialDuet3(Duet<T> x, Duet<T> y, Duet<T> z)
        {
            NegX = x.Neg;
            PosX = x.Pos;
            NegY = y.Neg;
            PosY = y.Pos;
            NegZ = z.Neg;
            PosZ = z.Pos;
        }

        public AxialDuet3(T negX, T posX, T negY, T posY, T negZ, T posZ)
        {
            NegX = negX;
            PosX = posX;
            NegY = negY;
            PosY = posY;
            NegZ = negZ;
            PosZ = posZ;
        }

        /// <summary>
        /// Constructs an <see cref="AxialDuet3{T}"/> with the same value for all components.
        /// </summary>
        public static AxialDuet3<T> Uniform(T value)
        {
            return new AxialDuet3<T>(value, value, value, value, value, value);
        }

        /// <summary>
        /// Gets or sets a component of this <see cref="AxialDuet3{T}"/>.
        /// </summary>
        public Duet<T> this[Axis3 axis]
        {
            get
            {
                if (axis == Axis3.X) return X;
                else if (axis == Axis3.Y) return Y;
                else return Z;
            }
            set
            {
                if (axis == Axis3.X) X = value;
                else if (axis == Axis3.Y) Y = value;
                else Z = value;
            }
        }

        /// <summary>
        /// Gets or sets a component of this <see cref="AxialDuet3{T}"/>
        /// </summary>
        public T this[Dir3 dir]
        {
            get
            {
                switch (dir)
                {
                    case Dir3.NegX: return NegX;
                    case Dir3.PosX: return PosX;
                    case Dir3.NegY: return NegY;
                    case Dir3.PosY: return PosY;
                    case Dir3.NegZ: return NegZ;
                    case Dir3.PosZ: return PosZ;
                    default: throw new ArgumentException();
                }
            }
            set
            {
                switch (dir)
                {
                    case Dir3.NegX: NegX = value; break;
                    case Dir3.PosX: PosX = value; break;
                    case Dir3.NegY: NegY = value; break;
                    case Dir3.PosY: PosY = value; break;
                    case Dir3.NegZ: NegZ = value; break;
                    case Dir3.PosZ: PosZ = value; break;
                    default: throw new ArgumentException();
                }
            }
        }

        /// <summary>
        /// The component of this <see cref="AxialDuet3{T}"/> on the XY plane
        /// </summary>
        public AxialDuet2<T> X_Y
        {
            get
            {
                return new AxialDuet2<T>(X, Y);
            }
            set
            {
                X = value.X;
                Y = value.Y;
            }
        }

        /// <summary>
        /// The component of this <see cref="AxialDuet3{T}"/> on the XZ plane
        /// </summary>
        public AxialDuet2<T> X_Z
        {
            get
            {
                return new AxialDuet2<T>(X, Z);
            }
            set
            {
                X = value.X;
                Z = value.Y;
            }
        }

        /// <summary>
        /// The component of this <see cref="AxialDuet3{T}"/> on the YZ plane
        /// </summary>
        public AxialDuet2<T> Y_Z
        {
            get
            {
                return new AxialDuet2<T>(Y, Z);
            }
            set
            {
                Y = value.X;
                Z = value.Y;
            }
        }
        
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Duet<T> X
        {
            get
            {
                return new Duet<T>(NegX, PosX);
            }
            set
            {
                NegX = value.Neg;
                PosX = value.Pos;
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Duet<T> Y
        {
            get
            {
                return new Duet<T>(NegY, PosY);
            }
            set
            {
                NegY = value.Neg;
                PosY = value.Pos;
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Duet<T> Z
        {
            get
            {
                return new Duet<T>(NegZ, PosZ);
            }
            set
            {
                NegZ = value.Neg;
                PosZ = value.Pos;
            }
        }

        /// <summary>
        /// Gets an <see cref="IEqualityComparer{T}"/> for this type given a comparer for a direction.
        /// </summary>
        public static IEqualityComparer<AxialDuet3<T>> GetComparer(IEqualityComparer<T> dir)
        {
            return new _Comparer(dir);
        }

        /// <summary>
        /// Implements an <see cref="IEqualityComparer{T}"/> for axis quartets.
        /// </summary>
        private sealed class _Comparer : IEqualityComparer<AxialDuet3<T>>
        {
            public _Comparer(IEqualityComparer<T> dir)
            {
                Direction = dir;
            }

            /// <summary>
            /// The <see cref="IEqualityComparer{T}"/> for directions of the axis duet.
            /// </summary>
            public IEqualityComparer<T> Direction { get; }

            bool IEqualityComparer<AxialDuet3<T>>.Equals(AxialDuet3<T> a, AxialDuet3<T> b)
            {
                return
                    Direction.Equals(a.NegX, b.NegX) &&
                    Direction.Equals(a.PosX, b.PosX) &&
                    Direction.Equals(a.NegY, b.NegY) &&
                    Direction.Equals(a.PosY, b.PosY) &&
                    Direction.Equals(a.NegZ, b.NegZ) &&
                    Direction.Equals(a.PosZ, b.PosZ);
            }

            int IEqualityComparer<AxialDuet3<T>>.GetHashCode(AxialDuet3<T> obj)
            {
                return HashCodeHelper.Combine(
                    Direction.GetHashCode(obj.NegX),
                    Direction.GetHashCode(obj.PosX),
                    Direction.GetHashCode(obj.NegY),
                    Direction.GetHashCode(obj.PosY),
                    Direction.GetHashCode(obj.NegZ),
                    Direction.GetHashCode(obj.PosZ));
            }
        }

        public static AxialDuet3<T> operator *(Roflection3i rot, AxialDuet3<T> axisDuet)
        {
            return rot.Apply(axisDuet);
        }
    }

    /// <summary>
    /// Contains functions related to <see cref="AxialDuet3{T}"/>.
    /// </summary>
    public static class AxialDuet3
    {
        /// <summary>
        /// An <see cref="AxialDuet3"/> where each direction contains itself.
        /// </summary>
        public static AxialDuet3<Dir3> Identity => new AxialDuet3<Dir3>(
            Dir3.NegX, Dir3.PosX,
            Dir3.NegY, Dir3.PosY,
            Dir3.NegZ, Dir3.PosZ);

        /// <summary>
        /// Constructs an <see cref="AxialDuet3{T}"/> with the given value for all directions.
        /// </summary>
        public static AxialDuet3<T> Uniform<T>(T value)
        {
            return new AxialDuet3<T>(value, value, value, value, value, value);
        }
    }

    /// <summary>
    /// Associates a <see cref="Quartet{T}"/> for every orthogonal plane in three-dimensional spaces. This is commonly
    /// used to describe the faces surronding a point.
    /// </summary>
    public struct AxialQuartet<T>
    {
        public T NegY_NegZ;
        public T PosY_NegZ;
        public T NegY_PosZ;
        public T PosY_PosZ;
        public T NegZ_NegX;
        public T PosZ_NegX;
        public T NegZ_PosX;
        public T PosZ_PosX;
        public T NegX_NegY;
        public T PosX_NegY;
        public T NegX_PosY;
        public T PosX_PosY;
        public AxialQuartet(
            T negY_negZ, T posY_negZ, T negY_posZ, T posY_posZ,
            T negZ_negX, T posZ_negX, T negZ_posX, T posZ_posX,
            T negX_negY, T posX_negY, T negX_posY, T posX_posY)
        {
            NegY_NegZ = negY_negZ;
            PosY_NegZ = posY_negZ;
            NegY_PosZ = negY_posZ;
            PosY_PosZ = posY_posZ;
            NegZ_NegX = negZ_negX;
            PosZ_NegX = posZ_negX;
            NegZ_PosX = negZ_posX;
            PosZ_PosX = posZ_posX;
            NegX_NegY = negX_negY;
            PosX_NegY = posX_negY;
            NegX_PosY = negX_posY;
            PosX_PosY = posX_posY;
        }

        public AxialQuartet(Quartet<T> y_z, Quartet<T> z_x, Quartet<T> x_y)
        {
            NegY_NegZ = y_z.NegX_NegY;
            PosY_NegZ = y_z.PosX_NegY;
            NegY_PosZ = y_z.NegX_PosY;
            PosY_PosZ = y_z.PosX_PosY;
            NegZ_NegX = z_x.NegX_NegY;
            PosZ_NegX = z_x.PosX_NegY;
            NegZ_PosX = z_x.NegX_PosY;
            PosZ_PosX = z_x.PosX_PosY;
            NegX_NegY = x_y.NegX_NegY;
            PosX_NegY = x_y.PosX_NegY;
            NegX_PosY = x_y.NegX_PosY;
            PosX_PosY = x_y.PosX_PosY;
        }

        /// <summary>
        /// Builds an <see cref="AxialQuartet{T}"/> for the faces in an <see cref="Octet{T}"/>, given a function
        /// which gets the face between two arbitrary octants.
        /// </summary>
        public static AxialQuartet<T> FromFaces<TA>(Func<Duet<TA>, T> face, Octet<TA> octet)
        {
            AxialQuartet<T> res = new AxialQuartet<T>();

            res.NegY_NegZ = face(new Duet<TA>(octet.NegX_NegY_NegZ, octet.PosX_NegY_NegZ));
            res.PosY_NegZ = face(new Duet<TA>(octet.NegX_PosY_NegZ, octet.PosX_PosY_NegZ));
            res.NegY_PosZ = face(new Duet<TA>(octet.NegX_NegY_PosZ, octet.PosX_NegY_PosZ));
            res.PosY_PosZ = face(new Duet<TA>(octet.NegX_PosY_PosZ, octet.PosX_PosY_PosZ));
            res.NegZ_NegX = face(new Duet<TA>(octet.NegX_NegY_NegZ, octet.NegX_PosY_NegZ));
            res.PosZ_NegX = face(new Duet<TA>(octet.NegX_NegY_PosZ, octet.NegX_PosY_PosZ));
            res.NegZ_PosX = face(new Duet<TA>(octet.PosX_NegY_NegZ, octet.PosX_PosY_NegZ));
            res.PosZ_PosX = face(new Duet<TA>(octet.PosX_NegY_PosZ, octet.PosX_PosY_PosZ));
            res.NegX_NegY = face(new Duet<TA>(octet.NegX_NegY_NegZ, octet.NegX_NegY_PosZ));
            res.PosX_NegY = face(new Duet<TA>(octet.PosX_NegY_NegZ, octet.PosX_NegY_PosZ));
            res.NegX_PosY = face(new Duet<TA>(octet.NegX_PosY_NegZ, octet.NegX_PosY_PosZ));
            res.PosX_PosY = face(new Duet<TA>(octet.PosX_PosY_NegZ, octet.PosX_PosY_PosZ));
            return res;
        }

        /// <summary>
        /// Gets or sets the value for the given component.
        /// </summary>
        public T this[AxisQuadrant axisQuad]
        {
            get
            {
                switch (axisQuad)
                {
                    case AxisQuadrant.NegY_NegZ: return NegY_NegZ;
                    case AxisQuadrant.PosY_NegZ: return PosY_NegZ;
                    case AxisQuadrant.NegY_PosZ: return NegY_PosZ;
                    case AxisQuadrant.PosY_PosZ: return PosY_PosZ;
                    case AxisQuadrant.NegZ_NegX: return NegZ_NegX;
                    case AxisQuadrant.PosZ_NegX: return PosZ_NegX;
                    case AxisQuadrant.NegZ_PosX: return NegZ_PosX;
                    case AxisQuadrant.PosZ_PosX: return PosZ_PosX;
                    case AxisQuadrant.NegX_NegY: return NegX_NegY;
                    case AxisQuadrant.PosX_NegY: return PosX_NegY;
                    case AxisQuadrant.NegX_PosY: return NegX_PosY;
                    case AxisQuadrant.PosX_PosY: return PosX_PosY;
                    default: throw new ArgumentException();
                }
            }
            set
            {
                switch (axisQuad)
                {

                    case AxisQuadrant.NegY_NegZ: NegY_NegZ = value; break;
                    case AxisQuadrant.PosY_NegZ: PosY_NegZ = value; break;
                    case AxisQuadrant.NegY_PosZ: NegY_PosZ = value; break;
                    case AxisQuadrant.PosY_PosZ: PosY_PosZ = value; break;
                    case AxisQuadrant.NegZ_NegX: NegZ_NegX = value; break;
                    case AxisQuadrant.PosZ_NegX: PosZ_NegX = value; break;
                    case AxisQuadrant.NegZ_PosX: NegZ_PosX = value; break;
                    case AxisQuadrant.PosZ_PosX: PosZ_PosX = value; break;
                    case AxisQuadrant.NegX_NegY: NegX_NegY = value; break;
                    case AxisQuadrant.PosX_NegY: PosX_NegY = value; break;
                    case AxisQuadrant.NegX_PosY: NegX_PosY = value; break;
                    case AxisQuadrant.PosX_PosY: PosX_PosY = value; break;
                    default: throw new ArgumentException();
                }
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Quartet<T> Y_Z
        {
            get
            {
                return new Quartet<T>(NegY_NegZ, PosY_NegZ, NegY_PosZ, PosY_PosZ);
            }
            set
            {
                NegY_NegZ = value.NegX_NegY;
                PosY_NegZ = value.PosX_NegY;
                NegY_PosZ = value.NegX_PosY;
                PosY_PosZ = value.PosX_PosY;
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Quartet<T> Z_X
        {
            get
            {
                return new Quartet<T>(NegZ_NegX, PosZ_NegX, NegZ_PosX, PosZ_PosX);
            }
            set
            {
                NegZ_NegX = value.NegX_NegY;
                PosZ_NegX = value.PosX_NegY;
                NegZ_PosX = value.NegX_PosY;
                PosZ_PosX = value.PosX_PosY;
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Quartet<T> X_Y
        {
            get
            {
                return new Quartet<T>(NegX_NegY, PosX_NegY, NegX_PosY, PosX_PosY);
            }
            set
            {
                NegX_NegY = value.NegX_NegY;
                PosX_NegY = value.PosX_NegY;
                NegX_PosY = value.NegX_PosY;
                PosX_PosY = value.PosX_PosY;
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public AxialDuet2<T> NegX_Y_Z
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get
            {
                return new AxialDuet2<T>(NegX_NegY, NegX_PosY, NegZ_NegX, PosZ_NegX);
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            set
            {
                NegX_NegY = value.NegX;
                NegX_PosY = value.PosX;
                NegZ_NegX = value.NegY;
                PosZ_NegX = value.PosY;
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public AxialDuet2<T> PosX_Y_Z
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get
            {
                return new AxialDuet2<T>(PosX_NegY, PosX_PosY, NegZ_PosX, PosZ_PosX);
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            set
            {
                PosX_NegY = value.NegX;
                PosX_PosY = value.PosX;
                NegZ_PosX = value.NegY;
                PosZ_PosX = value.PosY;
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public AxialDuet2<T> NegY_Z_X
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get
            {
                return new AxialDuet2<T>(NegY_NegZ, NegY_PosZ, NegX_NegY, PosX_NegY);
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            set
            {
                NegY_NegZ = value.NegX;
                NegY_PosZ = value.PosX;
                NegX_NegY = value.NegY;
                PosX_NegY = value.PosY;
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public AxialDuet2<T> PosY_Z_X
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get
            {
                return new AxialDuet2<T>(PosY_NegZ, PosY_PosZ, NegX_PosY, PosX_PosY);
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            set
            {
                PosY_NegZ = value.NegX;
                PosY_PosZ = value.PosX;
                NegX_PosY = value.NegY;
                PosX_PosY = value.PosY;
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public AxialDuet2<T> NegZ_X_Y
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get
            {
                return new AxialDuet2<T>(NegZ_NegX, NegZ_PosX, NegY_NegZ, PosY_NegZ);
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            set
            {
                NegZ_NegX = value.NegX;
                NegZ_PosX = value.PosX;
                NegY_NegZ = value.NegY;
                PosY_NegZ = value.PosY;
            }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public AxialDuet2<T> PosZ_X_Y
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get
            {
                return new AxialDuet2<T>(PosZ_NegX, PosZ_PosX, NegY_PosZ, PosY_PosZ);
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            set
            {
                PosZ_NegX = value.NegX;
                PosZ_PosX = value.PosX;
                NegY_PosZ = value.NegY;
                PosY_PosZ = value.PosY;
            }
        }

        /// <summary>
        /// Gets an <see cref="IEqualityComparer{T}"/> for this type given a comparer for an quadrant.
        /// </summary>
        public static IEqualityComparer<AxialQuartet<T>> GetComparer(IEqualityComparer<T> quadrant)
        {
            return new _Comparer(quadrant);
        }

        /// <summary>
        /// Implements an <see cref="IEqualityComparer{T}"/> for axis quartets.
        /// </summary>
        private sealed class _Comparer : IEqualityComparer<AxialQuartet<T>>
        {
            public _Comparer(IEqualityComparer<T> quadrant)
            {
                Quadrant = quadrant;
            }

            /// <summary>
            /// The <see cref="IEqualityComparer{T}"/> for quadrants of the axis quartet.
            /// </summary>
            public IEqualityComparer<T> Quadrant { get; }

            bool IEqualityComparer<AxialQuartet<T>>.Equals(AxialQuartet<T> x, AxialQuartet<T> y)
            {
                return
                    Quadrant.Equals(x.NegY_NegZ, y.NegY_NegZ) &&
                    Quadrant.Equals(x.PosY_NegZ, y.PosY_NegZ) &&
                    Quadrant.Equals(x.NegY_PosZ, y.NegY_PosZ) &&
                    Quadrant.Equals(x.PosY_PosZ, y.PosY_PosZ) &&
                    Quadrant.Equals(x.NegZ_NegX, y.NegZ_NegX) &&
                    Quadrant.Equals(x.PosZ_NegX, y.PosZ_NegX) &&
                    Quadrant.Equals(x.NegZ_PosX, y.NegZ_PosX) &&
                    Quadrant.Equals(x.PosZ_PosX, y.PosZ_PosX) &&
                    Quadrant.Equals(x.NegX_NegY, y.NegX_NegY) &&
                    Quadrant.Equals(x.PosX_NegY, y.PosX_NegY) &&
                    Quadrant.Equals(x.NegX_PosY, y.NegX_PosY) &&
                    Quadrant.Equals(x.PosX_PosY, y.PosX_PosY);
            }

            int IEqualityComparer<AxialQuartet<T>>.GetHashCode(AxialQuartet<T> obj)
            {
                return HashCodeHelper.Combine(
                    HashCodeHelper.Combine(
                        Quadrant.GetHashCode(obj.NegY_NegZ),
                        Quadrant.GetHashCode(obj.PosY_NegZ),
                        Quadrant.GetHashCode(obj.NegY_PosZ),
                        Quadrant.GetHashCode(obj.PosY_PosZ)),
                    HashCodeHelper.Combine(
                        Quadrant.GetHashCode(obj.NegZ_NegX),
                        Quadrant.GetHashCode(obj.PosZ_NegX),
                        Quadrant.GetHashCode(obj.NegZ_PosX),
                        Quadrant.GetHashCode(obj.PosZ_PosX)),
                    HashCodeHelper.Combine(
                        Quadrant.GetHashCode(obj.NegX_NegY),
                        Quadrant.GetHashCode(obj.PosX_NegY),
                        Quadrant.GetHashCode(obj.NegX_PosY),
                        Quadrant.GetHashCode(obj.PosX_PosY)));
            }
        }
    }

    /// <summary>
    /// Contains functions related to <see cref="AxialQuartet{T}"/>.
    /// </summary>
    public static class AxialQuartet
    {
        /// <summary>
        /// An <see cref="AxialQuartet"/> where each quadrant contains itself.
        /// </summary>
        public static AxialQuartet<AxisQuadrant> Identity => new AxialQuartet<AxisQuadrant>(
            AxisQuadrant.NegY_NegZ,
            AxisQuadrant.PosY_NegZ,
            AxisQuadrant.NegY_PosZ,
            AxisQuadrant.PosY_PosZ,
            AxisQuadrant.NegZ_NegX,
            AxisQuadrant.PosZ_NegX,
            AxisQuadrant.NegZ_PosX,
            AxisQuadrant.PosZ_PosX,
            AxisQuadrant.NegX_NegY,
            AxisQuadrant.PosX_NegY,
            AxisQuadrant.NegX_PosY,
            AxisQuadrant.PosX_PosY);

        /// <summary>
        /// Constructs an <see cref="AxialQuartet{T}"/> with the given value for all <see cref="AxisQuadrant"/>s.
        /// </summary>
        public static AxialQuartet<T> Uniform<T>(T value)
        {
            return new AxialQuartet<T>(
                Quartet.Uniform(value),
                Quartet.Uniform(value),
                Quartet.Uniform(value));
        }
    }

    /// <summary>
    /// Describes a set of points in the cubic region ([0, 1), [0, 1), [0, 1)) as a union of 8 broad regions.
    /// </summary>
    public struct Posit3 : IEquatable<Posit3>
    {
        public Posit3(byte bitmap)
        {
            Bitmap = bitmap;
        }

        /// <summary>
        /// The bitmap specifying which regions are included in this <see cref="Posit3"/>.
        /// </summary>
        public byte Bitmap;

        /// <summary>
        /// Indicates whether the posit includes the region ((0, 1), (0, 1), (0, 1))
        /// </summary>
        public bool Region_Interior
        {
            get { return (Bitmap & 0b10000000) != 0; }
            set { Bitmap &= 0b01111111; if (value) Bitmap |= 0b10000000; }
        }

        /// <summary>
        /// Indicates whether the posit includes the region ((0, 1), (0, 1), 0)
        /// </summary>
        public bool Region_Face_X_Y
        {
            get { return (Bitmap & 0b00001000) != 0; }
            set { Bitmap &= 0b11110111; if (value) Bitmap |= 0b00001000; }
        }

        /// <summary>
        /// Indicates whether the posit includes the region ((0, 1), 0, (0, 1))
        /// </summary>
        public bool Region_Face_X_Z
        {
            get { return (Bitmap & 0b00100000) != 0; }
            set { Bitmap &= 0b11011111; if (value) Bitmap |= 0b00100000; }
        }

        /// <summary>
        /// Indicates whether the posit includes the region (0, (0, 1), (0, 1))
        /// </summary>
        public bool Region_Face_Y_Z
        {
            get { return (Bitmap & 0b01000000) != 0; }
            set { Bitmap &= 0b10111111; if (value) Bitmap |= 0b01000000; }
        }

        /// <summary>
        /// Indicates whether the posit includes the region ((0, 1), 0, 0)
        /// </summary>
        public bool Region_Edge_X
        {
            get { return (Bitmap & 0b00000010) != 0; }
            set { Bitmap &= 0b11111101; if (value) Bitmap |= 0b00000010; }
        }

        /// <summary>
        /// Indicates whether the posit includes the region (0, (0, 1), 0)
        /// </summary>
        public bool Region_Edge_Y
        {
            get { return (Bitmap & 0b00000100) != 0; }
            set { Bitmap &= 0b11111011; if (value) Bitmap |= 0b00000100; }
        }

        /// <summary>
        /// Indicates whether the posit includes the region (0, 0, (0, 1))
        /// </summary>
        public bool Region_Edge_Z
        {
            get { return (Bitmap & 0b00010000) != 0; }
            set { Bitmap &= 0b11101111; if (value) Bitmap |= 0b00010000; }
        }

        /// <summary>
        /// Indicates whether the posit includes the point (0, 0, 0)
        /// </summary>
        public bool Region_Corner
        {
            get { return (Bitmap & 0b00000001) != 0; }
            set { Bitmap &= 0b11111110; if (value) Bitmap |= 0b00000001; }
        }

        /// <summary>
        /// A <see cref="Posit3"/> representing the entire domain, ([0, 1), [0, 1), [0, 1)).
        /// </summary>
        public static Posit3 All => new Posit3(0b11111111);

        /// <summary>
        /// A <see cref="Posit3"/> representing the empty set.
        /// </summary>
        public static Posit3 None => new Posit3(0b00000000);

        /// <summary>
        /// A <see cref="Posit3"/> representing just the region ((0, 1), (0, 1), (0, 1))
        /// </summary>
        public static Posit3 Interior => new Posit3(0b10000000);

        /// <summary>
        /// A <see cref="Posit3"/> representing just the region ((0, 1), (0, 1), 0)
        /// </summary>
        public static Posit3 Face_X_Y => new Posit3(0b00001000);

        /// <summary>
        /// A <see cref="Posit3"/> representing just the region ((0, 1), 0, (0, 1))
        /// </summary>
        public static Posit3 Face_X_Z => new Posit3(0b00100000);

        /// <summary>
        /// A <see cref="Posit3"/> representing just the region (0, (0, 1), (0, 1))
        /// </summary>
        public static Posit3 Face_Y_Z => new Posit3(0b01000000);

        /// <summary>
        /// A <see cref="Posit3"/> representing just the region ((0, 1), 0, 0)
        /// </summary>
        public static Posit3 Edge_X => new Posit3(0b00000010);

        /// <summary>
        /// A <see cref="Posit3"/> representing just the region (0, (0, 1), 0)
        /// </summary>
        public static Posit3 Edge_Y => new Posit3(0b00000100);

        /// <summary>
        /// A <see cref="Posit3"/> representing just the region (0, 0, (0, 1))
        /// </summary>
        public static Posit3 Edge_Z => new Posit3(0b00010000);

        /// <summary>
        /// A <see cref="Posit3"/> representing just the point (0, 0, 0)
        /// </summary>
        public static Posit3 Corner => new Posit3(0b00000001);

        /// <summary>
        /// Determines whether the posit includes the given point, assuming it is in
        /// the domain ([0, 1), [0, 1), [0, 1)).
        /// </summary>
        public bool Contains(Vector3 point)
        {
            byte region = 1;
            if (point.X > 0) region <<= 1;
            if (point.Y > 0) region <<= 2;
            if (point.Z > 0) region <<= 4;
            return (Bitmap & region) != 0;
        }

        /// <summary>
        /// Gets a <see cref="Posit3"/> representing the intersection of the points of the
        /// given <see cref="Posit3"/>s.
        /// </summary>
        public static Posit3 operator &(Posit3 a, Posit3 b)
        {
            return new Posit3((byte)(a.Bitmap & b.Bitmap));
        }

        /// <summary>
        /// Gets a <see cref="Posit3"/> representing the union of the points of the
        /// given <see cref="Posit3"/>s.
        /// </summary>
        public static Posit3 operator |(Posit3 a, Posit3 b)
        {
            return new Posit3((byte)(a.Bitmap | b.Bitmap));
        }

        /// <summary>
        /// Gets a <see cref="Posit3"/> representing the complement of the set of points
        /// in the given <see cref="Posit3"/>.
        /// </summary>
        public static Posit3 operator ~(Posit3 a)
        {
            return new Posit3(unchecked((byte)(~a.Bitmap)));
        }

        public static bool operator ==(Posit3 a, Posit3 b)
        {
            return a.Bitmap == b.Bitmap;
        }

        public static bool operator !=(Posit3 a, Posit3 b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Posit3))
                return false;
            return this == (Posit3)obj;
        }

        bool IEquatable<Posit3>.Equals(Posit3 other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return Bitmap.GetHashCode();
        }
    }

    /// <summary>
    /// Contains helper functions related to orthogonal types.
    /// </summary>
    public static partial class Orthogonal
    {
        /// <summary>
        /// Converts a <see cref="Dir1"/> to an integer value. Either 1 for <see cref="Dir1.Pos"/> or
        /// -1 for <see cref="Dir1.Neg"/>.
        /// </summary>
        public static int ToInt(this Dir1 dir)
        {
            return ((int)dir << 1) - 1;
        }

        /// <summary>
        /// Swaps the X axis for the Y axis, and vice versa.
        /// </summary>
        public static Axis2 Swap(this Axis2 axis)
        {
            return (Axis2)((int)axis ^ 0b1);
        }

        /// <summary>
        /// Gets the axis for a direction.
        /// </summary>
        public static Axis2 GetAxis(this Dir2 dir)
        {
            return (Axis2)((int)dir >> 1);
        }

        /// <summary>
        /// Gets the axis for a direction.
        /// </summary>
        public static Axis2 GetAxis(this Dir2 dir, out Dir1 adir)
        {
            adir = (Dir1)((int)dir & 0b1);
            return (Axis2)((int)dir >> 1);
        }

        /// <summary>
        /// Gets the axis for a direction.
        /// </summary>
        public static Axis3 GetAxis(this Dir3 dir)
        {
            return (Axis3)((int)dir >> 1);
        }

        /// <summary>
        /// Gets the axis for a direction.
        /// </summary>
        public static Axis3 GetAxis(this Dir3 dir, out Dir1 adir)
        {
            adir = (Dir1)((int)dir & 0b1);
            return (Axis3)((int)dir >> 1);
        }

        /// <summary>
        /// Gets the axis for a <see cref="AxisQuadrant"/>.
        /// </summary>
        public static Axis3 GetAxis(this AxisQuadrant axisQuad, out Quadrant quad)
        {
            quad = (Quadrant)((int)axisQuad & 0b11);
            return (Axis3)((int)axisQuad >> 2);
        }

        /// <summary>
        /// Gets the direction of an octant on a particular axis.
        /// </summary>
        public static Dir1 GetDir(this Octant octant, Axis3 axis)
        {
            return (Dir1)(((int)octant >> (int)axis) & 0b1);
        }

        /// <summary>
        /// Converts an axis to a direction.
        /// </summary>
        public static Dir2 ToDir(this Axis2 axis, Dir1 dir)
        {
            return (Dir2)(((int)axis << 1) | (int)dir);
        }

        /// <summary>
        /// Converts an axis to a direction.
        /// </summary>
        public static Dir3 ToDir(this Axis3 axis, Dir1 dir)
        {
            return (Dir3)(((int)axis << 1) | (int)dir);
        }

        /// <summary>
        /// Given an axis and a direction perpendicular to it, returns the direction in relation to the axis.
        /// </summary>
        public static Dir1 Localize(this Axis2 axis, Dir2 dir)
        {
            dir.GetAxis(out Dir1 res);
            return res;
        }

        /// <summary>
        /// The inverse operation of <see cref="Localize(Axis2, Dir2)"/>.
        /// </summary>
        public static Dir2 Unlocalize(this Axis2 axis, Dir1 dir)
        {
            return axis.Swap().ToDir(dir);
        }

        /// <summary>
        /// Given an axis and a direction perpendicular to it, returns the direction in relation to the axis.
        /// </summary>
        public static Dir2 Localize(this Axis3 axis, Dir3 dir)
        {
            int i = (int)dir + 4 - ((int)axis << 1);
            if (i >= 6) i -= 6;
            return (Dir2)i;
        }

        /// <summary>
        /// The inverse operation of <see cref="Localize(Axis3, Dir3)"/>.
        /// </summary>
        public static Dir3 Unlocalize(this Axis3 axis, Dir2 dir)
        {
            int i = ((int)axis << 1) + (int)dir + 2;
            if (i >= 6) i -= 6;
            return (Dir3)i;
        }

        /// <summary>
        /// Given an axis and another axis perpendicular to it, returns the other axis in relation to the first axis.
        /// </summary>
        public static Axis2 Localize(this Axis3 axis, Axis3 other)
        {
            int i = (int)other + 2 - (int)axis;
            if (i >= 3) i -= 3;
            return (Axis2)i;
        }

        /// <summary>
        /// The inverse operation of <see cref="Localize(Axis3, Axis3)"/>.
        /// </summary>
        public static Axis3 Unlocalize(this Axis3 axis, Axis2 other)
        {
            int i = (int)axis + (int)other + 1;
            if (i >= 3) i -= 3;
            return (Axis3)i;
        }

        /// <summary>
        /// Gets the components of the given vector in relation to this axis.
        /// </summary>
        public static void Localize(this Axis2 axis, Vector2 vec, out Scalar on, out Scalar off)
        {
            if (axis == Axis2.X)
            {
                on = vec.X;
                off = vec.Y;
            }
            else
            {
                on = vec.Y;
                off = vec.X;
            }
        }

        /// <summary>
        /// The inverse operation of <see cref="Localize(Axis2, Vector2, out Scalar, out Scalar)"/>
        /// </summary>
        public static Vector2 Unlocalize(this Axis2 axis, Scalar on, Scalar off)
        {
            if (axis == Axis2.X) return new Vector2(on, off);
            else return new Vector2(off, on);
        }

        /// <summary>
        /// Gets the components of the given vector in relation to this axis.
        /// </summary>
        public static void Localize(this Axis3 axis, Vector3 vec, out Scalar on, out Vector2 off)
        {
            if (axis == Axis3.X)
            {
                on = vec.X;
                off = vec.Y_Z;
            }
            else if (axis == Axis3.Y)
            {
                on = vec.Y;
                off = vec.Z_X;
            }
            else
            {
                on = vec.Z;
                off = vec.X_Y;
            }
        }

        /// <summary>
        /// The inverse operation of <see cref="Localize(Axis3, Vector3, out Scalar, out Vector2)"/>
        /// </summary>
        public static Vector3 Unlocalize(this Axis3 axis, Scalar on, Vector2 off)
        {
            if (axis == Axis3.X) return new Vector3(on, off.X, off.Y);
            else if (axis == Axis3.Y) return new Vector3(off.Y, on, off.X);
            else return new Vector3(off.X, off.Y, on);
        }

        /// <summary>
        /// Converts an axis to a <see cref="AxisQuadrant"/>.
        /// </summary>
        public static AxisQuadrant ToAxisQuadrant(this Axis3 axis, Quadrant quad)
        {
            return (AxisQuadrant)(((int)axis << 2) | (int)quad);
        }

        /// <summary>
        /// Flips the given direction.
        /// </summary>
        public static Dir1 Flip(this Dir1 dir)
        {
            return (Dir1)((int)dir ^ 0b1);
        }

        /// <summary>
        /// Flips the given direction.
        /// </summary>
        public static Dir2 Flip(this Dir2 dir)
        {
            return (Dir2)((int)dir ^ 0b1);
        }

        /// <summary>
        /// Flips the given direction.
        /// </summary>
        public static Dir3 Flip(this Dir3 dir)
        {
            return (Dir3)((int)dir ^ 0b1);
        }

        /// <summary>
        /// Flips the given octant on the given axis.
        /// </summary>
        public static Octant Flip(this Octant octant, Axis3 axis)
        {
            return (Octant)((int)octant ^ (1 << (int)axis));
        }

        /// <summary>
        /// Flips the given octant on all axes.
        /// </summary>
        public static Octant Flip(this Octant octant)
        {
            return (Octant)((int)octant ^ 0b111);
        }

        /// <summary>
        /// Converts a <see cref="Quadrant"/> to a <see cref="Vector2i"/>, with each component being 1 or -1.
        /// </summary>
        public static Vector2i ToVector(this Quadrant quad)
        {
            return new Vector2i(
                (((int)quad & 0b01) << 1) - 1,
                ((int)quad & 0b10) - 1);
        }


        /// <summary>
        /// Converts a <see cref="Octant"/> to a <see cref="Vector3i"/>, with each component being 1 or -1.
        /// </summary>
        public static Vector3i ToVector(this Octant octant)
        {
            return new Vector3i(
                (((int)octant & 0b001) << 1) - 1,
                ((int)octant & 0b010) - 1,
                (((int)octant & 0b100) >> 1) - 1);
        }

        /// <summary>
        /// Returns <paramref name="value"/> if <paramref name="dir"/> is positive and 0 otherwise.
        /// </summary>
        public static int IfPos(this Dir1 dir, int value)
        {
            return value & -(int)dir;
        }

        /// <summary>
        /// For each axis, returns <paramref name="value"/> if <paramref name="octant"/> is positive
        /// on that axis and 0 otherwise.
        /// </summary>
        public static Vector3i IfPos(this Octant octant, int value)
        {
            return new Vector3i(
                octant.GetDir(Axis3.X).IfPos(value),
                octant.GetDir(Axis3.Y).IfPos(value),
                octant.GetDir(Axis3.Z).IfPos(value));
        }

        /// <summary>
        /// For each axis, returns the corresponding component from <paramref name="value"/> if <paramref name="octant"/>
        /// is positive on that axis and 0 otherwise.
        /// </summary>
        public static Vector3i IfPos(this Octant octant, Vector3i value)
        {
            return new Vector3i(
                octant.GetDir(Axis3.X).IfPos(value.X),
                octant.GetDir(Axis3.Y).IfPos(value.Y),
                octant.GetDir(Axis3.Z).IfPos(value.Z));
        }

        /// <summary>
        /// Sums the components corresponding to the axes where <paramref name="octant"/> is positive.
        /// </summary>
        public static int SumIfPos(this Octant octant, Vector3i value)
        {
            return
                octant.GetDir(Axis3.X).IfPos(value.X) +
                octant.GetDir(Axis3.Y).IfPos(value.Y) +
                octant.GetDir(Axis3.Z).IfPos(value.Z);
        }
    }
}
