﻿using System;
using System.Collections.Generic;

namespace Alunite.Data.Geometry
{
    /// <summary>
    /// A bounded discrete grid in three-dimensional space where each cell is associated with a value of type
    /// <typeparamref name="T"/>.
    /// </summary>
    public interface IGrid3<T>
    {
        /// <summary>
        /// The size of this grid.
        /// </summary>
        Vector3i Size { get; }

        /// <summary>
        /// Gets the value of a cell in this grid.
        /// </summary>
        T this[Vector3i cell] { get; }
    }
    
    /// <summary>
    /// Associates a <see cref="IGrid3{T}"/> cell with its position.
    /// </summary>
    public struct Position3_Cell<T> : IComparable<Position3_Cell<T>>
    {
        public Vector3i Position;
        public T Cell;
        public Position3_Cell(Vector3i pos, T cell)
        {
            Position = pos;
            Cell = cell;
        }

        int IComparable<Position3_Cell<T>>.CompareTo(Position3_Cell<T> other)
        {
            return Vector3i.Compare(Position, other.Position);
        }
    }

    /// <summary>
    /// A three-dimensional grid defined by an array.
    /// </summary>
    public struct ArrayGrid3<T> : IGrid3<T>
    {
        public ArrayGrid3(Vector3i size, T[] array)
        {
            Size = size;
            Array = array;
        }

        /// <summary>
        /// The size of the grid.
        /// </summary>
        public Vector3i Size;

        /// <summary>
        /// The array defining the grid.
        /// </summary>
        public T[] Array;

        /// <summary>
        /// Creates an <see cref="ArrayGrid3{T}"/> of the given size, with all cells initialized to the default value
        /// of <typeparamref name="T"/>.
        /// </summary>
        public static ArrayGrid3<T> Create(Vector3i size)
        {
            return new ArrayGrid3<T>(size, new T[size.X * size.Y * size.Z]);
        }

        /// <summary>
        /// Creates an <see cref="ArrayGrid3{T}"/> of the given size, with all cells initialized to the default value
        /// of <typeparamref name="T"/>.
        /// </summary>
        public static ArrayGrid3<T> Create(uint sizeX, uint sizeY, uint sizeZ)
        {
            return Create(new Vector3i((int)sizeX, (int)sizeY, (int)sizeZ));
        }

        /// <summary>
        /// Creates a copy of the given grid.
        /// </summary>
        public static ArrayGrid3<T> Create<TGrid>(TGrid source)
            where TGrid : IGrid3<T>
        {
            Vector3i size = source.Size;
            int index = 0;
            ArrayGrid3<T> res = Create(size);
            for (int z = 0; z < size.Z; z++)
            {
                for (int y = 0; y < size.Y; y++)
                {
                    for (int x = 0; x < size.X; x++)
                    {
                        res.Array[index++] = source[new Vector3i(x, y, z)];
                    }
                }
            }
            return res;
        }

        /// <summary>
        /// Gets a reference to a cell in this grid.
        /// </summary>
        public ref T this[Vector3i cell]
        {
            get
            {
                return ref Array[cell.Z * (Size.X * Size.Y) + cell.Y * Size.X + cell.X];
            }
        }

        /// <summary>
        /// Gets a reference to a cell in this grid.
        /// </summary>
        public ref T this[uint x, uint y, uint z]
        {
            get
            {
                return ref Array[z * (Size.X * Size.Y) + y * Size.X + x];
            }
        }

        /// <summary>
        /// Appends the cells that meet the given criteria to the given list, in sorted order.
        /// </summary>
        public void Search<TN>(ListBuilder<Position3_Cell<TN>> cells, FuncPredicate<T, TN> pred)
        {
            Grid3.Search(this, cells, pred);
        }

        /// <summary>
        /// Gets the list of cells that meet the given criteria, in sorted order.
        /// </summary>
        public ListBuilder<Position3_Cell<TN>> Search<TN>(FuncPredicate<T, TN> pred)
        {
            var res = new ListBuilder<Position3_Cell<TN>>();
            Search(res, pred);
            return res;
        }

        Vector3i IGrid3<T>.Size
        {
            get
            {
                return Size;
            }
        }

        T IGrid3<T>.this[Vector3i cell]
        {
            get
            {
                return this[cell];
            }
        }
    }

    /// <summary>
    /// A three-dimensional grid defined by a given sampling function.
    /// </summary>
    public struct FuncGrid3<T> : IGrid3<T>
    {
        public FuncGrid3(Vector3i size, Func<Vector3i, T> func)
        {
            Size = size;
            Func = func;
        }

        /// <summary>
        /// The size of this grid.
        /// </summary>
        public Vector3i Size;

        /// <summary>
        /// The function defining this grid.
        /// </summary>
        public Func<Vector3i, T> Func;

        /// <summary>
        /// Gets the value of a cell in this grid.
        /// </summary>
        public T this[Vector3i cell]
        {
            get
            {
                return Func(cell);
            }
        }

        /// <summary>
        /// Appends the cells that meet the given criteria to the given list, in sorted order.
        /// </summary>
        public void Search<TN>(ListBuilder<Position3_Cell<TN>> cells, FuncPredicate<T, TN> pred)
        {
            Grid3.Search(this, cells, pred);
        }

        /// <summary>
        /// Gets the list of cells that meet the given criteria, in sorted order.
        /// </summary>
        public ListBuilder<Position3_Cell<TN>> Search<TN>(FuncPredicate<T, TN> pred)
        {
            var res = new ListBuilder<Position3_Cell<TN>>();
            Search(res, pred);
            return res;
        }

        Vector3i IGrid3<T>.Size
        {
            get
            {
                return Size;
            }
        }
    }

    /// <summary>
    /// A three-dimensional grid defined by applying a mapping function to a source grid.
    /// </summary>
    public struct MapGrid3<T, TN> : IGrid3<TN>
    {
        public MapGrid3(IGrid3<T> source, Func<T, TN> func)
        {
            Source = source;
            Func = func;
        }

        /// <summary>
        /// The source grid being mapped.
        /// </summary>
        public IGrid3<T> Source;

        /// <summary>
        /// The mapping function applied by this grid.
        /// </summary>
        public Func<T, TN> Func;

        /// <summary>
        /// The size of this grid.
        /// </summary>
        public Vector3i Size => Source.Size;

        /// <summary>
        /// Gets the value of a cell in this grid.
        /// </summary>
        public TN this[Vector3i cell] => Func(Source[cell]);
    }

    /// <summary>
    /// Contains helper functions related to <see cref="IGrid3{T}"/>.
    /// </summary>
    public static class Grid3
    {
        /// <summary>
        /// Appends the cells that meet the given criteria to the given list in sorted order, using a generic
        /// unoptimized implementation.
        /// </summary>
        public static void Search<TGrid, T, TN>(TGrid grid, ListBuilder<Position3_Cell<TN>> cells, FuncPredicate<T, TN> pred)
            where TGrid : IGrid3<T>
        {
            Vector3i size = grid.Size;
            for (int z = 0; z < size.Z; z++)
            {
                for (int y = 0; y < size.Y; y++)
                {
                    for (int x = 0; x < size.X; x++)
                    {
                        Vector3i pos = new Vector3i(x, y, z);
                        if (pred(grid[pos], out TN cell)) cells.Append(new Position3_Cell<TN>(pos, cell));
                    }
                }
            }
        }

        /// <summary>
        /// Gets a cell in the grid, or returns the given default value if the cell is out of bounds of the grid.
        /// </summary>
        public static T Get<T>(this IGrid3<T> grid, Vector3i cell, T outside)
        {
            Vector3i size = grid.Size;
            if ((uint)cell.X < size.X && (uint)cell.Y < size.Y && (uint)cell.Z < size.Z)
                return grid[cell];
            else
                return outside;
        }
    }

    /// <summary>
    /// A function which maps cells in a <see cref="IGrid3{T}"/> while threading information from/to neighboring
    /// cells in the X, Y and Z directions. 
    /// </summary>
    /// <typeparam name="T">The type of information stored in cells prior to mapping</typeparam>
    /// <typeparam name="TN">The type of information stored in cells after mapping</typeparam>
    /// <typeparam name="T_X">The type of information threaded along the X direction</typeparam>
    /// <typeparam name="T_Y">The type of information threaded along the Y direction</typeparam>
    /// <typeparam name="T_Z">The type of information threaded along the Z direction</typeparam>
    /// <param name="x">The information received from the neighboring cell in the negative X direction
    /// and transmitted to neighboring cell in the positive X direction</param>
    /// <param name="y">The information received from the neighboring cell in the negative Y direction
    /// and transmitted to neighboring cell in the positive Y direction</param>
    /// <param name="z">The information received from the neighboring cell in the negative Z direction
    /// and transmitted to neighboring cell in the positive Z direction</param>
    public delegate TN SweepFunc3<T, TN, T_X, T_Y, T_Z>(T cell, ref T_X x, ref T_Y y, ref T_Z z);
}