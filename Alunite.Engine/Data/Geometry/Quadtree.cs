﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Alunite.Data.Geometry
{
    /// <summary>
    /// A specialization of <see cref="Quartet{T}"/> for <see cref="Wad"/>s. This is the group type for a
    /// <see cref="WadDictionary{TGroup, TLeaf}"/> of quadtree wads.
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 8)]
    public unsafe struct WadQuartet : IEquatable<WadQuartet>, IWadGroup
    {
        [FieldOffset(0)] private fixed ushort _members[4];
        [FieldOffset(0)] private ulong _low;
        [FieldOffset(0)] public Wad NegX_NegY;
        [FieldOffset(2)] public Wad PosX_NegY;
        [FieldOffset(4)] public Wad NegX_PosY;
        [FieldOffset(6)] public Wad PosX_PosY;

        private WadQuartet(ulong low) : this()
        {
            _low = low;
        }

        public WadQuartet(Wad a, Wad b, Wad c, Wad d) : this()
        {
            NegX_NegY = a;
            PosX_NegY = b;
            NegX_PosY = c;
            PosX_PosY = d;
        }

        /// <summary>
        /// Gets or sets a value of an quadrant in this quartet.
        /// </summary>
        public Wad this[Quadrant quad]
        {
            get
            {
                return new Wad(_members[(int)quad]);
            }
            set
            {
                _members[(int)quad] = value._name;
            }
        }

        /// <summary>
        /// Constructs an <see cref="WadQuartet"/> with the given value for all quadrants.
        /// </summary>
        public static WadQuartet Uniform(Wad wad)
        {
            ulong val = wad._name;
            val |= val << 16;
            val |= val << 32;
            return new WadQuartet(val);
        }

        Wad IWadGroup.First
        {
            get
            {
                return NegX_NegY;
            }
        }

        Wad IWadGroup.Uniform
        {
            set
            {
                this = Uniform(value);
            }
        }

        public static implicit operator WadQuartet(Quartet<Wad> quads)
        {
            return new WadQuartet(
                quads.NegX_NegY, quads.PosX_NegY,
                quads.NegX_PosY, quads.PosX_PosY);
        }

        public static implicit operator Quartet<Wad>(WadQuartet quads)
        {
            return new Quartet<Wad>(
                quads.NegX_NegY, quads.PosX_NegY,
                quads.NegX_PosY, quads.PosX_PosY);
        }

        public static bool operator ==(WadQuartet a, WadQuartet b)
        {
            return a._low == b._low;
        }

        public static bool operator !=(WadQuartet a, WadQuartet b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is WadQuartet))
                return false;
            return this == (WadQuartet)obj;
        }

        bool IEquatable<WadQuartet>.Equals(WadQuartet other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return _low.GetHashCode();
        }
    }

    /// <summary>
    /// Contains helper functions for <see cref="Wad"/>s and <see cref="WadDictionary{TGroup, TLeaf}"/> being used to
    /// implement quadtrees.
    /// </summary>
    public static class Quadtree
    {
        /// <summary>
        /// Gets or constructs a <see cref="Wad"/> for an interior node with the given children. If this dictionary is
        /// not expandable according to <see cref="WadDictionary{TGroup, TLeaf}.IsExpandable"/>, or the maximum definition
        /// limit has been reached, this will return <see cref="Wad.Invalid"/>.
        /// </summary>
        public static Wad Group<T>(
            this WadDictionary<WadQuartet, T> dict,
            Wad a, Wad b, Wad c, Wad d)
        {
            return dict.Group(new WadQuartet(a, b, c, d));
        }

        /// <summary>
        /// Gets a <see cref="Wad"/> representation of the given <see cref="Quartet{T}"/> of leaf values.
        /// </summary>
        public static Wad LeafQuartet<T>(this WadDictionary<WadQuartet, T> dict, in Quartet<T> quartet)
        {
            return dict.Group(
                dict.Leaf(quartet.NegX_NegY),
                dict.Leaf(quartet.PosX_NegY),
                dict.Leaf(quartet.NegX_PosY),
                dict.Leaf(quartet.PosX_PosY));
        }

        /// <summary>
        /// Gets an <see cref="Quartet{T}"/> representation of the leaves in the given <see cref="Wad"/>, assuming it has
        /// a depth of at most 1.
        /// </summary>
        public static Quartet<T> AsLeafQuartet<T>(this WadDictionary<WadQuartet, T> dict, Wad wad)
        {
            if (dict.AsLeaf(wad, out T value))
                return Quartet<T>.Uniform(value);
            WadQuartet group = dict.AsGroup(wad);
            return new Quartet<T>(
                dict.AsLeaf(group.NegX_NegY),
                dict.AsLeaf(group.PosX_NegY),
                dict.AsLeaf(group.NegX_PosY),
                dict.AsLeaf(group.PosX_PosY));
        }

        /// <summary>
        /// Constructs an quadtree representing a region of the given 2D function.
        /// </summary>
        /// <param name="size">The size of the resulting quadtree.</param>
        /// <param name="offset">The offset at which to start sampling <paramref name="func"/></param>
        public static Wad Build<T>(
            this WadDictionary<WadQuartet, T> dict,
            Func<Vector2i, T> func, int size, Vector2i offset)
        {
            var grid = new FuncGrid2<T>(default, func);
            return dict.Build(grid, size, offset);
        }

        /// <summary>
        /// Constructs a node for an quadtree representing a region of the given grid.
        /// </summary>
        public static Wad Build<TGrid, T>(
            this WadDictionary<WadQuartet, T> dict,
            TGrid grid, int size, Vector2i offset)
            where TGrid : IGrid2<T>
        {
            if (size <= 1)
            {
                return dict.Leaf(grid[offset]);
            }
            else
            {
                Debug.Assert((int)(size & 0b1) == 0);
                int hsize = size >> 1;
                Debug.Assert(hsize > 0);
                Vector2i altset = new Vector2i(offset.X + hsize, offset.Y + hsize);
                return dict.Group(
                    dict.Build(grid, hsize, new Vector2i(offset.X, offset.Y)),
                    dict.Build(grid, hsize, new Vector2i(altset.X, offset.Y)),
                    dict.Build(grid, hsize, new Vector2i(offset.X, altset.Y)),
                    dict.Build(grid, hsize, new Vector2i(altset.X, altset.Y)));
            }
        }

        /// <summary>
        /// Imports a node from another dictionary into this dictionary. If this dictionary is
        /// not expandable according to <see cref="WadDictionary{TGroup, TLeaf}.IsExpandable"/>,
        /// or the maximum definition limit has been reached, this will return <see cref="Wad.Invalid"/>.
        /// </summary>
        public static Wad Import<T>(
            this WadDictionary<WadQuartet, T> dict,
            WadDictionary<WadQuartet, T> other,
            Wad node)
        {
            if (dict == other) return node;
            else return dict._import(other, node);
        }

        /// <summary>
        /// Implementation of <see cref="Import"/>, making the assumption that <paramref name="other"/> is
        /// not this.
        /// </summary>
        private static Wad _import<T>(
            this WadDictionary<WadQuartet, T> dict,
            WadDictionary<WadQuartet, T> other,
            Wad node)
        {
            if (other.AsLeaf(node, out T leaf))
            {
                return dict.Leaf(leaf);
            }
            else
            {
                var quartet = other.AsGroup(node);
                return dict.Group(
                    dict._import(other, quartet.NegX_NegY),
                    dict._import(other, quartet.PosX_NegY),
                    dict._import(other, quartet.NegX_PosY),
                    dict._import(other, quartet.PosX_PosY));
            }
        }

        /// <summary>
        /// Applies a mapping function to the leaf values of a given wad.
        /// </summary>
        public static Wad Map<T, TN>(
            this WadDictionary<WadQuartet, TN> target,
            Func<T, TN> func, WadDictionary<WadQuartet, T> source, Wad wad)
        {
            return new Transformer2<T, TN>(func, Quartet.Identity, source, target).Transform(wad);
        }

        /// <summary>
        /// A helper for implementing mappings and transformations that result in <see cref="Duetree"/>s.
        /// </summary>
        public struct Transformer1<T, TN>
        {
            public Transformer1(
                Func<T, TN> mapping, Duet<Quadrant> trans,
                WadDictionary<WadQuartet, T> source,
                WadDictionary<WadDuet, TN> target)
            {
                Mapping = mapping;
                Transformation = trans;
                Source = source;
                Target = target;
                Cache = new TempCache<Wad, Wad>(16);
            }

            /// <summary>
            /// The mapping function applied to leaf values.
            /// </summary>
            public Func<T, TN> Mapping;

            /// <summary>
            /// Provides the source quadrant for a given target direction.
            /// </summary>
            public Duet<Quadrant> Transformation;

            /// <summary>
            /// The dictionary where source wads are defined.
            /// </summary>
            public WadDictionary<WadQuartet, T> Source;

            /// <summary>
            /// The dictionary where target wads are defined.
            /// </summary>
            public WadDictionary<WadDuet, TN> Target;

            /// <summary>
            /// Caches the results of transforming a given source octet.
            /// </summary>
            public TempCache<Wad, Wad> Cache;

            /// <summary>
            /// Gets the mapped and transformed form of the given source wad.
            /// </summary>
            public Wad Transform(Wad source)
            {
                if (Source.AsLeaf(source, out T leaf))
                {
                    return Target.Leaf(Mapping(leaf));
                }
                else
                {
                    // Check cache
                    if (!Cache.TryGet(source, out Wad res))
                    {
                        WadQuartet quartet = Source.AsGroup(source);
                        res = Target.Group(new WadDuet(
                            Transform(quartet[Transformation.Neg]),
                            Transform(quartet[Transformation.Pos])));
                        Cache.Add(source, res);
                    }
                    return res;
                }
            }
        }

        /// <summary>
        /// A helper for implementing mappings and transformations that result in <see cref="Quadtree"/>s.
        /// </summary>
        public struct Transformer2<T, TN>
        {
            public Transformer2(
                Func<T, TN> mapping, Quartet<Quadrant> trans,
                WadDictionary<WadQuartet, T> source,
                WadDictionary<WadQuartet, TN> target)
            {
                Mapping = mapping;
                Transformation = trans;
                Source = source;
                Target = target;
                Cache = new TempCache<Wad, Wad>(16);
            }

            /// <summary>
            /// The mapping function applied to leaf values.
            /// </summary>
            public Func<T, TN> Mapping;

            /// <summary>
            /// Provides the source quadrant for a given target quadrant.
            /// </summary>
            public Quartet<Quadrant> Transformation;

            /// <summary>
            /// The dictionary where source wads are defined.
            /// </summary>
            public WadDictionary<WadQuartet, T> Source;

            /// <summary>
            /// The dictionary where target wads are defined.
            /// </summary>
            public WadDictionary<WadQuartet, TN> Target;

            /// <summary>
            /// Caches the results of transforming a given source octet.
            /// </summary>
            public TempCache<Wad, Wad> Cache;

            /// <summary>
            /// Gets the mapped and transformed form of the given source wad.
            /// </summary>
            public Wad Transform(Wad source)
            {
                if (Source.AsLeaf(source, out T leaf))
                {
                    return Target.Leaf(Mapping(leaf));
                }
                else
                {
                    // Check cache
                    if (!Cache.TryGet(source, out Wad res))
                    {
                        WadQuartet quartet = Source.AsGroup(source);
                        res = Target.Group(new WadQuartet(
                            Transform(quartet[Transformation.NegX_NegY]),
                            Transform(quartet[Transformation.PosX_NegY]),
                            Transform(quartet[Transformation.NegX_PosY]),
                            Transform(quartet[Transformation.PosX_PosY])));
                        Cache.Add(source, res);
                    }
                    return res;
                }
            }
        }
    }
}
