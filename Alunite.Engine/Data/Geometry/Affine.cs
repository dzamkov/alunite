﻿using System;

using Alunite.Data.Probability;

namespace Alunite.Data.Geometry
{
    /// <summary>
    /// An affine transformation in two-dimensional space.
    /// </summary>
    public struct Affine2
    {
        public Affine2(Matrix2x2 linear, Vector2 offset)
        {
            Linear = linear;
            Offset = offset;
        }

        /// <summary>
        /// The linear component of this transformation.
        /// </summary>
        public Matrix2x2 Linear;

        /// <summary>
        /// The translation component of this transformation.
        /// </summary>
        public Vector2 Offset;

        /// <summary>
        /// The identity transformation.
        /// </summary>
        public static Affine2 Identity => new Affine2(Matrix2x2.Identity, Vector2.Zero);

        /// <summary>
        /// Constructs a transformation which translates by the given amount.
        /// </summary>
        public static Affine2 Translate(Vector2 offset)
        {
            return new Affine2(Matrix2x2.Identity, offset);
        }

        /// <summary>
        /// Constructs a transformation which translates by the given amount.
        /// </summary>
        public static Affine2 Translate(Scalar x, Scalar y)
        {
            return Translate(new Vector2(x, y));
        }

        /// <summary>
        /// Constructs a transformation which scales by the given amount.
        /// </summary>
        public static Affine2 Scale(Vector2 amount)
        {
            return new Affine2(
                new Matrix2x2(
                    new Vector2(amount.X, 0),
                    new Vector2(0, amount.Y)),
                Vector2.Zero);
        }

        /// <summary>
        /// Constructs a transformation which scales by the given amount.
        /// </summary>
        public static Affine2 Scale(Scalar x, Scalar y)
        {
            return Scale(new Vector2(x, y));
        }

        /// <summary>
        /// Gets the inverse of this transformation.
        /// </summary>
        public Affine2 Inverse
        {
            get
            {
                Matrix2x2 linearInv = Linear.Inverse;
                return new Affine2(linearInv, linearInv * -Offset);
            }
        }

        /// <summary>
        /// Applies this transformation to a position.
        /// </summary>
        public Vector2 Apply(Vector2 pos)
        {
            return (Linear * pos) + Offset;
        }

        /// <summary>
        /// Composes the given transformations, first applying the right, then the left.
        /// </summary>
        public static Affine2 Compose(Affine2 a, Affine2 b)
        {
            return new Affine2(a.Linear * b.Linear, a.Apply(b.Offset));
        }

        /// <summary>
        /// Determines whether the given <see cref="Affine2"/>s likely have the same true value
        /// after compensating for representation error.
        /// </summary>
        public static bool AreLikelyEqual(Affine2 a, Affine2 b)
        {
            return AreEqualOdds(a, b) > 1;
        }

        /// <summary>
        /// Determines the relative probability that the given <see cref="Affine2"/>s have the same true value, over
        /// the probability that their true values are independent. This is based on the apriori distribution of
        /// matrix values, and individual value distributions of <paramref name="a"/> and <paramref name="b"/>.
        /// </summary>
        public static LogScalar AreEqualOdds(Affine2 a, Affine2 b)
        {
            return Vector2.AreEqualOdds(a.Offset, b.Offset) * Matrix2x2.AreEqualOdds(a.Linear, b.Linear);
        }

        public static implicit operator Affine2(Rotation2 source)
        {
            return new Affine2(source, Vector2.Zero);
        }

        public static implicit operator Affine2(Motion2 source)
        {
            return new Affine2(source.Linear, source.Offset);
        }

        public static implicit operator Affine2(Similarity2 source)
        {
            return new Affine2(source.Linear, source.Offset);
        }

        public static Vector2 operator *(Affine2 a, Vector2 b)
        {
            return a.Apply(b);
        }

        public static Affine2 operator *(Affine2 a, Affine2 b)
        {
            return Compose(a, b);
        }
    }

    /// <summary>
    /// An affine transformation in three-dimensional space.
    /// </summary>
    public struct Affine3
    {
        public Affine3(Matrix3x3 linear, Vector3 offset)
        {
            Linear = linear;
            Offset = offset;
        }

        /// <summary>
        /// The linear component of this transformation.
        /// </summary>
        public Matrix3x3 Linear;

        /// <summary>
        /// The translation component of this transformation.
        /// </summary>
        public Vector3 Offset;

        /// <summary>
        /// The identity transformation.
        /// </summary>
        public static Affine3 Identity => new Affine3(Matrix3x3.Identity, Vector3.Zero);

        /// <summary>
        /// Constructs a transformation which translates by the given amount.
        /// </summary>
        public static Affine3 Translation(Vector3 offset)
        {
            return new Affine3(Matrix3x3.Identity, offset);
        }

        /// <summary>
        /// Constructs a transformation which translates by the given amount.
        /// </summary>
        public static Affine3 Translation(Scalar x, Scalar y, Scalar z)
        {
            return Translation(new Vector3(x, y, z));
        }

        /// <summary>
        /// Constructs a transformation which scales by the given amount on each axis.
        /// </summary>
        public static Affine3 Scale(Vector3 amount)
        {
            return new Affine3(
                new Matrix3x3(
                    new Vector3(amount.X, 0, 0),
                    new Vector3(0, amount.Y, 0),
                    new Vector3(0, 0, amount.Z)),
                Vector3.Zero);
        }

        /// <summary>
        /// Constructs a transformation which scales by the given amount on each axis.
        /// </summary>
        public static Affine3 Scale(Scalar x, Scalar y, Scalar z)
        {
            return Scale(new Vector3(x, y, z));
        }

        /// <summary>
        /// Gets the inverse of this transformation.
        /// </summary>
        public Affine3 Inverse
        {
            get
            {
                Matrix3x3 linearInv = Linear.Inverse;
                return new Affine3(linearInv, linearInv * -Offset);
            }
        }

        /// <summary>
        /// Applies this transformation to a position.
        /// </summary>
        public Vector3 Apply(Vector3 pos)
        {
            return (Linear * pos) + Offset;
        }

        /// <summary>
        /// Composes the given transformations, first applying the right, then the left.
        /// </summary>
        public static Affine3 Compose(Affine3 a, Affine3 b)
        {
            return new Affine3(a.Linear * b.Linear, a.Apply(b.Offset));
        }

        public static implicit operator Affine3(Rotation3i source)
        {
            return new Affine3(source, Vector3.Zero);
        }

        public static implicit operator Affine3(Rotation3 source)
        {
            return new Affine3(source, Vector3.Zero);
        }

        public static implicit operator Affine3(Motion3 source)
        {
            return new Affine3(source.Linear, source.Offset);
        }
        
        public static Vector3 operator *(Affine3 a, Vector3 b)
        {
            return a.Apply(b);
        }

        public static Affine3 operator *(Affine3 a, Affine3 b)
        {
            return Compose(a, b);
        }
    }
}
