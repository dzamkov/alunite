﻿using System;

namespace Alunite.Data.Geometry
{
    /// <summary>
    /// Describes a projection from three-dimensional camera view space (a right-handed coordinate system with X going
    /// forward, Y going left and Z going up) to three-dimensional device space, a 2x2x2 box centered on the origin.
    /// </summary>
    public struct Projection
    {
        private Scalar _scaleHorizontal;
        private Scalar _scaleVertical;
        private Scalar _scaleDepth;
        private Scalar _scaleDivisor;
        private Scalar _biasDepth;
        private Scalar _biasDivisor;

        /// <summary>
        /// Constructs a finite perspective projection.
        /// </summary>
        /// <param name="width">The width of the view volume at the near view plane.</param>
        /// <param name="height">The height of the view volume at the near view plane.</param>
        /// <param name="near">The distance to the near view plane.</param>
        /// <param name="far">The distance to the far view plane.</param>
        public static Projection Perspective(Scalar width, Scalar height, Scalar near, Scalar far)
        {
            return new Projection
            {
                _scaleHorizontal = 2 * near / width,
                _scaleVertical = 2 * near / height,
                _scaleDepth = -far / (near - far),
                _scaleDivisor = 1,
                _biasDepth = near * far / (near - far),
                _biasDivisor = 0
            };
        }

        /// <summary>
        /// Constructs an infinite perspective projection.
        /// </summary>
        /// <param name="width">The width of the view volume at the near view plane.</param>
        /// <param name="height">The height of the view volume at the near view plane.</param>
        /// <param name="near">The distance to the near view plane.</param>
        public static Projection Perspective(Scalar width, Scalar height, Scalar near)
        {
            return new Projection
            {
                _scaleHorizontal = 2 * near / width,
                _scaleVertical = 2 * near / height,
                _scaleDepth = 1,
                _scaleDivisor = 1,
                _biasDepth = -near,
                _biasDivisor = 0
            };
        }

        /// <summary>
        /// Constructs an orthographic projection.
        /// </summary>
        public static Projection Orthographic(Scalar width, Scalar height, Scalar near, Scalar far)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a normalized view space vector pointing outward (according to this projection) at the given view
        /// space position. All points along the ray defined by <paramref name="pos"/> and the returned direction
        /// will project to the same point. This is useful when computing specular highlights.
        /// </summary>
        public Vector3 GetEyeDirection(Vector3 pos)
        {
            return Vector3.Normalize(_scaleDivisor * pos + new Vector3(_biasDivisor, 0, 0));
        }

        /// <summary>
        /// A vector using homogeneous coordinates, which, when added to a position in view space and normalized,
        /// will yield the "eye direction" at that point. All points along the ray formed by the point and the
        /// eye direction will project to the same point. This is useful when computing specular highlights.
        /// </summary>
        public Vector4 EyeBias => new Vector4(_biasDivisor, 0, 0, _scaleDivisor);

        /// <summary>
        /// Gets a matrix representation of this projection. This matrix takes a vector (with W = 1) and transforms it
        /// to device space using homogeneous coordinates.
        /// </summary>
        public static implicit operator Matrix4x4(Projection proj)
        {
            return new Matrix4x4(
                new Vector4(0, 0, proj._scaleDepth, proj._scaleDivisor),
                new Vector4(-proj._scaleHorizontal, 0, 0, 0),
                new Vector4(0, proj._scaleVertical, 0, 0),
                new Vector4(0, 0, proj._biasDepth, proj._biasDivisor));
        }

        public static Matrix4x4 operator *(Projection proj, Motion3 trans)
        {
            return (Matrix4x4)proj * (Matrix4x4)trans;
        }
    }
}
