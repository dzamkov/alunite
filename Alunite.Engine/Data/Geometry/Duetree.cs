﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Alunite.Data.Geometry
{
    /// <summary>
    /// A specialization of <see cref="Duet{T}"/> for <see cref="Wad"/>s. This is the group type for a
    /// <see cref="WadDictionary{TGroup, TLeaf}"/> of duetree wads.
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 4)]
    public unsafe struct WadDuet : IEquatable<WadDuet>, IWadGroup
    {
        [FieldOffset(0)] private fixed ushort _members[2];
        [FieldOffset(0)] private uint _low;
        [FieldOffset(0)] public Wad Neg;
        [FieldOffset(2)] public Wad Pos;

        private WadDuet(uint low) : this()
        {
            _low = low;
        }

        public WadDuet(Wad a, Wad b) : this()
        {
            Neg = a;
            Pos = b;
        }

        /// <summary>
        /// Gets or sets a value of an direction in this duet.
        /// </summary>
        public Wad this[Dir1 dir]
        {
            get
            {
                return new Wad(_members[(int)dir]);
            }
            set
            {
                _members[(int)dir] = value._name;
            }
        }

        /// <summary>
        /// Constructs an <see cref="WadDuet"/> with the given value for all directions.
        /// </summary>
        public static WadDuet Uniform(Wad wad)
        {
            return new WadDuet(wad, wad);
        }

        Wad IWadGroup.First
        {
            get
            {
                return Neg;
            }
        }

        Wad IWadGroup.Uniform
        {
            set
            {
                this = Uniform(value);
            }
        }

        public static implicit operator WadDuet(Duet<Wad> duet)
        {
            return new WadDuet(duet.Neg, duet.Pos);
        }

        public static implicit operator Duet<Wad>(WadDuet duet)
        {
            return new Duet<Wad>(duet.Neg, duet.Pos);
        }

        public static bool operator ==(WadDuet a, WadDuet b)
        {
            return a._low == b._low;
        }

        public static bool operator !=(WadDuet a, WadDuet b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is WadDuet))
                return false;
            return this == (WadDuet)obj;
        }

        bool IEquatable<WadDuet>.Equals(WadDuet other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return _low.GetHashCode();
        }
    }

    /// <summary>
    /// Contains helper functions for <see cref="Wad"/>s and <see cref="WadDictionary{TGroup, TLeaf}"/> being used to
    /// implement duetree, the word I made up for the 1-dimensional analogue of an octree or quadtree.
    /// </summary>
    public static class Duetree
    {
    }
}
