﻿using System;
using System.Collections.Generic;

namespace Alunite.Data.Geometry
{
    /// <summary>
    /// Describes a framework of joints and "bones" which parameterize the allowable deformations of a
    /// three-dimensional object.
    /// </summary>
    public sealed class Skeleton
    {
        /// <summary>
        /// A <see cref="Skeleton"/> which contains just the <see cref="Root"/> bone and no joints.
        /// </summary>
        public static Skeleton Trivial { get; } = new Skeleton();

        /// <summary>
        /// The bone for the root transformation space of this skeleton.
        /// </summary>
        public Bone Root => default;

        /// <summary>
        /// Identifies a local transformation space within a <see cref="Skeleton"/>.
        /// </summary>
        public struct Bone
        {

        }
    }
}
