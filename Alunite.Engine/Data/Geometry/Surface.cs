﻿using System;
using System.Collections.Generic;

namespace Alunite.Data.Geometry
{
    /// <summary>
    /// Describes an oriented plane in three-dimensional space.
    /// </summary>
    public struct Plane
    {
        public Plane(Vector3 normal, Scalar offset)
        {
            Normal = normal;
            Offset = offset;
        }

        /// <summary>
        /// The normal direction of the plane. This will always be a unit vector.
        /// </summary>
        public Vector3 Normal;

        /// <summary>
        /// The offset of the plane along <see cref="Normal"/>. The plane includes all points whose dot product
        /// with <see cref="Normal"/> is this.
        /// </summary>
        public Scalar Offset;
    }
}
