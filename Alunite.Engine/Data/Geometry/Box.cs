﻿using System;
using System.Collections.Generic;

namespace Alunite.Data.Geometry
{
    /// <summary>
    /// An axis-aligned bounding box in three-dimensional space.
    /// </summary>
    public struct Box3
    {
        public Vector3 Min;
        public Vector3 Max;
        public Box3(Vector3 min, Vector3 max)
        {
            Min = min;
            Max = max;
        }

        public Box3(Scalar minX, Scalar maxX, Scalar minY, Scalar maxY, Scalar minZ, Scalar maxZ)
        {
            Min = new Vector3(minX, minY, minZ);
            Max = new Vector3(maxX, maxY, maxZ);
        }

        /// <summary>
        /// A vector whose components are the size of this box along the different axes.
        /// </summary>
        public Vector3 Size
        {
            get
            {
                return Max - Min;
            }
        }

        /// <summary>
        /// Determines whether this box contains the given vector.
        /// </summary>
        public bool Contains(Vector3 vec)
        {
            return
                vec.X >= Min.X && vec.X <= Max.X &&
                vec.Y >= Min.Y && vec.Y <= Max.Y &&
                vec.Z >= Min.Z && vec.Z <= Max.Z;
        }

        public static Box3 operator +(Vector3 a, Box3 b)
        {
            return new Box3(a + b.Min, a + b.Max);
        }

        public static Box3 operator +(Box3 a, Vector3 b)
        {
            return new Box3(a.Min + b, a.Max + b);
        }

        public static Box3 operator -(Box3 a, Vector3 b)
        {
            return new Box3(a.Min - b, a.Max - b);
        }

        public override string ToString()
        {
            return $"([{Min.X}, {Max.X}], [{Min.Y}, {Max.Y}], [{Min.Z}, {Max.Z}])";
        }
    }

    /// <summary>
    /// An axis-aligned bounding box in discrete two-dimensional space.
    /// </summary>
    public struct Box2i
    {
        public Vector2i Min;
        public Vector2i Max;
        public Box2i(Vector2i min, Vector2i max)
        {
            Min = min;
            Max = max;
        }

        public Box2i(int minX, int maxX, int minY, int maxY)
        {
            Min = new Vector2i(minX, minY);
            Max = new Vector2i(maxX, maxY);
        }

        /// <summary>
        /// Gets the box occupied by the given grid cell.
        /// </summary>
        public static Box2i Cell(Vector2i index)
        {
            return new Box2i(index, index + new Vector2i(1, 1));
        }

        /// <summary>
        /// The size of this box along both axes.
        /// </summary>
        public Index2 Size => (Index2)(Max - Min);

        /// <summary>
        /// Determines whether this box contains the given vector.
        /// </summary>
        public bool Contains(Vector2i vec)
        {
            return
                vec.X >= Min.X && vec.X < Max.X &&
                vec.Y >= Min.Y && vec.Y < Max.Y;
        }

        /// <summary>
        /// Gets the intersection of two boxes.
        /// </summary>
        public static Box2i Intersection(Box2i a, Box2i b)
        {
            // TODO: Check validity of resulting box
            return new Box2i(
                Math.Max(a.Min.X, b.Min.X), Math.Min(a.Max.X, b.Max.X),
                Math.Max(a.Min.Y, b.Min.Y), Math.Min(a.Max.Y, b.Max.Y));
        }

        public Vector2i MinX_MinY => Min;
        public Vector2i MaxX_MinY => new Vector2i(Max.X, Min.Y);
        public Vector2i MinX_MaxY => new Vector2i(Min.X, Max.Y);
        public Vector2i MaxX_MaxY => Max;

        public static Box2i operator +(Box2i a, Vector2i b)
        {
            return new Box2i(a.Min + b, a.Max + b);
        }

        public static Box2i operator -(Box2i a, Vector2i b)
        {
            return new Box2i(a.Min - b, a.Max - b);
        }

        public override string ToString()
        {
            return $"([{Min.X} .. {Max.X}), [{Min.Y} .. {Max.Y}))";
        }
    }

    /// <summary>
    /// An axis-aligned bounding box in discrete three-dimensional space.
    /// </summary>
    public struct Box3i
    {
        public Vector3i Min;
        public Vector3i Max;
        public Box3i(Vector3i min, Vector3i max)
        {
            Min = min;
            Max = max;
        }

        public Box3i(int minX, int maxX, int minY, int maxY, int minZ, int maxZ)
        {
            Min = new Vector3i(minX, minY, minZ);
            Max = new Vector3i(maxX, maxY, maxZ);
        }

        /// <summary>
        /// Gets the box occupied by the given grid cell.
        /// </summary>
        public static Box3i Cell(Vector3i index)
        {
            return new Box3i(index, index + new Vector3i(1, 1, 1));
        }

        /// <summary>
        /// A vector whose components are the size of this box along the different axes.
        /// </summary>
        public Vector3i Size
        {
            get
            {
                return Max - Min;
            }
        }

        /// <summary>
        /// Determines whether this box contains the given vector.
        /// </summary>
        public bool Contains(Vector3i vec)
        {
            return
                vec.X >= Min.X && vec.X < Max.X &&
                vec.Y >= Min.Y && vec.Y < Max.Y &&
                vec.Z >= Min.Z && vec.Z < Max.Z;
        }

        /// <summary>
        /// Gets the intersection of two boxes.
        /// </summary>
        public static Box3i Intersection(Box3i a, Box3i b)
        {
            return new Box3i(
                Math.Max(a.Min.X, b.Min.X), Math.Min(a.Max.X, b.Max.X),
                Math.Max(a.Min.Y, b.Min.Y), Math.Min(a.Max.Y, b.Max.Y),
                Math.Max(a.Min.Z, b.Min.Z), Math.Min(a.Max.Z, b.Max.Z));
        }

        public Vector3i MinX_MinY_MinZ => Min;
        public Vector3i MaxX_MinY_MinZ => new Vector3i(Max.X, Min.Y, Min.Z);
        public Vector3i MinX_MaxY_MinZ => new Vector3i(Min.X, Max.Y, Min.Z);
        public Vector3i MaxX_MaxY_MinZ => new Vector3i(Max.X, Max.Y, Min.Z);
        public Vector3i MinX_MinY_MaxZ => new Vector3i(Min.X, Min.Y, Max.Z);
        public Vector3i MaxX_MinY_MaxZ => new Vector3i(Max.X, Min.Y, Max.Z);
        public Vector3i MinX_MaxY_MaxZ => new Vector3i(Min.X, Max.Y, Max.Z);
        public Vector3i MaxX_MaxY_MaxZ => Max;

        public static Box3i operator +(Vector3i a, Box3i b)
        {
            return new Box3i(a + b.Min, a + b.Max);
        }

        public static Box3i operator +(Box3i a, Vector3i b)
        {
            return new Box3i(a.Min + b, a.Max + b);
        }

        public static Box3i operator -(Box3i a, Vector3i b)
        {
            return new Box3i(a.Min - b, a.Max - b);
        }

        public static implicit operator Box3(Box3i box)
        {
            // TODO: Respect half-open interval
            return new Box3(box.Min, box.Max);
        }

        public override string ToString()
        {
            return $"([{Min.X} .. {Max.X}), [{Min.Y} .. {Max.Y}), [{Min.Z} .. {Max.Z}))";
        }
    }
}