﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace Alunite.Data.Geometry
{

    /// <summary>
    /// A specialization of <see cref="Duet{T}"/> for boolean values.
    /// </summary>
    public struct BoolDuet
    {
        public BoolDuet(byte bitmap)
        {
            Bitmap = bitmap;
        }

        public BoolDuet(bool neg, bool pos)
        {
            Bitmap = 0;
            if (neg) Bitmap |= 0b01;
            if (pos) Bitmap |= 0b10;
        }

        /// <summary>
        /// A bit array with each bit corresponding to an <see cref="Dir1"/>.
        /// </summary>
        public byte Bitmap;

        /// <summary>
        /// Gets or sets the value for the given direction.
        /// </summary>
        public bool this[Dir1 dir]
        {
            get { return Bitwise.Get(Bitmap, (uint)dir); }
            set { Bitwise.Set(ref Bitmap, (uint)dir, value); }
        }

        /// <summary>
        /// Flips the components of this duet.
        /// </summary>
        public BoolDuet Flip()
        {
            return new BoolDuet(Pos, Neg);
        }

        public bool Neg
        {
            get { return Bitwise.Get(Bitmap, 0); }
            set { Bitwise.Set(ref Bitmap, 0, value); }
        }

        public bool Pos
        {
            get { return Bitwise.Get(Bitmap, 1); }
            set { Bitwise.Set(ref Bitmap, 1, value); }
        }

        public static implicit operator BoolDuet(Duet<bool> source)
        {
            return new BoolDuet(source.Neg, source.Pos);
        }

        public static implicit operator Duet<bool>(BoolDuet source)
        {
            return new Duet<bool>(source.Neg, source.Pos);
        }
    }

    /// <summary>
    /// A specialization of <see cref="Quartet{T}"/> for boolean values.
    /// </summary>
    public struct BoolQuartet
    {
        public BoolQuartet(byte bitmap)
        {
            Bitmap = bitmap;
        }

        public BoolQuartet(bool a, bool b, bool c, bool d)
        {
            Bitmap = 0;
            if (a) Bitmap |= 0b0001;
            if (b) Bitmap |= 0b0010;
            if (c) Bitmap |= 0b0100;
            if (d) Bitmap |= 0b1000;
        }

        /// <summary>
        /// Constructs a <see cref="BoolQuartet"/> with the given value for all quadrants.
        /// </summary>
        public static BoolQuartet Uniform(bool value)
        {
            return value ? new BoolQuartet(0b1111) : default;
        }

        /// <summary>
        /// A bit array with each bit corresponding to an <see cref="Quadrant"/>.
        /// </summary>
        public byte Bitmap;

        /// <summary>
        /// Gets or sets the value for the given quadrant.
        /// </summary>
        public bool this[Quadrant quad]
        {
            get { return Bitwise.Get(Bitmap, (uint)quad); }
            set { Bitwise.Set(ref Bitmap, (uint)quad, value); }
        }

        public bool NegX_NegY
        {
            get { return Bitwise.Get(Bitmap, 0); }
            set { Bitwise.Set(ref Bitmap, 0, value); }
        }

        public bool PosX_NegY
        {
            get { return Bitwise.Get(Bitmap, 1); }
            set { Bitwise.Set(ref Bitmap, 1, value); }
        }

        public bool NegX_PosY
        {
            get { return Bitwise.Get(Bitmap, 2); }
            set { Bitwise.Set(ref Bitmap, 2, value); }
        }

        public bool PosX_PosY
        {
            get { return Bitwise.Get(Bitmap, 3); }
            set { Bitwise.Set(ref Bitmap, 3, value); }
        }

        public static implicit operator BoolQuartet(Quartet<bool> source)
        {
            return new BoolQuartet(
                source.NegX_NegY,
                source.PosX_NegY,
                source.NegX_PosY,
                source.PosX_PosY);
        }

        public static implicit operator Quartet<bool>(BoolQuartet source)
        {
            return new Quartet<bool>(
                source.NegX_NegY,
                source.PosX_NegY,
                source.NegX_PosY,
                source.PosX_PosY);
        }
    }

    /// <summary>
    /// A specialization of <see cref="Octet{T}"/> for boolean values.
    /// </summary>
    public struct BoolOctet
    {
        public BoolOctet(byte bitmap)
        {
            Bitmap = bitmap;
        }

        public BoolOctet(bool a, bool b, bool c, bool d, bool e, bool f, bool g, bool h)
        {
            Bitmap = 0;
            if (a) Bitmap |= 0b00000001;
            if (b) Bitmap |= 0b00000010;
            if (c) Bitmap |= 0b00000100;
            if (d) Bitmap |= 0b00001000;
            if (e) Bitmap |= 0b00010000;
            if (f) Bitmap |= 0b00100000;
            if (g) Bitmap |= 0b01000000;
            if (h) Bitmap |= 0b10000000;
        }

        /// <summary>
        /// Constructs a <see cref="BoolOctet"/> with the given value for all octants.
        /// </summary>
        public static BoolOctet Uniform(bool value)
        {
            return value ? new BoolOctet(0b11111111) : default;
        }

        /// <summary>
        /// A bit array with each bit corresponding to an <see cref="Octant"/>.
        /// </summary>
        public byte Bitmap;

        /// <summary>
        /// Gets or sets the value for the given octant.
        /// </summary>
        public bool this[Octant octant]
        {
            get { return Bitwise.Get(Bitmap, (uint)octant); }
            set
            {
                int bit = 1 << (int)octant;
                Bitmap &= (byte)~bit;
                if (value) Bitmap |= (byte)bit;
            }
        }

        /// <summary>
        /// Constructs a <see cref="BoolOctet"/> that indicates which octants of the given octet have a value.
        /// </summary>
        public static BoolOctet IsSome<T>(Octet<Option<T>> octet)
        {
            return new BoolOctet(
                octet.NegX_NegY_NegZ.HasValue,
                octet.PosX_NegY_NegZ.HasValue,
                octet.NegX_PosY_NegZ.HasValue,
                octet.PosX_PosY_NegZ.HasValue,
                octet.NegX_NegY_PosZ.HasValue,
                octet.PosX_NegY_PosZ.HasValue,
                octet.NegX_PosY_PosZ.HasValue,
                octet.PosX_PosY_PosZ.HasValue);
        }

        public bool NegX_NegY_NegZ
        {
            get { return Bitwise.Get(Bitmap, 0); }
            set { Bitwise.Set(ref Bitmap, 0, value); }
        }

        public bool PosX_NegY_NegZ
        {
            get { return Bitwise.Get(Bitmap, 1); }
            set { Bitwise.Set(ref Bitmap, 1, value); }
        }

        public bool NegX_PosY_NegZ
        {
            get { return Bitwise.Get(Bitmap, 2); }
            set { Bitwise.Set(ref Bitmap, 2, value); }
        }

        public bool PosX_PosY_NegZ
        {
            get { return Bitwise.Get(Bitmap, 3); }
            set { Bitwise.Set(ref Bitmap, 3, value); }
        }

        public bool NegX_NegY_PosZ
        {
            get { return Bitwise.Get(Bitmap, 4); }
            set { Bitwise.Set(ref Bitmap, 4, value); }
        }

        public bool PosX_NegY_PosZ
        {
            get { return Bitwise.Get(Bitmap, 5); }
            set { Bitwise.Set(ref Bitmap, 5, value); }
        }

        public bool NegX_PosY_PosZ
        {
            get { return Bitwise.Get(Bitmap, 6); }
            set { Bitwise.Set(ref Bitmap, 6, value); }
        }

        public bool PosX_PosY_PosZ
        {
            get { return Bitwise.Get(Bitmap, 7); }
            set { Bitwise.Set(ref Bitmap, 7, value); }
        }

        public static implicit operator BoolOctet(Octet<bool> source)
        {
            return new BoolOctet(
                source.NegX_NegY_NegZ,
                source.PosX_NegY_NegZ,
                source.NegX_PosY_NegZ,
                source.PosX_PosY_NegZ,
                source.NegX_NegY_PosZ,
                source.PosX_NegY_PosZ,
                source.NegX_PosY_PosZ,
                source.PosX_PosY_PosZ);
        }

        public static implicit operator Octet<bool>(BoolOctet source)
        {
            return new Octet<bool>(
                source.NegX_NegY_NegZ,
                source.PosX_NegY_NegZ,
                source.NegX_PosY_NegZ,
                source.PosX_PosY_NegZ,
                source.NegX_NegY_PosZ,
                source.PosX_NegY_PosZ,
                source.NegX_PosY_PosZ,
                source.PosX_PosY_PosZ);
        }

        public static BoolOctet operator *(Roflection3i rof, BoolOctet octet)
        {
            return rof.Apply((Octet<bool>)octet);
        }
    }

    /// <summary>
    /// A specialization of <see cref="AxialDuet2{T}"/> for boolean values.
    /// </summary>
    public struct BoolAxisDuet2
    {
        public BoolAxisDuet2(byte bitmap)
        {
            Bitmap = bitmap;
        }

        public BoolAxisDuet2(bool negX, bool posX, bool negY, bool posY)
        {
            Bitmap = 0b0000;
            if (negX) Bitmap |= 0b0001;
            if (posX) Bitmap |= 0b0010;
            if (negY) Bitmap |= 0b0100;
            if (posY) Bitmap |= 0b1000;
        }

        /// <summary>
        /// A bit array with each bit corresponding to an <see cref="Dir2"/>.
        /// </summary>
        public byte Bitmap;

        /// <summary>
        /// Gets or sets a component of this <see cref="AxialDuet2{T}"/>
        /// </summary>
        public bool this[Dir2 dir]
        {
            get { return Bitwise.Get(Bitmap, (uint)dir); }
            set { Bitwise.Set(ref Bitmap, (uint)dir, value); }
        }

        public bool NegX
        {
            get { return Bitwise.Get(Bitmap, 0); }
            set { Bitwise.Set(ref Bitmap, 0, value); }
        }

        public bool PosX
        {
            get { return Bitwise.Get(Bitmap, 1); }
            set { Bitwise.Set(ref Bitmap, 1, value); }
        }

        public bool NegY
        {
            get { return Bitwise.Get(Bitmap, 2); }
            set { Bitwise.Set(ref Bitmap, 2, value); }
        }

        public bool PosY
        {
            get { return Bitwise.Get(Bitmap, 3); }
            set { Bitwise.Set(ref Bitmap, 3, value); }
        }

        public static implicit operator BoolAxisDuet2(AxialDuet2<bool> source)
        {
            return new BoolAxisDuet2(source.NegX, source.PosX, source.NegY, source.PosY);
        }

        public static implicit operator AxialDuet2<bool>(BoolAxisDuet2 source)
        {
            return new AxialDuet2<bool>(source.NegX, source.PosX, source.NegY, source.PosY);
        }
    }

    /// <summary>
    /// A specialization of <see cref="AxialDuet2{T}"/> for boolean values.
    /// </summary>
    public struct BoolAxisDuet3
    {
        public BoolAxisDuet3(byte bitmap)
        {
            Bitmap = bitmap;
        }

        public BoolAxisDuet3(bool negX, bool posX, bool negY, bool posY, bool negZ, bool posZ)
        {
            Bitmap = 0b000000;
            if (negX) Bitmap |= 0b000001;
            if (posX) Bitmap |= 0b000010;
            if (negY) Bitmap |= 0b000100;
            if (posY) Bitmap |= 0b001000;
            if (negZ) Bitmap |= 0b010000;
            if (posZ) Bitmap |= 0b100000;
        }

        /// <summary>
        /// A bit array with each bit corresponding to an <see cref="Dir3"/>.
        /// </summary>
        public byte Bitmap;

        /// <summary>
        /// Gets or sets a component of this <see cref="AxialDuet3{T}"/>
        /// </summary>
        public bool this[Dir3 dir]
        {
            get { return Bitwise.Get(Bitmap, (uint)dir); }
            set { Bitwise.Set(ref Bitmap, (uint)dir, value); }
        }

        /// <summary>
        /// The component of this <see cref="AxialDuet3{T}"/> on the XY plane
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public BoolAxisDuet2 X_Y
        {
            get
            {
                return new BoolAxisDuet2((byte)(Bitmap & 0b001111));
            }
            set
            {
                Bitmap &= 0b110000;
                Bitmap |= value.Bitmap;
            }
        }

        /// <summary>
        /// The component of this <see cref="AxialDuet3{T}"/> on the XZ plane
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public BoolAxisDuet2 X_Z
        {
            get
            {
                return new BoolAxisDuet2((byte)(((Bitmap & 0b110000) >> 2) | (Bitmap & 0b000011)));
            }
            set
            {
                Bitmap &= 0b001100;
                Bitmap |= (byte)(value.Bitmap & 0b0011);
                Bitmap |= (byte)((value.Bitmap & 0b1100) << 2);
            }
        }

        /// <summary>
        /// The component of this <see cref="AxialDuet3{T}"/> on the YZ plane
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public BoolAxisDuet2 Y_Z
        {
            get
            {
                return new BoolAxisDuet2((byte)((Bitmap & 0b111100) >> 2));
            }
            set
            {
                Bitmap &= 0b000011;
                Bitmap |= (byte)(value.Bitmap << 2);
            }
        }

        public bool NegX
        {
            get { return Bitwise.Get(Bitmap, 0); }
            set { Bitwise.Set(ref Bitmap, 0, value); }
        }

        public bool PosX
        {
            get { return Bitwise.Get(Bitmap, 1); }
            set { Bitwise.Set(ref Bitmap, 1, value); }
        }

        public bool NegY
        {
            get { return Bitwise.Get(Bitmap, 2); }
            set { Bitwise.Set(ref Bitmap, 2, value); }
        }

        public bool PosY
        {
            get { return Bitwise.Get(Bitmap, 3); }
            set { Bitwise.Set(ref Bitmap, 3, value); }
        }

        public bool NegZ
        {
            get { return Bitwise.Get(Bitmap, 4); }
            set { Bitwise.Set(ref Bitmap, 4, value); }
        }

        public bool PosZ
        {
            get { return Bitwise.Get(Bitmap, 5); }
            set { Bitwise.Set(ref Bitmap, 5, value); }
        }

        public static implicit operator BoolAxisDuet3(AxialDuet3<bool> source)
        {
            return new BoolAxisDuet3(source.NegX, source.PosX, source.NegY, source.PosY, source.NegZ, source.PosZ);
        }

        public static implicit operator AxialDuet3<bool>(BoolAxisDuet3 source)
        {
            return new AxialDuet3<bool>(source.NegX, source.PosX, source.NegY, source.PosY, source.NegZ, source.PosZ);
        }
    }

    /// <summary>
    /// A specialization of <see cref="AxialQuartet{T}"/> for boolean values.
    /// </summary>
    public struct BoolAxisQuartet : IEquatable<BoolAxisQuartet>
    {
        public BoolAxisQuartet(ushort bitmap)
        {
            Bitmap = bitmap;
        }

        public BoolAxisQuartet(
            bool negY_negZ, bool posY_negZ, bool negY_posZ, bool posY_posZ,
            bool negZ_negX, bool posZ_negX, bool negZ_posX, bool posZ_posX,
            bool negX_negY, bool posX_negY, bool negX_posY, bool posX_posY)
        {
            Bitmap = 0b0000_0000_0000;
            if (negY_negZ) Bitmap |= 0b0000_0000_0001;
            if (posY_negZ) Bitmap |= 0b0000_0000_0010;
            if (negY_posZ) Bitmap |= 0b0000_0000_0100;
            if (posY_posZ) Bitmap |= 0b0000_0000_1000;
            if (negZ_negX) Bitmap |= 0b0000_0001_0000;
            if (posZ_negX) Bitmap |= 0b0000_0010_0000;
            if (negZ_posX) Bitmap |= 0b0000_0100_0000;
            if (posZ_posX) Bitmap |= 0b0000_1000_0000;
            if (negX_negY) Bitmap |= 0b0001_0000_0000;
            if (posX_negY) Bitmap |= 0b0010_0000_0000;
            if (negX_posY) Bitmap |= 0b0100_0000_0000;
            if (posX_posY) Bitmap |= 0b1000_0000_0000;
        }

        /// <summary>
        /// A bit array with each bit corresponding to an <see cref="AxisQuadrant"/>.
        /// </summary>
        public ushort Bitmap;

        /// <summary>
        /// Constructs a <see cref="BoolAxisQuartet"/> with the given value for all quadrants.
        /// </summary>
        public static BoolAxisQuartet Uniform(bool value)
        {
            return value ? new BoolAxisQuartet(0b1111_1111_1111) : default;
        }

        /// <summary>
        /// Gets or sets a component of this <see cref="BoolAxisQuartet"/>.
        /// </summary>
        public bool this[AxisQuadrant quad]
        {
            get { return Bitwise.Get(Bitmap, (uint)quad); }
            set { Bitwise.Set(ref Bitmap, (uint)quad, value); }
        }
        
        public bool NegY_NegZ
        {
            get { return Bitwise.Get(Bitmap, 0); }
            set { Bitwise.Set(ref Bitmap, 0, value); }
        }

        public bool PosY_NegZ
        {
            get { return Bitwise.Get(Bitmap, 1); }
            set { Bitwise.Set(ref Bitmap, 1, value); }
        }

        public bool NegY_PosZ
        {
            get { return Bitwise.Get(Bitmap, 2); }
            set { Bitwise.Set(ref Bitmap, 2, value); }
        }

        public bool PosY_PosZ
        {
            get { return Bitwise.Get(Bitmap, 3); }
            set { Bitwise.Set(ref Bitmap, 3, value); }
        }

        public bool NegZ_NegX
        {
            get { return Bitwise.Get(Bitmap, 4); }
            set { Bitwise.Set(ref Bitmap, 4, value); }
        }
        
        public bool PosZ_NegX
        {
            get { return Bitwise.Get(Bitmap, 5); }
            set { Bitwise.Set(ref Bitmap, 5, value); }
        }
        
        public bool NegZ_PosX
        {
            get { return Bitwise.Get(Bitmap, 6); }
            set { Bitwise.Set(ref Bitmap, 6, value); }
        }

        public bool PosZ_PosX
        {
            get { return Bitwise.Get(Bitmap, 7); }
            set { Bitwise.Set(ref Bitmap, 7, value); }
        }

        public bool NegX_NegY
        {
            get { return Bitwise.Get(Bitmap, 8); }
            set { Bitwise.Set(ref Bitmap, 8, value); }
        }

        public bool PosX_NegY
        {
            get { return Bitwise.Get(Bitmap, 9); }
            set { Bitwise.Set(ref Bitmap, 9, value); }
        }

        public bool NegX_PosY
        {
            get { return Bitwise.Get(Bitmap, 10); }
            set { Bitwise.Set(ref Bitmap, 10, value); }
        }

        public bool PosX_PosY
        {
            get { return Bitwise.Get(Bitmap, 11); }
            set { Bitwise.Set(ref Bitmap, 11, value); }
        }
        
        public static implicit operator BoolAxisQuartet(AxialQuartet<bool> source)
        {
            return new BoolAxisQuartet(
                source.NegY_NegZ, source.PosY_NegZ, source.NegY_PosZ, source.PosY_PosZ,
                source.NegZ_NegX, source.PosZ_NegX, source.NegZ_PosX, source.PosZ_PosX,
                source.NegX_NegY, source.PosX_NegY, source.NegX_PosY, source.PosX_PosY);
        }

        public static implicit operator AxialQuartet<bool>(BoolAxisQuartet source)
        {
            return new AxialQuartet<bool>(
                source.NegY_NegZ, source.PosY_NegZ, source.NegY_PosZ, source.PosY_PosZ,
                source.NegZ_NegX, source.PosZ_NegX, source.NegZ_PosX, source.PosZ_PosX,
                source.NegX_NegY, source.PosX_NegY, source.NegX_PosY, source.PosX_PosY);
        }

        public static BoolAxisQuartet operator &(BoolAxisQuartet a, BoolAxisQuartet b)
        {
            return new BoolAxisQuartet((ushort)(a.Bitmap & b.Bitmap));
        }

        public static BoolAxisQuartet operator |(BoolAxisQuartet a, BoolAxisQuartet b)
        {
            return new BoolAxisQuartet((ushort)(a.Bitmap | b.Bitmap));
        }

        public static BoolAxisQuartet operator *(Roflection3i rof, BoolAxisQuartet axisQuad)
        {
            return rof.Apply((AxialQuartet<bool>)axisQuad);
        }

        public static bool operator ==(BoolAxisQuartet a, BoolAxisQuartet b)
        {
            return a.Bitmap == b.Bitmap;
        }

        public static bool operator !=(BoolAxisQuartet a, BoolAxisQuartet b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is BoolAxisQuartet))
                return false;
            return this == (BoolAxisQuartet)obj;
        }

        bool IEquatable<BoolAxisQuartet>.Equals(BoolAxisQuartet other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return Bitmap.GetHashCode();
        }
    }
}