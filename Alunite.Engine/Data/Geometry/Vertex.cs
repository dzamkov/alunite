﻿using System;
using System.Collections.Generic;

namespace Alunite.Data.Geometry
{
    /// <summary>
    /// A vertex with two-dimensional position information.
    /// </summary>
    public interface IPosition2
    {
        /// <summary>
        /// The position component of this vertex.
        /// </summary>
        Vector2 Position { get; }
    }

    /// <summary>
    /// A vertex with three-dimensional position information.
    /// </summary>
    public interface IPosition3
    {
        /// <summary>
        /// The position component of this vertex.
        /// </summary>
        Vector3 Position { get; }
    }

    /// <summary>
    /// Data for a vertex that includes two-dimensional position and normal information.
    /// </summary>
    public struct Position2_Normal : IPosition2
    {
        Vector2 IPosition2.Position => Position;
        public Position2_Normal(Vector2 pos, Vector2 norm)
        {
            Position = pos;
            Normal = norm;
        }

        /// <summary>
        /// The position component of this vertex.
        /// </summary>
        public Vector2 Position;

        /// <summary>
        /// The normal component of this vertex.
        /// </summary>
        public Vector2 Normal;
    }

    /// <summary>
    /// Data for a vertex that includes three-dimensional position and normal information.
    /// </summary>
    public struct Position3_Normal : IPosition3
    {
        Vector3 IPosition3.Position => Position;
        public Position3_Normal(Vector3 pos, Vector3 norm)
        {
            Position = pos;
            Normal = norm;
        }

        /// <summary>
        /// The position component of this vertex.
        /// </summary>
        public Vector3 Position;

        /// <summary>
        /// The normal component of this vertex.
        /// </summary>
        public Vector3 Normal;
    }
}
