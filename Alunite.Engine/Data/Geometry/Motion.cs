﻿using System;

using Alunite.Data.Probability;

namespace Alunite.Data.Geometry
{
    /// <summary>
    /// A coordinate transformation in two-dimensional space composed of rotation and translation. This is
    /// also known as a "direct isometry".
    /// </summary>
    public struct Motion2
    {
        public Motion2(Rotation2 linear, Vector2 offset)
        {
            Linear = linear;
            Offset = offset;
        }

        /// <summary>
        /// The linear component of this motion.
        /// </summary>
        public Rotation2 Linear;

        /// <summary>
        /// The translation component of this motion.
        /// </summary>
        public Vector2 Offset;

        /// <summary>
        /// The identity motion.
        /// </summary>
        public static Motion2 Identity => new Motion2(Rotation2.Identity, Vector2.Zero);

        /// <summary>
        /// Constructs a motion which translates by the given amount.
        /// </summary>
        public static Motion2 Translate(Vector2 offset)
        {
            return new Motion2(Rotation2.Identity, offset);
        }

        /// <summary>
        /// Constructs a motion which translates by the given amount.
        /// </summary>
        public static Motion2 Translate(Scalar x, Scalar y)
        {
            return Translate(new Vector2(x, y));
        }

        /// <summary>
        /// Indicates whether this motion is close enough to <see cref="Identity"/> such that it is likely
        /// represent it after accounting for rounding error incurred during arithmetic operations.
        /// </summary>
        /// <param name="range">The maximum absolute translation component value expected between
        /// <paramref name="a"/> and <paramref name="b"/>.</param>
        public bool IsApproxIdentity(Scalar range)
        {
            return AreApproxEqual(range, this, Identity);
        }

        /// <summary>
        /// Gets the inverse of this motion.
        /// </summary>
        public Motion2 Inverse
        {
            get
            {
                Rotation2 linearInv = Linear.Inverse;
                return new Motion2(linearInv, linearInv.Apply(-Offset));
            }
        }

        /// <summary>
        /// Applies this motion to a position.
        /// </summary>
        public Vector2 Apply(Vector2 pos)
        {
            return Linear.Apply(pos) + Offset;
        }
        
        /// <summary>
        /// Composes the given motions, first applying the right, then the left.
        /// </summary>
        public static Motion2 Compose(Motion2 a, Motion2 b)
        {
            return new Motion2(Rotation2.Compose(a.Linear, b.Linear), a.Apply(b.Offset));
        }

        /// <summary>
        /// Indicates whether the given values are close enough that they likely represent identical
        /// transformations after accounting for rounding error incurred during arithmetic operations.
        /// </summary>
        /// <param name="range">The maximum absolute translation component value expected between
        /// <paramref name="a"/> and <paramref name="b"/>.</param>
        public static bool AreApproxEqual(Scalar range, Motion2 a, Motion2 b)
        {
            // TODO: Replace with AreEqualOdds method
            return Rotation2.AreLikelyEqual(a.Linear, b.Linear) && 
                Vector2.AreEqualOdds(a.Offset, b.Offset) > 1;
        }

        public static implicit operator Motion2(Rotation2 source)
        {
            return new Motion2(source, Vector2.Zero);
        }
        
        public static explicit operator Motion2(Affine2 source)
        {
            return new Motion2((Rotation2)source.Linear, source.Offset);
        }

        public static Vector2 operator *(Motion2 a, Vector2 b)
        {
            return a.Apply(b);
        }
        
        public static Motion2 operator *(Motion2 a, Motion2 b)
        {
            return Compose(a, b);
        }
    }

    /// <summary>
    /// A coordinate transformation in three-dimensional space composed of rotation and translation. This is
    /// also known as a "direct isometry".
    /// </summary>
    public struct Motion3
    {
        internal Motion3(Rotation3 linear, Vector3 offset)
        {
            Linear = linear;
            Offset = offset;
        }

        /// <summary>
        /// The linear component of this motion.
        /// </summary>
        public Rotation3 Linear;

        /// <summary>
        /// The translation component of this motion.
        /// </summary>
        public Vector3 Offset;

        /// <summary>
        /// The identity motion.
        /// </summary>
        public static Motion3 Identity => new Motion3(Rotation3.Identity, Vector3.Zero);

        /// <summary>
        /// Constructs a motion which translates by the given amount.
        /// </summary>
        public static Motion3 Translate(Vector3 offset)
        {
            return new Motion3(Rotation3.Identity, offset);
        }

        /// <summary>
        /// Constructs a motion which translates by the given amount.
        /// </summary>
        public static Motion3 Translate(Scalar x, Scalar y, Scalar z)
        {
            return Translate(new Vector3(x, y, z));
        }

        /// <summary>
        /// Constructs a motion for a rotation of the given angle about the given axis, following
        /// the right-hand rule.
        /// </summary>
        public static Motion3 RotationAbout(Vector3 axis, Scalar angle)
        {
            return Rotation3.About(axis, angle);
        }

        /// <summary>
        /// Constructs a world-to-view transform for the situation where the camera is at a given position and looking
        /// at the given target.
        /// </summary>
        public static Motion3 LookAt(Vector3 eye, Vector3 target, Vector3 up)
        {
            Vector3 forward = Vector3.Normalize(target - eye);
            Rotation3 linear = Rotation3.LookAt(forward, up);
            return new Motion3(linear, linear.Apply(-eye));
        }

        /// <summary>
        /// Gets the inverse of this motion.
        /// </summary>
        public Motion3 Inverse
        {
            get
            {
                Rotation3 linearInv = Linear.Inverse;
                return new Motion3(linearInv, linearInv.Apply(-Offset));
            }
        }

        /// <summary>
        /// Applies this motion to a vector representing a position.
        /// </summary>
        public Vector3 Apply(Vector3 pos)
        {
            return Linear.Apply(pos) + Offset;
        }

        /// <summary>
        /// Applies this motion to a vector representing a direction.
        /// </summary>
        public Vector3 ApplyDirection(Vector3 dir)
        {
            return Linear.Apply(dir);
        }

        /// <summary>
        /// Composes the given motions, first applying the right, then the left.
        /// </summary>
        public static Motion3 Compose(Motion3 a, Motion3 b)
        {
            return new Motion3(Rotation3.Compose(a.Linear, b.Linear), a.Apply(b.Offset));
        }

        /// <summary>
        /// Determines whether the given <see cref="Motion3"/>s likely have the same true value
        /// after compensating for representation error.
        /// </summary>
        public static bool AreLikelyEqual(Motion3 a, Motion3 b)
        {
            return AreEqualOdds(a, b) > 1;
        }

        /// <summary>
        /// Determines the relative probability that the given <see cref="Motion3"/>s have the same
        /// true value, over the probability that their true values are independent.
        /// </summary>
        public static LogScalar AreEqualOdds(Motion3 a, Motion3 b)
        {
            return Vector3.AreEqualOdds(a.Offset, b.Offset) * Rotation3.AreEqualOdds(a.Linear, b.Linear);
        }
        
        public static implicit operator Motion3(Rotation3 linear)
        {
            return new Motion3(linear, Vector3.Zero);
        }

        public static implicit operator Matrix3x4(Motion3 trans)
        {
            Matrix3x3 linear = trans.Linear;
            return new Matrix3x4(linear.X, linear.Y, linear.Z, trans.Offset);
        }

        public static implicit operator Matrix4x4(Motion3 trans)
        {
            Matrix3x3 linear = trans.Linear;
            return new Matrix4x4(
                new Vector4(linear.X, 0),
                new Vector4(linear.Y, 0),
                new Vector4(linear.Z, 0),
                new Vector4(trans.Offset, 1));
        }

        public static Vector3 operator *(Motion3 a, Vector3 b)
        {
            return a.Apply(b);
        }

        public static Vector4 operator *(Motion3 a, Vector4 b)
        {
            return new Vector4(a.Linear * b.X_Y_Z + a.Offset * b.W, b.W);
        }

        public static Motion3 operator *(Motion3 a, Motion3 b)
        {
            return Compose(a, b);
        }
    }
}