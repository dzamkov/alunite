﻿using System;
using System.Diagnostics;

namespace Alunite.Data
{
    /// <summary>
    /// A quaternion.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct Quaternion
    {
        // Store B_C_D first so that it is more likely to be aligned.
        [DebuggerBrowsable(DebuggerBrowsableState.Never)] public Vector3 B_C_D;
        public Scalar A;
        public Quaternion(Scalar a, Vector3 b_c_d)
        {
            B_C_D = b_c_d;
            A = a;
        }

        public Quaternion(Scalar a, Scalar b, Scalar c, Scalar d)
        {
            B_C_D = new Vector3(b, c, d);
            A = a;
        }

        public Scalar B
        {
            get { return B_C_D.X; }
            set { B_C_D.X = value; }
        }

        public Scalar C
        {
            get { return B_C_D.Y; }
            set { B_C_D.Y = value; }
        }

        public Scalar D
        {
            get { return B_C_D.Z; }
            set { B_C_D.Z = value; }
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Vector4 B_C_D_A
        {
            get { return new Vector4(B_C_D, A); }
            set { B_C_D = value.X_Y_Z; A = value.W; }
        }

        /// <summary>
        /// The magnitude of this quaternion.
        /// </summary>
        public Scalar Magnitude => Scalar.Sqrt(SqrMagnitude);

        /// <summary>
        /// The square of <see cref="Magnitude"/>.
        /// </summary>
        public Scalar SqrMagnitude => A * A + B_C_D.SqrLength;

        /// <summary>
        /// The sign of the first non-zero component of the quaternion, or 0 if the quaternion
        /// itself is zero.
        /// </summary>
        public int Sign
        {
            get
            {
                if (A > 0) return 1;
                else if (A < 0) return -1;
                else if (B > 0) return 1;
                else if (B < 0) return -1;
                else if (C > 0) return 1;
                else if (C < 0) return -1;
                else if (D > 0) return 1;
                else if (D < 0) return -1;
                else return 0;
            }
        }

        /// <summary>
        /// Normalizes a quaternion.
        /// </summary>
        public static Quaternion Normalize(Quaternion quat)
        {
            Scalar invLen = 1 / quat.Magnitude;
            return new Quaternion(quat.A * invLen, quat.B_C_D * invLen);
        }
        
        public static Quaternion operator -(Quaternion a)
        {
            return new Quaternion(-a.A, -a.B_C_D);
        }

        public static Quaternion operator +(Quaternion a, Quaternion b)
        {
            return new Quaternion(a.A + b.A, a.B_C_D + b.B_C_D);
        }

        public static Quaternion operator -(Quaternion a, Quaternion b)
        {
            return new Quaternion(a.A - b.A, a.B_C_D - b.B_C_D);
        }
        
        public static Quaternion operator *(Scalar a, Quaternion b)
        {
            return b * a;
        }

        public static Quaternion operator *(Quaternion a, Scalar b)
        {
            return new Quaternion(a.A * b, a.B_C_D * b);
        }
        
        public static Quaternion operator *(Quaternion a, Quaternion b)
        {
            return new Quaternion(
                a.A * b.A - Vector3.Dot(a.B_C_D, b.B_C_D),
                a.A * b.B_C_D + b.A * a.B_C_D + Vector3.Cross(a.B_C_D, b.B_C_D));
        }

        public override string ToString()
        {
            string b = (B >= 0 ? " + " + B.ToString() : " - " + (-B).ToString()) + "i";
            string c = (C >= 0 ? " + " + C.ToString() : " - " + (-C).ToString()) + "j";
            string d = (D >= 0 ? " + " + D.ToString() : " - " + (-D).ToString()) + "k";
            return A.ToString() + b + c + d;
        }
    }
}
