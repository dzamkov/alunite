﻿using System;
using System.Runtime.CompilerServices;

using Alunite.Data.Serialization;

namespace Alunite.Data
{
    /// <summary>
    /// A helper class for reading characters from a <see cref="string"/> with a streaming interface.
    /// </summary>
    public sealed class StringReader : ISeekableBufferedListReader<char>
    {
        private StringReader(string str)
        {
            Source = str;
            Index = 0;
        }

        /// <summary>
        /// The <see cref="string"/> being read by this <see cref="StringReader"/>.
        /// </summary>
        public string Source { get; }

        /// <summary>
        /// The index of the next character in <see cref="Source"/>.
        /// </summary>
        public uint Index;

        /// <summary>
        /// Creates a <see cref="StringReader"/> which reads from the given string.
        /// </summary>
        public static StringReader Create(string str)
        {
            return new StringReader(str);
        }

        /// <summary>
        /// Reads the next characters of the string into <paramref name="buffer"/>, limited by either the length of the
        /// buffer or the string.
        /// </summary>
        /// <returns>The number of characters read. This will be less than the length of <paramref name="buffer"/> iff the
        /// end of the string has been reached.</returns>
        public uint Read(Span<char> buffer)
        {
            uint numRem = (uint)Source.Length - Index;
            if (numRem < (uint)buffer.Length) buffer = buffer.Slice(0, (int)numRem);
            Source.AsSpan((int)Index, buffer.Length).CopyTo(buffer);
            Index += (uint)buffer.Length;
            return (uint)buffer.Length;
        }

        /// <summary>
        /// Advances the reader by the given number of characters.
        /// </summary>
        public void Skip(uint num)
        {
            Index += num;
        }
        
        SpanList<char> IBufferedListReader<char>.Buffer
        {
            get
            {
                return new SpanList<char>(Source.AsSpan((int)Index));
            }
        }

        ulong ISeekableListReader<char>.Position
        {
            get
            {
                return Index;
            }
            set
            {
                Index = (uint)value;
            }
        }

        ulong IBacktrackingListReader<char, ulong>.Mark()
        {
            return Index;
        }

        bool IBacktrackingListReader<char, ulong>.Reset(ulong point)
        {
            Index = (uint)point;
            return true;
        }
    }

    /// <summary>
    /// Contains helper functions for writing string content to a <see cref="IListWriter{T}"/>.
    /// </summary>
    public static class StringWriter
    {
        /// <summary>
        /// Appends the given string to this writer.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Write(this IListWriter<char> writer, string str)
        {
            writer.Write(str.AsSpan());
        }

        /// <summary>
        /// Appends the given character to this writer.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static unsafe void WriteItem(this IListWriter<char> writer, char ch)
        {
            writer.Write(new ReadOnlySpan<char>(&ch, 1));
        }

        /// <summary>
        /// Appends the escaped form of the given string to this writer.
        /// </summary>
        public static void WriteEscaped(this IListWriter<char> writer, ReadOnlySpan<char> chs)
        {
            // TODO: Additional special characters
            int startIndex = 0;
            int scanIndex = 0;
        next:
            if (scanIndex < chs.Length)
            {
                char ch = chs[scanIndex];
                if (ch == '\"' || ch == '\'' || ch == '\\')
                {
                    // Flush scanned characters
                    int len = scanIndex - startIndex;
                    if (len > 0) writer.Write(chs.Slice(startIndex, len));

                    // Append escape sequence
                    writer.WriteItem('\\');
                    writer.WriteItem(ch);

                    // Restart scan from next character
                    scanIndex++;
                    startIndex = scanIndex;
                    goto next;
                }
                else
                {
                    scanIndex++;
                    goto next;
                }
            }
            else
            {
                // Flush remaining characters
                writer.Write(chs.Slice(startIndex));
            }
        }

        /// <summary>
        /// Appends the escaped form of the given string to this writer.
        /// </summary>
        public static void WriteEscaped(this IListWriter<char> writer, string str)
        {
            writer.WriteEscaped(str.AsSpan());
        }

        /// <summary>
        /// Appends the escaped form of the given character to this writer.
        /// </summary>
        public static void WriteEscapedItem(this IListWriter<char> writer, char ch)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// A helper class for constructing a <see cref="string"/> by incrementally appending characters.
    /// </summary>
    public struct StringBuilder : IListWriter<char>, IBuilder<string>
    {
        private System.Text.StringBuilder _target;
        private StringBuilder(System.Text.StringBuilder target)
        {
            _target = target;
        }

        /// <summary>
        /// Creates a new initially-empty <see cref="StringBuilder"/>.
        /// </summary>
        public static StringBuilder CreateEmpty()
        {
            return new StringBuilder(new System.Text.StringBuilder());
        }

        /// <summary>
        /// The length of the string written so far.
        /// </summary>
        public uint Length => (uint)_target.Length;

        /// <summary>
        /// Appends the given characters to the string.
        /// </summary>
        public unsafe void Write(ReadOnlySpan<char> chs)
        {
            // TODO: Replace with Span overload once that's available.
            fixed (char* chsStart = chs)
                _target.Append(chsStart, chs.Length);
        }

        /// <summary>
        /// Gets the string that was constructed using this <see cref="StringBuilder"/>.
        /// </summary>
        public string Finish()
        {
            return _target.ToString();
        }
    }

    /// <summary>
    /// An interface for reading characters from a <see cref="IBoundedBinaryReader"/> using UTF-8
    /// encoding.
    /// </summary>
    public struct Utf8Decoder : IListReader<char>
    {
        public Utf8Decoder(IBoundedBinaryReader source)
        {
            Source = source;
        }

        /// <summary>
        /// The <see cref="IBoundedBinaryReader"/> this <see cref="Utf8Decoder"/> reads from.
        /// </summary>
        public IBoundedBinaryReader Source;

        /// <summary>
        /// Reads the next items of the list into <paramref name="buffer"/>, limited by either the length of the
        /// buffer or the number of remaining items in the list.
        /// </summary>
        /// <returns>The number of items read. This will be less than the length of <paramref name="buffer"/> iff the
        /// end of the list has been reached.</returns>
        public uint Read(Span<char> buffer)
        {
            return _read(Source, buffer);
        }

        /// <summary>
        /// Advances the list reader by the given number of items, stopping at the end of the list.
        /// </summary>
        public void Skip(uint num)
        {
            // TODO: Make more performant
            Span<char> buffer = stackalloc char[(int)num];
            _read(Source, buffer);
        }

        /// <summary>
        /// Reads the next items of the list into <paramref name="buffer"/>, limited by either the length of the
        /// buffer or the number of remaining items in the list.
        /// </summary>
        internal static uint _read(IBoundedBinaryReader source, Span<char> buffer)
        {
            if (buffer.Length == 0)
                return 0;
            uint numRead = 0;
            uint numWrite = 0;
            ReadOnlySpan<byte> sourceBuffer = source.Buffer;

        next:
            {
                if (sourceBuffer.Length < 4)
                {
                    // Populate more bytes
                    source.Skip(numRead);
                    numRead = 0;
                    if (source.PrepareMax())
                    {
                        sourceBuffer = source.Buffer;
                        goto nextFinal;
                    }
                    else
                    {
                        sourceBuffer = source.Buffer;
                    }
                }

                // Decode next character
                byte b0 = sourceBuffer[0];
                if (b0 < 0b10000000)
                {
                    numRead++;
                    buffer[(int)numWrite++] = (char)b0;
                    sourceBuffer = sourceBuffer.Slice(1);
                }
                else if (b0 < 0b11100000)
                {
                    numRead += 2;
                    byte b1 = sourceBuffer[1];
                    uint pt = (uint)(
                        (b0 & 0b00011111) << 6 |
                        (b1 & 0b00111111));
                    buffer[(int)numWrite++] = (char)pt;
                    sourceBuffer = sourceBuffer.Slice(2);
                }
                else if (b0 < 0b11110000)
                {
                    numRead += 3;
                    byte b1 = sourceBuffer[1];
                    byte b2 = sourceBuffer[2];
                    uint pt = (uint)(
                        (b0 & 0b00001111) << 12 |
                        (b1 & 0b00111111) << 6 |
                        (b2 & 0b00111111));
                    buffer[(int)numWrite++] = (char)pt;
                    sourceBuffer = sourceBuffer.Slice(3);
                }
                else if (b0 < 0b11111000)
                {
                    numRead += 4;
                    byte b1 = sourceBuffer[1];
                    byte b2 = sourceBuffer[2];
                    byte b3 = sourceBuffer[3];
                    uint pt = (uint)(
                        (b0 & 0b00000111) << 18 |
                        (b1 & 0b00111111) << 12 |
                        (b2 & 0b00111111) << 6 |
                        (b3 & 0b00111111));
                    buffer[(int)numWrite++] = (char)pt;
                    sourceBuffer = sourceBuffer.Slice(4);
                }
                else
                {
                    // Stop decoding upon seeing an invalid sequence
                    goto done;
                }

                // Are we done?
                if (numWrite < (uint)buffer.Length)
                    goto next;
                else
                    goto done;
            }

        nextFinal:
            {
                // Has the source been exhausted?
                if (sourceBuffer.Length == 0)
                    goto done;

                // Decode next character
                byte b0 = sourceBuffer[0];
                if (b0 < 0b10000000)
                {
                    numRead++;
                    buffer[(int)numWrite++] = (char)b0;
                    sourceBuffer = sourceBuffer.Slice(1);
                }
                else if (b0 < 0b11100000)
                {
                    numRead += 2;
                    byte b1 = sourceBuffer[1];
                    throw new NotImplementedException();
                }
                else if (b0 < 0b11110000)
                {
                    numRead += 3;
                    byte b1 = sourceBuffer[1];
                    byte b2 = sourceBuffer[2];
                    uint pt = (uint)(
                        (b0 & 0b00001111) << 12 |
                        (b1 & 0b00111111) << 6 |
                        (b2 & 0b00111111));
                    buffer[(int)numWrite++] = (char)pt;
                    sourceBuffer = sourceBuffer.Slice(3);
                }
                else
                {
                    throw new NotImplementedException();
                }

                // Are we done?
                if (numWrite < (uint)buffer.Length)
                    goto nextFinal;
                else
                    goto done;
            }

        done:
            source.Skip(numRead);
            return numWrite;
        }
    }

    /// <summary>
    /// A variant of <see cref="Utf8Decoder"/> which makes use of a <see cref="ISeekableBinaryReader"/> to provide
    /// backtracking capability.
    /// </summary>
    public struct Utf8BacktrackingDecoder : IBacktrackingListReader<char, ulong>
    {
        public Utf8BacktrackingDecoder(ISeekableBinaryReader source)
        {
            Source = source;
        }

        /// <summary>
        /// The <see cref="ISeekableBinaryReader"/> this <see cref="Utf8BacktrackingDecoder"/>
        /// reads from.
        /// </summary>
        public ISeekableBinaryReader Source;

        /// <summary>
        /// Gets or sets the position of the next unread item within the underlying data source.
        /// </summary>
        public ulong Position
        {
            get
            {
                return Source.Position;
            }
            set
            {
                Source.Position = value;
            }
        }
        
        /// <summary>
        /// Reads the next items of the list into <paramref name="buffer"/>, limited by either the length of the
        /// buffer or the number of remaining items in the list.
        /// </summary>
        /// <returns>The number of items read. This will be less than the length of <paramref name="buffer"/> iff the
        /// end of the list has been reached.</returns>
        public uint Read(Span<char> buffer)
        {
            return Utf8Decoder._read(Source, buffer);
        }

        /// <summary>
        /// Advances the list reader by the given number of items, stopping at the end of the list.
        /// </summary>
        public void Skip(uint num)
        {
            // TODO: Make more performant
            Span<char> buffer = stackalloc char[(int)num];
            Utf8Decoder._read(Source, buffer);
        }

        ulong IBacktrackingListReader<char, ulong>.Mark()
        {
            return Source.Position;
        }

        bool IBacktrackingListReader<char, ulong>.Reset(ulong point)
        {
            Source.Position = point;
            return true;
        }
    }

    /// <summary>
    /// Contains helper functions for decoding character data from binary streams.
    /// </summary>
    public static class StringDecoder
    {
        /// <summary>
        /// Constructs a <see cref="Utf8Decoder"/> which reads characters from a <see cref="IBinaryReader"/> using
        /// UTF-8 encoding. The decoder must be disposed after use and should have exclusive use of the
        /// <see cref="IBinaryReader"/> until disposed.
        /// </summary>
        public static IDisposable DecodeUtf8(this IBoundedBinaryReader reader, out Utf8Decoder decoder)
        {
            decoder = new Utf8Decoder(reader);
            return null;
        }

        /// <summary>
        /// Constructs a <see cref="Utf8Decoder"/> which reads characters from a <see cref="IBinaryReader"/> using
        /// UTF-8 encoding. The decoder must be disposed after use and should have exclusive use of the
        /// <see cref="IBinaryReader"/> until disposed.
        /// </summary>
        public static IDisposable DecodeUtf8(this ISeekableBinaryReader reader, out Utf8BacktrackingDecoder decoder)
        {
            decoder = new Utf8BacktrackingDecoder(reader);
            return null;
        }
    }
}
