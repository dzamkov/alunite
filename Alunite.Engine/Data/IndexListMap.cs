﻿using System;
using System.Diagnostics;

namespace Alunite.Data
{
    /// <summary>
    /// A specialization of <see cref="Map{TKey, TValue}"/> whose keys are fixed-length lists of small
    /// <see cref="uint"/> indices.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    [DebuggerTypeProxy(typeof(RegularIndexListMapDebugView<>))]
    public struct RegularIndexListMap<T> : IEquatable<RegularIndexListMap<T>>
    {
        internal RegularIndexListMap(uint keyLen, _Internal source)
        {
            KeyLength = keyLen;
            _source = source;
        }

        /// <summary>
        /// The length of the keys used in this map, or <see cref="uint.MaxValue"/> if this map is empty.
        /// </summary>
        public uint KeyLength { get; }

        /// <summary>
        /// The underlying map for this <see cref="RegularIndexListMap{T}"/>.
        /// </summary>
        internal _Internal _source;

        /// <summary>
        /// The empty <see cref="RegularIndexListMap{T}"/>.
        /// </summary>
        public static RegularIndexListMap<T> Empty => new RegularIndexListMap<T>(uint.MaxValue, _Internal.Empty);

        /// <summary>
        /// Constructs a map whose only entry is the empty key associated with the given value.
        /// </summary>
        public static RegularIndexListMap<T> SingletonEmpty(T value)
        {
            return new RegularIndexListMap<T>(0, new _Internal(value));
        }

        /// <summary>
        /// Constructs a map whose only entry has the given key and value.
        /// </summary>
        public static RegularIndexListMap<T> Singleton(ReadOnlySpan<uint> key, T value)
        {
            return new RegularIndexListMap<T>((uint)key.Length, _Internal.Singleton(key, value));
        }

        /// <summary>
        /// Constructs a <see cref="RegularIndexListMap{T}"/> over lists whose first item is an
        /// index in <paramref name="cases"/> and whose entries are determined by the map at
        /// that index, using the remainder of the list as the key.
        /// </summary>
        public static RegularIndexListMap<T> Switch(ReadOnlySpan<RegularIndexListMap<T>> cases)
        {
            uint last = (uint)cases.Length;
            while (last > 0)
            {
                if (!cases[(int)--last].IsEmpty)
                    goto foundLast;
            }
            return Empty;

        foundLast:
            uint numCases = last + 1;
            uint keyLen = cases[(int)last].KeyLength;
            _Case[] nCases = new _Case[numCases];
            for (int i = 0; i < nCases.Length; i++)
            {
                RegularIndexListMap<T> @case = cases[i];
                if (@case.IsEmpty)
                {
                    nCases[i] = _Case.Empty;
                }
                else
                {
                    if (@case.KeyLength != keyLen)
                        throw new ArgumentException("All cases must have the same length", nameof(cases));
                    nCases[i] = _Case.Build(@case._source);
                }
            }
            return new RegularIndexListMap<T>(keyLen + 1, new _Internal(nCases));
        }

        /// <summary>
        /// Indicates whether this is <see cref="Empty"/>.
        /// </summary>
        public bool IsEmpty => KeyLength == uint.MaxValue;

        /// <summary>
        /// Indicates whether this is a <see cref="SingletonEmpty"/> map.
        /// </summary>
        public bool IsSingletonEmpty(out T value)
        {
            if (KeyLength == 0)
            {
                value = (T)_source._def;
                return true;
            }
            value = default;
            return false;
        }

        /// <summary>
        /// Indicates whether this is a <see cref="Singleton"/> map.
        /// </summary>
        public bool IsSingleton(out List<uint> key, out T value)
        {
            Span<uint> keyItems = stackalloc uint[(int)KeyLength];
            if (_source.TryWriteSingleton(keyItems, out value))
            {
                key = List.Of(keyItems);
                return true;
            }
            key = default;
            return false;
        }

        /// <summary>
        /// Determines whether the keys in this map are non-empty, and if so, deconstructs the map
        /// as a <see cref="Switch"/>.
        /// </summary>
        public bool IsSwitch(out List<RegularIndexListMap<T>> cases)
        {
            if (unchecked(KeyLength + 1) > 1)
            {
                uint nKeyLen = KeyLength - 1;
                _Case[] sourceCases = (_Case[])_source._def;
                RegularIndexListMap<T>[] items = new RegularIndexListMap<T>[sourceCases.Length];
                for (int i = 0; i < sourceCases.Length; i++)
                {
                    _Case sourceCase = sourceCases[i];
                    if (sourceCase.IsEmpty)
                        items[i] = Empty;
                    else
                        items[i] = new RegularIndexListMap<T>(nKeyLen, sourceCase.Tail);
                }
                cases = new List<RegularIndexListMap<T>>(items);
                return true;
            }
            cases = default;
            return false;
        }

        /// <summary>
        /// Gets the value corresponding to the given key, assuming it exists.
        /// </summary>
        public T this[List<uint> key]
        {
            get
            {
                if (!TryGet(key, out T value))
                    throw new System.Collections.Generic.KeyNotFoundException();
                return value;
            }
        }

        /// <summary>
        /// Determines whether there is an entry for the given key in this map, and if so, gets the
        /// corresponding value.
        /// </summary>
        public bool TryGet(ReadOnlySpan<uint> key, out T value)
        {
            return _source.TryGet(key, out value);
        }

        /// <summary>
        /// The number of entries in this map, capped to <see cref="uint.MaxValue"/>.
        /// </summary>
        public uint Size
        {
            get
            {
                uint size = 0;
                if (_source.IsEmpty)
                    return size;
                var cache = new MapBuilder<_Internal, uint>();
                _source._computeSize(cache, ref size);
                return size;
            }
        }

        /// <summary>
        /// The internal recursive definition of <see cref="RegularIndexListMap{T}"/>, which doesn't store the
        /// length at each node.
        /// </summary>
        internal struct _Internal : IEquatable<_Internal>
        {
            internal _Internal(T value)
            {
                _def = value;
            }

            internal _Internal(_Case[] cases)
            {
                _def = cases;
            }

            /// <summary>
            /// The table or value defining this map. If the map is empty, this will be <see cref="null"/>. If the key
            /// length is non-zero, this will be an array of <see cref="_Case"/>s. If the key length is zero, this will
            /// contain the value for the empty list.
            /// </summary>
            internal object _def;
            
            /// <summary>
            /// The empty map.
            /// </summary>
            public static _Internal Empty => default;

            /// <summary>
            /// Constructs a <see cref="_Internal"/> whose only entry has the given key and value.
            /// </summary>
            public static _Internal Singleton(ReadOnlySpan<uint> keyItems, T value)
            {
                if (keyItems.Length == 0)
                {
                    return new _Internal(value);
                }
                else
                {
                    uint keyItem = keyItems[0];
                    _Case[] cases = new _Case[keyItem + 1];
                    for (uint i = 0; i < keyItem; i++)
                        cases[i] = _Case.Empty;
                    cases[keyItem] = _Case.Build(Singleton(keyItems.Slice(1), value));
                    return new _Internal(cases);
                }
            }

            /// <summary>
            /// Indicates whether this is <see cref="Empty"/>.
            /// </summary>
            public bool IsEmpty => _def is null;

            /// <summary>
            /// Determines whether there is an entry for the given key in this map, and if so, gets the
            /// corresponding value.
            /// </summary>
            public bool TryGet(ReadOnlySpan<uint> keyItems, out T value)
            {
                _Internal map = this;
            next:
                if (!(map._def is null))
                {
                    if (keyItems.Length == 0)
                    {
                        value = (T)map._def;
                        return true;
                    }
                    else
                    {
                        uint item = keyItems[0];
                        _Case[] cases = (_Case[])map._def;
                        if (item < (uint)cases.Length)
                        {
                            map = cases[item].Tail;
                            keyItems = keyItems.Slice(1);
                            goto next;
                        }
                    }
                }
                value = default;
                return false;
            }

            /// <summary>
            /// The number of entries in this map, capped to <see cref="uint.MaxValue"/>.
            /// </summary>
            public uint Size
            {
                get
                {
                    if (IsEmpty)
                        return 0;
                    var cache = new MapBuilder<_Internal, uint>();
                    uint size = 0;
                    _computeSize(cache, ref size);
                    return size;
                }
            }

            /// <summary>
            /// Computes the size of this map.
            /// </summary>
            internal void _computeSize(MapBuilder<_Internal, uint> cache, ref uint size)
            {
                if (!(_def is null))
                {
                    if (_def is _Case[] cases)
                    {
                        if (cache.TryGet(this, out uint thisSize))
                        {
                            size = Saturate.Add(size, thisSize);
                        }
                        else
                        {
                            uint startSize = size;
                            for (int i = 0; i < cases.Length; i++)
                            {
                                cases[i].Tail._computeSize(cache, ref size);
                                if (size == uint.MaxValue)
                                    return;
                            }
                            thisSize = size - startSize;
                            cache.Add(this, thisSize);
                        }
                    }
                    else if (size < uint.MaxValue)
                    {
                        size++;
                    }
                }
            }

            /// <summary>
            /// If this <see cref="_Internal"/> contains just one entry, writes the items of the key to
            /// <paramref name="keyItems"/> and gets the value. Otherwise, returns false.
            /// </summary>
            public bool TryWriteSingleton(Span<uint> keyItems, out T value)
            {
                _Internal map = this;
                if (map._def is null)
                {
                    value = default;
                    return false;
                }
            next:
                if (keyItems.Length == 0)
                {
                    value = (T)map._def;
                    return true;
                }
                else
                {
                    _Case[] cases = (_Case[])map._def;
                    uint last = (uint)cases.Length - 1;
                    for (uint i = 0; i < last; i++)
                    {
                        if (!cases[i].IsEmpty)
                        {
                            value = default;
                            return false;
                        }
                    }
                    map = cases[last].Tail;
                    keyItems[0] = last;
                    keyItems = keyItems.Slice(1);
                    goto next;
                }
            }

            /// <summary>
            /// Determines whether two <see cref="_Internal"/>s are equivalent without shortcutting based on
            /// reference.
            /// </summary>
            public static bool AreEqual(_Internal a, _Internal b)
            {
                if (a._def is null)
                    return b._def is null;
                if (b._def is null)
                    return false;
                if (a._def is _Case[] aCases)
                {
                    if (!(b._def is _Case[] bCases))
                        return false;
                    if (aCases.Length != bCases.Length)
                        return false;
                    for (int i = 0; i < aCases.Length; i++)
                        if (!_Case.AreEqual(ref aCases[i], ref bCases[i]))
                            return false;
                    return true;
                }
                else
                {
                    T aValue = (T)a._def;
                    if (!(b._def is T bValue))
                        return false;
                    return EqualityHelper.Equals(aValue, bValue);
                }
            }

            public static bool operator ==(_Internal a, _Internal b)
            {
                if (ReferenceEquals(a._def, b._def))
                    return true;
                return AreEqual(a, b);
            }

            public static bool operator !=(_Internal a, _Internal b)
            {
                return !(a == b);
            }

            public override bool Equals(object obj)
            {
                if (!(obj is _Internal other))
                    return false;
                return this == other;
            }

            bool IEquatable<_Internal>.Equals(_Internal other)
            {
                return this == other;
            }

            public override int GetHashCode()
            {
                if (_def is null)
                {
                    return 0;
                }
                else if (_def is _Case[] cases)
                {
                    int hash = 0x79db86f3;
                    for (int i = 0; i < cases.Length; i++)
                        hash = HashCodeHelper.Combine(hash, cases[i].Hash);
                    return hash;
                }
                else
                {
                    return EqualityHelper.GetHashCode(_def);
                }
            }
        }

        /// <summary>
        /// Describes the contents of an <see cref="RegularIndexListMap{T}"/> assuming a particular value for the
        /// first item.
        /// </summary>
        internal struct _Case
        {
            private _Case(int hash, _Internal tail)
            {
                Hash = hash;
                Tail = tail;
            }

            /// <summary>
            /// Builds a <see cref="_Case"/> from the given map.
            /// </summary>
            public static _Case Build(_Internal tail)
            {
                return new _Case(tail.GetHashCode(), tail);
            }

            /// <summary>
            /// The hash code for this case.
            /// </summary>
            public int Hash { get; }

            /// <summary>
            /// Describes the remaining items of the map.
            /// </summary>
            public _Internal Tail;

            /// <summary>
            /// The <see cref="_Case"/> corresponding to <see cref="Empty"/>.
            /// </summary>
            public static _Case Empty => default;

            /// <summary>
            /// Indicates whether this is <see cref="Empty"/>.
            /// </summary>
            public bool IsEmpty => Tail.IsEmpty;

            /// <summary>
            /// Determines whether two <see cref="_Case"/>s are the same. If so, this may coalesce them to make that
            /// determination faster in the future.
            /// </summary>
            public static bool AreEqual(ref _Case a, ref _Case b)
            {
                if (ReferenceEquals(a.Tail._def, b.Tail._def))
                    return true;
                if (a.Hash != b.Hash)
                    return false;
                if (_Internal.AreEqual(a.Tail, b.Tail))
                {
                    Util.Coalesce(ref a.Tail._def, ref b.Tail._def);
                    return true;
                }
                return false;
            }
        }

        public static bool operator ==(RegularIndexListMap<T> a, RegularIndexListMap<T> b)
        {
            return a.KeyLength == b.KeyLength && a._source == b._source;
        }

        public static bool operator !=(RegularIndexListMap<T> a, RegularIndexListMap<T> b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is RegularIndexListMap<T>))
                return false;
            return this == (RegularIndexListMap<T>)obj;
        }

        bool IEquatable<RegularIndexListMap<T>>.Equals(RegularIndexListMap<T> other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return HashCodeHelper.Combine(KeyLength.GetHashCode(), _source.GetHashCode());
        }

        public override string ToString()
        {
            if (IsEmpty)
            {
                return "∅";
            }
            else if (IsSingleton(out List<uint> key, out T value))
            {
                return "{" + key.ToString() + " : " + value.ToString() + "}";
            }
            else
            {
                uint size = Size;
                if (size == uint.MaxValue)
                    return "{[... <" + KeyLength.ToStringInvariant() + ">] : ...}";
                else
                    return "{[... <" + KeyLength.ToStringInvariant() + ">] : ... <" + size.ToStringInvariant() + ">}";
            }
        }
    }

    /// <summary>
    /// Contains functions related to <see cref="RegularIndexListMap{T}"/>.
    /// </summary>
    public struct RegularIndexListMap
    {
        /// <summary>
        /// Constructs an empty <see cref="RegularIndexListMap{T}"/>.
        /// </summary>
        public static RegularIndexListMap<T> Empty<T>()
        {
            return RegularIndexListMap<T>.Empty;
        }

        /// <summary>
        /// Constructs a map whose only entry is the empty key associated with the given value.
        /// </summary>
        public static RegularIndexListMap<T> SingletonEmpty<T>(T value)
        {
            return RegularIndexListMap<T>.SingletonEmpty(value);
        }

        /// <summary>
        /// Constructs a map whose only entry has the given key and value.
        /// </summary>
        public static RegularIndexListMap<T> Singleton<T>(ReadOnlySpan<uint> key, T value)
        {
            return RegularIndexListMap<T>.Singleton(key, value);
        }
    }

    /// <summary>
    /// A debugger type proxy for <see cref="RegularIndexListMap{T}"/>s.
    /// </summary>
    public sealed class RegularIndexListMapDebugView<T>
    {
        private readonly RegularIndexListMap<T> _source;
        public RegularIndexListMapDebugView(RegularIndexListMap<T> source)
        {
            _source = source;
        }

        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public object Content
        {
            get
            {
                if (_source.KeyLength == 0)
                {
                    return _source._source._def;
                }
                else if (_source._source._def is RegularIndexListMap<T>._Case[] cases)
                {
                    uint nKeyLen = _source.KeyLength - 1;
                    RegularIndexListMap<T>[] nCases = new RegularIndexListMap<T>[cases.Length];
                    for (int i = 0; i < nCases.Length; i++)
                    {
                        if (cases[i].IsEmpty)
                            nCases[i] = RegularIndexListMap<T>.Empty;
                        else
                            nCases[i] = new RegularIndexListMap<T>(nKeyLen, cases[i].Tail);
                    }
                    return nCases;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
