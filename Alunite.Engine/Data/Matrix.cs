﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using Numerics = System.Numerics;

using Alunite.Data.Probability;

namespace Alunite.Data
{
    /// <summary>
    /// A 2x2 matrix.
    /// </summary>
    public struct Matrix2x2 : IEquatable<Matrix2x2>
    {
        public Vector2 X;
        public Vector2 Y;
        public Matrix2x2(Vector2 x, Vector2 y)
        {
            X = x;
            Y = y;
        }

        /// <summary>
        /// The zero matrix.
        /// </summary>
        public static Matrix2x2 Zero => default;

        /// <summary>
        /// The identity matrix.
        /// </summary>
        public static Matrix2x2 Identity => new Matrix2x2(
            new Vector2(1, 0),
            new Vector2(0, 1));

        /// <summary>
        /// Transposes this matrix.
        /// </summary>
        public Matrix2x2 Transpose
        {
            get
            {
                return new Matrix2x2(
                    new Vector2(X.X, Y.X),
                    new Vector2(X.Y, Y.Y));
            }
        }

        /// <summary>
        /// The inverse of this matrix.
        /// </summary>
        public Matrix2x2 Inverse
        {
            get
            {
                Scalar det = X.X * Y.Y - X.Y * Y.X;
                return new Matrix2x2(
                    new Vector2(Y.Y, -X.Y),
                    new Vector2(-Y.X, X.X)) / det;
            }
        }

        /// <summary>
        /// Indicates whether the "true" value of this matrix is likely to be <see cref="Zero"/>
        /// after accounting for rounding error incurred during arithmetic operations.
        /// </summary>
        public bool IsLikelyZero => AreLikelyEqual(Zero, this); // TODO: Specialize for performance

        /// <summary>
        /// Indicates whether the "true" value of this matrix is likely to be <see cref="Identity"/>
        /// after accounting for rounding error incurred during arithmetic operations.
        /// </summary>
        public bool IsLikelyIdentity => AreLikelyEqual(Identity, this); // TODO: Specialize for performance

        /// <summary>
        /// Determines whether the given <see cref="Matrix2x2"/>s likely have the same true value
        /// after compensating for representation error.
        /// </summary>
        public static bool AreLikelyEqual(Matrix2x2 a, Matrix2x2 b)
        {
            return AreEqualOdds(a, b) > 1;
        }

        /// <summary>
        /// Determines the relative probability that the given <see cref="Matrix2x2"/>s have the same true value, over
        /// the probability that their true values are independent. This is based on the apriori distribution of
        /// matrix values, and individual value distributions of <paramref name="a"/> and <paramref name="b"/>.
        /// </summary>
        public static LogScalar AreEqualOdds(Matrix2x2 a, Matrix2x2 b)
        {
            // See Scalar.AreEqualOdds for explanation
            float aSqrMeanX = Numerics.Vector2.Dot(a.X._mean, a.X._mean);
            float aSqrMeanY = Numerics.Vector2.Dot(a.Y._mean, a.Y._mean);
            float aSqrMean = aSqrMeanX + aSqrMeanY;
            float bSqrMeanX = Numerics.Vector2.Dot(b.X._mean, b.X._mean);
            float bSqrMeanY = Numerics.Vector2.Dot(b.Y._mean, b.Y._mean);
            float bSqrMean = bSqrMeanX + bSqrMeanY;
            float aVar = Scalar._beta + Scalar._gamma * aSqrMean;
            float bVar = Scalar._beta + Scalar._gamma * bSqrMean;
            float aPriorFactor = Scalar._alpha + aSqrMean;
            float bPriorFactor = Scalar._alpha + bSqrMean;
            Numerics.Vector2 diffX = a.X._mean - b.X._mean;
            Numerics.Vector2 diffY = a.Y._mean - b.Y._mean;
            float sqrDiffX = Numerics.Vector2.Dot(diffX, diffX);
            float sqrDiffY = Numerics.Vector2.Dot(diffY, diffY);
            float sqrDiff = sqrDiffX + sqrDiffY;
            return new LogScalar(
                4f * (Scalar._lnNormalNorm - Scalar._lnPriorNorm)
                + sqrDiff / (-2f * (aVar + bVar))
                - Scalar._ln((aVar + bVar) / (aPriorFactor * bPriorFactor)) * (4f / 2f));
        }

        public static Matrix2x2 operator -(Matrix2x2 a)
        {
            return new Matrix2x2(-a.X, -a.Y);
        }

        public static Matrix2x2 operator +(Matrix2x2 a, Matrix2x2 b)
        {
            return new Matrix2x2(a.X + b.X, a.Y + b.Y);
        }

        public static Matrix2x2 operator -(Matrix2x2 a, Matrix2x2 b)
        {
            return new Matrix2x2(a.X - b.X, a.Y - b.Y);
        }
        
        public static Matrix2x2 operator *(Scalar a, Matrix2x2 b)
        {
            return b * a;
        }
        
        public static Matrix2x2 operator *(Matrix2x2 a, Scalar b)
        {
            return new Matrix2x2(a.X * b, a.Y * b);
        }
        
        public static Vector2 operator *(Matrix2x2 a, Vector2 b)
        {
            return a.X * b.X + a.Y * b.Y;
        }
        
        public static Matrix2x2 operator *(Matrix2x2 a, Matrix2x2 b)
        {
            return new Matrix2x2(a * b.X, a * b.Y);
        }

        public static Matrix2x2 operator /(Matrix2x2 a, Scalar b)
        {
            return a * (1 / b);
        }

        public static bool operator ==(Matrix2x2 a, Matrix2x2 b)
        {
            return a.X == b.X && a.Y == b.Y;
        }

        public static bool operator !=(Matrix2x2 a, Matrix2x2 b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Matrix2x2))
                return false;
            return this == (Matrix2x2)obj;
        }

        bool IEquatable<Matrix2x2>.Equals(Matrix2x2 other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return HashCodeHelper.Combine(X.GetHashCode(), Y.GetHashCode());
        }
    }

    /// <summary>
    /// A 2x3 matrix.
    /// </summary>
    public struct Matrix2x3
    {
        public Vector2 X;
        public Vector2 Y;
        public Vector2 Z;
        public Matrix2x3(Vector2 x, Vector2 y, Vector2 z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public Matrix2x3(Matrix2x2 x_y, Vector2 z)
        {
            X = x_y.X;
            Y = x_y.Y;
            Z = z;
        }

        /// <summary>
        /// Transposes this matrix.
        /// </summary>
        public Matrix3x2 Transpose
        {
            get
            {
                return new Matrix3x2(
                    new Vector3(X.X, Y.X, Z.X),
                    new Vector3(X.Y, Y.Y, Z.Y));
            }
        }

        public static Matrix2x3 operator *(Scalar a, Matrix2x3 b)
        {
            return b * a;
        }
        
        public static Matrix2x3 operator *(Matrix2x3 a, Scalar b)
        {
            return new Matrix2x3(a.X * b, a.Y * b, a.Z * b);
        }
        
        public static Vector2 operator *(Matrix2x3 a, Vector3 b)
        {
            return a.X * b.X + a.Y * b.Y + a.Z * b.Z;
        }
        
        public static Matrix2x3 operator *(Matrix2x2 a, Matrix2x3 b)
        {
            return new Matrix2x3(a * b.X, a * b.Y, a * b.Z);
        }
    }

    /// <summary>
    /// A 3x2 matrix.
    /// </summary>
    public struct Matrix3x2
    {
        public Vector3 X;
        public Vector3 Y;
        public Matrix3x2(Vector3 x, Vector3 y)
        {
            X = x;
            Y = y;
        }

        /// <summary>
        /// Transposes this matrix.
        /// </summary>
        public Matrix2x3 Transpose
        {
            get
            {
                return new Matrix2x3(
                    new Vector2(X.X, Y.X),
                    new Vector2(X.Y, Y.Y),
                    new Vector2(X.Z, Y.Z));
            }
        }

        public static Matrix3x2 operator *(Scalar a, Matrix3x2 b)
        {
            return b * a;
        }
        
        public static Matrix3x2 operator *(Matrix3x2 a, Scalar b)
        {
            return new Matrix3x2(a.X * b, a.Y * b);
        }
        
        public static Vector3 operator *(Matrix3x2 a, Vector2 b)
        {
            return a.X * b.X + a.Y * b.Y;
        }
    }

    /// <summary>
    /// A 3x3 matrix.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct Matrix3x3
    {
        public Vector3 X;
        public Vector3 Y;
        public Vector3 Z;
        public Matrix3x3(Vector3 x, Vector3 y, Vector3 z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        /// <summary>
        /// The zero matrix.
        /// </summary>
        public static Matrix3x3 Zero => default;

        /// <summary>
        /// The identity matrix.
        /// </summary>
        public static Matrix3x3 Identity => new Matrix3x3(
            new Vector3(1, 0, 0),
            new Vector3(0, 1, 0),
            new Vector3(0, 0, 1));

        /// <summary>
        /// Transposes this matrix.
        /// </summary>
        public Matrix3x3 Transpose
        {
            get
            {
                return new Matrix3x3(
                    new Vector3(X.X, Y.X, Z.X),
                    new Vector3(X.Y, Y.Y, Z.Y),
                    new Vector3(X.Z, Y.Z, Z.Z));
            }
        }

        /// <summary>
        /// The inverse of this matrix.
        /// </summary>
        public Matrix3x3 Inverse
        {
            get
            {
                DeterminantCofactor(out Scalar det, out Matrix3x3 cof);
                return (1 / det) * cof.Transpose;
            }
        }
        
        /// <summary>
        /// The determinant of this matrix.
        /// </summary>
        public Scalar Determinant
        {
            get
            {
                DeterminantCofactor(out Scalar det, out _);
                return det;
            }
        }

        /// <summary>
        /// The square of <see cref="Length"/>.
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Scalar SqrLength
        {
            get
            {
                return X.SqrLength + Y.SqrLength + Z.SqrLength;
            }
        }

        /// <summary>
        /// The Frobenius norm of this matrix.
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Scalar Length
        {
            get
            {
                return Scalar.Sqrt(SqrLength);
            }
        }

        /// <summary>
        /// Computes the determinant and cofactor matrix for this matrix.
        /// </summary>
        public void DeterminantCofactor(out Scalar det, out Matrix3x3 cof)
        {
            Scalar xx = Y.Y * Z.Z - Z.Y * Y.Z;
            Scalar yx = Z.Y * X.Z - X.Y * Z.Z;
            Scalar zx = X.Y * Y.Z - Y.Y * X.Z;
            Scalar xy = Y.Z * Z.X - Z.Z * Y.X;
            Scalar yy = Z.Z * X.X - X.Z * Z.X;
            Scalar zy = X.Z * Y.X - Y.Z * X.X;
            Scalar xz = Y.X * Z.Y - Z.X * Y.Y;
            Scalar yz = Z.X * X.Y - X.X * Z.Y;
            Scalar zz = X.X * Y.Y - Y.X * X.Y;
            det = X.X * xx + Y.X * yx + Z.X * zx;
            cof = new Matrix3x3(
                new Vector3(xx, xy, xz),
                new Vector3(yx, yy, yz),
                new Vector3(zx, zy, zz));
        }

        /// <summary>
        /// Indicates whether the "true" value of this matrix is likely to be <see cref="Zero"/>
        /// after accounting for rounding error incurred during arithmetic operations.
        /// </summary>
        public bool IsLikelyZero => AreLikelyEqual(Zero, this); // TODO: Specialize for performance

        /// <summary>
        /// Indicates whether the "true" value of this matrix is likely to be <see cref="Identity"/>
        /// after accounting for rounding error incurred during arithmetic operations.
        /// </summary>
        public bool IsLikelyIdentity => AreLikelyEqual(Identity, this); // TODO: Specialize for performance

        /// <summary>
        /// Determines whether the given <see cref="Matrix3x3"/>s likely have the same true value
        /// after compensating for representation error.
        /// </summary>
        public static bool AreLikelyEqual(Matrix3x3 a, Matrix3x3 b)
        {
            return AreEqualOdds(a, b) > 1;
        }

        /// <summary>
        /// Determines the relative probability that the given <see cref="Matrix3x3"/>s have the same true value, over
        /// the probability that their true values are independent. This is based on the apriori distribution of
        /// matrix values, and individual value distributions of <paramref name="a"/> and <paramref name="b"/>.
        /// </summary>
        public static LogScalar AreEqualOdds(Matrix3x3 a, Matrix3x3 b)
        {
            // See Scalar.AreEqualOdds for explanation
            float aSqrMeanX = Numerics.Vector3.Dot(a.X._mean, a.X._mean);
            float aSqrMeanY = Numerics.Vector3.Dot(a.Y._mean, a.Y._mean);
            float aSqrMeanZ = Numerics.Vector3.Dot(a.Z._mean, a.Z._mean);
            float aSqrMean = aSqrMeanX + aSqrMeanY + aSqrMeanZ;
            float bSqrMeanX = Numerics.Vector3.Dot(b.X._mean, b.X._mean);
            float bSqrMeanY = Numerics.Vector3.Dot(b.Y._mean, b.Y._mean);
            float bSqrMeanZ = Numerics.Vector3.Dot(b.Z._mean, b.Z._mean);
            float bSqrMean = bSqrMeanX + bSqrMeanY + bSqrMeanZ;
            float aVar = Scalar._beta + Scalar._gamma * aSqrMean;
            float bVar = Scalar._beta + Scalar._gamma * bSqrMean;
            float aPriorFactor = Scalar._alpha + aSqrMean;
            float bPriorFactor = Scalar._alpha + bSqrMean;
            Numerics.Vector3 diffX = a.X._mean - b.X._mean;
            Numerics.Vector3 diffY = a.Y._mean - b.Y._mean;
            Numerics.Vector3 diffZ = a.Z._mean - b.Z._mean;
            float sqrDiffX = Numerics.Vector3.Dot(diffX, diffX);
            float sqrDiffY = Numerics.Vector3.Dot(diffY, diffY);
            float sqrDiffZ = Numerics.Vector3.Dot(diffZ, diffZ);
            float sqrDiff = sqrDiffX + sqrDiffY + sqrDiffZ;
            return new LogScalar(
                9f * (Scalar._lnNormalNorm - Scalar._lnPriorNorm)
                + sqrDiff / (-2f * (aVar + bVar))
                - Scalar._ln((aVar + bVar) / (aPriorFactor * bPriorFactor)) * (9f / 2f));
        }

        public static Matrix3x3 operator -(Matrix3x3 a)
        {
            return new Matrix3x3(-a.X, -a.Y, -a.Z);
        }

        public static Matrix3x3 operator +(Matrix3x3 a, Matrix3x3 b)
        {
            return new Matrix3x3(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        }

        public static Matrix3x3 operator -(Matrix3x3 a, Matrix3x3 b)
        {
            return new Matrix3x3(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        }

        public static Matrix3x3 operator *(Scalar a, Matrix3x3 b)
        {
            return b * a;
        }
        
        public static Matrix3x3 operator *(Matrix3x3 a, Scalar b)
        {
            return new Matrix3x3(a.X * b, a.Y * b, a.Z * b);
        }
        
        public static Matrix3x3 operator /(Matrix3x3 a, Scalar b)
        {
            return a * (1 / b);
        }

        public static Vector3 operator *(Matrix3x3 a, Vector3 b)
        {
            return a.X * b.X + a.Y * b.Y + a.Z * b.Z;
        }
        
        public static Matrix3x3 operator *(Matrix3x3 a, Matrix3x3 b)
        {
            return new Matrix3x3(a * b.X, a * b.Y, a * b.Z);
        }
    }

    /// <summary>
    /// A 3x4 matrix.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct Matrix3x4
    {
        public Vector3 X;
        public Vector3 Y;
        public Vector3 Z;
        public Vector3 W;
        public Matrix3x4(Vector3 x, Vector3 y, Vector3 z, Vector3 w)
        {
            X = x;
            Y = y;
            Z = z;
            W = w;
        }

        public static Matrix3x4 operator *(Scalar a, Matrix3x4 b)
        {
            return b * a;
        }
        
        public static Matrix3x4 operator *(Matrix3x4 a, Scalar b)
        {
            return new Matrix3x4(a.X * b, a.Y * b, a.Z * b, a.W * b);
        }
        
        public static Vector3 operator *(Matrix3x4 a, Vector4 b)
        {
            return a.X * b.X + a.Y * b.Y + a.Z * b.Z + a.W * b.W;
        }
    }

    /// <summary>
    /// A 4x3 matrix.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct Matrix4x3
    {
        public Vector4 X;
        public Vector4 Y;
        public Vector4 Z;
        public Matrix4x3(Vector4 x, Vector4 y, Vector4 z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        /// <summary>
        /// Transposes this matrix.
        /// </summary>
        public Matrix3x4 Transpose
        {
            get
            {
                return new Matrix3x4(
                    new Vector3(X.X, Y.X, Z.X),
                    new Vector3(X.Y, Y.Y, Z.Y),
                    new Vector3(X.Z, Y.Z, Z.Z),
                    new Vector3(X.W, Y.W, Z.W));
            }
        }

        public static Matrix4x3 operator *(Scalar a, Matrix4x3 b)
        {
            return b * a;
        }

        public static Matrix4x3 operator *(Matrix4x3 a, Scalar b)
        {
            return new Matrix4x3(a.X * b, a.Y * b, a.Z * b);
        }

        public static Vector4 operator *(Matrix4x3 a, Vector3 b)
        {
            return a.X * b.X + a.Y * b.Y + a.Z * b.Z;
        }

        public static Matrix3x3 operator *(Matrix3x4 a, Matrix4x3 b)
        {
            return new Matrix3x3(a * b.X, a * b.Y, a * b.Z);
        }
    }

    /// <summary>
    /// A 4x4 matrix.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct Matrix4x4
    {
        public Vector4 X;
        public Vector4 Y;
        public Vector4 Z;
        public Vector4 W;
        public Matrix4x4(Vector4 x, Vector4 y, Vector4 z, Vector4 w)
        {
            X = x;
            Y = y;
            Z = z;
            W = w;
        }

        /// <summary>
        /// The identity matrix.
        /// </summary>
        public static Matrix4x4 Identity => new Matrix4x4(
            new Vector4(1, 0, 0, 0),
            new Vector4(0, 1, 0, 0),
            new Vector4(0, 0, 1, 0),
            new Vector4(0, 0, 0, 1));

        public static Matrix4x4 operator *(Scalar a, Matrix4x4 b)
        {
            return b * a;
        }
        
        public static Matrix4x4 operator *(Matrix4x4 a, Scalar b)
        {
            return new Matrix4x4(a.X * b, a.Y * b, a.Z * b, a.W * b);
        }
        
        public static Vector4 operator *(Matrix4x4 a, Vector4 b)
        {
            return a.X * b.X + a.Y * b.Y + a.Z * b.Z + a.W * b.W;
        }
        
        public static Matrix4x4 operator *(Matrix4x4 a, Matrix4x4 b)
        {
            return new Matrix4x4(a * b.X, a * b.Y, a * b.Z, a * b.W);
        }
    }

    /// <summary>
    /// Contains functions related to matrices.
    /// </summary>
    public static class Matrix
    {
        /// <summary>
        /// Constructs a matrix by "stacking" <paramref name="top"/> on top of the
        /// row vector <paramref name="bottom"/>.
        /// </summary>
        public static Matrix4x3 Stack(Matrix3x3 top, Vector3 bottom)
        {
            return new Matrix4x3(
                new Vector4(top.X, bottom.X),
                new Vector4(top.Y, bottom.Y),
                new Vector4(top.Z, bottom.Z));
        }

        /// <summary>
        /// Constructs the outer product of <paramref name="a"/> and <paramref name="b"/>.
        /// </summary>
        public static Matrix2x2 Outer(Vector2 a, Vector2 b)
        {
            return new Matrix2x2(a * b.X, a * b.Y);
        }

        /// <summary>
        /// Constructs the outer product of <paramref name="a"/> and <paramref name="b"/>.
        /// </summary>
        public static Matrix3x3 Outer(Vector3 a, Vector3 b)
        {
            return new Matrix3x3(a * b.X, a * b.Y, a * b.Z);
        }

        /// <summary>
        /// Constructs a diagonal "scaling" matrix from the given values.
        /// </summary>
        public static Matrix3x3 Diagonal(Vector3 d)
        {
            return new Matrix3x3(
                new Vector3(d.X, 0, 0),
                new Vector3(0, d.Y, 0),
                new Vector3(0, 0, d.Z));
        }
        
        /// <summary>
        /// Computes a singular value decomposition of <paramref name="mat"/> as
        /// '<paramref name="u"/> * diag(<paramref name="s"/>) * <paramref name="v"/>^T', where
        /// <paramref name="u"/> and <paramref name="v"/> are orthonormal and <paramref name="s"/> contains
        /// the non-negative singular values of <paramref name="mat"/> in decreasing order.
        /// </summary>
        public static void Svd(this Matrix3x3 mat, out Matrix3x3 u, out Vector3 s, out Matrix3x3 v)
        {
            // Zero out Y and Z rows of the X column of mat
            Matrix3x3 u1 = HouseholderAnnihilate(mat.X);
            u = u1;
            mat = u1 * mat;
            mat.X.Y_Z = Vector2.Zero;

            // Zero out the X row of the Z column of mat
            Matrix2x2 sv1 = HouseholderAnnihilate(new Vector2(mat.Y.X, mat.Z.X));
            Matrix3x3 v1 = new Matrix3x3(new Vector3(1, 0, 0), new Vector3(0, sv1.X), new Vector3(0, sv1.Y));
            v = v1;
            mat = mat * v1;
            mat.Z.X = Scalar.Zero;

            // Zero out the Z row of the Y column of mat
            Matrix2x2 su2 = HouseholderAnnihilate(mat.Y.Y_Z);
            Matrix3x3 u2 = new Matrix3x3(new Vector3(1, 0, 0), new Vector3(0, su2.X), new Vector3(0, su2.Y));
            u = u * u2;
            mat = u2 * mat;
            mat.Y.Z = Scalar.Zero;

            // Compute SVD for remaining bidiagonal matrix
            s = new Vector3(mat.X.X, mat.Y.Y, mat.Z.Z);
            Vector2 e = new Vector2(mat.Y.X, mat.Z.Y);
            _svdBidiagonal(ref u, ref s, e, ref v);
        }
        
        /// <summary>
        /// Finds an orthonormal involutory matrix which, when applied to <paramref name="vec"/>,
        /// zeros out its Y components.
        /// </summary>
        public static Matrix2x2 HouseholderAnnihilate(Vector2 vec)
        {
            float len = vec.Length._mean;
            if (len == 0)
            {
                // Any reflection matrix will do
                return new Matrix2x2(
                    new Vector2(-1, 0),
                    new Vector2(0, 1));
            }
            else if (vec.X >= 0)
            {
                float d = vec.X._mean + len;
                Vector2 w = new Vector2(1, new Scalar(vec.Y._mean / d));
                Scalar t = new Scalar(d / len);
                return Matrix2x2.Identity - Outer(t * w, w);
            }
            else
            {
                float d = vec.X._mean - len;
                Vector2 w = new Vector2(1, new Scalar(vec.Y._mean / d));
                Scalar t = new Scalar(d / len);
                return Matrix2x2.Identity + Outer(t * w, w);
            }
        }

        /// <summary>
        /// Finds an orthonormal involutory matrix which, when applied to <paramref name="vec"/>,
        /// zeros out its Y and Z components.
        /// </summary>
        public static Matrix3x3 HouseholderAnnihilate(Vector3 vec)
        {
            float len = vec.Length._mean;
            if (len == 0)
            {
                // Any reflection matrix will do
                return new Matrix3x3(
                    new Vector3(-1, 0, 0),
                    new Vector3(0, 1, 0),
                    new Vector3(0, 0, 1));
            }
            else if (vec.X >= 0)
            {
                float d = vec.X._mean + len;
                Vector3 w = new Vector3(1, new Vector2(vec.Y_Z._mean / d));
                Scalar t = new Scalar(d / len);
                return Matrix3x3.Identity - Outer(t * w, w);
            }
            else
            {
                float d = vec.X._mean - len;
                Vector3 w = new Vector3(1, new Vector2(vec.Y_Z._mean / d));
                Scalar t = new Scalar(d / len);
                return Matrix3x3.Identity + Outer(t * w, w);
            }
        }

        /// <summary>
        /// Computes the SVD of a bidiagonal matrix with values given by <paramref name="d"/>
        /// and <paramref name="e"/>.
        /// </summary>
        private static void _svdBidiagonal(ref Matrix3x3 u, ref Vector3 d, Vector2 e, ref Matrix3x3 v)
        {
            // Convert the matrix to double-precision since the convergence speed and quality is highly
            // dependent on these variables.
            double dx = d.X._mean;
            double dy = d.Y._mean;
            double dz = d.Z._mean;
            double ex = e.X._mean;
            double ey = e.Y._mean;

            // Algorithm from http://www.math.pitt.edu/~sussmanm/2071Spring08/lab09/index.html
            // Repeat sweep until convergence
            const uint maxIters = 40;
            for (uint i = 0; i < maxIters; i++)
            {
                Vector3 tmp;
                double uc, us, vc, vs, r;
                double odx = dx;
                double ody = dy;
                double odz = dz;
                _givensAnnihilate(dx, ex, out vc, out vs, out r);
                Scalar svc = vc;
                Scalar svs = vs;
                tmp = v.X;
                v.X = svc * tmp + svs * v.Y;
                v.Y = svc * v.Y - svs * tmp;
                _givensAnnihilate(r, vs * dy, out uc, out us, out r);
                Scalar suc = uc;
                Scalar sus = us;
                tmp = u.X;
                u.X = suc * tmp + sus * u.Y;
                u.Y = suc * u.Y - sus * tmp;
                dx = r;
                _givensAnnihilate(vc * dy, ey, out vc, out vs, out r);
                svc = vc;
                svs = vs;
                tmp = v.Y;
                v.Y = svc * tmp + svs * v.Z;
                v.Z = svc * v.Z - svs * tmp;
                ex = r * us;
                _givensAnnihilate(uc * r, vs * dz, out uc, out us, out r);
                suc = uc;
                sus = us;
                tmp = u.Y;
                u.Y = suc * tmp + sus * u.Z;
                u.Z = suc * u.Z - sus * tmp;
                dy = r;
                double h = vc * dz;
                ey = h * us;
                dz = h * uc;

                // Check for convergence. For most matrices, this occurs well before maxIters.
                if (dx == odx & dy == ody & dz == odz)
                    break;
            }

            // Convert back to single-precision
            d.X = dx;
            d.Y = dy;
            d.Z = dz;
            e.X = ex;
            e.Y = ey;

            // Make singular values positive
            if (d.X < 0)
            {
                d.X = -d.X;
                u.X = -u.X;
            }
            if (d.Y < 0)
            {
                d.Y = -d.Y;
                u.Y = -u.Y;
            }
            if (d.Z < 0)
            {
                d.Z = -d.Z;
                u.Z = -u.Z;
            }

            // TODO: Sort singular values? It seems like they always converge into the correct order regardless?
        }

        /// <summary>
        /// Computes the rotation matrix which zeros out the Y component of the
        /// vector (<paramref name="x"/>, <paramref name="y"/>).
        /// </summary>
        /// <param name="c">The cosine of the rotation.</param>
        /// <param name="s">The sine of the rotation.</param>
        /// <param name="r">The X component of the rotated <paramref name="vec"/>.</param>
        private static void _givensAnnihilate(double x, double y, out double c, out double s, out double r)
        {
            if (x == 0)
            {
                c = 0.0;
                s = 1.0;
                r = y;
            }
            else if (Math.Abs(x) > Math.Abs(y))
            {
                double t = y / x;
                double d = Math.Sqrt(1.0 + t * t);
                c = 1.0 / d;
                s = t * c;
                r = x * d;
            }
            else
            {
                double t = x / y;
                double d = Math.Sqrt(1.0 + t * t);
                s = 1.0 / d;
                c = t * s;
                r = y * d;
            }
        }
    }
}