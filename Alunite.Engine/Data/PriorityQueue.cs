﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Alunite.Data
{
    /// <summary>
    /// A reference to a priority queue data structure.
    /// </summary>
    /// <remarks>Adapted from https://stackoverflow.com/a/4994931 </remarks>
    [DebuggerDisplay("Count = {Count}")]
    public sealed class PriorityQueue<T>
    {
        private T[] _heap;
        private uint _count;
        public PriorityQueue(IComparer<T> comparer, uint capacity)
        {
            Comparer = comparer;
            _heap = new T[capacity];
            _count = 0;
        }

        /// <summary>
        /// The <see cref="IComparer{T}"/> used to compare items in the priority queue.
        /// </summary>
        public IComparer<T> Comparer { get; }

        /// <summary>
        /// The number of items in this priority queue.
        /// </summary>
        public uint Count => _count;

        /// <summary>
        /// The minimum item in this priority queue, assuming it is not empty.
        /// </summary>
        public ref T Top => ref _heap[0];

        /// <summary>
        /// Adds a new item to this priority queue.
        /// </summary>
        /// <param name="comparer">The comparer for the priority queue.</param>
        public void Push(T value)
        {
            if (_count >= _heap.Length) Array.Resize(ref _heap, (int)_count * 2);
            _heap[_count] = value;
            _siftUp(_count++);
        }

        /// <summary>
        /// Returns and removes the minimum item from this priority queue.
        /// </summary>
        public T Pop()
        {
            var v = Top;
            _heap[0] = _heap[--_count];
            if (_count > 0) _siftDown(0);
            return v;
        }

        private void _siftUp(uint n)
        {
            var v = _heap[n];
            for (var n2 = n / 2; n > 0 && Comparer.Compare(v, _heap[n2]) < 0; n = n2, n2 /= 2) _heap[n] = _heap[n2];
            _heap[n] = v;
        }

        private void _siftDown(uint n)
        {
            var v = _heap[n];
            for (var n2 = n * 2; n2 < _count; n = n2, n2 *= 2)
            {
                if (n2 + 1 < _count && Comparer.Compare(_heap[n2 + 1], _heap[n2]) < 0) n2++;
                if (Comparer.Compare(v, _heap[n2]) <= 0) break;
                _heap[n] = _heap[n2];
            }
            _heap[n] = v;
        }
    }
}
