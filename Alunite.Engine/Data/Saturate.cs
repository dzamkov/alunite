﻿using System;

namespace Alunite.Data
{
    /// <summary>
    /// Contains saturation arithmetic functions. These functions clip values instead of overflowing.
    /// </summary>
    public static class Saturate
    {
        /// <summary>
        /// Adds the given values with clipping instead of overflow.
        /// </summary>
        public static uint Add(uint a, uint b)
        {
            uint c = unchecked(a + b);
            if (c < a)
                return uint.MaxValue;
            return c;
        }

        /// <summary>
        /// Subtracts the given values with clipping instead of underflow.
        /// </summary>
        public static uint Sub(uint a, uint b)
        {
            uint c = unchecked(a - b);
            if (c > a)
                return 0;
            return c;
        }
    }
}
