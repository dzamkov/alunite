﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Threading;
using System.Diagnostics;

namespace Alunite.Data
{
    /// <summary>
    /// A thread-safe cache data structure.
    /// </summary>
    public struct ConcurrentCache<TKey, TValue>
    {
        // TODO: Real implementation
        private LookupDictionary<TKey, TValue> _dict;
        public ConcurrentCache(IEqualityComparer<TKey> keyComparer)
        {
            _dict = new LookupDictionary<TKey, TValue>(keyComparer);
        }

        /// <summary>
        /// Tries getting the value associated with the given key, returning false if there is no such entry in
        /// the cache.
        /// </summary>
        public bool TryGet(TKey key, out TValue value)
        {
            return _dict.TryGet(key, out value);
        }

        /// <summary>
        /// Adds or updates an entry for the given key.
        /// </summary>
        public void Update(TKey key, TValue value)
        {
            _dict[key] = value;
        }

        /// <summary>
        /// Either gets a pre-existing value from the cache, or creates a new one using the given factory function.
        /// </summary>
        public TValue GetOrAdd(TKey key, Func<TKey, TValue> valueFactory)
        {
            return _dict.GetOrAdd(key, valueFactory);
        }
    }

    /// <summary>
    /// A thread-safe cache which manages the lifetimes of a bounded number of <see cref="TEntry"/>s, leaving the
    /// responsibility of locating entries to the client.
    /// </summary>
    public sealed class ConcurrentCache<TEntry>
        where TEntry : CacheEntry, IDisposable
    {
        public ConcurrentCache(uint capacity)
        {
            _remCapacity = capacity;
            _head = new CacheEntry();
            _head._prev = _head;
            _head._next = _head;
        }

        /// <summary>
        /// The number of additional entries that can be added to this cache without evicting existing entries.
        /// </summary>
        private uint _remCapacity;

        /// <summary>
        /// A dummy <see cref="CacheEntry"/> which acts as the start and end of the doubly-linked list implementing
        /// the eviction queue.
        /// </summary>
        private readonly CacheEntry _head;

        /// <summary>
        /// Gives ownership of the given entry to this cache and signals that it has been recently used. This will result
        /// in <see cref="TEntry.Dispose"/> being called exactly once: if the entry is not yet in the cache, it will be added
        /// and <see cref="TEntry.Dispose"/> will be called upon evication; if the entry is already in the cache, it will be
        /// promoted and <see cref="TEntry.Dispose"/> will be called immediately (useful for reference-counted entries). A
        /// particular entry may only ever be associated with one <see cref="ConcurrentCache{TEntry}"/>.
        /// </summary>
        public void Using(TEntry entry)
        {
            lock (this)
            {
                // Remove from current place
                if (entry._prev != null)
                {
                    var prev = entry._prev;
                    var next = entry._next;
                    prev._next = next;
                    next._prev = prev;
                    entry.Dispose();
                }
                else if (_remCapacity > 0)
                {
                    _remCapacity--;
                }
                else
                {
                    // Evict oldest entry
                    var oldest = _head._prev;
                    var prev = oldest._prev;
                    prev._next = _head;
                    _head._prev = prev;
                    oldest._next = null;
                    oldest._prev = null;
                    ((TEntry)oldest).Dispose();
                }

                // Add to beginning
                {
                    var next = _head._next;
                    entry._next = next;
                    entry._prev = _head;
                    _head._next = entry;
                    next._prev = entry;
                }
            }
        }
    }

    /// <summary>
    /// The required base type for the entries in a <see cref="ConcurrentCache{TEntry}"/>.
    /// </summary>
    public class CacheEntry
    {
        /// <summary>
        /// Identifies the next most recently used entry.
        /// </summary>
        internal CacheEntry _prev;

        /// <summary>
        /// Identifies the next least recently used entry.
        /// </summary>
        internal CacheEntry _next;
    }
}
