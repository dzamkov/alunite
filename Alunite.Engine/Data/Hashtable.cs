﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Alunite.Data
{
    /// <summary>
    /// Contains functions for implementing lightweight hashtables within arrays.
    /// </summary>
    public static class Hashtable
    {
        /// <summary>
        /// Determines whether b is between a (exclusive) and c (inclusive) in a modular field.
        /// </summary>
        public static bool BetweenMod(uint a, uint b, uint c)
        {
            return (b > a) ^ (b <= c) ^ (a < c);
        }

        /// <summary>
        /// Gets the bin corresponding to the given hash in a hashtable with a power-of-2 size.
        /// </summary>
        public static uint GetBinPow2(int length, int hash)
        {
            return Bitwise.ModPow2(hash, (uint)length);
        }

        /// <summary>
        /// Adds a value to an index in a hashtable with wraparound, assuming that value is less
        /// than the length of the hashtable.
        /// </summary>
        public static void Traverse(int length, ref uint index, uint delta)
        {
            // Modulo operation is too slow
            index += delta;
            if (index >= length)
                index -= (uint)length;
        }

        /// <summary>
        /// Inserts an item into a simple hashset of references. <paramref name="table"/> must maintain a
        /// power-of-2 size.
        /// </summary>
        public static void Insert<T>(ref T[] table, T item)
            where T : class
        {
            if (table.Length == 0)
                goto resize;

            insert:
            {
                int hash = item.GetHashCode();
                uint bin = GetBinPow2(table.Length, hash);
                uint endBin = bin;
            nextBin:
                ref T cur = ref table[bin];
                if (cur == null)
                {
                    cur = item;
                    return;
                }
                else if (cur == item)
                {
                    return;
                }

                // Try next bin
                Traverse(table.Length, ref bin, 1);
                if (bin == endBin)
                    goto resize;
                else
                    goto nextBin;
            }

        resize:
            {
                // Create new table
                int nSize = Math.Max(4, table.Length * 2);
                T[] nTable = new T[nSize];

                // Copy current items
                for (int i = 0; i < table.Length; i++)
                {
                    var copy = table[i];
                    int hash = copy.GetHashCode();
                    uint bin = GetBinPow2(nTable.Length, hash);
                    while (nTable[bin] != null)
                        Traverse(nTable.Length, ref bin, 1);
                    nTable[bin] = copy;
                }

                // Finish up with new table
                table = nTable;
                goto insert;
            }
        }
    }
}
