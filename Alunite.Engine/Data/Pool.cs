﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Alunite.Data
{
    /// <summary>
    /// An efficient storage location for values of type <typeparamref name="T"/>. This is not thread safe.
    /// </summary>
    public struct Pool<T>
    {
        public Pool(uint initialCapacity)
        {
            Debug.Assert(initialCapacity % 64 == 0);
            _items = new T[initialCapacity];
            _useBitmap = new ulong[_items.Length / 64];
            _allocHint = 0;
        }

        /// <summary>
        /// The array storing all the items for this pool.
        /// </summary>
        private T[] _items;

        /// <summary>
        /// A bitmap indicating which items in <see cref="_items"/> are in use
        /// (i.e. have been allocated, but not yet freed).
        /// </summary>
        private ulong[] _useBitmap;

        /// <summary>
        /// The index of <see cref="_useBitmap"/> that should be checked first for the next
        /// allocation.
        /// </summary>
        private uint _allocHint;

        /// <summary>
        /// Identifies an item in a pool.
        /// </summary>
        public struct Id : IEquatable<Id>
        {
            internal uint _index;
            internal Id(uint index)
            {
                _index = index;
            }

            /// <summary>
            /// A sentinel <see cref="Id"/> used to indicate the absence of an item.
            /// </summary>
            public static readonly Id Dummy = new Id(uint.MaxValue);

            /// <summary>
            /// Indicates whether this is <see cref="Dummy"/>.
            /// </summary>
            public bool IsDummy
            {
                get
                {
                    return _index == uint.MaxValue;
                }
            }

            public static bool operator ==(Id a, Id b)
            {
                return a._index == b._index;
            }

            public static bool operator !=(Id a, Id b)
            {
                return a._index != b._index;
            }

            public override bool Equals(object obj)
            {
                if (!(obj is Id))
                    return false;
                return this == (Id)obj;
            }

            bool IEquatable<Id>.Equals(Id other)
            {
                return this == other;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        /// <summary>
        /// Allocates space for a new item in this pool. The item can be retreived and modified
        /// using <see cref="this[Id]"/>.
        /// </summary>
        public Id Allocate()
        {
            // Find an index in _useBitmap with free space
            ulong useBitmap;
            uint startIndex = _allocHint;
            while ((useBitmap = _useBitmap[_allocHint]) == 0xFFFFFFFFFFFFFFFF)
            {
                _allocHint = (_allocHint + 1) % (uint)_useBitmap.Length;
                if (_allocHint == startIndex)
                {
                    // Expand pool
                    int itemCount = _items.Length;
                    int useBitmapCount = _useBitmap.Length;
                    T[] nItems = new T[itemCount * 2];
                    ulong[] nUseBitmap = new ulong[useBitmapCount * 2];
                    Array.Copy(_items, nItems, itemCount);
                    Array.Copy(_useBitmap, nUseBitmap, useBitmapCount);
                    _items = nItems;
                    _useBitmap = nUseBitmap;

                    // Allocate new item in the obvious place (start of new region)
                    _useBitmap[useBitmapCount] = 0x0000000000000001;
                    _allocHint = (uint)useBitmapCount;
                    return new Id((uint)itemCount);
                }
            }

            // Mark first available item as active
            uint bit = Bitwise.FindFirstZero(useBitmap);
            _useBitmap[_allocHint] = useBitmap | ((ulong)1 << (int)bit);
            return new Id(_allocHint * 64 + bit);
        }

        /// <summary>
        /// Frees the space for the given item in this pool. After this call, the item should no longer
        /// be accessed.
        /// </summary>
        public void Free(Id id)
        {
            _useBitmap[id._index / 64] &= ~((ulong)1 << (int)(id._index % 64));
        }

        /// <summary>
        /// Gets a reference to an item in this pool. References are valid until the next call to
        /// <see cref="Allocate"/> or <see cref="Free(Id)"/>.
        /// </summary>
        public ref T this[Id id]
        {
            get
            {
                return ref _items[id._index];
            }
        }
    }
}
