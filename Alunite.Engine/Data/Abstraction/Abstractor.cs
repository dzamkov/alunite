﻿using System;
using System.Collections.Generic;

namespace Alunite.Data.Abstraction
{
    /// <summary>
    /// A helper class which uses <see cref="Viewer{T}"/>s to generalize some operation, with an input of type
    /// <typeparamref name="T"/>, to other inputs.
    /// </summary>
    public sealed class Abstractor<T>
    {
        // TODO: Real implementation with real generalization
        public Abstractor(ref Viewer<T> input)
        {
            Input = input;
        }

        /// <summary>
        /// A <see cref="Viewer{T}"/> wrapper over the input to the operation which will be generalized. All access
        /// to the input should be through this wrapper.
        /// </summary>
        public Viewer<T> Input { get; }

        /// <summary>
        /// The <see cref="Pattern{T}"/> of inputs which this <see cref="Abstractor{T}"/> was able to generalize to. At
        /// minimum, this is guranteed to accept <see cref="Input"/>.
        /// </summary>
        public Pattern<T> Pattern => (T)Input;

        /// <summary>
        /// Gets a <see cref="Property{T}"/> which relates the record produced by <see cref="Pattern"/> to the given
        /// operation output.
        /// </summary>
        public Property<K> Generalize<K>(Viewer<K> output)
        {
            return (K)output;
        }
    }
}
