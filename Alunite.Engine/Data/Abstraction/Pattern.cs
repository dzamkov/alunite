﻿using System;
using System.Collections.Generic;

namespace Alunite.Data.Abstraction
{
    /// <summary>
    /// Describes a set of values of type <typeparamref name="T"/> and associated each to a <see cref="Record"/>
    /// in a common schema.
    /// </summary>
    public struct Pattern<T>
    {
        // TODO
        internal T _value;

        /// <summary>
        /// Determines whether the given value is of this pattern, and if so, gets the <see cref="Record"/>
        /// representation of it.
        /// </summary>
        public bool TryMatch(T value, out Record record)
        {
            if (EqualityHelper.Equals(_value, value))
            {
                record = Record.Empty;
                return true;
            }
            record = default;
            return false;
        }

        /// <summary>
        /// Determines whether the given value is of this pattern, and if so, gets the <see cref="Record"/>
        /// representation of it.
        /// </summary>
        public bool TryMatch(Viewer<T> value, out RecordViewer record)
        {
            if (EqualityHelper.Equals(_value, (T)value))
            {
                record = RecordViewer.Empty;
                return true;
            }
            record = default;
            return false;
        }

        /// <summary>
        /// Constructs a <see cref="Pattern{T}"/> which matches the given exact value and produces an
        /// empty <see cref="Record"/>.
        /// </summary>
        public static implicit operator Pattern<T>(T value)
        {
            return new Pattern<T>
            {
                _value = value
            };
        }
    }

    /// <summary>
    /// Contains helper functions related to <see cref="Pattern{T}"/>.
    /// </summary>
    public static class Pattern
    {
        /// <summary>
        /// Gets a <see cref="Pattern{T}"/> which matches any value, producing a single-field <see cref="Record"/>
        /// with that value.
        /// </summary>
        public static Pattern<T> Any<T>()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a <see cref="Pattern{T}"/> which matches an exact value, producing an empty <see cref="Record"/>.
        /// </summary>
        public static Pattern<T> Exact<T>(T value)
        {
            return value;
        }
    }

    /// <summary>
    /// Associates values of type <typeparamref name="TKey"/> to values of <typeparamref name="TValue"/> through the use
    /// of <see cref="Pattern{T}"/>s. This allows potentially infinite sets of keys to be associated at once. This class
    /// is not thread-safe.
    /// </summary>
    public sealed class PatternDictionary<TKey, TValue>
    {
        private Dictionary<TKey, TValue> _source;
        public PatternDictionary()
        {
            _source = new Dictionary<TKey, TValue>(EqualityHelper.Comparer<TKey>());
        }

        /// <summary>
        /// Gets or sets the value of a particular key in this dictionary.
        /// </summary>
        public TValue this[TKey key]
        {
            get
            {
                return _source[key];
            }
            set
            {
                _source[key] = value;
            }
        }

        /// <summary>
        /// Associates every key that matches the given pattern to the given value, excluding keys that are already
        /// associated.
        /// </summary>
        /// <returns>True if any keys were updated. False otherwise</returns>
        public bool Add(Pattern<TKey> pattern, TValue value)
        {
            if (_source.ContainsKey(pattern._value))
                return false;
            _source.Add(pattern._value, value);
            return true;
        }

        /// <summary>
        /// Associates every key that matches the given pattern to the given value.
        /// </summary>
        public void Update(Pattern<TKey> pattern, TValue value)
        {
            _source[pattern._value] = value;
        }

        /// <summary>
        /// Removes all keys from this dictionary that match the given pattern.
        /// </summary>
        public void Remove(Pattern<TKey> pattern)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the value associated with the given key, or returns false if no such association exists.
        /// </summary>
        /// <param name="record">The record produced by the pattern which matched <paramref name="key"/>.</param>
        public bool TryGet(TKey key, out Record record, out TValue value)
        {
            record = Record.Empty;
            return _source.TryGetValue(key, out value);
        }

        /// <summary>
        /// Gets the value associated with the given key, or returns false if no such association exists.
        /// </summary>
        /// <param name="record">The record produced by the pattern which matched <paramref name="key"/>.</param>
        public bool TryGet(Viewer<TKey> key, out RecordViewer record, out TValue value)
        {
            record = RecordViewer.Empty;
            return _source.TryGetValue(key, out value);
        }
    }
}