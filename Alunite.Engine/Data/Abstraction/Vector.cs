﻿using System;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;

using Alunite.Data.Geometry;

namespace Alunite.Data.Abstraction
{
    /// <summary>
    /// A specialization of <see cref="Viewer{T}"/> for <see cref="Vector2"/>s.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct VectorViewer2
    {
        private Vector2 _value;
        private IViewer<Vector2> _source;
        public VectorViewer2(Vector2 value)
        {
            _value = value;
            _source = null;
        }

        public VectorViewer2(IViewer<Vector2> source)
        {
            _value = default;
            _source = source;
        }

        public VectorViewer2(ScalarViewer x, ScalarViewer y)
        {
            if (x.IsKnown(out Scalar value_x) &&
                y.IsKnown(out Scalar value_y))
            {
                _value = new Vector2(value_x, value_y);
                _source = null;
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Checks whether this <see cref="Viewer{T}"/> has been fully evaluated, and if so, gets its value.
        /// </summary>
        public bool IsKnown(out Vector2 value)
        {
            value = _value;
            return _source == null;
        }

        public ScalarViewer X
        {
            get
            {
                if (IsKnown(out Vector2 value))
                    return value.X;
                throw new NotImplementedException();
            }
        }

        public ScalarViewer Y
        {
            get
            {

                if (IsKnown(out Vector2 value))
                    return value.Y;
                throw new NotImplementedException();
            }
        }

        public static implicit operator Viewer<Vector2>(VectorViewer2 source)
        {
            if (source._source is null)
                return source._value;
            return new Viewer<Vector2>(source._source);
        }

        public static implicit operator VectorViewer2(Viewer<Vector2> source)
        {
            return new VectorViewer2(source);
        }

        public static implicit operator Vector2(VectorViewer2 viewer)
        {
            if (viewer._source is null)
                return viewer._value;
            return viewer._source.Value;
        }

        public static implicit operator VectorViewer2(Vector2 value)
        {
            return new VectorViewer2(value);
        }

        public override string ToString()
        {
            if (_source is null)
                return _value.ToString();
            return _source.ToString();
        }
    }

    /// <summary>
    /// A specialization of <see cref="Viewer{T}"/> for <see cref="Vector3"/>s.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct VectorViewer3
    {
        private Vector3 _value;
        private IViewer<Vector3> _source;
        public VectorViewer3(Vector3 value)
        {
            _value = value;
            _source = null;
        }

        private VectorViewer3(IViewer<Vector3> source)
        {
            _value = default;
            _source = source;
        }

        public VectorViewer3(ScalarViewer x, ScalarViewer y, ScalarViewer z)
        {
            if (x.IsKnown(out Scalar value_x) && 
                y.IsKnown(out Scalar value_y) && 
                z.IsKnown(out Scalar value_z))
            {
                _value = new Vector3(value_x, value_y, value_z);
                _source = null;
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Checks whether this <see cref="Viewer{T}"/> has been fully evaluated, and if so, gets its value.
        /// </summary>
        public bool IsKnown(out Vector3 value)
        {
            value = _value;
            return _source == null;
        }

        public ScalarViewer X
        {
            get
            {
                if (IsKnown(out Vector3 value))
                    return value.X;
                throw new NotImplementedException();
            }
        }

        public ScalarViewer Y
        {
            get
            {
                if (IsKnown(out Vector3 value))
                    return value.Y;
                throw new NotImplementedException();
            }
        }

        public ScalarViewer Z
        {
            get
            {
                if (IsKnown(out Vector3 value))
                    return value.Z;
                throw new NotImplementedException();
            }
        }

        public static implicit operator Viewer<Vector3>(VectorViewer3 source)
        {
            if (source._source is null)
                return source._value;
            return new Viewer<Vector3>(source._source);
        }

        public static implicit operator VectorViewer3(Viewer<Vector3> source)
        {
            if (source.IsKnown(out Vector3 value))
                return value;
            return new VectorViewer3(source);
        }

        public static implicit operator Vector3(VectorViewer3 viewer)
        {
            if (viewer._source is null)
                return viewer._value;
            return viewer._source.Value;
        }

        public static implicit operator VectorViewer3(Vector3 value)
        {
            return new VectorViewer3(value);
        }

        public override string ToString()
        {
            if (_source is null)
                return _value.ToString();
            return _source.ToString();
        }
    }

    /// <summary>
    /// A <see cref="Vector2"/> derived from a linear combination of <see cref="Scalar"/> parameters.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct VectorForm2 : IEquatable<VectorForm2>
    {
        internal Vector2 _bias;
        internal (uint, Vector2)[] _factors;
        public VectorForm2(ScalarForm x, ScalarForm y)
        {
            if (x._terms.Length == 0 && y._terms.Length == 0)
            {
                _bias = new Vector2(x._bias, y._bias);
                _factors = Array.Empty<(uint, Vector2)>();
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// The zero <see cref="VectorForm2"/>.
        /// </summary>
        public static VectorForm2 Zero => new VectorForm2
        {
            _bias = Vector2.Zero,
            _factors = Array.Empty<(uint, Vector2)>()
        };

        /// <summary>
        /// Isolates the X component of this vector form.
        /// </summary>
        public ScalarForm X
        {
            get
            {
                if (_factors.Length == 0)
                    return _bias.X;
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Isolates the Y component of this vector form.
        /// </summary>
        public ScalarForm Y
        {
            get
            {
                if (_factors.Length == 0)
                    return _bias.Y;
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Evaluates this parameterized <see cref="Vector2"/> using the given arguments.
        /// </summary>
        public Vector2 this[Scalar[] args]
        {
            get
            {
                Vector2 res = _bias;
                foreach (var (param, coeff) in _factors)
                    res += coeff * args[param];
                return res;
            }
        }

        /// <summary>
        /// Adds the given index offset to all parameter references in this form.
        /// </summary>
        public VectorForm2 Shift(uint offset)
        {
            if (_factors.Length == 0)
                return _bias;
            if (offset == 0)
                return this;
            var nFactors = new (uint, Vector2)[_factors.Length];
            for (uint i = 0; i < (uint)nFactors.Length; i++)
            {
                var (param, coeff) = _factors[i];
                nFactors[i] = (param + offset, coeff);
            }
            return new VectorForm2
            {
                _bias = _bias,
                _factors = nFactors
            };
        }

        public static implicit operator VectorForm2(Vector2 source)
        {
            return new VectorForm2
            {
                _bias = source,
                _factors = Array.Empty<(uint, Vector2)>()
            };
        }

        public static VectorForm2 operator +(VectorForm2 a, VectorForm2 b)
        {
            if (a._factors.Length == 0)
            {
                return new VectorForm2
                {
                    _bias = a._bias + b._bias,
                    _factors = b._factors
                };
            }
            if (b._factors.Length == 0)
            {
                return new VectorForm2
                {
                    _bias = a._bias + b._bias,
                    _factors = a._factors
                };
            }
            throw new NotImplementedException();
        }

        public static VectorForm2 operator *(Scalar a, VectorForm2 b)
        {
            if (b._factors.Length == 0)
                return a * b._bias;
            var nFactors = new (uint, Vector2)[b._factors.Length];
            for (uint i = 0; i < nFactors.Length; i++)
            {
                (uint param, Vector2 coeff) = b._factors[i];
                nFactors[i] = (param, a * coeff);
            }
            return new VectorForm2
            {
                _bias = a * b._bias,
                _factors = nFactors
            };
        }

        public static VectorForm2 operator *(VectorForm2 a, Scalar b)
        {
            return b * a;
        }

        public static VectorForm2 operator *(Roflection2i rof, VectorForm2 a)
        {
            if (a._factors.Length == 0)
                return rof * a._bias;
            var nFactors = new (uint, Vector2)[a._factors.Length];
            for (uint i = 0; i < nFactors.Length; i++)
            {
                (uint param, Vector2 coeff) = a._factors[i];
                nFactors[i] = (param, rof * coeff);
            }
            return new VectorForm2
            {
                _bias = rof * a._bias,
                _factors = nFactors
            };
        }

        public static bool operator ==(VectorForm2 a, VectorForm2 b)
        {
            if (a._bias != b._bias)
                return false;
            if (a._factors.Length != b._factors.Length)
                return false;
            for (uint i = 0; i < (uint)a._factors.Length; i++)
            {
                (uint param_a, Vector2 coeff_a) = a._factors[i];
                (uint param_b, Vector2 coeff_b) = b._factors[i];
                if (param_a != param_b)
                    return false;
                if (coeff_a != coeff_b)
                    return false;
            }
            return true;
        }

        public static bool operator !=(VectorForm2 a, VectorForm2 b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is VectorForm2))
                return false;
            return this == (VectorForm2)obj;
        }
        
        bool IEquatable<VectorForm2>.Equals(VectorForm2 other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            int hash = _bias.GetHashCode();
            foreach (var (param, coeff) in _factors)
                hash = HashCodeHelper.Combine(hash,
                    param.GetHashCode(),
                    coeff.GetHashCode());
            return hash;
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            bool first = true;
            if (_bias == Vector2.Zero)
            {
                str.Write(_bias.ToString());
                first = false;
            }
            foreach (var (param, coeff) in _factors)
            {
                if (first)
                    first = false;
                else
                    str.Write(" + ");
                str.Write(coeff.ToString() + " * p₇₅" + param.ToStringInvariant());
            }
            if (first) str.Write(_bias.ToString());
            return str.ToString();
        }
    }

    /// <summary>
    /// A <see cref="Vector3"/> derived from a linear combination of <see cref="Scalar"/> parameters.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct VectorForm3 : IEquatable<VectorForm3>
    {
        private Vector3 _bias;
        private (uint, Vector3)[] _factors;
        public VectorForm3(ScalarForm x, ScalarForm y, ScalarForm z)
        {
            if (x._terms.Length == 0 && y._terms.Length == 0 && z._terms.Length == 0)
            {
                _bias = new Vector3(x._bias, y._bias, z._bias);
                _factors = Array.Empty<(uint, Vector3)>();
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public VectorForm3(VectorForm2 x_y, ScalarForm z)
        {
            if (x_y._factors.Length == 0 && z._terms.Length == 0)
            {
                _bias = new Vector3(x_y._bias, z._bias);
                _factors = Array.Empty<(uint, Vector3)>();
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public VectorForm3(ScalarForm x, VectorForm2 y_z)
        {
            if (x._terms.Length == 0 && y_z._factors.Length == 0)
            {
                _bias = new Vector3(x._bias, y_z._bias);
                _factors = Array.Empty<(uint, Vector3)>();
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// The zero <see cref="VectorForm3"/>.
        /// </summary>
        public static VectorForm3 Zero => new VectorForm3
        {
            _bias = Vector3.Zero,
            _factors = Array.Empty<(uint, Vector3)>()
        };

        /// <summary>
        /// Isolates the X component of this vector form.
        /// </summary>
        public ScalarForm X
        {
            get
            {
                if (_factors.Length == 0)
                    return _bias.X;
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Isolates the Y component of this vector form.
        /// </summary>
        public ScalarForm Y
        {
            get
            {
                if (_factors.Length == 0)
                    return _bias.Y;
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Isolates the Z component of this vector form.
        /// </summary>
        public ScalarForm Z
        {
            get
            {
                if (_factors.Length == 0)
                    return _bias.Z;
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Evaluates this parameterized <see cref="Vector3"/> using the given arguments.
        /// </summary>
        public Vector3 this[Scalar[] args]
        {
            get
            {
                Vector3 res = _bias;
                foreach (var (param, coeff) in _factors)
                    res += coeff * args[param];
                return res;
            }
        }

        public static implicit operator VectorForm3(Vector3 source)
        {
            return new VectorForm3
            {
                _bias = source,
                _factors = Array.Empty<(uint, Vector3)>()
            };
        }

        public static VectorForm3 operator +(VectorForm3 a, VectorForm3 b)
        {
            if (a._factors.Length == 0)
            {
                return new VectorForm3
                {
                    _bias = a._bias + b._bias,
                    _factors = b._factors
                };
            }
            if (b._factors.Length == 0)
            {
                return new VectorForm3
                {
                    _bias = a._bias + b._bias,
                    _factors = a._factors
                };
            }
            throw new NotImplementedException();
        }

        public static VectorForm3 operator *(Scalar a, VectorForm3 b)
        {
            if (b._factors.Length == 0)
                return a * b._bias;
            var nFactors = new (uint, Vector3)[b._factors.Length];
            for (uint i = 0; i < nFactors.Length; i++)
            {
                var (param, coeff) = b._factors[i];
                nFactors[i] = (param, a * coeff);
            }
            return new VectorForm3
            {
                _bias = a * b._bias,
                _factors = nFactors
            };
        }

        public static VectorForm3 operator *(VectorForm3 a, Scalar b)
        {
            return b * a;
        }

        public static VectorForm3 operator *(Roflection3i rof, VectorForm3 a)
        {
            if (a._factors.Length == 0)
                return rof * a._bias;
            var nFactors = new (uint, Vector3)[a._factors.Length];
            for (uint i = 0; i < nFactors.Length; i++)
            {
                (uint param, Vector3 coeff) = a._factors[i];
                nFactors[i] = (param, rof * coeff);
            }
            return new VectorForm3
            {
                _bias = rof * a._bias,
                _factors = nFactors
            };
        }

        public static bool operator ==(VectorForm3 a, VectorForm3 b)
        {
            if (a._bias != b._bias)
                return false;
            if (a._factors.Length != b._factors.Length)
                return false;
            for (uint i = 0; i < (uint)a._factors.Length; i++)
            {
                (uint param_a, Vector3 coeff_a) = a._factors[i];
                (uint param_b, Vector3 coeff_b) = b._factors[i];
                if (param_a != param_b)
                    return false;
                if (coeff_a != coeff_b)
                    return false;
            }
            return true;
        }

        public static bool operator !=(VectorForm3 a, VectorForm3 b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is VectorForm3))
                return false;
            return this == (VectorForm3)obj;
        }

        bool IEquatable<VectorForm3>.Equals(VectorForm3 other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            int hash = _bias.GetHashCode();
            foreach (var (param, coeff) in _factors)
                hash = HashCodeHelper.Combine(hash,
                    param.GetHashCode(),
                    coeff.GetHashCode());
            return hash;
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            bool first = true;
            if (_bias == Vector3.Zero)
            {
                str.Write(_bias.ToString());
                first = false;
            }
            foreach (var (param, coeff) in _factors)
            {
                if (first)
                    first = false;
                else
                    str.Write(" + ");
                str.Write(coeff.ToString() + " * p_" + param.ToStringInvariant());
            }
            if (first) str.Write(_bias.ToString());
            return str.ToString();
        }
    }
}
