﻿using System;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;

using Alunite.Data.Geometry;

#pragma warning disable CS0660
#pragma warning disable CS0661

namespace Alunite.Data.Abstraction
{
    /// <summary>
    /// A specialization of <see cref="Viewer{T}"/> for <see cref="Scalar"/>.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct ScalarViewer
    {
        private Scalar _value;
        internal IViewer<Scalar> _source;
        public ScalarViewer(Scalar value)
        {
            _value = value;
            _source = null;
        }

        private ScalarViewer(IViewer<Scalar> source)
        {
            _value = default;
            _source = source;
        }

        /// <summary>
        /// Checks whether this <see cref="Viewer{T}"/> has been fully evaluated, and if so, gets its value.
        /// </summary>
        public bool IsKnown(out Scalar value)
        {
            value = _value;
            return _source == null;
        }

        /// <summary>
        /// Determines whether two <see cref="ScalarViewer"/>'s have the same value. This may require the viewers
        /// to be evaluated.
        /// </summary>
        public static bool operator ==(ScalarViewer a, ScalarViewer b)
        {
            if (a._source == null && b._source == null)
                return a._value == b._value;
            throw new NotImplementedException();
        }

        /// <summary>
        /// Determines whether two <see cref="Viewer{T}"/>'s have a different value. This may require the viewers
        /// to be evaluated.
        /// </summary>
        public static bool operator !=(ScalarViewer a, ScalarViewer b)
        {
            return !(a == b);
        }

        public static ScalarViewer operator -(ScalarViewer a)
        {
            if (a.IsKnown(out Scalar value))
                return -value;
            throw new NotImplementedException();
        }

        public static ScalarViewer operator +(ScalarViewer a, ScalarViewer b)
        {
            if (a.IsKnown(out Scalar value_a) && b.IsKnown(out Scalar value_b))
                return value_a + value_b;
            throw new NotImplementedException();
        }

        public static ScalarViewer operator -(ScalarViewer a, ScalarViewer b)
        {
            if (a.IsKnown(out Scalar value_a) && b.IsKnown(out Scalar value_b))
                return value_a - value_b;
            throw new NotImplementedException();
        }

        public static ScalarViewer operator *(Scalar a, ScalarViewer b)
        {
            if (b.IsKnown(out Scalar value_b))
                return a * value_b;
            throw new NotImplementedException();
        }

        public static ScalarViewer operator *(ScalarViewer a, Scalar b)
        {
            return b * a;
        }

        public static ScalarViewer operator *(ScalarViewer a, ScalarViewer b)
        {
            if (a.IsKnown(out Scalar value_a) && b.IsKnown(out Scalar value_b))
                return value_a * value_b;
            throw new NotImplementedException();
        }

        public static implicit operator Viewer<Scalar>(ScalarViewer source)
        {
            if (source._source is null)
                return source._value;
            return new Viewer<Scalar>(source._source);
        }

        public static implicit operator ScalarViewer(Viewer<Scalar> source)
        {
            if (source.IsKnown(out Scalar value))
                return value;
            return new ScalarViewer(source);
        }

        public static implicit operator Scalar(ScalarViewer viewer)
        {
            if (viewer._source is null)
                return viewer._value;
            return viewer._source.Value;
        }

        public static implicit operator ScalarViewer(Scalar value)
        {
            return new ScalarViewer(value);
        }

        public static implicit operator ScalarViewer(float source)
        {
            return (Scalar)source;
        }

        public static implicit operator ScalarViewer(double source)
        {
            return (Scalar)source;
        }

        public override string ToString()
        {
            if (_source is null)
                return _value.ToString();
            return _source.ToString();
        }
    }

    // TODO: Move form stuff to Alunite.Data
    /// <summary>
    /// A <see cref="Scalar"/> derived from a linear combination of <see cref="Scalar"/> parameters.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct ScalarForm : IEquatable<ScalarForm>
    {
        internal Scalar _bias;
        internal _Term[] _terms;
        private ScalarForm(Scalar bias, _Term[] terms)
        {
            _bias = bias;
            _terms = terms;
        }

        /// <summary>
        /// Describes a term in a <see cref="ScalarForm"/>.
        /// </summary>
        internal struct _Term
        {
            public _Term(uint index, Scalar coeff)
            {
                Index = index;
                Coefficient = coeff;
            }

            /// <summary>
            /// The index of the parameter for this term.
            /// </summary>
            public uint Index;

            /// <summary>
            /// The coefficient for this term.
            /// </summary>
            public Scalar Coefficient;

            public static _Term operator -(_Term term)
            {
                return new _Term(term.Index, -term.Coefficient);
            }

            public static _Term operator *(Scalar factor, _Term term)
            {
                return new _Term(term.Index, factor * term.Coefficient);
            }
        }

        /// <summary>
        /// The zero <see cref="ScalarForm"/>.
        /// </summary>
        public static ScalarForm Zero => new ScalarForm(Scalar.Zero, Array.Empty<_Term>());

        /// <summary>
        /// Constructs a <see cref="ScalarForm"/> whose value is the value of the given parameter.
        /// </summary>
        public static ScalarForm Parameter(uint index)
        {
            // TODO: Reusable arrays
            return new ScalarForm(0, new _Term[] { new _Term(index, 1) });
        }

        /// <summary>
        /// Declares a new variable in a context.
        /// </summary>
        public static ScalarForm Declare(ListBuilder<ScalarForm> vars)
        {
            ScalarForm res = Parameter(vars.Length);
            vars.Append(res);
            return res;
        }

        /// <summary>
        /// Constructs a <see cref="ScalarForm"/> from the given bias and terms.
        /// </summary>
        private static ScalarForm _build(Scalar bias, ReadOnlySpan<_Term> terms)
        {
            _Term[] fTerms = new _Term[terms.Length];
            for (int i = 0; i < fTerms.Length; i++)
                fTerms[i] = terms[i];
            return new ScalarForm(bias, fTerms);
        }

        /// <summary>
        /// Constructs a <see cref="ScalarForm"/> from the given bias and scaled terms.
        /// </summary>
        private static ScalarForm _build(Scalar bias, Scalar scale, ReadOnlySpan<_Term> terms)
        {
            _Term[] fTerms = new _Term[terms.Length];
            for (int i = 0; i < fTerms.Length; i++)
                fTerms[i] = scale * terms[i];
            return new ScalarForm(bias, fTerms);
        }

        /// <summary>
        /// Indicates whether this <see cref="ScalarForm"/> is a parameter, and if so, gets its index.
        /// </summary>
        public bool IsParameter(out uint index)
        {
            if (_bias == 0 && _terms.Length == 1)
            {
                _Term term = _terms[0];
                if (term.Coefficient == 1)
                {
                    index = term.Index;
                    return true;
                }
            }
            index = default;
            return false;
        }

        /// <summary>
        /// Indicates whether this <see cref="ScalarForm"/> is a specific parameter.
        /// </summary>
        public bool IsParameter(uint index)
        {
            if (_bias == 0 && _terms.Length == 1)
            {
                _Term term = _terms[0];
                return term.Index == index && term.Coefficient == 1;
            }
            return false;
        }

        /// <summary>
        /// Indicates whether this <see cref="ScalarForm"/> is a constant, and if so, gets its value.
        /// </summary>
        public bool IsConstant(out Scalar value)
        {
            value = _bias;
            return _terms.Length == 0;
        }
        
        /// <summary>
        /// Evaluates this parameterized <see cref="Scalar"/> using the given arguments.
        /// </summary>
        public Scalar this[List<Scalar> args] => Evaluate(args);

        /// <summary>
        /// Lazily evaluates this parameterized <see cref="Scalar"/> using the given arguments.
        /// </summary>
        public ScalarViewer this[ScalarViewer[] args]
        {
            get
            {
                // TODO: Lazily
                ScalarViewer res = _bias;
                foreach (_Term term in _terms)
                    res += term.Coefficient * args[term.Index];
                return res;
            }
        }

        /// <summary>
        /// Evaluates this form with the given arguments.
        /// </summary>
        public Scalar Evaluate(ReadOnlySpan<Scalar> args)
        {
            Scalar res = _bias;
            foreach (_Term term in _terms)
                res += term.Coefficient * args[(int)term.Index];
            return res;
        }

        /// <summary>
        /// Evaluates this form using the given parameterized arguments.
        /// </summary>
        public ScalarForm Apply(ReadOnlySpan<ScalarForm> args)
        {
            ScalarForm res = _bias;
            foreach (_Term term in _terms)
                res += term.Coefficient * args[(int)term.Index];
            return res;
        }

        /// <summary>
        /// Adds the given index offset to all parameter references in this form.
        /// </summary>
        public ScalarForm Shift(uint offset)
        {
            if (_terms.Length == 0)
                return _bias;
            if (offset == 0)
                return this;
            var nTerms = new _Term[_terms.Length];
            for (uint i = 0; i < (uint)nTerms.Length; i++)
            {
                _Term term = _terms[i];
                nTerms[i] = new _Term(term.Index + offset, term.Coefficient);
            }
            return new ScalarForm(_bias, nTerms);
        }

        /// <summary>
        /// Gets an arbitrary solution (complete variable assignment) for the given variable context.
        /// </summary>
        public static List<Scalar> Solve(ReadOnlySpan<ScalarForm> vars)
        {
            Scalar[] values = new Scalar[vars.Length];
            for (int i = 0; i < vars.Length; i++)
                values[i] = vars[i].Evaluate(values);
            return new List<Scalar>(values);
        }

        /// <summary>
        /// Attempts to make the minimal assignments in the given variable context such that the given
        /// forms will be equal. Returns false if this is not possible.
        /// </summary>
        /// <param name="vars">Provides the current assignments of variables in the context. Each variable may
        /// either be a <see cref="ScalarForm"/> of the lower-index variables, or itself (if unassigned).</param>
        public static bool TryEquate(Span<ScalarForm> vars, ScalarForm a, ScalarForm b)
        {
            return TryApproxEquate(0, vars, a, b);
        }

        /// <summary>
        /// Attempts to make the minimal assignments in the given variable context such that the given
        /// forms will be approximately equal. Returns false if this is not possible.
        /// </summary>
        /// <param name="eps">The maximum allowable difference between the two values.</param>
        /// <param name="vars">Provides the current assignments of variables in the context. Each variable may
        /// either be a <see cref="ScalarForm"/> of the lower-index variables, or itself (if unassigned).</param>
        public static bool TryApproxEquate(Scalar eps, Span<ScalarForm> vars, ScalarForm a, ScalarForm b)
        {
            // Compute difference of forms
            Scalar bias = a._bias - b._bias;
            Span<_Term> terms = stackalloc _Term[vars.Length];
            _add(a._terms, -1, b._terms, terms, out int numTerms);

            // Process the last term until it is assignable, or there are no terms left.
            Span<_Term> tempTerms = stackalloc _Term[vars.Length];
            while (numTerms > 0)
            {
                numTerms--;
                _Term lastTerm = terms[numTerms];
                ref ScalarForm lastTermVar = ref vars[(int)lastTerm.Index];
                if (lastTermVar.IsParameter(lastTerm.Index))
                {
                    // Assign the variable to remaining form
                    Scalar scale = -1 / lastTerm.Coefficient;
                    vars[(int)lastTerm.Index] = _build(scale * bias, scale, terms.Slice(0, numTerms));
                    return true;
                }
                else
                {
                    // Add the term's value to the remaining form
                    bias += lastTermVar._bias * lastTerm.Coefficient;
                    _add(terms.Slice(0, numTerms), lastTerm.Coefficient, lastTermVar._terms, tempTerms, out numTerms);
                    Span<_Term> nTempTerms = terms;
                    terms = tempTerms;
                    tempTerms = nTempTerms;
                }
            }
            return Scalar.Abs(bias) <= eps;
        }

        public static implicit operator ScalarForm(float source)
        {
            return (Scalar)source;
        }

        public static implicit operator ScalarForm(double source)
        {
            return (Scalar)source;
        }

        public static implicit operator ScalarForm(Scalar source)
        {
            return new ScalarForm(source, Array.Empty<_Term>());
        }

        public static ScalarForm operator -(ScalarForm a)
        {
            if (a._terms.Length == 0)
                return new ScalarForm(-a._bias, a._terms);
            _Term[] nTerms = new _Term[a._terms.Length];
            for (int i = 0; i < nTerms.Length; i++)
                nTerms[i] = -a._terms[i];
            return new ScalarForm(-a._bias, nTerms);
        }

        public static ScalarForm operator +(ScalarForm a, ScalarForm b)
        {
            if (a._terms.Length == 0)
                return new ScalarForm(a._bias + b._bias, b._terms);
            if (b._terms.Length == 0)
                return new ScalarForm(a._bias + b._bias, a._terms);
            Span<_Term> nTerms = stackalloc _Term[a._terms.Length + b._terms.Length];
            _add(a._terms, 1, b._terms, nTerms, out int nIndex);
            return _build(a._bias + b._bias, nTerms.Slice(0, nIndex));
        }

        public static ScalarForm operator -(ScalarForm a, ScalarForm b)
        {
            if (a._terms.Length == 0)
                return _build(a._bias - b._bias, -1, b._terms);
            if (b._terms.Length == 0)
                return new ScalarForm(a._bias - b._bias, a._terms);
            Span<_Term> nTerms = stackalloc _Term[a._terms.Length + b._terms.Length];
            _add(a._terms, -1, b._terms, nTerms, out int nIndex);
            return _build(a._bias - b._bias, nTerms.Slice(0, nIndex));
        }

        /// <summary>
        /// Merges the terms (by scaled addition) in the given spans.
        /// </summary>
        private static void _add(
            ReadOnlySpan<_Term> aTerms,
            Scalar bScale, ReadOnlySpan<_Term> bTerms,
            Span<_Term> nTerms, out int nIndex)
        {
            nIndex = 0;
            int aIndex = 0;
            int bIndex = 0;
            _Term aTerm, bTerm;
        seed:
            if (aIndex < aTerms.Length)
            {
                aTerm = aTerms[aIndex++];
                if (bIndex < bTerms.Length)
                {
                    bTerm = bScale * bTerms[bIndex++];
                    goto next;
                }
                else
                {
                    nTerms[nIndex++] = aTerm;
                    while (aIndex < aTerms.Length)
                        nTerms[nIndex++] = aTerms[aIndex++];
                }
            }
            else
            {
                while (bIndex < bTerms.Length)
                    nTerms[nIndex++] = bScale * bTerms[bIndex++];
            }
            return;
        next:
            if (aTerm.Index == bTerm.Index)
            {
                _Term nTerm = new _Term(aTerm.Index, aTerm.Coefficient + bTerm.Coefficient);
                if (nTerm.Coefficient != 0)
                    nTerms[nIndex++] = nTerm;
                goto seed;
            }
            else if (aTerm.Index < bTerm.Index)
            {
                nTerms[nIndex++] = aTerm;
                if (aIndex < aTerms.Length)
                {
                    aTerm = aTerms[aIndex++];
                    goto next;
                }
                else
                {
                    nTerms[nIndex++] = bTerm;
                    while (bIndex < bTerms.Length)
                        nTerms[nIndex++] = bScale * bTerms[bIndex++];
                }
            }
            else
            {
                nTerms[nIndex++] = bTerm;
                if (bIndex < bTerms.Length)
                {
                    bTerm = bScale * bTerms[bIndex++];
                    goto next;
                }
                else
                {
                    nTerms[nIndex++] = aTerm;
                    while (aIndex < aTerms.Length)
                        nTerms[nIndex++] = aTerms[aIndex++];
                }
            }
        }

        public static ScalarForm operator *(Scalar a, ScalarForm b)
        {
            if (a == 0)
                return Zero;
            if (b._terms.Length == 0)
                return a * b._bias;
            var nTerms = new _Term[b._terms.Length];
            for (uint i = 0; i < nTerms.Length; i++)
            {
                _Term term = b._terms[i];
                nTerms[i] = new _Term(term.Index, a * term.Coefficient);
            }
            return new ScalarForm(a * b._bias, nTerms);
        }

        public static ScalarForm operator *(ScalarForm a, Scalar b)
        {
            return b * a;
        }

        public static VectorForm2 operator *(ScalarForm a, Vector2 b)
        {
            throw new NotImplementedException();
        }

        public static VectorForm2 operator *(Vector2 a, ScalarForm b)
        {
            return b * a;
        }

        public static VectorForm3 operator *(ScalarForm a, Vector3 b)
        {
            throw new NotImplementedException();
        }

        public static VectorForm3 operator *(Vector3 a, ScalarForm b)
        {
            return b * a;
        }

        public static ScalarForm operator *(Reflection1 refl, ScalarForm a)
        {
            if (a._terms.Length == 0)
                return refl * a._bias;
            var nTerms = new _Term[a._terms.Length];
            for (uint i = 0; i < nTerms.Length; i++)
            {
                _Term term = a._terms[i];
                nTerms[i] = new _Term(term.Index, refl * term.Coefficient);
            }
            return new ScalarForm(refl * a._bias, nTerms);
        }

        public static bool operator ==(ScalarForm a, ScalarForm b)
        {
            if (a._bias != b._bias)
                return false;
            if (a._terms.Length != b._terms.Length)
                return false;
            for (uint i = 0; i < (uint)a._terms.Length; i++)
            {
                _Term aTerm = a._terms[i];
                _Term bTerm = b._terms[i];
                if (aTerm.Index != bTerm.Index)
                    return false;
                if (aTerm.Coefficient != bTerm.Coefficient)
                    return false;
            }
            return true;
        }

        public static bool operator !=(ScalarForm a, ScalarForm b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is ScalarForm))
                return false;
            return this == (ScalarForm)obj;
        }

        bool IEquatable<ScalarForm>.Equals(ScalarForm other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            int hash = _bias.GetHashCode();
            foreach (_Term term in _terms)
                hash = HashCodeHelper.Combine(hash,
                    term.Index.GetHashCode(),
                    term.Coefficient.GetHashCode());
            return hash;
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            bool first = true;
            if (_bias != Scalar.Zero)
            {
                str.Write(_bias.ToString());
                first = false;
            }
            foreach (_Term term in _terms)
            {
                if (first)
                {
                    first = false;
                    if (term.Coefficient == 1)
                    {
                        str.Write("p" + term.Index.ToSubscriptStringInvariant());
                    }
                    else
                    {
                        str.Write(term.Coefficient.ToString() + "p" + term.Index.ToSubscriptStringInvariant());
                    }
                }
                else if (term.Coefficient == 1)
                {
                    str.Write(" + p" + term.Index.ToSubscriptStringInvariant());
                }
                else if (term.Coefficient >= 0)
                {
                    str.Write(" + " + term.Coefficient.ToString() + "p" + term.Index.ToSubscriptStringInvariant());
                }
                else if (term.Coefficient == -1)
                {
                    str.Write(" - p" + term.Index.ToSubscriptStringInvariant());
                }
                else
                {
                    str.Write(" - " + (-term.Coefficient).ToString() + "p" + term.Index.ToSubscriptStringInvariant());
                }
            }
            if (first) str.Write(_bias.ToString());
            return str.ToString();
        }
    }

    /// <summary>
    /// A helper class for reinterpreting scalar and vector <see cref="Viewer{T}"/>s as linear combinations of
    /// a list of <see cref="ScalarViewer"/> parameters. This allows structures to be described with linear forms
    /// instead of exact values, which delays full evaluation and enables generalization with
    /// <see cref="Abstractor{T}"/>.
    /// </summary>
    public sealed class Linearizer
    {
        private Dictionary<IViewer<Scalar>, uint> _argIndex;
        private ListBuilder<IViewer<Scalar>> _args;
        public Linearizer()
        {
            _argIndex = new Dictionary<IViewer<Scalar>, uint>();
            _args = new ListBuilder<IViewer<Scalar>>();
        }

        /// <summary>
        /// Provides the definitions for the scalar arguments of the linear forms returned by this linearizer so far.
        /// </summary>
        public ScalarViewer[] Arguments
        {
            get
            {
                ScalarViewer[] args = new ScalarViewer[_args.Length];
                for (uint i = 0; i < (uint)args.Length; i++)
                    args[i] = new Viewer<Scalar>(_args[i]);
                return args;
            }
        }

        /// <summary>
        /// Reinterprets the given scalar value as a linear combination of <see cref="Arguments"/>.
        /// </summary>
        public ScalarForm Linearize(ScalarViewer value)
        {
            // TODO: Deconstruct linear forms
            if (value.IsKnown(out Scalar v))
                return v;
            IViewer<Scalar> arg = value._source;
            if (!_argIndex.TryGetValue(arg, out uint index))
            {
                index = _args.Append(arg);
                _argIndex[arg] = index;
            }
            return ScalarForm.Parameter(index);
        }

        /// <summary>
        /// Reinterprets the given vector value as a linear combination of <see cref="Arguments"/>.
        /// </summary>
        public VectorForm2 Linearize(VectorViewer2 value)
        {
            // TODO: Deconstruct linear forms
            if (value.IsKnown(out Vector2 v))
                return v;
            throw new NotImplementedException();
        }

        /// <summary>
        /// Reinterprets the given vector value as a linear combination of <see cref="Arguments"/>.
        /// </summary>
        public VectorForm3 Linearize(VectorViewer3 value)
        {
            // TODO: Deconstruct linear forms
            if (value.IsKnown(out Vector3 v))
                return v;
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// A helper class for finding a general solution to a set of equations involving <see cref="ScalarForm"/>s.
    /// </summary>
    public sealed class FormSolver
    {
        private bool _hasSolution;
        private ScalarForm[] _deps;
        public FormSolver(uint numIndParams, uint numDepParams)
        {
            _hasSolution = true;
            NumIndependentParameters = numIndParams;
            _deps = new ScalarForm[numDepParams];
        }

        /// <summary>
        /// The total number of parameters that may appear in equations, including "independent" parameters that
        /// may not be constrained in solutions.
        /// </summary>
        public uint NumParameters => NumIndependentParameters + NumDependentParameters;

        /// <summary>
        /// The number of "independent", or universally-quantified, parameters that may appear in equations but
        /// not be constrained in solutions. Such parameters occur at the lowest indices.
        /// </summary>
        public uint NumIndependentParameters { get; }

        /// <summary>
        /// The number of "dependent", or existentially-quantified, parameters that may appear in equations and
        /// be constrained in solutions. Such parameters occur at higher indices than independent parameters.
        /// </summary>
        public uint NumDependentParameters => (uint)_deps.Length;

        /// <summary>
        /// Indicates whether there is still a solution for the situation described to the <see cref="FormSolver"/>.
        /// </summary>
        public bool HasSolution => _hasSolution;

        /// <summary>
        /// Sets the given <see cref="ScalarForm"/>s equal in the solution for this <see cref="FormSolver"/>.
        /// </summary>
        public void Equate(ScalarForm a, ScalarForm b)
        {
            if (a._terms.Length == 0 && b._terms.Length == 0)
            {
                _hasSolution &= (a._bias == b._bias);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Sets the given <see cref="VectorForm2"/>s equal in the solution for this <see cref="FormSolver"/>.
        /// </summary>
        public void Equate(VectorForm2 a, VectorForm2 b)
        {
            // TODO: Could be faster
            Equate(a.X, b.X);
            Equate(a.Y, b.Y);
        }

        /// <summary>
        /// Determines whether there is a solution for the situation described to the <see cref="FormSolver"/>, and if so,
        /// gets the solution.
        /// </summary>
        /// <param name="numParams">The number of parameters in the solution.</param>
        /// <param name="deps">The general forms of the dependent parameters in terms of the solution parameters.</param>
        public bool TryGetSolution(out uint numParams, out ScalarForm[] deps)
        {
            if (NumParameters != 0)
                throw new NotImplementedException();
            numParams = 0;
            deps = Array.Empty<ScalarForm>();
            return _hasSolution;
        }

        /// <summary>
        /// Marks the current state of the solver with a backtracking point. This state can then be restored by calling
        /// <see cref="Reset(Point)"/>. If multiple points exist, they must be reset in reverse order of when they
        /// were created.
        /// </summary>
        public Point Mark()
        {
            return new Point
            {
                _hasSolution = _hasSolution
            };
        }

        /// <summary>
        /// Resets the state of the solver to the given point.
        /// </summary>
        public void Reset(Point point)
        {
            _hasSolution = point._hasSolution;
        }

        /// <summary>
        /// Identifies a backtracking point for a <see cref="FormSolver"/>.
        /// </summary>
        public struct Point
        {
            internal bool _hasSolution;
        }
    }
}
