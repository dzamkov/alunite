﻿using System;
using System.Collections.Generic;

namespace Alunite.Data.Abstraction
{
    /// <summary>
    /// An immutable list of arbitrary heterogeneous data.
    /// </summary>
    public struct Record
    {
        // TODO
        
        /// <summary>
        /// The empty <see cref="Record"/>.
        /// </summary>
        public static Record Empty => default;
    }

    /// <summary>
    /// A specialization of <see cref="Viewer{T}"/> for <see cref="Record"/>.
    /// </summary>
    public struct RecordViewer
    {
        internal IViewer[] _fields;

        /// <summary>
        /// A viewer for the empty <see cref="Record"/>.
        /// </summary>
        public static RecordViewer Empty => new RecordViewer
        {
            _fields = Array.Empty<IViewer>()
        };
    }

    /// <summary>
    /// Describes a value of type <typeparamref name="T"/> stored directly in a <see cref="Record"/> of a particular
    /// format.
    /// </summary>
    public struct Field<T>
    {
        /// <summary>
        /// The index of this field in the record.
        /// </summary>
        public uint Index { get; }
    }

    /// <summary>
    /// Describes a value of type <typeparamref name="T"/> derived from a <see cref="Record"/> of a particular format.
    /// </summary>
    public struct Property<T>
    {
        private T _value;

        /// <summary>
        /// Gets the value of this property within the given record.
        /// </summary>
        public T Get(Record record)
        {
            return _value;
        }

        /// <summary>
        /// Gets the value of this property within the given record.
        /// </summary>
        public Viewer<T> Get(RecordViewer record)
        {
            return _value;
        }

        /// <summary>
        /// Constructs a property for a given constant value.
        /// </summary>
        public static implicit operator Property<T>(T value)
        {
            return new Property<T>
            {
                _value = value
            };
        }

        public static implicit operator Property<T>(Field<T> field)
        {
            throw new NotImplementedException();
        }
    }
}
