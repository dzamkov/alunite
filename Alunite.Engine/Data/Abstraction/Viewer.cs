﻿using System;
using System.Collections.Generic;

#pragma warning disable CS0660
#pragma warning disable CS0661

namespace Alunite.Data.Abstraction
{
    /// <summary>
    /// A helper class for lazily inspecting a value of type <typeparamref name="T"/>. This type is logically
    /// equivalent to <typeparamref name="T"/>, but may bring benefits related to performance and abstraction.
    /// </summary>
    public struct Viewer<T>
    {
        internal IViewer<T> _source;
        public Viewer(IViewer<T> source)
        {
            _source = source;
        }

        /// <summary>
        /// Checks whether this <see cref="Viewer{T}"/> has been fully evaluated, and if so, gets its value.
        /// </summary>
        public bool IsKnown(out T value)
        {
            if (_source is _Viewer<T>)
            {
                value = ((_Viewer<T>)_source).Value;
                return true;
            }
            value = default;
            return false;
        }

        /// <summary>
        /// Determines whether two <see cref="Viewer{T}"/>'s have the same value. This may require the viewers
        /// to be evaluated.
        /// </summary>
        public static bool operator ==(Viewer<T> a, Viewer<T> b)
        {
            if (Viewer.AreSame(a, b))
                return true;
            return EqualityHelper.Equals((T)a, (T)b);
        }

        /// <summary>
        /// Determines whether two <see cref="Viewer{T}"/>'s have a different value. This may require the viewers
        /// to be evaluated.
        /// </summary>
        public static bool operator !=(Viewer<T> a, Viewer<T> b)
        {
            return !(a == b);
        }

        public static implicit operator T(Viewer<T> viewer)
        {
            return viewer._source.Value;
        }

        public static implicit operator Viewer<T>(T source)
        {
            if (source is IViewer<T>)
                return new Viewer<T>((IViewer<T>)source);
            return new Viewer<T>(new _Viewer<T>(source));
        }
    }

    /// <summary>
    /// An <see cref="IViewer{T}"/> of some type.
    /// </summary>
    public interface IViewer
    {

    }

    /// <summary>
    /// Provides a value for a <see cref="Viewer{T}"/>.
    /// </summary>
    public interface IViewer<T> : IViewer
    {
        /// <summary>
        /// Gets the underlying value for the viewer.  Note that this forces a strict evaluation of the value, so this
        /// should be avoided or deferred as much as possible.
        /// </summary>
        T Value { get; }
    }

    /// <summary>
    /// Contains functions related to <see cref="Viewer{T}"/>s.
    /// </summary>
    public static class Viewer
    {
        /// <summary>
        /// Constructs a <see cref="Viewer{T}"/> for a value selected from another <see cref="Viewer{T}"/> based on
        /// a given function.
        /// </summary>
        public static Viewer<K> Select<T, K>(this Viewer<T> viewer, Func<T, K> func)
        {
            // TODO: Lazily
            return func(viewer);
        }

        /// <summary>
        /// Constructs a <see cref="ScalarViewer"/> for a value selected from another <see cref="Viewer{T}"/> based on
        /// a given function.
        /// </summary>
        public static ScalarViewer Select<T>(this Viewer<T> viewer, Func<T, Scalar> func)
        {
            // TODO: Lazily
            return func(viewer);
        }

        /// <summary>
        /// Determines whether two <see cref="Viewer{T}"/>'s necessarily contain the same value without evaluating
        /// them. This may return false negatives.
        /// </summary>
        public static bool AreSame<T>(Viewer<T> a, Viewer<T> b)
        {
            return a._source.Equals(b._source);
        }
    }

    /// <summary>
    /// A <see cref="IViewer{T}"/> wrapper over a known value.
    /// </summary>
    internal sealed class _Viewer<T> : IViewer<T>
    {
        public _Viewer(T value)
        {
            Value = value;
        }

        /// <summary>
        /// The value this wrapper is for.
        /// </summary>
        public T Value { get; }

        public override bool Equals(object obj)
        {
            if (!(obj is _Viewer<T>))
                return false;
            return EqualityHelper.Equals(Value, ((_Viewer<T>)obj).Value);
        }

        public override int GetHashCode()
        {
            return EqualityHelper.GetHashCode(Value);
        }
    }
}