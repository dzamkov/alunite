﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;

using Alunite.Diagnostics;

namespace Alunite.Data
{
    /// <summary>
    /// A finite set of values of type <typeparamref name="T"/>.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    [DebuggerTypeProxy(typeof(EnumerableDebugView<>))]
    public struct Set<T> : ISet<T>, IEnumerable<T>, IEquatable<Set<T>>, IPrintable
    {
        internal _Entry[] _entries;
        internal Set(_Entry[] entries)
        {
            _entries = entries;
        }

        /// <summary>
        /// Describes an entry in the hashtable for a <see cref="Set{T}"/>.
        /// </summary>
        internal struct _Entry
        {
            /// <summary>
            /// The hash code for this entry, or zero if this entry is empty.
            /// </summary>
            public int Hash;

            /// <summary>
            /// The value for this entry, if <see cref="Hash"/> is not zero.
            /// </summary>
            public T Value;

            /// <summary>
            /// The empty entry.
            /// </summary>
            public static _Entry Empty => new _Entry
            {
                Hash = 0,
                Value = default
            };
        }
        
        /// <summary>
        /// The empty <see cref="Set{T}"/>.
        /// </summary>
        public static Set<T> None => new Set<T>(_emptyEntries);

        /// <summary>
        /// Constructs a <see cref="Set{T}"/> consisting of a single value.
        /// </summary>
        public static Set<T> Singleton(T value)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// A table for <see cref="_entries"/> that is always empty.
        /// </summary>
        internal static readonly _Entry[] _emptyEntries = new[] { _Entry.Empty };

        /// <summary>
        /// Indicates whether this is <see cref="None"/>.
        /// </summary>
        public bool IsNone => Size == 0;

        /// <summary>
        /// Determines whether this set consists of a single member, and if so, gets its value.
        /// </summary>
        public bool IsSingleton(out T value)
        {
            int i;
            for (i = 0; i < _entries.Length; i++)
            {
                _Entry entry = _entries[i];
                if (entry.Hash != 0)
                {
                    value = entry.Value;
                    goto findOthers;
                }
            }
            value = default;
            return false;

        findOthers:
            for (i++; i < _entries.Length; i++)
                if (_entries[i].Hash != 0)
                    return false;
            return true;
        }

        /// <summary>
        /// The number of members in this set.
        /// </summary>
        public uint Size
        {
            get
            {
                uint size = 0;
                for (int i = 0; i < _entries.Length; i++)
                    if (_entries[i].Hash != 0)
                        size++;
                return size;
            }
        }

        /// <summary>
        /// Indicates whether this set contains the given value.
        /// </summary>
        public bool Contains(T value)
        {
            int hash = _hash(value);
            uint bin = _entries.GetBinPow2(hash);
            while (_entries[bin].Hash != 0)
            {
                if (EqualityHelper.Equals(value, _entries[bin].Value))
                    return true;
                bin = _entries.Next(bin);
            }
            return false;
        }

        /// <summary>
        /// Computes the hash code for the given value, automatically "correcting" hash codes of zero.
        /// </summary>
        internal static int _hash(T value)
        {
            int hash = EqualityHelper.GetHashCode(value);
            if (hash == 0)
                hash = 1;
            return hash;
        }

        /// <summary>
        /// Gets an enumerator for the items in this set, in no particular order.
        /// </summary>
        public Enumerator GetEnumerator()
        {
            return new Enumerator(_entries);
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// An enumerator for a <see cref="Set{T}"/>
        /// </summary>
        public struct Enumerator : IEnumerator<T>
        {
            private _Entry[] _entries;
            private uint _index;
            internal Enumerator(_Entry[] entries)
            {
                _entries = entries;
                _index = uint.MaxValue;
            }

            /// <summary>
            /// Gets the current item for this enumerator.
            /// </summary>
            public T Current => _entries[_index].Value;

            /// <summary>
            /// Advances to the next, or first, item.
            /// </summary>
            public bool MoveNext()
            {
                unchecked
                {
                    while (++_index < (uint)_entries.Length)
                        if (_entries[_index].Hash != 0)
                            return true;
                    return false;
                }
            }

            /// <summary>
            /// Resets the enumerator to before the beginning of the first item.
            /// </summary>
            public void Reset()
            {
                _index = uint.MaxValue;
            }

            object System.Collections.IEnumerator.Current => Current;

            void IDisposable.Dispose() { }
        }

        public static Set<T> operator |(Set<T> a, Set<T> b)
        {
            throw new NotImplementedException();
        }

        public static Set<T> operator &(Set<T> a, Set<T> b)
        {
            throw new NotImplementedException();
        }

        public static bool operator ==(Set<T> a, Set<T> b)
        {
            if (a.Size != b.Size)
                return false;
            foreach (T member in a)
                if (!b.Contains(member))
                    return false;
            return true;
        }

        public static bool operator !=(Set<T> a, Set<T> b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Set<T>))
                return false;
            return this == (Set<T>)obj;
        }

        bool IEquatable<Set<T>>.Equals(Set<T> other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            int hash = 0x15bdc401;
            for (int i = 0; i < _entries.Length; i++)
                hash = unchecked(hash + _entries[i].Hash);
            return hash;
        }

        public override string ToString()
        {
            return this.ToShortString();
        }

        void IPrintable.Print(IPrinter printer, Precedence prec)
        {
            if (IsNone)
            {
                printer.WriteItem('∅');
            }
            else
            {
                printer.WriteItem('{');
                bool isFirst = true;
                foreach (var entry in this)
                {
                    if (isFirst)
                        isFirst = false;
                    else
                        printer.Write(", ");
                    printer.Print(Precedence.Free, entry);
                }
                printer.WriteItem('}');
            }
        }
    }

    /// <summary>
    /// A helper class for constructing a <see cref="Set{T}"/> by incrementally adding and removing members.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    [DebuggerTypeProxy(typeof(EnumerableDebugView<>))]
    public sealed class SetBuilder<T> : ISet<T>, IEnumerable<T>, IBuilder<Set<T>>
    {
        private Set<T>._Entry[] _entries;
        private uint _capacity;
        public SetBuilder() // TODO: Make private in favor of CreateEmpty
        {
            _entries = Set<T>._emptyEntries;
            _capacity = 0;
        }

        /// <summary>
        /// Creates a new initially-empty <see cref="SetBuilder{T}"/>.
        /// </summary>
        public static SetBuilder<T> CreateEmpty()
        {
            return new SetBuilder<T>();
        }

        /// <summary>
        /// Includes the given value in the set.
        /// </summary>
        /// <returns>True if the value was added. False if the value was already in the set.</returns>
        public bool Include(T value)
        {
            // Check for existing entry
            int hash = Set<T>._hash(value);
        retry:
            uint bin = _entries.GetBinPow2(hash);
            while (_entries[bin].Hash != 0)
            {
                if (EqualityHelper.Equals(value, _entries[bin].Value))
                    return false;
                bin = _entries.Next(bin);
            }

            // Add new entry
            if (_capacity == 0)
            {
                _expand();
                goto retry;
            }
            else
            {
                _capacity--;
                ref Set<T>._Entry entry = ref _entries[bin];
                entry.Hash = hash;
                entry.Value = value;
                return true;
            }
        }

        /// <summary>
        /// Includes all the members from the given set in this set.
        /// </summary>
        public void Union(Set<T> set)
        {
            foreach (T member in set)
                Include(member);
        }

        /// <summary>
        /// Excludes the given value from the set.
        /// </summary>
        public void Exclude(T value)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The number of members in this set.
        /// </summary>
        public uint Size
        {
            get
            {
                uint size = 0;
                for (int i = 0; i < _entries.Length; i++)
                    if (_entries[i].Hash != 0)
                        size++;
                return size;
            }
        }

        /// <summary>
        /// Indicates whether this set contains the given value.
        /// </summary>
        public bool Contains(T value)
        {
            int hash = Set<T>._hash(value);
            uint bin = _entries.GetBinPow2(hash);
            while (_entries[bin].Hash != 0)
            {
                if (EqualityHelper.Equals(value, _entries[bin].Value))
                    return true;
                bin = _entries.Next(bin);
            }
            return false;
        }

        /// <summary>
        /// Enlarges <see cref="_entries"/> in order to accommodate more entries.
        /// </summary>
        private void _expand()
        {
            Set<T>._Entry[] nEntries = new Set<T>._Entry[Math.Max(8, _entries.Length * 2)];
            _capacity = _getCapacity(nEntries.Length);
            for (uint i = 0; i < (uint)_entries.Length; i++)
            {
                if (_entries[i].Hash != 0)
                {
                    int hash = _entries[i].Hash;
                    uint bin = nEntries.GetBinPow2(hash);
                    while (nEntries[bin].Hash != 0)
                        bin = nEntries.Next(bin);
                    nEntries[bin] = _entries[i];
                    _capacity--;
                }
            }
            _entries = nEntries;
        }

        /// <summary>
        /// Gets the capacity of this builder when <see cref="_entries"/> is the given size.
        /// </summary>
        private static uint _getCapacity(int size)
        {
            return 3 + (uint)size * 3 / 5;
        }

        /// <summary>
        /// Gets an enumerator for the items in this set, in no particular order.
        /// </summary>
        public Set<T>.Enumerator GetEnumerator()
        {
            return new Set<T>.Enumerator(_entries);
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Gets the <see cref="Set{T}"/> resulting from this builder.
        /// </summary>
        public Set<T> Finish()
        {
            return new Set<T>(_entries);
        }

        /// <summary>
        /// Constructs a list consisting of the members of this set in some arbitrary order.
        /// </summary>
        public List<T> FinishSortArbitrary()
        {
            T[] _items = new T[Size];
            uint index = 0;
            for (uint i = 0; i < (uint)_entries.Length; i++)
                if (_entries[i].Hash != 0)
                    _items[index++] = _entries[i].Value;
            Debug.Assert(index == (uint)_items.Length);
            return new List<T>(_items);
        }

        public override string ToString()
        {
            return Finish().ToString();
        }
    }

    /// <summary>
    /// Contains helper functions and algorithms related to <see cref="Set{T}"/>.
    /// </summary>
    public static class Set
    {
        /// <summary>
        /// Gets the empty set of the given type.
        /// </summary>
        public static Set<T> None<T>()
        {
            return Set<T>.None;
        }

        /// <summary>
        /// Constructs a set consisting of the given members.
        /// </summary>
        public static Set<T> Of<T>(params T[] members)
        {
            SetBuilder<T> builder = new SetBuilder<T>();
            foreach (T member in members)
                builder.Include(member);
            return builder.Finish();
        }
    }

    /// <summary>
    /// A set of values of type <typeparamref name="T"/>.
    /// </summary>
    public interface ISet<T>
    {
        /// <summary>
        /// Indicates whether the given value is in this set.
        /// </summary>
        bool Contains(T value);
    }
}