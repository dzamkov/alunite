﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Alunite.Data
{
    /// <summary>
    /// A streaming interface for reading items from a collection. This is essentially <see cref="IEnumerator{T}"/>,
    /// but faster and without the cruft.
    /// </summary>
    public interface IIterator<T> : IDisposable
    {
        /// <summary>
        /// Attempts to get the next item from the iterator, returning false if the end of the collection
        /// has been reached.
        /// </summary>
        bool TryNext(out T value);
    }

    /// <summary>
    /// An <see cref="IIterator{T}"/> which has the ability to selectively remove items as they are iterated.
    /// </summary>
    public interface IFilterIterator<T> : IIterator<T>
    {
        /// <summary>
        /// Removes the current item from the collection. This may only be called if the previous call to
        /// <see cref="IIterator{T}.TryNext(out T)"/> returned true. This may be called at most once
        /// per item.
        /// </summary>
        void Remove();
    }

    /// <summary>
    /// An <see cref="IEnumerator{T}"/> wrapper over an <see cref="IIterator{T}"/>.
    /// </summary>
    public struct IteratorEnumerator<TIterator, T> : IEnumerator<T>
        where TIterator : IIterator<T>
    {
        internal T _current;
        public IteratorEnumerator(TIterator iterator)
        {
            Iterator = iterator;
            _current = default;
        }

        /// <summary>
        /// The underlying <see cref="IIterator{T}"/> for this enumerator.
        /// </summary>
        public TIterator Iterator;

        /// <summary>
        /// The current item of the enumeration. This is undefined when the enumerator is in its initial state.
        /// </summary>
        public T Current => _current;

        /// <summary>
        /// Attempts to advance to the next item, returning false if the end of the collection has been reached.
        /// </summary>
        public bool MoveNext()
        {
            return Iterator.TryNext(out _current);
        }

        /// <summary>
        /// Indicates that the enumerator will no longer be used.
        /// </summary>
        public void Dispose()
        {
            Iterator.Dispose();
        }
        
        object IEnumerator.Current => Current;
        
        void IEnumerator.Reset()
        {
            throw new NotSupportedException();
        }
    }
}
