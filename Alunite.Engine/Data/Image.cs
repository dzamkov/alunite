﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

using Alunite.Data.Geometry;

namespace Alunite.Data
{
    /// <summary>
    /// A two-dimensional array of values of type <typeparamref name="T"/>. This can also be interpreted as
    /// a non-empty <see cref="List{T}"/> of <see cref="List{T}"/>s where the inner lists are all of the same length.
    /// </summary>
    public struct Image2<T>
    {
        private uint _size_x;
        internal T[] _data;
        internal Image2(uint size_x, T[] data)
        {
            _size_x = size_x;
            _data = data;
        }

        /// <summary>
        /// The dimensions of this image. Both components must be non-zero.
        /// </summary>
        public Index2 Size => new Index2(_size_x, (uint)_data.Length / _size_x);
        
        /// <summary>
        /// Gets the value of a pixel in this image.
        /// </summary>
        public T this[Index2 pos]
        {
            get
            {
                if (!(pos.X < _size_x))
                    throw new IndexOutOfRangeException();
                return _data[pos.Y * _size_x + pos.X];
            }
        }

        /// <summary>
        /// Constructs a <see cref="List{T}"/> of the items that occur at the given X index.
        /// </summary>
        public List<T> At_X(uint x)
        {
            if (!(x < _size_x))
                throw new IndexOutOfRangeException();
            uint size_y = (uint)_data.Length / _size_x;
            T[] items = new T[size_y];
            uint index = x;
            for (uint y = 0; y < size_y; y++)
            {
                items[y] = _data[index];
                index += _size_x;
            }
            return new List<T>(items);
        }

        /// <summary>
        /// Constructs a <see cref="List{T}"/> of the items that occur at the given Y index.
        /// </summary>
        public SliceList<T> At_Y(uint y)
        {
            uint offset = y * _size_x;
            if (!(offset < (uint)_data.Length))
                throw new IndexOutOfRangeException();
            return new SliceList<T>(_data, offset, _size_x);
        }

        public static explicit operator Image2<T>(List<List<T>> items)
        {
            // Verify dimensions
            if (items.Length == 0)
                throw new ArgumentException("Non-empty list required", nameof(items));
            uint len = items[0].Length;
            if (items.Length == 0)
                throw new ArgumentException("Inner lists may not be empty", nameof(items));
            for (uint i = 0; i < items.Length; i++)
                if (items[i].Length != len)
                    throw new ArgumentException("Inner lists must all be of the same length", nameof(items));

            // Construct data array
            T[] data = new T[items.Length * len];
            uint index = 0;
            for (uint y = 0; y < items.Length; y++)
            {
                List<T> row = items[y];
                for (uint x = 0; x < len; x++)
                    data[index++] = row[x];
            }

            // Finalize image
            return new Image2<T>(len, data);
        }
    }

    /// <summary>
    /// Contains helper functions related to <see cref="Image2{T}"/>.
    /// </summary>
    public static class Image2
    {
        /// <summary>
        /// Constructs a 1x1 image with the given pixel value.
        /// </summary>
        public static Image2<T> Singleton<T>(T value)
        {
            return new Image2<T>(1, new T[] { value });
        }
    }

    /// <summary>
    /// A vector consisting of two non-negative integral components.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct Index2
    {
        public uint X;
        public uint Y;
        public Index2(uint x, uint y)
        {
            X = x;
            Y = y;
        }

        /// <summary>
        /// The zero index.
        /// </summary>
        public static Index2 Zero => new Index2(0, 0);

        public static Index2 operator +(Index2 a, Index2 b)
        {
            return new Index2(a.X + b.X, a.Y + b.Y);
        }

        public static explicit operator Index2(Vector2i source)
        {
            return new Index2((uint)source.X, (uint)source.Y);
        }

        public static implicit operator Vector2i(Index2 source)
        {
            return new Vector2i((int)source.X, (int)source.Y);
        }

        public override string ToString()
        {
            return $"({X}, {Y})";
        }
    }
    
    /// <summary>
    /// A helper class for constructing an <see cref="Image2{T}"/> by incremental element modifications.
    /// </summary>
    public sealed class Image2Builder<T> : IBuilder<Image2<T>>
    {
        private uint _size_x;
        private T[] _data;
        public Image2Builder(uint size_x, uint size_y)
        {
            _size_x = size_x;
            _data = new T[size_x * size_y];
        }

        /// <summary>
        /// The size of this image. Both components must be non-zero.
        /// </summary>
        public Index2 Size => new Index2(_size_x, (uint)_data.Length / _size_x);

        /// <summary>
        /// Gets or sets the value of an element of this image.
        /// </summary>
        public ref T this[Index2 pos]
        {
            get
            {
                if (!(pos.Y < _size_x))
                    throw new IndexOutOfRangeException();
                return ref _data[pos.Y * _size_x + pos.X];
            }
        }

        /// <summary>
        /// Gets the <see cref="Image2{T}"/> resulting from this builder.
        /// </summary>
        public Image2<T> Finish()
        {
            return new Image2<T>(_size_x, _data);
        }
    }

    /// <summary>
    /// A vector consisting of three non-negative integral components.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct Index3
    {
        public uint X;
        public uint Y;
        public uint Z;
        public Index3(uint x, uint y, uint z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public Index3(Index2 x_y, uint z)
        {
            X = x_y.X;
            Y = x_y.Y;
            Z = z;
        }

        /// <summary>
        /// The zero index.
        /// </summary>
        public static Index3 Zero => new Index3(0, 0, 0);

        public static Index3 operator +(Index3 a, Index3 b)
        {
            return new Index3(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        }

        public static explicit operator Index3(Vector3i source)
        {
            return new Index3((uint)source.X, (uint)source.Y, (uint)source.Z);
        }

        public static implicit operator Vector3i(Index3 source)
        {
            return new Vector3i((int)source.X, (int)source.Y, (int)source.Z);
        }

        public override string ToString()
        {
            return $"({X}, {Y}, {Z})";
        }
    }
}