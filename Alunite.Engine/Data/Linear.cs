﻿using System;
using System.Collections.Generic;

using Alunite.Data.Geometry;

namespace Alunite.Data
{
    /// <summary>
    /// Defines a vector space over the values in <typeparamref name="T"/>.
    /// </summary>
    public abstract class Linear<T>
    {
        /// <summary>
        /// Adds two vectors in the vector space.
        /// </summary>
        public abstract T Add(T a, T b);

        /// <summary>
        /// Multiplies a vector by the given scalar.
        /// </summary>
        public abstract T Scale(T a, Scalar b);
    }
}