﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;

namespace Alunite.Data.Serialization
{
    /// <summary>
    /// Describes a method of serializing values of type <typeparamref name="T"/> as binary data.
    /// Note that the domain of a encoding need not included every possible value of <typeparamref name="T"/>.
    /// </summary>
    public abstract class Encoding<T> : _IEncodingAny
    {
        public Encoding(BinarySize size, BinaryAlign align)
        {
            Size = size;
            Align = align;
        }

        /// <summary>
        /// The possible sizes of a value under this encoding.
        /// </summary>
        public BinarySize Size { get; }

        /// <summary>
        /// The stream alignment required for an aligned read or write of a value using this encoding.
        /// </summary>
        public BinaryAlign Align { get; }

        /// <summary>
        /// Reads a value from the given <see cref="IBinaryReader"/> using this encoding. For performance, the current
        /// read buffer and read head are passed directly. Violation of any assumptions will result in undefined behavior.
        /// </summary>
        /// <param name="head">A reference to the current read head within <paramref name="buffer"/>. Upon calling,
        /// there should be at least <see cref="BinarySize.Min"/> or <see cref="Binary.MinBufferCapacity"/> (whichever
        /// is smaller) bytes between the read head and the end of <paramref name="buffer"/>.</param>
        /// <param name="buffer">A snapshot of the latest buffer returned from <paramref name="reader"/>.</param>
        /// <param name="reader">The reader for the stream being accessed. This may be <see cref="null"/> if all remaining
        /// stream data is already available in <paramref name="buffer"/>.</param>
        /// <returns>The read head within <paramref name="buffer"/> after reading.</returns>
        public abstract ref byte FastRead(ref byte head, ref ReadOnlySpan<byte> buffer, IBinaryReader reader, out T value);
        
        /// <summary>
        /// Reads a value from the given <see cref="IBinaryReader"/> using this encoding, assuming that the read head is
        /// aligned according to <see cref="Align"/>. For performance, the current read buffer and read head are passed
        /// directly. Violation of any assumptions will result in undefined behavior.
        /// </summary>
        /// <param name="head">A reference to the current read head within <paramref name="buffer"/>. Upon calling,
        /// there should be at least <see cref="BinarySize.Min"/> or <see cref="Binary.MinBufferCapacity"/> (whichever
        /// is smaller) bytes between the read head and the end of <paramref name="buffer"/>.</param>
        /// <param name="buffer">A snapshot of the latest buffer returned from <paramref name="reader"/>.</param>
        /// <param name="reader">The reader for the stream being accessed. This may be <see cref="null"/> if all remaining
        /// stream data is already available in <paramref name="buffer"/>.</param>
        /// <returns>The read head within <paramref name="buffer"/> after reading.</returns>
        public abstract ref byte FastReadAligned(ref byte head, ref ReadOnlySpan<byte> buffer, IBinaryReader reader, out T value);

        /// <summary>
        /// Writes a value to the given <see cref="IBinaryWriter"/> using this encoding. For performance, the current
        /// write buffer nd write head are passed directly. Violation of any assumptions will result in undefined behavior.
        /// </summary>
        /// <param name="head">A reference to the current write head within <paramref name="buffer"/>. Upon calling,
        /// there should be at least <see cref="BinarySize.Min"/> or <see cref="Binary.MinBufferCapacity"/> (whichever
        /// is smaller) bytes between the write head and the end of <paramref name="buffer"/>.</param>
        /// <param name="buffer">A snapshot of the latest buffer returned from <paramref name="writer"/>.</param>
        /// <param name="writer">The writer for the stream being accessed. This may be null if there is enough space
        /// in <paramref name="buffer"/> to write all remaining data.</param>
        /// <returns>The write head within <paramref name="buffer"/> after writing.</returns>
        public abstract ref byte FastWrite(ref byte head, ref Span<byte> buffer, IBinaryWriter writer, in T value);

        /// <summary>
        /// Writes a value to the given <see cref="IBinaryWriter"/> using this encoding, assuming that the write head is
        /// aligned according to <see cref="Align"/>. For performance, the current write buffer and write head are passed
        /// directly. Violation of any assumptions will result in undefined behavior.
        /// </summary>
        /// <param name="head">A reference to the current write head within <paramref name="buffer"/>. Upon calling,
        /// there should be at least <see cref="BinarySize.Min"/> or <see cref="Binary.MinBufferCapacity"/> (whichever
        /// is smaller) bytes between the write head and the end of <paramref name="buffer"/>.</param>
        /// <param name="buffer">A snapshot of the latest buffer returned from <paramref name="writer"/>.</param>
        /// <param name="writer">The writer for the stream being accessed. This may be null if there is enough space
        /// in <paramref name="buffer"/> to write all remaining data.</param>
        /// <returns>The write head within <paramref name="buffer"/> after writing.</returns>
        public abstract ref byte FastWriteAligned(ref byte head, ref Span<byte> buffer, IBinaryWriter writer, in T value);
        
        // TODO: This virtual function is awfully specific. Can we generalize it to be more useful?
        /// <summary>
        /// Constructs an <see cref="IBinaryReader"/> to read an encoded list of values.
        /// </summary>
        public virtual IBinaryReader Encode(List<T> source)
        {
            throw new NotImplementedException();
        }
        
        Type _IEncodingAny.Type => typeof(T);
    }

    /// <summary>
    /// A <see cref="Encoding{T}"/> which adheres to the unmanaged representation of <typeparamref name="T"/>, with the
    /// size of <typeparamref name="T"/> being at most <see cref="Binary.MinBufferCapacity"/> to allow blitting.
    /// </summary>
    public sealed unsafe class BlittableEncoding<T> : Encoding<T>
        where T : unmanaged
    {
        internal BlittableEncoding(uint size, uint align)
            : base(size, align)
        {
            Debug.Assert(size == (uint)sizeof(T));
            Debug.Assert(align % Util.AlignOf<T>() == 0);
            Debug.Assert(size <= Binary.MinBufferCapacity);
        }

        public override ref byte FastRead(
            ref byte head, ref ReadOnlySpan<byte> buffer,
            IBinaryReader reader, out T value)
        {
            value = Unsafe.ReadUnaligned<T>(ref head);
            return ref Unsafe.AddByteOffset(ref head, (IntPtr)sizeof(T));
        }

        public override ref byte FastReadAligned(
            ref byte head, ref ReadOnlySpan<byte> buffer,
            IBinaryReader reader, out T value)
        {
            value = Unsafe.As<byte, T>(ref head);
            return ref Unsafe.AddByteOffset(ref head, (IntPtr)sizeof(T));
        }

        public override ref byte FastWrite(
            ref byte head, ref Span<byte> buffer,
            IBinaryWriter writer, in T value)
        {
            Unsafe.WriteUnaligned(ref head, value);
            return ref Unsafe.AddByteOffset(ref head, (IntPtr)sizeof(T));
        }

        public override ref byte FastWriteAligned(
            ref byte head, ref Span<byte> buffer,
            IBinaryWriter writer, in T value)
        {
            Unsafe.As<byte, T>(ref head) = value;
            return ref Unsafe.AddByteOffset(ref head, (IntPtr)sizeof(T));
        }

        public override IBinaryReader Encode(List<T> source)
        {
            return new MemoryReader<T>(source._items, 0);
        }
    }

    /// <summary>
    /// A wrapper over an <see cref="Encoding{T}"/> which adds the minimal amount of trailing padding needed to ensure
    /// that the <see cref="BinarySize.Increment"/> of the value is divisible by a particular power-of-2. This is useful
    /// for maintaining alignment of variable-size encodings.
    /// </summary>
    public sealed class PaddedEncoding<T> : Encoding<T>
    {
        internal PaddedEncoding(Encoding<T> inner, ushort inc)
            : base(_size(inner.Size, inc), new BinaryAlign(inc, inner.Align._offset))
        {
            Inner = inner;
        }

        /// <summary>
        /// Computes the <see cref="BinarySize"/> for a padded encoding.
        /// </summary>
        private static BinarySize _size(BinarySize innerSize, ushort inc)
        {
            if (innerSize.Multiplicity == ushort.MaxValue)
                return new BinarySize(innerSize.Min, inc, ushort.MaxValue);
            uint maxExtraSize = (uint)innerSize.Increment * innerSize.Multiplicity;
            uint mult = (maxExtraSize + inc - 1) / inc;
            if (mult > ushort.MaxValue)
                return new BinarySize(innerSize.Min, inc, ushort.MaxValue);
            return new BinarySize(innerSize.Min, inc, (ushort)mult);
        }

        /// <summary>
        /// The unpadded inner encoding for this padded encoding.
        /// </summary>
        public Encoding<T> Inner { get; }

        public override ref byte FastRead(ref byte head, ref ReadOnlySpan<byte> buffer, IBinaryReader reader, out T value)
        {
            throw new NotImplementedException();
        }

        public override ref byte FastReadAligned(ref byte head, ref ReadOnlySpan<byte> buffer, IBinaryReader reader, out T value)
        {
            throw new NotImplementedException();
        }

        public override ref byte FastWrite(ref byte head, ref Span<byte> buffer, IBinaryWriter writer, in T value)
        {
            throw new NotImplementedException();
        }

        public override ref byte FastWriteAligned(ref byte head, ref Span<byte> buffer, IBinaryWriter writer, in T value)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// A <see cref="Encoding{T}"/> of some type.
    /// </summary>
    internal interface _IEncodingAny
    {
        /// <summary>
        /// The type of this <see cref="Encoding{T}"/>.
        /// </summary>
        Type Type { get; }

        /// <summary>
        /// The possible sizes of a value under this encoding.
        /// </summary>
        BinarySize Size { get; }

        /// <summary>
        /// The stream alignment required for an aligned read or write of a value using this encoding.
        /// </summary>
        BinaryAlign Align { get; }
    }
    
    /// <summary>
    /// Contains functions related to <see cref="Encoding{T}"/>s.
    /// </summary>
    public static partial class Encoding
    {
        /// <summary>
        /// Gets the preferred non-portable <see cref="Encoding{T}"/> for a type. This will throw
        /// an exception if no eligible encoding exists.
        /// </summary>
        public static Encoding<T> Default<T>(EncodingStrategy strat)
        {
            if (!TryDefault(strat, out Encoding<T> encoding))
                throw new Exception("No available encoding");
            return encoding;
        }

        /// <summary>
        /// Attempts to get the preferred non-portable <see cref="Encoding{T}"/> for a type,
        /// returning <see cref="false"/> if no eligible encoding exists.
        /// </summary>
        public static bool TryDefault<T>(EncodingStrategy strat, out Encoding<T> encoding)
        {
            return EncodingResolver.Default(strat).TryResolve(Domain<T>.General, out encoding);
        }

        /// <summary>
        /// Gets the standard portable <see cref="Encoding{T}"/> for a type. This will throw
        /// an exception if no eligible encoding exists.
        /// </summary>
        public static Encoding<T> Standard<T>()
        {
            if (!TryStandard(out Encoding<T> encoding))
                throw new Exception("No available encoding");
            return encoding;
        }

        /// <summary>
        /// Attempts to get the standard portable non-portable <see cref="Encoding{T}"/> for a type,
        /// returning <see cref="false"/> if no eligible encoding exists.
        /// </summary>
        public static bool TryStandard<T>(out Encoding<T> encoding)
        {
            return EncodingResolver.Standard.TryResolve(Domain<T>.General, out encoding);
        }
    }
}