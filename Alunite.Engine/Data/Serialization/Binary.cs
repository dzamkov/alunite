﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Alunite.Data.Serialization
{
    /// <summary>
    /// Contains helper functions related to binary data sources.
    /// </summary>
    public static class Binary
    {
        /// <summary>
        /// The minimum buffer size that an <see cref="IBinaryReader"/> or <see cref="IBinaryWriter"/>
        /// must support. The buffer need not actually be this size until requested by a call to
        /// <see cref="IBinaryReader.Prepare(uint)"/> or <see cref="IBinaryWriter.Prepare(uint)"/>.
        /// </summary>
        public const uint MinBufferCapacity = 64;

        /// <summary>
        /// Advances a buffer by the minimum number of bytes such that it ends up aligned with the specified
        /// byte boundary.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static unsafe void SkipAlign(ref ReadOnlySpan<byte> buffer, uint align)
        {
            uint ptr = (uint)Unsafe.AsPointer(ref Unsafe.AsRef(in buffer[0]));
            uint skip = unchecked((uint)-ptr & (align - 1));
            buffer = buffer.Slice((int)skip);
        }

        /// <summary>
        /// Advances a buffer by the minimum number of bytes such that it ends up aligned with the specified
        /// byte boundary.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static unsafe void SkipAlign(ref Span<byte> buffer, uint align)
        {
            uint ptr = (uint)Unsafe.AsPointer(ref buffer[0]);
            uint skip = unchecked((uint)-ptr & (align - 1));
            buffer = buffer.Slice((int)skip);
        }

        /// <summary>
        /// Advances a buffer by the minimum number of bytes such that it ends up aligned with the specified
        /// byte boundary.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static unsafe void SkipAlign(ref byte* buffer, uint align)
        {
            uint ptr = (uint)buffer;
            uint skip = unchecked((uint)-ptr & (align - 1));
            buffer += skip;
        }

        #region Pointer Read
        /// <summary>
        /// Reads a value from a buffer using the given <see cref="Encoding{T}"/>. The stream must be aligned
        /// to the boundary specified by <see cref="BinaryProfile.StartAlign"/>.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static unsafe void ReadAligned<T>(ref byte* buffer, Encoding<T> encoding, out T value)
        {
            // TODO: Alignment check
            ReadOnlySpan<byte> dummyBuffer = new ReadOnlySpan<byte>(buffer, int.MaxValue);
            buffer = (byte*)Unsafe.AsPointer(ref encoding.FastRead(ref *buffer, ref dummyBuffer, null, out value));
        }
        #endregion

        #region Pointer Write
        /// <summary>
        /// Writes a value to a buffer using the given <see cref="Encoding{T}"/>. The stream must be aligned
        /// to the boundary specified by <see cref="BinaryProfile.StartAlign"/>.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static unsafe void WriteAligned<T>(ref byte* buffer, Encoding<T> encoding, in T value)
        {
            Span<byte> dummyBuffer = new Span<byte>(buffer, int.MaxValue);
            buffer = (byte*)Unsafe.AsPointer(ref encoding.FastWrite(ref *buffer, ref dummyBuffer, null, in value));
        }
        #endregion

        #region Span Read
        /// <summary>
        /// Advances the buffer by the given number of bytes.
        /// </summary>
        public static void Skip(ref ReadOnlySpan<byte> buffer, uint num)
        {
            buffer = buffer.Slice((int)num);
        }

        /// <summary>
        /// Reads a value from a buffer using the given <see cref="Encoding{T}"/>.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Read<T>(ref ReadOnlySpan<byte> buffer, Encoding<T> encoding, out T value)
        {
            // TODO: Alignment check
            ref byte head = ref MemoryMarshal.GetReference(buffer);
            ref byte nHead = ref encoding.FastRead(ref head, ref buffer, null, out value);
            buffer = buffer.Slice((int)Unsafe.ByteOffset(ref head, ref nHead));
        }

        /// <summary>
        /// Reads a fixed-length span of bytes from a buffer.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ReadOnlySpan<byte> ReadRaw(ref ReadOnlySpan<byte> buffer, uint len)
        {
            ReadOnlySpan<byte> res = buffer.Slice(0, (int)len);
            buffer = buffer.Slice((int)len);
            return res;
        }

        /// <summary>
        /// Reads a blittable value from a buffer using native encoding.
        /// </summary>
        public static unsafe T ReadNative<T>(ref ReadOnlySpan<byte> buffer)
            where T : unmanaged
        {
            if (sizeof(T) > buffer.Length)
                throw new BufferOverflowException();
            T res = Unsafe.ReadUnaligned<T>(ref MemoryMarshal.GetReference(buffer));
            buffer = buffer.Slice(sizeof(T));
            return res;
        }
        
        /// <summary>
        /// Reads a fixed-length list of blittable values from a buffer using native encoding, assuming the buffer
        /// is aligned according to <see cref="Util.AlignOf{T}"/>.
        /// </summary>
        public static unsafe SpanList<T> ReadAlignedNativeList<T>(ref ReadOnlySpan<byte> buffer, uint len)
            where T : unmanaged
        {
            // TODO: Check alignment
            var bytes = ReadRaw(ref buffer, len * (uint)sizeof(T));
            return new SpanList<T>(MemoryMarshal.Cast<byte, T>(bytes));
        }

        /// <summary>
        /// Reads a blittable value from a buffer using native encoding, assuming the buffer is aligned according
        /// to <see cref="Util.AlignOf{T}"/>. Violation of any assumptions will result in undefined behavior.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal static unsafe ref readonly T _fastReadAlignedNative<T>(ref ReadOnlySpan<byte> buffer)
            where T : unmanaged
        {
            ref byte bufferRef = ref MemoryMarshal.GetReference(buffer);
            ref T res = ref Unsafe.As<byte, T>(ref bufferRef);
            buffer = buffer.Slice(sizeof(T)); // TODO: Do this without bounds checking
            return ref res;
        }
        #endregion

        #region Span Write
        /// <summary>
        /// Writes a value to a buffer using the given <see cref="Encoding{T}"/>.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Write<T>(ref Span<byte> buffer, Encoding<T> encoding, in T value)
        {
            // TODO: Alignment check
            ref byte head = ref MemoryMarshal.GetReference(buffer);
            ref byte nHead = ref encoding.FastWrite(ref head, ref buffer, null, in value);
            buffer = buffer.Slice((int)Unsafe.ByteOffset(ref head, ref nHead));
        }
        
        /// <summary>
        /// Writes the given raw data to a buffer.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void WriteRaw(ref Span<byte> buffer, ReadOnlySpan<byte> data)
        {
            Copy(data, buffer.Slice(0, data.Length));
            buffer = buffer.Slice(data.Length);
        }

        /// <summary>
        /// Writes a <see cref="ushort"/> to a buffer using native byte order, assuming the buffer is
        /// currently aligned to a 2-byte boundary.
        /// </summary>
        public static unsafe void WriteAlignedNativeUInt16(ref Span<byte> buffer, ushort value)
        {
            const byte size = sizeof(ushort);
            if ((uint)buffer.Length < size)
                throw new BufferOverflowException();
            Unsafe.As<byte, ushort>(ref MemoryMarshal.GetReference(buffer)) = value;
            buffer = buffer.Slice(size);
        }

        /// <summary>
        /// Writes a <see cref="uint"/> to a buffer using native byte order, assuming the buffer is
        /// currently aligned to a 4-byte boundary.
        /// </summary>
        public static unsafe void WriteAlignedNativeUInt32(ref Span<byte> buffer, uint value)
        {
            const byte size = sizeof(uint);
            if ((uint)buffer.Length < size)
                throw new BufferOverflowException();
            Unsafe.As<byte, uint>(ref MemoryMarshal.GetReference(buffer)) = value;
            buffer = buffer.Slice(size);
        }

        /// <summary>
        /// Writes a <see cref="ulong"/> to a buffer using native byte order, assuming the buffer is
        /// currently aligned to a 8-byte boundary.
        /// </summary>
        public static unsafe void WriteAlignedNativeUInt64(ref Span<byte> buffer, ulong value)
        {
            const byte size = sizeof(ulong);
            if ((uint)buffer.Length < size)
                throw new BufferOverflowException();
            Unsafe.As<byte, ulong>(ref MemoryMarshal.GetReference(buffer)) = value;
            buffer = buffer.Slice(size);
        }

        /// <summary>
        /// Writes a blittable value to a buffer using native byte order, assuming the stream is
        /// currently aligned according to <see cref="Util.AlignOf{T}"/>. Violation of any assumptions will
        /// result in undefined behavior.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static unsafe void _fastWriteAlignedNative<T>(ref Span<byte> buffer, in T value)
            where T : unmanaged
        {
            uint size = (uint)sizeof(T);
            Unsafe.As<byte, T>(ref MemoryMarshal.GetReference(buffer)) = value;
            buffer = buffer.Slice((int)size);
        }

        /// <summary>
        /// Writes a list of blittable values to a buffer using native encoding, assuming the buffer
        /// is aligned according to <see cref="Util.AlignOf{T}"/>.
        /// </summary>
        public static unsafe void WriteAlignedNativeList<T>(ref Span<byte> buffer, SpanList<T> list)
            where T : unmanaged
        {
            // TODO: Check alignment
            WriteRaw(ref buffer, MemoryMarshal.AsBytes<T>(list));
        }
        #endregion

        #region Array Read
        /// <summary>
        /// Reads a fixed-length span of bytes from a buffer.
        /// </summary>
        public static ReadOnlySpan<byte> ReadRaw(byte[] data, ref uint head, uint len)
        {
            ReadOnlySpan<byte> res = data.AsSpan((int)head, (int)len);
            head += len;
            return res;
        }

        /// <summary>
        /// Reads a blittable value from a buffer using native encoding.
        /// </summary>
        public static unsafe T ReadNative<T>(byte[] data, ref uint head)
            where T : unmanaged
        {
            if ((head + (uint)sizeof(T)) > (uint)data.Length)
                throw new BufferOverflowException();
            // TODO: Swap with GetArrayDataReference
            ref byte dataRef = ref MemoryMarshal.GetReference<byte>(data);
            ref byte headRef = ref Unsafe.AddByteOffset(ref dataRef, (IntPtr)head);
            T res = Unsafe.ReadUnaligned<T>(ref headRef);
            head += (uint)sizeof(T);
            return res;
        }

        /// <summary>
        /// Reads a blittable value from a buffer using native encoding, assuming the read head is aligned according
        /// to <see cref="Util.AlignOf{T}"/>. The returned reference may be used to modify the value in the buffer.
        /// </summary>
        public static unsafe ref T ReadAlignedNative<T>(byte[] data, ref uint head)
            where T : unmanaged
        {
            if ((head + (uint)sizeof(T)) > (uint)data.Length)
                throw new BufferOverflowException();
            // TODO: Alignment check
            // TODO: Swap with GetArrayDataReference
            ref byte dataRef = ref MemoryMarshal.GetReference<byte>(data);
            ref byte headRef = ref Unsafe.AddByteOffset(ref dataRef, (IntPtr)head);
            head += (uint)sizeof(T);
            return ref Unsafe.As<byte, T>(ref headRef);
        }

        /// <summary>
        /// Reads a fixed-length list of blittable values from a buffer using native encoding, assuming the buffer
        /// and read head are aligned according to <see cref="Util.AlignOf{T}"/>.
        /// </summary>
        public static unsafe SpanList<T> ReadAlignedNativeList<T>(byte[] data, ref uint head, uint len)
            where T : unmanaged
        {
            // TODO: Check alignment
            var bytes = ReadRaw(data, ref head, len * (uint)sizeof(T));
            return new SpanList<T>(MemoryMarshal.Cast<byte, T>(bytes));
        }

        /// <summary>
        /// Reads a <see cref="uint"/> from a buffer using native byte order, assuming the buffer and read
        /// head are aligned to a 4-byte boundary. Violation of any assumptions will result in undefined behavior.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal static unsafe uint _fastReadAlignedNativeUInt32(byte[] data, ref uint head)
        {
            // TODO: Swap with GetArrayDataReference
            ref byte dataRef = ref MemoryMarshal.GetReference<byte>(data);
            ref byte headRef = ref Unsafe.AddByteOffset(ref dataRef, (IntPtr)head);
            head += sizeof(uint);
            return Unsafe.As<byte, uint>(ref headRef);
        }

        /// <summary>
        /// Reads a blittable value from a buffer using native encoding, assuming the buffer and read head
        /// are aligned according to <see cref="Util.AlignOf{T}"/>. Violation of any assumptions will result in
        /// undefined behavior.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal static unsafe ref T _fastReadAlignedNative<T>(byte[] data, ref uint head)
            where T : unmanaged
        {
            // TODO: Swap with GetArrayDataReference
            ref byte dataRef = ref MemoryMarshal.GetReference<byte>(data);
            ref byte headRef = ref Unsafe.AddByteOffset(ref dataRef, (IntPtr)head);
            head += (uint)sizeof(T);
            return ref Unsafe.As<byte, T>(ref headRef);
        }
        #endregion

        #region Array Write
        /// <summary>
        /// Writes an arbitrary span of bytes to a buffer.
        /// </summary>
        public static void WriteRaw(byte[] data, ref uint head, ReadOnlySpan<byte> source)
        {
            Copy(source, data.AsSpan((int)head, source.Length));
            head += (uint)source.Length;
        }

        /// <summary>
        /// Writes a <see cref="uint"/> to a buffer using native byte order, assuming the buffer and write
        /// head are aligned to a 4-byte boundary. Violation of any assumptions will result in undefined behavior.
        /// </summary>
        internal static unsafe void _fastWriteAlignedNativeUInt32(byte[] data, ref uint head, uint value)
        {
            // TODO: Swap with GetArrayDataReference
            ref byte dataRef = ref MemoryMarshal.GetReference<byte>(data);
            ref byte headRef = ref Unsafe.AddByteOffset(ref dataRef, (IntPtr)head);
            Unsafe.As<byte, uint>(ref headRef) = value;
            head += sizeof(uint);
        }

        /// <summary>
        /// Writes a blittable value to a buffer using native encoding, assuming the buffer and write head are
        /// aligned according to <see cref="Util.AlignOf{T}"/>. Violation of any assumptions will result
        /// in undefined behavior.
        /// </summary>
        internal static unsafe void _fastWriteAlignedNative<T>(byte[] data, ref uint head, in T value)
            where T : unmanaged
        {
            // TODO: Swap with GetArrayDataReference
            ref byte dataRef = ref MemoryMarshal.GetReference<byte>(data);
            ref byte headRef = ref Unsafe.AddByteOffset(ref dataRef, (IntPtr)head);
            Unsafe.As<byte, T>(ref headRef) = value;
            head += (uint)sizeof(T);
        }

        /// <summary>
        /// Writes a list of blittable values to a buffer using native encoding, assuming the buffer
        /// and write head are aligned according to <see cref="Util.AlignOf{T}"/>.
        /// </summary>
        public static unsafe void WriteAlignedNativeList<T>(byte[] data, ref uint head, SpanList<T> list)
            where T : unmanaged
        {
            // TODO: Check alignment
            WriteRaw(data, ref head, MemoryMarshal.AsBytes<T>(list));
        }
        #endregion

        #region Array Allocate
        /// <summary>
        /// Reserves space for a blittable value in a buffer using native encoding, assuming the buffer
        /// and write head are aligned according to <see cref="Util.AlignOf{T}"/>.
        /// </summary>
        public static unsafe ref T AllocateAlignedNative<T>(byte[] data, ref uint head)
            where T : unmanaged
        {
            // TODO: Check alignment
            ref byte dataRef = ref MemoryMarshal.GetReference<byte>(data);
            ref byte headRef = ref Unsafe.AddByteOffset(ref dataRef, (IntPtr)head);
            head += (uint)sizeof(T);
            return ref Unsafe.As<byte, T>(ref headRef);
        }

        /// <summary>
        /// Reserves space for a list of blittable values in a buffer using native encoding, assuming the buffer
        /// and write head are aligned according to <see cref="Util.AlignOf{T}"/>.
        /// </summary>
        public static unsafe Span<T> AllocateAlignedNativeList<T>(byte[] data, ref uint head, uint len)
            where T : unmanaged
        {
            // TODO: Check alignment
            Span<byte> bytes = data.AsSpan((int)head, (int)(len * sizeof(T)));
            head += (uint)bytes.Length;
            return MemoryMarshal.Cast<byte, T>(bytes);
        }
        #endregion

        /// <summary>
        /// Copies the given number of bytes from the given source buffer to the given destination buffer. This will
        /// work even if the buffers overlap.
        /// </summary>
        public static unsafe void Copy(byte* src, byte* dst, ulong size)
        {
            Buffer.MemoryCopy(src, dst, size, size);
        }

        /// <summary>
        /// Copies the data from the given source buffer to the given destination buffer. This will
        /// work even if the buffers overlap.
        /// </summary>
        public static unsafe void Copy(ReadOnlySpan<byte> src, byte* dst)
        {
            // TODO: Avoid overwriting
            // TODO: There's probably a faster way to do this
            for (int i = 0; i < src.Length; i++)
                *dst++ = src[i];
        }

        /// <summary>
        /// Copies the data from the given source buffer to the given destination buffer. This will
        /// work even if the buffers overlap.
        /// </summary>
        public static void Copy(ReadOnlySpan<byte> src, Span<byte> dst)
        {
            // TODO: Avoid overwriting
            // TODO: There's probably a faster way to do this
            if (src.Length != dst.Length)
                throw new ArgumentException("Buffer size mismatch");
            for (int i = 0; i < src.Length; i++)
                dst[i] = src[i];
        }
    }
}