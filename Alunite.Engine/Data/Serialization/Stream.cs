﻿using System;
using System.IO;

namespace Alunite.Data.Serialization
{
    /// <summary>
    /// An <see cref="IBinaryReader"/> which reads from a <see cref="Stream"/>.
    /// </summary>
    public sealed class StreamBinaryReader : ISeekableBinaryReader
    {
        private byte[] _buffer;
        private uint _bufferStart;
        private uint _bufferEnd;
        public StreamBinaryReader(Stream stream, uint bufferSize)
        {
            bufferSize = Math.Max(bufferSize, Binary.MinBufferCapacity);
            _buffer = new byte[bufferSize];
            _bufferStart = 0;
            _bufferEnd = 0;
            Stream = stream;
        }

        /// <summary>
        /// The underlying stream for this reader. While the reader is in use, all stream operations should go
        /// through the reader.
        /// </summary>
        public Stream Stream { get; }

        /// <summary>
        /// Gets or sets the absolute position of the next unread byte within the underlying data source.
        /// </summary>
        public ulong Position
        {
            get
            {
                return (ulong)Stream.Position - (_bufferEnd - _bufferStart);
            }
            set
            {
                long nBufferStart = _bufferEnd + ((long)value - Stream.Position);
                if (nBufferStart >= 0 && nBufferStart < _bufferEnd)
                {
                    // Adjust position in buffer
                    _bufferStart = (uint)nBufferStart;
                }
                else
                {
                    // Fall back to seeking the source stream
                    _bufferStart = _bufferEnd = 0;
                    Stream.Position = (long)value;
                }
            }
        }

        /// <summary>
        /// A buffer containing the next few unread bytes from the stream. The size of this buffer is arbitrary
        /// and variable, however, <see cref="Prepare(uint)"/> can be used populate it up to a given minimum size.
        /// The contents of the buffer are invalidated upon calling <see cref="Prepare(uint)"/>.
        /// </summary>
        public ReadOnlySpan<byte> Buffer => _buffer.AsSpan((int)_bufferStart, (int)(_bufferEnd - _bufferStart));

        /// <summary>
        /// Advances the stream by the given number of bytes.
        /// </summary>
        public void Skip(uint num)
        {
            _bufferStart += num;
            if (_bufferStart > _bufferEnd)
            {
                // Advance the underlying stream to account for excess skipped bytes
                Stream.Seek(_bufferStart - _bufferEnd, SeekOrigin.Current);
                _bufferStart = 0;
                _bufferEnd = 0;
            }
        }

        /// <summary>
        /// Asserts that there are at least the <paramref name="num"/> bytes remaining in the stream, and populates
        /// <see cref="Buffer"/> up to a length of at least <paramref name="num"/> or <see cref="Binary.MinBufferCapacity"/>
        /// (whichever is smaller).
        /// </summary>
        public void Prepare(uint num)
        {
            // Do we need to do anything?
            uint nBufferStart = _bufferStart + num;
            if (nBufferStart > _bufferEnd)
            {
                // Do we need to make space?
                if (nBufferStart > (uint)_buffer.Length)
                {
                    // Move remaining buffer data to beginning of the buffer
                    // TODO: Alignment considerations
                    uint bufferLen = _bufferEnd - _bufferStart;
                    Binary.Copy(_buffer.AsSpan((int)_bufferStart, (int)bufferLen), _buffer.AsSpan(0, (int)bufferLen));
                    _bufferStart = 0;
                    _bufferEnd = bufferLen;
                }

                // Populate more data from stream
                _bufferEnd += (uint)Stream.Read(_buffer, (int)_bufferEnd, _buffer.Length - (int)_bufferEnd);
            }
        }

        /// <summary>
        /// Populates <see cref="Buffer"/> with as many bytes as possible. Returns <see cref="true"/> iff
        /// all remaining bytes have been populated. If this returns <see cref="false"/>, at least
        /// <see cref="Binary.MinBufferCapacity"/> bytes must have been populated.
        /// </summary>
        public bool PrepareMax()
        {
            // Move remaining buffer data to beginning of the buffer
            // TODO: Alignment considerations
            uint bufferLen = _bufferEnd - _bufferStart;
            Binary.Copy(_buffer.AsSpan((int)_bufferStart, (int)bufferLen), _buffer.AsSpan(0, (int)bufferLen));
            _bufferStart = 0;
            _bufferEnd = bufferLen;

            // Populate more data from stream
            _bufferEnd += (uint)Stream.Read(_buffer, (int)_bufferEnd, _buffer.Length - (int)_bufferEnd);

            // Have we populated everything?
            return _bufferEnd < (uint)_buffer.Length;
        }
    }
}