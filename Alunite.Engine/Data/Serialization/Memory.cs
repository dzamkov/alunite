﻿using System;
using System.Runtime.InteropServices;

namespace Alunite.Data.Serialization
{
    /// <summary>
    /// An <see cref="IBinaryReader"/> which reads from a memory buffer, presenting the entire buffer at once.
    /// </summary>
    public abstract class BufferReader : IBinaryReader
    {
        /// <summary>
        /// The remaining buffer being read by this <see cref="BufferReader"/>.
        /// </summary>
        public abstract ReadOnlySpan<byte> Buffer { get; }

        /// <summary>
        /// Advances the buffer by the given number of bytes.
        /// </summary>
        public abstract void Skip(uint num);

        /// <summary>
        /// Ensures that the buffer has at least the given amount of remaining bytes, throwing a
        /// <see cref="BufferOverflowException"/> if this is not the case.
        /// </summary>
        public void Prepare(uint num)
        {
            if ((uint)Buffer.Length < num)
                throw new BufferOverflowException();
        }
    }

    /// <summary>
    /// A <see cref="IBinaryWriter"/> which writes to a fixed-size memory buffer, presenting the entire buffer
    /// at once.
    /// </summary>
    public abstract class BufferWriter : IBinaryWriter
    {
        /// <summary>
        /// The remaining buffer being written by this <see cref="BufferWriter"/>.
        /// </summary>
        public abstract Span<byte> Buffer { get; }

        /// <summary>
        /// Advances the buffer by the given number of bytes.
        /// </summary>
        public abstract void Flush(uint num);

        /// <summary>
        /// Ensures that the buffer has at least the given amount of remaining bytes, throwing a
        /// <see cref="BufferOverflowException"/> if this is not the case.
        /// </summary>
        public void Prepare(uint num)
        {
            if ((uint)Buffer.Length < num)
                throw new BufferOverflowException();
        }
    }

    /// <summary>
    /// A <see cref="BufferReader"/> which reads from an array of values of type <typeparamref name="T"/>, using the
    /// unmanaged representation of the values.
    /// </summary>
    public sealed class MemoryReader<T> : BufferReader
        where T : unmanaged
    {
        public MemoryReader(T[] source, uint head)
        {
            Source = source;
            Head = head;
        }

        /// <summary>
        /// The full array being read by this reader.
        /// </summary>
        public T[] Source { get; }

        /// <summary>
        /// The byte offset of this reader within <see cref="Source"/>.
        /// </summary>
        public uint Head;

        public override ReadOnlySpan<byte> Buffer => MemoryMarshal.AsBytes<T>(Source).Slice((int)Head);

        public override void Skip(uint num)
        {
            Head += num;
        }
    }

    /// <summary>
    /// A <see cref="BufferReader"/> which reads from a span of unmanaged memory identified by two pointers.
    /// </summary>
    public sealed unsafe class PointerReader : BufferReader
    {
        public PointerReader(byte* head, byte* end)
        {
            Head = head;
            End = end;
        }

        /// <summary>
        /// The next byte in the buffer.
        /// </summary>
        public byte* Head;

        /// <summary>
        /// The first byte past the end of the buffer.
        /// </summary>
        public byte* End { get; }

        public override ReadOnlySpan<byte> Buffer => new ReadOnlySpan<byte>(Head, (int)(End - Head));

        public override void Skip(uint num)
        {
            Head += num;
        }
    }

    /// <summary>
    /// A <see cref="BufferReader"/> which writes to a span of unmanaged memory identified by two pointers.
    /// </summary>
    public sealed unsafe class PointerWriter : BufferWriter
    {
        public PointerWriter(byte* head, byte* end)
        {
            Head = head;
            End = end;
        }

        /// <summary>
        /// The next byte in the buffer.
        /// </summary>
        public byte* Head;

        /// <summary>
        /// The first byte past the end of the buffer.
        /// </summary>
        public byte* End { get; }

        public override Span<byte> Buffer => new Span<byte>(Head, (int)(End - Head));

        public override void Flush(uint num)
        {
            Head += num;
        }
    }

    /// <summary>
    /// An exception that is thrown when there is an attempt to read or write past the end of a buffer.
    /// </summary>
    public sealed class BufferOverflowException : Exception
    {

    }
}