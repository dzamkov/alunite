﻿using System;
using System.Reflection;

namespace Alunite.Data.Serialization
{
    /// <summary>
    /// A method of selecting an <see cref="Encoding{T}"/> for each type.
    /// </summary>
    public class EncodingResolver
    {
        private EncodingResolver(IOpenEncodingResolver open)
        {
            Open = open;
        }

        internal EncodingResolver()
        {
            Open = (IOpenEncodingResolver)this;
        }

        /// <summary>
        /// The "open" form of this resolver.
        /// </summary>
        public IOpenEncodingResolver Open { get; }

        /// <summary>
        /// An <see cref="EncodingResolver"/> which attempts to define encodings for as many types as possible.
        /// </summary>
        public static DefaultEncodingResolver Default(EncodingStrategy strat)
        {
            return DefaultEncodingResolver.Get(strat);
        }

        /// <summary>
        /// A fully-portable <see cref="IOpenEncodingResolver"/> which behaves the same across all machines and
        /// program versions. This is intended to be standard <see cref="EncodingResolver"/> for values that need to be
        /// persisted or transmitted in a portable way. 
        /// </summary>
        public static StandardEncodingResolver Standard => StandardEncodingResolver.Instance;

        /// <summary>
        /// Constructs an <see cref="EncodingResolver"/> by "fixing" an <see cref="IOpenEncodingResolver"/> to itself.
        /// </summary>
        public static EncodingResolver Build(IOpenEncodingResolver open)
        {
            if (open is OpenEncodingResolver resolver)
                return resolver;
            return new EncodingResolver(open);
        }

        /// <summary>
        /// Attempts to resolve the <see cref="Encoding{T}"/> for the given type, returning <see cref="false"/> if this
        /// <see cref="EncodingResolver"/> does not define an encoding for that type.
        /// </summary>
        public bool TryResolve<T>(Domain<T> domain, out Encoding<T> encoding)
        {
            // TODO: Caching
            return Open.TryResolve(this, domain, out encoding);
        }

        /// <summary>
        /// Attempts to resolve the <see cref="Encoding{T}"/> for the given type, returning <see cref="false"/> if this
        /// <see cref="EncodingResolver"/> does not define an encoding for that type.
        /// </summary>
        internal bool _tryResolve(DomainAny domain, out _IEncodingAny encoding)
        {
            object[] args = new object[] { this, domain };
            object res = _resolveInfo.MakeGenericMethod(domain.Type).Invoke(null, args);
            if (!(res is null))
            {
                encoding = (_IEncodingAny)res;
                return true;
            }
            encoding = default;
            return false;
        }

        /// <summary>
        /// Helper for implementing <see cref="_tryResolve(DomainAny, out _IEncodingAny)"/>.
        /// </summary>
        private static Encoding<T> _resolve<T>(EncodingResolver resolver, DomainAny domain)
        {
            if (!resolver.TryResolve((Domain<T>)domain, out Encoding<T> encoding))
                encoding = null;
            return encoding;
        }

        /// <summary>
        /// The <see cref="MethodInfo"/> for <see cref="_resolve"/>.
        /// </summary>
        private static MethodInfo _resolveInfo { get; } =
            ((Func<EncodingResolver, DomainAny, Encoding<object>>)_resolve<object>)
            .Method.GetGenericMethodDefinition();

        /// <summary>
        /// Gets the type which has encoding information for the given type. This is used for resolving encodings for
        /// primitive types.
        /// </summary>
        internal static Type _encodingType(Type type)
        {
            switch (Type.GetTypeCode(type))
            {
                case TypeCode.Byte: return typeof(Encoding.Byte);
                case TypeCode.UInt16: return typeof(Encoding.UInt16);
                case TypeCode.UInt32: return typeof(Encoding.UInt32);
                default: return type;
            }
        }
    }

    /// <summary>
    /// An <see cref="EncodingResolver"/> which is useable both as-is, and as an <see cref="IOpenEncodingResolver"/> which
    /// uses a different <see cref="EncodingResolver"/> for inner values.
    /// </summary>
    public abstract class OpenEncodingResolver : EncodingResolver, IOpenEncodingResolver
    {
        /// <summary>
        /// Attempts to resolve the <see cref="Encoding{T}"/> for the given type, returning <see cref="false"/> if this
        /// <see cref="IOpenEncodingResolver"/> does not define an encoding for that type.
        /// </summary>
        /// <param name="inner">The <see cref="EncodingResolver"/> used for selecting the encoding for inner
        /// values of a complex type.</param>
        public abstract bool TryResolve<T>(EncodingResolver inner, Domain<T> domain, out Encoding<T> encoding);
    }

    /// <summary>
    /// A method of selecting an <see cref="Encoding{T}"/> for each type, parameterized by the
    /// <see cref="EncodingResolver"/> used for inner values of complex types.
    /// </summary>
    public interface IOpenEncodingResolver
    {
        /// <summary>
        /// Attempts to resolve the <see cref="Encoding{T}"/> for the given type, returning <see cref="false"/> if this
        /// <see cref="IOpenEncodingResolver"/> does not define an encoding for that type.
        /// </summary>
        /// <param name="inner">The <see cref="EncodingResolver"/> used for selecting the encoding for inner
        /// values of a complex type.</param>
        bool TryResolve<T>(EncodingResolver inner, Domain<T> domain, out Encoding<T> encoding);
    }

    /// <summary>
    /// An <see cref="EncodingResolver"/> which attempts to define encodings for as many types as possible.
    /// </summary>
    public sealed class DefaultEncodingResolver : OpenEncodingResolver
    {
        private DefaultEncodingResolver(EncodingStrategy strat)
        {
            Strategy = strat;
        }

        /// <summary>
        /// The overall <see cref="EncodingStrategy"/> used for selecting encodings using this
        /// <see cref="DefaultEncodingResolver"/>.
        /// </summary>
        public EncodingStrategy Strategy { get; }

        /// <summary>
        /// Gets the <see cref="DefaultEncodingResolver"/> for the given strategy.
        /// </summary>
        public static DefaultEncodingResolver Get(EncodingStrategy strat)
        {
            return _instances[(byte)strat];
        }

        /// <summary>
        /// The available instances of this class.
        /// </summary>
        private static readonly DefaultEncodingResolver[] _instances = new DefaultEncodingResolver[]
        {
            new DefaultEncodingResolver(EncodingStrategy.Fast),
            new DefaultEncodingResolver(EncodingStrategy.Balanced),
            new DefaultEncodingResolver(EncodingStrategy.Compact)
        };

        public override bool TryResolve<T>(EncodingResolver inner, Domain<T> domain, out Encoding<T> encoding)
        {
            // Check for attribute
            if (_tryResolveAttr(_encodingType(typeof(T)), inner, domain, out encoding))
                return true;

            // Fallback to using the standard encoding
            return Standard.TryResolve(inner, domain, out encoding);
        }

        /// <summary>
        /// Attempts to find a suitable encoding for this <see cref="DefaultEncodingResolver"/> by searching for
        /// members tagged with a <see cref="DefaultEncodingAttribute"/> on the given container type.
        /// </summary>
        private bool _tryResolveAttr<T>(
            Type container, EncodingResolver inner,
            Domain<T> domain, out Encoding<T> encoding)
        {
            var flags = BindingFlags.Public | BindingFlags.Static;
            foreach (MemberInfo member in container.GetMembers(flags))
            {
                // Check for attribute
                foreach (var attr in member.GetCustomAttributes<DefaultEncodingAttribute>())
                {
                    if (attr.Strategy == null || attr.Strategy == Strategy)
                    {
                        // Get encoding from field
                        if (member is FieldInfo field)
                        {
                            object value = field.GetValue(null);
                            if (value is Encoding<T>)
                            {
                                encoding = (Encoding<T>)value;
                                return true;
                            }
                        }

                        // Get encoding from property
                        if (member is PropertyInfo prop)
                        {
                            object value = prop.GetValue(null);
                            if (value is Encoding<T>)
                            {
                                encoding = (Encoding<T>)value;
                                return true;
                            }
                        }

                        // Get encoding from method
                        if (member is MethodInfo method)
                        {
                            throw new NotImplementedException();
                        }
                    }
                }

                // Resolve from nested container
                if (member is Type nestedContainer)
                    if (_tryResolveAttr(nestedContainer, inner, domain, out encoding))
                        return true;
            }
            encoding = default;
            return false;
        }
    }

    /// <summary>
    /// A fully-portable <see cref="EncodingResolver"/> which behaves the same across all machines and
    /// program versions. This is intended to be standard <see cref="EncodingResolver"/> for values that need to be
    /// persisted or transmitted in a portable way.
    /// </summary>
    public sealed class StandardEncodingResolver : OpenEncodingResolver
    {
        private StandardEncodingResolver() { }

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        public static StandardEncodingResolver Instance { get; } = new StandardEncodingResolver();

        public override bool TryResolve<T>(EncodingResolver inner, Domain<T> domain, out Encoding<T> encoding)
        {
            throw new NotImplementedException();
        }
    }
    
    /// <summary>
    /// Specifies restrictions on a value of type <typeparamref name="T"/> that are relevant for encoding purposes.
    /// </summary>
    public struct Domain<T>
    {
        /// <summary>
        /// An unrestricted and unspecified <see cref="Domain{T}"/>.
        /// </summary>
        public static Domain<T> General => default;
        
        public static explicit operator Domain<T>(DomainAny source)
        {
            if (!(source.Type == typeof(T)))
                throw new InvalidCastException();
            return General;
        }
    }

    /// <summary>
    /// A <see cref="Domain{T}"/> of some type.
    /// </summary>
    public struct DomainAny
    {
        private DomainAny(Type type)
        {
            Type = type;
        }

        /// <summary>
        /// The type of this <see cref="Domain{T}"/>.
        /// </summary>
        public Type Type { get; }

        /// <summary>
        /// Gets the domain of the given field.
        /// </summary>
        public static DomainAny FromField(FieldInfo field)
        {
            return new DomainAny(field.FieldType);
        }
    }

    /// <summary>
    /// Identifies an overall strategy for selecting or constructing an <see cref="Encoding{T}"/> for a
    /// particular type.
    /// </summary>
    public enum EncodingStrategy : byte
    {
        /// <summary>
        /// The encoding is optimized for read/write speed.
        /// </summary>
        Fast = 0,

        /// <summary>
        /// The encoding tends towards compactness, with occasional concessions for read/write speed.
        /// </summary>
        Balanced = 1,

        /// <summary>
        /// The encoding is optimized to minimize encoded size.
        /// </summary>
        Compact = 2,
    }

    /// <summary>
    /// Specifies a potential default <see cref="Encoding{T}"/> for a type. When used on a static field or property,
    /// indicates that the value of the field or property contains the default <see cref="Encoding{T}"/>. When used
    /// on a struct, indicates the default encoding for the type is constructed using <see cref="Encoding.Struct"/>.
    /// When used on a method, indicates that the default encoding is obtained by calling the method appropriate
    /// resolution parameters. The default encoding must be applicable to all possible values of the type.
    /// </summary>
    [AttributeUsage(
        AttributeTargets.Field | AttributeTargets.Property |
        AttributeTargets.Struct | AttributeTargets.Method)]
    public sealed class DefaultEncodingAttribute : Attribute
    {
        public DefaultEncodingAttribute()
        {
            Strategy = null;
        }

        public DefaultEncodingAttribute(EncodingStrategy strat)
        {
            Strategy = strat;
        }

        /// <summary>
        /// The <see cref="EncodingStrategy"/> this is the default encoding for, or <see cref="null"/> if this
        /// is the applicable for all strategies.
        /// </summary>
        public EncodingStrategy? Strategy { get; }
    }

    /// <summary>
    /// Specifies the unique "standard" <see cref="Encoding{T}"/> for a type. When used on a static field or property,
    /// indicates that the value of the field or property contains the standard <see cref="Encoding{T}"/>. When used
    /// on a struct, indicates the standard encoding for the type is constructed using <see cref="Encoding.Struct"/>.
    /// When used on a method, indicates that the standard encoding is obtained by calling the method appropriate
    /// resolution parameters. The standard encoding for a type must be constructed deterministically, and will be the
    /// same across all machines and program versions. The standard encoding must be applicable to all possible values
    /// of the type.
    /// </summary>
    [AttributeUsage(
        AttributeTargets.Field | AttributeTargets.Property |
        AttributeTargets.Struct | AttributeTargets.Method)]
    public sealed class StandardEncodingAttribute : Attribute
    {

    }
}
