﻿using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;

namespace Alunite.Data.Serialization
{
    public partial class Encoding
    {
        /// <summary>
        /// Gets the automatically-generated struct-based <see cref="Encoding{T}"/> for a type,
        /// subject to the given parameters, throwing an exception if not possible. This encoding can not be used to
        /// encode <see cref="null"/> or any values of a derived type.
        /// </summary>
        /// <param name="isPortable">If true, indicates that the encoding must be constructed deterministically such that
        /// the same encoding will always be returned, across all machines, program versions, etc.</param>
        /// <param name="resolver">The <see cref="EncodingResolver"/> used to get encodings for struct fields.</param>
        public static Encoding<T> Struct<T>(bool isPortable, EncodingResolver resolver, EncodingStrategy strat)
        {
            if (!TryStruct(isPortable, resolver, strat, out Encoding<T> encoding))
                throw new Exception("No available encoding");
            return encoding;
        }

        /// <summary>
        /// Attempts to get the automatically-generated struct-based <see cref="Encoding{T}"/> for a type,
        /// subject to the given parameters, returning <see cref="false"/> if not possible. In order to create
        /// a struct encoding, the type must have a default public constructor and all fields of the type must
        /// be public, with no custom layout. This encoding can not be used to encode <see cref="null"/> or any
        /// values of a derived type.
        /// </summary>
        /// <param name="isPortable">If true, indicates that the encoding must be constructed deterministically such that
        /// the same encoding will always be returned, across all machines, program versions, etc.</param>
        /// <param name="resolver">The <see cref="EncodingResolver"/> used to get encodings for struct fields.</param>
        public static bool TryStruct<T>(
            bool isPortable,
            EncodingResolver resolver,
            EncodingStrategy strat,
            out Encoding<T> encoding)
        {
            // TODO: Caching
            if (typeof(T).IsPlain(out List<FieldInfo> fields))
            {
                // Get encodings for each field
                var fieldEncodings = ListBuilder<_IEncodingAny>.CreateDefault(fields.Length);
                for (uint i = 0; i < fields.Length; i++)
                {
                    // TODO: If isPortable, require deterministic ordering of fields
                    FieldInfo field = fields[i];
                    if (!resolver._tryResolve(DomainAny.FromField(field), out fieldEncodings[i]))
                        goto noEncoding;
                }

                // Build encoding
                encoding = (Encoding<T>)_buildStruct(typeof(T), strat, fields, fieldEncodings);
                return true;
            }
        noEncoding:
            encoding = default;
            return false;
        }

        /// <summary>
        /// Creates a struct <see cref="Encoding{T}"/> for the given type.
        /// </summary>
        private static _IEncodingAny _buildStruct(
            Type type, EncodingStrategy strat,
            List<FieldInfo> fields,
            SpanList<_IEncodingAny> fieldEncodings)
        {
            // Build layout
            // TODO: Check if this is a blittable encoding
            BinaryLayoutBuilder layoutBuilder = BinaryLayoutBuilder.Create(strat);
            for (uint i = 0; i < fieldEncodings.Length; i++)
            {
                _IEncodingAny fieldEncoding = fieldEncodings[i];
                layoutBuilder.Append(i, fieldEncoding.Size, fieldEncoding.Align);
            }
            BinaryLayout layout = layoutBuilder.Finish();

            // Dynamically generate type for encoding
            Type baseEncodingType = typeof(Encoding<>).MakeGenericType(type);
            string typeName = type.Name + "_Encoding";
            _dynamicEncodingModule.DeconflictTypeName(ref typeName);
            TypeBuilder encodingType = _dynamicEncodingModule.DefineType(
                typeName, TypeAttributes.NotPublic, baseEncodingType);

            // Define fields for storing field encodings
            FieldBuilder[] encodingFields = new FieldBuilder[fields.Length];
            for (uint i = 0; i < fields.Length; i++)
            {
                FieldInfo field = fields[i];
                string fieldName = field.Name;
                encodingFields[i] = encodingType.DefineField(fieldName, fieldEncodings[i].GetType(), FieldAttributes.Public);
            }

            // Add default constructor
            {
                Type[] constructorParamTypes = new Type[] { typeof(BinarySize), typeof(BinaryAlign) };
                ConstructorInfo baseConstructor = baseEncodingType.GetConstructor(
                    constructorParamTypes);
                ConstructorBuilder constructor = encodingType.DefineConstructor(
                    MethodAttributes.Public,
                    CallingConventions.HasThis,
                    constructorParamTypes);
                ILGenerator il = constructor.GetILGenerator();
                il.Emit(OpCodes.Ldarg_0);
                il.Emit(OpCodes.Ldarg_1);
                il.Emit(OpCodes.Ldarg_2);
                il.Emit(OpCodes.Call, baseConstructor);
                il.Emit(OpCodes.Ret);
            }

            // Add implementation of FastRead
            {
                MethodInfo baseFastRead = baseEncodingType.GetMethod(nameof(Encoding<object>.FastRead));
                MethodBuilder fastRead = encodingType.DefineMethod(
                    baseFastRead.Name,
                    MethodAttributes.Public | MethodAttributes.Virtual,
                    CallingConventions.HasThis);
                fastRead.SetReturnType(typeof(byte).MakeByRefType());
                fastRead.SetParameters(
                    typeof(byte).MakeByRefType(),
                    typeof(ReadOnlySpan<byte>).MakeByRefType(),
                    typeof(IBinaryReader),
                    type.MakeByRefType());
                ILGenerator il = fastRead.GetILGenerator();
                _generateStructFastRead(il, false, layout, fields, encodingFields);
                encodingType.DefineMethodOverride(fastRead, baseFastRead);
            }

            // Add implementation of FastReadAligned
            {
                MethodInfo baseFastReadAligned = baseEncodingType.GetMethod(nameof(Encoding<object>.FastReadAligned));
                MethodBuilder fastReadAligned = encodingType.DefineMethod(
                    baseFastReadAligned.Name,
                    MethodAttributes.Public | MethodAttributes.Virtual,
                    CallingConventions.HasThis);
                fastReadAligned.SetReturnType(typeof(byte).MakeByRefType());
                fastReadAligned.SetParameters(
                    typeof(byte).MakeByRefType(),
                    typeof(ReadOnlySpan<byte>).MakeByRefType(),
                    typeof(IBinaryReader),
                    type.MakeByRefType());
                ILGenerator il = fastReadAligned.GetILGenerator();
                _generateStructFastRead(il, true, layout, fields, encodingFields);
                encodingType.DefineMethodOverride(fastReadAligned, baseFastReadAligned);
            }

            // Add implementation of FastWrite
            {
                MethodInfo baseFastWrite = baseEncodingType.GetMethod(nameof(Encoding<object>.FastWrite));
                MethodInfo genericFastWrite = typeof(Encoding).GetMethod(nameof(FastWrite));
                MethodBuilder fastWrite = encodingType.DefineMethod(
                    baseFastWrite.Name,
                    MethodAttributes.Public | MethodAttributes.Virtual,
                    CallingConventions.HasThis);
                fastWrite.SetSignature(
                    typeof(byte).MakeByRefType(), null, null,
                    new Type[] {
                        typeof(byte).MakeByRefType(),
                        typeof(Span<byte>).MakeByRefType(),
                        typeof(IBinaryWriter),
                        type.MakeByRefType()
                    }, new Type[][] {
                        null,
                        null,
                        null,
                        new Type[] { typeof(InAttribute) }
                    }, null);
                ILGenerator il = fastWrite.GetILGenerator();
                _generateStructFastWrite(il, false, layout, fields, encodingFields);
                encodingType.DefineMethodOverride(fastWrite, baseFastWrite);
            }

            // Add implementation of FastWriteAligned
            {
                MethodInfo baseFastWriteAligned = baseEncodingType.GetMethod(nameof(Encoding<object>.FastWriteAligned));
                MethodBuilder fastWrite = encodingType.DefineMethod(
                    baseFastWriteAligned.Name,
                    MethodAttributes.Public | MethodAttributes.Virtual,
                    CallingConventions.HasThis);
                fastWrite.SetSignature(
                    typeof(byte).MakeByRefType(), null, null,
                    new Type[] {
                        typeof(byte).MakeByRefType(),
                        typeof(Span<byte>).MakeByRefType(),
                        typeof(IBinaryWriter),
                        type.MakeByRefType()
                    }, new Type[][] {
                        null,
                        null,
                        null,
                        new Type[] { typeof(InAttribute) }
                    }, null);
                ILGenerator il = fastWrite.GetILGenerator();
                _generateStructFastWrite(il, true, layout, fields, encodingFields);
                encodingType.DefineMethodOverride(fastWrite, baseFastWriteAligned);
            }

            // Create and initialize encoding
            Type fEncodingType = encodingType.CreateType();
            var fEncoding = (_IEncodingAny)Activator.CreateInstance(fEncodingType, layout.Size, layout.Align);
            for (uint i = 0; i < fields.Length; i++)
                fEncodingType.GetField(encodingFields[i].Name).SetValue(fEncoding, fieldEncodings[i]);
            return fEncoding;
        }

        /// <summary>
        /// Generates the body of the <see cref="Encoding{T}.FastRead"/> or
        /// <see cref="Encoding{T}.FastReadAligned"/> method for a struct encoding.
        /// </summary>
        private static void _generateStructFastRead(
            ILGenerator il, bool isAligned, BinaryLayout layout,
            SpanList<FieldInfo> fields, FieldBuilder[] encodingFields)
        {
            LocalBuilder head = il.DeclareLocal(typeof(byte).MakeByRefType());
            il.Emit(OpCodes.Ldarg_1);
            foreach (var op in layout.Operations)
            {
                // Generate code to implement layout operation
                switch (op.Kind)
                {
                    case BinaryLayoutOperationKind.Skip:
                        il.Emit(OpCodes.Ldc_I4, op.Argument);
                        il.Emit(OpCodes.Add);
                        break;
                    case BinaryLayoutOperationKind.Prepare:
                        throw new NotImplementedException();
                    case BinaryLayoutOperationKind.FieldAligned:
                    case BinaryLayoutOperationKind.Field:
                        {
                            // Read field
                            uint id = op.Argument;
                            FieldInfo field = fields[id];
                            il.Emit(OpCodes.Stloc, head);
                            il.Emit(OpCodes.Ldarg_0);
                            il.Emit(OpCodes.Ldfld, encodingFields[id]);
                            il.Emit(OpCodes.Ldloc, head);
                            il.Emit(OpCodes.Ldarg_2);
                            il.Emit(OpCodes.Ldarg_3);
                            il.Emit(OpCodes.Ldarg_S, (short)4);
                            il.Emit(OpCodes.Ldflda, field);
                            il.Emit(OpCodes.Callvirt, typeof(Encoding<>)
                                .MakeGenericType(field.FieldType)
                                .GetMethod(
                                    isAligned && op.Kind == BinaryLayoutOperationKind.FieldAligned
                                    ? nameof(Encoding<object>.FastReadAligned)
                                    : nameof(Encoding<object>.FastRead)));
                        }
                        break;
                }
            }
            il.Emit(OpCodes.Ret);
        }

        /// <summary>
        /// Generates the body of the <see cref="Encoding{T}.FastWrite"/> or
        /// <see cref="Encoding{T}.FastWriteAligned"/> method for a struct encoding.
        /// </summary>
        private static void _generateStructFastWrite(
            ILGenerator il, bool isAligned, BinaryLayout layout,
            SpanList<FieldInfo> fields, FieldBuilder[] encodingFields)
        {
            MethodInfo genericFastWrite = typeof(Encoding).GetMethod(nameof(FastWrite));
            MethodInfo genericFastWriteAligned = typeof(Encoding).GetMethod(nameof(FastWriteAligned));
            LocalBuilder head = il.DeclareLocal(typeof(byte).MakeByRefType());
            il.Emit(OpCodes.Ldarg_1);
            foreach (var op in layout.Operations)
            {
                // Generate code to implement layout operation
                switch (op.Kind)
                {
                    case BinaryLayoutOperationKind.Skip:
                        il.Emit(OpCodes.Ldc_I4, op.Argument);
                        il.Emit(OpCodes.Add);
                        break;
                    case BinaryLayoutOperationKind.Prepare:
                        throw new NotImplementedException();
                    case BinaryLayoutOperationKind.FieldAligned:
                    case BinaryLayoutOperationKind.Field:
                        {
                            // Write field
                            uint id = op.Argument;
                            FieldInfo field = fields[id];
                            il.Emit(OpCodes.Stloc, head);
                            il.Emit(OpCodes.Ldarg_0);
                            il.Emit(OpCodes.Ldfld, encodingFields[id]);
                            il.Emit(OpCodes.Ldloc, head);
                            il.Emit(OpCodes.Ldarg_2);
                            il.Emit(OpCodes.Ldarg_3);
                            il.Emit(OpCodes.Ldarg_S, (byte)4);
                            il.Emit(OpCodes.Ldflda, field);
                            il.Emit(OpCodes.Call,
                                (isAligned && op.Kind == BinaryLayoutOperationKind.FieldAligned
                                    ? genericFastWriteAligned
                                    : genericFastWrite)
                                .MakeGenericMethod(field.FieldType));
                        }
                        break;
                }
            }
            il.Emit(OpCodes.Ret);
        }

        /// <summary>
        /// Writes a value to the given <see cref="IBinaryWriter"/> using this encoding.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ref byte FastWrite<T>(
            Encoding<T> encoding, ref byte head, ref Span<byte> buffer,
            IBinaryWriter writer, ref T value)
        {
            // Reflection.Emit has a bug which makes it impossible to call a method using a required modifier (e.g. "in").
            // This is a wrapper of Encoding.FastWrite that allows dynamically generated code to use the "FastWrite" method.
            // https://github.com/dotnet/runtime/issues/25958
            // TODO: Remove once no longer needed
            return ref encoding.FastWrite(ref head, ref buffer, writer, value);
        }

        /// <summary>
        /// Writes a value to the given <see cref="IBinaryWriter"/> using this encoding.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ref byte FastWriteAligned<T>(
            Encoding<T> encoding, ref byte head, ref Span<byte> buffer,
            IBinaryWriter writer, ref T value)
        {
            // TODO: Remove once no longer needed
            return ref encoding.FastWriteAligned(ref head, ref buffer, writer, value);
        }

        /// <summary>
        /// Gets a <see cref="ModuleBuilder"/> for the module to which dynamically-generated <see cref="Encoding{T}"/>s
        /// are generated in.
        /// </summary>
        private static ModuleBuilder _dynamicEncodingModule =
            AssemblyBuilder.DefineDynamicAssembly(
                new AssemblyName("Dynamically generated Encoding"),
                AssemblyBuilderAccess.Run)
            .DefineDynamicModule("Module");
    }
}