﻿using System;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Alunite.Data.Serialization
{
    /// <summary>
    /// Contains helper functions for reading and writing data to blobs, finite-length blocks of binary data
    /// which store data sequentially and compactly in little-endian byte order. This is a fully portable
    /// format suitable for file storage and exchange.
    /// </summary>
    public static class Blob
    {
        /// <summary>
        /// Reads a byte of data from a blob.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static byte ReadByte(ref Span<byte> blob)
        {
            if (!(sizeof(byte) <= blob.Length))
                _throwOutOfBounds();
            byte res = blob[0];
            blob = blob.Slice(sizeof(byte));
            return res;
        }

        /// <summary>
        /// Reads the next 2 bytes from a blob and interprets them as a <see cref="ushort"/>.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ushort ReadUInt16(ref Span<byte> blob)
        {
            if (!(sizeof(ushort) <= blob.Length))
                _throwOutOfBounds();
            if (BitConverter.IsLittleEndian)
            {
                ushort res = Unsafe.ReadUnaligned<ushort>(ref blob[0]);
                blob = blob.Slice(sizeof(ushort));
                return res;
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Reads the next 4 bytes from a blob and interprets them as a <see cref="uint"/>.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static uint ReadUInt32(ref Span<byte> blob)
        {
            if (!(sizeof(uint) <= blob.Length))
                _throwOutOfBounds();
            if (BitConverter.IsLittleEndian)
            {
                uint res = Unsafe.ReadUnaligned<uint>(ref blob[0]);
                blob = blob.Slice(sizeof(uint));
                return res;
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Reads the next 8 bytes from a blob and interprets them as a <see cref="ulong"/>.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ulong ReadUInt64(ref Span<byte> blob)
        {
            if (!(sizeof(ulong) <= blob.Length))
                _throwOutOfBounds();
            if (BitConverter.IsLittleEndian)
            {
                ulong res = Unsafe.ReadUnaligned<ulong>(ref blob[0]);
                blob = blob.Slice(sizeof(ulong));
                return res;
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Reads a byte of data from a blob.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static byte ReadByte(Span<byte> blob, ref uint head)
        {
            blob = blob.Slice((int)head);
            if (!(sizeof(byte) <= blob.Length))
                _throwOutOfBounds();
            head += sizeof(byte);
            return blob[0];
        }

        /// <summary>
        /// Reads the next 2 bytes from a blob and interprets them as a <see cref="ushort"/>.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ushort ReadUInt16(Span<byte> blob, ref uint head)
        {
            blob = blob.Slice((int)head);
            if (!(sizeof(ushort) <= blob.Length))
                _throwOutOfBounds();
            head += sizeof(ushort);
            if (BitConverter.IsLittleEndian)
            {
                return Unsafe.ReadUnaligned<ushort>(ref blob[0]);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Reads the next 4 bytes from a blob and interprets them as a <see cref="uint"/>.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static uint ReadUInt32(Span<byte> blob, ref uint head)
        {
            blob = blob.Slice((int)head);
            if (!(sizeof(uint) <= blob.Length))
                _throwOutOfBounds();
            head += sizeof(uint);
            if (BitConverter.IsLittleEndian)
            {
                return Unsafe.ReadUnaligned<uint>(ref blob[0]);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Reads the next 8 bytes from a blob and interprets them as a <see cref="ulong"/>.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ulong ReadUInt64(Span<byte> blob, ref uint head)
        {
            blob = blob.Slice((int)head);
            if (!(sizeof(ulong) <= blob.Length))
                _throwOutOfBounds();
            head += sizeof(ulong);
            if (BitConverter.IsLittleEndian)
            {
                return Unsafe.ReadUnaligned<ulong>(ref blob[0]);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Reads a fixed-length array of bytes from a blob.
        /// </summary>
        public unsafe static ReadOnlySpan<byte> ReadRaw(ref byte* blob, uint num)
        {
            // TODO: Write to a buffer instead of returning a reference to the blob
            var res = new ReadOnlySpan<byte>(blob, (int)num);
            blob += num;
            return res;
        }

        /// <summary>
        /// Writes a byte of data to a blob.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void WriteByte(ref Span<byte> blob, byte value)
        {
            if (!(sizeof(byte) <= blob.Length))
                _throwOutOfBounds();
            blob[0] = value;
            blob = blob.Slice(sizeof(byte));
        }
        
        /// <summary>
        /// Writes a <see cref="ushort"/> of data to a blob.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void WriteUInt16(ref Span<byte> blob, ushort value)
        {
            if (!(sizeof(ushort) <= blob.Length))
                _throwOutOfBounds();
            if (BitConverter.IsLittleEndian)
            {
                Unsafe.WriteUnaligned(ref blob[0], value);
                blob = blob.Slice(sizeof(ushort));
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        
        /// <summary>
        /// Writes a <see cref="uint"/> of data to a blob.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void WriteUInt32(ref Span<byte> blob, uint value)
        {
            if (!(sizeof(uint) <= blob.Length))
                _throwOutOfBounds();
            if (BitConverter.IsLittleEndian)
            {
                Unsafe.WriteUnaligned(ref blob[0], value);
                blob = blob.Slice(sizeof(uint));
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Writes a <see cref="ulong"/> of data to a blob.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void WriteUInt64(ref Span<byte> blob, ulong value)
        {
            if (!(sizeof(ulong) <= blob.Length))
                _throwOutOfBounds();
            if (BitConverter.IsLittleEndian)
            {
                Unsafe.WriteUnaligned(ref blob[0], value);
                blob = blob.Slice(sizeof(ulong));
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Writes a byte of data to a blob.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void WriteByte(Span<byte> blob, ref uint head, byte value)
        {
            blob = blob.Slice((int)head);
            if (!(sizeof(byte) <= blob.Length))
                _throwOutOfBounds();
            head += sizeof(byte);
            blob[0] = value;
        }

        /// <summary>
        /// Writes a <see cref="ushort"/> of data to a blob.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void WriteUInt16(Span<byte> blob, ref uint head, ushort value)
        {
            blob = blob.Slice((int)head);
            if (!(sizeof(ushort) <= blob.Length))
                _throwOutOfBounds();
            head += sizeof(ushort);
            if (BitConverter.IsLittleEndian)
            {
                Unsafe.WriteUnaligned(ref blob[0], value);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Writes a <see cref="uint"/> of data to a blob.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void WriteUInt32(Span<byte> blob, ref uint head, uint value)
        {
            blob = blob.Slice((int)head);
            if (!(sizeof(uint) <= blob.Length))
                _throwOutOfBounds();
            head += sizeof(uint);
            if (BitConverter.IsLittleEndian)
            {
                Unsafe.WriteUnaligned(ref blob[0], value);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Writes a <see cref="ulong"/> of data to a blob.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void WriteUInt64(Span<byte> blob, ref uint head, ulong value)
        {
            blob = blob.Slice((int)head);
            if (!(sizeof(ulong) <= blob.Length))
                _throwOutOfBounds();
            head += sizeof(ulong);
            if (BitConverter.IsLittleEndian)
            {
                Unsafe.WriteUnaligned(ref blob[0], value);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Writes a byte to a blob.
        /// </summary>
        public unsafe static void WriteByte(ref byte* blob, byte value)
        {
            *blob = value;
            blob++;
        }

        /// <summary>
        /// Writes an array of bytes to a blob.
        /// </summary>
        public unsafe static void WriteRaw(ref byte* blob, ReadOnlySpan<byte> buffer)
        {
            Binary.Copy(buffer, blob);
            blob += buffer.Length;
        }

        /// <summary>
        /// Throws an exception that indicates that the bounds of a blob have been reached.
        /// </summary>
        internal static void _throwOutOfBounds()
        {
            throw new Exception("Out of bounds");
        }
    }

    /// <summary>
    /// A <see cref="IBinaryReader"/> which reads from a finite source and allows seeking.
    /// </summary>
    public interface IBlobReader : IBinaryReader
    {
        /// <summary>
        /// The current position of this reader in the underlying source.
        /// </summary>
        ulong Head { get; set; }

        /// <summary>
        /// The length of the underlying source.
        /// </summary>
        ulong Length { get; }
    }
}
