﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Alunite.Data.Serialization
{
    /// <summary>
    /// An interface for reading arbitrary data from a binary stream.
    /// </summary>
    public interface IBinaryReader
    {
        /// <summary>
        /// A buffer containing the next few unread bytes from the stream. The size of this buffer is arbitrary
        /// and variable, however, <see cref="Prepare(uint)"/> can be used populate it up to a given minimum size.
        /// The contents of the buffer are invalidated upon calling <see cref="Prepare(uint)"/>.
        /// </summary>
        ReadOnlySpan<byte> Buffer { get; }

        /// <summary>
        /// Advances the stream by the given number of bytes.
        /// </summary>
        void Skip(uint num);

        /// <summary>
        /// Asserts that there are at least <paramref name="num"/> bytes remaining in the stream, and populates
        /// <see cref="Buffer"/> up to a length of at least <paramref name="num"/> or <see cref="Binary.MinBufferCapacity"/>
        /// (whichever is smaller).
        /// </summary>
        void Prepare(uint num);
    }

    /// <summary>
    /// An <see cref="IBinaryReader"/> whose length is known.
    /// </summary>
    public interface IBoundedBinaryReader : IBinaryReader
    {
        /// <summary>
        /// Populates <see cref="IBinaryReader.Buffer"/> with as many bytes as possible. Returns <see cref="true"/> iff
        /// all remaining bytes have been populated. If this returns <see cref="false"/>, at least
        /// <see cref="Binary.MinBufferCapacity"/> bytes must have been populated.
        /// </summary>
        bool PrepareMax();
    }

    /// <summary>
    /// An <see cref="IBinaryReader"/> which tracks its position in the data source, allowing for arbitrary seeking of
    /// the reader.
    /// </summary>
    public interface ISeekableBinaryReader : IBoundedBinaryReader
    {
        /// <summary>
        /// Gets or sets the position of the next unread byte within the underlying data source. Note that unless otherwise
        /// specified, the origin of this position is arbitrary and only relative differences are meaningful.
        /// </summary>
        ulong Position { get; set; }
    }

    /// <summary>
    /// Contains helper functions related to <see cref="IBinaryReader"/>.
    /// </summary>
    public static class BinaryReader
    {
        /// <summary>
        /// Reads a value from the stream using the given <see cref="Encoding{T}"/>.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static unsafe T Read<T>(this IBinaryReader reader, Encoding<T> encoding)
        {
            Read(reader, encoding, out T res);
            return res;
        }

        /// <summary>
        /// Reads a value from the stream using the given <see cref="Encoding{T}"/>.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static unsafe void Read<T>(this IBinaryReader reader, Encoding<T> encoding, out T value)
        {
            reader.Prepare(encoding.Size.Min);
            ReadOnlySpan<byte> buffer = reader.Buffer;
            ref byte head = ref MemoryMarshal.GetReference(buffer);
            if (false) // TODO: Alignment check
            {
                ref byte nHead = ref encoding.FastReadAligned(ref head, ref buffer, reader, out value);
                reader.Skip((uint)Unsafe.ByteOffset(ref Unsafe.AsRef(in buffer[0]), ref nHead));
            }
            else
            {
                ref byte nHead = ref encoding.FastRead(ref head, ref buffer, reader, out value);
                reader.Skip((uint)Unsafe.ByteOffset(ref Unsafe.AsRef(in buffer[0]), ref nHead));
            }
        }

        /// <summary>
        /// Reads a fixed-length span of bytes from the stream. The span is valid only until the
        /// next call is made to the reader.
        /// </summary>
        public static ReadOnlySpan<byte> ReadRaw(this IBinaryReader reader, uint num)
        {
            // Try to return a reference to the buffer
            ReadOnlySpan<byte> buffer = reader.Buffer;
            if (num <= (uint)buffer.Length)
            {
                reader.Skip(num);
                return buffer.Slice(0, (int)num);
            }

            // Try again, more insistently
            reader.Prepare(num);
            buffer = reader.Buffer;
            if (num <= (uint)buffer.Length)
            {
                reader.Skip(num);
                return buffer.Slice(0, (int)num);
            }

            // Okay fine, copy the data into a new buffer and return that
            byte[] res = new byte[num];
            Binary.Copy(buffer, res.AsSpan(0, buffer.Length));
            Span<byte> rem = res.AsSpan(buffer.Length);
            reader.Skip((uint)buffer.Length);
            reader.Prepare((uint)rem.Length);
            reader.ReadRaw(rem);
            return res;
        }

        /// <summary>
        /// Reads a span of bytes from the stream.
        /// </summary>
        public static void ReadRaw(this IBinaryReader reader, Span<byte> target)
        {
        next:
            ReadOnlySpan<byte> buffer = reader.Buffer;
            if (target.Length <= buffer.Length)
            {
                // Final copy, limited by target size
                Binary.Copy(buffer.Slice(0, target.Length), target);
                reader.Skip((uint)target.Length);
            }
            else
            {
                // Intermediate copy, limited by source buffer size
                Binary.Copy(buffer, target.Slice(0, buffer.Length));
                target = target.Slice(buffer.Length);
                reader.Skip((uint)buffer.Length);
                reader.Prepare((uint)target.Length);
                goto next;
            }
        }

        /// <summary>
        /// Reads a <see cref="byte"/> from the stream.
        /// </summary>
        public static byte ReadByte(this IBinaryReader reader)
        {
        retry:
            const byte size = sizeof(byte);
            ReadOnlySpan<byte> buffer = reader.Buffer;
            if (buffer.Length < size)
            {
                reader.Prepare(size);
                goto retry;
            }
            byte res = buffer[0];
            reader.Skip(size);
            return res;
        }

        /// <summary>
        /// Reads a <see cref="ushort"/> from the stream assuming little endian byte order.
        /// </summary>
        public static ushort ReadUInt16(this IBinaryReader reader)
        {
            if (BitConverter.IsLittleEndian)
                return ReadNativeUInt16(reader);
            else
                return Bitwise.Reverse8(ReadNativeUInt16(reader));
        }

        /// <summary>
        /// Reads a <see cref="uint"/> from the stream assuming little endian byte order.
        /// </summary>
        public static uint ReadUInt32(this IBinaryReader reader)
        {
            if (BitConverter.IsLittleEndian)
                return ReadNativeUInt32(reader);
            else
                return Bitwise.Reverse8(ReadNativeUInt32(reader));
        }

        /// <summary>
        /// Reads a <see cref="ulong"/> from the stream assuming little endian byte order.
        /// </summary>
        public static ulong ReadUInt64(this IBinaryReader reader)
        {
            if (BitConverter.IsLittleEndian)
                return ReadNativeUInt64(reader);
            else
                return Bitwise.Reverse8(ReadNativeUInt64(reader));
        }

        /// <summary>
        /// Reads a <see cref="float"/> from the stream assuming little endian byte order.
        /// </summary>
        public static float ReadSingle(this IBinaryReader reader)
        {
            // TODO: Considerations for systems with different byte order for floats and ints
            if (BitConverter.IsLittleEndian)
                return ReadNativeUInt32(reader).AsSingle();
            else
                return Bitwise.Reverse8(ReadNativeUInt32(reader)).AsSingle();
        }

        /// <summary>
        /// Reads a <see cref="double"/> from the stream assuming little endian byte order.
        /// </summary>
        public static double ReadDouble(this IBinaryReader reader)
        {
            // TODO: Considerations for systems with different byte order for floats and ints
            if (BitConverter.IsLittleEndian)
                return ReadNativeUInt64(reader).AsDouble();
            else
                return Bitwise.Reverse8(ReadNativeUInt64(reader)).AsDouble();
        }

        /// <summary>
        /// Reads a <see cref="ushort"/> from the stream assuming big endian byte order.
        /// </summary>
        public static ushort ReadNetworkUInt16(this IBinaryReader reader)
        {
            if (BitConverter.IsLittleEndian)
                return Bitwise.Reverse8(ReadNativeUInt16(reader));
            else
                return ReadNativeUInt16(reader);
        }

        /// <summary>
        /// Reads a <see cref="uint"/> from the stream assuming big endian byte order.
        /// </summary>
        public static uint ReadNetworkUInt32(this IBinaryReader reader)
        {
            if (BitConverter.IsLittleEndian)
                return Bitwise.Reverse8(ReadNativeUInt32(reader));
            else
                return ReadNativeUInt32(reader);
        }

        /// <summary>
        /// Reads a <see cref="ulong"/> from the stream assuming big endian byte order.
        /// </summary>
        public static ulong ReadNetworkUInt64(this IBinaryReader reader)
        {
            if (BitConverter.IsLittleEndian)
                return Bitwise.Reverse8(ReadNativeUInt64(reader));
            else
                return ReadNativeUInt64(reader);
        }

        /// <summary>
        /// Reads a <see cref="ushort"/> from the stream using native byte order.
        /// </summary>
        public static ushort ReadNativeUInt16(this IBinaryReader reader)
        {
        retry:
            const byte size = sizeof(ushort);
            ReadOnlySpan<byte> buffer = reader.Buffer;
            if (buffer.Length < size)
            {
                reader.Prepare(size);
                goto retry;
            }
            ushort res = Unsafe.ReadUnaligned<ushort>(ref MemoryMarshal.GetReference(buffer));
            reader.Skip(size);
            return res;
        }

        /// <summary>
        /// Reads a <see cref="uint"/> from the stream using native byte order.
        /// </summary>
        public static unsafe uint ReadNativeUInt32(this IBinaryReader reader)
        {
        retry:
            const byte size = sizeof(uint);
            ReadOnlySpan<byte> buffer = reader.Buffer;
            if (buffer.Length < size)
            {
                reader.Prepare(size);
                goto retry;
            }
            uint res = Unsafe.ReadUnaligned<uint>(ref MemoryMarshal.GetReference(buffer));
            reader.Skip(size);
            return res;
        }

        /// <summary>
        /// Reads a <see cref="ulong"/> from the stream using native byte order.
        /// </summary>
        public static unsafe ulong ReadNativeUInt64(this IBinaryReader reader)
        {
        retry:
            const byte size = sizeof(ulong);
            ReadOnlySpan<byte> buffer = reader.Buffer;
            if (buffer.Length < size)
            {
                reader.Prepare(size);
                goto retry;
            }
            ulong res = Unsafe.ReadUnaligned<ulong>(ref MemoryMarshal.GetReference(buffer));
            reader.Skip(size);
            return res;
        }

        /// <summary>
        /// Reads a <see cref="short"/> from the stream using native byte order.
        /// </summary>
        public static short ReadNativeInt16(this IBinaryReader reader)
        {
            return unchecked((short)reader.ReadNativeUInt16());
        }

        /// <summary>
        /// Reads a <see cref="int"/> from the stream using native byte order.
        /// </summary>
        public static unsafe int ReadNativeInt32(this IBinaryReader reader)
        {
            return unchecked((int)reader.ReadNativeUInt32());
        }

        /// <summary>
        /// Reads a <see cref="ulong"/> from the stream using native byte order.
        /// </summary>
        public static unsafe long ReadNativeInt64(this IBinaryReader reader)
        {
            return unchecked((long)reader.ReadNativeUInt64());
        }

        /// <summary>
        /// Reads a <see cref="float"/> from the stream using native byte order.
        /// </summary>
        public static unsafe float ReadNativeSingle(this IBinaryReader reader)
        {
        retry:
            const byte size = sizeof(float);
            ReadOnlySpan<byte> buffer = reader.Buffer;
            if (buffer.Length < size)
            {
                reader.Prepare(size);
                goto retry;
            }
            float res = Unsafe.ReadUnaligned<float>(ref MemoryMarshal.GetReference(buffer));
            reader.Skip(size);
            return res;
        }

        /// <summary>
        /// Reads a <see cref="double"/> from the stream using native byte order.
        /// </summary>
        public static unsafe double ReadNativeDouble(this IBinaryReader reader)
        {
        retry:
            const byte size = sizeof(double);
            ReadOnlySpan<byte> buffer = reader.Buffer;
            if (buffer.Length < size)
            {
                reader.Prepare(size);
                goto retry;
            }
            double res = Unsafe.ReadUnaligned<double>(ref MemoryMarshal.GetReference(buffer));
            reader.Skip(size);
            return res;
        }

        /// <summary>
        /// Reads a blittable value from the stream using native byte order.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static unsafe T ReadNative<T>(this IBinaryReader reader)
            where T : unmanaged
        {
            ReadNative(reader, out T res);
            return res;
        }

        /// <summary>
        /// Reads a blittable value from the stream using native byte order.
        /// </summary>
        public static unsafe void ReadNative<T>(this IBinaryReader reader, out T value)
            where T : unmanaged
        {
        retry:
            uint size = (uint)sizeof(T);
            ReadOnlySpan<byte> buffer = reader.Buffer;
            if ((uint)buffer.Length < size)
            {
                reader.Prepare(size);
                goto retry;
            }
            value = Unsafe.ReadUnaligned<T>(ref MemoryMarshal.GetReference(buffer));
            reader.Skip(size);
        }

        /// <summary>
        /// Reads a <see cref="ushort"/> from the stream using native byte order, assuming the stream is
        /// currently aligned to a 2-byte boundary.
        /// </summary>
        public static unsafe ushort ReadAlignedNativeUInt16(this IBinaryReader reader)
        {
        retry:
            const byte size = sizeof(ushort);
            ReadOnlySpan<byte> buffer = reader.Buffer;
            if (buffer.Length < size)
            {
                reader.Prepare(size);
                goto retry;
            }
            ushort res = Unsafe.As<byte, ushort>(ref MemoryMarshal.GetReference(buffer));
            reader.Skip(size);
            return res;
        }

        /// <summary>
        /// Reads a <see cref="uint"/> from the stream using native byte order, assuming the stream is
        /// currently aligned to a 4-byte boundary.
        /// </summary>
        public static unsafe uint ReadAlignedNativeUInt32(this IBinaryReader reader)
        {
        retry:
            const byte size = sizeof(uint);
            ReadOnlySpan<byte> buffer = reader.Buffer;
            if (buffer.Length < size)
            {
                reader.Prepare(size);
                goto retry;
            }
            uint res = Unsafe.As<byte, uint>(ref MemoryMarshal.GetReference(buffer));
            reader.Skip(size);
            return res;
        }

        /// <summary>
        /// Reads a <see cref="ulong"/> from the stream using native byte order, assuming the stream is
        /// currently aligned to a 8-byte boundary.
        /// </summary>
        public static unsafe ulong ReadAlignedNativeUInt64(this IBinaryReader reader)
        {
        retry:
            const byte size = sizeof(ulong);
            ReadOnlySpan<byte> buffer = reader.Buffer;
            if (buffer.Length < size)
            {
                reader.Prepare(size);
                goto retry;
            }
            ulong res = Unsafe.As<byte, ulong>(ref MemoryMarshal.GetReference(buffer));
            reader.Skip(size);
            return res;
        }

        /// <summary>
        /// Reads a <see cref="short"/> from the stream using native byte order, assuming the stream is
        /// currently aligned to a 2-byte boundary.
        /// </summary>
        public static short ReadAlignedNativeInt16(this IBinaryReader reader)
        {
            return unchecked((short)reader.ReadAlignedNativeUInt16());
        }

        /// <summary>
        /// Reads a <see cref="int"/> from the stream using native byte order, assuming the stream is
        /// currently aligned to a 4-byte boundary.
        /// </summary>
        public static unsafe int ReadAlignedNativeInt32(this IBinaryReader reader)
        {
            return unchecked((int)reader.ReadAlignedNativeUInt32());
        }

        /// <summary>
        /// Reads a <see cref="ulong"/> from the stream using native byte order, assuming the stream is
        /// currently aligned to a 8-byte boundary.
        /// </summary>
        public static unsafe long ReadAlignedNativeInt64(this IBinaryReader reader)
        {
            return unchecked((long)reader.ReadAlignedNativeUInt64());
        }

        /// <summary>
        /// Reads a <see cref="float"/> from the stream using native byte order, assuming the stream is
        /// currently aligned to a 4-byte boundary.
        /// </summary>
        public static unsafe float ReadAlignedNativeSingle(this IBinaryReader reader)
        {
        retry:
            const byte size = sizeof(float);
            ReadOnlySpan<byte> buffer = reader.Buffer;
            if (buffer.Length < size)
            {
                reader.Prepare(size);
                goto retry;
            }
            float res = Unsafe.As<byte, float>(ref MemoryMarshal.GetReference(buffer));
            reader.Skip(size);
            return res;
        }

        /// <summary>
        /// Reads a <see cref="double"/> from the stream using native byte order, assuming the stream is
        /// currently aligned to a 8-byte boundary.
        /// </summary>
        public static unsafe double ReadAlignedNativeDouble(this IBinaryReader reader)
        {
        retry:
            const byte size = sizeof(double);
            ReadOnlySpan<byte> buffer = reader.Buffer;
            if (buffer.Length < size)
            {
                reader.Prepare(size);
                goto retry;
            }
            double res = Unsafe.As<byte, double>(ref MemoryMarshal.GetReference(buffer));
            reader.Skip(size);
            return res;
        }

        /// <summary>
        /// Reads a blittable value from the stream using native byte order, assuming the stream is
        /// currently aligned according to <see cref="Util.AlignOf{T}"/>.
        /// </summary>
        public static unsafe void ReadAlignedNative<T>(this IBinaryReader reader, out T value)
            where T : unmanaged
        {
        retry:
            uint size = (uint)sizeof(T);
            ReadOnlySpan<byte> buffer = reader.Buffer;
            if ((uint)buffer.Length < size)
            {
                reader.Prepare(size);
                goto retry;
            }
            value = Unsafe.As<byte, T>(ref MemoryMarshal.GetReference(buffer));
            reader.Skip(size);
        }

        /// <summary>
        /// Reads a blittable value from the stream using native byte order, assuming the stream is
        /// currently aligned according to <see cref="Util.AlignOf{T}"/>.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static unsafe T ReadAlignedNative<T>(this IBinaryReader reader)
            where T : unmanaged
        {
            ReadAlignedNative(reader, out T res);
            return res;
        }
    }
}
