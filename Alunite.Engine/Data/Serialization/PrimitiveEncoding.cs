﻿using System;
using System.Runtime.CompilerServices;

namespace Alunite.Data.Serialization
{
    public partial class Encoding
    {
        /// <summary>
        /// Contains potential encodings for <see cref="byte"/> values.
        /// </summary>
        public static class Byte
        {
            /// <summary>
            /// An encoding for <see cref="byte"/> as an 8-bit integer.
            /// </summary>
            [StandardEncoding, DefaultEncoding]
            public static Encoding<byte> I8 => new BlittableEncoding<byte>(1, 1);
        }

        /// <summary>
        /// Contains potential encodings for <see cref="ushort"/> values.
        /// </summary>
        public static class UInt16
        {
            /// <summary>
            /// An encoding for <see cref="ushort"/> as 8-bit integers.
            /// </summary>
            public static Encoding<ushort> I8 => _I8.Instance;

            /// <summary>
            /// An encoding for <see cref="ushort"/> as 16-bit little-endian integers.
            /// </summary>
            [StandardEncoding]
            public static Encoding<ushort> I16 => BitConverter.IsLittleEndian ? In16 : throw new NotImplementedException();

            /// <summary>
            /// An encoding for <see cref="ushort"/> as 16-bit native-endian integers.
            /// </summary>
            [DefaultEncoding]
            public static Encoding<ushort> In16 => new BlittableEncoding<ushort>(2, 2);
            
            /// <summary>
            /// The implementation for <see cref="I8"/>.
            /// </summary>
            private sealed class _I8 : Encoding<ushort>
            {
                public _I8() : base(1, 1) { }

                /// <summary>
                /// The only instance of this class.
                /// </summary>
                public static _I8 Instance { get; } = new _I8();

                public override ref byte FastRead(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out ushort value)
                {
                    return ref FastReadAligned(ref head, ref buffer, reader, out value);
                }

                public override ref byte FastReadAligned(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out ushort value)
                {
                    value = head;
                    return ref Unsafe.AddByteOffset(ref head, (IntPtr)1);
                }

                public override ref byte FastWrite(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in ushort value)
                {
                    return ref FastWriteAligned(ref head, ref buffer, writer, in value);
                }

                public override ref byte FastWriteAligned(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in ushort value)
                {
                    head = (byte)value;
                    return ref Unsafe.AddByteOffset(ref head, (IntPtr)1);
                }
            }
        }

        /// <summary>
        /// Contains potential encodings for <see cref="uint"/> values.
        /// </summary>
        public static class UInt32
        {
            /// <summary>
            /// An encoding for <see cref="uint"/> as 8-bit integers.
            /// </summary>
            public static Encoding<uint> I8 => _I8.Instance;

            /// <summary>
            /// An encoding for <see cref="uint"/> as 16-bit little-endian integers.
            /// </summary>
            public static Encoding<uint> I16 => BitConverter.IsLittleEndian ? In16 : throw new NotImplementedException();

            /// <summary>
            /// An encoding for <see cref="uint"/> as 32-bit little-endian integers.
            /// </summary>
            [StandardEncoding]
            public static Encoding<uint> I32 => BitConverter.IsLittleEndian ? In32 : throw new NotImplementedException();

            /// <summary>
            /// An encoding for <see cref="uint"/> as 16-bit native-endian integers.
            /// </summary>
            public static Encoding<uint> In16 => _In16.Instance;

            /// <summary>
            /// An encoding for <see cref="uint"/> as 32-bit native-endian integers.
            /// </summary>
            [DefaultEncoding]
            public static Encoding<uint> In32 { get; } = new BlittableEncoding<uint>(4, 4);

            /// <summary>
            /// The implementation for <see cref="I8"/>.
            /// </summary>
            private sealed class _I8 : Encoding<uint>
            {
                public _I8() : base(1, 1) { }

                /// <summary>
                /// The only instance of this class.
                /// </summary>
                public static _I8 Instance { get; } = new _I8();

                public override ref byte FastRead(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out uint value)
                {
                    return ref FastReadAligned(ref head, ref buffer, reader, out value);
                }

                public override ref byte FastReadAligned(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out uint value)
                {
                    value = head;
                    return ref Unsafe.AddByteOffset(ref head, (IntPtr)1);
                }

                public override ref byte FastWrite(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in uint value)
                {
                    return ref FastWriteAligned(ref head, ref buffer, writer, in value);
                }

                public override ref byte FastWriteAligned(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in uint value)
                {
                    head = (byte)value;
                    return ref Unsafe.AddByteOffset(ref head, (IntPtr)1);
                }
            }

            /// <summary>
            /// The implementation for <see cref="In16"/>.
            /// </summary>
            private sealed class _In16 : Encoding<uint>
            {
                public _In16() : base(2, 2) { }

                /// <summary>
                /// The only instance of this class.
                /// </summary>
                public static _In16 Instance { get; } = new _In16();

                public override ref byte FastRead(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out uint value)
                {
                    value = Unsafe.ReadUnaligned<ushort>(ref head);
                    return ref Unsafe.AddByteOffset(ref head, (IntPtr)2);
                }

                public override ref byte FastReadAligned(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out uint value)
                {
                    value = Unsafe.As<byte, ushort>(ref head);
                    return ref Unsafe.AddByteOffset(ref head, (IntPtr)2);
                }

                public override ref byte FastWrite(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in uint value)
                {
                    Unsafe.WriteUnaligned(ref head, (ushort)value);
                    return ref Unsafe.AddByteOffset(ref head, (IntPtr)2);
                }

                public override ref byte FastWriteAligned(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in uint value)
                {
                    Unsafe.As<byte, ushort>(ref head) = (ushort)value;
                    return ref Unsafe.AddByteOffset(ref head, (IntPtr)2);
                }
            }
        }
    }
}
