﻿using System;
using System.Diagnostics;

namespace Alunite.Data.Serialization
{
    /// <summary>
    /// Describes a packing of a fixed sequence of fields (possibly of variable size) into a binary stream. Each
    /// field is identified by a <see cref="uint"/>.
    /// </summary>
    public sealed class BinaryLayout
    {
        internal BinaryLayout(BinarySize size, BinaryAlign align, List<BinaryLayoutOperation> ops)
        {
            Size = size;
            Align = align;
            Operations = ops;
        }
        /// <summary>
        /// The size of this layout.
        /// </summary>
        public BinarySize Size { get; }

        /// <summary>
        /// The alignment required for an aligned read or write of this layout.
        /// </summary>
        public BinaryAlign Align { get; }

        /// <summary>
        /// A sequence of operations which will read or write a value using this layout. Before executing,
        /// <see cref="BinarySize.Min"/> or <see cref="Binary.MinBufferCapacity"/> (whichever is smaller) bytes
        /// must be prepared on the stream.
        /// </summary>
        public List<BinaryLayoutOperation> Operations { get; }
    }

    /// <summary>
    /// A lax approximation of the possible sizes for a binary value.
    /// </summary>
    public struct BinarySize
    {
        internal BinarySize(uint min, ushort inc, ushort mult)
        {
            Min = min;
            Increment = inc;
            Multiplicity = mult;
        }

        /// <summary>
        /// The minimum size in bytes.
        /// </summary>
        public uint Min { get; }

        /// <summary>
        /// The maximum size in bytes, or <see cref="uint.MaxValue"/> if there is no limit.
        /// </summary>
        public uint Max
        {
            get
            {
                if (Multiplicity == ushort.MaxValue)
                    return uint.MaxValue;
                return Min + (uint)Increment * Multiplicity;
            }
        }

        /// <summary>
        /// The increment that extra bytes can be added in.
        /// </summary>
        public ushort Increment { get; }

        /// <summary>
        /// The maximum number of <see cref="Increment"/> bytes that can be added to the size, or
        /// <see cref="ushort.MaxValue"/> if there is no limit.
        /// </summary>
        public ushort Multiplicity { get; }

        /// <summary>
        /// Indicates whether this <see cref="BinarySize"/> has no maximum size.
        /// </summary>
        public bool IsUnbounded => Multiplicity == ushort.MaxValue;

        /// <summary>
        /// Gets the possible sizes for combining a value of size <paramref name="a"/> and a value of
        /// size <paramref name="b"/> with no additional padding.
        /// </summary>
        public static BinarySize operator +(BinarySize a, BinarySize b)
        {
            uint min = a.Min + b.Min;
            uint inc = Util.Gcd(a.Increment, b.Increment);
            if (inc == 0)
                return new BinarySize(min, 0, 0);
            uint mult = (a.Increment / inc) * a.Multiplicity + (b.Increment / inc) * b.Multiplicity;
            if (mult > ushort.MaxValue)
                return new BinarySize(min, (ushort)inc, ushort.MaxValue);
            return new BinarySize(min, (ushort)inc, (ushort)mult);
        }

        /// <summary>
        /// Gets a <see cref="BinarySize"/> for a fixed number of bytes.
        /// </summary>
        public static implicit operator BinarySize(uint size)
        {
            return new BinarySize(size, 0, 0);
        }
    }

    /// <summary>
    /// A periodic set of pointers or offsets which describe the alignment of a point in a binary stream.
    /// </summary>
    public struct BinaryAlign
    {
        internal ushort _period;
        internal ushort _offset;
        internal BinaryAlign(ushort period, ushort offset)
        {
            Debug.Assert(offset < period);
            _period = period;
            _offset = offset;
        }

        /// <summary>
        /// The number of bytes between members of this alignment. This must be a power of 2.
        /// </summary>
        public uint Period => _period;

        /// <summary>
        /// The offset of this alignment from a multiple of <see cref="Period"/>. This must be less than
        /// <see cref="Period"/>.
        /// </summary>
        public uint Offset => _offset;

        /// <summary>
        /// Constructs a <see cref="BinaryAlign"/> with the given period.
        /// </summary>
        public static implicit operator BinaryAlign(uint period)
        {
            if (period == 0)
                throw new ArgumentException("Period may not be zero", nameof(period));
            if (Bitwise.UpperPow2(period) != period)
                throw new ArgumentException("Period must be a power of 2", nameof(period));
            return new BinaryAlign((ushort)period, 0);
        }
    }

    /// <summary>
    /// An operation in the procedure to read or write a struct using a particular <see cref="BinaryLayout"/>.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct BinaryLayoutOperation
    {
        private uint _def;
        private BinaryLayoutOperation(BinaryLayoutOperationKind kind, uint arg)
        {
            Debug.Assert(arg == (arg & 0x3FFFFFFF));
            _def = ((uint)kind << 30) | arg;
        }

        /// <summary>
        /// The kind of this operation.
        /// </summary>
        public BinaryLayoutOperationKind Kind => (BinaryLayoutOperationKind)(_def >> 30);

        /// <summary>
        /// The argument for this operation.
        /// </summary>
        public uint Argument => _def & 0x3FFFFFFF;

        /// <summary>
        /// Constructs a <see cref="BinaryLayoutOperation"/> which advances the stream by the given number of bytes.
        /// The skipped bytes must be <see cref="Prepare(uint)"/>d.
        /// </summary>
        public static BinaryLayoutOperation Skip(uint num)
        {
            return new BinaryLayoutOperation(BinaryLayoutOperationKind.Skip, num);
        }
        
        /// <summary>
        /// Constructs a <see cref="BinaryLayoutOperation"/> which ensures that the given number of stream bytes are
        /// prepared for reading or writing.
        /// </summary>
        public static BinaryLayoutOperation Prepare(uint num)
        {
            return new BinaryLayoutOperation(BinaryLayoutOperationKind.Prepare, num);
        }

        /// <summary>
        /// Constructs a <see cref="BinaryLayoutOperation"/> which reads or writes the field with the given index,
        /// assuming buffer requirements have been met.
        /// </summary>
        /// <param name="isAligned">Indicates whether the field alignment is satisfied to allow for an aligned
        /// read/write, assuming the alignment requirements for the containing struct have been met.</param>
        public static BinaryLayoutOperation Field(uint id, bool isAligned)
        {
            var kind = isAligned ? BinaryLayoutOperationKind.FieldAligned : BinaryLayoutOperationKind.Field;
            return new BinaryLayoutOperation(kind, id);
        }

        public override string ToString()
        {
            return Kind.ToString() + "(" + Argument.ToStringInvariant() + ")";
        }
    }

    /// <summary>
    /// Identifies a kind of <see cref="BinaryLayoutOperation"/>.
    /// </summary>
    public enum BinaryLayoutOperationKind : byte
    {
        Skip,
        Prepare,
        Field,
        FieldAligned
    }
    
    /// <summary>
    /// A helper class for constructing an optimized <see cref="BinaryLayout"/> by incrementally appending fields.
    /// </summary>
    public sealed class BinaryLayoutBuilder : IBuilder<BinaryLayout>
    {
        // TODO: Rearrange fields to improve layout
        private ListBuilder<BinaryLayoutOperation> _ops;
        private uint _prepared;
        private BinarySize _size;
        private BinaryAlign _align;
        private BinaryLayoutBuilder(EncodingStrategy strat)
        {
            Strategy = strat;
            _ops = new ListBuilder<BinaryLayoutOperation>();
            _prepared = Binary.MinBufferCapacity;
            _size = 0;
            _align = 1;
        }

        /// <summary>
        /// The <see cref="EncodingStrategy"/> used to construct the binary layout.
        /// </summary>
        public EncodingStrategy Strategy { get; }

        /// <summary>
        /// Creates a new initially-empty <see cref="BinaryLayoutBuilder"/>.
        /// </summary>
        public static BinaryLayoutBuilder Create(EncodingStrategy strat)
        {
            return new BinaryLayoutBuilder(strat);
        }
        
        /// <summary>
        /// Appends a field with the given layout parameters to the resulting <see cref="BinaryLayout"/>.
        /// </summary>
        /// <param name="id">The identifier for the field.</param>
        public void Append(uint id, BinarySize size, BinaryAlign align)
        {
            // Can we align the field?
            bool isAligned = false;
            if (_size.Increment % align.Period == 0)
            {
                // Is the field already aligned?
                uint curOffset = _size.Min % align.Period;
                if (curOffset == align.Offset)
                {
                    isAligned = true;
                }
                else if (Strategy == EncodingStrategy.Fast ||
                    (Strategy == EncodingStrategy.Balanced && size.IsUnbounded))
                {
                    // Skip bytes to maintain alignment
                    uint numSkip = (align.Period + align.Offset - curOffset) % align.Period;
                    _ops.Append(BinaryLayoutOperation.Skip(numSkip));
                    _size += numSkip;
                    _prepared = Saturate.Sub(_prepared, numSkip);
                    isAligned = true;
                }
            }

            // Do we need to prepare the buffer?
            if (_prepared < size.Min && _prepared < Binary.MinBufferCapacity)
            {
                _ops.Append(BinaryLayoutOperation.Prepare(_size.Min)); // We'll update with the correct size later
                _prepared = Binary.MinBufferCapacity;
            }

            // Read/write field
            _ops.Append(BinaryLayoutOperation.Field(id, isAligned));
            _size += size;
            _prepared = Saturate.Sub(_prepared, size.Max);
        }

        /// <summary>
        /// Gets the resulting <see cref="BinaryLayout"/>.
        /// </summary>
        public BinaryLayout Finish()
        {
            // Set prepare numbers to the minimum number of remaining bytes
            for (uint i = 0; i < _ops.Length; i++)
            {
                ref BinaryLayoutOperation op = ref _ops[i];
                if (op.Kind == BinaryLayoutOperationKind.Prepare)
                    op = BinaryLayoutOperation.Prepare(_size.Min - op.Argument);
            }

            // Finalize layout
            return new BinaryLayout(_size, _align, _ops.Finish());
        }
    }
}
