﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Alunite.Data.Serialization
{
    /// <summary>
    /// An interface for writing arbitrary data to a binary stream.
    /// </summary>
    public interface IBinaryWriter
    {
        /// <summary>
        /// A buffer containing the next few bytes to be written to the stream. The size of this buffer is arbitrary
        /// and variable, however, <see cref="Prepare(uint)"/> can be used allocate up to a given minimum size.
        /// </summary>
        Span<byte> Buffer { get; }

        /// <summary>
        /// Removes the given number of bytes from the beginning of <see cref="Buffer"/> and writes them to the stream.
        /// </summary>
        void Flush(uint num);

        /// <summary>
        /// Asserts that at least <paramref name="num"/> more bytes will be written to the stream, and ensures
        /// that <see cref="Buffer"/> has a length of at least <paramref name="num"/> or <see cref="Binary.MinBufferCapacity"/>
        /// (whichever is smaller).
        /// </summary>
        void Prepare(uint num);
    }

    /// <summary>
    /// Contains helper functions related to <see cref="IBinaryWriter"/>.
    /// </summary>
    public static class BinaryWriter
    {
        /// <summary>
        /// Writes a value to the stream using the given <see cref="Encoding{T}"/>.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static unsafe void Write<T>(this IBinaryWriter writer, Encoding<T> encoding, in T value)
        {
            writer.Prepare(encoding.Size.Min);
            Span<byte> buffer = writer.Buffer;
            ref byte head = ref MemoryMarshal.GetReference(buffer);
            if (false) // TODO: Alignment check
            {
                ref byte nHead = ref encoding.FastWriteAligned(ref head, ref buffer, writer, in value);
                writer.Flush((uint)Unsafe.ByteOffset(ref buffer[0], ref nHead));
            }
            else
            {
                ref byte nHead = ref encoding.FastWrite(ref head, ref buffer, writer, in value);
                writer.Flush((uint)Unsafe.ByteOffset(ref buffer[0], ref nHead));
            }
        }

        /// <summary>
        /// Writes a blittable value to the stream using native byte order.
        /// </summary>
        public static unsafe void WriteNative<T>(this IBinaryWriter writer, in T value)
            where T : unmanaged
        {
        retry:
            uint size = (uint)sizeof(T);
            Span<byte> buffer = writer.Buffer;
            if ((uint)buffer.Length < size)
            {
                writer.Prepare(size);
                goto retry;
            }
            Unsafe.WriteUnaligned(ref MemoryMarshal.GetReference(buffer), value);
            writer.Flush(size);
        }

        /// <summary>
        /// Writes a blittable value to the stream using native byte order, assuming the stream is
        /// currently aligned according to <see cref="Util.AlignOf{T}"/>.
        /// </summary>
        public static unsafe void WriteAlignedNative<T>(this IBinaryWriter writer, in T value)
            where T : unmanaged
        {
        retry:
            uint size = (uint)sizeof(T);
            Span<byte> buffer = writer.Buffer;
            if ((uint)buffer.Length < size)
            {
                writer.Prepare(size);
                goto retry;
            }
            Unsafe.As<byte, T>(ref MemoryMarshal.GetReference(buffer)) = value;
            writer.Flush(size);
        }
    }
}
