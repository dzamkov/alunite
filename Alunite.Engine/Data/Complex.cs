﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace Alunite.Data
{
    /// <summary>
    /// A complex numeric value.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct Complex
    {
        public Complex(Scalar a, Scalar b)
        {
            A = a;
            B = b;
        }

        /// <summary>
        /// The real component of this complex number.
        /// </summary>
        public Scalar A;

        /// <summary>
        /// The imaginary component of this complex number. 
        /// </summary>
        public Scalar B;

        /// <summary>
        /// The magnitude of this complex number.
        /// </summary>
        public Scalar Magnitude => Scalar.Sqrt(SquareMagnitude);

        /// <summary>
        /// The square of <see cref="Magnitude"/>.
        /// </summary>
        public Scalar SquareMagnitude => A * A + B * B;
        
        /// <summary>
        /// Normalizes a complex number.
        /// </summary>
        public static Complex Normalize(Complex comp)
        {
            Scalar invLen = 1 / comp.Magnitude;
            return new Complex(comp.A * invLen, comp.B * invLen);
        }

        public override string ToString()
        {
            string b = (B >= 0 ? " + " + B.ToString() : " - " + (-B).ToString()) + "i";
            return A.ToString() + b;
        }
    }
}
