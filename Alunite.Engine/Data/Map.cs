﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;

using Alunite.Diagnostics;

namespace Alunite.Data
{
    /// <summary>
    /// A mapping of a set of keys of type <typeparamref name="TKey"/> to values of type <typeparamref name="TValue"/>.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    [DebuggerTypeProxy(typeof(EnumerableDebugView<>))]
    public struct Map<TKey, TValue> : IMap<TKey, TValue>,
        IEnumerable<KeyValuePair<TKey, TValue>>,
        IEquatable<Map<TKey, TValue>>,
        IPrintable
    {
        internal _Bin[] _bins;
        internal Map(_Bin[] bins)
        {
            _bins = bins;
        }

        /// <summary>
        /// Describes a potential entry in the hashtable for a <see cref="Map{TKey, TValue}"/>.
        /// </summary>
        internal struct _Bin
        {
            /// <summary>
            /// The hash code for this entry, or zero if the bin is empty.
            /// </summary>
            public int Hash;

            /// <summary>
            /// The key for this entry, if <see cref="Hash"/> is not zero.
            /// </summary>
            public TKey Key;

            /// <summary>
            /// The value for this entry, if <see cref="Hash"/> is not zero.
            /// </summary>
            public TValue Value;

            /// <summary>
            /// The empty bin.
            /// </summary>
            public static _Bin Empty => new _Bin
            {
                Hash = 0,
                Key = default,
                Value = default
            };
        }

        /// <summary>
        /// The empty <see cref="Map{TKey, TValue}"/>.
        /// </summary>
        public static Map<TKey, TValue> Empty => new Map<TKey, TValue>(_emptyEntries);

        /// <summary>
        /// Constructs a <see cref="Set{T}"/> consisting of a single entry.
        /// </summary>
        public static Map<TKey, TValue> Singleton(TKey key, TValue value)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// A table for <see cref="_bins"/> that is always empty.
        /// </summary>
        internal static readonly _Bin[] _emptyEntries = new[] { _Bin.Empty };

        /// <summary>
        /// Indicates whether this is <see cref="Empty"/>.
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                for (int i = 0; i < _bins.Length; i++)
                    if (_bins[i].Hash != 0)
                        return false;
                return true;
            }
        }

        /// <summary>
        /// Determines whether this map consists of a single entry, and if so, gets its key and value.
        /// </summary>
        public bool IsSingleton(out TKey key, out TValue value)
        {
            int i;
            for (i = 0; i < _bins.Length; i++)
            {
                _Bin bin = _bins[i];
                if (bin.Hash != 0)
                {
                    key = bin.Key;
                    value = bin.Value;
                    goto findOthers;
                }
            }
            key = default;
            value = default;
            return false;

        findOthers:
            for (i++; i < _bins.Length; i++)
                if (_bins[i].Hash != 0)
                    return false;
            return true;
        }

        /// <summary>
        /// Gets the value corresponding to the given key, assuming it exists.
        /// </summary>
        public TValue this[TKey key]
        {
            get
            {
                if (!TryGet(key, out TValue value))
                    throw new KeyNotFoundException();
                return value;
            }
        }
        
        /// <summary>
        /// The number of entries in this map.
        /// </summary>
        public uint Size
        {
            get
            {
                uint size = 0;
                for (int i = 0; i < _bins.Length; i++)
                    if (_bins[i].Hash != 0)
                        size++;
                return size;
            }
        }

        /// <summary>
        /// The set of keys associated in this map.
        /// </summary>
        public MapKeySet<TKey, TValue> Keys => new Data.MapKeySet<TKey, TValue>(_bins);

        /// <summary>
        /// Constructs a list of the values in this <see cref="Map{TKey, TValue}"/> ordered by their
        /// corresponding key.
        /// </summary>
        public List<TValue> Sort<TKeyComparer>(TKeyComparer comparer)
            where TKeyComparer : IComparer<TKey>
        {
            Span<uint> indices = stackalloc uint[_bins.Length];
            uint numIndices = 0;
            for (uint i = 0; i < _bins.Length; i++)
            {
                if (_bins[i].Hash != 0)
                    indices[(int)numIndices++] = i;
            }
            indices = indices.Slice(0, (int)numIndices);
            indices.Sort(new _ValueComparer<TKeyComparer>
            {
                Bins = _bins,
                KeyComparer = comparer
            });
            TValue[] items = new TValue[numIndices];
            for (uint i = 0; i < numIndices; i++)
                items[i] = _bins[indices[(int)i]].Value;
            return new List<TValue>(items);
        }

        /// <summary>
        /// Compares entry indices based on their key.
        /// </summary>
        private struct _ValueComparer<TKeyComparer> : IComparer<uint>
            where TKeyComparer : IComparer<TKey>
        {
            public _Bin[] Bins;
            public TKeyComparer KeyComparer;
            public int Compare(uint x, uint y)
            {
                return KeyComparer.Compare(Bins[x].Key, Bins[y].Key);
            }
        }

        /// <summary>
        /// Determines whether there is an entry for the given key in this map, and if so, gets the
        /// corresponding value.
        /// </summary>
        public bool TryGet(TKey key, out TValue value)
        {
            int hash = _hash(key);
            uint binIndex = _bins.GetBinPow2(hash);
        nextBin:
            ref _Bin bin = ref _bins[binIndex];
            if (bin.Hash != 0)
            {
                if (EqualityHelper.Equals(key, bin.Key))
                {
                    value = bin.Value;
                    return true;
                }
                binIndex = _bins.Next(binIndex);
                goto nextBin;
            }
            else
            {
                value = default;
                return false;
            }
        }

        /// <summary>
        /// Computes the hash code for the given key, automatically "correcting" hash codes of zero.
        /// </summary>
        internal static int _hash(TKey key)
        {
            int hash = EqualityHelper.GetHashCode(key);
            if (hash == 0)
                hash = 1;
            return hash;
        }

        /// <summary>
        /// Gets an enumerator for the entries in this map, in no particular order.
        /// </summary>
        public Enumerator GetEnumerator()
        {
            return new Enumerator(_bins);
        }

        IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator()
        {
            return GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// An enumerator for a <see cref="Map{TKey, TValue}"/>
        /// </summary>
        public struct Enumerator : IEnumerator<KeyValuePair<TKey, TValue>>
        {
            private _Bin[] _bins;
            private uint _index;
            internal Enumerator(_Bin[] bins)
            {
                _bins = bins;
                _index = uint.MaxValue;
            }

            /// <summary>
            /// Gets the current item for this enumerator.
            /// </summary>
            public KeyValuePair<TKey, TValue> Current
            {
                get
                {
                    _Bin entry = _bins[_index];
                    return new KeyValuePair<TKey, TValue>(entry.Key, entry.Value);
                }
            }

            /// <summary>
            /// Advances to the next, or first, item.
            /// </summary>
            public bool MoveNext()
            {
                unchecked
                {
                    while (++_index < (uint)_bins.Length)
                        if (_bins[_index].Hash != 0)
                            return true;
                    return false;
                }
            }

            /// <summary>
            /// Resets the enumerator to before the beginning of the first item.
            /// </summary>
            public void Reset()
            {
                _index = uint.MaxValue;
            }

            object System.Collections.IEnumerator.Current => Current;

            void IDisposable.Dispose() { }
        }

        public static bool operator ==(Map<TKey, TValue> a, Map<TKey, TValue> b)
        {
            if (a.Size != b.Size)
                return false;
            foreach (var entry in a)
                if (!b.TryGet(entry.Key, out TValue bValue) || !EqualityHelper.Equals(entry.Value, bValue))
                    return false;
            return true;
        }

        public static bool operator !=(Map<TKey, TValue> a, Map<TKey, TValue> b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Map<TKey, TValue>))
                return false;
            return this == (Map<TKey, TValue>)obj;
        }

        bool IEquatable<Map<TKey, TValue>>.Equals(Map<TKey, TValue> other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            int hash = 0x15bdc401;
            for (int i = 0; i < _bins.Length; i++)
            {
                _Bin entry = _bins[i];
                hash = unchecked(hash + HashCodeHelper.Combine(entry.Hash, EqualityHelper.GetHashCode(entry.Value)));
            }
            return hash;
        }

        public override string ToString()
        {
            return this.ToShortString();
        }

        void IPrintable.Print(IPrinter printer, Precedence prec)
        {
            if (IsEmpty)
            {
                printer.WriteItem('∅');
            }
            else
            {
                printer.WriteItem('{');
                bool isFirst = true;
                foreach (var entry in this)
                {
                    if (isFirst)
                        isFirst = false;
                    else
                        printer.Write(", ");
                    printer.Print(Precedence.Additive, entry.Key);
                    printer.Write(" : ");
                    printer.Print(Precedence.Free, entry.Value);
                }
                printer.WriteItem('}');
            }
        }
    }

    /// <summary>
    /// The <see cref="Set{T}"/> of keys that occur in a particular <see cref="Map{TKey, TValue}"/>.
    /// </summary>
    public struct MapKeySet<TKey, TValue>
    {
        internal Map<TKey, TValue>._Bin[] _bins;
        internal MapKeySet(Map<TKey, TValue>._Bin[] bins)
        {
            _bins = bins;
        }

        /// <summary>
        /// The number of members in this set.
        /// </summary>
        public uint Size
        {
            get
            {
                uint size = 0;
                for (int i = 0; i < _bins.Length; i++)
                    if (_bins[i].Hash != 0)
                        size++;
                return size;
            }
        }

        /// <summary>
        /// Sorts the elements in this set using the given <see cref="IComparer{T}"/>.
        /// </summary>
        public List<TKey> Sort(IComparer<TKey> comparer)
        {
            TKey[] items = new TKey[Size];
            uint index = 0;
            for (uint i = 0; i < _bins.Length; i++)
                if (_bins[i].Hash != 0)
                    items[index++] = _bins[i].Key;
            Array.Sort(items, comparer);
            return new List<TKey>(items);
        }

        public static implicit operator Set<TKey>(MapKeySet<TKey, TValue> source)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// A helper class for constructing a <see cref="Map{TKey, TValue}"/> by incrementally adding, updating and removing entries.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    [DebuggerTypeProxy(typeof(EnumerableDebugView<>))]
    public sealed class MapBuilder<TKey, TValue> : IMap<TKey, TValue>,
        IEnumerable<KeyValuePair<TKey, TValue>>,
        IBuilder<Map<TKey, TValue>>
    {
        private Map<TKey, TValue>._Bin[] _bins;
        private uint _capacity;
        public MapBuilder() // TODO: Make private in favor of CreateEmpty
        {
            _bins = Map<TKey, TValue>._emptyEntries;
            _capacity = 0;
        }

        /// <summary>
        /// Creates a new <see cref="MapBuilder{TKey, TValue}"/> whose initial contents are given
        /// by <paramref name="initial"/>.
        /// </summary>
        public static MapBuilder<TKey, TValue> Create(Map<TKey, TValue> initial)
        {
            // Copy bins into a new array
            uint size = 0;
            var bins = new Map<TKey, TValue>._Bin[initial._bins.Length];
            for (uint i = 0; i < (uint)bins.Length; i++)
            {
                Map<TKey, TValue>._Bin bin = initial._bins[i];
                if (bin.Hash != 0)
                {
                    size++;
                    bins[i] = bin;
                }
            }

            // Compute capacity
            uint capacity = bins.Length >= 8 ? (_getCapacity(bins.Length) - size) : 0;

            // Create builder
            return new MapBuilder<TKey, TValue>
            {
                _bins = bins,
                _capacity = capacity
            };
        }

        /// <summary>
        /// Creates a new initially-empty <see cref="MapBuilder{TKey, TValue}"/>.
        /// </summary>
        public static MapBuilder<TKey, TValue> CreateEmpty()
        {
            return new MapBuilder<TKey, TValue>();
        }

        /// <summary>
        /// Gets a reference to the value for the given key, assuming an association for the key exists. The returned
        /// reference is only valid up to the next modification to the <see cref="MapBuilder{TKey, TValue}"/>. Note that
        /// this can not be used to add new keys to the map. 
        /// </summary>
        public ref TValue this[TKey key]
        {
            get
            {
                int hash = Map<TKey, TValue>._hash(key);
                uint binIndex = _bins.GetBinPow2(hash);
            nextBin:
                ref Map<TKey, TValue>._Bin bin = ref _bins[binIndex];
                if (bin.Hash == 0)
                    throw new KeyNotFoundException();
                if (EqualityHelper.Equals(key, bin.Key))
                    return ref bin.Value;
                binIndex = _bins.Next(binIndex);
                goto nextBin;
            }
        }
        
        /// <summary>
        /// The number of entries in this map.
        /// </summary>
        public uint Size
        {
            get
            {
                uint size = 0;
                for (int i = 0; i < _bins.Length; i++)
                    if (_bins[i].Hash != 0)
                        size++;
                return size;
            }
        }

        /// <summary>
        /// Associates the given key with the given value in this map, replacing the current association if it exists.
        /// </summary>
        /// <returns>True if a new association was added. False if an existing association was updated.</returns>
        public bool Add(TKey key, TValue value)
        {
            bool res = Allocate(key, out Entry entry);
            entry.Value = value;
            return res;
        }

        /// <summary>
        /// Adds an association for the given key or returns the existing association. The initial values for associations
        /// added using this method are undefined.
        /// </summary>
        /// <returns><see cref="true"/> if a new association was added. <see cref="false"/> if an existing association
        /// was returned.</returns>
        public bool Allocate(TKey key, out Entry entry)
        {
            // Check for existing entry
            int hash = Map<TKey, TValue>._hash(key);
        retry:
            uint binIndex = _bins.GetBinPow2(hash);
        nextBin:
            ref Map<TKey, TValue>._Bin bin = ref _bins[binIndex];
            if (bin.Hash != 0)
            {
                if (EqualityHelper.Equals(key, bin.Key))
                {
                    entry = new Entry(_bins, binIndex);
                    return false;
                }
                binIndex = _bins.Next(binIndex);
                goto nextBin;
            }
            else
            {
                // Add new entry
                if (_capacity == 0)
                {
                    _expand();
                    goto retry;
                }
                else
                {
                    _capacity--;
                    bin.Hash = hash;
                    bin.Key = key;
                    entry = new Entry(_bins, binIndex);
                    return true;
                }
            }
        }

        /// <summary>
        /// Determines whether there is an entry for the given key in this map, and if so, gets the
        /// corresponding value.
        /// </summary>
        public bool TryGet(TKey key, out TValue value)
        {
            int hash = Map<TKey, TValue>._hash(key);
            uint binIndex = _bins.GetBinPow2(hash);
        nextBin:
            ref Map<TKey, TValue>._Bin bin = ref _bins[binIndex];
            if (bin.Hash != 0)
            {
                if (EqualityHelper.Equals(key, bin.Key))
                {
                    value = bin.Value;
                    return true;
                }
                binIndex = _bins.Next(binIndex);
                goto nextBin;
            }
            else
            {
                value = default;
                return false;
            }
        }

        /// <summary>
        /// Attempts to remove the association for the given key from this map.
        /// </summary>
        /// <returns>True if an association was removed. False if no such association exists.</returns>
        public bool Remove(TKey key)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Indicates whether there is an entry for the given key in this map.
        /// </summary>
        public bool ContainsKey(TKey key)
        {
            int hash = Map<TKey, TValue>._hash(key);
            uint binIndex = _bins.GetBinPow2(hash);
        nextBin:
            ref Map<TKey, TValue>._Bin bin = ref _bins[binIndex];
            if (bin.Hash == 0)
                return false;
            if (EqualityHelper.Equals(key, bin.Key))
                return true;
            binIndex = _bins.Next(binIndex);
            goto nextBin;
        }

        /// <summary>
        /// Enlarges <see cref="_bins"/> in order to accommodate more entries.
        /// </summary>
        private void _expand()
        {
            Map<TKey, TValue>._Bin[] nBins = new Map<TKey, TValue>._Bin[Math.Max(8, _bins.Length * 2)];
            _capacity = _getCapacity(nBins.Length);
            for (uint i = 0; i < (uint)_bins.Length; i++)
            {
                if (_bins[i].Hash != 0)
                {
                    int hash = _bins[i].Hash;
                    uint binIndex = nBins.GetBinPow2(hash);
                    while (nBins[binIndex].Hash != 0)
                        binIndex = nBins.Next(binIndex);
                    nBins[binIndex] = _bins[i];
                    _capacity--;
                }
            }
            _bins = nBins;
        }

        /// <summary>
        /// Gets the capacity of this builder when <see cref="_bins"/> is the given size.
        /// </summary>
        private static uint _getCapacity(int size)
        {
            return 3 + (uint)size * 3 / 5;
        }

        /// <summary>
        /// Identifies an association within a map. This identifier is only valid up to the next modification made
        /// to the map.
        /// </summary>
        public ref struct Entry
        {
            private Map<TKey, TValue>._Bin[] _bins;
            private uint _index;
            internal Entry(Map<TKey, TValue>._Bin[] bins, uint index)
            {
                _bins = bins;
                _index = index;
            }

            /// <summary>
            /// The key for this entry.
            /// </summary>
            public TKey Key => _bins[_index].Key;

            /// <summary>
            /// A reference to the value for this entry.
            /// </summary>
            public ref TValue Value => ref _bins[_index].Value;
        }


        /// <summary>
        /// Gets an enumerator for the entries in this map, in no particular order.
        /// </summary>
        public Map<TKey, TValue>.Enumerator GetEnumerator()
        {
            return new Map<TKey, TValue>.Enumerator(_bins);
        }

        IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator()
        {
            return GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Gets the <see cref="Map{TKey, TValue}"/> resulting from this builder.
        /// </summary>
        public Map<TKey, TValue> Finish()
        {
            return new Map<TKey, TValue>(_bins);
        }

        public static implicit operator Map<TKey, TValue>(MapBuilder<TKey, TValue> source)
        {
            var nBins = new Map<TKey, TValue>._Bin[source._bins.Length];
            for (int i = 0; i < nBins.Length; i++)
                nBins[i] = source._bins[i];
            return new Map<TKey, TValue>(nBins);
        }

        public override string ToString()
        {
            return Finish().ToString();
        }
    }

    /// <summary>
    /// Contains helper functions related to <see cref="Map{TKey, TValue}"/>.
    /// </summary>
    public static class Map
    {
        /// <summary>
        /// The empty <see cref="Map{TKey, TValue}"/>.
        /// </summary>
        public static Map<TKey, TValue> Empty<TKey, TValue>()
        {
            return Map<TKey, TValue>.Empty;
        }
        
        /// <summary>
        /// Constructs a <see cref="Map{TKey, TValue}"/> consisting of the given entries.
        /// </summary>
        public static Map<TKey, TValue> Build<TKey, TValue>(SpanList<(TKey, TValue)> entries)
        {
            MapBuilder<TKey, TValue> builder = new MapBuilder<TKey, TValue>();
            foreach ((TKey key, TValue value) in entries._span)
                if (!builder.Add(key, value))
                    throw new Exception("Duplicate key found");
            return builder.Finish();
        }

        /// <summary>
        /// Constructs a <see cref="Map{TKey, TValue}"/> consisting of the given entries.
        /// </summary>
        public static Map<TKey, TValue> Of<TKey, TValue>(params (TKey, TValue)[] entries)
        {
            return Build(List.Of(entries));
        }
    }

    /// <summary>
    /// A mapping of a set of keys of type <typeparamref name="TKey"/> to values of type <typeparamref name="TValue"/>.
    /// </summary>
    public interface IMap<TKey, TValue>
    {
        /// <summary>
        /// Determines whether there is an entry for the given key in this map, and if so, gets the
        /// corresponding value.
        /// </summary>
        bool TryGet(TKey key, out TValue value);
    }
}