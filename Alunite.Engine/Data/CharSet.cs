﻿using System;
using System.Collections.Generic;

namespace Alunite.Data
{
    /// <summary>
    /// A specialization of <see cref="Set{T}"/> for <see cref="char"/>.
    /// </summary>
    public struct CharSet : ISet<char>
    {
        private bool _default;
        private ulong[] _bitmap;

        /// <summary>
        /// A <see cref="CharSet"/> with no members.
        /// </summary>
        public static CharSet None => new CharSet
        {
            _default = false,
            _bitmap = Array.Empty<ulong>()
        };

        /// <summary>
        /// A <see cref="CharSet"/> with every <see cref="char"/> as a member.
        /// </summary>
        public static CharSet All => new CharSet
        {
            _default = true,
            _bitmap = Array.Empty<ulong>()
        };

        /// <summary>
        /// The <see cref="CharSet"/> consisting of whitespace characters.
        /// </summary>
        public static CharSet Whitespace { get; } = Of(' ', '\t', '\r', '\n');

        /// <summary>
        /// The <see cref="CharSet"/> consisting of alphabetic characters.
        /// </summary>
        public static CharSet Alphabetic { get; } = Range('a', 'z') | Range('A', 'Z');

        /// <summary>
        /// The <see cref="CharSet"/> consisting of numeric characters.
        /// </summary>
        public static CharSet Numeric { get; } = Range('0', '9');

        /// <summary>
        /// Constructs a <see cref="CharSet"/> containing only <see cref="char"/>s for which the given predicate is true.
        /// </summary>
        public static CharSet Where(Func<char, bool> pred)
        {
            uint wordBound = 0;
            for (uint i = 0; i < 65536; i++)
            {
                char ch = (char)i;
                if (pred(ch))
                {
                    uint word = (uint)ch / 64;
                    if (wordBound <= word)
                        wordBound = word + 1;
                }
            }
            ulong[] bitmap = new ulong[wordBound];
            for (uint i = 0; i < 65536; i++)
            {
                char ch = (char)i;
                if (pred(ch))
                {
                    uint word = (uint)ch / 64;
                    uint offset = ch - word * 64;
                    ulong cur = bitmap[word];
                    cur |= 1ul << (int)offset;
                    bitmap[word] = cur;
                }
            }
            return new CharSet
            {
                _default = false,
                _bitmap = bitmap
            };
        }

        /// <summary>
        /// Constructs a <see cref="CharSet"/> consisting from the given inclusive range of <see cref="char"/>s.
        /// </summary>
        public static CharSet Range(char from, char to)
        {
            if (from == char.MinValue && to == char.MaxValue)
                return All;
            uint wordBound = (uint)to / 64 + 1;
            ulong[] bitmap = new ulong[wordBound];
            // TODO: Could be a bit faster using bit twiddling
            for (char ch = from; ch <= to; ch++)
            {
                uint word = (uint)ch / 64;
                uint offset = ch - word * 64;
                ulong cur = bitmap[word];
                cur |= 1ul << (int)offset;
                bitmap[word] = cur;
            }
            return new CharSet
            {
                _default = false,
                _bitmap = bitmap
            };
        }

        /// <summary>
        /// Constructs a <see cref="CharSet"/> containing only the given <see cref="char"/>s.
        /// </summary>
        public static CharSet Of(params char[] chs)
        {
            return Of((IEnumerable<char>)chs);
        }

        /// <summary>
        /// Constructs a <see cref="CharSet"/> containing only the given <see cref="char"/>s.
        /// </summary>
        public static CharSet Of(IEnumerable<char> chs)
        {
            uint wordBound = 0;
            foreach (char ch in chs)
            {
                uint word = (uint)ch / 64;
                if (wordBound <= word)
                    wordBound = word + 1;
            }
            ulong[] bitmap = new ulong[wordBound];
            foreach (char ch in chs)
            {
                uint word = (uint)ch / 64;
                uint offset = ch - word * 64;
                ulong cur = bitmap[word];
                cur |= 1ul << (int)offset;
                bitmap[word] = cur;
            }
            return new CharSet
            {
                _default = false,
                _bitmap = bitmap
            };
        }
        
        /// <summary>
        /// Indicates whether the given <see cref="char"/> is included in this set.
        /// </summary>
        public bool Contains(char ch)
        {
            uint index = ch;
            uint word = index / 64;
            if (word < _bitmap.Length)
            {
                uint offset = index - word * 64;
                return (_bitmap[word] & (1ul << (int)offset)) != 0ul;
            }
            else
            {
                return _default;
            }
        }

        public static CharSet operator |(CharSet a, CharSet b)
        {
            bool @default;
            ulong[] bitmap;
            if (a._bitmap.Length < b._bitmap.Length)
            {
                if (a._default)
                {
                    bitmap = new ulong[a._bitmap.Length];
                    @default = true;
                }
                else
                {
                    bitmap = new ulong[b._bitmap.Length];
                    @default = b._default;
                    for (int i = a._bitmap.Length; i < b._bitmap.Length; i++)
                        bitmap[i] = b._bitmap[i];
                }
                for (int i = 0; i < a._bitmap.Length; i++)
                    bitmap[i] = a._bitmap[i] | b._bitmap[i];
            }
            else
            {
                if (b._default)
                {
                    bitmap = new ulong[b._bitmap.Length];
                    @default = true;
                }
                else
                {
                    bitmap = new ulong[a._bitmap.Length];
                    @default = a._default;
                    for (int i = b._bitmap.Length; i < a._bitmap.Length; i++)
                        bitmap[i] = a._bitmap[i];
                }
                for (int i = 0; i < b._bitmap.Length; i++)
                    bitmap[i] = a._bitmap[i] | b._bitmap[i];
            }
            return new CharSet
            {
                _default = @default,
                _bitmap = bitmap
            };
        }
    }
}
