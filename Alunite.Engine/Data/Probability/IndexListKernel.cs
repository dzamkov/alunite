﻿using System;
using System.Diagnostics;

namespace Alunite.Data.Probability
{
    /// <summary>
    /// An <see cref="IKernel{T}"/> whose support is a set of fixed-length lists of small <see cref="uint"/>s.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    [DebuggerTypeProxy(typeof(IndexListKernelDebugView))]
    public struct RegularIndexListKernel : IKernel<List<uint>>
    {
        internal LogScalar _weight;
        internal uint _len;
        internal RegularIndexListDistribution._Internal _source;
        internal RegularIndexListKernel(LogScalar weight, uint len, RegularIndexListDistribution._Internal source)
        {
            _weight = weight;
            _len = len;
            _source = source;
        }

        internal RegularIndexListKernel(LogScalar weight, RegularIndexListDistribution dist)
        {
            Debug.Assert(!weight.IsZero);
            _weight = weight;
            _len = dist.Length;
            _source = dist._source;
        }

        internal RegularIndexListKernel(uint len, _Internal source)
        {
            _weight = source.Weight;
            _len = len;
            _source = source.Distribution;
        }

        /// <summary>
        /// The total weight of this kernel.
        /// </summary>
        public LogScalar Weight => _weight;

        /// <summary>
        /// The length of the lists within this kernel.
        /// </summary>
        public uint Length => _len;

        /// <summary>
        /// The normalized distribution for this kernel, assuming <see cref="Weight"/> is non-zero.
        /// </summary>
        public RegularIndexListDistribution Distribution => new RegularIndexListDistribution(_len, _source);
        
        /// <summary>
        /// The <see cref="RegularIndexListKernel"/> whose weight is zero for all values.
        /// </summary>
        public static RegularIndexListKernel Zero => new RegularIndexListKernel(
            LogScalar.Zero, uint.MaxValue,
            RegularIndexListDistribution._Internal.PointEmpty);

        /// <summary>
        /// A list which contains one more than the maximum value for each item in this kernel. This is
        /// not applicable for the zero kernel.
        /// </summary>
        public List<uint> Bounds
        {
            get
            {
                if (Weight.IsZero)
                    throw new InvalidOperationException();
                return Distribution.Bounds;
            }
        }

        /// <summary>
        /// Constructs an <see cref="RegularIndexListKernel"/> where each item is selected based on an
        /// <see cref="IndexKernel"/>, independent of all other items.
        /// </summary>
        public static RegularIndexListKernel Independent(params IndexKernel[] items)
        {
            return Independent(List.Of(items));
        }
        
        /// <summary>
        /// Constructs an <see cref="RegularIndexListKernel"/> where each item is selected based on an
        /// <see cref="IndexKernel"/>, independent of all other items.
        /// </summary>
        public static RegularIndexListKernel Independent(SpanList<IndexKernel> items)
        {
            uint len = (uint)items.Length;
            uint rem = len;
            LogScalar weight = LogScalar.One;
            RegularIndexListDistribution._Internal tail = RegularIndexListDistribution._Internal.PointEmpty;
            while (rem > 0)
            {
                rem--;
                IndexKernel kernel = items[rem];
                if (!kernel.TryNormalize(out LogScalar kernelWeight, out IndexDistribution dist))
                    return Zero;
                weight *= kernelWeight;
                var cases = new RegularIndexListDistribution._Case[dist.Bound];
                for (uint i = 0; i < cases.Length; i++)
                    cases[i] = RegularIndexListDistribution._Case.Build(dist[i], tail);
                tail = new RegularIndexListDistribution._Internal(cases);
            }
            return new RegularIndexListKernel(weight, len, tail);
        }

        /// <summary>
        /// Constructs an <see cref="RegularIndexListKernel"/> which has a value of one for all lists where
        /// every individual item is below the corresponding bound in the given list.
        /// </summary>
        public static RegularIndexListKernel IndependentIndicatorBelow(params uint[] bounds)
        {
            return IndependentIndicatorBelow((Span<uint>)bounds);
        }

        /// <summary>
        /// Constructs an <see cref="RegularIndexListKernel"/> which has a value of one for all lists where
        /// every individual item is below the corresponding bound in the given list.
        /// </summary>
        public static RegularIndexListKernel IndependentIndicatorBelow(ReadOnlySpan<uint> bounds)
        {
            uint len = (uint)bounds.Length;
            uint rem = len;
            LogScalar weight = LogScalar.One;
            RegularIndexListDistribution._Internal tail = RegularIndexListDistribution._Internal.PointEmpty;
            while (rem > 0)
            {
                rem--;
                uint bound = bounds[(int)rem];
                IndexDistribution dist = IndexDistribution.UniformBelow(bound);
                weight *= (LogScalar)bound;
                var cases = new RegularIndexListDistribution._Case[dist.Bound];
                for (uint i = 0; i < cases.Length; i++)
                    cases[i] = RegularIndexListDistribution._Case.Build(dist[i], tail);
                tail = new RegularIndexListDistribution._Internal(cases);
            }
            return new RegularIndexListKernel(weight, len, tail);
        }

        /// <summary>
        /// Constructs a <see cref="RegularIndexListKernel"/> which has a value of one for all lists in
        /// the given support set.
        /// </summary>
        public static RegularIndexListKernel Indicator(RegularIndexListSet support)
        {
            if (support.IsNone)
                return Zero;
            if (support.IsSingletonEmpty)
                return new RegularIndexListKernel(LogScalar.One, RegularIndexListDistribution.PointEmpty);
            var cache = new MapBuilder<RegularIndexListSet._Internal, _Internal>();
            return new RegularIndexListKernel(support.Length, _Internal._indicator(cache, support._source));
        }

        /// <summary>
        /// Constructs an <see cref="RegularIndexListKernel"/> as the concatenation of the given lists.
        /// </summary>
        public static RegularIndexListKernel Concat(params RegularIndexListKernel[] lists)
        {
            return Concat((Span<RegularIndexListKernel>)lists);
        }

        /// <summary>
        /// Constructs an <see cref="RegularIndexListKernel"/> as the concatenation of the given lists.
        /// </summary>
        public static RegularIndexListKernel Concat(ReadOnlySpan<RegularIndexListKernel> lists)
        {
            LogScalar weight = LogScalar.One;
            RegularIndexListDistribution[] dists = new RegularIndexListDistribution[lists.Length];
            for (int i = 0; i < lists.Length; i++)
            {
                RegularIndexListKernel list = lists[i];
                weight *= list.Weight;
                dists[i] = list.Distribution;
            }
            if (weight.IsZero)
                return Zero;
            return new RegularIndexListKernel(weight, RegularIndexListDistribution.Concat(dists));
        }

        /// <summary>
        /// Constructs an <see cref="RegularIndexListKernel"/> for a list whose first item is an
        /// index in <paramref name="cases"/> and whose remaining items are taken from the kernel at
        /// that index.
        /// </summary>
        public static RegularIndexListKernel Switch(ReadOnlySpan<RegularIndexListKernel> cases)
        {
            uint last = (uint)cases.Length;
            while (last > 0)
            {
                if (!cases[(int)--last].Weight.IsZero)
                    goto foundLast;
            }
            return Zero;

        foundLast:
            uint numCases = last + 1;
            uint len = cases[(int)last].Length;
            Scalar maxCaseLn = -Scalar.Inf;
            RegularIndexListDistribution._Case[] nCases = new RegularIndexListDistribution._Case[numCases];
            for (int i = 0; i < nCases.Length; i++)
            {
                RegularIndexListKernel @case = cases[i];
                if (@case.Weight.IsZero)
                {
                    nCases[i] = RegularIndexListDistribution._Case.Zero;
                }
                else
                {
                    if (@case.Length != len)
                        throw new ArgumentException("All cases must have the same length", nameof(cases));
                    Scalar caseLn = @case.Weight.Ln;
                    if (caseLn > maxCaseLn) maxCaseLn = caseLn;
                    nCases[i] = RegularIndexListDistribution._Case.Build(new LogScalar(caseLn), @case._source);
                }
            }

            // Normalize probabilities
            Debug.Assert(maxCaseLn != -Scalar.Inf);
            Scalar addExp = 0;
            for (int i = 0; i < nCases.Length; i++)
                addExp += Scalar.Exp(nCases[i].Probability.Ln - maxCaseLn);
            Scalar norm = maxCaseLn + Scalar.Ln(addExp);
            for (int i = 0; i < nCases.Length; i++)
                nCases[i].Probability = new LogScalar(nCases[i].Probability.Ln - norm);

            // Construct new distribution
            return new RegularIndexListKernel(
                new LogScalar(norm), len, 
                new RegularIndexListDistribution._Internal(nCases));
        }

        /// <summary>
        /// Determines whether this is a kernel of non-empty lists, and if so, deconstructs it as a 
        /// <see cref="Switch"/>.
        /// </summary>
        public bool IsSwitch(out List<RegularIndexListKernel> cases)
        {
            if (Weight.IsZero)
            {
                cases = List.Empty<RegularIndexListKernel>();
                return true;
            }
            else if (Length > 0)
            {
                uint nLen = Length - 1;
                RegularIndexListDistribution._Case[] sourceCases = Distribution._source._cases;
                RegularIndexListKernel[] items = new RegularIndexListKernel[sourceCases.Length];
                for (int i = 0; i < sourceCases.Length; i++)
                {
                    RegularIndexListDistribution._Case sourceCase = sourceCases[i];
                    if (sourceCase.Probability.IsZero)
                        items[i] = Zero;
                    else
                        items[i] = new RegularIndexListKernel(sourceCase.Probability * Weight, nLen, sourceCase.Tail);
                }
                cases = new List<RegularIndexListKernel>(items);
                return true;
            }
            cases = default;
            return false;
        }

        /// <summary>
        /// Provides a measure of how complex this kernel is. This is minimal for <see cref="Independent"/>
        /// kernels and increases depending on how interdependent the kernel is.
        /// </summary>
        public uint Complexity
        {
            get
            {
                if (Weight.IsZero)
                    return 0;
                return Distribution.Complexity;
            }
        }

        /// <summary>
        /// Gets the kernel for some contiguous subsequence of this list.
        /// </summary>
        public RegularIndexListKernel Slice(uint start, uint len)
        {
            if (Weight.IsZero)
                return Zero;
            return new RegularIndexListKernel(Weight, Distribution.Slice(start, len));
        }

        /// <summary>
        /// Gets the kernel for a subsequence of this list.
        /// </summary>
        public RegularIndexListKernel Subseq(IndexSet indices)
        {
            if (Weight.IsZero)
                return Zero;
            return new RegularIndexListKernel(Weight, Distribution.Subseq(indices));
        }

        /// <summary>
        /// Gets the kernel for a list constructed by selecting items from this list.
        /// </summary>
        public RegularIndexListKernel Select(List<uint> indices)
        {
            if (Weight.IsZero)
                return Zero;
            return new RegularIndexListKernel(Weight, Distribution.Select(indices));
        }

        /// <summary>
        /// Filters this kernel to only include lists where the given subsequence of items are in <paramref name="other"/>.
        /// </summary>
        public RegularIndexListKernel IntersectSubseq(IndexSet indices, RegularIndexListSet other)
        {
            if (other.IsNone)
                return Zero;
            if (!(indices.Size == other.Length))
                throw new ArgumentException("List length mismatch", nameof(other));
            if (Weight.IsZero)
                return Zero;
            if (!(indices.Bound <= Length))
                throw new ArgumentException("Indices out of range", nameof(indices));
            int i = 0;
            var cache = new MapBuilder<(RegularIndexListDistribution._Internal, RegularIndexListSet._Internal), _Internal>();
            Span<uint> indicesSpan = stackalloc uint[(int)indices.Size];
            foreach (uint index in indices)
                indicesSpan[i++] = index;
            _Internal res = _source._intersectSubseq(cache, indicesSpan, other._source, 0);
            if (res.Weight.IsZero)
                return Zero;
            return Weight * new RegularIndexListKernel(Length, res);
        }

        /// <summary>
        /// Filters this kernel to only include lists where the given subsequence of items satisfies the given predicate.
        /// </summary>
        public RegularIndexListKernel FilterSubseq(IndexSet indices, Predicate<List<uint>> pred)
        {
            if (Weight.IsZero)
                return Zero;
            if (!(indices.Bound <= Length))
                throw new ArgumentException("Indices out of range", nameof(indices));
            int i = 0;
            Span<uint> indicesSpan = stackalloc uint[(int)indices.Size];
            Span<uint> values = stackalloc uint[indicesSpan.Length];
            foreach (uint index in indices)
                indicesSpan[i++] = index;
            _Internal res = _source._filterSubseq(indicesSpan, values, 0, pred, 0);
            if (res.Weight.IsZero)
                return Zero;
            return Weight * new RegularIndexListKernel(Length, res);
        }

        /// <summary>
        /// Multiplies the weights in this kernel based on the value of a given item.
        /// </summary>
        /// <param name="other">The kernel which provides the factor to multiply by based on the
        /// selected item.</param>
        public RegularIndexListKernel ConflateItem(uint index, IndexKernel other)
        {
            return ConflateSubseq(IndexSet.Of(index), Independent(other));
        }

        /// <summary>
        /// Multiplies the weights in this kernel based on a given subsequence of items.
        /// </summary>
        /// <param name="other">The kernel which provides the factor to multiply by based on the
        /// selected subsequence.</param>
        public RegularIndexListKernel ConflateSubseq(IndexSet indices, RegularIndexListKernel other)
        {
            if (other.Weight.IsZero)
                return Zero;
            if (!(indices.Size == other.Length))
                throw new ArgumentException("List length mismatch", nameof(other));
            if (Weight.IsZero)
                return Zero;
            if (!(indices.Bound <= Length))
                throw new ArgumentException("Indices out of range", nameof(indices));
            int i = 0;
            var cache = new MapBuilder<(RegularIndexListDistribution._Internal, RegularIndexListDistribution._Internal), _Internal>();
            Span<uint> indicesSpan = stackalloc uint[(int)indices.Size];
            foreach (uint index in indices)
                indicesSpan[i++] = index;
            _Internal res = _source._conflateSubseq(cache, indicesSpan, other._source, 0);
            if (res.Weight.IsZero)
                return Zero;
            return (Weight * other.Weight) * new RegularIndexListKernel(Length, res);
        }

        /// <summary>
        /// Evaluates this kernel for the given value.
        /// </summary>
        public LogScalar this[List<uint> value]
        {
            get
            {
                if (Weight.IsZero)
                    return LogScalar.Zero;
                return Weight * Distribution[value];
            }
        }

        /// <summary>
        /// The set of all lists which have a non-zero weight under this kernel.
        /// </summary>
        public RegularIndexListSet Support
        {
            get
            {
                if (Weight.IsZero)
                    return RegularIndexListSet.None;
                return Distribution.Support;
            }
        }

        /// <summary>
        /// Reinterprets this kernel as the concatenation of prefix of a certain length and a suffix chosen based on
        /// the prefix.
        /// </summary>
        public void Split(uint prefixLen,
            out RegularIndexListKernel prefix,
            out RegularIndexListMap<RegularIndexListDistribution> suffix)
        {
            if (Weight.IsZero)
            {
                prefix = Zero;
                suffix = RegularIndexListMap.Empty<RegularIndexListDistribution>();
            }
            if (!(prefixLen < Length))
                throw new ArgumentException("Invalid prefix length", nameof(prefixLen));
            if (prefixLen == 0)
            {
                prefix = new RegularIndexListKernel(Weight, RegularIndexListDistribution.PointEmpty);
                suffix = RegularIndexListMap.SingletonEmpty(Distribution);
                return;
            }

            // Construct prefix
            var prefixCache = new MapBuilder<
                RegularIndexListDistribution._Internal,
                RegularIndexListDistribution._Internal>();
            prefix = new RegularIndexListKernel(Weight,
                new RegularIndexListDistribution(
                    prefixLen, _source._splitPrefix(prefixLen, prefixCache)));

            // Construct suffix
            var suffixCache = new MapBuilder<
                RegularIndexListDistribution._Internal, 
                RegularIndexListMap<RegularIndexListDistribution>._Internal>();
            suffix = new RegularIndexListMap<RegularIndexListDistribution>(prefixLen,
                _source._splitSuffix(prefixLen, Length - prefixLen, suffixCache));
        }

        /// <summary>
        /// Calculates the "distance" between two <see cref="RegularIndexListKernel"/>s. This will be <see cref="Scalar.Inf"/>
        /// if the distributions do not have the same support.
        /// </summary>
        public static Scalar Distance(RegularIndexListKernel a, RegularIndexListKernel b)
        {
            return Scalar.Sqrt(SqrDistance(a, b));
        }

        /// <summary>
        /// Calculates the square of <see cref="Distance"/>. This is equal to the sum of the square differences of the
        /// log-weights for every list in their support. This will be <see cref="Scalar.Inf"/> if the kernels
        /// do not have the same support.
        /// </summary>
        public static Scalar SqrDistance(RegularIndexListKernel a, RegularIndexListKernel b)
        {
            if (a.Length != b.Length)
                return Scalar.Inf;
            if (a.Weight.IsZero)
            {
                if (b.Weight.IsZero)
                    return Scalar.Zero;
                return Scalar.Inf;
            }
            if (b.Weight.IsZero)
                return Scalar.Inf;
            var cache = new MapBuilder<(RegularIndexListDistribution._Internal, RegularIndexListDistribution._Internal), ScalarPoly2>();
            return RegularIndexListDistribution._Internal._getSqrDistance(cache, a._source, b._source)[a.Weight.Ln - b.Weight.Ln];
        }
        
        /// <summary>
        /// The internal recursive definition of <see cref="RegularIndexListKernel"/>, which doesn't store the length
        /// at each node.
        /// </summary>
        internal struct _Internal : IEquatable<_Internal>
        {
            internal _Internal(LogScalar weight, RegularIndexListDistribution._Internal dist)
            {
                Weight = weight;
                Distribution = dist;
            }

            /// <summary>
            /// The total weight of this kernel.
            /// </summary>
            public LogScalar Weight;

            /// <summary>
            /// The distribution for this kernel.
            /// </summary>
            public RegularIndexListDistribution._Internal Distribution;

            /// <summary>
            /// The kernel whose weight is zero for all values.
            /// </summary>
            public static _Internal Zero => new _Internal(LogScalar.Zero,
                RegularIndexListDistribution._Internal.PointEmpty);


            /// <summary>
            /// Constructs an <see cref="_Internal"/> based on a subset
            /// of a <see cref="FiniteKernel{T}"/>.
            /// </summary>
            /// <param name="indices">The indices to be considered.</param>
            /// <param name="offset">The first item to be included in the resulting set.</param>
            internal static _Internal _fromKernel(
                ReadOnlySpan<PointKernel<List<uint>>> entries,
                ReadOnlySpan<uint> indices,
                uint offset)
            {
                // Check for special cases
                if (indices.Length == 0)
                    return Zero;
                PointKernel<List<uint>> firstEntry = entries[(int)indices[0]];
                uint len = firstEntry.Value.Length;
                if (offset >= len)
                {
                    Debug.Assert(indices.Length == 1);
                    return new _Internal(firstEntry.Weight, RegularIndexListDistribution._Internal.PointEmpty);
                }

                // Get bound for this item
                uint bound = 0;
                for (int i = 0; i < indices.Length; i++)
                {
                    uint item = entries[(int)indices[i]].Value[offset];
                    if (bound < item)
                        bound = item;
                }
                bound++;

                // Get number of lists per value
                Span<uint> numLists = stackalloc uint[(int)bound];
                for (int i = 0; i < indices.Length; i++)
                {
                    uint item = entries[(int)indices[i]].Value[offset];
                    numLists[(int)item]++;
                }

                // Allocate space for indices recursively
                uint head = 0;
                Span<uint> heads = numLists;
                for (uint i = 0; i < bound; i++)
                {
                    uint nHead = head + numLists[(int)i];
                    heads[(int)i] = head;
                    head = nHead;
                }
                Debug.Assert(head == (uint)indices.Length);
                Span<uint> nIndices = stackalloc uint[indices.Length];

                // Group indices based on item
                for (int i = 0; i < indices.Length; i++)
                {
                    uint index = indices[i];
                    uint item = entries[(int)indices[i]].Value[offset];
                    nIndices[(int)heads[(int)item]++] = index;
                }

                // Construct cases recursively
                uint start = 0;
                uint nOffset = offset + 1;
                Scalar maxCaseLn = -Scalar.Inf;
                var cases = new RegularIndexListDistribution._Case[bound];
                for (uint i = 0; i < bound; i++)
                {
                    uint end = heads[(int)i];
                    var nKernel = _fromKernel(entries, nIndices.Slice((int)start, (int)(end - start)), nOffset);
                    Scalar caseLn = nKernel.Weight.Ln;
                    if (caseLn > maxCaseLn) maxCaseLn = caseLn;
                    cases[i] = RegularIndexListDistribution._Case.Build(new LogScalar(caseLn), nKernel.Distribution);
                    start = end;
                }

                // Normalize probabilities
                Scalar addExp = 0;
                for (int i = 0; i < cases.Length; i++)
                    addExp += Scalar.Exp(cases[i].Probability.Ln - maxCaseLn);
                Scalar norm = maxCaseLn + Scalar.Ln(addExp);
                for (int i = 0; i < cases.Length; i++)
                    cases[i].Probability = new LogScalar(cases[i].Probability.Ln - norm);
                return new LogScalar(norm) * new RegularIndexListDistribution._Internal(cases);
            }

            /// <summary>
            /// Constructs a kernel which has a value of one for all lists in the given support set.
            /// </summary>
            internal static _Internal _indicator(
                MapBuilder<RegularIndexListSet._Internal, _Internal> cache,
                RegularIndexListSet._Internal support)
            {
                if (support.IsNone)
                    return Zero;
                if (cache.TryGet(support, out _Internal res))
                    return res;
                if (support._table is RegularIndexListSet._Case[] cases)
                {
                    // Construct cases recursively
                    Scalar maxCaseLn = -Scalar.Inf;
                    var nCases = new RegularIndexListDistribution._Case[cases.Length];
                    for (int i = 0; i < nCases.Length; i++)
                    {
                        _Internal nKernel = _indicator(cache, cases[i].Tail);
                        Scalar caseLn = nKernel.Weight.Ln;
                        if (caseLn > maxCaseLn) maxCaseLn = caseLn;
                        nCases[i] = RegularIndexListDistribution._Case.Build(new LogScalar(caseLn), nKernel.Distribution);
                    }

                    // Normalize probabilities
                    Scalar addExp = 0;
                    for (int i = 0; i < nCases.Length; i++)
                        addExp += Scalar.Exp(nCases[i].Probability.Ln - maxCaseLn);
                    Scalar norm = maxCaseLn + Scalar.Ln(addExp);
                    for (int i = 0; i < nCases.Length; i++)
                        nCases[i].Probability = new LogScalar(nCases[i].Probability.Ln - norm);

                    // Construct new kernel
                    res = new LogScalar(norm) * new RegularIndexListDistribution._Internal(nCases);
                }
                else
                {
                    // Construct length-one list kernel from bitmap
                    ulong[] bitmap = (ulong[])support._table;
                    IndexSet indices = new IndexSet(bitmap);
                    LogScalar weight = indices.Size;
                    LogScalar prob = weight.Reciprocal;
                    var nCases = new RegularIndexListDistribution._Case[indices.Bound];
                    var @case = RegularIndexListDistribution._Case.Build(prob, RegularIndexListDistribution._Internal.PointEmpty);
                    for (uint i = 0; i < (uint)nCases.Length; i++)
                    {
                        if (indices.Contains(i))
                            nCases[i] = @case;
                        else
                            nCases[i] = RegularIndexListDistribution._Case.Zero;
                    }
                    res = weight * new RegularIndexListDistribution._Internal(nCases);
                }
                cache.Add(support, res);
                return res;
            }

            public static _Internal operator *(_Internal a, LogScalar b)
            {
                return b * a;
            }

            public static _Internal operator *(LogScalar a, _Internal b)
            {
                if (a.IsZero)
                    return Zero;
                return new _Internal(a * b.Weight, b.Distribution);
            }

            public static bool operator ==(_Internal a, _Internal b)
            {
                return a.Weight == b.Weight && a.Distribution == b.Distribution;
            }

            public static bool operator !=(_Internal a, _Internal b)
            {
                return !(a == b);
            }

            public override bool Equals(object obj)
            {
                if (!(obj is _Internal))
                    return false;
                return this == (_Internal)obj;
            }

            bool IEquatable<_Internal>.Equals(_Internal other)
            {
                return this == other;
            }

            public override int GetHashCode()
            {
                return HashCodeHelper.Combine(Weight.GetHashCode(), Distribution.GetHashCode());
            }
        }

        public static implicit operator RegularIndexListKernel(RegularIndexListDistribution dist)
        {
            return new RegularIndexListKernel(LogScalar.One, dist);
        }

        public static explicit operator RegularIndexListDistribution(RegularIndexListKernel kernel)
        {
            Debug.Assert(Scalar.Abs((Scalar)kernel.Weight - 1) < 0.0001);
            return kernel.Distribution;
        }

        public static RegularIndexListKernel operator *(RegularIndexListKernel a, RegularIndexListKernel b)
        {
            throw new NotImplementedException();
        }

        public static RegularIndexListKernel operator *(RegularIndexListKernel a, LogScalar b)
        {
            return b * a;
        }

        public static RegularIndexListKernel operator *(LogScalar a, RegularIndexListKernel b)
        {
            return new RegularIndexListKernel(a * b.Weight, b.Distribution);
        }

        public static explicit operator RegularIndexListKernel(FiniteKernel<List<uint>> source)
        {
            // Search for first item to establish the length of the kernel
            uint len;
            uint index = 0;
            while (index < source._entries.Length)
            {
                var entry = source._entries[index++];
                if (!entry.Weight.IsZero)
                {
                    len = entry.Value.Length;
                    goto foundOne;
                }
            }
            return Zero;

        foundOne:
            // Determine number of lists in the set and verify all the lists have the same length
            uint numLists = 1;
            while (index < source._entries.Length)
            {
                var entry = source._entries[index++];
                if (!entry.Weight.IsZero)
                {
                    if (entry.Value.Length != len)
                        throw new InvalidCastException("All lists must have the same length");
                    numLists++;
                }
            }

            // Construct an index table pointing to all the lists
            Span<uint> indices = stackalloc uint[(int)numLists];
            int head = 0;
            for (uint i = 0; i < source._entries.Length; i++)
                if (!source._entries[i].Weight.IsZero)
                    indices[head++] = i;

            // Build set
            return new RegularIndexListKernel(len, _Internal._fromKernel(source._entries, indices, 0));
        }

        public override string ToString()
        {
            if (Weight.IsZero)
                return "∅";
            RegularIndexListSet support = Support;
            if (support.IsSingleton(out List<uint> value))
            {
                if (Weight.IsOne)
                    return value.ToString();
                return value.ToString() + " : " + Weight.ToString();
            }
            else
            {
                uint size = support.Size;
                if (size == uint.MaxValue)
                    return "{[... <" + Length.ToStringInvariant() + ">], ...} : " + Weight.ToString();
                else
                    return "{[... <" + Length.ToStringInvariant() + ">], ... <" + size.ToStringInvariant() + ">} : " + Weight.ToString();
            }
        }
    }

    /// <summary>
    /// A debugger type proxy for <see cref="RegularIndexListKernel"/>s.
    /// </summary>
    public sealed class IndexListKernelDebugView
    {
        private readonly RegularIndexListKernel _source;
        public IndexListKernelDebugView(RegularIndexListKernel source)
        {
            _source = source;
        }

        public IndexListKernelDebugView(RegularIndexListDistribution source)
        {
            _source = source;
        }

        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public RegularIndexListKernel[] Cases
        {
            get
            {
                if (_source._source._cases is null)
                {
                    return null;
                }
                else
                {
                    var cases = _source._source._cases;
                    var nCases = new RegularIndexListKernel[cases.Length];
                    uint nLength = _source.Length - 1;
                    for (int i = 0; i < nCases.Length; i++)
                    {
                        var @case = cases[i];
                        nCases[i] = new RegularIndexListKernel(@case.Probability, nLength, @case.Tail);
                    }
                    return nCases;
                }
            }
        }
    }
}
