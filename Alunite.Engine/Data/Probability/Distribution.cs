﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace Alunite.Data.Probability
{
    /// <summary>
    /// A probability distribution over values of type <typeparamref name="T"/> which allows for random sampling.
    /// </summary>
    public interface IDistribution<T>
    {
        /// <summary>
        /// Samples from this distribution.
        /// </summary>
        T Sample(ISampler sampler);
    }

    /// <summary>
    /// A function which assigns values of type <typeparamref name="T"/> to non-negative scalars.
    /// </summary>
    public interface IKernel<T>
    {
        /// <summary>
        /// Evaluates this kernel for the given value.
        /// </summary>
        LogScalar this[T value] { get; }
    }

    /// <summary>
    /// A <see cref="IKernel{T}"/> whose support includes at most one value.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct PointKernel<T> : IKernel<T>
    {
        public PointKernel(T value, LogScalar weight)
        {
            Weight = weight;
            Value = value;
        }
        
        /// <summary>
        /// The only value in the kernel that may have a non-zero weight.
        /// </summary>
        public T Value;

        /// <summary>
        /// The weight associated to <see cref="Value"/>.
        /// </summary>
        public LogScalar Weight;

        /// <summary>
        /// A kernel that is zero everywhere.
        /// </summary>
        public static PointKernel<T> Zero => new PointKernel<T>(default, LogScalar.Zero);

        /// <summary>
        /// Evaluates this kernel for the given value.
        /// </summary>
        public LogScalar this[T value]
        {
            get
            {
                if (EqualityHelper.Equals(value, Value))
                    return Weight;
                return LogScalar.Zero;
            }
        }

        public static PointKernel<T> operator *(LogScalar a, PointKernel<T> b)
        {
            return new PointKernel<T>(b.Value, a * b.Weight);
        }

        public static PointKernel<T> operator *(PointKernel<T> a, LogScalar b)
        {
            return b * a;
        }

        public override string ToString()
        {
            if (Weight.IsZero)
                return "∅";
            if (Weight.IsOne)
                return Value.ToString();
            return Value.ToString() + " : " + Weight.ToString();
        }
    }

    /// <summary>
    /// Contains functions related to probability distributions.
    /// </summary>
    public static class Distribution
    {
        /// <summary>
        /// Constructs a <see cref="FiniteDistribution{T}"/> that is uniformly distributed among the given options.
        /// </summary>
        public static FiniteDistribution<T> Uniform<T>(params T[] options)
        {
            LogScalar weight = ((LogScalar)options.Length).Reciprocal;
            FiniteKernelBuilder<T> builder = new FiniteKernelBuilder<T>();
            foreach (var option in options)
                builder.Add(option, weight);
            return (FiniteDistribution<T>)builder.Finish();
        }
        
        /// <summary>
        /// Samples a <see cref="Scalar"/> uniformly distributed from 0 to 1.
        /// </summary>
        public static Scalar SampleUnit(this ISampler sampler)
        {
            uint value = sampler.SampleUInt32();
            return (Scalar)value / uint.MaxValue;
        }

        /// <summary>
        /// Samples from the given distribution.
        /// </summary>
        public static T Sample<T>(this ISampler sampler, IDistribution<T> dist)
        {
            return dist.Sample(sampler);
        }
    }
}
