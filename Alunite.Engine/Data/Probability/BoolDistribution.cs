﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace Alunite.Data.Probability
{
    /// <summary>
    /// A probability distribution for boolean values (i.e. Bernoulli distribution).
    /// </summary>
    /// <remarks>This type is optimized for numerical stability even when the probability of <see cref="true"/> is
    /// very high or very low.</remarks>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct BoolDistribution : IDistribution<bool>, IEquatable<BoolDistribution>
    {
        internal float _lnProb;
        internal BoolDistribution(float lnProb)
        {
            _lnProb = lnProb;
        }


        /// <summary>
        /// A <see cref="UnlikelyBoolDistribution"/> where the probability of <see cref="false"/> is one.
        /// </summary>
        public static BoolDistribution AlwaysFalse => new BoolDistribution(-float.NegativeInfinity);

        /// <summary>
        /// A <see cref="UnlikelyBoolDistribution"/> where the probability of <see cref="AlwaysTrue"/> is one.
        /// </summary>
        public static BoolDistribution AlwaysTrue => new BoolDistribution(float.NegativeInfinity);

        /// <summary>
        /// Constructs a <see cref="BoolDistribution"/> with the given probability of being <see cref="true"/>.
        /// </summary>
        public static BoolDistribution FromTrue(LogScalar prob)
        {
            if (prob._ln <= -_ln2)
                return new BoolDistribution(prob._ln);
            else
                return new BoolDistribution(-LogScalar._softminus(prob._ln));
        }

        /// <summary>
        /// Constructs a <see cref="BoolDistribution"/> with the given probability of being <see cref="false"/>.
        /// </summary>
        public static BoolDistribution FromFalse(LogScalar prob)
        {
            if (prob._ln < -_ln2)
                return new BoolDistribution(-prob._ln);
            else
                return new BoolDistribution(LogScalar._softminus(prob._ln));
        }

        /// <summary>
        /// The constant ln(2).
        /// </summary>
        internal const float _ln2 = 0.69314718056f;
        
        /// <summary>
        /// Determines whether the probability of <see cref="false"/> is at least as likely as the probability
        /// of <see cref="true"/>, and if so returns the <see cref="UnlikelyBoolDistribution"/> representation
        /// of this distribution. Note that if this returns false, it will return the
        /// <see cref="UnlikelyBoolDistribution"/> representation of the negation of this distribution instead.
        /// </summary>
        public bool IsUnlikely(out UnlikelyBoolDistribution dist)
        {
            if (_lnProb < 0)
            {
                dist = new UnlikelyBoolDistribution(_lnProb);
                return true;
            }
            else
            {
                dist = new UnlikelyBoolDistribution(-_lnProb);
                return false;
            }
        }

        /// <summary>
        /// Indicates whether this is <see cref="AlwaysFalse"/>.
        /// </summary>
        public bool IsAlwaysFalse => float.IsNegativeInfinity(_lnProb);

        /// <summary>
        /// Indicates whether this is <see cref="AlwaysTrue"/>.
        /// </summary>
        public bool IsAlwaysTrue => float.IsPositiveInfinity(_lnProb);

        /// <summary>
        /// The probability of <see cref="false"/> under this distribution.
        /// </summary>
        public LogScalar OfFalse
        {
            get
            {
                if (_lnProb < 0)
                    return new LogScalar(LogScalar._softminus(_lnProb));
                return new LogScalar(-_lnProb);
            }
        }

        /// <summary>
        /// The probability of <see cref="true"/> under this distribution.
        /// </summary>
        public LogScalar OfTrue
        {
            get
            {
                if (_lnProb > 0)
                    return new LogScalar(LogScalar._softminus(-_lnProb));
                return new LogScalar(_lnProb);
            }
        }

        /// <summary>
        /// Evaluates this distribution for the given value.
        /// </summary>
        public LogScalar this[bool value] => value ? OfTrue : OfFalse;

        /// <summary>
        /// Samples from this distribution.
        /// </summary>
        public bool Sample(ISampler sampler)
        {
            throw new NotImplementedException();
        }

        public static implicit operator FiniteDistribution<bool>(BoolDistribution dist)
        {
            // TODO: Nifty, but maybe too hacky?
            Debug.Assert(false.GetHashCode() == 0);
            Debug.Assert(true.GetHashCode() == 1);
            return (FiniteDistribution<bool>)new FiniteKernel<bool>(new PointKernel<bool>[]
            {
                new PointKernel<bool>(false, dist.OfFalse),
                new PointKernel<bool>(true, dist.OfTrue)
            });
        }

        public static bool operator ==(BoolDistribution a, BoolDistribution b)
        {
            return a._lnProb == b._lnProb;
        }

        public static bool operator !=(BoolDistribution a, BoolDistribution b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is BoolDistribution))
                return false;
            return this == (BoolDistribution)obj;
        }

        bool IEquatable<BoolDistribution>.Equals(BoolDistribution other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return _lnProb.GetHashCode();
        }

        public override string ToString()
        {
            if (IsAlwaysFalse)
                return false.ToString();
            if (IsAlwaysTrue)
                return true.ToString();
            return "Coin(" + OfTrue.ToString() + ")";
        }
    }

    /// <summary>
    /// A <see cref="BoolDistribution"/> where <see cref="false"/> is at least as likely as <see cref="true"/>. This
    /// allows for slightly faster sampling.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct UnlikelyBoolDistribution : IDistribution<bool>, IKernel<bool>, IEquatable<UnlikelyBoolDistribution>
    {
        private float _lnProb;
        internal UnlikelyBoolDistribution(float lnProb)
        {
            Debug.Assert(lnProb <= -BoolDistribution._ln2);
            _lnProb = lnProb;
        }

        /// <summary>
        /// A <see cref="UnlikelyBoolDistribution"/> where the probability of <see cref="false"/> is one.
        /// </summary>
        public static UnlikelyBoolDistribution AlwaysFalse => new UnlikelyBoolDistribution(float.NegativeInfinity);

        /// <summary>
        /// Constructs a <see cref="UnlikelyBoolDistribution"/> with the given probability of being <see cref="true"/>. The
        /// probability may be no more than 0.5.
        /// </summary>
        public static UnlikelyBoolDistribution FromTrue(LogScalar prob)
        {
            return new UnlikelyBoolDistribution(prob._ln);
        }

        /// <summary>
        /// Indicates whether this is <see cref="AlwaysFalse"/>.
        /// </summary>
        public bool IsAlwaysFalse => float.IsNegativeInfinity(_lnProb);

        /// <summary>
        /// The probability of <see cref="false"/> under this distribution.
        /// </summary>
        public LogScalar OfFalse => new LogScalar(LogScalar._softminus(_lnProb));

        /// <summary>
        /// The probability of <see cref="true"/> under this distribution.
        /// </summary>
        public LogScalar OfTrue => new LogScalar(_lnProb);

        /// <summary>
        /// Evaluates this distribution for the given value.
        /// </summary>
        public LogScalar this[bool value] => value ? OfTrue : OfFalse;

        /// <summary>
        /// Samples from this distribution.
        /// </summary>
        public bool Sample(ISampler sampler)
        {
            float lnProb = _lnProb;
            if (float.IsNegativeInfinity(lnProb))
                return false;
            while (lnProb < -_ln2_32)
            {
                if (sampler.SampleUInt32() != 0)
                    return false;
                lnProb += _ln2_32;
            }
            if (lnProb < -_ln2_16)
            {
                if (sampler.SampleUInt16() != 0)
                    return false;
                lnProb += _ln2_16;
            }
            uint pivot = (uint)(Math.Exp(lnProb) * uint.MaxValue);
            if (sampler.SampleUInt32() < pivot)
                return true;
            return false;
        }

        /// <summary>
        /// The constant ln(2^32).
        /// </summary>
        internal const float _ln2_32 = BoolDistribution._ln2 * 32;

        /// <summary>
        /// The constant ln(2^16).
        /// </summary>
        internal const float _ln2_16 = BoolDistribution._ln2 * 16;

        public static explicit operator UnlikelyBoolDistribution(BoolDistribution dist)
        {
            return new UnlikelyBoolDistribution(dist._lnProb);
        }

        public static implicit operator BoolDistribution(UnlikelyBoolDistribution dist)
        {
            return new BoolDistribution(dist._lnProb);
        }

        public static implicit operator FiniteDistribution<bool>(UnlikelyBoolDistribution dist)
        {
            return (BoolDistribution)dist;
        }

        public static bool operator ==(UnlikelyBoolDistribution a, UnlikelyBoolDistribution b)
        {
            return a._lnProb == b._lnProb;
        }

        public static bool operator !=(UnlikelyBoolDistribution a, UnlikelyBoolDistribution b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is UnlikelyBoolDistribution))
                return false;
            return this == (UnlikelyBoolDistribution)obj;
        }

        bool IEquatable<UnlikelyBoolDistribution>.Equals(UnlikelyBoolDistribution other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return _lnProb.GetHashCode();
        }

        public override string ToString()
        {
            return ((BoolDistribution)this).ToString();
        }
    }
}