﻿using System;
using System.Diagnostics;

using Alunite.Data.Abstraction;

namespace Alunite.Data.Probability
{
    /// <summary>
    /// An arbitrary probability distribution over fixed-length lists of small <see cref="uint"/> indices.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    [DebuggerTypeProxy(typeof(IndexListKernelDebugView))]
    public struct RegularIndexListDistribution : 
        IDistribution<List<uint>>, 
        IKernel<List<uint>>,
        IEquatable<RegularIndexListDistribution>
    {
        internal RegularIndexListDistribution(uint len, _Internal source)
        {
            Length = len;
            _source = source;
        }

        /// <summary>
        /// The length of all the lists in this distribution.
        /// </summary>
        public uint Length { get; }

        /// <summary>
        /// The underlying set for this <see cref="RegularIndexListSet"/>.
        /// </summary>
        internal _Internal _source;

        /// <summary>
        /// The <see cref="RegularIndexListDistribution"/> consisting solely of the zero-length list.
        /// </summary>
        public static RegularIndexListDistribution PointEmpty => new RegularIndexListDistribution(0, _Internal.PointEmpty);

        /// <summary>
        /// Constructs an <see cref="RegularIndexListDistribution"/> whose support contains only the given value.
        /// </summary>
        public static RegularIndexListDistribution Point(List<uint> value)
        {
            return Point(value);
        }

        /// <summary>
        /// Constructs an <see cref="RegularIndexListDistribution"/> whose support contains only the given value.
        /// </summary>
        public static RegularIndexListDistribution Point(ReadOnlySpan<uint> value)
        {
            uint len = (uint)value.Length;
            uint rem = len;
            _Internal tail = _Internal.PointEmpty;
            while (rem > 0)
            {
                rem--;
                uint item = value[(int)rem];
                _Case[] cases = new _Case[item + 1];
                for (uint i = 0; i < cases.Length; i++)
                    cases[i] = _Case.Zero;
                cases[item] = _Case.Build(LogScalar.One, tail);
                tail = new _Internal(cases);
            }
            return new RegularIndexListDistribution(len, tail);
        }

        /// <summary>
        /// Constructs an <see cref="RegularIndexListDistribution"/> where each item has a given distribution, independent
        /// of all other items.
        /// </summary>
        public static RegularIndexListDistribution Independent(params IndexDistribution[] items)
        {
            return Independent((Span<IndexDistribution>)items);
        }

        /// <summary>
        /// Constructs an <see cref="RegularIndexListDistribution"/> where each item has a given distribution, independent
        /// of all other items.
        /// </summary>
        public static RegularIndexListDistribution Independent(ReadOnlySpan<IndexDistribution> items)
        {
            uint len = (uint)items.Length;
            uint rem = len;
            _Internal tail = _Internal.PointEmpty;
            while (rem > 0)
            {
                rem--;
                IndexDistribution dist = items[(int)rem];
                _Case[] cases = new _Case[dist.Bound];
                for (uint i = 0; i < cases.Length; i++)
                    cases[i] = _Case.Build(dist[i], tail);
                tail = new _Internal(cases);
            }
            return new RegularIndexListDistribution(len, tail);
        }

        /// <summary>
        /// Constructs an <see cref="RegularIndexListDistribution"/> where all items are independent and uniform for values
        /// less than the given bounds.
        /// </summary>
        public static RegularIndexListDistribution IndependentUniformBelow(params uint[] bounds)
        {
            return IndependentUniformBelow((Span<uint>)bounds);
        }

        /// <summary>
        /// Constructs an <see cref="RegularIndexListDistribution"/> where all items are independent and uniform for values
        /// less than the given bounds.
        /// </summary>
        public static RegularIndexListDistribution IndependentUniformBelow(ReadOnlySpan<uint> bounds)
        {
            uint len = (uint)bounds.Length;
            uint rem = len;
            _Internal tail = _Internal.PointEmpty;
            while (rem > 0)
            {
                rem--;
                uint bound = bounds[(int)rem];
                LogScalar prob = ((LogScalar)bound).Reciprocal;
                _Case[] cases = new _Case[bound];
                _Case @case = _Case.Build(prob, tail);
                for (uint i = 0; i < cases.Length; i++)
                    cases[i] = @case;
                tail = new _Internal(cases);
            }
            return new RegularIndexListDistribution(len, tail);
        }

        /// <summary>
        /// Constructs an <see cref="RegularIndexListDistribution"/> as the concatenation of the given lists.
        /// </summary>
        public static RegularIndexListDistribution Concat(params RegularIndexListDistribution[] lists)
        {
            return Concat((Span<RegularIndexListDistribution>)lists);
        }

        /// <summary>
        /// Constructs an <see cref="RegularIndexListDistribution"/> as the concatenation of the given lists.
        /// </summary>
        public static RegularIndexListDistribution Concat(ReadOnlySpan<RegularIndexListDistribution> lists)
        {
            throw new NotImplementedException();
        }
        
        /// <summary>
        /// Constructs a <see cref="RegularIndexListDistribution"/> that is uniformly distributed among the lists in the
        /// given set.
        /// </summary>
        public static RegularIndexListDistribution Uniform(RegularIndexListSet set)
        {
            if (set.IsNone)
                throw new ArgumentException("Set must not be empty", nameof(set));
            return RegularIndexListKernel.Indicator(set).Distribution;
        }

        /// <summary>
        /// Indicates whether this is <see cref="PointEmpty"/>.
        /// </summary>
        public bool IsPointEmpty => _source._cases is null;

        /// <summary>
        /// Determines whether this is a distribution of non-empty lists, and if so, deconstructs it as a 
        /// <see cref="RegularIndexListKernel.Switch"/>.
        /// </summary>
        public bool IsSwitch(out List<RegularIndexListKernel> cases)
        {
            return ((RegularIndexListKernel)this).IsSwitch(out cases);
        }

        /// <summary>
        /// Evaluates this distribution for the given value.
        /// </summary>
        public LogScalar this[List<uint> value] => Evaluate(value);

        /// <summary>
        /// Evaluates this distribution for the given value.
        /// </summary>
        public LogScalar Evaluate(SpanList<uint> items)
        {
            if ((uint)items.Length != Length)
                return LogScalar.Zero;
            return _source.Evaluate(items);
        }

        /// <summary>
        /// Samples from this distribution.
        /// </summary>
        public List<uint> Sample(ISampler sampler)
        {
            uint[] items = new uint[Length];
            _source.Sample(sampler, items);
            return new List<uint>(items);
        }

        /// <summary>
        /// Samples from this distribution.
        /// </summary>
        public void Sample(ISampler sampler, Span<uint> items)
        {
            _source.Sample(sampler, items);
        }

        /// <summary>
        /// A list which contains one more than the maximum value for each item in this distribution.
        /// </summary>
        public List<uint> Bounds
        {
            get
            {
                if (IsPointEmpty)
                    return List.Empty<uint>();
                var cache = new SetBuilder<_Internal>();
                uint[] bounds = new uint[Length];
                _source._getBounds(cache, ref bounds[0], bounds.AsSpan().Slice(1));
                return new List<uint>(bounds);
            }
        }

        /// <summary>
        /// The set of all lists which have a non-zero probability under this distribution.
        /// </summary>
        public RegularIndexListSet Support
        {
            get
            {
                if (IsPointEmpty) return RegularIndexListSet.SingletonEmpty;
                var cache = new MapBuilder<_Internal, RegularIndexListSet._Internal>();
                return new RegularIndexListSet(Length, _source._getSupport(cache));
            }
        }
        
        /// <summary>
        /// Provides a measure of how complex this distribution is. This is minimal for <see cref="Independent"/>
        /// distributions and increases depending on how interdependent the distribution is.
        /// </summary>
        public uint Complexity
        {
            get
            {
                if (IsPointEmpty)
                    return 0;
                var set = new SetBuilder<_Internal>();
                _source._getComplexity(set);
                return set.Size;
            }
        }

        /// <summary>
        /// Gets the distribution for some contiguous subsequence of this list.
        /// </summary>
        public RegularIndexListDistribution Slice(uint start, uint len)
        {
            if (start == 0)
            {
                uint thisLen = Length;
                if (len < thisLen)
                {
                    var cache = new MapBuilder<_Internal, _Internal>();
                    return new RegularIndexListDistribution(len, _source._splitPrefix(len, cache));
                }
                else
                {
                    if (len != thisLen)
                        throw new ArgumentOutOfRangeException(nameof(len));
                    return this;
                }
            }
            return Subseq(IndexSet.Range(start, len));
        }

        /// <summary>
        /// Gets the distribution for a subsequence of this list.
        /// </summary>
        public RegularIndexListDistribution Subseq(IndexSet indices)
        {
            if (indices.IsBelow(out uint bound) && bound == Length)
                return this;
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the distribution for a list constructed by selecting items from this list.
        /// </summary>
        public RegularIndexListDistribution Select(ReadOnlySpan<uint> indices)
        {
            // Prepare selection plan
            RegularIndexListSet._prepareSelect(
                Length, indices,
                out IndexSet subseqIndices,
                out RegularIndexListSet._SelectInstruction[] insts);

            // Do subseq
            RegularIndexListDistribution source = Subseq(subseqIndices);

            // Shortcut for if we are already done at this point
            if (insts.Length == 0)
                return source;

            // Perform actual selection
            var cache = new MapBuilder<(uint, _Internal, List<uint>), _Internal>();
            var pullCache = new MapBuilder<(uint, _Internal), _Case[]>();
            _Internal apply(uint step, _Internal rem, List<uint> vars)
            {
                if (step < (uint)insts.Length)
                {
                    var key = (step, rem, vars);
                    if (cache.TryGet(key, out _Internal res))
                        return res;
                    RegularIndexListSet._SelectInstruction inst = insts[step];
                    if (inst.IsLoad)
                    {
                        // Load next item from variable
                        uint var = vars[inst.Index];
                        _Case[] cases = new _Case[var + 1];
                        for (uint i = 0; i < var; i++)
                            cases[i] = _Case.Zero;
                        if (inst.ShouldModify)
                            vars = vars.RemoveAt(inst.Index);
                        cases[var] = _Case.Build(LogScalar.One, apply(step + 1, rem, vars));
                        res = new _Internal(cases);
                    }
                    else
                    {
                        // Pull item from rem
                        _Case[] cases = rem._pull(pullCache, inst.Index);
                        _Case[] nCases = new _Case[cases.Length];
                        uint nStep = step + 1;
                        if (inst.ShouldModify)
                        {
                            for (uint i = 0; i < cases.Length; i++)
                            {
                                _Case @case = cases[i];
                                List<uint> nVars = vars.Append(i);
                                if (@case.Probability.IsZero)
                                    nCases[i] = _Case.Zero;
                                else
                                    nCases[i] = _Case.Build(@case.Probability, apply(step + 1, @case.Tail, nVars));
                            }
                        }
                        else
                        {
                            for (uint i = 0; i < cases.Length; i++)
                            {
                                _Case @case = cases[i];
                                if (@case.Probability.IsZero)
                                    nCases[i] = _Case.Zero;
                                else
                                    nCases[i] = _Case.Build(@case.Probability, apply(step + 1, @case.Tail, vars));
                            }
                        }
                        res = new _Internal(nCases);
                    }
                    cache.Add(key, res);
                    return res;
                }
                else
                {
                    return rem;
                }
            }
            return new RegularIndexListDistribution((uint)indices.Length, apply(0, source._source, List.Empty<uint>()));
        }

        /// <summary>
        /// Reinterprets this distribution as the concatenation of prefix of a certain length and a suffix chosen based on
        /// the prefix.
        /// </summary>
        public void Split(uint prefixLen,
            out RegularIndexListDistribution prefix,
            out RegularIndexListMap<RegularIndexListDistribution> suffix)
        {
            if (!(prefixLen < Length))
                throw new ArgumentException("Invalid prefix length", nameof(prefixLen));
            if (prefixLen == 0)
            {
                prefix = PointEmpty;
                suffix = RegularIndexListMap.SingletonEmpty(this);
                return;
            }

            // Construct prefix
            var prefixCache = new MapBuilder<_Internal, _Internal>();
            prefix = new RegularIndexListDistribution(prefixLen, _source._splitPrefix(prefixLen, prefixCache));

            // Construct suffix
            var suffixCache = new MapBuilder<_Internal, RegularIndexListMap<RegularIndexListDistribution>._Internal>();
            suffix = new RegularIndexListMap<RegularIndexListDistribution>(prefixLen,
                _source._splitSuffix(prefixLen, Length - prefixLen, suffixCache));
        }

        /// <summary>
        /// Calculates the "distance" between two <see cref="RegularIndexListDistribution"/>.
        /// </summary>
        public static Scalar Distance(RegularIndexListDistribution a, RegularIndexListDistribution b)
        {
            return Scalar.Sqrt(SqrDistance(a, b));
        }

        /// <summary>
        /// Calculates the square of <see cref="Distance"/>. This is equal to the sum of the square differences of the
        /// log-probabilities for every list in their support. This will be <see cref="Scalar.Inf"/> if the distributions
        /// do not have the same support.
        /// </summary>
        public static Scalar SqrDistance(RegularIndexListDistribution a, RegularIndexListDistribution b)
        {
            if (a.Length != b.Length)
                return Scalar.Inf;
            var cache = new MapBuilder<(_Internal, _Internal), ScalarPoly2>();
            return _Internal._getSqrDistance(cache, a._source, b._source).C_0;
        }

        /// <summary>
        /// The internal recursive definition of <see cref="RegularIndexListDistribution"/>, which doesn't store the length
        /// at each node.
        /// </summary>
        internal struct _Internal : IEquatable<_Internal>
        {
            internal _Internal(_Case[] cases)
            {
                _cases = cases;
            }

            /// <summary>
            /// The table defining the contents of this <see cref="RegularIndexListDistribution"/>. If this is null, the
            /// distribution consists solely of the empty list. Otherwise, this is a <see cref="_Case"/> array which
            /// decomposes the distribution based on the value of the first item.
            /// </summary>
            internal _Case[] _cases;

            /// <summary>
            /// The distribution consisting solely of the empty list.
            /// </summary>
            public static _Internal PointEmpty => default;
            
            /// <summary>
            /// Constructs an <see cref="_Internal"/> from the given cases, trimming zero-probability cases
            /// at the end of the array.
            /// </summary>
            public static _Internal Switch(_Case[] cases)
            {
                uint last = (uint)cases.Length;
                while (last > 0)
                {
                    if (!cases[--last].Probability.IsZero)
                        goto foundLast;
                }
                return default;

            foundLast:
                uint len = last + 1;
                if (len < (uint)cases.Length)
                {
                    _Case[] nCases = new _Case[len];
                    for (int i = 0; i < nCases.Length; i++)
                        nCases[i] = cases[i];
                    cases = nCases;
                }
                return new _Internal(cases);
            }

            /// <summary>
            /// Indicates whether this is <see cref="PointEmpty"/>.
            /// </summary>
            public bool IsPointEmpty => _cases is null;

            /// <summary>
            /// Evaluates this distribution for the given value.
            /// </summary>
            public LogScalar Evaluate(ReadOnlySpan<uint> items)
            {
                LogScalar prob = LogScalar.One;
                _Internal dist = this;
            next:
                if (dist._cases is null)
                {
                    return prob;
                }
                else
                {
                    uint first = items[0];
                    if (first < (uint)dist._cases.Length)
                    {
                        _Case @case = dist._cases[first];
                        prob *= @case.Probability;
                        dist = @case.Tail;
                        items = items.Slice(1);
                        goto next;
                    }
                }
                return LogScalar.Zero;
            }

            /// <summary>
            /// Samples from this distribution.
            /// </summary>
            public void Sample(ISampler sampler, Span<uint> items)
            {
                _Internal dist = this;
            next:
                if (items.Length > 0)
                {
                    Scalar sample = sampler.SampleUnit();
                    uint last = (uint)dist._cases.Length - 1;
                    for (uint i = 0; i < last; i++)
                    {
                        _Case @case = dist._cases[i];
                        Scalar prob = @case.Probability;
                        if (sample < prob)
                        {
                            items[0] = i;
                            items = items.Slice(1);
                            dist = @case.Tail;
                            goto next;
                        }
                        sample -= prob;
                    }
                    items[0] = last;
                    items = items.Slice(1);
                    dist = dist._cases[last].Tail;
                    goto next;
                }
            }

            /// <summary>
            /// Constructs the support for this non-<see cref="PointEmpty"/> distribution.
            /// </summary>
            internal RegularIndexListSet._Internal _getSupport(MapBuilder<_Internal, RegularIndexListSet._Internal> cache)
            {
                if (cache.TryGet(this, out RegularIndexListSet._Internal res))
                    return res;
                uint last = (uint)_cases.Length - 1;
                if (_cases[last].Tail._cases is null)
                {
                    // Construct bitmap for length-one lists
                    ulong[] bitmap = new ulong[BoolList._numWords((uint)_cases.Length)];
                    for (uint i = 0; i < (uint)_cases.Length; i++)
                    {
                        _Case @case = _cases[i];
                        if (!@case.Probability.IsZero)
                            BoolList._setTrue(bitmap, i);
                    }
                    res = new RegularIndexListSet._Internal(bitmap);
                }
                else
                {
                    // Constructs case table for longer lists
                    RegularIndexListSet._Case[] nCases = new RegularIndexListSet._Case[_cases.Length];
                    for (int i = 0; i < _cases.Length; i++)
                    {
                        _Case @case = _cases[i];
                        if (@case.Probability.IsZero)
                            nCases[i] = RegularIndexListSet._Case.None;
                        else
                            nCases[i] = RegularIndexListSet._Case.Build(@case.Tail._getSupport(cache));
                    }
                    res = new RegularIndexListSet._Internal(nCases);
                }
                cache.Add(this, res);
                return res;
            }

            /// <summary>
            /// Computes the <see cref="Bounds"/> for distributions.
            /// </summary>
            internal void _getBounds(SetBuilder<_Internal> cache, ref uint headBound, Span<uint> tailBounds)
            {
                if (!(_cases is null))
                {
                    if (cache.Include(this))
                    {
                        if (headBound < (uint)_cases.Length)
                            headBound = (uint)_cases.Length;
                        if (tailBounds.Length > 0)
                        {
                            ref uint nHeadBound = ref tailBounds[0];
                            Span<uint> nTailBounds = tailBounds.Slice(1);
                            for (uint i = 0; i < _cases.Length; i++)
                            {
                                _Case @case = _cases[i];
                                if (!@case.Probability.IsZero)
                                    @case.Tail._getBounds(cache, ref nHeadBound, nTailBounds);
                            }
                        }
                    }
                }
            }

            /// <summary>
            /// Provides a measure of how complex this distribution is. This is minimal for <see cref="Independent"/>
            /// distributions and increases depending on how interdependent the distribution is.
            /// </summary>
            public uint Complexity
            {
                get
                {
                    if (IsPointEmpty)
                        return 0;
                    var set = new SetBuilder<_Internal>();
                    _getComplexity(set);
                    return set.Size;
                }
            }

            /// <summary>
            /// Enumerates all <see cref="_Internal"/>s referenced in this distribution, not including the
            /// <see cref="PointEmpty"/> distribution.
            /// </summary>
            internal void _getComplexity(SetBuilder<_Internal> set)
            {
                if (!(_cases is null))
                {
                    if (set.Include(this))
                    {
                        foreach (var @case in _cases)
                            @case.Tail._getComplexity(set);
                    }
                }
            }

            /// <summary>
            /// Gets the possible lists for a prefix of this distribution.
            /// </summary>
            internal _Internal _splitPrefix(uint prefixLen, MapBuilder<_Internal, _Internal> cache)
            {
                if (prefixLen == 0)
                    return PointEmpty;
                if (cache.TryGet(this, out _Internal res))
                    return res;
                _Case[] nCases = new _Case[_cases.Length];
                uint nPrefixLen = prefixLen - 1;
                for (uint i = 0; i < (uint)_cases.Length; i++)
                {
                    _Case @case = _cases[i];
                    if (@case.Probability.IsZero)
                        nCases[i] = _Case.Zero;
                    else
                        nCases[i] = _Case.Build(@case.Probability, @case.Tail._splitPrefix(nPrefixLen, cache));
                }
                res = new _Internal(nCases);
                cache.Add(this, res);
                return res;
            }

            /// <summary>
            /// Gets the possible suffixes after a prefix of the given length in this distribution.
            /// </summary>
            internal RegularIndexListMap<RegularIndexListDistribution>._Internal _splitSuffix(
                uint prefixLen, uint suffixLen,
                MapBuilder<_Internal, RegularIndexListMap<RegularIndexListDistribution>._Internal> cache)
            {
                if (cache.TryGet(this, out var res))
                    return res;
                if (prefixLen == 0)
                {
                    var dist = new RegularIndexListDistribution(suffixLen, this);
                    res = new RegularIndexListMap<RegularIndexListDistribution>._Internal(dist);
                }
                else
                {
                    // Recursively split cases
                    var nCases = new RegularIndexListMap<RegularIndexListDistribution>._Case[_cases.Length];
                    uint nPrefixLen = prefixLen - 1;
                    for (uint i = 0; i < (uint)_cases.Length; i++)
                    {
                        _Case @case = _cases[i];
                        if (@case.Probability.IsZero)
                            nCases[i] = RegularIndexListMap<RegularIndexListDistribution>._Case.Empty;
                        else
                            nCases[i] = RegularIndexListMap<RegularIndexListDistribution>._Case.Build(
                                @case.Tail._splitSuffix(nPrefixLen, suffixLen, cache));
                    }
                    res = new RegularIndexListMap<RegularIndexListDistribution>._Internal(nCases);
                }
                cache.Add(this, res);
                return res;
            }

            /// <summary>
            /// Filters this distribution to only include lists where the given subsequence of items are in a given set.
            /// </summary>
            internal RegularIndexListKernel._Internal _intersectSubseq(
                MapBuilder<(_Internal, RegularIndexListSet._Internal), RegularIndexListKernel._Internal> cache,
                ReadOnlySpan<uint> indices, RegularIndexListSet._Internal other, uint offset)
            {
                if (other.IsNone)
                    return RegularIndexListKernel._Internal.Zero;
                if (_cases is null)
                {
                    // Empty distribution can't be filtered further
                    return this;
                }
                else
                {
                    var key = (this, other);
                    if (cache.TryGet(key, out RegularIndexListKernel._Internal res))
                        return res;

                    // Get maximum log probability for this distribution, necessary for numerical stability
                    Scalar maxThisLn = -Scalar.Inf;
                    for (int i = 0; i < _cases.Length; i++)
                    {
                        Scalar ln = _cases[i].Probability.Ln;
                        if (ln > maxThisLn) maxThisLn = ln;
                    }

                    // Check if these cases correspond to a subsequence item
                    _Case[] nCases;
                    Scalar maxCaseLn = -Scalar.Inf;
                    uint nOffset = offset + 1;
                    if (offset == indices[0])
                    {
                        if (other._table is RegularIndexListSet._Case[] otherCases)
                        {
                            // Intersect cases with setCases
                            ReadOnlySpan<uint> nIndices = indices.Slice(1);
                            nCases = new _Case[Math.Min(_cases.Length, otherCases.Length)];
                            for (int i = 0; i < nCases.Length; i++)
                            {
                                _Case @case = _cases[i];
                                if (@case.Probability.IsZero)
                                {
                                    nCases[i] = _Case.Zero;
                                }
                                else
                                {
                                    var nKernel = @case.Tail._intersectSubseq(cache, nIndices, otherCases[i].Tail, nOffset);
                                    Scalar caseLn = (@case.Probability.Ln - maxThisLn) + nKernel.Weight.Ln;
                                    if (caseLn > maxCaseLn) maxCaseLn = caseLn;
                                    nCases[i] = _Case.Build(new LogScalar(caseLn), nKernel.Distribution);
                                }
                            }
                        }
                        else
                        {
                            // Intersect cases with set item
                            IndexSet otherItem = new IndexSet((ulong[])other._table);
                            nCases = new _Case[Math.Min(_cases.Length, otherItem.Bound)];
                            for (uint i = 0; i < (uint)nCases.Length; i++)
                            {
                                _Case @case = _cases[i];
                                if (@case.Probability.IsZero)
                                {
                                    nCases[i] = _Case.Zero;
                                }
                                else if (otherItem.Contains(i))
                                {
                                    Scalar caseLn = @case.Probability.Ln - maxThisLn;
                                    if (caseLn > maxCaseLn) maxCaseLn = caseLn;
                                    nCases[i] = _Case.Build(new LogScalar(caseLn), @case.Tail);
                                }
                                else
                                {
                                    nCases[i] = _Case.Zero;
                                }
                            }
                        }
                    }
                    else
                    {
                        // Intersect cases
                        nCases = new _Case[_cases.Length];
                        for (int i = 0; i < nCases.Length; i++)
                        {
                            _Case @case = _cases[i];
                            if (@case.Probability.IsZero)
                            {
                                nCases[i] = _Case.Zero;
                            }
                            else
                            {
                                var nKernel = @case.Tail._intersectSubseq(cache, indices, other, nOffset);
                                Scalar caseLn = (@case.Probability.Ln - maxThisLn) + nKernel.Weight.Ln;
                                if (caseLn > maxCaseLn) maxCaseLn = caseLn;
                                nCases[i] = _Case.Build(new LogScalar(caseLn), nKernel.Distribution);
                            }
                        }
                    }

                    // Normalize probabilities
                    if (maxCaseLn == -Scalar.Inf)
                        return RegularIndexListKernel._Internal.Zero;
                    Scalar addExp = 0;
                    for (int i = 0; i < nCases.Length; i++)
                        addExp += Scalar.Exp(nCases[i].Probability.Ln - maxCaseLn);
                    Scalar norm = maxCaseLn + Scalar.Ln(addExp);
                    for (int i = 0; i < nCases.Length; i++)
                        nCases[i].Probability = new LogScalar(nCases[i].Probability.Ln - norm);

                    // Construct new distribution
                    res = new LogScalar(maxThisLn + norm) * Switch(nCases);
                    cache.Add(key, res);
                    return res;
                }
            }

            /// <summary>
            /// Filters this distribution to only include lists where the given subsequence of items satisfy the given predicate.
            /// </summary>
            internal RegularIndexListKernel._Internal _filterSubseq(
                ReadOnlySpan<uint> indices, Span<uint> values, int place,
                Predicate<List<uint>> pred, uint offset)
            {
                // TODO: Caching
                if (place < indices.Length)
                {
                    // Get maximum log probability for this distribution, necessary for numerical stability
                    Scalar maxThisLn = -Scalar.Inf;
                    for (int i = 0; i < _cases.Length; i++)
                    {
                        Scalar ln = _cases[i].Probability.Ln;
                        if (ln > maxThisLn) maxThisLn = ln;
                    }

                    // Check if these cases correspond to a list item
                    _Case[] nCases;
                    uint index = indices[place];
                    uint nOffset = offset + 1;
                    Scalar maxCaseLn = -Scalar.Inf;
                    if (offset == index)
                    {
                        // Filter with an additional list item
                        nCases = new _Case[_cases.Length];
                        int nPlace = place + 1;
                        for (uint i = 0; i < (uint)nCases.Length; i++)
                        {
                            _Case @case = _cases[i];
                            if (@case.Probability.IsZero)
                            {
                                nCases[i] = _Case.Zero;
                            }
                            else
                            {
                                values[place] = i;
                                var nKernel = @case.Tail._filterSubseq(indices, values, nPlace, pred, nOffset);
                                Scalar caseLn = (@case.Probability.Ln - maxThisLn) + nKernel.Weight.Ln;
                                if (caseLn > maxCaseLn) maxCaseLn = caseLn;
                                nCases[i] = _Case.Build(new LogScalar(caseLn), nKernel.Distribution);
                            }
                        }
                    }
                    else
                    {
                        // Filter children
                        nCases = new _Case[_cases.Length];
                        for (int i = 0; i < nCases.Length; i++)
                        {
                            _Case @case = _cases[i];
                            if (@case.Probability.IsZero)
                            {
                                nCases[i] = _Case.Zero;
                            }
                            else
                            {
                                var nKernel = @case.Tail._filterSubseq(indices, values, place, pred, nOffset);
                                Scalar caseLn = (@case.Probability.Ln - maxThisLn) + nKernel.Weight.Ln;
                                if (caseLn > maxCaseLn) maxCaseLn = caseLn;
                                nCases[i] = _Case.Build(new LogScalar(caseLn), nKernel.Distribution);
                            }
                        }
                    }

                    // Normalize probabilities
                    if (maxCaseLn == -Scalar.Inf)
                        return RegularIndexListKernel._Internal.Zero;
                    Scalar addExp = 0;
                    for (int i = 0; i < nCases.Length; i++)
                        addExp += Scalar.Exp(nCases[i].Probability.Ln - maxCaseLn);
                    Scalar norm = maxCaseLn + Scalar.Ln(addExp);
                    for (int i = 0; i < nCases.Length; i++)
                        nCases[i].Probability = new LogScalar(nCases[i].Probability.Ln - norm);

                    // Construct new distribution
                    return new LogScalar(maxThisLn + norm) * Switch(nCases);
                }
                else if (pred(List.Of(values)))
                {
                    return this;
                }
                else
                {
                    return RegularIndexListKernel._Internal.Zero;
                }
            }
            
            /// <summary>
            /// Multiplies the weights in this distribution based on the given subsequence of items.
            /// </summary>
            internal RegularIndexListKernel._Internal _conflateSubseq(
                MapBuilder<(_Internal, _Internal), RegularIndexListKernel._Internal> cache,
                ReadOnlySpan<uint> indices, _Internal other, uint offset)
            {
                if (indices.Length == 0)
                {
                    Debug.Assert(other.IsPointEmpty);
                    return this;
                }
                else
                {
                    return _conflateSubseq(cache, indices[0], indices.Slice(1), other, offset);
                }
            }

            /// <summary>
            /// Multiplies the weights in this distribution based on the given subsequence of items.
            /// </summary>
            internal RegularIndexListKernel._Internal _conflateSubseq(
                MapBuilder<(_Internal, _Internal), RegularIndexListKernel._Internal> cache,
                uint headIndex, ReadOnlySpan<uint> tailIndices,
                _Internal other, uint offset)
            {
                if (_cases is null)
                {
                    // Empty distribution can't be filtered further
                    return this;
                }
                else
                {
                    // Check cache
                    var key = (this, other);
                    if (!cache.TryGet(key, out RegularIndexListKernel._Internal res))
                    {
                        uint nOffset = offset + 1;
                        if (offset == headIndex)
                        {
                            // Get maximum log probabilities for this and dist, necessary for numerical stability
                            Scalar maxThisLn = -Scalar.Inf;
                            for (int i = 0; i < _cases.Length; i++)
                            {
                                Scalar ln = _cases[i].Probability.Ln;
                                if (ln > maxThisLn) maxThisLn = ln;
                            }
                            Scalar maxDistLn = -Scalar.Inf;
                            for (int i = 0; i < other._cases.Length; i++)
                            {
                                Scalar ln = other._cases[i].Probability.Ln;
                                if (ln > maxDistLn) maxDistLn = ln;
                            }

                            // Conflate cases with distCases
                            Scalar maxCaseLn = -Scalar.Inf;
                            _Case[] nCases = new _Case[Math.Min(_cases.Length, other._cases.Length)];
                            for (int i = 0; i < nCases.Length; i++)
                            {
                                _Case @case = _cases[i];
                                _Case otherCase = other._cases[i];
                                Scalar caseLn = (@case.Probability.Ln - maxThisLn) + (otherCase.Probability.Ln - maxDistLn);
                                if (caseLn > -Scalar.Inf)
                                {
                                    var nKernel = @case.Tail._conflateSubseq(cache, tailIndices, otherCase.Tail, nOffset);
                                    caseLn = caseLn + nKernel.Weight._ln;
                                    if (caseLn > maxCaseLn) maxCaseLn = caseLn;
                                    nCases[i] = _Case.Build(new LogScalar(caseLn), nKernel.Distribution);
                                }
                                else
                                {
                                    nCases[i] = _Case.Zero;
                                }
                            }

                            // Normalize probabilities
                            if (maxCaseLn == -Scalar.Inf)
                                return RegularIndexListKernel._Internal.Zero;
                            Scalar addExp = 0;
                            for (int i = 0; i < nCases.Length; i++)
                                addExp += Scalar.Exp(nCases[i].Probability.Ln - maxCaseLn);
                            Scalar norm = maxCaseLn + Scalar.Ln(addExp);
                            for (int i = 0; i < nCases.Length; i++)
                                nCases[i].Probability = new LogScalar(nCases[i].Probability.Ln - norm);

                            // Construct new distribution
                            res = new LogScalar(maxThisLn + (maxDistLn + norm)) * Switch(nCases);
                        }
                        else
                        {
                            // Get maximum log probability for this distribution, required for numerical stability
                            Scalar maxThisLn = -Scalar.Inf;
                            for (int i = 0; i < _cases.Length; i++)
                            {
                                Scalar ln = _cases[i].Probability.Ln;
                                if (ln > maxThisLn) maxThisLn = ln;
                            }

                            // Conflate cases
                            Scalar maxCaseLn = -Scalar.Inf;
                            _Case[] nCases = new _Case[_cases.Length];
                            for (int i = 0; i < nCases.Length; i++)
                            {
                                _Case @case = _cases[i];
                                if (@case.Probability.IsZero)
                                {
                                    nCases[i] = _Case.Zero;
                                }
                                else
                                {
                                    var nKernel = @case.Tail._conflateSubseq(cache, headIndex, tailIndices, other, nOffset);
                                    Scalar caseLn = (@case.Probability.Ln - maxThisLn) + nKernel.Weight.Ln;
                                    if (caseLn > maxCaseLn) maxCaseLn = caseLn;
                                    nCases[i] = _Case.Build(new LogScalar(caseLn), nKernel.Distribution);
                                }
                            }

                            // Normalize probabilities
                            if (maxCaseLn == -Scalar.Inf)
                                return RegularIndexListKernel._Internal.Zero;
                            Scalar addExp = 0;
                            for (int i = 0; i < nCases.Length; i++)
                                addExp += Scalar.Exp(nCases[i].Probability.Ln - maxCaseLn);
                            Scalar norm = maxCaseLn + Scalar.Ln(addExp);
                            for (int i = 0; i < nCases.Length; i++)
                                nCases[i].Probability = new LogScalar(nCases[i].Probability.Ln - norm);

                            // Construct new distribution
                            res = new LogScalar(maxThisLn + norm) * Switch(nCases);
                        }
                        cache.Add(key, res);
                    }
                    return res;
                }
            }

            /// <summary>
            /// Constructs a new distribution by "pulling" the item at the given index to the beginning of the list.
            /// </summary>
            internal _Case[] _pull(MapBuilder<(uint, _Internal), _Case[]> cache, uint index)
            {
                if (index == 0)
                {
                    // Nothing to be done, the item is already at the beginning of the list.
                    return _cases;
                }
                else
                {
                    var key = (index, this);
                    if (cache.TryGet(key, out _Case[] res))
                        return res;

                    // Get maximum log probability for cases, necessary for numerical stability
                    Scalar maxThisLn = -Scalar.Inf;
                    for (int i = 0; i < _cases.Length; i++)
                    {
                        Scalar ln = _cases[i].Probability.Ln;
                        if (ln > maxThisLn) maxThisLn = ln;
                    }

                    // Pull child cases
                    uint bound = 0;
                    uint nIndex = index - 1;
                    _Case[][] pullCases = new _Case[_cases.Length][];
                    for (int i = 0; i < _cases.Length; i++)
                    {
                        _Case @case = _cases[i];
                        if (!@case.Probability.IsZero)
                        {
                            _Case[] pullCase = @case.Tail._pull(cache, nIndex);
                            if (bound < (uint)pullCase.Length)
                                bound = (uint)pullCase.Length;
                            pullCases[i] = pullCase;
                        }
                        else
                        {
                            pullCases[i] = Array.Empty<_Case>();
                        }
                    }

                    // Build new cases by transposing pullCases
                    Scalar maxCaseLn = -Scalar.Inf;
                    _Case[] nCases = new _Case[bound];
                    for (uint i = 0; i < bound; i++)
                    {
                        // Get maximum probability over pull cases, necessary for numerical stability
                        Scalar maxPullLn = -Scalar.Inf;
                        for (int j = 0; j < _cases.Length; j++)
                        {
                            if (i < (uint)pullCases[j].Length)
                            {
                                Scalar ln = pullCases[j][i].Probability.Ln;
                                if (ln > maxPullLn) maxPullLn = ln;
                            }
                        }

                        // Is this case even possible?
                        if (maxPullLn == -Scalar.Inf)
                        {
                            nCases[i] = _Case.Zero;
                        }
                        else
                        {
                            // Build new cases for inner distribution
                            Scalar maxPullCaseLn = -Scalar.Inf;
                            _Case[] nPullCases = new _Case[_cases.Length];
                            for (int j = 0; j < nPullCases.Length; j++)
                            {
                                _Case @case = _cases[j];
                                if (!@case.Probability.IsZero && i < (uint)pullCases[j].Length)
                                {
                                    _Case pullCase = pullCases[j][i];
                                    Scalar ln = (@case.Probability.Ln - maxThisLn) + (pullCase.Probability.Ln - maxPullLn);
                                    if (ln > maxPullCaseLn) maxPullCaseLn = ln;
                                    nPullCases[j] = new _Case(new LogScalar(ln), pullCase.TailHash, pullCase.Tail);
                                }
                                else
                                {
                                    nPullCases[j] = _Case.Zero;
                                }
                            }

                            // Normalize probabilities
                            Scalar addExp = 0;
                            for (int j = 0; j < nPullCases.Length; j++)
                                addExp += Scalar.Exp(nPullCases[j].Probability.Ln - maxPullCaseLn);
                            Scalar norm = maxPullCaseLn + Scalar.Ln(addExp);
                            for (int j = 0; j < nPullCases.Length; j++)
                                nPullCases[j].Probability = new LogScalar(nPullCases[j].Probability.Ln - norm);
                            Scalar caseLn = maxPullLn + norm;
                            if (caseLn > maxCaseLn) maxCaseLn = caseLn;
                            nCases[i] = _Case.Build(new LogScalar(caseLn), Switch(nPullCases));
                        }
                    }

                    // Normalize probabilities
                    {
                        Scalar addExp = 0;
                        for (int j = 0; j < nCases.Length; j++)
                            addExp += Scalar.Exp(nCases[j].Probability.Ln - maxCaseLn);
                        Scalar norm = maxCaseLn + Scalar.Ln(addExp);
                        for (int j = 0; j < nCases.Length; j++)
                            nCases[j].Probability = new LogScalar(nCases[j].Probability.Ln - norm);
                    }
                    res = Switch(nCases)._cases;
                    cache.Add(key, res);
                    return res;
                }
            }

            /// <summary>
            /// Computes the <see cref="SqrDistance"/> between two distributions as a function of the uniform log-probability
            /// offset added to the first.
            /// </summary>
            internal static ScalarPoly2 _getSqrDistance(
                MapBuilder<(_Internal, _Internal), ScalarPoly2> cache,
                _Internal a, _Internal b)
            {
                if (a._cases is null)
                {
                    // Zero-length distributions are always identical.
                    Debug.Assert(b._cases is null);
                    return new ScalarPoly2(0, 0, 1);
                }
                else
                {
                    // If the distributions do not have the same support, they are infinitely distant.
                    if (a._cases.Length != b._cases.Length)
                        return Scalar.Inf;

                    // Check cache
                    var key = (a, b);
                    if (cache.TryGet(key, out ScalarPoly2 res))
                        return res;

                    // Do case-by-case comparison
                    res = ScalarPoly2.Zero;
                    for (int i = 0; i < a._cases.Length; i++)
                    {
                        _Case aCase = a._cases[i];
                        _Case bCase = b._cases[i];
                        if (!aCase.Probability.IsZero)
                        {
                            if (!bCase.Probability.IsZero)
                            {
                                ScalarPoly2 abSqrDist = _getSqrDistance(cache, aCase.Tail, bCase.Tail);
                                if (abSqrDist.C_0 == Scalar.Inf)
                                    goto supportMismatch;
                                res += abSqrDist.Shift(aCase.Probability.Ln - bCase.Probability.Ln);
                            }
                            else
                            {
                                goto supportMismatch;
                            }
                        }
                        else if (!bCase.Probability.IsZero)
                        {
                            goto supportMismatch;
                        }
                    }
                    cache.Add(key, res);
                    return res;

                supportMismatch:
                    // If the supports of the distributions don't match, return an infinite distance
                    res = Scalar.Inf;
                    cache.Add(key, res);
                    return res;
                }
            }

            public static implicit operator RegularIndexListKernel._Internal(_Internal dist)
            {
                return new RegularIndexListKernel._Internal(LogScalar.One, dist);
            }

            public static RegularIndexListKernel._Internal operator *(_Internal dist, LogScalar weight)
            {
                return weight * dist;
            }

            public static RegularIndexListKernel._Internal operator *(LogScalar weight, _Internal dist)
            {
                if (weight.IsZero)
                    return RegularIndexListKernel._Internal.Zero;
                return new RegularIndexListKernel._Internal(weight, dist);
            }

            /// <summary>
            /// Determines whether two <see cref="_Internal"/>s are equivalent without shortcutting based on
            /// reference.
            /// </summary>
            public static bool AreEqual(_Internal a, _Internal b)
            {
                if (a._cases is null)
                    return b._cases is null;
                if (b._cases is null)
                    return false;
                if (a._cases.Length != b._cases.Length)
                    return false;
                for (int i = 0; i < a._cases.Length; i++)
                    if (!_Case.AreEqual(ref a._cases[i], ref b._cases[i]))
                        return false;
                return true;
            }

            public static bool operator ==(_Internal a, _Internal b)
            {
                if (ReferenceEquals(a._cases, b._cases))
                    return true;
                return AreEqual(a, b);
            }

            public static bool operator !=(_Internal a, _Internal b)
            {
                return !(a == b);
            }

            public override bool Equals(object obj)
            {
                if (!(obj is _Internal other))
                    return false;
                return this == other;
            }

            bool IEquatable<_Internal>.Equals(_Internal other)
            {
                return this == other;
            }

            public override int GetHashCode()
            {
                if (_cases is null)
                {
                    return 0;
                }
                else
                {
                    int hash = 0x1a44a85e;
                    for (int i = 0; i < _cases.Length; i++)
                    {
                        _Case @case = _cases[i];
                        hash = HashCodeHelper.Combine(hash, @case.Probability.GetHashCode(), @case.TailHash);
                    }
                    return hash;
                }
            }
        }

        /// <summary>
        /// Describes the contents of an <see cref="RegularIndexListDistribution"/> assuming a particular value for the first item.
        /// </summary>
        internal struct _Case
        {
            internal _Case(LogScalar prob, int tailHash, _Internal tail)
            {
                Probability = prob;
                TailHash = tailHash;
                Tail = tail;
            }

            /// <summary>
            /// Builds a <see cref="_Case"/> with the given probability and tail.
            /// </summary>
            public static _Case Build(LogScalar prob, _Internal tail)
            {
                if (prob.IsZero)
                    return Zero;
                return new _Case(prob, tail.GetHashCode(), tail);
            }

            /// <summary>
            /// The probability associated with this case.
            /// </summary>
            public LogScalar Probability;

            /// <summary>
            /// The hash code for <see cref="Tail"/>.
            /// </summary>
            public int TailHash;

            /// <summary>
            /// Describes the remaining items of the set.
            /// </summary>
            public _Internal Tail;
            
            /// <summary>
            /// The zero-probability <see cref="_Case"/>.
            /// </summary>
            public static _Case Zero => new _Case(LogScalar.Zero, 0, _Internal.PointEmpty);

            /// <summary>
            /// Determines whether two <see cref="_Case"/>s are the same. If so, this may coalesce them to make that
            /// determination faster in the future.
            /// </summary>
            public static bool AreEqual(ref _Case a, ref _Case b)
            {
                if (a.Probability != b.Probability)
                    return false;
                if (ReferenceEquals(a.Tail._cases, b.Tail._cases))
                    return true;
                if (a.TailHash != b.TailHash)
                    return false;
                if (_Internal.AreEqual(a.Tail, b.Tail))
                {
                    Util.Coalesce(ref a.Tail._cases, ref b.Tail._cases);
                    return true;
                }
                return false;
            }
        }

        public static RegularIndexListKernel operator *(RegularIndexListDistribution dist, LogScalar weight)
        {
            return weight * dist;
        }

        public static RegularIndexListKernel operator *(LogScalar weight, RegularIndexListDistribution dist)
        {
            if (weight.IsZero)
                return RegularIndexListKernel.Zero;
            return new RegularIndexListKernel(weight, dist);
        }

        public static explicit operator RegularIndexListDistribution(FiniteDistribution<List<uint>> source)
        {
            return (RegularIndexListDistribution)(RegularIndexListKernel)(FiniteKernel<List<uint>>)source;
        }

        public static bool operator ==(RegularIndexListDistribution a, RegularIndexListDistribution b)
        {
            return a.Length == b.Length && a._source == b._source;
        }

        public static bool operator !=(RegularIndexListDistribution a, RegularIndexListDistribution b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is RegularIndexListDistribution))
                return false;
            return this == (RegularIndexListDistribution)obj;
        }

        bool IEquatable<RegularIndexListDistribution>.Equals(RegularIndexListDistribution other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return _source.GetHashCode();
        }
        
        public override string ToString()
        {
            RegularIndexListSet support = Support;
            if (support.IsSingleton(out List<uint> value))
            {
                return value.ToString();
            }
            else
            {
                uint size = support.Size;
                if (size == uint.MaxValue)
                    return "{[... <" + Length.ToStringInvariant() + ">], ...}";
                else
                    return "{[... <" + Length.ToStringInvariant() + ">], ... <" + size.ToStringInvariant() + ">}";
            }
        }
    }
}
