﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace Alunite.Data.Probability
{
    /// <summary>
    /// A non-negative <see cref="Scalar"/> defined by its natural log. This is ideal for representing very small
    /// quantities, or quantities that are multiplied often, such as those that come up with probabilities.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct LogScalar : IEquatable<LogScalar>
    {
        // Not defined directly in terms of Scalar because the floating point representation is a relevant detail.
        internal float _ln;
        internal LogScalar(float ln)
        {
            _ln = ln;
        }

        public LogScalar(Scalar ln)
        {
            _ln = ln._mean;
        }

        /// <summary>
        /// The natural log of this scalar.
        /// </summary>
        public Scalar Ln => new Scalar(_ln);

        /// <summary>
        /// The reciprocal of this scalar.
        /// </summary>
        public LogScalar Reciprocal => new LogScalar(-_ln);

        /// <summary>
        /// The <see cref="LogScalar"/> representation of 0.
        /// </summary>
        public static LogScalar Zero => new LogScalar(float.NegativeInfinity);

        /// <summary>
        /// The <see cref="LogScalar"/> representation of 1.
        /// </summary>
        public static LogScalar One => new LogScalar(0);

        /// <summary>
        /// Determines whether this is 0.
        /// </summary>
        public bool IsZero => float.IsNegativeInfinity(_ln);

        /// <summary>
        /// Determines whether this is 1.
        /// </summary>
        public bool IsOne => _ln == 0f;

        /// <summary>
        /// Computes the sum of the given set of values.
        /// </summary>
        public static LogScalar Add(params LogScalar[] values)
        {
            return Add(values.AsSpan());
        }

        /// <summary>
        /// Computes the sum of the given set of values.
        /// </summary>
        public static LogScalar Add(ReadOnlySpan<LogScalar> values)
        {
            float maxLn = float.NegativeInfinity;
            for (int i = 0; i < values.Length; i++)
                maxLn = Math.Max(maxLn, values[i]._ln);
            if (float.IsNegativeInfinity(maxLn))
                return Zero;
            float addExp = 0f;
            for (int i = 0; i < values.Length; i++)
                addExp += (float)Math.Exp(values[i]._ln - maxLn);
            return new LogScalar(maxLn + (float)Math.Log(addExp));
        }

        public static LogScalar operator +(LogScalar a, LogScalar b)
        {
            if (a._ln < b._ln)
                return new LogScalar(b._ln + _softplus(a._ln - b._ln));
            if (a.IsZero)
                return Zero;
            return new LogScalar(a._ln + _softplus(b._ln - a._ln));
        }

        public static LogScalar operator -(LogScalar a, LogScalar b)
        {
            Debug.Assert(b._ln <= a._ln);
            if (a.IsZero) return Zero;
            return new LogScalar(a._ln + _softminus(b._ln - a._ln));
        }
        
        /// <summary>
        /// Computes ln(1+x) in the domain x &gt;= 0
        /// </summary>
        internal static float _log1p(float arg)
        {
            // TODO: Handle small values
            return (float)Math.Log(1.0 + arg);
        }

        /// <summary>
        /// Computes ln(1+exp(x)) in the domain x &lt;= 0
        /// </summary>
        internal static float _softplus(float arg)
        {
            if (arg < -17)
                return (float)Math.Exp(arg);
            return (float)Math.Log(1.0 + Math.Exp(arg));
        }

        /// <summary>
        /// Computes ln(1-exp(x)) in the domain x &lt;= 0
        /// </summary>
        internal static float _softminus(float arg)
        {
            if (arg < -17)
                return (float)-Math.Exp(arg);
            return (float)Math.Log(1.0 - Math.Exp(arg));
        }

        public static LogScalar operator *(LogScalar a, LogScalar b)
        {
            return new LogScalar(a.Ln + b.Ln);
        }

        public static LogScalar operator /(LogScalar a, LogScalar b)
        {
            return new LogScalar(a.Ln - b.Ln);
        }

        public static implicit operator Scalar(LogScalar a)
        {
            return Math.Exp(a._ln);
        }

        public static implicit operator LogScalar(Scalar a)
        {
            return new LogScalar(Math.Log(a._mean));
        }
        
        public static implicit operator LogScalar(float source)
        {
            return new LogScalar(Math.Log(source));
        }
        
        public static implicit operator LogScalar(double source)
        {
            return new LogScalar(Math.Log(source));
        }

        public static bool operator ==(LogScalar a, LogScalar b)
        {
            return a.Ln == b.Ln;
        }

        public static bool operator !=(LogScalar a, LogScalar b)
        {
            return !(a.Ln == b.Ln);
        }

        public static bool operator <(LogScalar a, LogScalar b)
        {
            return a.Ln < b.Ln;
        }
        
        public static bool operator >(LogScalar a, LogScalar b)
        {
            return a.Ln > b.Ln;
        }
        
        public static bool operator <=(LogScalar a, LogScalar b)
        {
            return a.Ln <= b.Ln;
        }
        
        public static bool operator >=(LogScalar a, LogScalar b)
        {
            return a.Ln >= b.Ln;
        }

        public override int GetHashCode()
        {
            return Ln.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is LogScalar))
                return false;
            return this == (LogScalar)obj;
        }

        bool IEquatable<LogScalar>.Equals(LogScalar other)
        {
            return this == other;
        }

        public override string ToString()
        {
            if (IsZero) return "0";
            else if (IsOne) return "1";
            else return "exp(" + Ln.ToString() + ")";
        }
    }
}
