﻿using System;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;

namespace Alunite.Data.Probability
{
    /// <summary>
    /// An arbitrary probability distribution over small <see cref="uint"/>s.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct IndexDistribution : 
        IDistribution<uint>, IKernel<uint>,
        IEquatable<IndexDistribution>
    {
        internal LogScalar[] _probs;
        internal IndexDistribution(LogScalar[] probs)
        {
            _probs = probs;
        }

        /// <summary>
        /// Attempts to build an <see cref="IndexDistribution"/> by normalizing the given table of weights. If the
        /// total weight is <see cref="LogScalar.Zero"/>, this will return false.
        /// </summary>
        /// <param name="weight">The total weight of the supplied weights.</param>
        internal static bool _tryBuild(ReadOnlySpan<LogScalar> weights, out LogScalar weight, out IndexDistribution dist)
        {
            uint last = (uint)weights.Length;
            while (last > 0)
            {
                last--;
                if (!weights[(int)last].IsZero)
                    goto foundLast;
            }
            weight = default;
            dist = default;
            return false;

        foundLast:
            uint len = last + 1;
            weight = LogScalar.Add(weights);
            if (weights[(int)last] == weight)
            {
                dist = Point(last);
                return true;
            }
            LogScalar[] nWeights = new LogScalar[len];
            for (int i = 0; i < nWeights.Length; i++)
                nWeights[i] = weights[i] / weight;
            dist = new IndexDistribution(nWeights);
            return true;
        }

        /// <summary>
        /// Constructs an <see cref="IndexDistribution"/> that is uniform for all values less than the given bound.
        /// </summary>
        public static IndexDistribution UniformBelow(uint bound)
        {
            Debug.Assert(bound > 0);
            if (bound < (uint)_uniformBelowTable.Length)
                return _uniformBelowTable[bound];
            LogScalar[] probs = new LogScalar[bound];
            LogScalar prob = ((LogScalar)bound).Reciprocal;
            for (int i = 0; i < probs.Length; i++)
                probs[i] = prob;
            return new IndexDistribution(probs);
        }

        /// <summary>
        /// A table of reusable <see cref="UniformBelow"/> distributions for small bounds.
        /// </summary>
        private static readonly IndexDistribution[] _uniformBelowTable = _buildUniformBelowTable();

        /// <summary>
        /// Builds <see cref="_uniformBelowTable"/>.
        /// </summary>
        private static IndexDistribution[] _buildUniformBelowTable()
        {
            IndexDistribution[] table = new IndexDistribution[16];
            for (int i = 1; i < table.Length; i++)
            {
                LogScalar[] probs = new LogScalar[i];
                LogScalar prob = ((LogScalar)i).Reciprocal;
                for (int j = 0; j < probs.Length; j++)
                    probs[j] = prob;
                table[i] = new IndexDistribution(probs);
            }
            return table;
        }

        /// <summary>
        /// Constructs an <see cref="IndexDistribution"/> whose support contains only the given value.
        /// </summary>
        public static IndexDistribution Point(uint value)
        {
            if (value < (uint)_pointTable.Length)
                return _pointTable[value];
            LogScalar[] probs = new LogScalar[value + 1];
            for (int i = 0; i < probs.Length; i++)
                probs[i] = LogScalar.Zero;
            probs[value] = LogScalar.One;
            return new IndexDistribution(probs);
        }

        /// <summary>
        /// A table of reusable <see cref="Point"/> distributions for small values.
        /// </summary>
        private static readonly IndexDistribution[] _pointTable = _buildUniformPointTable();

        /// <summary>
        /// Builds <see cref="_pointTable"/>.
        /// </summary>
        private static IndexDistribution[] _buildUniformPointTable()
        {
            IndexDistribution[] table = new IndexDistribution[16];
            for (int i = 0; i < table.Length; i++)
            {
                LogScalar[] probs = new LogScalar[i + 1];
                for (int j = 0; j < probs.Length; j++)
                    probs[j] = LogScalar.Zero;
                probs[i] = LogScalar.One;
                table[i] = new IndexDistribution(probs);
            }
            return table;
        }

        /// <summary>
        /// Constructs a distribution which equal probability between the given values.
        /// </summary>
        public static IndexDistribution Uniform(IndexSet support)
        {
            if (support.IsSingleton(out uint value))
                return Point(value);
            if (support.IsBelow(out uint bound))
                return UniformBelow(bound);
            LogScalar[] probs = new LogScalar[support.Bound];
            LogScalar prob = ((LogScalar)probs.Length).Reciprocal;
            for (uint i = 0; i < (uint)probs.Length; i++)
                probs[i] = support.Contains(i) ? prob : LogScalar.Zero;
            return new IndexDistribution(probs);
        }

        /// <summary>
        /// Determines whether this is a point distribution and, if so, gets its one supported value.
        /// </summary>
        public bool IsPoint(out uint value)
        {
            value = (uint)_probs.Length - 1;
            return _probs[value].IsOne;
        }

        /// <summary>
        /// One more than the greatest index in this distribution.
        /// </summary>
        public uint Bound => (uint)_probs.Length;

        /// <summary>
        /// Evaluates this distribution for the given value.
        /// </summary>
        public LogScalar this[uint value]
        {
            get
            {
                if (value < (uint)_probs.Length)
                    return _probs[value];
                return LogScalar.Zero;
            }
        }

        /// <summary>
        /// Samples from this distribution.
        /// </summary>
        public uint Sample(ISampler sampler)
        {
            Scalar sample = sampler.SampleUnit();
            uint last = (uint)_probs.Length - 1;
            for (uint i = 0; i < last; i++)
            {
                Scalar prob = _probs[i];
                if (sample < prob)
                    return i;
                sample -= prob;
            }
            return last;
        }

        /// <summary>
        /// The set of values for which the probability of this distribution is non-zero.
        /// </summary>
        public IndexSet Support
        {
            get
            {
                if (_probs[_probs.Length - 1].IsOne)
                    return IndexSet.Of((uint)(_probs.Length - 1));
                if (_probs.Length <= 8)
                {
                    byte byteBitmap = 0;
                    for (int i = 0; i < _probs.Length; i++)
                        if (!_probs[i].IsZero)
                            byteBitmap |= (byte)(1 << i);
                    return IndexSet.FromBitmap(byteBitmap);
                }
                ulong[] bitmap = new ulong[(_probs.Length + 63) / 64];
                for (uint i = 0; i < (uint)_probs.Length; i++)
                    if (!_probs[i].IsZero)
                        IndexSetBuilder._include(ref bitmap, i);
                return new IndexSet(bitmap);
            }
        }

        /// <summary>
        /// Constructs a variant of this distribution that is optimized for sampling.
        /// </summary>
        public FastFiniteDistribution<uint> Optimize()
        {
            return ((FiniteDistribution<uint>)this).Optimize();
        }

        public static implicit operator IndexDistribution(FiniteDistribution<uint> source)
        {
            return (IndexDistribution)(IndexKernel)(FiniteKernel<uint>)source;
        }

        public static implicit operator FiniteDistribution<uint>(IndexDistribution source)
        {
            return (FiniteDistribution<uint>)(FiniteKernel<uint>)(IndexKernel)source;
        }

        public static implicit operator IndexKernel(IndexDistribution dist)
        {
            return new IndexKernel(dist._probs);
        }

        public static explicit operator IndexDistribution(IndexKernel kernel)
        {
            uint last = (uint)(kernel._weights.Length - 1);
            if (kernel._weights[last].IsOne)
                return Point(last);
            return new IndexDistribution(kernel._weights);
        }

        public static bool operator ==(IndexDistribution a, IndexDistribution b)
        {
            if (a._probs.Length != b._probs.Length)
                return false;
            for (int i = 0; i < a._probs.Length; i++)
                if (a._probs[i] != b._probs[i])
                    return false;
            return true;
        }

        public static bool operator !=(IndexDistribution a, IndexDistribution b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is IndexDistribution))
                return false;
            return this == (IndexDistribution)obj;
        }

        bool IEquatable<IndexDistribution>.Equals(IndexDistribution other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            int hash = 0x09a82702;
            for (int i = 0; i < _probs.Length; i++)
                hash = HashCodeHelper.Combine(hash, _probs[i].GetHashCode());
            return hash;
        }

        public override string ToString()
        {
            return ((IndexKernel)this).ToString();
        }
    }

    /// <summary>
    /// An <see cref="IKernel{T}"/> whose support is small <see cref="uint"/> values.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct IndexKernel : IKernel<uint>
    {
        internal LogScalar[] _weights;
        internal IndexKernel(LogScalar[] weights)
        {
            _weights = weights;
        }

        /// <summary>
        /// An <see cref="IndexKernel"/> whose weight is zero for all values.
        /// </summary>
        public static IndexKernel Zero => new IndexKernel(Array.Empty<LogScalar>());

        /// <summary>
        /// Constructs an <see cref="IndexKernel"/> which is one for all values in the given set and zero otherwise.
        /// </summary>
        public static IndexKernel Indicator(IndexSet set)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Constructs an <see cref="IndexKernel"/> which is one for all values less than the given bound and zero otherwise.
        /// </summary>
        public static IndexKernel IndicatorBelow(uint bound)
        {
            Debug.Assert(bound > 0);
            if (bound < (uint)_indicatorBelowTable.Length)
                return _indicatorBelowTable[bound];
            LogScalar[] weights = new LogScalar[bound];
            for (int j = 0; j < weights.Length; j++)
                weights[j] = LogScalar.One;
            return new IndexKernel(weights);
        }

        /// <summary>
        /// A table of reusable <see cref="IndicatorBelow"/> distributions for small bounds.
        /// </summary>
        private static readonly IndexKernel[] _indicatorBelowTable = _buildIndicatorBelowTable();

        /// <summary>
        /// Builds <see cref="_indicatorBelowTable"/>.
        /// </summary>
        private static IndexKernel[] _buildIndicatorBelowTable()
        {
            IndexKernel[] table = new IndexKernel[16];
            table[0] = Zero;
            for (int i = 1; i < table.Length; i++)
            {
                LogScalar[] weights = new LogScalar[i];
                for (int j = 0; j < weights.Length; j++)
                    weights[j] = LogScalar.One;
                table[i] = new IndexKernel(weights);
            }
            return table;
        }

        /// <summary>
        /// Builds an <see cref="IndexKernel"/> from the given table of weights.
        /// </summary>
        public static IndexKernel Build(ReadOnlySpan<LogScalar> weights)
        {
            uint last = (uint)weights.Length;
            while (last > 0)
            {
                if (!weights[(int)--last].IsZero)
                    goto foundLast;
            }
            return Zero;

        foundLast:
            uint len = last + 1;
            LogScalar[] nWeights = new LogScalar[len];
            for (int i = 0; i < nWeights.Length; i++)
                nWeights[i] = weights[i];
            return new IndexKernel(nWeights);
        }

        /// <summary>
        /// Builds an <see cref="IndexKernel"/> from the given table of weights.
        /// </summary>
        internal static IndexKernel _build(LogScalar[] weights)
        {
            uint last = (uint)weights.Length;
            while (last > 0)
            {
                if (!weights[--last].IsZero)
                    goto foundLast;
            }
            return Zero;

        foundLast:
            uint len = last + 1;
            if (len < (uint)weights.Length)
            {
                LogScalar[] nWeights = new LogScalar[len];
                for (int i = 0; i < nWeights.Length; i++)
                    nWeights[i] = weights[i];
                return new IndexKernel(nWeights);
            }
            else
            {
                return new IndexKernel(weights);
            }
        }

        /// <summary>
        /// The total weight of this kernel.
        /// </summary>
        public LogScalar Weight => LogScalar.Add(_weights);

        /// <summary>
        /// Evaluates this kernel for the given value.
        /// </summary>
        public LogScalar this[uint value]
        {
            get
            {
                if (value < (uint)_weights.Length)
                    return _weights[value];
                return LogScalar.Zero;
            }
        }

        /// <summary>
        /// Attempts to normalize this kernel to get an <see cref="IndexDistribution"/>, returning false
        /// if this is <see cref="Zero"/>.
        /// </summary>
        /// <param name="weight">The weight of this kernel relative to <paramref name="dist"/>.</param>
        public bool TryNormalize(out LogScalar weight, out IndexDistribution dist)
        {
            return IndexDistribution._tryBuild(_weights, out weight, out dist);
        }

        public static implicit operator IndexKernel(FiniteKernel<uint> source)
        {
            // Determine maximum index that appears in the distribution
            uint max = 0;
            foreach (var entry in source)
                max = Math.Max(max, entry.Value);

            // Build weights table
            LogScalar[] weights = new LogScalar[max + 1];
            for (int i = 0; i < weights.Length; i++)
                weights[i] = LogScalar.Zero;
            foreach (var entry in source)
                weights[entry.Value] = entry.Weight;
            return new IndexKernel(weights);
        }

        public static implicit operator FiniteKernel<uint>(IndexKernel source)
        {
            FiniteKernelBuilder<uint> builder = new FiniteKernelBuilder<uint>();
            for (uint i = 0; i < (uint)source._weights.Length; i++)
            {
                LogScalar prob = source._weights[i];
                if (!prob.IsZero)
                    builder.Add(i, prob);
            }
            return builder.Finish();
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.WriteItem('{');
            bool isFirst = true;
            for (uint i = 0; i < (uint)_weights.Length; i++)
            {
                if (!_weights[i].IsZero)
                {
                    if (isFirst)
                        isFirst = false;
                    else
                        str.Write(", ");
                    str.Write(new PointKernel<uint>(i, _weights[i]).ToString());
                }
            }
            str.WriteItem('}');
            return str.ToString();
        }
    }

    /// <summary>
    /// A helper class for constructing a <see cref="IndexKernel"/>.
    /// </summary>
    public sealed class IndexKernelBuilder : IBuilder<IndexKernel>
    {
        private LogScalar[] _weights;
        public IndexKernelBuilder()
        {
            _weights = Array.Empty<LogScalar>();
        }

        /// <summary>
        /// Adds the given weight to the given index value.
        /// </summary>
        public void Add(uint index, LogScalar weight)
        {
            while (index >= _weights.Length)
            {
                LogScalar[] nWeights = new LogScalar[Math.Max(8, _weights.Length * 2)];
                for (int i = 0; i < _weights.Length; i++)
                    nWeights[i] = _weights[i];
                for (int i = _weights.Length; i < nWeights.Length; i++)
                    nWeights[i] = LogScalar.Zero;
                _weights = nWeights;
            }
            _weights[index] += weight;
        }

        /// <summary>
        /// Gets the <see cref="IndexKernel{T}"/> resulting from this builder.
        /// </summary>
        public IndexKernel Finish()
        {
            return IndexKernel._build(_weights);
        }
    }
}
