﻿using System;
using System.Collections.Generic;

namespace Alunite.Data.Probability
{
    /// <summary>
    /// A source of entropy which allows probability distributions to be sampled.
    /// </summary>
    public interface ISampler
    {
        /// <summary>
        /// Samples from a uniform distribution of <see cref="bool"/>s.
        /// </summary>
        bool SampleBit();

        /// <summary>
        /// Samples from a uniform distribution of <see cref="byte"/>s.
        /// </summary>
        byte SampleByte();

        /// <summary>
        /// Samples from a uniform distribution of <see cref="ushort"/>s.
        /// </summary>
        ushort SampleUInt16();

        /// <summary>
        /// Samples from a uniform distribution of <see cref="uint"/>s.
        /// </summary>
        uint SampleUInt32();

        /// <summary>
        /// Samples from a uniform distribution of <see cref="ulong"/>s.
        /// </summary>
        ulong SampleUInt64();
    }
}
