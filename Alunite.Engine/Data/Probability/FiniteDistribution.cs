﻿using System;
using System.Text;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;

namespace Alunite.Data.Probability
{
    /// <summary>
    /// A probability distribution over a finite set of values of type <typeparamref name="T"/>.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct FiniteDistribution<T> : IDistribution<T>, IKernel<T>, IEnumerable<PointKernel<T>>
    {
        private FiniteKernel<T> _kernel;
        private FiniteDistribution(FiniteKernel<T> kernel)
        {
            Debug.Assert(Scalar.Abs((Scalar)kernel.Weight - 1) < 0.0001);
            _kernel = kernel;
        }

        /// <summary>
        /// Gets the probability for the given value in this distribution.
        /// </summary>
        public LogScalar this[T value] => _kernel[value];

        /// <summary>
        /// The set of values which have a non-zero probability under this distribution.
        /// </summary>
        public Set<T> Support => _kernel.Support;

        /// <summary>
        /// The number of values which have a non-zero probability under this distribution.
        /// </summary>
        public uint SupportSize
        {
            get
            {
                uint size = 0;
                foreach (var point in this)
                    size++;
                return size;
            }
        }

        /// <summary>
        /// Samples from this distribution.
        /// </summary>
        public T Sample(ISampler sampler)
        {
            T last = default;
            Scalar sample = sampler.SampleUnit();
            foreach (var point in _kernel)
            {
                Scalar prob = point.Weight;
                if (sample < prob)
                    return point.Value;
                sample -= prob;
                last = point.Value;
            }
            return last;
        }

        /// <summary>
        /// Constructs a variant of this distribution that is optimized for sampling.
        /// </summary>
        public FastFiniteDistribution<T> Optimize()
        {
            return FastFiniteDistribution<T>.Build(this);
        }

        /// <summary>
        /// Gets an enumerator for the components of this distribution.
        /// </summary>
        public FiniteKernel<T>.Enumerator GetEnumerator()
        {
            return new FiniteKernel<T>.Enumerator(_kernel);
        }

        IEnumerator<PointKernel<T>> IEnumerable<PointKernel<T>>.GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public static ScaleFiniteKernel<T> operator *(LogScalar a, FiniteDistribution<T> b)
        {
            return a * (FiniteKernel<T>)b;
        }

        public static ScaleFiniteKernel<T> operator *(FiniteDistribution<T> a, LogScalar b)
        {
            return b * a;
        }

        public static implicit operator FiniteKernel<T>(FiniteDistribution<T> dist)
        {
            return dist._kernel;
        }

        public static explicit operator FiniteDistribution<T>(FiniteKernel<T> kernel)
        {
            return new FiniteDistribution<T>(kernel);
        }

        public static explicit operator FiniteDistribution<T>(ScaleFiniteKernel<T> kernel)
        {
            return new FiniteDistribution<T>(kernel);
        }

        public override string ToString()
        {
            return _kernel.ToString();
        }
    }

    /// <summary>
    /// A <see cref="FiniteDistribution{T}"/> optimized for sampling.
    /// </summary>
    public struct FastFiniteDistribution<T> : IDistribution<T>
    {
        internal FastFiniteDistribution(_Entry[] table)
        {
            _table = table;
        }

        /// <summary>
        /// The lookup table for this distribution. This will always be a power-of-two size. One of the entries will
        /// be selected uniformly, and then one of two values may be taken depending on the entry's pivot.
        /// </summary>
        /// <remarks>This is a variant of the alias method which uses integers and forces a power-of-two size
        /// for efficiency.</remarks>
        internal _Entry[] _table;

        /// <summary>
        /// Builds a <see cref="FastFiniteDistribution{T}"/> for the given distribution.
        /// </summary>
        public static FastFiniteDistribution<T> Build(FiniteDistribution<T> dist)
        {
            // Determine size of table.
            uint numPoints = dist.SupportSize;
            if (numPoints <= 1)
            {
                Debug.Assert(numPoints == 1);
                foreach (var point in dist)
                {
                    _Entry entry = new _Entry(UnlikelyBoolDistribution.AlwaysFalse, default, point.Value);
                    return new FastFiniteDistribution<T>(new[] { entry });
                }
            }
            uint tableSize = Bitwise.UpperPow2(numPoints - 1);
            uint mask = ~(tableSize - 1);

            // Categorize possibilities based on their probability
            LogScalar baseline = ((LogScalar)tableSize).Reciprocal;
            ListBuilder<PointKernel<T>> unlikely = new ListBuilder<PointKernel<T>>();
            ListBuilder<PointKernel<T>> likely = new ListBuilder<PointKernel<T>>();
            foreach (var point in dist)
            {
                if (point.Weight < baseline)
                    unlikely.Push(point);
                else
                    likely.Push(point);
            }

            // Build the table by repeatedly pairing unlikely possibilities with likely possibilities.
            _Entry[] table = new _Entry[tableSize];
            uint index = 0;
            while (index < table.Length)
            {
                if (unlikely.TryPop(out var unlikelyPoint))
                {
                    if (likely.TryPop(out var likelyPoint))
                    {
                        // Add entry which decides between the unlikely point and the unlikely point
                        BoolDistribution useUnlikely = BoolDistribution.FromTrue(unlikelyPoint.Weight / baseline);
                        table[index++] = useUnlikely.IsUnlikely(out UnlikelyBoolDistribution cond) ?
                            new _Entry(cond, unlikelyPoint.Value, likelyPoint.Value) :
                            new _Entry(cond, likelyPoint.Value, unlikelyPoint.Value);

                        // Re-add the remainder of likelyPoint to the pool
                        likelyPoint.Weight = likelyPoint.Weight + unlikelyPoint.Weight - baseline;
                        if (!likelyPoint.Weight.IsZero)
                        {
                            if (likelyPoint.Weight < baseline)
                                unlikely.Push(likelyPoint);
                            else
                                likely.Push(likelyPoint);
                        }
                    }
                    else
                    {
                        // We might end up in this situation if there is a rounding error. All the remaining points should
                        // be roughly uniform
                        table[index++] = new _Entry(UnlikelyBoolDistribution.AlwaysFalse, default, unlikelyPoint.Value);
                    }
                }
                else
                {
                    var likelyPoint = likely.Pop();
                    table[index++] = new _Entry(UnlikelyBoolDistribution.AlwaysFalse, default, likelyPoint.Value);
                    likelyPoint.Weight -= baseline;
                    if (!likelyPoint.Weight.IsZero)
                    {
                        if (likelyPoint.Weight < baseline)
                            unlikely.Push(likelyPoint);
                        else
                            likely.Push(likelyPoint);
                    }
                }
            }
            return new FastFiniteDistribution<T>(table);
        }

        /// <summary>
        /// Samples from this distribution.
        /// </summary>
        public T Sample(ISampler sampler)
        {
            uint sample = sampler.SampleUInt32();
            _Entry entry = _table[Bitwise.ModPow2(sample, (uint)_table.Length)];
            return entry.Condition.Sample(sampler) ? entry.OnTrue : entry.OnFalse;
        }

        /// <summary>
        /// Describes an entry in the lookup table
        /// </summary>
        internal struct _Entry
        {
            public _Entry(UnlikelyBoolDistribution cond, T onTrue, T onFalse)
            {
                Condition = cond;
                OnTrue = onTrue;
                OnFalse = onFalse;
            }

            /// <summary>
            /// Decides whether the value for this entry will come from <see cref="OnFalse"/> or <see cref="OnTrue"/>.
            /// </summary>
            public UnlikelyBoolDistribution Condition;

            /// <summary>
            /// The value taken by the distribution if <see cref="Condition"/> is true.
            /// </summary>
            public T OnTrue;

            /// <summary>
            /// The value taken by the distribution if <see cref="Condition"/> is false.
            /// </summary>
            public T OnFalse;
        }
    }

    /// <summary>
    /// An <see cref="IKernel{T}"/> with finite support.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    [DebuggerTypeProxy(typeof(FiniteKernelDebugView<>))]
    public struct FiniteKernel<T> : IKernel<T>, IEnumerable<PointKernel<T>>
    {
        internal PointKernel<T>[] _entries;
        internal FiniteKernel(PointKernel<T>[] entries)
        {
            _entries = entries;
        }

        /// <summary>
        /// A <see cref="FiniteKernel{T}"/> whose weight is zero for all values.
        /// </summary>
        public static FiniteKernel<T> Zero => new FiniteKernel<T>(_emptyEntries);

        /// <summary>
        /// A table for <see cref="_entries"/> that is always empty.
        /// </summary>
        internal static readonly PointKernel<T>[] _emptyEntries = new[] { PointKernel<T>.Zero };

        /// <summary>
        /// The total weight of this kernel.
        /// </summary>
        public LogScalar Weight
        {
            get
            {
                Scalar maxLn = -Scalar.Inf;
                for (uint i = 0; i < (uint)_entries.Length; i++)
                {
                    Scalar ln = _entries[i].Weight.Ln;
                    if (ln > maxLn) maxLn = ln;
                }
                Scalar addExp = 0;
                for (int i = 0; i < _entries.Length; i++)
                    addExp += Scalar.Exp(_entries[i].Weight.Ln - maxLn);
                return new LogScalar(maxLn + Scalar.Ln(addExp));
            }
        }

        /// <summary>
        /// Evaluates this kernel for the given value.
        /// </summary>
        public LogScalar this[T value]
        {
            get
            {
                int hash = EqualityHelper.GetHashCode(value);
                uint bin = _entries.GetBinPow2(hash);
                while (!_entries[bin].Weight.IsZero)
                {
                    if (EqualityHelper.Equals(value, _entries[bin].Value))
                        return _entries[bin].Weight;
                    bin = _entries.Next(bin);
                }
                return LogScalar.Zero;
            }
        }

        /// <summary>
        /// The set of values which have a non-zero weight under this kernel.
        /// </summary>
        public Set<T> Support
        {
            get
            {
                SetBuilder<T> builder = new SetBuilder<T>();
                foreach (var point in this)
                    builder.Include(point.Value);
                return builder.Finish();
            }
        }

        /// <summary>
        /// Attempts to normalize this kernel to get a <see cref="FiniteDistribution{T}"/>, returning false
        /// if this is <see cref="Zero"/>.
        /// </summary>
        /// <param name="weight">The weight of this kernel relative to <paramref name="dist"/>.</param>
        public bool TryNormalize(out LogScalar weight, out FiniteDistribution<T> dist)
        {
            weight = Weight;
            if (!weight.IsZero)
            {
                dist = (FiniteDistribution<T>)(weight.Reciprocal * this);
                return true;
            }
            dist = default;
            return false;
        }

        /// <summary>
        /// Gets an enumerator for the components of this kernel.
        /// </summary>
        public Enumerator GetEnumerator()
        {
            return new Enumerator(this);
        }

        IEnumerator<PointKernel<T>> IEnumerable<PointKernel<T>>.GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// An enumerator for a <see cref="FiniteKernel{T}"/>
        /// </summary>
        public struct Enumerator : IEnumerator<PointKernel<T>>
        {
            private PointKernel<T>[] _entries;
            private uint _index;
            public Enumerator(FiniteKernel<T> kernel)
            {
                _entries = kernel._entries;
                _index = uint.MaxValue;
            }

            /// <summary>
            /// Gets the current item for this enumerator.
            /// </summary>
            public PointKernel<T> Current => _entries[_index];

            /// <summary>
            /// Advances to the next, or first, item.
            /// </summary>
            public bool MoveNext()
            {
                unchecked
                {
                    while (++_index < (uint)_entries.Length)
                        if (!_entries[_index].Weight.IsZero)
                            return true;
                    return false;
                }
            }

            /// <summary>
            /// Resets the enumerator to before the beginning of the first item.
            /// </summary>
            public void Reset()
            {
                _index = uint.MaxValue;
            }

            object IEnumerator.Current => Current;

            void IDisposable.Dispose() { }
        }

        public static ScaleFiniteKernel<T> operator *(LogScalar a, FiniteKernel<T> b)
        {
            return new ScaleFiniteKernel<T>(a, b);
        }

        public static ScaleFiniteKernel<T> operator *(FiniteKernel<T> a, LogScalar b)
        {
            return b * a;
        }

        public static FiniteKernel<T> operator +(FiniteKernel<T> a, FiniteKernel<T> b)
        {
            return (ScaleFiniteKernel<T>)a + (ScaleFiniteKernel<T>)b;
        }

        public static implicit operator FiniteKernel<T>(PointKernel<T> point)
        {
            return new FiniteKernel<T>(new PointKernel<T>[] { point });
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.WriteItem('{');
            bool isFirst = true;
            foreach (var entry in this)
            {
                if (isFirst)
                    isFirst = false;
                else
                    str.Write(", ");
                str.Write(entry.ToString());
            }
            str.WriteItem('}');
            return str.ToString();
        }
    }

    /// <summary>
    /// A <see cref="FiniteKernel{T}"/> defined in terms of an existing <see cref="FiniteKernel{T}"/> and
    /// a uniform scaling factor.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct ScaleFiniteKernel<T> : IKernel<T>
    {
        public ScaleFiniteKernel(LogScalar scale, FiniteKernel<T> source)
        {
            Scale = scale;
            Source = source;
        }

        /// <summary>
        /// The scaling factor applied to <see cref="Source"/>.
        /// </summary>
        public LogScalar Scale { get; }

        /// <summary>
        /// The <see cref="FiniteKernel{T}"/> on which this is based.
        /// </summary>
        public FiniteKernel<T> Source { get; }

        /// <summary>
        /// The total weight of this kernel.
        /// </summary>
        public LogScalar Weight => Scale * Source.Weight;

        /// <summary>
        /// Evaluates this kernel for the given value.
        /// </summary>
        public LogScalar this[T value] => Scale * Source[value];

        /// <summary>
        /// The set of values which have a non-zero weight under this kernel.
        /// </summary>
        public Set<T> Support
        {
            get
            {
                if (Scale.IsZero)
                    return Set.None<T>();
                return Source.Support;
            }
        }

        public static ScaleFiniteKernel<T> operator +(ScaleFiniteKernel<T> a, ScaleFiniteKernel<T> b)
        {
            // TODO: Performance could probably be improved a bit
            FiniteKernelBuilder<T> builder = new FiniteKernelBuilder<T>();
            foreach (PointKernel<T> aEntry in a.Source)
                builder.Add(aEntry.Value, a.Scale * aEntry.Weight);
            foreach (PointKernel<T> bEntry in b.Source)
                builder.Add(bEntry.Value, b.Scale * bEntry.Weight);
            return builder.Finish();
        }

        public static implicit operator ScaleFiniteKernel<T>(PointKernel<T> point)
        {
            return (FiniteKernel<T>)point;
        }

        public static implicit operator ScaleFiniteKernel<T>(FiniteKernel<T> source)
        {
            return new ScaleFiniteKernel<T>(LogScalar.One, source);
        }

        public static implicit operator FiniteKernel<T>(ScaleFiniteKernel<T> kernel)
        {
            if (kernel.Scale.IsZero || kernel.Source._entries == FiniteKernel<T>._emptyEntries)
                return FiniteKernel<T>.Zero;
            if (kernel.Scale.IsOne)
                return kernel.Source;
            PointKernel<T>[] nEntries = new PointKernel<T>[kernel.Source._entries.Length];
            for (uint i = 0; i < (uint)kernel.Source._entries.Length; i++)
                nEntries[i] = kernel.Scale * kernel.Source._entries[i];
            return new FiniteKernel<T>(nEntries);
        }

        public override string ToString()
        {
            return ((FiniteKernel<T>)this).ToString();
        }
    }

    /// <summary>
    /// A helper for constructing a <see cref="FiniteKernel{T}"/>.
    /// </summary>
    public sealed class FiniteKernelBuilder<T> : IBuilder<FiniteKernel<T>>
    {
        private PointKernel<T>[] _entries;
        private uint _capacity;
        internal FiniteKernelBuilder(PointKernel<T>[] entries)
        {
            _entries = entries;
            _capacity = 0;
        }

        public FiniteKernelBuilder()
        {
            _entries = FiniteKernel<T>._emptyEntries;
            _capacity = 0;
        }

        /// <summary>
        /// Adds the given weight to the given value.
        /// </summary>
        public void Add(T value, LogScalar weight)
        {
            // Check for existing entry
            int hash = EqualityHelper.GetHashCode(value);
        retry:
            uint bin = _entries.GetBinPow2(hash);
            while (!_entries[bin].Weight.IsZero)
            {
                if (EqualityHelper.Equals(value, _entries[bin].Value))
                {
                    _entries[bin].Weight += weight;
                    return;
                }
                bin = _entries.Next(bin);
            }

            // Add new entry
            if (_capacity == 0)
            {
                _expand();
                goto retry;
            }
            else
            {
                _capacity--;
                _entries[bin] = new PointKernel<T>(value, weight);
            }
        }

        /// <summary>
        /// Enlarges <see cref="_entries"/> in order to accommodate more entries.
        /// </summary>
        private void _expand()
        {
            PointKernel<T>[] nEntries = new PointKernel<T>[Math.Max(8, _entries.Length * 2)];
            for (int i = 0; i < nEntries.Length; i++)
                nEntries[i].Weight = LogScalar.Zero;
            _capacity = _getCapacity(nEntries.Length);
            for (uint i = 0; i < (uint)_entries.Length; i++)
            {
                if (!_entries[i].Weight.IsZero)
                {
                    int hash = EqualityHelper.GetHashCode(_entries[i].Value);
                    uint bin = nEntries.GetBinPow2(hash);
                    while (!nEntries[bin].Weight.IsZero)
                        bin = nEntries.Next(bin);
                    nEntries[bin] = _entries[i];
                    _capacity--;
                }
            }
            _entries = nEntries;
        }

        /// <summary>
        /// Gets the capacity of this builder when <see cref="_entries"/> is the given size.
        /// </summary>
        private static uint _getCapacity(int size)
        {
            return 3 + (uint)size * 3 / 5;
        }

        /// <summary>
        /// Gets the <see cref="FiniteKernel{T}"/> resulting from this builder.
        /// </summary>
        public FiniteKernel<T> Finish()
        {
            return new FiniteKernel<T>(_entries);
        }
    }

    /// <summary>
    /// A debugger type proxy for <see cref="FiniteKernel{T}"/>s.
    /// </summary>
    public sealed class FiniteKernelDebugView<T>
    {
        private readonly FiniteKernel<T> _source;
        public FiniteKernelDebugView(FiniteKernel<T> source)
        {
            _source = source;
        }

        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public PointKernel<T>[] Cases => System.Linq.Enumerable.ToArray(_source);
    }
}
