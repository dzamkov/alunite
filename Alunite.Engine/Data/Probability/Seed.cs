﻿using System;
using System.Collections.Generic;

namespace Alunite.Data.Probability
{
    /// <summary>
    /// A store of entropy that can be be used to generate pseudo-random numbers based on supplied
    /// data. This is deterministic in that the same sequence of operations will yield the same results,
    /// across program runs and between machines. This is not intended for cryptographic applications.
    /// </summary>
    public struct Seed
    {
        // TODO: Can we make this shorter? 16 bytes is quite hefty
        private ulong _a;
        private ulong _b;

        /// <summary>
        /// The canonical initial configuration of a <see cref="Seed"/>.
        /// </summary>
        public static Seed Initial => new Seed
        {
            _a = 0x428a2f9871374491ul,
            _b = 0xb5c0fbcfe9b5dba5ul
        };

        /// <summary>
        /// Applies a permutation to the given seed, deterministically altering the results of future operations.
        /// </summary>
        public static void Stir(ref Seed seed)
        {
            // From http://vigna.di.unimi.it/xorshift/xoroshiro128plus.c
            seed._b ^= seed._a;
            seed._a = Bitwise.RotateLeft(seed._a, 24) ^ seed._b ^ (seed._b << 16);
            seed._b = Bitwise.RotateLeft(seed._b, 37);
        }

        /// <summary>
        /// Contributes a 16-bit value to the entropy of a seed.
        /// </summary>
        public static void AbsorbUInt16(ref Seed seed, ushort value)
        {
            AbsorbInt64(ref seed, value);
        }

        /// <summary>
        /// Contributes a 32-bit value to the entropy of a seed.
        /// </summary>
        public static void AbsorbUInt32(ref Seed seed, uint value)
        {
            AbsorbInt64(ref seed, value);
        }

        /// <summary>
        /// Contributes a 64-bit value to the entropy of a seed.
        /// </summary>
        public static void AbsorbUInt64(ref Seed seed, ulong value)
        {
            unchecked
            {
                seed._b += value;
                value += value << 1;
                value ^= value << 5;
                value += value << 13;
                value ^= value << 31;
                seed._a ^= value;
            }
            Stir(ref seed);
        }

        /// <summary>
        /// Contributes a 16-bit value to the entropy of a seed.
        /// </summary>
        public static void AbsorbInt16(ref Seed seed, short value)
        {
            AbsorbUInt16(ref seed, unchecked((ushort)value));
        }

        /// <summary>
        /// Contributes a 32-bit value to the entropy of a seed.
        /// </summary>
        public static void AbsorbInt32(ref Seed seed, int value)
        {
            AbsorbUInt32(ref seed, unchecked((uint)value));
        }

        /// <summary>
        /// Contributes a 64-bit value to the entropy of a seed.
        /// </summary>
        public static void AbsorbInt64(ref Seed seed, long value)
        {
            AbsorbUInt64(ref seed, unchecked((ulong)value));
        }
        
        /// <summary>
        /// Samples from a uniform distribution of <see cref="bool"/>s using a seed.
        /// </summary>
        public static bool SampleBit(ref Seed seed)
        {
            return (SampleUInt64(ref seed) >> 63) != 0;
        }

        /// <summary>
        /// Samples from a uniform distribution of <see cref="byte"/>s using a seed.
        /// </summary>
        public static byte SampleByte(ref Seed seed)
        {
            return unchecked((byte)(SampleUInt64(ref seed) >> 56));
        }

        /// <summary>
        /// Samples from a uniform distribution of <see cref="ushort"/>s using a seed.
        /// </summary>
        public static ushort SampleUInt16(ref Seed seed)
        {
            return unchecked((ushort)(SampleUInt64(ref seed) >> 48));
        }

        /// <summary>
        /// Samples from a uniform distribution of <see cref="uint"/>s using a seed.
        /// </summary>
        public static uint SampleUInt32(ref Seed seed)
        {
            return unchecked((uint)(SampleUInt64(ref seed) >> 32));
        }

        /// <summary>
        /// Samples from a uniform distribution of <see cref="ulong"/>s using a seed.
        /// </summary>
        public static ulong SampleUInt64(ref Seed seed)
        {
            ulong res = unchecked(seed._a + seed._b);
            Stir(ref seed);
            return res;
        }
    }
    
    /// <summary>
    /// A sampler which uses entropy from a <see cref="Seed"/>.
    /// </summary>
    public sealed class SeedSampler : ISampler
    {
        public SeedSampler(Seed seed)
        {
            Seed = seed;
        }

        /// <summary>
        /// The current seed for this sampler.
        /// </summary>
        public Seed Seed;

        /// <summary>
        /// Samples from a uniform distribution of <see cref="bool"/>s.
        /// </summary>
        public bool SampleBit()
        {
            return Seed.SampleBit(ref Seed);
        }

        /// <summary>
        /// Samples from a uniform distribution of <see cref="byte"/>s.
        /// </summary>
        public byte SampleByte()
        {
            return Seed.SampleByte(ref Seed);
        }

        /// <summary>
        /// Samples from a uniform distribution of <see cref="ushort"/>s.
        /// </summary>
        public ushort SampleUInt16()
        {
            return Seed.SampleUInt16(ref Seed);
        }

        /// <summary>
        /// Samples from a uniform distribution of <see cref="uint"/>s.
        /// </summary>
        public uint SampleUInt32()
        {
            return Seed.SampleUInt32(ref Seed);
        }

        /// <summary>
        /// Samples from a uniform distribution of <see cref="ulong"/>s.
        /// </summary>
        public ulong SampleUInt64()
        {
            return Seed.SampleUInt64(ref Seed);
        }
    }
}
