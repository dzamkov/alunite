﻿using System;
using System.Collections.Generic;

namespace Alunite.Data
{
    /// <summary>
    /// Contains useful functions for implementing data structures within arrays.
    /// </summary>
    public static class Table
    {
        /// <summary>
        /// Gets the next index in a table, with wrap-around.
        /// </summary>
        public static uint Next<T>(this T[] table, uint index)
        {
            index++;
            if (index == (uint)table.Length)
                index = 0;
            return index;
        }

        /// <summary>
        /// Gets the previous index in a table, with wrap-around.
        /// </summary>
        public static uint Prev<T>(this T[] table, uint index)
        {
            if (index == 0)
                index = (uint)table.Length;
            index--;
            return index;
        }

        /// <summary>
        /// Gets the bin corresponding to the given hash in a hashtable with a power-of-2 size.
        /// </summary>
        public static uint GetBinPow2<T>(this T[] table, int hash)
        {
            hash ^= hash << 16;
            hash += hash >> 11;
            hash += hash << 25;
            hash ^= hash >> 17;
            hash += hash << 1;
            return Bitwise.ModPow2(hash, (uint)table.Length);
        }

        /// <summary>
        /// Gets the smallest power of two that is at least the given value. This is useful for
        /// creating a power-of-2 size table.
        /// </summary>
        public static int UpperPow2(int num)
        {
            return (int)Bitwise.UpperPow2((uint)num);
        }

        /// <summary>
        /// Gets the smallest power of two that is at least the given value. This is useful for
        /// creating a power-of-2 size table.
        /// </summary>
        public static int UpperPow2(uint num)
        {
            return (int)Bitwise.UpperPow2(num);
        }

        /// <summary>
        /// Ensures that the given table has at least the given amount of space after the given next free item,
        /// resizing if needed.
        /// </summary>
        public static void Allocate<T>(ref T[] table, uint end, uint amount)
        {
            if (end + amount > (uint)table.Length)
            {
                T[] nBuffer = new T[Bitwise.UpperPow2(end + amount)];
                Array.Copy(table, nBuffer, end);
                table = nBuffer;
            }
        }

        /// <summary>
        /// Ensures that the given table is at least the given size, resizing if needed.
        /// </summary>
        public static void Allocate<T>(ref T[] table, uint size)
        {
            if ((uint)table.Length < size)
            {
                T[] nBuffer = new T[Bitwise.UpperPow2(size)];
                Array.Copy(table, nBuffer, table.Length);
                table = nBuffer;
            }
        }

        /// <summary>
        /// Copies the data from the given source span to the given destination span. This will
        /// work even if the buffers overlap.
        /// </summary>
        public static void Copy<T>(ReadOnlySpan<T> src, Span<T> dst)
        {
            // TODO: Avoid overwriting
            // TODO: There's probably a faster way to do this
            if (src.Length != dst.Length)
                throw new ArgumentException("Buffer size mismatch");
            for (int i = 0; i < src.Length; i++)
                dst[i] = src[i];
        }
    }
}
