﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Alunite.Data
{
    /// <summary>
    /// A fixed-size cache data structure intended for short-lived work on a single thread.
    /// </summary>
    public sealed class TempCache<TKey, TValue>
    {
        // TODO: Consider pooling for this class
        private ushort[] _hashtable;
        private _Entry[] _entries;
        private ushort _mostRecent;
        public TempCache(IEqualityComparer<TKey> keyComparer, ushort capacity)
        {
            KeyComparer = keyComparer;

            // Initialize hashtable
            _hashtable = new ushort[Bitwise.UpperPow2((uint)capacity * 5 / 3)]; // 60% load factor
            for (int i = 0; i < _hashtable.Length; i++)
                _hashtable[i] = ushort.MaxValue;

            // Initialize entries
            _entries = new _Entry[capacity];
            _mostRecent = (ushort)(capacity - 1);
            for (ushort i = 1; i < capacity; i++)
            {
                _entries[i].Next = (ushort)(i - 1);
                _entries[i - 1].Prev = i;
            }
            _entries[0].Next = _mostRecent;
            _entries[_mostRecent].Prev = 0;
        }

        public TempCache(ushort capacity)
            : this(EqualityHelper.Comparer<TKey>(), capacity)
        { }

        /// <summary>
        /// The <see cref="IEqualityComparer{T}"/> used to compare keys.
        /// </summary>
        public IEqualityComparer<TKey> KeyComparer { get; }

        /// <summary>
        /// The maximum number of entries this cache can hold.
        /// </summary>
        public ushort Capacity => (ushort)_entries.Length;

        /// <summary>
        /// Tries getting the value associated with the given key, returning false if there is no such entry in
        /// the cache.
        /// </summary>
        public bool TryGet(TKey key, out TValue value)
        {
            if (_search(key, out uint startBin, out ushort testEntry))
            {
                // Promote and return entry
                ref _Entry entry = ref _entries[testEntry];
                entry.IncrementPopularity();
                _promote(testEntry);
                value = entry.Value;
                return true;
            }
            value = default(TValue);
            return false;
        }

        /// <summary>
        /// Adds an entry for the given key.
        /// </summary>
        public void Add(TKey key, TValue value)
        {
            Update(key, out bool isNew) = value;
            Debug.Assert(isNew);
        }

        /// <summary>
        /// Adds or updates an entry for the given key.
        /// </summary>
        public void Update(TKey key, TValue value)
        {
            Update(key, out bool isNew) = value;
        }

        /// <summary>
        /// Gets a reference to the value associated to the given key. The reference is valid until the next call
        /// to the cache.
        /// </summary>
        /// <param name="isAdd">Indicates whether a new space for the value has just
        /// been allocated. If so, the value will need to be populated prior to use</param>
        public ref TValue Update(TKey key, out bool isAdd)
        {
            // Locate entry using hash table
            if (_search(key, out uint startBin, out ushort testEntry))
            {
                // Promote and return entry
                ref _Entry entry = ref _entries[testEntry];
                entry.IncrementPopularity();
                _promote(testEntry);
                isAdd = false;
                return ref entry.Value;
            }

            // Allocate new entry
            {
                testEntry = _allocate();

                // Bins might have moved around during eviction
                uint bin = startBin;
                while (_hashtable[bin] != ushort.MaxValue)
                    Hashtable.Traverse(_hashtable.Length, ref bin, 1);
                _hashtable[bin] = testEntry;
                ref _Entry entry = ref _entries[testEntry];
                entry.Popularity = 1;
                entry.Key = key;
                isAdd = true;
                return ref entry.Value;
            }
        }

        /// <summary>
        /// Searches for an entry with the given key.
        /// </summary>
        private bool _search(TKey key, out uint startBin, out ushort entry)
        {
            int hash = KeyComparer.GetHashCode(key);
            startBin = Hashtable.GetBinPow2(_hashtable.Length, hash);
            uint bin = startBin;
            while ((entry = _hashtable[bin]) != ushort.MaxValue)
            {
                if (KeyComparer.Equals(key, _entries[entry].Key))
                    return true;
                else
                    Hashtable.Traverse(_hashtable.Length, ref bin, 1);
            }
            return false;
        }

        /// <summary>
        /// Promotes the given entry to the beginning of the most recently used queue.
        /// </summary>
        private void _promote(ushort entry)
        {
            if (entry != _mostRecent)
            {
                // Remove from current place
                {
                    var next = _entries[entry].Next;
                    var prev = _entries[entry].Prev;
                    Debug.Assert(_entries[next].Prev == entry);
                    Debug.Assert(_entries[prev].Next == entry);
                    _entries[next].Prev = prev;
                    _entries[prev].Next = next;
                }

                // Re-add to beginning
                {
                    var next = _mostRecent;
                    var prev = _entries[next].Prev;
                    _entries[prev].Next = entry;
                    _entries[next].Prev = entry;
                    _entries[entry].Next = next;
                    _entries[entry].Prev = prev;
                    _mostRecent = entry;
                }
            }
        }

        /// <summary>
        /// Allocates space for a new entry, possibly by evicting an existing entry. This will
        /// also promote the entry to the beginning of the most recently used queue.
        /// </summary>
        private ushort _allocate()
        {
        retry:
            ushort leastRecent = _leastRecent;
            ref _Entry entry = ref _entries[leastRecent];
            if (entry.Popularity > 1)
            {
                // Halve popularity and repromote
                entry.Popularity >>= 1;
                _mostRecent = leastRecent;
                goto retry;
            }
            else
            {
                // Remove from hashtable
                if (entry.Popularity > 0)
                {
                    int hash = KeyComparer.GetHashCode(entry.Key);
                    uint bin = Hashtable.GetBinPow2(_hashtable.Length, hash);
                    while (_hashtable[bin] != leastRecent)
                    {
                        Debug.Assert(_hashtable[bin] != ushort.MaxValue);
                        Hashtable.Traverse(_hashtable.Length, ref bin, 1);
                    }
                    _hashtable[bin] = ushort.MaxValue;

                fix:
                    // Since this is open addressing, we need to make sure not to break a chain
                    uint nBin = bin;
                    while (true)
                    {
                        Hashtable.Traverse(_hashtable.Length, ref nBin, 1);
                        if (_hashtable[nBin] == ushort.MaxValue)
                            break;
                        int nHash = KeyComparer.GetHashCode(_entries[_hashtable[nBin]].Key);
                        uint ntBin = Hashtable.GetBinPow2(_hashtable.Length, nHash);
                        if (!Hashtable.BetweenMod(bin, ntBin, nBin))
                        {
                            _hashtable[bin] = _hashtable[nBin];
                            _hashtable[nBin] = ushort.MaxValue;
                            bin = nBin;
                            goto fix;
                        }
                    }
                }

                // Repromote and return
                _mostRecent = leastRecent;
                return leastRecent;
            }
        }

        /// <summary>
        /// Gets or sets the index of the least recently used entry.
        /// </summary>
        private ushort _leastRecent
        {
            get
            {
                return _entries[_mostRecent].Prev;
            }
            set
            {
                _entries[_mostRecent].Prev = value;
            }
        }

        /// <summary>
        /// Describes an entry in the cache.
        /// </summary>
        private struct _Entry
        {
            /// <summary>
            /// Identifies the next most recently used entry (wrapping around to the end after reaching the beginning).
            /// </summary>
            public ushort Prev;

            /// <summary>
            /// Identifies the next least recently used entry (wrapping around to the beginning after reaching the end).
            /// </summary>
            public ushort Next;

            /// <summary>
            /// A measure of the popularity of this entry. This increments each time the entry is accessed. If this
            /// is greated than 1 when the entry reaches the end of the recently used queue, it will halve and the
            /// entry will be placed back at the start of the queue. This prevents popular entries from being
            /// prematurely evicted, while still allowing them to be cleaned up after a long period of disuse
            /// (proportional to the log of the maximum number of uses).
            /// </summary>
            public ushort Popularity;

            /// <summary>
            /// The key for the entry.
            /// </summary>
            public TKey Key;

            /// <summary>
            /// The value for the entry.
            /// </summary>
            public TValue Value;

            /// <summary>
            /// Increments <see cref="Popularity"/>, saturating instead of wrapping around on overflow.
            /// </summary>
            public void IncrementPopularity()
            {
                Popularity = unchecked((ushort)(Popularity + 1));
                if (Popularity == 0) Popularity = ushort.MaxValue;
            }
        }
    }
}
