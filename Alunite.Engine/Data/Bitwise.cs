﻿using System;
using System.Collections.Generic;

namespace Alunite.Data
{
    /// <summary>
    /// Contains a set of useful bit manipulations.
    /// </summary>
    public static class Bitwise
    {
        /// <summary>
        /// Gets the value of an individual bit in the given <see cref="byte"/>.
        /// </summary>
        public static bool Get(byte num, uint bit)
        {
            return (num & (1 << (int)bit)) != 0;
        }

        /// <summary>
        /// Gets the value of an individual bit in the given <see cref="ushort"/>.
        /// </summary>
        public static bool Get(ushort num, uint bit)
        {
            return (num & (1 << (int)bit)) != 0;
        }

        /// <summary>
        /// Gets the value of an individual bit in the given <see cref="uint"/>.
        /// </summary>
        public static bool Get(uint num, uint bit)
        {
            return (num & (1u << (int)bit)) != 0;
        }

        /// <summary>
        /// Gets the value of an individual bit in the given <see cref="ulong"/>.
        /// </summary>
        public static bool Get(ulong num, uint bit)
        {
            return (num & (1ul << (int)bit)) != 0;
        }

        /// <summary>
        /// Sets the value of an individual bit in the given <see cref="byte"/>.
        /// </summary>
        public static void Set(ref byte num, uint bit, bool value)
        {
            // TODO: Avoid branching using mask
            num &= (byte)~(1 << (int)bit);
            if (value) num |= (byte)(1 << (int)bit);
        }

        /// <summary>
        /// Sets the value of an individual bit in the given <see cref="ushort"/>.
        /// </summary>
        public static void Set(ref ushort num, uint bit, bool value)
        {
            // TODO: Avoid branching using mask
            num &= (ushort)~(1 << (int)bit);
            if (value) num |= (ushort)(1 << (int)bit);
        }

        /// <summary>
        /// Sets the value of an individual bit in the given <see cref="uint"/>.
        /// </summary>
        public static void Set(ref uint num, uint bit, bool value)
        {
            // TODO: Avoid branching using mask
            num &= ~(1u << (int)bit);
            if (value) num |= (1u << (int)bit);
        }

        /// <summary>
        /// Sets the value of an individual bit in the given <see cref="ulong"/>.
        /// </summary>
        public static void Set(ref ulong num, uint bit, bool value)
        {
            // TODO: Avoid branching using mask
            num &= ~(1ul << (int)bit);
            if (value) num |= (1ul << (int)bit);
        }

        /// <summary>
        /// Finds the index of the least significant zero bit in <paramref name="num"/>, or returns 64
        /// if no such index exists.
        /// </summary>
        public static uint FindFirstZero(ulong num)
        {
            return FindFirstOne(~num);
        }

        /// <summary>
        /// Finds the index of the least significant one bit in <paramref name="num"/>, or returns 64
        /// if no such index exists.
        /// </summary>
        public static uint FindFirstOne(ulong num)
        {
            // TODO: Use intrinsic
            return 64 - Count(SmearLeft(num));
        }

        /// <summary>
        /// Finds the index of the most significant one bit in <paramref name="num"/>, or returns
        /// false if the input is entirely zeros.
        /// </summary>
        public static bool TryFindLastOne(ulong num, out uint index)
        {
            index = Count(SmearRight(num));
            bool res = index > 0;
            index = unchecked(index - 1);
            return res;
        }

        /// <summary>
        /// Creates a mask of the bits to the left of (and including) the rightmost 1 in the given input.
        /// </summary>
        public static ulong SmearLeft(ulong num)
        {
            num |= (num << 32);
            num |= (num << 16);
            num |= (num << 8);
            num |= (num << 4);
            num |= (num << 2);
            num |= (num << 1);
            return num;
        }

        /// <summary>
        /// Creates a mask of the bits to the right of (and including) the leftmost 1 in the given input.
        /// </summary>
        public static ulong SmearRight(ulong num)
        {
            num |= (num >> 32);
            num |= (num >> 16);
            num |= (num >> 8);
            num |= (num >> 4);
            num |= (num >> 2);
            num |= (num >> 1);
            return num;
        }

        /// <summary>
        /// Constructs an integer with the first <paramref name="count"/> bits set to 1.
        /// </summary>
        public static ulong MaskFirst(uint count)
        {
            if (count == 64)
                return ulong.MaxValue;
            return unchecked(((ulong)1 << (int)count) - 1);
        }

        /// <summary>
        /// Counts the number of 1 bits in <paramref name="num"/>.
        /// </summary>
        public static uint Count(ulong num)
        {
            // TODO: Use intrinsic
            num = ((num & 0xAAAAAAAAAAAAAAAA) >> 1) + (num & 0x5555555555555555);
            num = ((num & 0xCCCCCCCCCCCCCCCC) >> 2) + (num & 0x3333333333333333);
            num = ((num & 0xF0F0F0F0F0F0F0F0) >> 4) + (num & 0x0F0F0F0F0F0F0F0F);
            num = ((num & 0xFF00FF00FF00FF00) >> 8) + (num & 0x00FF00FF00FF00FF);
            num = ((num & 0xFFFF0000FFFF0000) >> 16) + (num & 0x0000FFFF0000FFFF);
            num = ((num & 0xFFFFFFFF00000000) >> 32) + (num & 0x00000000FFFFFFFF);
            return (uint)num;
        }

        /// <summary>
        /// Counts the number of 1 bits in <paramref name="num"/>.
        /// </summary>
        public static uint Count(uint num)
        {
            // TODO: Use intrinsic
            num = ((num & 0xAAAAAAAA) >> 1) + (num & 0x55555555);
            num = ((num & 0xCCCCCCCC) >> 2) + (num & 0x33333333);
            num = ((num & 0xF0F0F0F0) >> 4) + (num & 0x0F0F0F0F);
            num = ((num & 0xFF00FF00) >> 8) + (num & 0x00FF00FF);
            num = ((num & 0xFFFF0000) >> 16) + (num & 0x0000FFFF);
            return num;
        }

        /// <summary>
        /// Counts the number of 1 bits in <paramref name="num"/>.
        /// </summary>
        public static uint Count(ushort num)
        {
            // TODO: Use intrinsic
            uint inum = num;
            inum = ((inum & 0xAAAA) >> 1) + (inum & 0x5555);
            inum = ((inum & 0xCCCC) >> 2) + (inum & 0x3333);
            inum = ((inum & 0xF0F0) >> 4) + (inum & 0x0F0F);
            inum = ((inum & 0xFF00) >> 8) + (inum & 0x00FF);
            return inum;
        }

        /// <summary>
        /// Left-rotates the bits in <paramref name="num"/>
        /// </summary>
        public static ulong RotateLeft(ulong num, int amount)
        {
            return (num << amount) | (num >> (64 - amount));
        }

        /// <summary>
        /// Left-rotates the bits in <paramref name="num"/>
        /// </summary>
        public static uint RotateLeft(uint num, int amount)
        {
            return (num << amount) | (num >> (32 - amount));
        }

        /// <summary>
        /// Transposes the 8x8 bit matrix represented by the given 64 bit number.
        /// </summary>
        public static ulong Transpose8(ulong num)
        {
            // From https://www.hackersdelight.org/hdcodetxt/transpose8.c.txt
            ulong x = num;
            ulong t;
            t = (x ^ (x >> 7)) & 0x00AA00AA00AA00AAul;
            x = x ^ t ^ (t << 7);
            t = (x ^ (x >> 14)) & 0x0000CCCC0000CCCCul;
            x = x ^ t ^ (t << 14);
            t = (x ^ (x >> 28)) & 0x00000000F0F0F0F0ul;
            x = x ^ t ^ (t << 28);
            return x;
        }
        
        /// <summary>
        /// Reverses the groups of 4 bits in the given number.
        /// </summary>
        public static ulong Reverse4(ulong num)
        {
            num = ((num & 0xF0F0F0F0F0F0F0F0) >> 4) | ((num & 0x0F0F0F0F0F0F0F0F) << 4);
            num = ((num & 0xFF00FF00FF00FF00) >> 8) | ((num & 0x00FF00FF00FF00FF) << 8);
            num = ((num & 0xFFFF0000FFFF0000) >> 16) | ((num & 0x0000FFFF0000FFFF) << 16);
            num = ((num & 0xFFFFFFFF00000000) >> 32) | ((num & 0x00000000FFFFFFFF) << 32);
            return num;
        }

        /// <summary>
        /// Expands out the bits in <paramref name="num"/> by inserting 3 zero bits between each of them.
        /// </summary>
        public static ulong Expand4(ushort num)
        {
            ulong res = num;            // 000000000000000000000000000000000000000000000000abcdefghijklmnop
            res |= (res << 24);         // 000000000000000000000000abcdefghijklmnop00000000abcdefghijklmnop
            res |= (res << 12);         // 000000000000abcdefghijklXXXXefghijklXXXXefghijklXXXXefghijklmnop
            res &= 0x000F000F000F000F;  // 000000000000abcd000000000000efgh000000000000ijkl000000000000mnop
            res |= (res << 6);          // 000000abcd00abcd000000efgh00efgh000000ijkl00ijkl000000mnop00mnop
            res |= (res << 3);          // 000abcXbcXbcXbcd000efgXfgXfgXfgh000ijkXjkXjkXjkl000mnoXnoXnoXnop
            res &= 0x1111111111111111;  // 000a000b000c000d000e000f000g000h000i000j000k000l000m000n000o000p
            return res;
        }

        /// <summary>
        /// The inverse of <see cref="Expand4"/>. Concatenates every 4th bit from the given input.
        /// </summary>
        public static ushort Contract4(ulong num)
        {
            num &= 0x1111111111111111;
            num |= (num >> 3);
            num |= (num >> 6);
            num &= 0x000F000F000F000F;
            num |= (num >> 12);
            num |= (num >> 24);
            return unchecked((ushort)num);
        }

        /// <summary>
        /// Reveres the bytes in the given number.
        /// </summary>
        public static ushort Reverse8(ushort num)
        {
            num = (ushort)(((num & 0xFF00) >> 8) | ((num & 0x00FF) << 8));
            return num;
        }

        /// <summary>
        /// Reveres the bytes in the given number.
        /// </summary>
        public static uint Reverse8(uint num)
        {
            num = ((num & 0xFF00FF00) >> 8) | ((num & 0x00FF00FF) << 8);
            num = ((num & 0xFFFF0000) >> 16) | ((num & 0x0000FFFF) << 16);
            return num;
        }

        /// <summary>
        /// Reveres the bytes in the given number.
        /// </summary>
        public static ulong Reverse8(ulong num)
        {
            num = ((num & 0xFF00FF00FF00FF00) >> 8) | ((num & 0x00FF00FF00FF00FF) << 8);
            num = ((num & 0xFFFF0000FFFF0000) >> 16) | ((num & 0x0000FFFF0000FFFF) << 16);
            num = ((num & 0xFFFFFFFF00000000) >> 32) | ((num & 0x00000000FFFFFFFF) << 32);
            return num;
        }

        /// <summary>
        /// Rounds a number up to the next power of two.
        /// </summary>
        public static uint UpperPow2(uint num)
        {
            num--;
            num |= num >> 1;
            num |= num >> 2;
            num |= num >> 4;
            num |= num >> 8;
            num |= num >> 16;
            num++;
            return num;
        }

        /// <summary>
        /// Gets the log base-2 of the given power of two.
        /// </summary>
        public static uint Log2(uint num)
        {
            // TODO: Hardware intrinsic
            return _log2Table[unchecked(num * 0b00000111110001001010110011011101u) >> 27];
        }

        /// <summary>
        /// De Bruijn table used to implement <see cref="Log2(uint)"/>.
        /// </summary>
        private static byte[] _log2Table = new byte[]
        {
            0, 1, 10, 2, 11, 14, 22, 3, 30, 12, 15, 17, 19, 23, 26, 4,
            31, 9, 13, 21, 29, 16, 18, 25, 8, 20, 28, 24, 7, 27, 6, 5
        };

        /// <summary>
        /// Computes <paramref name="a"/> mod <paramref name="n"/> assuming that <paramref name="n"/> is a power
        /// of two.
        /// </summary>
        public static uint ModPow2(int a, uint n)
        {
            return (uint)(a & ((int)n - 1));
        }

        /// <summary>
        /// Computes <paramref name="a"/> mod <paramref name="n"/> assuming that <paramref name="n"/> is a power
        /// of two.
        /// </summary>
        public static uint ModPow2(uint a, uint n)
        {
            return a & (n - 1);
        }

        /// <summary>
        /// Rounds <paramref name="n"/> up to the next multiple of <paramref name="b"/>, which must be a power
        /// of two.
        /// </summary>
        public static ulong AlignUp(ulong n, uint b)
        {
            return unchecked(n + ((ulong)-(long)n & (b - 1)));
        }

        /// <summary>
        /// Rounds <paramref name="n"/> up to the next multiple of <paramref name="b"/>, which must be a power
        /// of two.
        /// </summary>
        public static uint AlignUp(uint n, uint b)
        {
            return unchecked(n + ((uint)-(int)n & (b - 1)));
        }
    }
}
