﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Alunite.Data
{
    /// <summary>
    /// A specialization of <see cref="Set{T}"/> for lists of small <see cref="uint"/> indices where all member lists
    /// have the same length.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    [DebuggerTypeProxy(typeof(RegularIndexListSetDebugView))]
    public struct RegularIndexListSet : IEquatable<RegularIndexListSet>, IEnumerable<List<uint>>
    {
        internal RegularIndexListSet(uint len, _Internal source)
        {
            Length = len;
            _source = source;
        }

        /// <summary>
        /// The length of all the lists in this set, or <see cref="uint.MaxValue"/> if this set is empty.
        /// </summary>
        public uint Length { get; }

        /// <summary>
        /// The underlying set for this <see cref="RegularIndexListSet"/>. This will be none if <see cref="Length"/>
        /// is 0.
        /// </summary>
        internal _Internal _source;

        /// <summary>
        /// The empty set.
        /// </summary>
        public static RegularIndexListSet None => new RegularIndexListSet(uint.MaxValue, _Internal.None);

        /// <summary>
        /// A set which contains only the empty list.
        /// </summary>
        public static RegularIndexListSet SingletonEmpty => new RegularIndexListSet(0, _Internal.None);

        /// <summary>
        /// Constructs a <see cref="RegularIndexListSet"/> consisting of only the given list.
        /// </summary>
        public static RegularIndexListSet Singleton(List<uint> list)
        {
            if (list.IsEmpty)
                return SingletonEmpty;
            return new RegularIndexListSet(list.Length, _Internal.Singleton(list));
        }

        /// <summary>
        /// Constructs a length-one <see cref="RegularIndexListSet"/> where the single element is described by the given set.
        /// </summary>
        public static RegularIndexListSet Independent(IndexSet item)
        {
            if (item.IsNone)
                return None;
            return new RegularIndexListSet(1, new _Internal(item._bitmap));
        }

        /// <summary>
        /// Constructs a <see cref="RegularIndexListSet"/> where each item is chosen from a given set, independent
        /// of all other items.
        /// </summary>
        public static RegularIndexListSet Independent(params IndexSet[] items)
        {
            return Independent((Span<IndexSet>)items);
        }

        /// <summary>
        /// Constructs a <see cref="RegularIndexListSet"/> where each item is chosen from a given set, independent
        /// of all other items.
        /// </summary>
        public static RegularIndexListSet Independent(ReadOnlySpan<IndexSet> items)
        {
            uint len = (uint)items.Length;
            uint rem = len;
            if (rem > 0)
            {
                rem--;
                IndexSet elem = items[(int)rem];
                if (elem.IsNone)
                    return None;
                _Internal tail = new _Internal(elem._bitmap);
                while (rem > 0)
                {
                    rem--;
                    elem = items[(int)rem];
                    if (elem.IsNone)
                        return None;
                    _Case[] cases = new _Case[elem.Bound];
                    _Case @case = _Case.Build(tail);
                    for (uint i = 0; i < cases.Length; i++)
                        cases[i] = elem.Contains(i) ? @case : _Case.None;
                    tail = new _Internal(cases);
                }
                return new RegularIndexListSet(len, tail);
            }
            else
            {
                return SingletonEmpty;
            }
        }

        /// <summary>
        /// Constructs a <see cref="RegularIndexListSet"/> containing all lists where each item is below the
        /// correspond bound.
        /// </summary>
        public static RegularIndexListSet IndependentBelow(params uint[] bounds)
        {
            return IndependentBelow((Span<uint>)bounds);
        }

        /// <summary>
        /// Constructs a <see cref="RegularIndexListSet"/> containing all lists where each item is below the
        /// correspond bound.
        /// </summary>
        public static RegularIndexListSet IndependentBelow(ReadOnlySpan<uint> bounds)
        {
            uint len = (uint)bounds.Length;
            uint rem = (uint)len;
            if (rem > 0)
            {
                rem--;
                uint bound = bounds[(int)rem];
                if (bound == 0)
                    return None;
                _Internal tail = new _Internal(IndexSet.Below(bound)._bitmap);
                while (rem > 0)
                {
                    rem--;
                    bound = bounds[(int)rem];
                    if (bound == 0)
                        return None;
                    _Case[] cases = new _Case[bound];
                    _Case @case = _Case.Build(tail);
                    for (uint i = 0; i < cases.Length; i++)
                        cases[i] = @case;
                    tail = new _Internal(cases);
                }
                return new RegularIndexListSet(len, tail);
            }
            else
            {
                return SingletonEmpty;
            }
        }

        /// <summary>
        /// Constructs a <see cref="RegularIndexListSet"/> from the given bounds and predicate.
        /// </summary>
        public static RegularIndexListSet Build(ReadOnlySpan<uint> bounds, Predicate<List<uint>> pred)
        {
            if (bounds.Length == 0)
            {
                if (pred(List.Empty<uint>()))
                    return SingletonEmpty;
                else
                    return None;
            }
            else
            {
                Span<uint> prefix = stackalloc uint[bounds.Length - 1];
                _Internal set = _Internal._build(bounds, prefix, 0, pred);
                if (set.IsNone)
                    return None;
                return new RegularIndexListSet((uint)bounds.Length, set);
            }
        }

        /// <summary>
        /// Constructs a <see cref="RegularIndexListSet"/> which consists of all the possible independent concatenations
        /// of lists from the given sets.
        /// </summary>
        public static RegularIndexListSet Concat(RegularIndexListSet a, RegularIndexListSet b)
        {
            if (a.IsNone || b.IsNone)
                return None;
            if (a.IsSingletonEmpty)
                return b;
            if (b.IsSingletonEmpty)
                return a;
            var cache = new MapBuilder<_Internal, _Internal>();
            _Internal res = a._source._concat(_Case.Build(b._source), cache);
            return new RegularIndexListSet(a.Length + b.Length, res);
        }

        /// <summary>
        /// Constructs a <see cref="RegularIndexListSet"/> which consists of all the possible independent concatenations
        /// of lists from the given sets.
        /// </summary>
        public static RegularIndexListSet Concat(params RegularIndexListSet[] sets)
        {
            return Concat((Span<RegularIndexListSet>)sets);
        }

        /// <summary>
        /// Constructs a <see cref="RegularIndexListSet"/> which consists of all the possible independent concatenations
        /// of lists from the given sets.
        /// </summary>
        public static RegularIndexListSet Concat(ReadOnlySpan<RegularIndexListSet> sets)
        {
            // Going right to left, find the first non-empty set
            RegularIndexListSet set;
            int index = sets.Length;
            while (index > 0)
            {
                set = sets[--index];
                if (set.IsNone)
                    return None;
                if (!set.IsSingletonEmpty)
                    goto foundNonEmpty;
            }
            return SingletonEmpty;

        foundNonEmpty:
            // Perform iterative concatenation
            uint len = set.Length;
            _Internal res = set._source;
            while (index > 0)
            {
                RegularIndexListSet prefix = sets[--index];
                if (prefix.IsNone)
                    return None;
                if (!prefix.IsSingletonEmpty)
                {
                    var cache = new MapBuilder<_Internal, _Internal>();
                    len += prefix.Length;
                    res = prefix._source._concat(_Case.Build(res), cache);
                }
            }
            return new RegularIndexListSet(len, res);
        }

        /// <summary>
        /// Constructs a <see cref="RegularIndexListSet"/> as the union of the given sets. All non-<see cref="None"/>
        /// sets must be of the same length.
        /// </summary>
        public static RegularIndexListSet Union(RegularIndexListSet a, RegularIndexListSet b)
        {
            if (a.IsNone)
                return b;
            if (b.IsNone)
                return a;
            if (a.Length != b.Length)
                throw new ArgumentException("Lists must be of the same length");
            if (a.Length == 0)
                return a;
            var cache = new MapBuilder<(_Internal, _Internal), _Internal>();
            return new RegularIndexListSet(a.Length, _Internal._union(cache, a._source, b._source));
        }

        /// <summary>
        /// Constructs a <see cref="RegularIndexListSet"/> as the union of the given sets. All non-<see cref="None"/>
        /// sets must be of the same length.
        /// </summary>
        public static RegularIndexListSet Union(params RegularIndexListSet[] sets)
        {
            return Union((Span<RegularIndexListSet>)sets);
        }

        /// <summary>
        /// Constructs a <see cref="RegularIndexListSet"/> as the union of the given sets. All non-<see cref="None"/>
        /// sets must be of the same length.
        /// </summary>
        public static RegularIndexListSet Union(ReadOnlySpan<RegularIndexListSet> sets)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Constructs an <see cref="RegularIndexListSet"/> as the intersection of the given sets. All non-<see cref="None"/>
        /// sets must be of the same length.
        /// </summary>
        public static RegularIndexListSet Intersection(RegularIndexListSet a, RegularIndexListSet b)
        {
            if (a.IsNone || b.IsNone)
                return None;
            if (a.Length != b.Length)
                throw new ArgumentException("Lists must be of the same length");
            if (a.Length == 0)
                return a;
            var cache = new MapBuilder<(_Internal, _Internal), _Internal>();
            return new RegularIndexListSet(a.Length, _Internal._intersection(cache, a._source, b._source));
        }

        /// <summary>
        /// Constructs an <see cref="RegularIndexListSet"/> as the difference of the given sets. All non-<see cref="None"/>
        /// sets must be of the same length.
        /// </summary>
        public static RegularIndexListSet Difference(RegularIndexListSet a, RegularIndexListSet b)
        {
            if (a.IsNone)
                return None;
            if (b.IsNone)
                return a;
            if (a.Length != b.Length)
                throw new ArgumentException("Lists must be of the same length");
            if (a.Length == 0)
                return None;
            var cache = new MapBuilder<(_Internal, _Internal), _Internal>();
            return new RegularIndexListSet(a.Length, _Internal._difference(cache, a._source, b._source));
        }

        /// <summary>
        /// Constructs an <see cref="RegularIndexListSet"/> for a list whose first item is an
        /// index in <paramref name="cases"/> and whose remaining items are taken from the set at
        /// that index. All non-<see cref="None"/> cases must have the same length.
        /// </summary>
        public static RegularIndexListSet Switch(ReadOnlySpan<RegularIndexListSet> cases)
        {
            uint last = (uint)cases.Length;
            while (last > 0)
            {
                if (!cases[(int)--last].IsNone)
                    goto foundLast;
            }
            return None;

        foundLast:
            uint numCases = last + 1;
            uint len = cases[(int)last].Length;
            if (len == 0)
            {
                throw new NotImplementedException();
            }
            else
            {
                _Case[] nCases = new _Case[numCases];
                for (int i = 0; i < nCases.Length; i++)
                {
                    RegularIndexListSet @case = cases[i];
                    if (@case.IsNone)
                    {
                        nCases[i] = _Case.None;
                    }
                    else
                    {
                        if (@case.Length != len)
                            throw new ArgumentException("All cases must have the same length", nameof(cases));
                        nCases[i] = _Case.Build(@case._source);
                    }
                }
                return new RegularIndexListSet(len + 1, new _Internal(nCases));
            }
        }

        /// <summary>
        /// Constructs an <see cref="RegularIndexListSet"/> for a list whose first item is an
        /// index in <paramref name="cases"/> and whose remaining items are taken from the list at
        /// that index.
        /// </summary>
        public static RegularIndexListSet SwitchSingleton(ReadOnlySpan<List<uint>> cases)
        {
            if (cases.Length == 0)
                return None;
            uint len = cases[0].Length;
            if (len == 0)
                return Independent(IndexSet.Below((uint)cases.Length));
            _Case[] nCases = new _Case[cases.Length];
            for (int i = 0; i < nCases.Length; i++)
            {
                List<uint> @case = cases[i];
                if (@case.Length != len)
                    throw new ArgumentException("All cases must have the same length", nameof(cases));
                nCases[i] = _Case.Build(_Internal.Singleton(@case));
            }
            return new RegularIndexListSet(len + 1, new _Internal(nCases));
        }

        /// <summary>
        /// Indicates whether this is <see cref="None"/>.
        /// </summary>
        public bool IsNone => Length == uint.MaxValue;

        /// <summary>
        /// Indicates whether this is <see cref="SingletonEmpty"/>.
        /// </summary>
        public bool IsSingletonEmpty => Length == 0;

        /// <summary>
        /// Determines whether this set consists of just one list, and if so, returns that list.
        /// </summary>
        public bool IsSingleton(out List<uint> value)
        {
            if (IsSingletonEmpty)
            {
                value = List.Empty<uint>();
                return true;
            }
            Span<uint> items = stackalloc uint[(int)Length];
            if (_source.TryWriteSingleton(items))
            {
                value = List.Of(items);
                return true;
            }
            value = default;
            return false;
        }

        /// <summary>
        /// Determines whether this set contains the given list.
        /// </summary>
        public bool Contains(ReadOnlySpan<uint> list)
        {
            if (list.Length == 0)
                return IsSingletonEmpty;
            return _source.Contains(list);
        }

        /// <summary>
        /// The number of lists in this set, capped to <see cref="uint.MaxValue"/>.
        /// </summary>
        public uint Size
        {
            get
            {
                uint size = Length == 0 ? 1u : 0;
                if (_source.IsNone)
                    return size;
                var cache = new MapBuilder<_Internal, uint>();
                _source._computeSize(cache, ref size);
                return size;
            }
        }

        /// <summary>
        /// Provides a measure of how complex this set is. This is minimal for <see cref="Independent"/>
        /// sets and increases depending on how interdependent the set is.
        /// </summary>
        public uint Complexity => _source.Complexity;

        /// <summary>
        /// A list which contains one more than the maximum value for each item in this set.
        /// This is not applicable for <see cref="None"/>.
        /// </summary>
        public List<uint> Bounds
        {
            get
            {
                if (IsNone)
                    throw new InvalidOperationException();
                if (Length == 0)
                    return List.Empty<uint>();
                var cache = new SetBuilder<_Internal>();
                uint[] bounds = new uint[Length];
                _source._getBounds(cache, ref bounds[0], bounds.AsSpan().Slice(1));
                return new List<uint>(bounds);
            }
        }

        /// <summary>
        /// Gets the possible lists for a contiguous subsequence of this set.
        /// </summary>
        public RegularIndexListSet Slice(uint start, uint len)
        {
            if (IsNone)
                return None;
            if (start == 0)
            {
                uint thisLen = Length;
                if (len < thisLen)
                {
                    if (len == 0)
                        return SingletonEmpty;
                    var cache = new MapBuilder<_Internal, _Internal>();
                    return new RegularIndexListSet(len, _source._splitPrefix(len, cache));
                }
                else
                {
                    if (len != thisLen)
                        throw new ArgumentOutOfRangeException(nameof(len));
                    return this;
                }
            }
            return Subseq(IndexSet.Range(start, len));
        }

        /// <summary>
        /// Gets the possible lists for a subsequence of this set.
        /// </summary>
        public RegularIndexListSet Subseq(IndexSet indices)
        {
            if (IsNone)
                return None;
            if (indices.IsNone)
                return SingletonEmpty;

            // Determine which indices we have to union over (complement of indices), and the
            // length of the list following the last pivot
            uint indicesBound = indices.Bound;
            Span<uint> pivots = stackalloc uint[(int)indicesBound];
            int numPivots = 0;
            uint lastLen = 0;
            uint finalLen = 0;
            for (uint i = 0; i < (uint)pivots.Length; i++)
            {
                if (indices.Contains(i))
                {
                    lastLen++;
                }
                else
                {
                    pivots[numPivots++] = i;
                    finalLen += lastLen;
                    lastLen = 0;
                }
            }
            finalLen += lastLen;
            pivots = pivots.Slice(0, numPivots);
            
            // Shortcut for when there are no pivots
            MapBuilder<_Internal, _Internal> sliceCache = null;
            if (indicesBound < Length)
            {
                sliceCache = new MapBuilder<_Internal, _Internal>();
                if (pivots.Length == 0)
                    return new RegularIndexListSet(lastLen, _source._splitPrefix(lastLen, sliceCache));
            }
            else
            {
                if (pivots.Length == 0)
                    return this;
            }

            // Make pivot indices relative rather than absolute
            uint pivotBase = 0;
            for (int i = 0; i < pivots.Length; i++)
            {
                uint pivot = pivots[i];
                pivots[i] = pivot - pivotBase;
                pivotBase = pivot + 1;
            }

            // Do subseq
            var cache = new MapBuilder<_Internal, _Internal>();
            var unionCache = new MapBuilder<(_Internal, _Internal), _Internal>();
            _Internal subseq(ReadOnlySpan<uint> remPivots, _Internal set)
            {
                if (remPivots.Length == 0)
                {
                    if (sliceCache == null)
                        return set;
                    else
                        return set._splitPrefix(lastLen, sliceCache);
                }
                else
                {
                    return subseqWith(remPivots[0], remPivots.Slice(1), set);
                }
            }
            _Internal subseqWith(uint pivot, ReadOnlySpan<uint> remPivots, _Internal set)
            {
                if (cache.TryGet(set, out _Internal res))
                    return res;
                _Case[] cases = (_Case[])set._table;
                if (pivot == 0)
                {
                    // Union over immediate cases
                    res = _Internal.None;
                    for (int i = 0; i < cases.Length; i++)
                    {
                        _Case @case = cases[i];
                        if (!@case.Tail.IsNone)
                            res = _Internal._union(unionCache, res, subseq(remPivots, @case.Tail));
                    }
                }
                else
                {
                    // Perform subseq on cases
                    _Case[] nCases = new _Case[cases.Length];
                    uint nPivot = pivot - 1;
                    for (int i = 0; i < cases.Length; i++)
                    {
                        _Case @case = cases[i];
                        if (@case.Tail.IsNone)
                            nCases[i] = _Case.None;
                        else
                            nCases[i] = _Case.Build(subseqWith(nPivot, remPivots, @case.Tail));
                    }
                    res = new _Internal(nCases);
                }
                cache.Add(set, res);
                return res;
            }
            return new RegularIndexListSet(finalLen, subseqWith(pivots[0], pivots.Slice(1), _source));
        }

        /// <summary>
        /// Gets the possible list constructed by selecting items from this list.
        /// </summary>
        public RegularIndexListSet Select(params uint[] indices)
        {
            return Select((Span<uint>)indices);
        }

        /// <summary>
        /// Gets the possible list constructed by selecting items from this list.
        /// </summary>
        public RegularIndexListSet Select(ReadOnlySpan<uint> indices)
        {
            if (IsNone)
                return None;

            // Prepare selection plan
            _prepareSelect(Length, indices,
                out IndexSet subseqIndices,
                out _SelectInstruction[] insts);

            // Do subseq
            RegularIndexListSet source = Subseq(subseqIndices);

            // Shortcut for if we are already done at this point
            if (insts.Length == 0)
                return source;

            // Perform actual selection
            var cache = new MapBuilder<(uint, _Internal, List<uint>), _Internal>();
            var pullCache = new MapBuilder<(uint, _Internal), _Case[]>();
            _Internal apply(uint step, _Internal rem, List<uint> vars)
            {
                if (step < insts.Length)
                {
                    _SelectInstruction inst = insts[step];
                    var key = (step, rem, vars);
                    if (cache.TryGet(key, out _Internal res))
                        return res;
                    uint nStep = step + 1;
                    if (inst.IsLoad)
                    {
                        // Load next item from variable
                        uint var = vars[inst.Index];
                        if (inst.ShouldModify)
                        {
                            List<uint> nVars = vars.RemoveAt(inst.Index);
                            if (nVars.IsEmpty && rem.IsNone)
                            {
                                // This is the last instruction, use var directly for final item
                                Debug.Assert(nStep == (uint)insts.Length);
                                return _Internal.Independent(IndexSet.Of(var));
                            }
                            else
                            {
                                _Case[] cases = new _Case[var + 1];
                                for (uint i = 0; i < var; i++)
                                    cases[i] = _Case.None;
                                cases[var] = _Case.Build(apply(nStep, rem, nVars));
                                res = new _Internal(cases);
                            }
                        }
                        else
                        {
                            _Case[] cases = new _Case[var + 1];
                            for (uint i = 0; i < var; i++)
                                cases[i] = _Case.None;
                            cases[var] = _Case.Build(apply(nStep, rem, vars));
                            res = new _Internal(cases);
                        }
                    }
                    else
                    {
                        if (rem._table is _Case[] remCases)
                        {
                            // Pull chosen item from rem
                            _Case[] cases = _Internal._pull(pullCache, inst.Index, remCases);
                            _Case[] nCases = new _Case[cases.Length];
                            if (inst.ShouldModify)
                            {
                                for (uint i = 0; i < (uint)cases.Length; i++)
                                {
                                    _Case @case = cases[i];
                                    if (@case.Tail.IsNone)
                                    {
                                        nCases[i] = _Case.None;
                                    }
                                    else
                                    {
                                        List<uint> nVars = vars.Append(i);
                                        nCases[i] = _Case.Build(apply(nStep, @case.Tail, nVars));
                                    }
                                }
                            }
                            else
                            {
                                for (uint i = 0; i < cases.Length; i++)
                                {
                                    _Case @case = cases[i];
                                    if (@case.Tail.IsNone)
                                        nCases[i] = _Case.None;
                                    else
                                        nCases[i] = _Case.Build(apply(nStep, @case.Tail, vars));
                                }
                            }
                            res = new _Internal(nCases);
                        }
                        else
                        {
                            // Pull only item from rem
                            ulong[] remBitmap = (ulong[])rem._table;
                            IndexSet remItem = new IndexSet(remBitmap);
                            Debug.Assert(inst.Index == 0);
                            if (inst.ShouldModify)
                            {
                                _Case[] nCases = new _Case[remItem.Bound];
                                for (uint i = 0; i < (uint)nCases.Length; i++)
                                {
                                    if (remItem.Contains(i))
                                    {
                                        List<uint> nVars = vars.Append(i);
                                        nCases[i] = _Case.Build(apply(nStep, _Internal.None, nVars));
                                    }
                                    else
                                    {
                                        nCases[i] = _Case.None;
                                    }
                                }
                                res = new _Internal(nCases);
                            }
                            else
                            {
                                Debug.Assert(!vars.IsEmpty);
                                _Case[] nCases = new _Case[remItem.Bound];
                                for (uint i = 0; i < (uint)nCases.Length; i++)
                                {
                                    if (remItem.Contains(i))
                                        nCases[i] = _Case.Build(apply(nStep, _Internal.None, vars));
                                    else
                                        nCases[i] = _Case.None;
                                }
                                res = new _Internal(nCases);
                            }
                        }
                    }
                    cache.Add(key, res);
                    return res;
                }
                else
                {
                    return rem;
                }
            }
            return new RegularIndexListSet((uint)indices.Length, apply(0, source._source, List.Empty<uint>()));
        }

        /// <summary>
        /// Prepares a plan for executing <see cref="Select"/>. The plan involves two parts: calling <see cref="Subseq"/> to
        /// remove extraneous items, and then running a sequence of <see cref="_SelectInstruction"/>s to rearrange the
        /// remaining items.
        /// </summary>
        /// <param name="len">The length of the list to run the plan on.</param>
        internal static void _prepareSelect(
            uint len, ReadOnlySpan<uint> indices,
            out IndexSet subseqIndices,
            out _SelectInstruction[] insts)
        {
            // For each source index, determine the last time it is needed.
            Span<uint> lastBounds = stackalloc uint[(int)len];
            for (uint i = 0; i < (uint)indices.Length; i++)
            {
                uint index = indices[(int)i];
                lastBounds[(int)index] = i + 1;
            }

            // Construct subseq of items that were used
            ulong[] bitmap = Array.Empty<ulong>();
            for (uint i = 0; i < (uint)lastBounds.Length; i++)
                if (lastBounds[(int)i] > 0)
                    IndexSetBuilder._include(ref bitmap, i);
            subseqIndices = IndexSet._build(bitmap);

            // Create a plan for constructing the final distribution
            Span<_SelectInstruction> sInsts = stackalloc _SelectInstruction[indices.Length];
            List<uint> rem = subseqIndices.Sort();
            List<uint> vars = List.Empty<uint>();
            for (uint i = 0; i < (uint)indices.Length; i++)
            {
                uint index = indices[(int)i];
                if (rem.TryFindFirst(index, out uint pullIndex))
                {
                    if (lastBounds[(int)index] - 1 > i)
                    {
                        sInsts[(int)i] = _SelectInstruction.Pull(pullIndex, true);
                        rem = rem.RemoveAt(pullIndex);
                        vars = vars.Append(index);
                    }
                    else
                    {
                        sInsts[(int)i] = _SelectInstruction.Pull(pullIndex, false);
                        rem = rem.RemoveAt(pullIndex);
                    }
                }
                else
                {
                    bool hasVar = vars.TryFindFirst(index, out uint loadIndex);
                    Debug.Assert(hasVar);
                    if (lastBounds[(int)index] - 1 > i)
                    {
                        sInsts[(int)i] = _SelectInstruction.Load(loadIndex, false);
                    }
                    else
                    {
                        sInsts[(int)i] = _SelectInstruction.Load(loadIndex, true);
                        vars = vars.RemoveAt(loadIndex);
                    }
                }
            }
            Debug.Assert(rem.IsEmpty);
            Debug.Assert(vars.Length == 0);

            // If there are no extraneous items at the end of rem, we can use it directly instead of
            // deconstructing it with redundant pull(0) instructions at the end of the list
            uint numInsts = (uint)sInsts.Length;
            while (numInsts > 0)
            {
                uint prevInstIndex = numInsts - 1;
                var lastInst = sInsts[(int)prevInstIndex];
                if (lastInst.Index == 0 && !lastInst.IsLoad && !lastInst.ShouldModify)
                    numInsts = prevInstIndex;
                else
                    break;
            }

            // Finalize insts
            insts = new _SelectInstruction[numInsts];
            for (int i = 0; i < insts.Length; i++)
                insts[i] = sInsts[i];
        }

        /// <summary>
        /// A step in the process of constructing a <see cref="Select"/> set. Each instruction is responsible
        /// for adding one item to the resulting list.
        /// </summary>
        internal struct _SelectInstruction
        {
            /// <summary>
            /// Indicates whether this is a <see cref="Load"/> instruction.
            /// </summary>
            public bool IsLoad;

            /// <summary>
            /// Indicates whether the variables list should be modified by this instruction.
            /// </summary>
            public bool ShouldModify;

            /// <summary>
            /// The affected index for this instruction.
            /// </summary>
            public uint Index;

            /// <summary>
            /// Removes a particular item from the remaining list, appending it to the resulting list
            /// and optionally to the variables list.
            /// </summary>
            /// <param name="shouldStore">If true, stores the value of the pulled item at the end of the variables list.</param>
            public static _SelectInstruction Pull(uint index, bool shouldStore)
            {
                return new _SelectInstruction
                {
                    IsLoad = false,
                    ShouldModify = shouldStore,
                    Index = index
                };
            }

            /// <summary>
            /// Appends the value of a variable to the resulting list, optionally removing it from the variables list.
            /// </summary>
            public static _SelectInstruction Load(uint index, bool shouldRemove)
            {
                return new _SelectInstruction
                {
                    IsLoad = true,
                    ShouldModify = shouldRemove,
                    Index = index
                };
            }
        }

        /// <summary>
        /// Reinterprets this set as the concatenation of prefix of a certain length and a suffix chosen based on
        /// the prefix.
        /// </summary>
        public void Split(uint prefixLen,
            out RegularIndexListSet prefix,
            out RegularIndexListMap<RegularIndexListSet> suffix)
        {
            if (IsNone)
            {
                prefix = None;
                suffix = RegularIndexListMap.Empty<RegularIndexListSet>();
                return;
            }
            if (!(prefixLen < Length))
                throw new ArgumentException("Invalid prefix length", nameof(prefixLen));
            if (prefixLen == 0)
            {
                prefix = SingletonEmpty;
                suffix = RegularIndexListMap.SingletonEmpty(this);
                return;
            }

            // Construct prefix
            var prefixCache = new MapBuilder<_Internal, _Internal>();
            prefix = new RegularIndexListSet(prefixLen, _source._splitPrefix(prefixLen, prefixCache));

            // Construct suffix
            var suffixCache = new MapBuilder<_Internal, RegularIndexListMap<RegularIndexListSet>._Internal>();
            suffix = new RegularIndexListMap<RegularIndexListSet>(prefixLen,
                _source._splitSuffix(prefixLen, Length - prefixLen, suffixCache));
        }

        /// <summary>
        /// Gets the set of lists that result from prepending an item from the given set to this set.
        /// </summary>
        public RegularIndexListSet Prepend(IndexSet item)
        {
            if (IsNone)
                return None;
            return new RegularIndexListSet(Length + 1, _source.Prepend(item));
        }

        /// <summary>
        /// Gets the set of lists that result from prepending the given item to this set.
        /// </summary>
        public RegularIndexListSet PrependSingleton(uint item)
        {
            if (IsNone)
                return None;
            return new RegularIndexListSet(Length + 1, _source.PrependSingleton(item));
        }

        /// <summary>
        /// Filters this set to only include lists where the given subsequence of items are in the given set.
        /// </summary>
        public RegularIndexListSet IntersectSubseq(IndexSet indices, RegularIndexListSet set)
        {
            if (set.IsNone)
                return None;
            if (!(indices.Size == set.Length))
                throw new ArgumentException("List length mismatch", nameof(set));
            if (IsNone)
                return None;
            if (!(indices.Bound <= Length))
                throw new ArgumentException("Indices out of range", nameof(indices));
            if (set.IsSingletonEmpty)
                return SingletonEmpty;
            var cache = new MapBuilder<(_Internal, _Internal), _Internal>();
            _Internal res = _source._intersectSubseq(cache, indices.Sort(), set._source, 0);
            if (res.IsNone)
                return None;
            return new RegularIndexListSet(Length, res);
        }

        /// <summary>
        /// Gets the members of this set in lexicographical order.
        /// </summary>
        public List<List<uint>> Sort()
        {
            if (IsNone)
                return List.Empty<List<uint>>();
            if (IsSingletonEmpty)
                return List.Of(List.Empty<uint>());
            ListBuilder<List<uint>> lists = new ListBuilder<List<uint>>();
            Span<uint> items = stackalloc uint[(int)Length];
            _source._sort(lists, items, 0);
            return lists.Finish();
        }

        /// <summary>
        /// Finds a minimal set of "factors" whose union is this set. Each factor is a concatenation of
        /// independent <see cref="RegularIndexListSet"/>s of the lengths specified by <paramref name="lens"/>.
        /// </summary>
        public Bag<List<RegularIndexListSet>> Factorize(ReadOnlySpan<uint> lens)
        {
            // Handle none special case
            if (IsNone)
                return Bag.Empty<List<RegularIndexListSet>>();

            // Check total length
            uint totalLen = 0;
            for (int i = 0; i < lens.Length; i++)
            {
                uint len = lens[i];
                if (len == 0)
                    throw new ArgumentException("Zero-length parts are not allowed", nameof(lens));
                totalLen += lens[i];
            }
            if (totalLen != Length)
                throw new ArgumentException("Wrong total length", nameof(lens));

            // Perform recursive factorization
            var cache = new MapBuilder<_Internal, (Bag<(_Internal, _Internal)>, uint)>();
            var res = BagBuilder<List<RegularIndexListSet>>.CreateEmpty();
            var working = new RegularIndexListSet[lens.Length];
            _source._factorize(lens, cache, res, working, 0);
            return res.Finish();
        }

        /// <summary>
        /// Converts a list of <see cref="_Internal"/>s to a list of <see cref="RegularIndexListSet"/>, assuming
        /// they all have the same non-zero length.
        /// </summary>
        private static List<RegularIndexListSet> _fromInternal(uint len, ReadOnlySpan<_Internal> items)
        {
            RegularIndexListSet[] nItems = new RegularIndexListSet[items.Length];
            for (int i = 0; i < items.Length; i++)
                nItems[i] = new RegularIndexListSet(len, items[i]);
            return new List<RegularIndexListSet>(nItems);
        }

        /// <summary>
        /// The internal recursive definition of <see cref="RegularIndexListMap{T}"/>, which has no
        /// representation for <see cref="SingletonEmpty"/> and doesn't store the length at each node.
        /// </summary>
        internal struct _Internal : IEquatable<_Internal>
        {
            internal _Internal(ulong[] bitmap)
            {
                _table = bitmap;
            }

            internal _Internal(_Case[] cases)
            {
                _table = cases;
            }

            /// <summary>
            /// The table describing the members in this set based on the possible values of the first item. This is
            /// either null for the <see cref="None"/> set, an array of <see cref="ulong"/>s for a length-1 set, or an
            /// array of <see cref="_Case"/>s for a longer set.
            /// </summary>
            internal object _table;

            /// <summary>
            /// The empty set.
            /// </summary>
            public static _Internal None => default;

            /// <summary>
            /// Constructs a <see cref="_Internal"/> consisting of only the given list.
            /// </summary>
            public static _Internal Singleton(ReadOnlySpan<uint> items)
            {
                if (items.Length <= 1)
                {
                    Debug.Assert(items.Length == 1);
                    return new _Internal(IndexSet.Of(items[0])._bitmap);
                }
                else
                {
                    uint item = items[0];
                    _Case[] cases = new _Case[item + 1];
                    for (uint i = 0; i < item; i++)
                        cases[i] = _Case.None;
                    cases[item] = _Case.Build(Singleton(items.Slice(1)));
                    return new _Internal(cases);
                }
            }

            /// <summary>
            /// Constructs a length-one <see cref="_Internal"/> where the single element is described by the given set.
            /// </summary>
            public static _Internal Independent(IndexSet item)
            {
                if (item.IsNone)
                    return None;
                return new _Internal(item._bitmap);
            }

            /// <summary>
            /// Constructs an <see cref="_Internal"/> based on a subset of entries from a set.
            /// </summary>
            /// <param name="indices">The indices to be considered.</param>
            /// <param name="offset">The first item to be included in the resulting set.</param>
            internal static _Internal _fromSet(
                Set<List<uint>>._Entry[] entries,
                ReadOnlySpan<uint> indices,
                uint offset)
            {
                if (indices.Length == 0)
                    return None;
                
                // Get bound for this item
                uint bound = 0;
                for (int i = 0; i < indices.Length; i++)
                {
                    uint item = entries[indices[i]].Value[offset];
                    if (bound < item)
                        bound = item;
                }
                bound++;

                // Are we at the last item?
                uint len = entries[indices[0]].Value.Length;
                uint nOffset = offset + 1;
                if (nOffset < len)
                {
                    // Get number of lists per value
                    Span<uint> numLists = stackalloc uint[(int)bound];
                    for (int i = 0; i < indices.Length; i++)
                    {
                        uint item = entries[indices[i]].Value[offset];
                        numLists[(int)item]++;
                    }

                    // Allocate space for indices recursively
                    uint head = 0;
                    Span<uint> heads = numLists;
                    for (uint i = 0; i < bound; i++)
                    {
                        uint nHead = head + numLists[(int)i];
                        heads[(int)i] = head;
                        head = nHead;
                    }
                    Debug.Assert(head == (uint)indices.Length);
                    Span<uint> nIndices = stackalloc uint[indices.Length];

                    // Group indices based on item
                    for (int i = 0; i < indices.Length; i++)
                    {
                        uint index = indices[i];
                        uint item = entries[indices[i]].Value[offset];
                        nIndices[(int)heads[(int)item]++] = index;
                    }

                    // Construct cases recursively
                    uint start = 0;
                    _Case[] cases = new _Case[bound ];
                    for (uint i = 0; i < bound; i++)
                    {
                        uint end = heads[(int)i];
                        cases[i] = _Case.Build(_fromSet(entries, nIndices.Slice((int)start, (int)(end - start)), nOffset));
                        start = end;
                    }
                    return new _Internal(cases);
                }
                else
                {
                    // Construct bitmap for final item values
                    ulong[] bitmap = new ulong[BoolList._numWords(bound)];
                    for (int i = 0; i < indices.Length; i++)
                    {
                        uint item = entries[indices[i]].Value[offset];
                        BoolList._setTrue(bitmap, item);
                    }
                    return new _Internal(bitmap);
                }
            }

            /// <summary>
            /// Constructs an <see cref="_Internal"/> from the given bounds and predicate.
            /// </summary>
            internal static _Internal _build(
                ReadOnlySpan<uint> bounds, Span<uint> prefix,
                int offset, Predicate<List<uint>> pred)
            {
                uint bound = bounds[offset];
                if (offset < prefix.Length)
                {
                    _Case[] cases = new _Case[bound];
                    int nOffset = offset + 1;
                    for (uint i = 0; i < bound; i++)
                    {
                        prefix[offset] = i;
                        _Internal tail = _build(bounds, prefix, nOffset, pred);
                        cases[i] = _Case.Build(tail);
                    }
                    return Switch(cases);
                }
                else
                {
                    return _build(prefix, bound, pred);
                }
            }

            /// <summary>
            /// Builds a length-one <see cref="_Internal"/> based on the given list prefix and predicate.
            /// </summary>
            internal static _Internal _build(ReadOnlySpan<uint> prefix, uint bound, Predicate<List<uint>> pred)
            {
                Span<ulong> bitmap = stackalloc ulong[(int)BoolList._numWords(bound)];
                for (uint i = 0; i < bound; i++)
                {
                    uint[] items = new uint[prefix.Length + 1];
                    for (int j = 0; j < prefix.Length; j++)
                        items[j] = prefix[j];
                    items[prefix.Length] = i;
                    if (pred(new List<uint>(items)))
                        IndexSetBuilder._include(bitmap, i);
                }
                return Independent(IndexSet._build(bitmap));
            }

            /// <summary>
            /// Constructs an <see cref="_Internal"/> from the given cases, trimming <see cref="None"/> cases
            /// at the end of the array.
            /// </summary>
            public static _Internal Switch(_Case[] cases)
            {
                uint last = (uint)cases.Length;
                while (last > 0)
                {
                    if (!cases[--last].IsNone)
                        goto foundLast;
                }
                return None;

            foundLast:
                uint len = last + 1;
                if (len < (uint)cases.Length)
                {
                    _Case[] nCases = new _Case[len];
                    for (int i = 0; i < nCases.Length; i++)
                        nCases[i] = cases[i];
                    cases = nCases;
                }
                return new _Internal(cases);
            }

            /// <summary>
            /// Constructs a <see cref="_Internal"/> as the union of the given sets.
            /// </summary>
            internal static _Internal _union(
                MapBuilder<(_Internal, _Internal), _Internal> cache,
                _Internal a, _Internal b)
            {
                if (a.IsNone)
                    return b;
                if (b.IsNone)
                    return a;
                if (a._table is _Case[] aCases)
                {
                    var key = (a, b);
                    if (cache.TryGet(key, out _Internal res))
                        return res;

                    // Case-by-case union
                    _Case[] nCases;
                    _Case[] bCases = (_Case[])b._table;
                    if (aCases.Length < bCases.Length)
                    {
                        nCases = new _Case[bCases.Length];
                        for (int i = 0; i < aCases.Length; i++)
                            nCases[i] = _Case.Build(_union(cache, aCases[i].Tail, bCases[i].Tail));
                        for (int i = aCases.Length; i < bCases.Length; i++)
                            nCases[i] = bCases[i];
                    }
                    else
                    {
                        nCases = new _Case[aCases.Length];
                        for (int i = 0; i < bCases.Length; i++)
                            nCases[i] = _Case.Build(_union(cache, aCases[i].Tail, bCases[i].Tail));
                        for (int i = bCases.Length; i < aCases.Length; i++)
                            nCases[i] = aCases[i];
                    }
                    res = new _Internal(nCases);
                    cache.Add(key, res);
                    return res;
                }
                else
                {
                    ulong[] aBitmap = (ulong[])a._table;
                    ulong[] bBitmap = (ulong[])b._table;
                    return Independent(new IndexSet(aBitmap) | new IndexSet(bBitmap));
                }
            }

            /// <summary>
            /// Constructs a <see cref="_Internal"/> as the intersection of the given sets.
            /// </summary>
            internal static _Internal _intersection(
                MapBuilder<(_Internal, _Internal), _Internal> cache,
                _Internal a, _Internal b)
            {
                if (a.IsNone || b.IsNone)
                    return None;
                if (a._table is _Case[] aCases)
                {
                    var key = (a, b);
                    if (cache.TryGet(key, out _Internal res))
                        return res;

                    // Case-by-case union
                    _Case[] nCases;
                    _Case[] bCases = (_Case[])b._table;
                    if (aCases.Length < bCases.Length)
                    {
                        nCases = new _Case[aCases.Length];
                        for (int i = 0; i < aCases.Length; i++)
                            nCases[i] = _Case.Build(_intersection(cache, aCases[i].Tail, bCases[i].Tail));
                    }
                    else
                    {
                        nCases = new _Case[bCases.Length];
                        for (int i = 0; i < bCases.Length; i++)
                            nCases[i] = _Case.Build(_intersection(cache, aCases[i].Tail, bCases[i].Tail));
                    }
                    res = Switch(nCases);
                    cache.Add(key, res);
                    return res;
                }
                else
                {
                    ulong[] aBitmap = (ulong[])a._table;
                    ulong[] bBitmap = (ulong[])b._table;
                    return Independent(new IndexSet(aBitmap) & new IndexSet(bBitmap));
                }
            }

            /// <summary>
            /// Constructs a <see cref="_Internal"/> as the difference of the given sets.
            /// </summary>
            internal static _Internal _difference(
                MapBuilder<(_Internal, _Internal), _Internal> cache,
                _Internal a, _Internal b)
            {
                if (a.IsNone)
                    return None;
                if (b.IsNone)
                    return a;
                if (a._table is _Case[] aCases)
                {
                    var key = (a, b);
                    if (cache.TryGet(key, out _Internal res))
                        return res;

                    // Case-by-case union
                    _Case[] nCases;
                    _Case[] bCases = (_Case[])b._table;
                    if (aCases.Length <= bCases.Length)
                    {
                        nCases = new _Case[aCases.Length];
                        for (int i = 0; i < aCases.Length; i++)
                            nCases[i] = _Case.Build(_difference(cache, aCases[i].Tail, bCases[i].Tail));
                        res = Switch(nCases);
                    }
                    else
                    {
                        nCases = new _Case[aCases.Length];
                        for (int i = 0; i < bCases.Length; i++)
                            nCases[i] = _Case.Build(_difference(cache, aCases[i].Tail, bCases[i].Tail));
                        for (int i = bCases.Length; i < aCases.Length; i++)
                            nCases[i] = aCases[i];
                        res = new _Internal(nCases);
                    }
                    cache.Add(key, res);
                    return res;
                }
                else
                {
                    ulong[] aBitmap = (ulong[])a._table;
                    ulong[] bBitmap = (ulong[])b._table;
                    return Independent(new IndexSet(aBitmap) % new IndexSet(bBitmap));
                }
            }

            /// <summary>
            /// Indicates whether this is <see cref="None"/>.
            /// </summary>
            public bool IsNone => _table is null;

            /// <summary>
            /// Determines whether this set contains the given non-empty list.
            /// </summary>
            public bool Contains(ReadOnlySpan<uint> list)
            {
                _Internal set = this;
            next:
                if (set._table is null)
                {
                    return false;
                }
                else
                {
                    if (list.Length <= 1)
                    {
                        Debug.Assert(list.Length == 1);
                        ulong[] bitmap = (ulong[])set._table;
                        return IndexSetBuilder._contains(bitmap, list[0]);
                    }
                    else
                    {
                        _Case[] cases = (_Case[])set._table;
                        uint first = list[0];
                        if (first < (uint)cases.Length)
                        {
                            _Case @case = cases[first];
                            set = @case.Tail;
                            list = list.Slice(1);
                            goto next;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }

            /// <summary>
            /// The number of lists in this set, capped to <see cref="uint.MaxValue"/>.
            /// </summary>
            public uint Size
            {
                get
                {
                    if (IsNone)
                        return 0;
                    var cache = new MapBuilder<_Internal, uint>();
                    uint size = 0;
                    _computeSize(cache, ref size);
                    return size;
                }
            }

            /// <summary>
            /// Computes the size of this set.
            /// </summary>
            internal void _computeSize(MapBuilder<_Internal, uint> cache, ref uint size)
            {
                if (!(_table is null))
                {
                    if (_table is _Case[] cases)
                    {
                        if (cache.TryGet(this, out uint thisSize))
                        {
                            size = Saturate.Add(size, thisSize);
                        }
                        else
                        {
                            uint startSize = size;
                            for (int i = 0; i < cases.Length; i++)
                            {
                                cases[i].Tail._computeSize(cache, ref size);
                                if (size == uint.MaxValue)
                                    return;
                            }
                            thisSize = size - startSize;
                            cache.Add(this, thisSize);
                        }
                    }
                    else
                    {
                        ulong[] bitmap = (ulong[])_table;
                        size = Saturate.Add(size, new IndexSet(bitmap).Size);
                    }
                }
            }

            /// <summary>
            /// Provides a measure of how complex this set is. This is minimal for <see cref="Independent"/>
            /// sets and increases depending on how interdependent the set is.
            /// </summary>
            public uint Complexity
            {
                get
                {
                    if (_table is null)
                        return 0;
                    SetBuilder<_Internal> set = new SetBuilder<_Internal>();
                    _getComplexity(set);
                    return set.Size;
                }
            }

            /// <summary>
            /// Enumerates all <see cref="_Internal"/>s referenced in this set, not including the
            /// empty/none set.
            /// </summary>
            internal void _getComplexity(SetBuilder<_Internal> set)
            {
                if (!(_table is null))
                {
                    if (set.Include(this))
                    {
                        if (_table is _Case[] cases)
                        {
                            foreach (var @case in cases)
                                @case.Tail._getComplexity(set);
                        }
                        else
                        {
                            Debug.Assert(_table is ulong[]);
                        }
                    }
                }
            }

            /// <summary>
            /// If this <see cref="_Internal"/> contains just one member, writes the items of that member to
            /// <paramref name="items"/>. Otherwise, returns false.
            /// </summary>
            public bool TryWriteSingleton(Span<uint> items)
            {
                _Internal set = this;
                if (set._table is null)
                    return false;

            next:
                if (items.Length <= 1)
                {
                    Debug.Assert(items.Length == 1);
                    ulong[] bitmap = (ulong[])set._table;
                    IndexSet item = new IndexSet(bitmap);
                    return item.IsSingleton(out items[0]);
                }
                else
                {
                    _Case[] cases = (_Case[])set._table;
                    uint last = (uint)cases.Length - 1;
                    for (uint i = 0; i < last; i++)
                        if (!cases[i].IsNone)
                            return false;
                    set = cases[last].Tail;
                    items[0] = last;
                    items = items.Slice(1);
                    goto next;
                }
            }

            /// <summary>
            /// Computes the <see cref="Bounds"/> for a non-empty list.
            /// </summary>
            internal void _getBounds(SetBuilder<_Internal> cache, ref uint headBound, Span<uint> tailBounds)
            {
                if (_table is _Case[] cases)
                {
                    if (cache.Include(this))
                    {
                        if (headBound < (uint)cases.Length)
                            headBound = (uint)cases.Length;
                        if (tailBounds.Length > 0)
                        {
                            ref uint nHeadBound = ref tailBounds[0];
                            Span<uint> nTailBounds = tailBounds.Slice(1);
                            for (uint i = 0; i < cases.Length; i++)
                            {
                                _Case @case = cases[i];
                                if (!@case.Tail.IsNone)
                                    @case.Tail._getBounds(cache, ref nHeadBound, nTailBounds);
                            }
                        }
                    }
                }
                else
                {
                    Debug.Assert(tailBounds.Length == 0);
                    ulong[] bitmap = (ulong[])_table;
                    uint bound = new IndexSet(bitmap).Bound;
                    if (headBound < bound)
                        headBound = bound;
                }
            }

            /// <summary>
            /// Concatenates the specified set onto the end of this set.
            /// </summary>
            internal _Internal _concat(_Case other, MapBuilder<_Internal, _Internal> cache)
            {
                if (_table is null)
                    return None;
                if (cache.TryGet(this, out _Internal res))
                    return res;
                if (_table is _Case[] cases)
                {
                    _Case[] nCases = new _Case[cases.Length];
                    for (uint i = 0; i < (uint)nCases.Length; i++)
                        nCases[i] = _Case.Build(cases[i].Tail._concat(other, cache));
                    res = new _Internal(nCases);
                }
                else
                {
                    IndexSet item = new IndexSet((ulong[])_table);
                    _Case[] nCases = new _Case[item.Bound];
                    for (uint i = 0; i < (uint)nCases.Length; i++)
                    {
                        if (item.Contains(i))
                            nCases[i] = other;
                        else
                            nCases[i] = _Case.None;
                    }
                    res = new _Internal(nCases);
                }
                cache.Add(this, res);
                return res;
            }

            /// <summary>
            /// Gets the possible prefixes of the given length in this regular none-<see cref="None"/> set.
            /// </summary>
            internal _Internal _splitPrefix(uint prefixLen, MapBuilder<_Internal, _Internal> cache)
            {
                _Case[] cases = (_Case[])_table;
                Debug.Assert(prefixLen > 0);
                if (cache.TryGet(this, out _Internal res))
                    return res;
                if (prefixLen <= 1)
                {
                    // Construct bitmap for remaining non-none cases
                    Debug.Assert(prefixLen == 1);
                    Span<ulong> bitmap = stackalloc ulong[(int)BoolList._numWords((uint)cases.Length)];
                    for (uint i = 0; i < (uint)cases.Length; i++)
                        if (!cases[i].Tail.IsNone)
                            BoolList._setTrue(bitmap, i);
                    res = new _Internal(IndexSet._build(bitmap)._bitmap);
                }
                else
                {
                    // Recursively slice cases
                    _Case[] nCases = new _Case[cases.Length];
                    uint nPrefixLen = prefixLen - 1;
                    for (uint i = 0; i < (uint)cases.Length; i++)
                    {
                        _Case @case = cases[i];
                        if (@case.Tail.IsNone)
                            nCases[i] = _Case.None;
                        else
                            nCases[i] = _Case.Build(@case.Tail._splitPrefix(nPrefixLen, cache));
                    }
                    res = new _Internal(nCases);
                }
                cache.Add(this, res);
                return res;
            }

            /// <summary>
            /// Gets the possible suffixes after a prefix of the given length in this regular
            /// none-<see cref="None"/> set.
            /// </summary>
            internal RegularIndexListMap<RegularIndexListSet>._Internal _splitSuffix(uint prefixLen, uint suffixLen,
                MapBuilder<_Internal, RegularIndexListMap<RegularIndexListSet>._Internal> cache)
            {
                if (cache.TryGet(this, out var res))
                    return res;
                if (prefixLen == 0)
                {
                    var set = new RegularIndexListSet(suffixLen, this);
                    res = new RegularIndexListMap<RegularIndexListSet>._Internal(set);
                }
                else
                {
                    // Recursively split cases
                    _Case[] cases = (_Case[])_table;
                    var nCases = new RegularIndexListMap<RegularIndexListSet>._Case[cases.Length];
                    uint nPrefixLen = prefixLen - 1;
                    for (uint i = 0; i < (uint)cases.Length; i++)
                    {
                        _Case @case = cases[i];
                        if (@case.Tail.IsNone)
                            nCases[i] = RegularIndexListMap<RegularIndexListSet>._Case.Empty;
                        else
                            nCases[i] = RegularIndexListMap<RegularIndexListSet>._Case.Build(
                                @case.Tail._splitSuffix(nPrefixLen, suffixLen, cache));
                    }
                    res = new RegularIndexListMap<RegularIndexListSet>._Internal(nCases);
                }
                cache.Add(this, res);
                return res;
            }

            /// <summary>
            /// Gets the set of lists that result from prepending an item from the given set to this set.
            /// </summary>
            public _Internal Prepend(IndexSet item)
            {
                if (IsNone)
                    return None;
                _Case[] cases = new _Case[item.Bound];
                _Case @case = _Case.Build(this);
                for (uint i = 0; i < (uint)cases.Length; i++)
                {
                    if (item.Contains(i))
                        cases[i] = @case;
                    else
                        cases[i] = _Case.None;
                }
                return new _Internal(cases);
            }

            /// <summary>
            /// Gets the set of lists that result from prepending the given item to this set.
            /// </summary>
            public _Internal PrependSingleton(uint item)
            {
                if (IsNone)
                    return None;
                _Case[] cases = new _Case[item + 1];
                for (uint i = 0; i < item; i++)
                    cases[i] = _Case.None;
                cases[item] = _Case.Build(this);
                return new _Internal(cases);
            }

            /// <summary>
            /// Filters this set to only include lists where the given subsequence of items are in the given set.
            /// </summary>
            internal _Internal _intersectSubseq(
                MapBuilder<(_Internal, _Internal), _Internal> cache,
                ReadOnlySpan<uint> indices, _Internal other, uint offset)
            {
                if (other.IsNone)
                    return None;
                if (_table is _Case[] cases)
                {
                    var key = (this, other);
                    if (cache.TryGet(key, out _Internal res))
                        return res;

                    // Check if these cases correspond to a subsequence item
                    _Case[] nCases;
                    Scalar maxCaseLn = -Scalar.Inf;
                    uint nOffset = offset + 1;
                    if (offset == indices[0])
                    {
                        if (other._table is _Case[] otherCases)
                        {
                            // Intersect cases with otherCases
                            ReadOnlySpan<uint> nIndices = indices.Slice(1);
                            nCases = new _Case[Math.Min(cases.Length, otherCases.Length)];
                            for (int i = 0; i < nCases.Length; i++)
                            {
                                _Case @case = cases[i];
                                if (@case.Tail.IsNone)
                                    nCases[i] = _Case.None;
                                else
                                    nCases[i] = _Case.Build(@case.Tail._intersectSubseq(cache, nIndices, otherCases[i].Tail, nOffset));
                            }
                        }
                        else
                        {
                            // Intersect cases with other item
                            IndexSet otherItem = new IndexSet((ulong[])other._table);
                            nCases = new _Case[Math.Min(cases.Length, otherItem.Bound)];
                            for (uint i = 0; i < (uint)nCases.Length; i++)
                            {
                                _Case @case = cases[i];
                                if (@case.Tail.IsNone)
                                    nCases[i] = _Case.None;
                                else if (otherItem.Contains(i))
                                    nCases[i] = @case;
                                else
                                    nCases[i] = _Case.None;
                            }
                        }
                    }
                    else
                    {
                        // Intersect cases
                        nCases = new _Case[cases.Length];
                        for (int i = 0; i < nCases.Length; i++)
                        {
                            _Case @case = cases[i];
                            if (@case.Tail.IsNone)
                                nCases[i] = _Case.None;
                            else
                                nCases[i] = _Case.Build(@case.Tail._intersectSubseq(cache, indices, other, nOffset));
                        }
                    }

                    // Construct new set
                    res = Switch(nCases);
                    cache.Add(key, res);
                    return res;
                }
                else
                {
                    // Intersect length-one sets
                    IndexSet item = new IndexSet((ulong[])_table);
                    IndexSet otherItem = new IndexSet((ulong[])other._table);
                    IndexSet nItem = item & otherItem;
                    if (nItem.IsNone)
                        return None;
                    return new _Internal(nItem._bitmap);
                }
            }

            /// <summary>
            /// Constructs a new set by "pulling" the item at the given index to the beginning of the list. The
            /// list must be at least length 2.
            /// </summary>
            internal _Case[] _pull(MapBuilder<(uint, _Internal), _Case[]> cache, uint index)
            {
                return _pull(cache, index, (_Case[])_table);
            }

            /// <summary>
            /// Constructs a new set by "pulling" the item at the given index to the beginning of the list. The
            /// list must be at least length 2.
            /// </summary>
            internal static _Case[] _pull(MapBuilder<(uint, _Internal), _Case[]> cache, uint index, _Case[] cases)
            {
                if (index == 0)
                {
                    // Nothing to be done, the item is already at the beginning of the list.
                    return cases;
                }
                else
                {
                    // Check cache
                    var key = (index, new _Internal(cases));
                    if (cache.TryGet(key, out _Case[] res))
                        return res;

                    // Check whether this list is of length exactly 2, or whether it is longer
                    if (cases[cases.Length - 1].Tail._table is _Case[])
                    {
                        // Pull child cases
                        uint bound = 0;
                        uint nIndex = index - 1;
                        _Case[][] pullCases = new _Case[cases.Length][];
                        for (int i = 0; i < cases.Length; i++)
                        {
                            _Case @case = cases[i];
                            if (!@case.Tail.IsNone)
                            {
                                _Case[] pullCase = @case.Tail._pull(cache, nIndex);
                                if (bound < (uint)pullCase.Length)
                                    bound = (uint)pullCase.Length;
                                pullCases[i] = pullCase;
                            }
                            else
                            {
                                pullCases[i] = Array.Empty<_Case>();
                            }
                        }

                        // Build new cases by transposing pullCases
                        res = new _Case[bound];
                        for (uint i = 0; i < bound; i++)
                        {
                            _Case[] nPullCases = new _Case[cases.Length];
                            for (int j = 0; j < cases.Length; j++)
                            {
                                if (i < (uint)pullCases[j].Length)
                                    nPullCases[j] = pullCases[j][i];
                                else
                                    nPullCases[j] = _Case.None;
                            }
                            res[i] = _Case.Build(Switch(nPullCases));
                        }
                    }
                    else
                    {
                        // Determine bound for first item
                        Debug.Assert(index == 1);
                        uint bound = 0;
                        for (int i = 0; i < cases.Length; i++)
                        {
                            _Case @case = cases[i];
                            if (!@case.Tail.IsNone)
                            {
                                ulong[] caseBitmap = (ulong[])@case.Tail._table;
                                uint caseBound = new IndexSet(caseBitmap).Bound;
                                if (bound < caseBound)
                                    bound = caseBound;
                            }
                        }

                        // Build new cases by transposing existing cases
                        res = new _Case[bound];
                        uint numWords = BoolList._numWords((uint)cases.Length);
                        for (uint i = 0; i < bound; i++)
                        {
                            Span<ulong> bitmap = stackalloc ulong[(int)numWords];
                            for (uint j = 0; j < cases.Length; j++)
                            {
                                _Case @case = cases[j];
                                if (!@case.Tail.IsNone)
                                {
                                    ulong[] caseBitmap = (ulong[])@case.Tail._table;
                                    if (IndexSetBuilder._contains(caseBitmap, i))
                                        BoolList._setTrue(bitmap, j);
                                }
                            }
                            res[i] = _Case.Build(Independent(IndexSet._build(bitmap)));
                        }
                    }
                    cache.Add(key, res);
                    return res;
                }
            }

            /// <summary>
            /// Finds a minimal set of "factors" whose union is this set. Each factor is a concatenation of
            /// independent <see cref="RegularIndexListSet"/>s of the lengths specified by <paramref name="lens"/>.
            /// </summary>
            internal void _factorize(
                ReadOnlySpan<uint> lens,
                MapBuilder<_Internal, (Bag<(_Internal, _Internal)>, uint)> cache,
                BagBuilder<List<RegularIndexListSet>> res,
                Span<RegularIndexListSet> working,
                int offset)
            {
                uint prefixLen = lens[offset];
                int nOffset = offset + 1;
                if (nOffset == lens.Length)
                {
                    // The last component is trivially factorized
                    working[offset] = new RegularIndexListSet(prefixLen, this);
                    res.Add(List.Of(working));
                }
                else
                {
                    // Assemble factors based on prefix factorization
                    Debug.Assert(nOffset < lens.Length);
                    _factorize(lens.Slice(offset), cache, out var factorsByPrefix, out uint numFactors);
                    foreach (var (factorPrefix, suffix) in factorsByPrefix)
                    {
                        working[offset] = new RegularIndexListSet(prefixLen, factorPrefix);
                        suffix._factorize(lens, cache, res, working, nOffset);
                    }
                }
            }

            /// <summary>
            /// Finds a minimal set of "factors" whose union is this set. Each factor is a concatenation of
            /// independent <see cref="RegularIndexListSet"/>s of the lengths specified by <paramref name="lens"/>.
            /// </summary>
            /// <param name="factorsByPrefix">The set of prefixes for the minimal factorization of this set. The
            /// suffixes are provided by the factorization of the <see cref="_Internal"/> in the pair.</param>
            /// <param name="numFactors">The number of factors in the minimal factorization of this set.</param>
            internal void _factorize(
                ReadOnlySpan<uint> lens,
                MapBuilder<_Internal, (Bag<(_Internal, _Internal)>, uint)> cache,
                out Bag<(_Internal, _Internal)> factorsByPrefix,
                out uint numFactors)
            {
                if (lens.Length <= 1)
                {
                    // Trivial factorization into only one component
                    Debug.Assert(lens.Length == 1);
                    factorsByPrefix = Bag.Of((this, None));
                    numFactors = 1;
                }
                else
                {
                    // Check cache
                    if (cache.TryGet(this, out var res))
                    {
                        (factorsByPrefix, numFactors) = res;
                        return;
                    }

                    // Get potential suffixes
                    uint prefixLen = lens[0];
                    SetBuilder<_Internal> suffixesBuilder = new SetBuilder<_Internal>();
                    SetBuilder<_Internal> suffixCache = new SetBuilder<_Internal>();
                    _getSuffixes(suffixesBuilder, suffixCache, prefixLen);
                    List<_Internal> suffixes = suffixesBuilder.FinishSortArbitrary();

                    // Use partition refinement to split up suffixes into disjoint fragments such that each suffix
                    // can be constructed as the union of some subset of fragments.
                    var frags = new ListBuilder<(IndexSet, _Internal)>();
                    var intersectionCache = new MapBuilder<(_Internal, _Internal), _Internal>();
                    var differenceCache = new MapBuilder<(_Internal, _Internal), _Internal>();
                    _Internal[] prefixes = new _Internal[suffixes.Length];
                    for (uint i = 0; i < suffixes.Length; i++)
                    {
                        // Get prefix corresponding to this suffix
                        _Internal suffix = suffixes[i];
                        MapBuilder<_Internal, _Internal> chopCache = new MapBuilder<_Internal, _Internal>();
                        prefixes[i] = _chop(suffix, chopCache, prefixLen);

                        // Split current set of fragments based on what values intersect with this source, and what don't.
                        var nFrags = new ListBuilder<(IndexSet, _Internal)>();
                        var nFragSources = new ListBuilder<uint>();
                        foreach ((IndexSet fragSources, _Internal fragSet) in frags)
                        {
                            _Internal intersection = _intersection(intersectionCache, suffix, fragSet);
                            _Internal difference = _difference(differenceCache, fragSet, intersection);
                            suffix = _difference(differenceCache, suffix, intersection);

                            // Add component of frag that doesn't intersect with suffix
                            if (!difference.IsNone)
                                nFrags.Append((fragSources, difference));

                            // Add component of frag that intersects with suffix
                            if (!intersection.IsNone)
                                nFrags.Append((fragSources.Include(i), intersection));
                        }

                        // Add remaining suffix as a fragment
                        if (!suffix.IsNone)
                            nFrags.Append((IndexSet.Of(i), suffix));

                        // Swap to new list
                        frags = nFrags;
                    }

                    // Assign each valid suffix/fragment pair to an index
                    uint numFragSources = 0;
                    Span<uint> fragSourcesBaseIndex = stackalloc uint[(int)frags.Length];
                    for (uint i = 0; i < frags.Length; i++)
                    {
                        (IndexSet fragSources, _) = frags[i];
                        fragSourcesBaseIndex[(int)i] = numFragSources;
                        numFragSources += fragSources.Size;
                    }

                    // Construct a set of "formal concepts", which are ideal candidates for the factorization. Each
                    // formal concept corresponds to an intersection of some set of suffixes.
                    SetBuilder<IndexSet> conceptsBuilder = new SetBuilder<IndexSet>();
                    foreach ((IndexSet fragSources, _) in frags)
                    {
                        if (conceptsBuilder.Include(fragSources))
                        {
                            ListBuilder<IndexSet> addConcepts = new ListBuilder<IndexSet>();
                            foreach (IndexSet concept in conceptsBuilder)
                            {
                                IndexSet addConcept = fragSources & concept;
                                if (!addConcept.IsNone)
                                    addConcepts.Append(addConcept);
                            }
                            foreach (IndexSet concept in addConcepts)
                                conceptsBuilder.Include(concept);
                        }
                    }
                    List<IndexSet> concepts = conceptsBuilder.FinishSortArbitrary();

                    // We need to factorize each potential suffix to figure out the "cost" of including it in the final
                    // factorization.
                    Scalar[] costs = new Scalar[concepts.Length];
                    _Internal[] conceptSuffixes = new _Internal[concepts.Length];
                    Span<uint> numConceptSuffixFactors = stackalloc uint[(int)concepts.Length];
                    ReadOnlySpan<uint> nLens = lens.Slice(1);
                    for (uint i = 0; i < concepts.Length; i++)
                    {
                        IndexSet concept = concepts[i];
                        var en = concept.GetEnumerator();
                        bool hasItem = en.MoveNext();
                        Debug.Assert(hasItem);
                        _Internal conceptSuffix = suffixes[en.Current];
                        while (en.MoveNext())
                            conceptSuffix = _intersection(intersectionCache, conceptSuffix, suffixes[en.Current]);
                        conceptSuffixes[i] = conceptSuffix;

                        // Compute factorization of suffix
                        conceptSuffix._factorize(nLens, cache, out _, out numConceptSuffixFactors[(int)i]);
                        costs[i] = numConceptSuffixFactors[(int)i];
                    }

                    // Construct a table which relates fragments with concepts.
                    // Here, we reduce the factorization problem into a weighted minimal
                    // set cover problem to find the minimal basis.
                    // https://www.sciencedirect.com/science/article/pii/S0022000009000415
                    BoolImageBuilder2 optionsBuilder = new BoolImageBuilder2(numFragSources, concepts.Length);
                    for (uint i = 0; i < frags.Length; i++)
                    {
                        (IndexSet fragSources, _) = frags[i];
                        for (uint j = 0; j < concepts.Length; j++)
                        {
                            IndexSet concept = concepts[j];

                            // If the concept is formed by a subset of the suffixes the fragment is, then
                            // the concept covers the fragment for those suffixes.
                            if (concept <= fragSources)
                            {
                                uint fragSourceIndex = fragSourcesBaseIndex[(int)i];
                                foreach (uint fragSource in fragSources)
                                {
                                    if (concept.Contains(fragSource))
                                        optionsBuilder[fragSourceIndex, j] = true;
                                    fragSourceIndex++;
                                }
                            }
                        }
                    }

                    // Determine which fragments should be used for the factorization
                    BoolImage2 options = optionsBuilder.Finish();
                    IndexSet keyConceptIndices = BoolImage2.MinimalCover(options, new List<Scalar>(costs)).Support;

                    // Build out factors based on the key fragments
                    uint index = 0;
                    numFactors = 0;
                    var unionCache = new MapBuilder<(_Internal, _Internal), _Internal>();
                    var factorsByPrefixItems = new (_Internal, _Internal)[keyConceptIndices.Size];
                    foreach (uint keyConceptIndex in keyConceptIndices)
                    {
                        IndexSet concept = concepts[keyConceptIndex];
                        _Internal conceptSuffix = conceptSuffixes[keyConceptIndex];
                        var en = concept.GetEnumerator();
                        bool hasItem = en.MoveNext();
                        Debug.Assert(hasItem);
                        _Internal conceptPrefix = prefixes[en.Current];
                        while (en.MoveNext())
                            conceptPrefix = _union(unionCache, conceptPrefix, prefixes[en.Current]);
                        factorsByPrefixItems[index++] = (conceptPrefix, conceptSuffix);
                        numFactors += numConceptSuffixFactors[(int)keyConceptIndex];
                    }
                    factorsByPrefix = new Bag<(_Internal, _Internal)>(factorsByPrefixItems);

                    // Add to cache
                    cache.Add(this, (factorsByPrefix, numFactors));
                }
            }

            /// <summary>
            /// Finds all possible suffixes of this list after a prefix of the given length. The prefix
            /// length must be less than the length of the list.
            /// </summary>
            internal void _getSuffixes(
                SetBuilder<_Internal> suffixes,
                SetBuilder<_Internal> cache,
                uint prefixLen)
            {
                if (prefixLen == 0)
                {
                    suffixes.Include(this);
                }
                else
                {
                    if (cache.Include(this))
                    {
                        _Case[] cases = (_Case[])_table;
                        uint nPrefixLen = prefixLen - 1;
                        for (int i = 0; i < cases.Length; i++)
                        {
                            _Case @case = cases[i];
                            if (!@case.Tail.IsNone)
                                @case.Tail._getSuffixes(suffixes, cache, nPrefixLen);
                        }
                    }
                }
            }

            /// <summary>
            /// Gets an <see cref="_Internal"/> of the prefix of this distribution that has a desired suffix.
            /// </summary>
            internal _Internal _chop(
                _Internal suffix,
                MapBuilder<_Internal, _Internal> cache,
                uint prefixLen)
            {
                if (cache.TryGet(this, out _Internal prefix))
                    return prefix;
                _Case[] cases = (_Case[])_table;
                if (prefixLen <= 1)
                {
                    Debug.Assert(prefixLen == 1);
                    Span<ulong> bitmap = stackalloc ulong[(int)BoolList._numWords((uint)cases.Length)];
                    for (uint i = 0; i < (uint)cases.Length; i++)
                    {
                        _Case @case = cases[i];
                        if (!@case.Tail.IsNone && @case.Tail == suffix)
                            BoolList._setTrue(bitmap, i);
                    }
                    prefix = Independent(IndexSet._build(bitmap));
                }
                else
                {
                    uint nPrefixLen = prefixLen - 1;
                    _Case[] nCases = new _Case[cases.Length];
                    for (int i = 0; i < cases.Length; i++)
                    {
                        _Case @case = cases[i];
                        if (!@case.Tail.IsNone)
                            nCases[i] = _Case.Build(@case.Tail._chop(suffix, cache, nPrefixLen));
                    }
                    prefix = Switch(nCases);
                }
                cache.Add(this, prefix);
                return prefix;
            }

            /// <summary>
            /// Gets the members of this set in lexicographical order.
            /// </summary>
            internal void _sort(ListBuilder<List<uint>> lists, Span<uint> items, int offset)
            {
                if (!(_table is null))
                {
                    if (_table is _Case[] cases)
                    {
                        int nOffset = offset + 1;
                        for (uint i = 0; i < (uint)cases.Length; i++)
                        {
                            items[offset] = i;
                            cases[i].Tail._sort(lists, items, nOffset);
                        }
                    }
                    else
                    {
                        IndexSet item = new IndexSet((ulong[])_table);
                        foreach (uint value in item)
                        {
                            items[offset] = value;
                            lists.Append(List.Of(items));
                        }
                    }
                }
            }
            
            /// <summary>
            /// Determines whether two <see cref="_Internal"/>s are equivalent without shortcutting based on
            /// reference.
            /// </summary>
            public static bool AreEqual(_Internal a, _Internal b)
            {
                if (a._table is null)
                    return b._table is null;
                if (b._table is null)
                    return false;
                if (a._table is _Case[] aCases)
                {
                    if (!(b._table is _Case[] bCases))
                        return false;
                    if (aCases.Length != bCases.Length)
                        return false;
                    for (int i = 0; i < aCases.Length; i++)
                        if (!_Case.AreEqual(ref aCases[i], ref bCases[i]))
                            return false;
                    return true;
                }
                else
                {
                    ulong[] aBitmap = (ulong[])a._table;
                    if (!(b._table is ulong[] bBitmap))
                        return false;
                    return new IndexSet(aBitmap) == new IndexSet(bBitmap);
                }
            }

            public static bool operator ==(_Internal a, _Internal b)
            {
                if (a._table == b._table)
                    return true;
                return AreEqual(a, b);
            }

            public static bool operator !=(_Internal a, _Internal b)
            {
                return !(a == b);
            }

            public override bool Equals(object obj)
            {
                if (!(obj is _Internal other))
                    return false;
                return this == other;
            }

            bool IEquatable<_Internal>.Equals(_Internal other)
            {
                return this == other;
            }

            public override int GetHashCode()
            {
                if (_table is null)
                {
                    return 0;
                }
                else if (_table is _Case[] cases)
                {
                    int hash = 0x3a8cac81;
                    for (int i = 0; i < cases.Length; i++)
                        hash = HashCodeHelper.Combine(hash, cases[i].Hash);
                    return hash;
                }
                else
                {
                    ulong[] bitmap = (ulong[])_table;
                    return new IndexSet(bitmap).GetHashCode();
                }
            }
        }

        /// <summary>
        /// Describes the contents of an <see cref="RegularIndexListSet"/> assuming a particular value for the first item.
        /// </summary>
        internal struct _Case
        {
            private _Case(int hash, _Internal tail)
            {
                Hash = hash;
                Tail = tail;
            }

            /// <summary>
            /// Builds a <see cref="_Case"/> from the given set.
            /// </summary>
            public static _Case Build(_Internal tail)
            {
                return new _Case(tail.GetHashCode(), tail);
            }

            /// <summary>
            /// The hash code for this case.
            /// </summary>
            public int Hash { get; }

            /// <summary>
            /// Describes the remaining items of the set.
            /// </summary>
            public _Internal Tail;

            /// <summary>
            /// The <see cref="_Case"/> corresponding to <see cref="RegularIndexListSet.None"/>.
            /// </summary>
            public static _Case None => default;

            /// <summary>
            /// Indicates whether this is <see cref="None"/>.
            /// </summary>
            public bool IsNone => Tail.IsNone;

            /// <summary>
            /// Determines whether two <see cref="_Case"/>s are the same. If so, this may coalesce them to make that
            /// determination faster in the future.
            /// </summary>
            public static bool AreEqual(ref _Case a, ref _Case b)
            {
                if (ReferenceEquals(a.Tail._table, b.Tail._table))
                    return true;
                if (a.Hash != b.Hash)
                    return false;
                if (_Internal.AreEqual(a.Tail, b.Tail))
                {
                    Util.Coalesce(ref a.Tail._table, ref b.Tail._table);
                    return true;
                }
                return false;
            }
        }

        public static RegularIndexListSet operator +(RegularIndexListSet a, RegularIndexListSet b)
        {
            return Concat(a, b);
        }

        public static RegularIndexListSet operator |(RegularIndexListSet a, RegularIndexListSet b)
        {
            return Union(a, b);
        }

        public static RegularIndexListSet operator &(RegularIndexListSet a, RegularIndexListSet b)
        {
            return Intersection(a, b);
        }

        public static RegularIndexListSet operator %(RegularIndexListSet a, RegularIndexListSet b)
        {
            return Difference(a, b);
        }

        public static explicit operator RegularIndexListSet(Set<List<uint>> source)
        {
            // Search for first item to establish the length of the set
            uint len;
            uint index = 0;
            while (index < source._entries.Length)
            {
                var entry = source._entries[index++];
                if (entry.Hash != 0)
                {
                    len = entry.Value.Length;
                    goto foundOne;
                }
            }
            return None;

        foundOne:
            // Determine number of lists in the set and verify all the lists have the same length
            uint numLists = 1;
            while (index < source._entries.Length)
            {
                var entry = source._entries[index++];
                if (entry.Hash != 0)
                {
                    if (entry.Value.Length != len)
                        throw new InvalidCastException("All lists must have the same length");
                    numLists++;
                }
            }

            // Construct an index table pointing to all the lists
            Span<uint> indices = stackalloc uint[(int)numLists];
            int head = 0;
            for (uint i = 0; i < source._entries.Length; i++)
                if (source._entries[i].Hash != 0)
                    indices[head++] = i;

            // Build set
            return new RegularIndexListSet(len, _Internal._fromSet(source._entries, indices, 0));
        }

        IEnumerator<List<uint>> IEnumerable<List<uint>>.GetEnumerator()
        {
            // TODO: We could make this more efficient
            return Sort().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            // TODO: We could make this more efficient
            return Sort().GetEnumerator();
        }

        public static bool operator ==(RegularIndexListSet a, RegularIndexListSet b)
        {
            return a.Length == b.Length && a._source == b._source;
        }

        public static bool operator !=(RegularIndexListSet a, RegularIndexListSet b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is RegularIndexListSet))
                return false;
            return this == (RegularIndexListSet)obj;
        }

        bool IEquatable<RegularIndexListSet>.Equals(RegularIndexListSet other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return HashCodeHelper.Combine(Length.GetHashCode(), _source.GetHashCode());
        }

        public override string ToString()
        {
            if (IsNone)
            {
                return "∅";
            }
            else if (IsSingleton(out List<uint> value))
            {
                return "{" + value.ToString() + "}";
            }
            else
            {
                uint size = Size;
                if (size == uint.MaxValue)
                    return "{[... <" + Length.ToStringInvariant() + ">], ...}";
                else
                    return "{[... <" + Length.ToStringInvariant() + ">], ... <" + size.ToStringInvariant() + ">}";
            }
        }
    }
    
    /// <summary>
    /// A debugger type proxy for <see cref="RegularIndexListSet"/>s.
    /// </summary>
    public sealed class RegularIndexListSetDebugView
    {
        private readonly RegularIndexListSet _source;
        public RegularIndexListSetDebugView(RegularIndexListSet source)
        {
            _source = source;
        }

        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public RegularIndexListSet[] Cases
        {
            get
            {
                if (_source._source._table is RegularIndexListSet._Case[] cases)
                {
                    uint nLen = _source.Length - 1;
                    RegularIndexListSet[] nCases = new RegularIndexListSet[cases.Length];
                    for (int i = 0; i < nCases.Length; i++)
                    {
                        if (cases[i].IsNone)
                            nCases[i] = RegularIndexListSet.None;
                        else
                            nCases[i] = new RegularIndexListSet(nLen, cases[i].Tail);
                    }
                    return nCases;
                }
                else if (_source._source._table is ulong[] bitmap)
                {
                    IndexSet set = new IndexSet(bitmap);
                    RegularIndexListSet[] nCases = new RegularIndexListSet[set.Bound];
                    for (uint i = 0; i < (uint)nCases.Length; i++)
                        nCases[i] = set.Contains(i) ? RegularIndexListSet.SingletonEmpty : RegularIndexListSet.None;
                    return nCases;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
