﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Collections.Generic;

namespace Alunite.Data
{
    /// <summary>
    /// A type with only one valid value.
    /// </summary>
    public struct Unit
    {
        /// <summary>
        /// The only valid value for <see cref="Unit"/>.
        /// </summary>
        public static readonly Unit Value = default(Unit);
    }

    /// <summary>
    /// Can either be a value of type <see cref="T"/> or indicate the lack of such a value.
    /// </summary>
    public struct Option<T>
    {
        public bool HasValue;
        public T Value;
        public Option(T value)
        {
            HasValue = true;
            Value = value;
        }

        /// <summary>
        /// Constructs an <see cref="Option{T}"/> with a value.
        /// </summary>
        public static Option<T> Some(T value)
        {
            return new Option<T>(value);
        }

        /// <summary>
        /// An <see cref="Option{T}"/> without a value.
        /// </summary>
        public static Option<T> None => default;

        /// <summary>
        /// Gets the value of this option, if available, or returns false otherwise.
        /// </summary>
        public bool TryGet(out T value)
        {
            value = Value;
            return HasValue;
        }
    }

    /// <summary>
    /// Contains functions related to <see cref="Option{T}"/>
    /// </summary>
    public static class Option
    {
        /// <summary>
        /// Constructs an <see cref="Option{T}"/> with a value.
        /// </summary>
        public static Option<T> Some<T>(T value)
        {
            return Option<T>.Some(value);
        }

        /// <summary>
        /// Gets an empty <see cref="Option{T}"/>.
        /// </summary>
        public static Option<T> None<T>()
        {
            return Option<T>.None;
        }
    }

    
    /// <summary>
    /// A debugger type proxy for <see cref="IEnumerable{T}"/>s.
    /// </summary>
    public sealed class EnumerableDebugView<T>
    {
        private readonly IEnumerable<T> _enumerable;
        public EnumerableDebugView(IEnumerable<T> enumerable)
        {
            _enumerable = enumerable;
        }

        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public T[] Items => _enumerable.ToArray();
    }
}
