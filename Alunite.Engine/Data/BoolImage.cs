﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Alunite.Data
{
    /// <summary>
    /// A specialization of <see cref="List{T}"/> for boolean values.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    [DebuggerTypeProxy(typeof(EnumerableDebugView<>))]
    public struct BoolList : IEquatable<BoolList>, IEnumerable<bool>
    {
        private ulong[] _bitmap;
        internal BoolList(uint len, ulong[] bitmap)
        {
            Length = len;
            _bitmap = bitmap;
        }

        /// <summary>
        /// The length of this list.
        /// </summary>
        public uint Length { get; }

        /// <summary>
        /// Gets the item at the given index.
        /// </summary>
        public bool this[uint index]
        {
            get
            {
                if (!(index < Length))
                    throw new IndexOutOfRangeException();
                return (_bitmap[index / 64] & (1ul << (int)(index % 64))) != 0;
            }
        }

        /// <summary>
        /// The empty <see cref="BoolList"/>.
        /// </summary>
        public static BoolList Empty => new BoolList(0, Array.Empty<ulong>());

        /// <summary>
        /// Constructs a <see cref="BoolList"/> of the given length that is <see cref="true"/> only at the indices
        /// specified in <paramref name="indices"/>. This is the inverse of <see cref="Support"/>.
        /// </summary>
        public static BoolList Indicator(uint len, IndexSet indices)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Constructs a <see cref="BoolList"/> of the given length from the given bitmap.
        /// </summary>
        internal static BoolList _build(uint len, ReadOnlySpan<ulong> bitmap)
        {
            Debug.Assert(_numWords(len) == (uint)bitmap.Length);
            return new BoolList(len, bitmap.ToArray());
        }

        /// <summary>
        /// Determines the number of <see cref="ulong"/>s required for a bitmap to store
        /// a <see cref="BoolList"/> of the given length.
        /// </summary>
        internal static uint _numWords(uint len)
        {
            return (len + 63) / 64;
        }
        
        /// <summary>
        /// Indicates whether this is the empty list.
        /// </summary>
        public bool IsEmpty => Length == 0;

        /// <summary>
        /// The set of indices for which this list is <see cref="true"/>.
        /// </summary>
        public IndexSet Support => IndexSet._build(_bitmap);

        /// <summary>
        /// Gets the item at the given index in a list with the given bitmap.
        /// </summary>
        internal static bool _get(ReadOnlySpan<ulong> bitmap, uint index)
        {
            return (bitmap[(int)(index / 64)] & (1ul << (int)(index % 64))) != 0;
        }

        /// <summary>
        /// Sets the item at the given index in a list with the given bitmap.
        /// </summary>
        internal static void _set(Span<ulong> bitmap, uint index, bool value)
        {
            if (value)
                _setTrue(bitmap, index);
            else
                _setFalse(bitmap, index);
        }

        /// <summary>
        /// Sets the item at the given index in a list with the given bitmap.
        /// </summary>
        internal static void _setTrue(Span<ulong> bitmap, uint index)
        {
            bitmap[(int)(index / 64)] |= 1ul << (int)(index % 64);
        }

        /// <summary>
        /// Sets the item at the given index in a list with the given bitmap.
        /// </summary>
        internal static void _setFalse(Span<ulong> bitmap, uint index)
        {
            bitmap[(int)(index / 64)] &= ~(1ul << (int)(index % 64));
        }

        /// <summary>
        /// Gets an enumerator for this list.
        /// </summary>
        public Enumerator GetEnumerator()
        {
            return new Enumerator(Length, _bitmap);
        }

        IEnumerator<bool> IEnumerable<bool>.GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// An enumerator for a <see cref="BoolList"/>
        /// </summary>
        public struct Enumerator : IEnumerator<bool>
        {
            private uint _len;
            private ulong[] _bitmap;
            private uint _index;
            internal Enumerator(uint len, ulong[] bitmap)
            {
                _len = len;
                _bitmap = bitmap;
                _index = uint.MaxValue;
            }

            /// <summary>
            /// Gets the current item for this enumerator.
            /// </summary>
            public bool Current => _get(_bitmap, _index);

            /// <summary>
            /// Advances to the next, or first, item.
            /// </summary>
            public bool MoveNext()
            {
                unchecked { _index++; }
                return _index < (uint)_len;
            }

            /// <summary>
            /// Resets the enumerator to before the beginning of the first item.
            /// </summary>
            public void Reset()
            {
                _index = uint.MaxValue;
            }

            object IEnumerator.Current => Current;

            void IDisposable.Dispose() { }
        }

        public static implicit operator List<bool>(BoolList list)
        {
            if (list.IsEmpty)
                return List<bool>.Empty;
            bool[] items = new bool[list.Length];
            for (uint i = 0; i < list.Length; i++)
                items[i] = list[i];
            return new List<bool>(items);
        }

        public static bool operator ==(BoolList a, BoolList b)
        {
            if (a.Length != b.Length)
                return false;
            for (int i = 0; i < a._bitmap.Length; i++)
                if (a._bitmap[i] != b._bitmap[i])
                    return false;
            return true;
        }

        public static bool operator !=(BoolList a, BoolList b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is BoolList))
                return false;
            return this == (BoolList)obj;
        }

        bool IEquatable<BoolList>.Equals(BoolList other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            int hash = _bitmap.Length;
            for (int i = 0; i < _bitmap.Length; i++)
                hash = HashCodeHelper.Combine(hash, EqualityHelper.GetHashCode(_bitmap[i]));
            return hash;
        }

        public override string ToString()
        {
            return ((List<bool>)this).ToString();
        }
    }

    /// <summary>
    /// A helper class for constructing a <see cref="BoolList"/> by incrementally appending items, or by updating
    /// existing items.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    [DebuggerTypeProxy(typeof(EnumerableDebugView<>))]
    public sealed class BoolListBuilder : IEnumerable<bool>
    {
        private ulong[] _bitmap;
        private uint _len;
        private BoolListBuilder(ulong[] bitmap, uint len)
        {
            _bitmap = bitmap;
            _len = len;
        }

        /// <summary>
        /// Creates an initially-empty <see cref="BoolListBuilder"/>.
        /// </summary>
        public static BoolListBuilder CreateEmpty()
        {
            return new BoolListBuilder(Array.Empty<ulong>(), 0);
        }

        /// <summary>
        /// Creates an initially-empty <see cref="BoolListBuilder"/>.
        /// </summary>
        /// <param name="capacity">The number of items expected to be in the list.</param>
        public static BoolListBuilder CreateEmpty(uint capacity)
        {
            return new BoolListBuilder(new ulong[BoolList._numWords(capacity)], 0);
        }

        /// <summary>
        /// Creates a <see cref="BoolListBuilder"/> which is initially set to the list of <paramref name="len"/>
        /// <see cref="false"/>s.
        /// </summary>
        public static BoolListBuilder CreateReplicateFalse(uint len)
        {
            return new BoolListBuilder(new ulong[BoolList._numWords(len)], len);
        }

        /// <summary>
        /// The current length of the list under construction.
        /// </summary>
        public uint Length => _len;

        /// <summary>
        /// Gets or sets the value of the item at the given index.
        /// </summary>
        public bool this[uint index]
        {
            get
            {
                return BoolList._get(_bitmap, index);
            }
            set
            {
                BoolList._set(_bitmap, index, value);
            }
        }


        /// <summary>
        /// Gets the <see cref="BoolList"/> resulting from this builder.
        /// </summary>
        public BoolList Finish()
        {
            BoolList res = new BoolList(_len, _bitmap);
            _bitmap = null; // Ensure the builder can not be used to modify the bitmap anymore
            return res;
        }

        /// <summary>
        /// Gets an enumerator for this list.
        /// </summary>
        public BoolList.Enumerator GetEnumerator()
        {
            return new BoolList.Enumerator(_len, _bitmap);
        }

        IEnumerator<bool> IEnumerable<bool>.GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override string ToString()
        {
            return ((List<bool>)new BoolList(_len, _bitmap)).ToString();
        }
    }

    /// <summary>
    /// A specialization of <see cref="Image2{T}"/> for boolean values.
    /// </summary>
    public struct BoolImage2
    {
        internal byte _logStride;
        internal uint _size_x;
        internal ulong[] _bitmap;

        /// <summary>
        /// The length of this image along the X axis.
        /// </summary>
        public uint Size_X => _size_x;

        /// <summary>
        /// The length of this image along the Y axis.
        /// </summary>
        public uint Size_Y => (uint)_bitmap.Length >> _logStride;

        /// <summary>
        /// Gets the contents for a row of this image.
        /// </summary>
        public BoolList this[uint y]
        {
            get
            {
                uint numWords = BoolList._numWords(_size_x);
                return BoolList._build(_size_x, _bitmap.AsSpan((int)(y << _logStride), (int)numWords));
            }
        }

        /// <summary>
        /// Gets the value of an element of this image.
        /// </summary>
        public bool this[uint x, uint y]
        {
            get
            {
                if (!(x < Size_X))
                    throw new IndexOutOfRangeException();
                return (_bitmap[(y << _logStride) | (x / 64)] & (1ul << (int)(x % 64))) != 0;
            }
        }

        /// <summary>
        /// Constructs a <see cref="BoolImage2"/> with rows of the given length that are <see cref="true"/> only at
        /// the indices specified in <paramref name="indices"/>.
        /// </summary>
        public static BoolImage2 Indicator(uint size_x, ReadOnlySpan<IndexSet> indices)
        {
            // Create bitmap
            uint stride = Bitwise.UpperPow2(BoolList._numWords(size_x));
            byte logStride = (byte)Bitwise.Log2(stride);
            ulong[] bitmap = new ulong[indices.Length << logStride];

            // Populate bitmap
            for (uint y = 0; y < (uint)indices.Length; y++)
            {
                uint i = y << logStride;
                ulong[] indicesBitmap = indices[(int)y]._bitmap;
                uint j = 0;
                while (j < indicesBitmap.Length)
                    bitmap[i++] = indicesBitmap[j++];
            }

            // Construct image
            return new BoolImage2
            {
                _logStride = logStride,
                _size_x = size_x,
                _bitmap = bitmap
            };
        }

        /// <summary>
        /// Gets the component-wise logical or of all rows of <paramref name="b"/> whose row index
        /// is true in <paramref name="a"/>.
        /// </summary>
        public static BoolList LogicalMultiply(BoolList a, BoolImage2 b)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the bitmap for a particular row of this image.
        /// </summary>
        internal ReadOnlySpan<ulong> _rowBitmap(uint y)
        {
            return _bitmap.AsSpan((int)(y << _logStride), 1 << _logStride);
        }

        /// <summary>
        /// Finds a minimal set of rows of <paramref name="options"/> such that their
        /// component-wise logical or is a list of all <see cref="true"/>s. i.e. The <see cref="LogicalMultiply"/>
        /// of the returned list with <paramref name="options"/> is a list of all <see cref="true"/>s.
        /// </summary>
        public static BoolList MinimalCover(BoolImage2 options)
        {
            Scalar[] costs = new Scalar[(int)options.Size_Y];
            for (int i = 0; i < costs.Length; i++)
                costs[i] = 1.0;
            return MinimalCover(options, new List<Scalar>(costs));
        }

        /// <summary>
        /// Finds a minimal-cost set of rows of <paramref name="options"/> such that their
        /// component-wise logical or is a list of all <see cref="true"/>s. i.e. The <see cref="LogicalMultiply"/>
        /// of the returned list with <paramref name="options"/> is a list of all <see cref="true"/>s.
        /// </summary>
        /// <param name="costs">Provides the cost associated with each option in <paramref name="options"/></param>
        public static BoolList MinimalCover(BoolImage2 options, List<Scalar> costs)
        {
            uint numItems = options.Size_X;
            uint numOptions = options.Size_Y;

            // Compute item "values" based on how easy it would be to include in an option
            Scalar[] itemValues = new Scalar[numItems];
            for (uint i = 0; i < numItems; i++)
            {
                ref Scalar itemValue = ref itemValues[i];
                for (uint j = 0; j < options.Size_Y; j++)
                    if (options[i, j])
                        itemValue += 1 / costs[j];
            }
            Scalar totalValue = 0;
            Scalar minItemValue = Scalar.Inf;
            for (uint i = 0; i < numItems; i++)
            {
                ref Scalar itemValue = ref itemValues[i];
                if (itemValue == 0)
                    throw new Exception("No solution");
                itemValue = 1 / itemValue;
                if (itemValue < minItemValue)
                    minItemValue = itemValue;
                totalValue += itemValue;
            }

            // Searches for potential solutions involving the given subset of candidates. This makes
            // no additional assumptions about the situation.
            void searchGeneral(
                Span<ulong> candidateBitmap,
                Span<ulong> solutionBitmap,
                Scalar curTotal, Scalar remValue,
                Span<ulong> itemsBitmap,
                ref Scalar bestTotal, ref BoolList best)
            {
                // Determine which items can be populated by the remaining candidates, and which can
                // be populated by exactly one of the remaining candidates
                Span<ulong> possibleItemsBitmap = stackalloc ulong[itemsBitmap.Length];
                Span<ulong> onlyItemsBitmap = stackalloc ulong[itemsBitmap.Length];
                for (uint i = 0; i < numOptions; i++)
                {
                    if (BoolList._get(candidateBitmap, i))
                    {
                        ReadOnlySpan<ulong> optionBitmap = options._rowBitmap(i);
                        for (int j = 0; j < itemsBitmap.Length; j++)
                        {
                            onlyItemsBitmap[j] &= ~optionBitmap[j];
                            onlyItemsBitmap[j] |= optionBitmap[j] & ~possibleItemsBitmap[j];
                            possibleItemsBitmap[j] |= optionBitmap[j];
                        }
                    }
                }

                // Is a solution even possible at this point?
                for (int i = 0; i < itemsBitmap.Length; i++)
                    if ((possibleItemsBitmap[i] | itemsBitmap[i]) != 0xFFFFFFFFFFFFFFFF)
                        return;

                // Check if there are any items that can only be populated by one candidate
                bool hasOnlyItems = false;
                for (int i = 0; i < onlyItemsBitmap.Length; i++)
                {
                    onlyItemsBitmap[i] &= ~itemsBitmap[i];
                    hasOnlyItems |= onlyItemsBitmap[i] != 0;
                }
                if (hasOnlyItems)
                {
                    // Include the solutions that must be included
                    Span<ulong> includedBitmap = stackalloc ulong[candidateBitmap.Length];
                    for (uint i = 0; i < numOptions; i++)
                    {
                        if (BoolList._get(candidateBitmap, i))
                        {
                            ReadOnlySpan<ulong> optionBitmap = options._rowBitmap(i);
                            for (int j = 0; j < itemsBitmap.Length; j++)
                                if ((optionBitmap[j] & onlyItemsBitmap[j]) != 0)
                                    goto include;
                            continue;
                        include:;
                            curTotal += costs[i];
                            BoolList._setTrue(includedBitmap, i);
                            for (int j = 0; j < itemsBitmap.Length; j++)
                                itemsBitmap[j] |= optionBitmap[j];
                        }
                    }
                    for (int i = 0; i < includedBitmap.Length; i++)
                    {
                        candidateBitmap[i] &= ~includedBitmap[i];
                        solutionBitmap[i] |= includedBitmap[i];
                    }

                    // Recompute remaining value
                    remValue = 0;
                    for (uint i = 0; i < numItems; i++)
                        if (!BoolList._get(itemsBitmap, i))
                            remValue += itemValues[i];

                    // Perform refined search on remaining sets
                    searchRefined(candidateBitmap, solutionBitmap, curTotal, remValue, itemsBitmap, ref bestTotal, ref best);

                    // Clean up candidate and solution set
                    for (int i = 0; i < includedBitmap.Length; i++)
                    {
                        candidateBitmap[i] |= includedBitmap[i];
                        solutionBitmap[i] &= ~includedBitmap[i];
                    }
                }
                else
                {
                    // Perform refined search on remaining sets
                    searchRefined(candidateBitmap, solutionBitmap, curTotal, remValue, itemsBitmap, ref bestTotal, ref best);
                }
            }

            // Searches for potential solutions involving the given subset of candidates, assuming that
            // a solution has not already been reached, and the solution set has already been reduced.
            void searchRefined(
                Span<ulong> candidateBitmap,
                Span<ulong> solutionBitmap,
                Scalar curTotal, Scalar remValue,
                Span<ulong> itemsBitmap,
                ref Scalar bestTotal, ref BoolList best)
            {
                // Exit immediately if we can't do better than the best found so far
                if (curTotal >= bestTotal)
                    return;
                
                // If there are no remaining items, we have a solution
                if (remValue <= 0)
                {
                    bestTotal = curTotal;
                    best = BoolList._build(numOptions, solutionBitmap);
                    return;
                }

                // Determine the candidate that covers the items with the most total value
                uint bestCandidateIndex = uint.MaxValue;
                Scalar bestCandidateMargin = 0;
                Scalar bestCandidateValue = 0;
                Span<ulong> possibleItemsBitmap = stackalloc ulong[itemsBitmap.Length];
                Span<ulong> onlyItemsBitmap = stackalloc ulong[itemsBitmap.Length];
                for (uint i = 0; i < numOptions; i++)
                {
                    if (BoolList._get(candidateBitmap, i))
                    {
                        Scalar candidateValue = 0;
                        ReadOnlySpan<ulong> optionBitmap = options._rowBitmap(i);
                        for (uint j = 0; j < numItems; j++)
                            if (BoolList._get(optionBitmap, j) && !BoolList._get(itemsBitmap, j))
                                candidateValue += itemValues[j];
                        Scalar candidateMargin = candidateValue / costs[i];
                        if (candidateMargin > bestCandidateMargin)
                        {
                            bestCandidateIndex = i;
                            bestCandidateMargin = candidateMargin;
                            bestCandidateValue = candidateValue;
                        }
                    }
                }
                Debug.Assert(bestCandidateIndex < uint.MaxValue);

                // Is it possible to find a better solution?
                if ((bestTotal - curTotal) * bestCandidateMargin <= remValue)
                    return;
                
                // First, assume that the best candidate will be included and see if that yields a solution
                ReadOnlySpan<ulong> bestOptionBitmap = options._rowBitmap(bestCandidateIndex);
                BoolList._setFalse(candidateBitmap, bestCandidateIndex);
                BoolList._setTrue(solutionBitmap, bestCandidateIndex);
                Span<ulong> nItemsBitmap = stackalloc ulong[itemsBitmap.Length];
                for (int i = 0; i < itemsBitmap.Length; i++)
                    nItemsBitmap[i] = itemsBitmap[i] | bestOptionBitmap[i];
                Scalar nTotal = curTotal + costs[bestCandidateIndex];
                Scalar nRemValue = remValue - bestCandidateValue;
                searchRefined(candidateBitmap, solutionBitmap, nTotal, nRemValue, nItemsBitmap, ref bestTotal, ref best);

                // Next, assume that the best candidate will be excluded and see if that yields a solution
                BoolList._setFalse(solutionBitmap, bestCandidateIndex);
                searchGeneral(candidateBitmap, solutionBitmap, curTotal, remValue, itemsBitmap, ref bestTotal, ref best);

                // Clean up search bitmap
                BoolList._setTrue(candidateBitmap, bestCandidateIndex);
            }

            // Kick off the recursive process to find the best set
            {
                Span<ulong> candidateBitmap = stackalloc ulong[(int)BoolList._numWords(numOptions)];
                IndexSetBuilder._includeBelow(candidateBitmap, numOptions);
                Span<ulong> solutionBitmap = stackalloc ulong[candidateBitmap.Length];
                Span<ulong> itemsBitmap = stackalloc ulong[(int)BoolList._numWords(numItems)];
                itemsBitmap[itemsBitmap.Length - 1] = ~Bitwise.MaskFirst(options.Size_X % 64);
                Scalar bestTotal = Scalar.Inf;
                BoolList best = default;
                Scalar startValue = totalValue - minItemValue * 0.5;
                searchGeneral(candidateBitmap, solutionBitmap, 0, startValue, itemsBitmap, ref bestTotal, ref best);
                Debug.Assert(bestTotal < Scalar.Inf);
                return best;
            }
        }
    }

    /// <summary>
    /// A helper class for constructing an <see cref="BoolImage2"/> by incremental element modifications.
    /// </summary>
    public sealed class BoolImageBuilder2 : IBuilder<BoolImage2>
    {
        private byte _logStride;
        private uint _size_x;
        private ulong[] _bitmap;
        public BoolImageBuilder2(uint size_x, uint size_y)
        {
            _size_x = size_x;
            uint stride = Bitwise.UpperPow2(BoolList._numWords(size_x));
            _logStride = (byte)Bitwise.Log2(stride);
            _bitmap = new ulong[size_y << _logStride];
        }

        /// <summary>
        /// The length of this image along the X axis.
        /// </summary>
        public uint Size_X => _size_x;

        /// <summary>
        /// The length of this image along the Y axis.
        /// </summary>
        public uint Size_Y => (uint)_bitmap.Length >> _logStride;

        /// <summary>
        /// Gets or sets the value of an element of this image.
        /// </summary>
        public bool this[uint x, uint y]
        {
            get
            {
                if (!(x < Size_X))
                    throw new IndexOutOfRangeException();
                return (_bitmap[(y << _logStride) | (x / 64)] & (1ul << (int)(x % 64))) != 0;
            }
            set
            {
                if (!(x < Size_X))
                    throw new IndexOutOfRangeException();
                if (value)
                    _bitmap[(y << _logStride) | (x / 64)] |= 1ul << (int)(x % 64);
                else
                    _bitmap[(y << _logStride) | (x / 64)] &= ~(1ul << (int)(x % 64));
            }
        }

        /// <summary>
        /// Gets the <see cref="BoolImage2"/> resulting from this builder.
        /// </summary>
        public BoolImage2 Finish()
        {
            return new BoolImage2
            {
                _logStride = _logStride,
                _size_x = _size_x,
                _bitmap = _bitmap
            };
        }
    }
}
