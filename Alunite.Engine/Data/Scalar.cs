﻿using System;
using System.Diagnostics;

using Alunite.Data.Probability;
using Alunite.Data.Serialization;

namespace Alunite.Data
{
    /// <summary>
    /// Approximates a real number given certain assumptions about its range and distribution.
    /// </summary>
    /// <remarks>More technically, a <see cref="Scalar"/> is a probability distribution of real numbers, representing
    /// the likelihood of a particular number being the "true" intended value. Operations on <see cref="Scalar"/>s
    /// act on the potential values of their underlying distributions and return the distribution which is
    /// the best fit for the result, while also factoring in an apriori distribution of the intended
    /// result. The individual <see cref="Scalar"/> distributions are designed to allow fast computations for basic
    /// operations while minimizing the information loss (i.e. error) incurred by performing them. The distributions
    /// are not scale invariant; <see cref="Scalar"/>s near 1 will behave subtly differently to those near 1000 or 0.001.
    /// They are tuned to support the common use cases at their scale.</remarks>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct Scalar : IEquatable<Scalar>
    {
        /// <summary>
        /// The mean (and mode) of the distribution of reals represented by this <see cref="Scalar"/>.
        /// </summary>
        internal float _mean;
        public Scalar(float mean)
        {
            _mean = mean;
        }

        public static Scalar Zero => new Scalar(0f);
        public static Scalar Inf => float.PositiveInfinity; // TODO: Remove
        public static Scalar Pi => Math.PI;

        /// <summary>
        /// Determines whether this <see cref="Scalar"/> has a likely true value of zero.
        /// </summary>
        public bool IsLikelyZero => AreLikelyEqual(this, Zero); // TODO: Specialize for performance

        /// <summary>
        /// A parameter used in the apriori distribution of scalar values: "sqrt(alpha) / (pi * (alpha + x^2))". This
        /// controls how "flat" the distribution is around 0, with higher values indicating that scalar values
        /// around 0 are distributed more uniformly.
        /// </summary>
        internal const float _alpha = 1f;

        /// <summary>
        /// A parameter used to determine the variance of the distribution of possible true values for a scalar:
        /// "beta + gamma * mean^2". The higher this is, the more variance is expected for values around 0.
        /// </summary>
        internal const float _beta = 1000 * _eps * _eps;

        /// <summary>
        /// A parameter used to determine the variance of the distribution of possible true values for a scalar:
        /// "beta + gamma * mean^2". The higher this is, the faster the variance increases based on the
        /// mean of the scalar.
        /// </summary>
        internal const float _gamma = _eps * _eps;

        /// <summary>
        /// The difference between 1 and the next representable float.
        /// </summary>
        internal const float _eps = 1.192092896e-7f;

        public static Scalar Abs(Scalar arg)
        {
            return new Scalar(_abs(arg._mean));
        }

        internal static float _abs(float x)
        {
            // TODO: Improve performance
            if (x < 0)
                return -x;
            return x;
        }
        
        public static Scalar Sqrt(Scalar arg)
        {
            return new Scalar(_sqrt(arg._mean));
        }
        
        internal static float _sqrt(float x)
        {
            // TODO: Use MathF
            return (float)Math.Sqrt(x);
        }

        public static Scalar Sin(Scalar arg)
        {
            return new Scalar((float)Math.Sin(arg._mean));
        }
        
        public static Scalar Cos(Scalar arg)
        {
            return new Scalar((float)Math.Cos(arg._mean));
        }
        
        public static Scalar Tan(Scalar arg)
        {
            return new Scalar((float)Math.Tan(arg._mean));
        }
        
        public static Scalar Asin(Scalar arg)
        {
            return new Scalar((float)Math.Asin(arg._mean));
        }
        
        public static Scalar Acos(Scalar arg)
        {
            return new Scalar((float)Math.Acos(arg._mean));
        }
        
        public static Scalar Atan(Scalar arg)
        {
            return new Scalar((float)Math.Atan(arg._mean));
        }
        
        public static int Floor(Scalar arg)
        {
            return (int)Math.Floor(arg._mean);
        }
        
        public static Scalar Min(Scalar x, Scalar y)
        {
            return new Scalar(Math.Min(x._mean, y._mean));
        }
        
        public static Scalar Max(Scalar x, Scalar y)
        {
            return new Scalar(Math.Max(x._mean, y._mean));
        }
        
        public static Scalar Pow(Scalar x, Scalar y)
        {
            return new Scalar((float)Math.Pow(x._mean, y._mean));
        }
        
        public static Scalar Ln(Scalar arg)
        {
            return new Scalar(_ln(arg._mean));
        }

        internal static float _ln(float x)
        {
            // TODO: Use MathF
            return (float)Math.Log(x);
        }

        public static Scalar Exp(Scalar arg)
        {
            return new Scalar(_exp(arg._mean));
        }

        internal static float _exp(float x)
        {
            // TODO: Use MathF
            return (float)Math.Exp(x);
        }

        public static Scalar Lerp(Scalar x, Scalar y, Scalar a)
        {
            return y * a + x * (1 - a);
        }
        
        public static Scalar Clamp(Scalar x, Scalar min, Scalar max)
        {
            if (x < min)
                return min;
            if (x > max)
                return max;
            return x;
        }

        /// <summary>
        /// Clamps a scalar such that its <see cref="Floor(Scalar)"/> is the given value.
        /// </summary>
        public static Scalar ClampFloor(Scalar x, int val)
        {
            // TODO: Does this work in all cases?
            return Clamp(x, (Scalar)val, (Scalar)(val + 1));
        }

        /// <summary>
        /// Determines whether the given <see cref="Scalar"/>s likely have the same true value
        /// after compensating for representation error.
        /// </summary>
        public static bool AreLikelyEqual(Scalar a, Scalar b)
        {
            return AreEqualOdds(a, b) > 1;
        }

        /// <summary>
        /// Determines the relative probability that the given <see cref="Scalar"/>s have the same true value, over
        /// the probability that their true values are independent. This is based on the apriori distribution of
        /// scalar values, and individual value distributions of <paramref name="a"/> and <paramref name="b"/>. Note
        /// that when multiple potential comparisons are tested, this should be divided by the number of comparisons to
        /// compensate for multiple trials.
        /// </summary>
        public static LogScalar AreEqualOdds(Scalar a, Scalar b)
        {
            // All this is an approximation to the odds equation "n / d", where "n" is the probability of selecting
            // a value from the prior scalar distribution and that being the true value for both "a" and "b",
            // and "d" is the probability of selecting a value from the prior scalar distribution and that being
            // the true value for "a", multiplied by the probability of selecting another value from the prior scalar
            // distribution and that being the true value for "b".
            float aSqrMean = a._mean * a._mean;
            float bSqrMean = b._mean * b._mean;
            float aVar = _beta + _gamma * aSqrMean;
            float bVar = _beta + _gamma * bSqrMean;
            float aPriorFactor = _alpha + aSqrMean;
            float bPriorFactor = _alpha + bSqrMean;
            float diff = a._mean - b._mean;
            float sqrDiff = diff * diff;
            return new LogScalar(
                (_lnNormalNorm - _lnPriorNorm)
                + sqrDiff / (-2f * (aVar + bVar))
                - _ln((aVar + bVar) / (aPriorFactor * bPriorFactor)) / 2f);
        }

        /// <summary>
        /// The natural log of the normalization factor of the scalar value prior distribution, "sqrt(alpha) / pi".
        /// </summary>
        internal static readonly float _lnPriorNorm = _ln(_sqrt(_alpha) / Pi._mean);

        /// <summary>
        /// The natural log of the normalization factor for a normal distribution, "1 / sqrt(2 * pi)".
        /// </summary>
        internal static readonly float _lnNormalNorm = _ln(2f * Pi._mean) / -2f;

        /// <summary>
        /// Contains potential encodings for <see cref="Scalar"/> values.
        /// </summary>
        public static class Encoding
        {
            /// <summary>
            /// An encoding for <see cref="Scalar"/>s as individual bytes scaled to the range [0, 1].
            public static Encoding<Scalar> U8 => _U8.Instance;

            /// <summary>
            /// An encoding for <see cref="Scalar"/>s as individual signed bytes scaled to the range [-1, 1].
            public static Encoding<Scalar> S8 => _S8.Instance;

            /// <summary>
            /// An encoding for <see cref="Scalar"/> as 32-bit little-endian floats.
            /// </summary>
            [StandardEncoding]
            public static Encoding<Scalar> F32 => BitConverter.IsLittleEndian ? Fn32 : throw new NotImplementedException();

            /// <summary>
            /// An encoding for <see cref="Scalar"/> as 32-bit native-endian floats.
            /// </summary>
            [DefaultEncoding]
            public static Encoding<Scalar> Fn32 { get; } = new BlittableEncoding<Scalar>(4, 4);
            
            /// <summary>
            /// Encodes a scalar value to a <see cref="byte"/> using the <see cref="U8"/> encoding.
            /// </summary>
            public static byte Encode_U8(Scalar value)
            {
                return (byte)(int)(value * 255.0);
            }

            /// <summary>
            /// Decodes a scalar value from a <see cref="byte"/> using the <see cref="U8"/> encoding.
            /// </summary>
            public static Scalar Decode_U8(byte value)
            {
                return value / 255.0;
            }

            /// <summary>
            /// Decodes a scalar value from a <see cref="ushort"/> using the <see cref="U16"/> encoding.
            /// </summary>
            public static Scalar Decode_U16(ushort value)
            {
                return value / 65535.0;
            }

            /// <summary>
            /// The implementation for <see cref="U8"/>.
            /// </summary>
            private sealed class _U8 : Encoding<Scalar>
            {
                public _U8() : base(1, 1) { }

                /// <summary>
                /// The only instance of this class.
                /// </summary>
                public static _U8 Instance { get; } = new _U8();

                public override ref byte FastRead(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out Scalar value)
                {
                    throw new NotImplementedException();
                }

                public override ref byte FastReadAligned(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out Scalar value)
                {
                    throw new NotImplementedException();
                }

                public override ref byte FastWrite(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in Scalar value)
                {
                    throw new NotImplementedException();
                }

                public override ref byte FastWriteAligned(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in Scalar value)
                {
                    throw new NotImplementedException();
                }
            }

            /// <summary>
            /// The implementation for <see cref="S8"/>.
            /// </summary>
            private sealed class _S8 : Encoding<Scalar>
            {
                public _S8() : base(1, 1) { }

                /// <summary>
                /// The only instance of this class.
                /// </summary>
                public static _S8 Instance { get; } = new _S8();

                public override ref byte FastRead(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out Scalar value)
                {
                    throw new NotImplementedException();
                }

                public override ref byte FastReadAligned(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out Scalar value)
                {
                    throw new NotImplementedException();
                }

                public override ref byte FastWrite(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in Scalar value)
                {
                    throw new NotImplementedException();
                }

                public override ref byte FastWriteAligned(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in Scalar value)
                {
                    throw new NotImplementedException();
                }
            }
        }
        
        /// <summary>
        /// The difference between the scalar 1 and the next representable scalar.
        /// </summary>
        internal const float _machineEps = 1.192092896e-7f;
        
        /// <summary>
        /// The smallest "normal" <see cref="float"/>.
        /// </summary>
        internal const float _normalEps = 1.1754944e-38f;

        /// <summary>
        /// The smallest value which can "safetly" be used as a divisor without risking catastrophic precision
        /// loss. Note that this should not be used as a threshold for comparison. Use
        /// <see cref="AreApproxEqual(Scalar, Scalar, Scalar)"/> for that.
        /// </summary>
        public static Scalar Epsilon => new Scalar(_normalEps);

        public static Scalar operator -(Scalar a)
        {
            return new Scalar(-a._mean);
        }

        public static Scalar operator +(Scalar a, Scalar b)
        {
            return new Scalar(a._mean + b._mean);
        }

        public static Scalar operator -(Scalar a, Scalar b)
        {
            return new Scalar(a._mean - b._mean);
        }

        public static Scalar operator *(Scalar a, Scalar b)
        {
            return new Scalar(a._mean * b._mean);
        }

        public static Scalar operator /(Scalar a, Scalar b)
        {
            // TODO: General regularization when the result would be large
            if (b._mean == 0f)
                return new Scalar(0f);
            return new Scalar(a._mean / b._mean);
        }
        
        public static Scalar operator %(Scalar a, Scalar b) // TODO: Remove; not numerically stable
        {
            return a - b * Floor(a / b);
        }
        
        public static bool operator ==(Scalar a, Scalar b)
        {
            return a._mean == b._mean;
        }
        
        public static bool operator !=(Scalar a, Scalar b)
        {
            return a._mean != b._mean;
        }
        
        public static bool operator <(Scalar a, Scalar b)
        {
            return a._mean < b._mean;
        }
        
        public static bool operator >(Scalar a, Scalar b)
        {
            return a._mean > b._mean;
        }
        
        public static bool operator <=(Scalar a, Scalar b)
        {
            return a._mean <= b._mean;
        }
        
        public static bool operator >=(Scalar a, Scalar b)
        {
            return a._mean >= b._mean;
        }
        
        public static implicit operator Scalar(float source)
        {
            return new Scalar(source);
        }
        
        public static implicit operator Scalar(double source)
        {
            return new Scalar((float)source);
        }
        
        public static explicit operator int(Scalar source)
        {
            return (int)source._mean;
        }
        
        public static explicit operator uint(Scalar source)
        {
            return (uint)source._mean;
        }

        public static explicit operator float(Scalar source)
        {
            return source._mean;
        }

        public static explicit operator double(Scalar source)
        {
            return source._mean;
        }

        public override int GetHashCode()
        {
            return _mean.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Scalar))
                return false;
            return this == (Scalar)obj;
        }

        bool IEquatable<Scalar>.Equals(Scalar other)
        {
            return this == other;
        }
        
        public override string ToString()
        {
            return _mean.ToString("R", System.Globalization.CultureInfo.InvariantCulture);
        }
    }
}