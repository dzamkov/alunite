﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Threading;

namespace Alunite.Data
{
    /// <summary>
    /// A thread-safe associative mapping of keys to values that's optimized for fast lookup. This differs from
    /// <see cref="ConcurrentDictionary{TKey, TValue}"/> in that it expects modifications to be rare outside of
    /// occasional additions in return for improved lookup performance.
    /// </summary>
    public sealed class LookupDictionary<TKey, TValue>
    {
        public LookupDictionary()
            : this(EqualityHelper.Comparer<TKey>())
        { }

        public LookupDictionary(IEqualityComparer<TKey> comparer)
        {
            Comparer = comparer;
            _entries = new _Entry[8];
            _capacity = 7;
        }

        /// <summary>
        /// A hashtable of entries for this dictionary.
        /// </summary>
        private _Entry[] _entries;

        /// <summary>
        /// The number of additional entries that can be added to <see cref="_entries"/> before it needs to be resized.
        /// </summary>
        private uint _capacity;
        
        /// <summary>
        /// The <see cref="IEqualityComparer{T}"/> used to compare keys in this dictionary.
        /// </summary>
        public IEqualityComparer<TKey> Comparer { get; }
        
        /// <summary>
        /// An entry in the backing hashtable.
        /// </summary>
        private struct _Entry
        {
            /// <summary>
            /// The hash for this entry, or 0 if the entry is empty.
            /// </summary>
            public int Hash;

            /// <summary>
            /// The key for this entry.
            /// </summary>
            public TKey Key;

            /// <summary>
            /// The value for this entry.
            /// </summary>
            public TValue Value;
        }
        
        /// <summary>
        /// Gets or sets a value from this dictionary.
        /// </summary>
        public TValue this[TKey key]
        {
            get
            {
                if (!TryGet(key, out TValue value))
                    throw new KeyNotFoundException();
                return value;
            }
            set
            {
                Update(key, value);
            }
        }

        /// <summary>
        /// Tries getting an entry from this dictionary, returning false if it does not exist.
        /// </summary>
        public bool TryGet(TKey key, out TValue value)
        {
            _Entry[] entries = Volatile.Read(ref _entries);
            int binHash;
            int hash = _getHash(key);
            uint bin = entries.GetBinPow2(hash);
            while ((binHash = Volatile.Read(ref entries[bin].Hash)) != 0)
            {
                if (binHash == hash && Comparer.Equals(key, entries[bin].Key))
                {
                    value = entries[bin].Value;
                    return true;
                }
                bin = entries.Next(bin);
            }
            value = default;
            return false;
        }

        /// <summary>
        /// Updates the value for the given key in this dictionary.
        /// </summary>
        public void Update(TKey key, TValue value)
        {
            lock (this)
            {
                int binHash;
                int hash = _getHash(key);
                uint bin = _entries.GetBinPow2(hash);
                while ((binHash = _entries[bin].Hash) != 0)
                {
                    if (binHash == hash && Comparer.Equals(key, _entries[bin].Key))
                    {
                        // TODO: Update, it's not as simple as replacing the value since that's not thread-safe
                        throw new NotImplementedException();
                    }
                    bin = _entries.Next(bin);
                }
                if (_capacity > 0)
                {
                    ref _Entry entry = ref _entries[bin];
                    entry.Hash = hash;
                    entry.Key = key;
                    entry.Value = value;
                    Volatile.Write(ref entry.Hash, hash);
                    _capacity--;
                }
                else
                {
                    _expand();
                    _add(hash, key, value);
                }
            }
        }

        /// <summary>
        /// Atomically gets the value for this given key in this dictionary, or uses the given factory function to
        /// create one if it doesn't already exist. When called simultaneously from multiple threads, this is guaranteed
        /// to call <paramref name="valueFactory"/> at most once per key.
        /// </summary>
        public TValue GetOrAdd(TKey key, Func<TKey, TValue> valueFactory)
        {
            _Entry[] entries = Volatile.Read(ref _entries);

            // Find target or empty bin
            int binHash;
            int hash = _getHash(key);
            uint bin = entries.GetBinPow2(hash);
            while ((binHash = Volatile.Read(ref entries[bin].Hash)) != 0)
            {
                if (binHash == hash && Comparer.Equals(key, entries[bin].Key))
                    return entries[bin].Value;
                bin = entries.Next(bin);
            }

            // Add entry
            lock (this)
            {
                TValue value = valueFactory(key);
                _add(hash, key, value);
                if (_capacity == 0)
                    _expand();
                return value;
            }
        }

        /// <summary>
        /// Expands the hash table for this dictionary, assuming it is locked. 
        /// </summary>
        private void _expand()
        {
            _Entry[] nEntries = new _Entry[_entries.Length * 2];
            _capacity = _getCapacity(nEntries.Length);
            for (uint i = 0; i < _entries.Length; i++)
            {
                int hash = _entries[i].Hash;
                if (hash != 0)
                {
                    uint bin = nEntries.GetBinPow2(hash);
                    while (nEntries[bin].Hash != 0)
                        bin = nEntries.Next(bin);
                    nEntries[bin] = _entries[i];
                    _capacity--;
                }
            }
            Volatile.Write(ref _entries, nEntries);
        }

        /// <summary>
        /// Gets the effective hash code for the given key in this dictionary.
        /// </summary>
        private int _getHash(TKey key)
        {
            int hash = Comparer.GetHashCode(key);
            if (hash == 0) hash = int.MaxValue;
            return hash;
        }

        /// <summary>
        /// Adds an entry to the hash table for this dictionary, assuming it is locked and has
        /// capacity for it.
        /// </summary>
        private void _add(int hash, TKey key, TValue value)
        {
            uint bin = _entries.GetBinPow2(hash);
            while (_entries[bin].Hash != 0)
                bin = _entries.Next(bin);
            ref _Entry entry = ref _entries[bin];
            entry.Key = key;
            entry.Value = value;
            Volatile.Write(ref entry.Hash, hash);
            _capacity--;
        }

        /// <summary>
        /// Gets the capacity of a hash table with the given size.
        /// </summary>
        private static uint _getCapacity(int size)
        {
            return ((uint)size * 3) / 5;
        }
    }
}