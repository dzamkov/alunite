﻿using System;
using System.Collections.Generic;

namespace Alunite.Data
{
    /// <summary>
    /// Describes a linear polynomial over the <see cref="Scalar"/>s.
    /// </summary>
    public struct ScalarPoly1
    {
        public Scalar C_0;
        public Scalar C_1;
        public ScalarPoly1(Scalar c_0, Scalar c_1)
        {
            C_0 = c_0;
            C_1 = c_1;
        }

        /// <summary>
        /// The constant zero polynomial.
        /// </summary>
        public static ScalarPoly1 Zero => new ScalarPoly1(0, 0);

        /// <summary>
        /// Evaluates this polynomial for the given argument.
        /// </summary>
        public Scalar this[Scalar arg] => C_0 + C_1 * arg;

        /// <summary>
        /// The derivative of this polynomial.
        /// </summary>
        public Scalar Derivative => C_1;

        public static implicit operator ScalarPoly1(Scalar @const)
        {
            return new ScalarPoly1(@const, 0);
        }
    }

    /// <summary>
    /// Describes a quadratic polynomial over the <see cref="Scalar"/>s.
    /// </summary>
    public struct ScalarPoly2
    {
        public Scalar C_0;
        public Scalar C_1;
        public Scalar C_2;
        public ScalarPoly2(Scalar c_0, Scalar c_1, Scalar c_2)
        {
            C_0 = c_0;
            C_1 = c_1;
            C_2 = c_2;
        }

        /// <summary>
        /// The constant zero polynomial.
        /// </summary>
        public static ScalarPoly2 Zero => new ScalarPoly2(0, 0, 0);

        /// <summary>
        /// Evaluates this polynomial for the given argument.
        /// </summary>
        public Scalar this[Scalar arg] => C_0 + (C_1 + C_2 * arg) * arg;

        /// <summary>
        /// The derivative of this polynomial.
        /// </summary>
        public ScalarPoly1 Derivative => new ScalarPoly1(C_1, 2 * C_2);

        /// <summary>
        /// Applies the given offset to the argument for this polynomial.
        /// </summary>
        public ScalarPoly2 Shift(Scalar offset)
        {
            return new ScalarPoly2(
                C_0 + (C_1 + C_2 * offset) * offset,
                C_1 + 2 * C_2 * offset,
                C_2);
        }

        public static ScalarPoly2 operator +(ScalarPoly2 a, ScalarPoly2 b)
        {
            return new ScalarPoly2(
                a.C_0 + b.C_0,
                a.C_1 + b.C_1,
                a.C_2 + b.C_2);
        }

        public static ScalarPoly2 operator -(ScalarPoly2 a, ScalarPoly2 b)
        {
            return new ScalarPoly2(
                a.C_0 - b.C_0,
                a.C_1 - b.C_1,
                a.C_2 - b.C_2);
        }

        public static implicit operator ScalarPoly2(Scalar @const)
        {
            return new ScalarPoly2(@const, 0, 0);
        }

        public static implicit operator ScalarPoly2(ScalarPoly1 source)
        {
            return new ScalarPoly2(source.C_0, source.C_1, 0);
        }
    }
}
