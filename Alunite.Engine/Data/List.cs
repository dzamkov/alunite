﻿using System;
using System.Text;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

using Alunite.Diagnostics;

namespace Alunite.Data
{
    /// <summary>
    /// A finite list of values of type <typeparamref name="T"/>.
    /// </summary>
    /// <remarks>This is intended as a general purpose way of passing lists of values. When performance is not a concern,
    /// this is preferable to arrays because it directly signals immutability and has a richer set of allowable
    /// operations.</remarks>
    [DebuggerDisplay("{ToString(),nq}")]
    [DebuggerTypeProxy(typeof(EnumerableDebugView<>))]
    public struct List<T> : IEquatable<List<T>>, IEnumerable<T>, IPrintable
    {
        internal T[] _items;
        internal List(T[] items)
        {
            _items = items;
        }

        /// <summary>
        /// The empty <see cref="List{T}"/>.
        /// </summary>
        public static List<T> Empty => new List<T>(Array.Empty<T>());

        /// <summary>
        /// Indicates whether this is the empty list.
        /// </summary>
        public bool IsEmpty => _items.Length == 0;

        /// <summary>
        /// The length of this list.
        /// </summary>
        public uint Length => (uint)_items.Length;

        /// <summary>
        /// Gets the item at the given index.
        /// </summary>
        public T this[uint index] => _items[index];

        /// <summary>
        /// Constructs a list consisting of some contiguous subsequence of this list.
        /// </summary>
        public SliceList<T> Slice(uint offset, uint len)
        {
            if (!(offset + len <= Length))
                throw new IndexOutOfRangeException();
            return new SliceList<T>(_items, offset, len);
        }

        /// <summary>
        /// Constructs a list consisting of the given items from this list, in the same order.
        /// </summary>
        public List<T> Subseq(IndexSet indices)
        {
            T[] nItems = new T[indices.Size];
            uint i = 0;
            foreach (uint index in indices)
                nItems[i++] = _items[index];
            return new List<T>(nItems);
        }

        /// <summary>
        /// Constructs a list by replacing each index in the given list with the corresponding item in this list.
        /// </summary>
        public List<T> Select(List<uint> indices)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Constructs a new list by appending the given item onto this list.
        /// </summary>
        public List<T> Append(T item)
        {
            T[] nItems = new T[_items.Length + 1];
            Array.Copy(_items, nItems, _items.Length);
            nItems[_items.Length] = item;
            return new List<T>(nItems);
        }

        /// <summary>
        /// Constructs a new list by removing the item at the given index.
        /// </summary>
        public List<T> RemoveAt(uint index)
        {
            if (!(index < (uint)_items.Length))
                throw new IndexOutOfRangeException();
            T[] nItems = new T[_items.Length - 1];
            Array.Copy(_items, 0, nItems, 0, (int)index);
            Array.Copy(_items, (int)(index + 1), nItems, (int)index, nItems.Length - (int)index);
            return new List<T>(nItems);
        }

        /// <summary>
        /// Searches for the index of the first item that has the given value, or returns false if no such item exists.
        /// </summary>
        public bool TryFindFirst(T value, out uint index)
        {
            index = 0;
            while (index < (uint)_items.Length)
            {
                if (EqualityHelper.Equals(value, _items[index]))
                    return true;
                index++;
            }
            return false;
        }

        /// <summary>
        /// Creates an <see cref="IIterator{T}"/> to iterate over the items in this list.
        /// </summary>
        public Iterator Iterate()
        {
            return new Iterator
            {
                _items = _items,
                _index = 0
            };
        }

        /// <summary>
        /// An <see cref="IIterator{T}"/> for a <see cref="List{T}"/>.
        /// </summary>
        public struct Iterator : IIterator<T>
        {
            internal T[] _items;
            internal uint _index;

            /// <summary>
            /// Attempts to get the next item from the iterator, returning false if the end of the list
            /// has been reached.
            /// </summary>
            public bool TryNext(out T value)
            {
                if (_index < (uint)_items.Length)
                {
                    value = _items[_index++];
                    return true;
                }
                value = default;
                return false;
            }

            void IDisposable.Dispose()
            {
                // Nothing to do here
            }
        }

        /// <summary>
        /// Gets an enumerator for this list.
        /// </summary>
        public IteratorEnumerator<Iterator, T> GetEnumerator()
        {
            return new IteratorEnumerator<Iterator, T>(Iterate());
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Concatenates two lists.
        /// </summary>
        public static List<T> operator +(List<T> a, List<T> b)
        {
            if (a.IsEmpty)
                return b;
            if (b.IsEmpty)
                return a;
            T[] nItems = new T[a._items.Length + b._items.Length];
            Array.Copy(a._items, 0, nItems, 0, a._items.Length);
            Array.Copy(b._items, 0, nItems, a._items.Length, b._items.Length);
            return new List<T>(nItems);
        }

        public static implicit operator ReadOnlySpan<T>(List<T> list)
        {
            return list._items;
        }

        public static bool operator ==(List<T> a, List<T> b)
        {
            if (a._items.Length != b._items.Length)
                return false;
            for (int i = 0; i < a._items.Length; i++)
                if (!EqualityHelper.Equals(a._items[i], b._items[i]))
                    return false;
            return true;
        }

        public static bool operator !=(List<T> a, List<T> b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is List<T>))
                return false;
            return this == (List<T>)obj;
        }

        bool IEquatable<List<T>>.Equals(List<T> other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            int hash = (int)_items.Length;
            for (int i = 0; i < _items.Length; i++)
                hash = HashCodeHelper.Combine(hash, EqualityHelper.GetHashCode(_items[i]));
            return hash;
        }

        public override string ToString()
        {
            return this.ToShortString();
        }

        void IPrintable.Print(IPrinter printer, Precedence prec)
        {
            if (_items == null)
            {
                printer.Write("<invalid>");
            }
            else
            {
                printer.WriteItem('[');
                bool isFirst = true;
                foreach (var item in this)
                {
                    if (isFirst)
                        isFirst = false;
                    else
                        printer.Write(", ");
                    printer.Print(Precedence.Free, item);
                }
                printer.WriteItem(']');
            }
        }
    }

    /// <summary>
    /// Contains functions for constructing and manipulating <see cref="List{T}"/>s.
    /// </summary>
    public static class List
    {
        /// <summary>
        /// Gets the empty list of the given type.
        /// </summary>
        public static List<T> Empty<T>()
        {
            return List<T>.Empty;
        }

        /// <summary>
        /// Constructs a list consisting of the given items.
        /// </summary>
        public static SpanList<T> Of<T>(params T[] items)
        {
            // We can't create a list directly because we can't be sure that the contents of
            // items won't be mutated.
            return new SpanList<T>(items);
        }

        /// <summary>
        /// Constructs a list consisting of the given items.
        /// </summary>
        public static SpanList<T> Of<T>(Span<T> items)
        {
            return new SpanList<T>(items);
        }

        /// <summary>
        /// Constructs a list consisting of the given items.
        /// </summary>
        public static SpanList<T> Of<T>(ReadOnlySpan<T> items)
        {
            return new SpanList<T>(items);
        }

        /// <summary>
        /// Constructs a list which consists of a specified number of copies of the given item.
        /// </summary>
        public static List<T> Replicate<T>(uint len, T item)
        {
            T[] items = new T[len];
            for (uint i = 0; i < (uint)items.Length; i++)
                items[i] = item;
            return new List<T>(items);
        }

        /// <summary>
        /// Interleaves items from the given lists, which must all have the same length.
        /// </summary>
        public static List<T> Interleave<T>(params List<T>[] lists)
        {
            return Interleave<T>(List.Of(lists));
        }

        /// <summary>
        /// Interleaves items from the given lists, which must all have the same length.
        /// </summary>
        public static List<T> Interleave<T>(SpanList<List<T>> lists)
        {
            if (lists.IsEmpty)
                return Empty<T>();
            uint len = lists[0].Length;
            uint index = 0;
            T[] items = new T[lists.Length * len];
            for (uint i = 0; i < len; i++)
                for (uint j = 0; j < lists.Length; j++)
                    items[index++] = lists[j]._items[i];
            return new List<T>(items);
        }

        /// <summary>
        /// Constructs a <see cref="List{T}"/> of the given length where every item is its own index.
        /// </summary>
        public static List<uint> Identity(uint len)
        {
            if (len < (uint)_identityTable.Length)
                return _identityTable[len];
            uint[] items = new uint[len];
            for (uint j = 0; j < len; j++)
                items[j] = j;
            return new List<uint>(items);
        }

        /// <summary>
        /// A table of reusable <see cref="Identity"/> lists of small lengths.
        /// </summary>
        private static readonly List<uint>[] _identityTable = _buildIdentityTable();

        /// <summary>
        /// Builds <see cref="_identityTable"/>.
        /// </summary>
        private static List<uint>[] _buildIdentityTable()
        {
            List<uint>[] table = new List<uint>[32];
            table[0] = Empty<uint>();
            for (int i = 1; i < table.Length; i++)
            {
                uint[] items = new uint[i];
                for (uint j = 0; j < i; j++)
                    items[j] = j;
                table[i] = new List<uint>(items);
            }
            return table;
        }
    }

    /// <summary>
    /// A helper class for constructing a <see cref="List{T}"/> by incrementally appending items, or by updating
    /// existing items.
    /// </summary>
    /// <remarks>This is a replacement for <see cref="System.Collections.Generic.List{T}"/> which is a better fit
    /// for our coding sensibilities.</remarks>
    [DebuggerDisplay("{ToString(),nq}")]
    [DebuggerTypeProxy(typeof(EnumerableDebugView<>))]
    public sealed class ListBuilder<T> : IListWriter<T>, IBuilder<List<T>>, IEnumerable<T>
    {
        private T[] _buffer;
        private uint _len;
        private ListBuilder(T[] buffer, uint len)
        {
            _buffer = buffer;
            _len = len;
        }

        public ListBuilder() // TODO: Make private in favor of CreateEmpty
        {
            _buffer = Array.Empty<T>();
            _len = 0;
        }

        /// <summary>
        /// Creates an initially-empty <see cref="ListBuilder{T}"/>.
        /// </summary>
        public static ListBuilder<T> CreateEmpty()
        {
            return new ListBuilder<T>();
        }

        /// <summary>
        /// Creates an initially-empty <see cref="ListBuilder{T}"/>.
        /// </summary>
        /// <param name="capacity">The number of items expected to be in the list.</param>
        public static ListBuilder<T> CreateEmpty(uint capacity)
        {
            return new ListBuilder<T>(new T[capacity], 0);
        }

        /// <summary>
        /// Creates a <see cref="ListBuilder{T}"/> which is initially filled with the default
        /// value of <typeparamref name="T"/>.
        /// </summary>
        /// <param name="len">The initial length of the list.</param>
        public static ListBuilder<T> CreateDefault(uint len)
        {
            return new ListBuilder<T>(new T[len], len);
        }

        /// <summary>
        /// Creates a <see cref="ListBuilder{T}"/> which is initially filled with <paramref name="len"/> copies
        /// of <paramref name="item"/>.
        /// </summary>
        public static ListBuilder<T> CreateReplicate(uint len, T item)
        {
            T[] items = new T[len];
            for (uint i = 0; i < (uint)items.Length; i++)
                items[i] = item;
            return new ListBuilder<T>(items, len);
        }

        /// <summary>
        /// Creates a <see cref="ListBuilder{T}"/> with the given initial value.
        /// </summary>
        public static ListBuilder<T> Create(SpanList<T> initial)
        {
            T[] buffer = new T[initial.Length];
            for (uint i = 0; i < (uint)buffer.Length; i++)
                buffer[i] = initial[i];
            return new ListBuilder<T>(buffer, initial.Length);
        }

        /// <summary>
        /// The current length of the list under construction. When set, any additional items will be
        /// initially filled with the default value of <typeparamref name="T"/>.
        /// </summary>
        public uint Length
        {
            get
            {
                return _len;
            }
            set
            {
                if (value > _len)
                {
                    Table.Allocate(ref _buffer, _len, value - _len);
                    for (uint i = _len; i < value; i++)
                        _buffer[i] = default;
                }
                _len = value;
            }
        }

        /// <summary>
        /// Gets a reference to the item at the given index.
        /// </summary>
        public ref T this[uint index]
        {
            get
            {
                if (!(index < _len))
                    throw new IndexOutOfRangeException();
                return ref _buffer[index]; // TODO: Skip bounds check
            }
        }

        /// <summary>
        /// Appends an item to this list, returning its index.
        /// </summary>
        public uint Append(T item)
        {
            uint index = _len++;
            Table.Allocate(ref _buffer, index, 1);
            _buffer[index] = item;
            return index;
        }
        
        /// <summary>
        /// Concatenates a list at the end of this list, returning the first index it was added to.
        /// </summary>
        public uint Concat(List<T> list)
        {
            uint index = _len;
            Table.Allocate(ref _buffer, index, list.Length);
            Array.Copy(list._items, 0, _buffer, (int)index, list._items.Length);
            _len += list.Length;
            return index;
        }

        /// <summary>
        /// Appends an item to this list.
        /// </summary>
        public void Push(T item)
        {
            uint index = _len++;
            Table.Allocate(ref _buffer, index, 1);
            _buffer[index] = item;
        }

        /// <summary>
        /// Removes and returns the last item from this list, assuming the list is not empty.
        /// </summary>
        public T Pop()
        {
            Debug.Assert(_len > 0);
            return _buffer[--_len];
        }

        /// <summary>
        /// Removes and returns the last item from this list, or returns false if the list is empty.
        /// </summary>
        public bool TryPop(out T value)
        {
            if (_len > 0)
            {
                value = _buffer[--_len];
                return true;
            }
            value = default;
            return false;
        }

        /// <summary>
        /// Removes all items from this list.
        /// </summary>
        public void Clear()
        {
            _len = 0;
        }

        /// <summary>
        /// Sorts the items in this list using their default comparer.
        /// </summary>
        public void Sort()
        {
            // TODO: Deprecate
            Array.Sort(_buffer, 0, (int)_len);
        }

        /// <summary>
        /// Gets the <see cref="List{T}"/> resulting from this builder.
        /// </summary>
        public SliceList<T> Finish()
        {
            SliceList<T> res = new SliceList<T>(_buffer, 0, _len);
            _buffer = null; // Ensure the builder can not be used to modify the buffer anymore
            return res;
        }

        List<T> IBuilder<List<T>>.Finish()
        {
            return Finish();
        }

        /// <summary>
        /// Gets the <see cref="List{T}"/> resulting from this builder as an array.
        /// </summary>
        public T[] FinishToArray()
        {
            if (_len == _buffer.Length)
            {
                T[] buffer = _buffer;
                _buffer = null; // Ensure the builder can not be used to modify the buffer anymore
                return buffer;
            }
            T[] array = new T[_len];
            Array.Copy(_buffer, array, (int)_len);
            return array;
        }

        /// <summary>
        /// Gets an enumerator for this list.
        /// </summary>
        public Enumerator GetEnumerator()
        {
            return new Enumerator(this);
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// An enumerator for a <see cref="List{T}"/>
        /// </summary>
        public struct Enumerator : IEnumerator<T>
        {
            private T[] _buffer;
            private uint _len;
            private uint _index;
            public Enumerator(ListBuilder<T> list)
            {
                _buffer = list._buffer;
                _len = list._len;
                _index = uint.MaxValue;
            }

            /// <summary>
            /// Gets the current item for this enumerator.
            /// </summary>
            public T Current => _buffer[_index];

            /// <summary>
            /// Advances to the next, or first, item.
            /// </summary>
            public bool MoveNext()
            {
                unchecked { _index++; }
                return _index < _len;
            }

            /// <summary>
            /// Resets the enumerator to before the beginning of the first item.
            /// </summary>
            public void Reset()
            {
                _index = uint.MaxValue;
            }

            object IEnumerator.Current => Current;

            void IDisposable.Dispose() { }
        }

        void IListWriter<T>.Write(ReadOnlySpan<T> items)
        {
            uint index = _len;
            Table.Allocate(ref _buffer, index, (uint)items.Length);
            items.CopyTo(_buffer.AsSpan((int)index));
            _len += (uint)items.Length;
        }

        public static implicit operator SpanList<T>(ListBuilder<T> source)
        {
            return new SpanList<T>(source._buffer.AsSpan(0, (int)source._len));
        }

        public static implicit operator List<T>(ListBuilder<T> source)
        {
            return (SpanList<T>)source;
        }

        public static implicit operator Span<T>(ListBuilder<T> source)
        {
            return source._buffer.AsSpan(0, (int)source._len);
        }

        public static implicit operator ReadOnlySpan<T>(ListBuilder<T> source)
        {
            return (Span<T>)source;
        }
    }

    /// <summary>
    /// A temporary <see cref="List{T}"/> defined by a slice of some other <see cref="List{T}"/> or
    /// immutable array.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public ref struct SliceList<T>
    {
        // This is marked as a ref struct to ensure that it's not possible to hold a persistent reference
        // to data, which could cause memory leaks. There may be some clever way to implement this as a 
        // Span<T> in the future, but I'm not sure how to do so without causing extraneous copying when
        // roundtripping to/from a List<T>.
        private T[] _data;
        private uint _offset;
        private uint _len;
        internal SliceList(T[] data, uint offset, uint len)
        {
            _data = data;
            _offset = offset;
            _len = len;
        }

        /// <summary>
        /// The length of this list.
        /// </summary>
        public uint Length => _len;

        /// <summary>
        /// Gets the item at the given index.
        /// </summary>
        public T this[uint index]
        {
            get
            {
                if (!(index < _len))
                    throw new IndexOutOfRangeException();
                return _data[index + _len];
            }
        }

        /// <summary>
        /// Indicates whether this is the empty list.
        /// </summary>
        public bool IsEmpty => _len == 0;

        public static implicit operator SliceList<T>(List<T> source)
        {
            return new SliceList<T>(source._items, 0, source.Length);
        }

        public static implicit operator List<T>(SliceList<T> source)
        {
            if (source._offset == 0 && source._len == (uint)source._data.Length)
                return new List<T>(source._data);
            return List.Of<T>(source);
        }

        public static implicit operator ReadOnlySpan<T>(SliceList<T> list)
        {
            return list._data.AsSpan((int)list._offset, (int)list._len);
        }

        public override string ToString()
        {
            return ((List<T>)this).ToString();
        }
    }

    /// <summary>
    /// A temporary <see cref="List{T}"/> defined by an immutable <see cref="Span{T}"/>.
    /// </summary>
    public ref struct SpanList<T>
    {
        internal ReadOnlySpan<T> _span;
        internal SpanList(ReadOnlySpan<T> span)
        {
            _span = span;
        }

        /// <summary>
        /// The length of this list.
        /// </summary>
        public uint Length => (uint)_span.Length;

        /// <summary>
        /// Gets the item at the given index.
        /// </summary>
        public ref readonly T this[uint index] => ref _span[(int)index];

        /// <summary>
        /// Indicates whether this is the empty list.
        /// </summary>
        public bool IsEmpty => _span.IsEmpty;

        /// <summary>
        /// Constructs a list consisting of some contiguous subsequence of this list.
        /// </summary>
        public SpanList<T> Slice(uint offset, uint len)
        {
            return new SpanList<T>(_span.Slice((int)offset, (int)len));
        }

        /// <summary>
        /// Constructs a list consisting of a prefix of this list.
        /// </summary>
        public SpanList<T> SlicePrefix(uint len)
        {
            return new SpanList<T>(_span.Slice(0, (int)len));
        }
        
        /// <summary>
        /// Constructs a list consisting of a suffix of this list.
        /// </summary>
        public SpanList<T> SliceSuffix(uint offset)
        {
            return new SpanList<T>(_span.Slice((int)offset));
        }

        /// <summary>
        /// Constructs a list consisting of the given items from this list, in the same order.
        /// </summary>
        public List<T> Subseq(IndexSet indices)
        {
            T[] nItems = new T[indices.Size];
            uint i = 0;
            foreach (uint index in indices)
                nItems[i++] = _span[(int)index];
            return new List<T>(nItems);
        }

        /// <summary>
        /// Constructs a list by replacing each index in the given list with the corresponding item in this list.
        /// </summary>
        public List<T> Select(List<uint> indices)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets an enumerator for this list.
        /// </summary>
        public ReadOnlySpan<T>.Enumerator GetEnumerator()
        {
            return _span.GetEnumerator();
        }

        public static implicit operator SpanList<T>(List<T> source)
        {
            return new SpanList<T>(source._items);
        }

        public static implicit operator SpanList<T>(SliceList<T> source)
        {
            return new SpanList<T>(source);
        }

        public static implicit operator List<T>(SpanList<T> source)
        {
            if (source.IsEmpty)
                return List<T>.Empty;
            var span = source._span;
            T[] items = new T[span.Length];
            for (int i = 0; i < span.Length; i++)
                items[i] = span[i];
            return new List<T>(items);
        }

        public static implicit operator ReadOnlySpan<T>(SpanList<T> list)
        {
            return list._span;
        }

        public override string ToString()
        {
            return ((List<T>)this).ToString();
        }
    }
    
    /// <summary>
    /// A streaming interface for reading items from a <see cref="List{T}"/>.
    /// </summary>
    public interface IListReader<T>
    {
        /// <summary>
        /// Reads the next items of the list into <paramref name="buffer"/>, limited by either the length of the
        /// buffer or the number of remaining items in the list.
        /// </summary>
        /// <returns>The number of items read. This will be less than the length of <paramref name="buffer"/> iff the
        /// end of the list has been reached.</returns>
        uint Read(Span<T> buffer);

        /// <summary>
        /// Advances the list reader by the given number of items, stopping at the end of the list.
        /// </summary>
        void Skip(uint num);
    }

    /// <summary>
    /// An <see cref="IListReader{T}"/> that provides a buffer which can be used to inspect the next few items in the
    /// list.
    /// </summary>
    public interface IBufferedListReader<T> : IListReader<T>
    {
        /// <summary>
        /// A buffer containing the next few items of the list. This must either have a length of at least
        /// <see cref="ListReader{T}.MinBufferLength"/>, or contain all the remaining items of the list.
        /// </summary>
        SpanList<T> Buffer { get; }
    }

    /// <summary>
    /// An <see cref="IListReader{T}"/> augmented with the ability to backtrack to previous marked points in the
    /// source list.
    /// </summary>
    public interface IBacktrackingListReader<T, TPoint> : IListReader<T>
    {
        /// <summary>
        /// Marks the current position in the input source as a possible backtracking point. The actual backtracking is
        /// done using <see cref="Reset(TPoint)"/>.
        /// </summary>
        TPoint Mark();

        /// <summary>
        /// Resets the position of the input source to the given <see cref="TPoint"/>. The point is still valid afterwards
        /// and can be used again. However, this does invalidate all points marked after the given point.
        /// </summary>
        /// <remarks>This always returns <see cref="true"/> for convience, so it can be used in the condition of an if
        /// statement when a long if-else-if chain is involved.</remarks>
        /// <returns><see cref="true"/></returns>
        bool Reset(TPoint point);
    }

    /// <summary>
    /// Combines the capabilities of an <see cref="IBufferedListReader{T}"/> and an
    /// <see cref="IBacktrackingListReader{T, TPoint}"/>.
    /// </summary>
    public interface IBacktrackingBufferedListReader<T, TPoint> :
        IBufferedListReader<T>,
        IBacktrackingListReader<T, TPoint> { }

    /// <summary>
    /// An <see cref="IListReader{T}"/> which tracks its position in the source list, allowing for arbitrary seeking of
    /// the reader.
    /// </summary>
    public interface ISeekableListReader<T> : IBacktrackingListReader<T, ulong>
    {
        /// <summary>
        /// Gets or sets the position of the next unread item within the source list. Note that unless
        /// otherwise specified, the origin of this position is arbitrary and only relative differences are meaningful.
        /// </summary>
        /// <remarks>The getter and setter of this property should be equivalent to
        /// <see cref="IBacktrackingListReader{T, TPoint}.Mark"/>
        /// and <see cref="IBacktrackingListReader{T, TPoint}.Reset(TPoint)"/> respectively. Note, however, that
        /// <see cref="Position"/> is more powerful than the backtracking methods. It guarantees that the
        /// difference between positions is the number of items between those positions, which isn't generally true
        /// for <see cref="IBacktrackingListReader{T, TPoint}"/>, even when the point type is numeric.
        /// </remarks>
        ulong Position { get; set; }
    }

    /// <summary>
    /// Combines the capabilities of an <see cref="IBufferedListReader{T}"/> and a
    /// <see cref="ISeekableListReader{T}"/>.
    /// </summary>
    public interface ISeekableBufferedListReader<T> :
        IBufferedListReader<T>,
        ISeekableListReader<T> { }
    
    /// <summary>
    /// Implements an <see cref="IBufferedListReader{T}"/> over an arbitrary <see cref="IListReader{T}"/> using
    /// an array-based buffer.
    /// </summary>
    public class MemoryBufferedListReader<T> : IBufferedListReader<T>
    {
        internal uint _head;
        internal uint _end;
        internal MemoryBufferedListReader(T[] memory, uint head, uint end, IListReader<T> rem)
        {
            _head = head;
            _end = end;
            Memory = memory;
            Remainder = rem;
        }

        /// <summary>
        /// A buffer containing the next few items of the list.
        /// </summary>
        public virtual SpanList<T> Buffer
        {
            get
            {
                // Ensure buffer minimum length
                uint bufferLen = _end - _head;
                if ((uint)Memory.Length - _head < ListReader<T>.MinBufferLength)
                {
                    // Copy unread items to the beginning of memory
                    for (uint i = 0; i < bufferLen; i++)
                        Memory[i] = Memory[_head + i];

                    // Populate the rest of memory
                    _head = 0;
                    _end = bufferLen + Remainder.Read(Memory.AsSpan((int)bufferLen));
                    bufferLen = _end;
                }
                return new SpanList<T>(Memory.AsSpan((int)_head, (int)bufferLen));
            }
        }

        /// <summary>
        /// The array which contains <see cref="Buffer"/>.
        /// </summary>
        public T[] Memory { get; }

        /// <summary>
        /// The index of the next list item in <see cref="Memory"/>.
        /// </summary>
        public uint Head => _head;

        /// <summary>
        /// The index of the end of the list in <see cref="Memory"/>, or the length of <see cref="Memory"/> if the
        /// end of the list has not yet been reached.
        /// </summary>
        public uint End => _end;

        /// <summary>
        /// The <see cref="IListReader{T}"/> used to populate new items into <see cref="Buffer"/>.
        /// </summary>
        public IListReader<T> Remainder { get; }

        /// <summary>
        /// Reads the next items of the list into <paramref name="buffer"/>, limited by either the length of the
        /// buffer or the number of remaining items in the list.
        /// </summary>
        /// <returns>The number of items read. This will be less than the length of <paramref name="buffer"/> iff the
        /// end of the list has been reached.</returns>
        public uint Read(Span<T> buffer)
        {
            uint bufferLen = _head - _end;
            if ((uint)buffer.Length < bufferLen)
            {
                for (int i = 0; i < buffer.Length; i++)
                    buffer[i] = Memory[_head++];
                return (uint)buffer.Length;
            }
            else
            {
                int index = 0;
                while (_head < _end)
                    buffer[index++] = Memory[_head++];
                return bufferLen + Remainder.Read(buffer.Slice(index));
            }
        }

        /// <summary>
        /// Advances the list reader by the given number of items, assuming that there are at least that many items
        /// remaining.
        /// </summary>
        public void Skip(uint num)
        {
            uint bufferLen = _end - _head;
            if (num <= bufferLen)
            {
                _head += num;
            }
            else
            {
                _head = _end;
                Remainder.Skip(num - bufferLen);
            }
        }
    }

    /// <summary>
    /// Augments a <see cref="MemoryBufferedListReader{T}"/> with the ability to backtrack, provided that the source
    /// reader is an <see cref="IBacktrackingListReader{T, TPoint}"/>.
    /// </summary>
    public sealed class BacktrackingMemoryBufferedListReader<T, TPoint> : 
        MemoryBufferedListReader<T>,
        IBacktrackingBufferedListReader<T, TPoint>,
        IDisposable
    {
        private TPoint _memoryPoint;
        internal BacktrackingMemoryBufferedListReader(
            T[] memory, uint head, uint end,
            IBacktrackingListReader<T, TPoint> rem)
            : base(memory, head, end, rem)
        { }

        /// <summary>
        /// The backtracking point corresponding to the start of <see cref="MemoryBufferedListReader{T}.Memory"/>.
        /// </summary>
        public TPoint MemoryPoint => _memoryPoint;

        /// <summary>
        /// The <see cref="IListReader{T}"/> used to populate new items into <see cref="Buffer"/>.
        /// </summary>
        public new IBacktrackingListReader<T, TPoint> Remainder => (IBacktrackingListReader<T, TPoint>)base.Remainder;

        /// <summary>
        /// Marks the current position in the input source as a possible backtracking point. The actual backtracking is
        /// done using <see cref="Reset(TPoint)"/>.
        /// </summary>
        public TPoint Mark()
        {
            if (_head == 0)
                return _memoryPoint;
            if (_head == _end)
                return Remainder.Mark();
            uint bufferLen = _end - _head;
            Remainder.Reset(_memoryPoint);
            Remainder.Skip(_head);
            var res = Remainder.Mark();
            Remainder.Skip(bufferLen);
            return res;
        }

        /// <summary>
        /// Resets the position of the input source to the given <see cref="TPoint"/>. The point is still valid afterwards
        /// and can be used again. However, this does invalidate all points marked after the given point.
        /// </summary>
        /// <remarks>This always returns <see cref="true"/> for convience, so it can be used in the condition of an if
        /// statement when a long if-else-if chain is involved.</remarks>
        /// <returns><see cref="true"/></returns>
        public bool Reset(TPoint point)
        {
            _head = _end = (uint)Memory.Length;
            Remainder.Reset(point);
            return true;
        }

        /// <summary>
        /// Updates the position of <see cref="Remainder"/> to match the position of this reader, destroying the
        /// <see cref="MemoryBufferedListReader{T}"/> in the process.
        /// </summary>
        public void Dispose()
        {
            Remainder.Reset(_memoryPoint);
            Remainder.Skip(_head);
        }

        public override SpanList<T> Buffer
        {
            get
            {
                // Ensure buffer minimum length
                uint bufferLen = _end - _head;
                if ((uint)Memory.Length - _head < ListReader<T>.MinBufferLength)
                {
                    if (bufferLen == 0)
                    {
                        // Populate memory without preserving items
                        _memoryPoint = Remainder.Mark();
                        _head = 0;
                        _end = bufferLen = Remainder.Read(Memory.AsSpan((int)_head));
                    }
                    else
                    {
                        Remainder.Reset(_memoryPoint);
                        Remainder.Skip(_head);
                        _memoryPoint = Remainder.Mark();
                        Remainder.Skip(bufferLen);

                        // Copy unread items to the beginning of memory
                        for (uint i = 0; i < bufferLen; i++)
                            Memory[i] = Memory[_head + i];

                        // Populate the rest of memory
                        _head = 0;
                        _end = bufferLen + Remainder.Read(Memory.AsSpan((int)bufferLen));
                        bufferLen = _end;
                    }
                }
                return new SpanList<T>(Memory.AsSpan((int)_head, (int)bufferLen));
            }
        }
    }

    /// <summary>
    /// Augments a <see cref="MemoryBufferedListReader{T}"/> with arbitrary seeking capabilities, provided that the
    /// source reader is an <see cref="ISeekableListReader{T}"/>.
    /// </summary>
    public sealed class SeekableMemoryBufferedListReader<T> :
        MemoryBufferedListReader<T>,
        ISeekableBufferedListReader<T>,
        IDisposable
    {
        internal SeekableMemoryBufferedListReader(
            T[] memory, uint head, uint end,
            ISeekableListReader<T> rem)
            : base(memory, head, end, rem)
        { }

        /// <summary>
        /// The <see cref="IListReader{T}"/> used to populate new items into <see cref="Buffer"/>.
        /// </summary>
        public new ISeekableListReader<T> Remainder => (ISeekableListReader<T>)base.Remainder;

        /// <summary>
        /// Gets or sets the position of the next unread item within the underlying data source.
        /// </summary>
        public ulong Position
        {
            get
            {
                return Remainder.Position - (End - Head);
            }
            set
            {
                Remainder.Position = value + (End - Head);
            }
        }

        /// <summary>
        /// Updates the position of <see cref="Remainder"/> to match the position of this reader, destroying the
        /// <see cref="MemoryBufferedListReader{T}"/> in the process.
        /// </summary>
        public void Dispose()
        {
            Remainder.Position = Position;
        }
        
        ulong IBacktrackingListReader<T, ulong>.Mark()
        {
            return Position;
        }

        bool IBacktrackingListReader<T, ulong>.Reset(ulong point)
        {
            Position = point;
            return true;
        }
    }
    
    /// <summary>
    /// Contains helper functions related to <see cref="IListReader{T}"/>.
    /// </summary>
    public static class ListReader
    {
        /// <summary>
        /// Reads the next items of the list into <paramref name="buffer"/>, limited by either <paramref name="len"/>
        /// or the number of remaining items in the list.
        /// </summary>
        /// <returns>The number of items read. This will be less than <paramref name="len"/> iff the
        /// end of the list has been reached.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe static uint Read<T>(this IListReader<T> reader, T* buffer, uint len)
            where T : unmanaged
        {
            return reader.Read(new Span<T>(buffer, (int)len));
        }

        /// <summary>
        /// Attempts to read the next item from the list, returning <see cref="false"/> if the end of the list has
        /// been reached.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe static bool TryReadItem<T>(this IListReader<T> reader, out T item)
        {
            // TODO: Replace with MemoryMarshal.CreateSpan once available
            item = default;
            ref byte byteRef = ref Unsafe.As<T, byte>(ref item);
            fixed (void* ptr = &byteRef)
                return reader.Read(new Span<T>(ptr, 1)) > 0;
        }

        /// <summary>
        /// Reads the next item from the list, assuming the end of the list has not been reached.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T ReadItem<T>(this IListReader<T> reader)
        {
            if (!reader.TryReadItem(out T item))
                throw new Exception("End of list reached");
            return item;
        }

        /// <summary>
        /// Gets an <see cref="IBufferedListReader{T}"/> wrapper over an <see cref="IListReader{T}"/>. Note that
        /// this consumes the source reader, as there is no way to restore the read position after finishing
        /// with the returned reader.
        /// </summary>
        public static void Buffered<T>(
            this IListReader<T> reader,
            out IBufferedListReader<T> bufferedReader)
        {
            if (reader is IBufferedListReader<T>)
            {
                // Use reader directly if possible
                bufferedReader = (IBufferedListReader<T>)reader;
            }
            else
            {
                // Create memory buffer
                // TODO: Pooling?
                uint len = ListReader<T>.MinBufferLength * 2;
                T[] memory = new T[len];
                var res = new MemoryBufferedListReader<T>(memory, len, len, reader);
                bufferedReader = res;
            }
        }

        /// <summary>
        /// Gets an <see cref="IBufferedListReader{T}"/> wrapper over an
        /// <see cref="IBacktrackingListReader{T, TPoint}"/>. Once reading is complete with the returned reader, it
        /// may be disposed, allowing reading to resume with the source reader.
        /// </summary>
        public static IDisposable Buffered<T, TPoint>(
            this IBacktrackingListReader<T, TPoint> reader,
            out IBacktrackingBufferedListReader<T, TPoint> bufferedReader)
        {
            if (reader is IBacktrackingBufferedListReader<T, TPoint>)
            {
                // Use reader directly if possible
                bufferedReader = (IBacktrackingBufferedListReader<T, TPoint>)reader;
                return null;
            }
            else
            {
                // Create memory buffer
                // TODO: Pooling?
                uint len = ListReader<T>.MinBufferLength * 2;
                T[] memory = new T[len];
                var res = new BacktrackingMemoryBufferedListReader<T, TPoint>(memory, len, len, reader);
                bufferedReader = res;
                return res;
            }
        }

        /// <summary>
        /// Gets an <see cref="IBufferedListReader{T}"/> wrapper over an
        /// <see cref="ISeekableListReader{T}"/>. Once reading is complete with the returned reader, it may be disposed,
        /// allowing reading to resume with the source reader.
        /// </summary>
        public static IDisposable Buffered<T>(
            this ISeekableListReader<T> reader,
            out ISeekableBufferedListReader<T> bufferedReader)
        {
            if (reader is ISeekableBufferedListReader<T>)
            {
                // Use reader directly if possible
                bufferedReader = (ISeekableBufferedListReader<T>)reader;
                return null;
            }
            else
            {
                // Create memory buffer
                // TODO: Pooling?
                uint len = ListReader<T>.MinBufferLength * 2;
                T[] memory = new T[len];
                var res = new SeekableMemoryBufferedListReader<T>(memory, len, len, reader);
                bufferedReader = res;
                return res;
            }
        }
    }

    /// <summary>
    /// A helper class for reading items from a <see cref="List{T}"/> with a streaming interface.
    /// </summary>
    public sealed class ListReader<T> : IListReader<T>
    {
        private T[] _items;
        private uint _index;
        public ListReader(List<T> list)
        {
            _items = list._items;
            _index = 0;
        }

        // TODO: Vary buffer length based on size of T
        /// <summary>
        /// The minimum buffer length for a <see cref="IBufferedListReader{T}"/> of the type <typeparamref name="T"/>.
        /// </summary>
        public const uint MinBufferLength = 64;

        /// <summary>
        /// Reads the next items of the list into <paramref name="buffer"/>, limited by either the length of the
        /// buffer or the number of remaining items in the list.
        /// </summary>
        /// <returns>The number of items read. This will be less than the length of <paramref name="buffer"/> iff the
        /// end of the list has been reached.</returns>
        public uint Read(Span<T> buffer)
        {
            uint numRem = (uint)_items.Length - _index;
            if (numRem < (uint)buffer.Length) buffer = buffer.Slice(0, (int)numRem);
            _items.AsSpan((int)_index, buffer.Length).CopyTo(buffer);
            _index += (uint)buffer.Length;
            return (uint)buffer.Length;
        }

        /// <summary>
        /// Advances the list reader by the given number of items, stopping at the end of the list.
        /// </summary>
        public void Skip(uint num)
        {
            _index += num;
            if (_index > (uint)_items.Length)
                _index = (uint)_items.Length;
        }

        /// <summary>
        /// Reads the next item from the list, assuming the end of the list has not been reached.
        /// </summary>
        public T ReadItem()
        {
            return _items[_index++];
        }

        /// <summary>
        /// Attempts to read the next item from the list, returning false if the end of the list has been reached.
        /// </summary>
        public bool TryReadItem(out T item)
        {
            if (_index < (uint)_items.Length)
            {
                item = _items[_index++];
                return true;
            }
            item = default;
            return false;
        }
    }

    /// <summary>
    /// An interface for specifying the contents of a <see cref="List{T}"/> by incrementally appending items.
    /// </summary>
    public interface IListWriter<T>
    {
        /// <summary>
        /// Appends the given items to the list.
        /// </summary>
        void Write(ReadOnlySpan<T> items);
    }

    /// <summary>
    /// Contains helper functions related to <see cref="IListWriter{T}"/>.
    /// </summary>
    public static class ListWriter
    {
        /// <summary>
        /// Appends the given items to the list.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe static void Write<T>(this IListWriter<T> writer, T* buffer, uint len)
            where T : unmanaged
        {
            writer.Write(new ReadOnlySpan<T>(buffer, (int)len));
        }

        /// <summary>
        /// Appends the given item to the list.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe static void WriteItem<T>(this IListWriter<T> writer, T item)
        {
            // TODO: Replace with MemoryMarshal.CreateSpan once available
            item = default;
            ref byte byteRef = ref Unsafe.As<T, byte>(ref item);
            fixed (void* ptr = &byteRef)
                writer.Write(new Span<T>(ptr, 1));
        }
    }
}