﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

using Alunite.Diagnostics;

namespace Alunite.Data
{
    /// <summary>
    /// A specialization of <see cref="Set{T}"/> for small <see cref="uint"/> indices.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    [DebuggerTypeProxy(typeof(EnumerableDebugView<uint>))]
    public struct IndexSet : IEquatable<IndexSet>, ISet<uint>, IEnumerable<uint>, IPrintable
    {
        internal ulong[] _bitmap;
        internal IndexSet(ulong[] bitmap)
        {
            _bitmap = bitmap;
        }

        /// <summary>
        /// The empty <see cref="IndexSet"/>.
        /// </summary>
        public static IndexSet None => new IndexSet(Array.Empty<ulong>());

        /// <summary>
        /// Constructs an <see cref="IndexSet"/> containing only the given index.
        /// </summary>
        public static IndexSet Of(uint index)
        {
            if (index < (uint)_singletonTable.Length)
                return _singletonTable[index];
            throw new NotImplementedException();
        }

        /// <summary>
        /// Constructs an <see cref="IndexSet"/> consisting of the given members.
        /// </summary>
        public static IndexSet Of(params uint[] members)
        {
            IndexSetBuilder builder = new IndexSetBuilder();
            foreach (var member in members)
                builder.Include(member);
            return builder.Finish();
        }

        /// <summary>
        /// Constructs an <see cref="IndexSet"/> from a bitmap holding up to 8 indices.
        /// </summary>
        public static IndexSet FromBitmap(byte bitmap)
        {
            return _fromBitmapTable[bitmap];
        }

        /// <summary>
        /// A table of reusable <see cref="FromBitmap"/> sets.
        /// </summary>
        private static readonly IndexSet[] _fromBitmapTable = _buildFromBitmapTable();

        /// <summary>
        /// Builds <see cref="_fromBitmapTable"/>.
        /// </summary>
        private static IndexSet[] _buildFromBitmapTable()
        {
            IndexSet[] res = new IndexSet[256];
            res[0] = None;
            for (int i = 1; i < res.Length; i++)
                res[i] = new IndexSet(new ulong[] { (byte)i });
            return res;
        }

        /// <summary>
        /// A table of reusable <see cref="Of"/> sets.
        /// </summary>
        private static readonly IndexSet[] _singletonTable = _buildSingletonTable();

        /// <summary>
        /// Builds <see cref="_singletonTable"/>.
        /// </summary>
        private static IndexSet[] _buildSingletonTable()
        {
            IndexSet[] res = new IndexSet[64];
            for (int i = 0; i < 8; i++)
                res[i] = FromBitmap((byte)(1 << i));
            for (int i = 8; i < res.Length; i++)
                res[i] = new IndexSet(new ulong[] { 1ul << i });
            return res;
        }

        /// <summary>
        /// Constructs an <see cref="IndexSet"/> containing all indices below the given bound.
        /// </summary>
        public static IndexSet Below(uint bound)
        {
            if (bound < (uint)_belowTable.Length)
                return _belowTable[bound];
            ulong[] bitmap = new ulong[BoolList._numWords(bound)];
            for (int i = 0; i < bitmap.Length; i++)
                bitmap[i] = ulong.MaxValue;
            bitmap[bitmap.Length - 1] = Bitwise.MaskFirst(((bound + 63) % 64) + 1);
            return new IndexSet(bitmap);
        }

        /// <summary>
        /// A table of reusable <see cref="Below"/> sets.
        /// </summary>
        private static readonly IndexSet[] _belowTable = _buildBelowTable();

        /// <summary>
        /// Builds <see cref="_belowTable"/>.
        /// </summary>
        private static IndexSet[] _buildBelowTable()
        {
            IndexSet[] res = new IndexSet[65];
            for (uint i = 0; i < 8; i++)
                res[i] = FromBitmap((byte)Bitwise.MaskFirst(i));
            for (uint i = 8; i < res.Length; i++)
                res[i] = new IndexSet(new ulong[] { Bitwise.MaskFirst(i) });
            return res;
        }

        /// <summary>
        /// Constructs an <see cref="IndexSet"/> for a given contiguous interval of indices.
        /// </summary>
        public static IndexSet Range(uint start, uint size)
        {
            if (start == 0)
                return Below(size);
            throw new NotImplementedException();
        }

        /// <summary>
        /// Determines whether this is the empty set.
        /// </summary>
        public bool IsNone => _bitmap.Length == 0;

        /// <summary>
        /// Determines whether this is a singleton set, and if so, returns its only member.
        /// </summary>
        public bool IsSingleton(out uint elem)
        {
            elem = default;
            if (IsNone)
                return false;
            for (int i = 0; i < _bitmap.Length - 1; i++)
                if (_bitmap[i] != 0)
                    return false;
            ulong last = _bitmap[_bitmap.Length - 1];
            elem = Bitwise.FindFirstOne(last);
            return last == (1ul << (int)elem);
        }

        /// <summary>
        /// Determines whether this set contains all and only indices below a given bound, and if so,
        /// returns that bound.
        /// </summary>
        public bool IsBelow(out uint bound)
        {
            bound = default;
            if (IsNone)
                return false;
            for (int i = 0; i < _bitmap.Length - 1; i++)
                if (_bitmap[i] != ulong.MaxValue)
                    return false;
            ulong last = _bitmap[_bitmap.Length - 1];
            bound = Bitwise.FindFirstZero(last);
            return last == Bitwise.MaskFirst(bound);
        }

        /// <summary>
        /// One more than the greatest index in this set, or zero if the set is empty.
        /// </summary>
        public uint Bound
        {
            get
            {
                if (_bitmap.Length > 0)
                {
                    uint i = (uint)_bitmap.Length - 1;
                    ulong bitmap = _bitmap[i];
                    bool hasLastOne = Bitwise.TryFindLastOne(bitmap, out uint index);
                    Debug.Assert(hasLastOne);
                    return i * 64 + index + 1;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// The number of indices in this set.
        /// </summary>
        public uint Size
        {
            get
            {
                uint size = 0;
                foreach (ulong bitmap in _bitmap)
                    size += Bitwise.Count(bitmap);
                return size;
            }
        }

        /// <summary>
        /// Gets an arbitrary index in this set, assuming it is non-empty.
        /// </summary>
        public uint Arbitrary
        {
            get
            {
                Enumerator en = GetEnumerator();
                bool hasItem = en.MoveNext();
                Debug.Assert(hasItem);
                return en.Current;
            }
        }

        /// <summary>
        /// Constructs an <see cref="IndexSet"/> from the given bitmap array, automatically trimming the
        /// excess words at the end of the array.
        /// </summary>
        internal static IndexSet _build(ulong[] bitmap)
        {
            uint size = (uint)bitmap.Length;
            while (size > 0)
                if (bitmap[--size] != 0)
                    goto nonEmpty;
            return None;
        nonEmpty:
            size++;
            if (size == 1)
            {
                ulong bitmap_0 = bitmap[0];
                byte byteBitmap = unchecked((byte)bitmap_0);
                if (bitmap_0 == byteBitmap)
                    return FromBitmap(byteBitmap);
            }
            else if (size == (uint)bitmap.Length)
            {
                return new IndexSet(bitmap);
            }
            ulong[] nBitmap = new ulong[size];
            Array.Copy(bitmap, nBitmap, (int)size);
            return new IndexSet(nBitmap);
        }

        /// <summary>
        /// Constructs an <see cref="IndexSet"/> from the given bitmap, automatically trimming the
        /// excess words at the end of the array.
        /// </summary>
        internal static IndexSet _build(ReadOnlySpan<ulong> bitmap)
        {
            int size = bitmap.Length;
            while (size > 0)
                if (bitmap[--size] != 0)
                    goto nonEmpty;
            return None;
        nonEmpty:
            size++;
            if (size == 1)
            {
                ulong bitmap_0 = bitmap[0];
                byte byteBitmap = unchecked((byte)bitmap_0);
                if (bitmap_0 == byteBitmap)
                    return FromBitmap(byteBitmap);
            }
            ulong[] nBitmap = new ulong[size];
            for (int i = 0; i < nBitmap.Length; i++)
                nBitmap[i] = bitmap[i];
            return new IndexSet(nBitmap);
        }

        /// <summary>
        /// Indicates whether this set contains the given index.
        /// </summary>
        public bool Contains(uint elem)
        {
            uint index = elem / 64;
            if (index < (int)_bitmap.Length)
                return (_bitmap[index] & (1ul << (int)(elem % 64))) != 0;
            return false;
        }

        /// <summary>
        /// Constructs a set by including the given index in this set.
        /// </summary>
        public IndexSet Include(uint elem)
        {
            if (Contains(elem))
                return this;
            if (_bitmap.Length == 0)
                return Of(elem);
            if (_bitmap.Length == 1 && elem < 64)
            {
                ulong bitmap_0 = _bitmap[0] | (1ul << (int)elem);
                byte byteBitmap = unchecked((byte)bitmap_0);
                if (bitmap_0 == byteBitmap)
                    return FromBitmap(byteBitmap);
                return new IndexSet(new ulong[] { bitmap_0 });
            }
            else
            {
                uint len = BoolList._numWords(elem + 1);
                if (len < (uint)_bitmap.Length)
                {
                    ulong[] bitmap = new ulong[_bitmap.Length];
                    for (int i = 0; i < bitmap.Length; i++)
                        bitmap[i] = _bitmap[i];
                    BoolList._setTrue(bitmap, elem);
                    return new IndexSet(bitmap);
                }
                else
                {
                    ulong[] bitmap = new ulong[len];
                    for (int i = 0; i < _bitmap.Length; i++)
                        bitmap[i] = _bitmap[i];
                    bitmap[len - 1] |= (1ul << (int)(elem % 64));
                    return new IndexSet(bitmap);
                }
            }
        }

        /// <summary>
        /// Gets an ordered list of the indices in this set.
        /// </summary>
        public List<uint> Sort()
        {
            int i = 0;
            uint[] items = new uint[Size];
            foreach (uint item in this)
                items[i++] = item;
            return new List<uint>(items);
        }

        /// <summary>
        /// Gets an enumerator for this set.
        /// </summary>
        public Enumerator GetEnumerator()
        {
            return new Enumerator(this);
        }

        IEnumerator<uint> IEnumerable<uint>.GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// An enumerator for an <see cref="IndexSet"/>. Indices are guaranteed to be returned in ascending order.
        /// </summary>
        public struct Enumerator : IEnumerator<uint>
        {
            private ulong[] _bitmap;
            private uint _current;
            internal Enumerator(ulong[] bitmap)
            {
                _bitmap = bitmap;
                _current = uint.MaxValue;
            }

            public Enumerator(IndexSet set)
            {
                _bitmap = set._bitmap;
                _current = uint.MaxValue;
            }

            /// <summary>
            /// Gets the current item for this enumerator.
            /// </summary>
            public uint Current => _current;

            /// <summary>
            /// Advances to the next, or first, item.
            /// </summary>
            public bool MoveNext()
            {
                // TODO: Could be faster maybe, using bitwise operations
                uint index;
                while ((index = unchecked(++_current) / 64) < _bitmap.Length)
                    if ((_bitmap[index] & (1ul << (int)(_current % 64))) != 0)
                        return true;
                return false;
            }

            /// <summary>
            /// Resets the enumerator to before the beginning of the first item.
            /// </summary>
            public void Reset()
            {
                _current = uint.MaxValue;
            }

            object IEnumerator.Current => Current;

            void IDisposable.Dispose() { }
        }

        /// <summary>
        /// Finds a minimal set of <paramref name="options"/> such that their union is
        /// <see cref="Below(uint)"/> of the given bound.
        /// </summary>
        public static IndexSet MinimalCover(uint bound, ReadOnlySpan<IndexSet> options)
        {
            return BoolImage2.MinimalCover(BoolImage2.Indicator(bound, options)).Support;
        }

        public static IndexSet operator |(IndexSet a, IndexSet b)
        {
            if (a.IsNone)
                return b;
            if (b.IsNone)
                return a;
            if (a._bitmap.Length < b._bitmap.Length)
            {
                ulong[] bitmap = new ulong[b._bitmap.Length];
                bool anyDiff = false;
                for (int i = 0; i < a._bitmap.Length; i++)
                {
                    ulong aBitmap = a._bitmap[i];
                    ulong bBitmap = b._bitmap[i];
                    bitmap[i] = aBitmap | bBitmap;
                    anyDiff |= (aBitmap != bBitmap);
                }
                if (!anyDiff)
                    return b;
                for (int i = a._bitmap.Length; i < b._bitmap.Length; i++)
                    bitmap[i] = b._bitmap[i];
                return new IndexSet(bitmap);
            }
            else if (a._bitmap.Length > 1)
            {
                ulong[] bitmap = new ulong[a._bitmap.Length];
                bool anyDiff = false;
                for (int i = 0; i < b._bitmap.Length; i++)
                {
                    ulong aBitmap = a._bitmap[i];
                    ulong bBitmap = b._bitmap[i];
                    bitmap[i] = aBitmap | bBitmap;
                    anyDiff |= (aBitmap != bBitmap);
                }
                if (!anyDiff)
                    return a;
                for (int i = b._bitmap.Length; i < a._bitmap.Length; i++)
                    bitmap[i] = a._bitmap[i];
                return new IndexSet(bitmap);
            }
            else
            {
                Debug.Assert(a._bitmap.Length == 1 && b._bitmap.Length == 1);
                ulong aBitmap_0 = a._bitmap[0];
                ulong bBitmap_0 = b._bitmap[0];
                ulong bitmap_0 = aBitmap_0 | bBitmap_0;
                byte byteBitmap = unchecked((byte)bitmap_0);
                if (bitmap_0 == byteBitmap)
                    return FromBitmap(byteBitmap);
                if (aBitmap_0 == bitmap_0)
                    return a;
                if (bBitmap_0 == bitmap_0)
                    return b;
                return new IndexSet(new ulong[] { bitmap_0 });
            }
        }

        public static IndexSet operator &(IndexSet a, IndexSet b)
        {
            if (a.IsNone || b.IsNone)
                return None;
            if (a._bitmap.Length <= 1 || b._bitmap.Length <= 1)
            {
                ulong aBitmap_0 = a._bitmap[0];
                ulong bBitmap_0 = b._bitmap[0];
                ulong bitmap_0 = aBitmap_0 & bBitmap_0;
                byte byteBitmap = unchecked((byte)bitmap_0);
                if (bitmap_0 == byteBitmap)
                    return FromBitmap(byteBitmap);
                if (aBitmap_0 == bitmap_0)
                    return a;
                if (bBitmap_0 == bitmap_0)
                    return b;
                return new IndexSet(new ulong[] { bitmap_0 });
            }
            else if (a._bitmap.Length < b._bitmap.Length)
            {
                ulong[] bitmap = new ulong[a._bitmap.Length];
                bool anyDiff = false;
                for (int i = 0; i < a._bitmap.Length; i++)
                {
                    ulong aBitmap = a._bitmap[i];
                    ulong bBitmap = b._bitmap[i];
                    bitmap[i] = aBitmap & bBitmap;
                    anyDiff |= (aBitmap != bBitmap);
                }
                if (!anyDiff)
                    return a;
                return new IndexSet(bitmap);
            }
            else
            {
                ulong[] bitmap = new ulong[b._bitmap.Length];
                bool anyDiff = false;
                for (int i = 0; i < b._bitmap.Length; i++)
                {
                    ulong aBitmap = a._bitmap[i];
                    ulong bBitmap = b._bitmap[i];
                    bitmap[i] = aBitmap & bBitmap;
                    anyDiff |= (aBitmap != bBitmap);
                }
                if (!anyDiff)
                    return a;
                return new IndexSet(bitmap);
            }
        }

        public static IndexSet operator %(IndexSet a, IndexSet b)
        {
            if (a.IsNone)
                return None;
            if (b.IsNone)
                return a;
            if (a._bitmap.Length <= 1)
            {
                ulong aBitmap_0 = a._bitmap[0];
                ulong bBitmap_0 = b._bitmap[0];
                ulong bitmap_0 = aBitmap_0 & ~bBitmap_0;
                byte byteBitmap = unchecked((byte)bitmap_0);
                if (bitmap_0 == byteBitmap)
                    return FromBitmap(byteBitmap);
                if (aBitmap_0 == bitmap_0)
                    return a;
                return new IndexSet(new ulong[] { bitmap_0 });
            }
            else if (a._bitmap.Length <= b._bitmap.Length)
            {
                ulong[] bitmap = new ulong[a._bitmap.Length];
                bool anyDiff = false;
                for (int i = 0; i < a._bitmap.Length; i++)
                {
                    ulong aBitmap = a._bitmap[i];
                    ulong bBitmap = b._bitmap[i];
                    bitmap[i] = aBitmap & ~bBitmap;
                    anyDiff |= (aBitmap != bBitmap);
                }
                if (!anyDiff)
                    return a;
                return new IndexSet(bitmap);
            }
            else
            {
                ulong[] bitmap = new ulong[a._bitmap.Length];
                bool anyDiff = false;
                for (int i = 0; i < b._bitmap.Length; i++)
                {
                    ulong aBitmap = a._bitmap[i];
                    ulong bBitmap = b._bitmap[i];
                    bitmap[i] = aBitmap & ~bBitmap;
                    anyDiff |= (aBitmap != bBitmap);
                }
                if (!anyDiff)
                    return a;
                for (int i = b._bitmap.Length; i < a._bitmap.Length; i++)
                    bitmap[i] = a._bitmap[i];
                return new IndexSet(bitmap);
            }
        }

        public static bool operator <=(IndexSet a, IndexSet b)
        {
            if (a._bitmap.Length > b._bitmap.Length)
                return false;
            for (int i = 0; i < a._bitmap.Length; i++)
                if ((a._bitmap[i] | b._bitmap[i]) != b._bitmap[i])
                    return false;
            return true;
        }
        
        public static bool operator >=(IndexSet a, IndexSet b)
        {
            return b <= a;
        }

        public static bool operator <(IndexSet a, IndexSet b)
        {
            return a <= b && a != b;
        }
        
        public static bool operator >(IndexSet a, IndexSet b)
        {
            return b < a;
        }

        public static bool operator ==(IndexSet a, IndexSet b)
        {
            if (a._bitmap.Length != b._bitmap.Length)
                return false;
            for (int i = 0; i < a._bitmap.Length; i++)
                if (a._bitmap[i] != b._bitmap[i])
                    return false;
            return true;
        }

        public static bool operator !=(IndexSet a, IndexSet b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is IndexSet))
                return false;
            return this == (IndexSet)obj;
        }

        bool IEquatable<IndexSet>.Equals(IndexSet other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            int hash = (int)_bitmap.Length;
            for (int i = 0; i < _bitmap.Length; i++)
                hash = HashCodeHelper.Combine(hash, EqualityHelper.GetHashCode(_bitmap[i]));
            return hash;
        }

        public override string ToString()
        {
            return this.ToShortString();
        }

        void IPrintable.Print(IPrinter printer, Precedence prec)
        {
            if (_bitmap is null)
            {
                printer.Write("<invalid>");
            }
            else if (IsNone)
            {
                printer.WriteItem('∅');
            }
            else
            {
                printer.WriteItem('{');
                bool isFirst = true;
                foreach (uint i in this)
                {
                    if (isFirst)
                        isFirst = false;
                    else
                        printer.Write(", ");
                    printer.Print(Precedence.Free, i);
                }
                printer.WriteItem('}');
            }
        }
    }

    /// <summary>
    /// A helper class for constructing an <see cref="IndexSet"/> by incrementally adding and removing members.
    /// </summary>
    public sealed class IndexSetBuilder : IBuilder<IndexSet>, IEnumerable<uint>
    {
        private ulong[] _bitmap;
        public IndexSetBuilder() // TODO: Deprecate in favor of CreateEmpty
        {
            _bitmap = Array.Empty<ulong>();
        }

        public IndexSetBuilder(uint bound) // TODO: Deprecate in favor of CreateEmpty
        {
            _bitmap = new ulong[BoolList._numWords(bound)];
        }

        /// <summary>
        /// Creates an initially-empty <see cref="IndexSetBuilder"/>.
        /// </summary>
        public static IndexSetBuilder CreateEmpty()
        {
            return new IndexSetBuilder();
        }

        /// <summary>
        /// Indicates whether this is currently the empty set.
        /// </summary>
        public bool IsNone => Bound == 0; // TODO: Faster?

        /// <summary>
        /// One more than the greatest index in this set, or zero if the set is empty.
        /// </summary>
        public uint Bound => _bound(_bitmap);

        /// <summary>
        /// Calculates the bound for a bitmap.
        /// </summary>
        internal static uint _bound(Span<ulong> bitmap)
        {
            uint i = (uint)bitmap.Length;
            while (i > 0)
            {
                i--;
                ulong word = bitmap[(int)i];
                if (Bitwise.TryFindLastOne(word, out uint index))
                    return i * 64 + index + 1;
            }
            return 0;
        }
        
        /// <summary>
        /// Includes the given index in the set.
        /// </summary>
        public void Include(uint elem)
        {
            _include(ref _bitmap, elem);
        }

        /// <summary>
        /// Includes an index in a bitmap.
        /// </summary>
        internal static void _include(ref ulong[] bitmap, uint elem)
        {
            uint index = elem / 64;
            Table.Allocate(ref bitmap, index + 1);
            bitmap[index] |= 1ul << (int)(elem % 64);
        }

        /// <summary>
        /// Includes an index in a non-resizable bitmap.
        /// </summary>
        internal static void _include(Span<ulong> bitmap, uint elem)
        {
            int index = (int)(elem / 64);
            bitmap[index] |= 1ul << (int)(elem % 64);
        }

        /// <summary>
        /// Includes all indices below the given bound in a non-resizable bitmap.
        /// </summary>
        internal static void _includeBelow(Span<ulong> bitmap, uint bound)
        {
            int index = (int)(bound / 64);
            for (int i = 0; i < index; i++)
                bitmap[i] = 0xFFFFFFFFFFFFFFFF;
            uint rem = bound % 64;
            if (rem > 0)
                bitmap[index] |= Bitwise.MaskFirst(rem);
        }

        /// <summary>
        /// Excludes the given index from the set.
        /// </summary>
        public void Exclude(uint item)
        {
            _exclude(_bitmap, item);
        }

        /// <summary>
        /// Excludes an index from a bitmap.
        /// </summary>
        internal static void _exclude(Span<ulong> bitmap, uint elem)
        {
            int index = (int)(elem / 64);
            if (index < bitmap.Length)
                bitmap[index] &= ~(1ul << (int)(elem % 64));
        }

        /// <summary>
        /// Includes all the indices from the given set in this set.
        /// </summary>
        public void Union(IndexSet set)
        {
            _union(ref _bitmap, set);
        }

        /// <summary>
        /// Includes all the indices from the given set, <paramref name="offset"/> added to each, to
        /// this set.
        /// </summary>
        public void UnionAdd(uint offset, IndexSet set)
        {
            // TODO: There's a faster way to do this
            foreach (uint index in set)
                Include(offset + index);
        }

        /// <summary>
        /// Includes all the indices from the given set in the given bitmap.
        /// </summary>
        internal static void _union(ref ulong[] bitmap, IndexSet set)
        {
            if (bitmap.Length < set._bitmap.Length)
            {
                ulong[] nBitmap = new ulong[Math.Max(set._bitmap.Length, bitmap.Length * 2)];
                for (int i = 0; i < bitmap.Length; i++)
                    nBitmap[i] = bitmap[i] | set._bitmap[i];
                for (int i = bitmap.Length; i < set._bitmap.Length; i++)
                    nBitmap[i] = set._bitmap[i];
                bitmap = nBitmap;
            }
            else
            {
                for (int i = 0; i < bitmap.Length; i++)
                    bitmap[i] |= set._bitmap[i];
            }
        }

        /// <summary>
        /// Filters all indices from this set except those present in the given set.
        /// </summary>
        public void Intersect(IndexSet set)
        {
            _intersect(_bitmap, set);
        }

        /// <summary>
        /// Filters all indices from the given bitmap except those present in the given set.
        /// </summary>
        internal static void _intersect(ulong[] bitmap, IndexSet set)
        {
            if (set._bitmap.Length < bitmap.Length)
            {
                for (int i = 0; i < set._bitmap.Length; i++)
                    bitmap[i] &= set._bitmap[i];
                for (int i = set._bitmap.Length; i < bitmap.Length; i++)
                    bitmap[i] = 0;
            }
            else
            {
                for (int i = 0; i < bitmap.Length; i++)
                    bitmap[i] &= set._bitmap[i];
            }
        }

        /// <summary>
        /// Indicates whether the given index is in the set.
        /// </summary>
        public bool Contains(uint elem)
        {
            return _contains(_bitmap, elem);
        }

        /// <summary>
        /// Determines whether a bitmap contains the given index.
        /// </summary>
        internal static bool _contains(ReadOnlySpan<ulong> bitmap, uint elem)
        {
            int index = (int)(elem / 64);
            if (index < bitmap.Length)
                return (bitmap[index] & (1ul << (int)(elem % 64))) != 0;
            else
                return false;
        }

        /// <summary>
        /// Removes all indices from this set.
        /// </summary>
        public void Clear()
        {
            for (int i = 0; i < _bitmap.Length; i++)
                _bitmap[i] = 0;
        }

        /// <summary>
        /// Gets the <see cref="IndexSet"/> resulting from this builder.
        /// </summary>
        public IndexSet Finish()
        {
            return IndexSet._build(_bitmap);
        }

        /// <summary>
        /// Gets an enumerator for this set.
        /// </summary>
        public IndexSet.Enumerator GetEnumerator()
        {
            return new IndexSet.Enumerator(_bitmap);
        }

        IEnumerator<uint> IEnumerable<uint>.GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
