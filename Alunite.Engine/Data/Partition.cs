﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Alunite.Data
{
    /// <summary>
    /// A partition of values of type <typeparamref name="T"/> into disjoint non-empty sets.
    /// </summary>
    public struct Partition<T>
    {
        // TODO
    }

    /// <summary>
    /// A specialization of <see cref="Partition{T}"/> for small <see cref="uint"/> indices.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    [DebuggerTypeProxy(typeof(EnumerableDebugView<IndexSet>))]
    public struct IndexPartition : IEnumerable<IndexSet>
    {
        private uint[] _roots;
        private uint _size;
        internal IndexPartition(uint[] roots, uint size)
        {
            _roots = roots;
            _size = size;
        }

        /// <summary>
        /// The number of sets in this partition.
        /// </summary>
        public uint Size => _size;

        /// <summary>
        /// One more than the greatest index in this partition, or zero if the partition is empty.
        /// </summary>
        public uint Bound => (uint)_roots.Length;
        
        /// <summary>
        /// Gets an enumerator for this partition.
        /// </summary>
        public Enumerator GetEnumerator()
        {
            IndexSetBuilder[] members = new IndexSetBuilder[_size];
            for (int j = 0; j < members.Length; j++)
                members[j] = new IndexSetBuilder((uint)_roots.Length);
            for (uint i = 0; i < (uint)_roots.Length; i++)
                members[_roots[i]].Include(i);
            return new Enumerator(members);
        }

        IEnumerator<IndexSet> IEnumerable<IndexSet>.GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// An enumerator for an <see cref="IndexSet"/>
        /// </summary>
        public struct Enumerator : IEnumerator<IndexSet>
        {
            private IndexSetBuilder[] _members;
            private uint _current;
            internal Enumerator(IndexSetBuilder[] members)
            {
                _members = members;
                _current = uint.MaxValue;
            }

            /// <summary>
            /// Gets the current item for this enumerator.
            /// </summary>
            public IndexSet Current => _members[_current].Finish();

            /// <summary>
            /// Advances to the next, or first, item.
            /// </summary>
            public bool MoveNext()
            {
                return unchecked(++_current) < _members.Length;
            }

            /// <summary>
            /// Resets the enumerator to before the beginning of the first item.
            /// </summary>
            public void Reset()
            {
                _current = uint.MaxValue;
            }

            object IEnumerator.Current => Current;

            void IDisposable.Dispose() { }
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.WriteItem('{');
            bool first = true;
            foreach (IndexSet set in this)
            {
                if (first)
                    first = false;
                else
                    str.Write(", ");
                str.Write(set.ToString());
            }
            str.WriteItem('}');
            return str.ToString();
        }
    }

    /// <summary>
    /// A helper class for constructing an <see cref="IndexPartition"/> by incrementally merging sets within
    /// the partition. This typically starts with the trivial partition of singletons.
    /// </summary>
    public sealed class IndexPartitionMergeBuilder : IBuilder<IndexPartition>
    {
        private uint[] _parents;
        public IndexPartitionMergeBuilder(uint size)
        {
            _parents = new uint[size];
            for (uint i = 0; i < size; i++)
                _parents[i] = i;
        }

        /// <summary>
        /// Merges the sets containing the given items.
        /// </summary>
        public void Merge(uint a, uint b)
        {
            a = _find(a);
            b = _find(b);
            if (a < b)
                _parents[b] = a;
            else
                _parents[a] = b;
        }
        
        /// <summary>
        /// Performs the minimal merge operations such that the given set of elements all occur in the
        /// same set under this partition.
        /// </summary>
        public void Merge(IndexSet set)
        {
            IndexSet.Enumerator en = set.GetEnumerator();
            if (en.MoveNext())
            {
                uint root = _find(en.Current);
                while (en.MoveNext())
                {
                    uint other = _find(en.Current);
                    if (root <= other)
                    {
                        _parents[other] = root;
                    }
                    else
                    {
                        _parents[root] = other;
                        root = other;
                    }
                }
            }
        }

        /// <summary>
        /// Finds the set identifier for the set containing the given item.
        /// </summary>
        private uint _find(uint item)
        {
            uint parent = _parents[item];
            if (parent == item)
                return parent;
            else
                return _parents[item] = _find(parent);
        }

        /// <summary>
        /// Gets the <see cref="IndexPartition"/> resulting from this builder.
        /// </summary>
        public IndexPartition Finish()
        {
            uint nextRootIndex = 0;
            uint[] roots = _parents;
            for (uint i = 0; i < (uint)roots.Length; i++)
            {
                uint parent = roots[i];
                if (parent == i)
                    roots[i] = nextRootIndex++;
                else
                    roots[i] = roots[parent];
            }
            return new IndexPartition(_parents, nextRootIndex);
        }
    }

    /// <summary>
    /// A helper class for constructing an <see cref="IndexPartition"/> by incrementally refining the partition.
    /// This typically starts with the trivial singleton partition.
    /// </summary>
    public sealed class IndexPartitionRefineBuilder : IBuilder<IndexPartition>
    {
        public IndexPartitionRefineBuilder(uint size)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Ensures that the members of the given set are not placed with any other items in the resulting partition.
        /// </summary>
        public void Refine(IndexSet set)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the <see cref="IndexPartition"/> resulting from this builder. This is the unique minimal-size partition which
        /// satisfies all specified constraints.
        /// </summary>
        public IndexPartition Finish()
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// A helper class for constructing an <see cref="IndexPartition"/> by incrementally specifying "split" constraints
    /// which force items to be placed in different sets. This typically starts with the trivial singleton partition.
    /// </summary>
    public sealed class IndexPartitionSplitBuilder : IBuilder<IndexPartition>
    {
        public IndexPartitionSplitBuilder(uint size)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Ensures that the members of the given set are not placed with any other items in the resulting partition.
        /// </summary>
        public void Refine(IndexSet set)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Ensures that the given items are not placed together in the resulting partition.
        /// </summary>
        public void Split(uint a, uint b)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Ensures that none of the given items are placed together in the resulting partition.
        /// </summary>
        public void Split(IndexSet set)
        {
            throw new NotImplementedException();
        }
        
        /// <summary>
        /// Gets the <see cref="IndexPartition"/> resulting from this builder. This is a minimal-size partition which
        /// satisfies all specified constraints.
        /// </summary>
        public IndexPartition Finish()
        {
            throw new NotImplementedException();
        }
    }
}
