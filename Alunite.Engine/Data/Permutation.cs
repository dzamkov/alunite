﻿using System;

namespace Alunite.Data
{
    /// <summary>
    /// An ordering of some set of values of type <typeparamref name="T"/>. This is a specialization of
    /// <see cref="List{T}"/> which guarantees uniqueness of items.
    /// </summary>
    public struct Permutation<T>
    {
        internal T[] _items;
        internal Permutation(T[] items)
        {
            _items = items;
        }

        /// <summary>
        /// The empty <see cref="Permutation{T}"/>.
        /// </summary>
        public static Permutation<T> Empty => new Permutation<T>(Array.Empty<T>());
        
        /// <summary>
        /// Indicates whether this is the empty permutation.
        /// </summary>
        public bool IsEmpty => _items.Length == 0;

        /// <summary>
        /// The length of this permutation.
        /// </summary>
        public uint Length => (uint)_items.Length;

        /// <summary>
        /// Gets the item at the given index.
        /// </summary>
        public T this[uint index] => _items[index];

        public static implicit operator List<T>(Permutation<T> perm)
        {
            return new List<T>(perm._items);
        }

        public static implicit operator SpanList<T>(Permutation<T> perm)
        {
            return new SpanList<T>(perm._items);
        }
    }

    /// <summary>
    /// Contains functions for constructing and manipulating <see cref="Permutation{T}"/>s.
    /// </summary>
    public static class Permutation
    {
        /// <summary>
        /// Constructs a permutation consisting of the given item.
        /// </summary>
        public static Permutation<T> Of<T>(T item)
        {
            return new Permutation<T>(new T[] { item });
        }

        /// <summary>
        /// The empty permutation of the given type.
        /// </summary>
        public static Permutation<T> Empty<T>()
        {
            return Permutation<T>.Empty;
        }
    }

    /// <summary>
    /// A helper class for constructing a <see cref="Permutation{T}"/> by incrementally appending items.
    /// </summary>
    public sealed class PermutationBuilder<T> : IBuilder<Permutation<T>>
    {
        // TODO: Make more efficient using specialized structures
        private ListBuilder<T> _items;
        private MapBuilder<T, uint> _map;
        private PermutationBuilder(ListBuilder<T> items, MapBuilder<T, uint> map)
        {
            _items = items;
            _map = map;
        }

        /// <summary>
        /// Creates a new initially-empty <see cref="PermutationBuilder{T}"/>.
        /// </summary>
        public static PermutationBuilder<T> CreateEmpty()
        {
            return new PermutationBuilder<T>(
                ListBuilder<T>.CreateEmpty(),
                MapBuilder<T, uint>.CreateEmpty());
        }

        /// <summary>
        /// Creates a <see cref="PermutationBuilder{T}"/> with the given initial permutation.
        /// </summary>
        public static PermutationBuilder<T> Create(Permutation<T> initial)
        {
            var map = MapBuilder<T, uint>.CreateEmpty();
            for (uint i = 0; i < initial.Length; i++)
                map.Add(initial[i], i);
            return new PermutationBuilder<T>(ListBuilder<T>.Create(initial), map);
        }

        /// <summary>
        /// The current length of the permutation under construction.
        /// </summary>
        public uint Length => _items.Length;

        /// <summary>
        /// Gets the item at the given index.
        /// </summary>
        public T this[uint index] => _items[index];

        /// <summary>
        /// Ensures that the given item is included in the resulting permutation, appending it if
        /// needed. The index of the item is returned.
        /// </summary>
        public uint Include(T item)
        {
            if (!_map.TryGet(item, out uint index))
            {
                index = _items.Append(item);
                _map.Add(item, index);
            }
            return index;
        }
        
        /// <summary>
        /// Gets the permutation resulting from this builder.
        /// </summary>
        public Permutation<T> Finish()
        {
            return new Permutation<T>(_items.FinishToArray());
        }
    }
}
