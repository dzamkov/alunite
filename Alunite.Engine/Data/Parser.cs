﻿using System;
using System.Runtime.CompilerServices;

namespace Alunite.Data
{
    /// <summary>
    /// Contains helper functions for parsing structures within a list or <see cref="string"/>.
    /// </summary>
    public static class Parser
    {
        /// <summary>
        /// Attempts to read the next item from a buffered list, returning <see cref="false"/> if the end of the list
        /// has been reached. This tracks the buffer with local variables to minimize virtual calls to the reader.
        /// </summary>
        /// <param name="buffer">A variable which holds <paramref name="reader"/>'s buffer.</param>
        /// <param name="head">The index of the next item to be read from <paramref name="buffer"/>.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool TryFastReadItem<T>(
            this IBufferedListReader<T> reader,
            ref SpanList<T> buffer, ref uint head,
            out T item)
        {
        retry:
            if (head < buffer.Length)
            {
                item = buffer[head++];
                return true;
            }
            else if (buffer.Length >= ListReader<T>.MinBufferLength)
            {
                reader.Skip(head);
                buffer = reader.Buffer;
                head = 0;
                goto retry;
            }
            else
            {
                item = default;
                return false;
            }
        }

        /// <summary>
        /// Parses the given item, failing if any other item is present.
        /// </summary>
        public static bool AcceptItem<T>(this IListReader<T> input, T expected)
        {
            if (!input.TryReadItem(out T item))
                return false;
            if (EqualityHelper.Equals(expected, item))
                return false;
            return true;
        }

        /// <summary>
        /// Parses the given <see cref="string"/>, failing if any other <see cref="string"/> is present.
        /// </summary>
        public static bool AcceptString(this IListReader<char> input, string expected)
        {
            Span<char> buffer = stackalloc char[expected.Length];
            if (input.Read(buffer) != (uint)expected.Length)
                return false;
            for (int i = 0; i < expected.Length; i++)
                if (buffer[i] != expected[i])
                    return false;
            return true;
        }

        /// <summary>
        /// Advances the parser input until it reaches an item that is not in <paramref name="set"/>, or the
        /// end of the input source.
        /// </summary>
        /// <returns>Whether any items were accepted.</returns>
        public static bool SkipClass<T, TSet>(this IBufferedListReader<T> input, TSet set)
            where TSet : ISet<T>
        {
            bool anyRead = false;
            SpanList<T> buffer = input.Buffer;
            uint head = 0;
        next:
            if (head < buffer.Length)
            {
                if (set.Contains(buffer[head]))
                {
                    head++;
                    goto next;
                }
            }
            else if (buffer.Length >= ListReader<T>.MinBufferLength)
            {
                input.Skip(head);
                anyRead = true;
                buffer = input.Buffer;
                head = 0;
                goto next;
            }
            if (head > 0)
            {
                input.Skip(head);
                return true;
            }
            else
            {
                return anyRead;
            }
        }

        /// <summary>
        /// Greedily parses a non-empty list of items from <paramref name="set"/>.
        /// </summary>
        public static bool AcceptClass<T, TSet>(
            this IBufferedListReader<T> input,
            TSet set, out List<T> list)
            where TSet : ISet<T>
        {
            ListBuilder<T> builder;
            SpanList<T> buffer = input.Buffer;
            uint head = 0;
        next:
            if (head < buffer.Length)
            {
                if (set.Contains(buffer[head]))
                {
                    head++;
                    goto next;
                }
            }
            else if (buffer.Length >= ListReader<T>.MinBufferLength)
            {
                builder = ListBuilder<T>.CreateEmpty();
                builder.Concat(buffer);
                input.Skip(head);
                buffer = input.Buffer;
                head = 0;
                goto nextWithBuilder;
            }
            if (head > 0)
            {
                list = buffer;
                input.Skip(head);
                return true;
            }
            else
            {
                list = default;
                return false;
            }
        nextWithBuilder:
            if (head < buffer.Length)
            {
                if (set.Contains(buffer[head]))
                {
                    head++;
                    goto nextWithBuilder;
                }
            }
            else if (buffer.Length >= ListReader<T>.MinBufferLength)
            {
                builder.Concat(buffer);
                input.Skip(head);
                buffer = input.Buffer;
                head = 0;
                goto nextWithBuilder;
            }
            if (head > 0)
            {
                builder.Concat(buffer.SlicePrefix(head));
                input.Skip(head);
            }
            list = builder.Finish();
            return true;
        }

        /// <summary>
        /// Greedily parses a non-empty <see cref="string"/> of characters from <paramref name="set"/>.
        /// </summary>
        public static unsafe bool AcceptClass<TSet>(
            this IBufferedListReader<char> input,
            TSet set, out string str)
            where TSet : ISet<char>
        {
            StringBuilder builder;
            SpanList<char> buffer = input.Buffer;
            uint head = 0;
        next:
            if (head < buffer.Length)
            {
                if (set.Contains(buffer[head]))
                {
                    head++;
                    goto next;
                }
            }
            else if (buffer.Length >= ListReader<char>.MinBufferLength)
            {
                builder = StringBuilder.CreateEmpty();
                builder.Write(buffer);
                input.Skip(head);
                buffer = input.Buffer;
                head = 0;
                goto nextWithBuilder;
            }
            if (head > 0)
            {
                fixed (char* bufferPtr = buffer._span)
                    str = new string(bufferPtr, 0, (int)head);
                input.Skip(head);
                return true;
            }
            else
            {
                str = default;
                return false;
            }
        nextWithBuilder:
            if (head < buffer.Length)
            {
                if (set.Contains(buffer[head]))
                {
                    head++;
                    goto nextWithBuilder;
                }
            }
            else if (buffer.Length >= ListReader<char>.MinBufferLength)
            {
                builder.Write(buffer);
                input.Skip(head);
                buffer = input.Buffer;
                head = 0;
                goto nextWithBuilder;
            }
            if (head > 0)
            {
                builder.Write(buffer.SlicePrefix(head));
                input.Skip(head);
            }
            str = builder.Finish();
            return true;
        }

        /// <summary>
        /// Greedily parses whitespace from the given input. Returns <see cref="true"/> if at least one whitespace
        /// character has been read.
        /// </summary>
        public static bool AcceptWhitespace(this IBufferedListReader<char> input)
        {
            return input.SkipClass(CharSet.Whitespace);
        }

        /// <summary>
        /// Parses an integer value.
        /// </summary>
        public static bool AcceptIntegerLiteral(this IBufferedListReader<char> input, out long value)
        {
            SpanList<char> buffer = input.Buffer;
            uint head = 0;
            if (input.TryFastReadItem(ref buffer, ref head, out char ch))
            {
                // Parse unary prefix
                bool negate = false;
                if (ch == '-')
                {
                    negate = true;
                    if (!input.TryFastReadItem(ref buffer, ref head, out ch))
                        goto fail;

                }
                else if (ch == '+')
                {
                    if (!input.TryFastReadItem(ref buffer, ref head, out ch))
                        goto fail;
                }

                // Parse leading digit
                if (ch >= '1' && ch <= '9')
                {
                    value = ch - '0';
                next:
                    if (input.TryFastReadItem(ref buffer, ref head, out ch))
                    {
                        if (ch >= '0' && ch <= '9')
                        {
                            value = value * 10;
                            value += ch - '0';
                            goto next;
                        }
                        else
                        {
                            input.Skip(head - 1);
                            if (negate) value = -value;
                            return true;
                        }
                    }
                }
                else if (ch == '0')
                {
                    input.Skip(head);
                    value = 0;
                    return true;
                }
            }
        fail:
            value = default;
            return false;
        }

        /// <summary>
        /// Parses a <see cref="Scalar"/> value.
        /// </summary>
        public static bool AcceptScalarLiteral(this IBufferedListReader<char> input, out Scalar value)
        {
            SpanList<char> buffer = input.Buffer;
            uint head = 0;
            if (input.TryFastReadItem(ref buffer, ref head, out char ch))
            {
                // Parse unary prefix
                bool negate = false;
                if (ch == '-')
                {
                    negate = true;
                    if (!input.TryFastReadItem(ref buffer, ref head, out ch))
                        goto fail;

                }
                else if (ch == '+')
                {
                    if (!input.TryFastReadItem(ref buffer, ref head, out ch))
                        goto fail;
                }

                // Parse leading digit
                if (ch >= '1' && ch <= '9')
                {
                    Scalar n = (uint)(ch - '0');
                    Scalar d = 1;
                nextN:
                    if (input.TryFastReadItem(ref buffer, ref head, out ch))
                    {
                        if (ch >= '0' && ch <= '9')
                        {
                            n = n * 10;
                            n += (uint)(ch - '0');
                            goto nextN;
                        }
                        else if (ch == '.')
                        {
                            goto nextD;
                        }
                        else if (ch == 'e' || ch == 'E')
                        {
                            throw new NotImplementedException();
                        }
                        else
                        {
                            input.Skip(head - 1);
                            value = n;
                            if (negate) value = -value;
                            return true;
                        }
                    }

                nextD:
                    if (input.TryFastReadItem(ref buffer, ref head, out ch))
                    {
                        if (ch >= '0' && ch <= '9')
                        {
                            n = n * 10;
                            d = d * 10;
                            n += (uint)(ch - '0');
                            goto nextD;
                        }
                        else if (ch == 'e' || ch == 'E')
                        {
                            throw new NotImplementedException();
                        }
                        else
                        {
                            input.Skip(head - 1);
                            value = n / d;
                            if (negate) value = -value;
                            return true;
                        }
                    }
                }
                else if (ch == '0')
                {
                    input.Skip(head);
                    value = 0;
                    return true;
                }
            }
        fail:
            value = default;
            return false;
        }

        /// <summary>
        /// Parses a quoted <see cref="string"/> value, potentially including escaped characters.
        /// </summary>
        public static bool AcceptStringLiteral(this IBufferedListReader<char> input, out string value)
        {
            SpanList<char> buffer = input.Buffer;
            uint head = 0;
            if (input.TryFastReadItem(ref buffer, ref head, out char ch) && ch == '"')
            {
                StringBuilder builder = StringBuilder.CreateEmpty();
                while (input.TryFastReadItem(ref buffer, ref head, out ch))
                {
                    switch (ch)
                    {
                        case '"':
                            input.Skip(head);
                            value = builder.Finish();
                            return true;
                        case '\\':
                            throw new NotImplementedException();
                        default:
                            builder.WriteItem(ch);
                            break;
                    }
                }
            }
            value = default;
            return false;
        }
    }
}