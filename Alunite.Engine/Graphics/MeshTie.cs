﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

using Alunite.Data;
using Alunite.Data.Geometry;
using Alunite.Data.Serialization;
using Alunite.Resources;

namespace Alunite.Graphics
{
    /// <summary>
    /// A particular arrangement of vertices and edges that can be exhibited by a <see cref="Mesh"/>. When placed
    /// next to a complementary tie on an adjacent mesh, the meshes will meld together, eliminating seams and
    /// redundant geometry between the ties.
    /// </summary>
    public struct MeshTie
    {
        /// <summary>
        /// The reference configuration on which this <see cref="MeshTie"/> is based.
        /// </summary>
        internal _ReferenceMeshTie _ref;

        /// <summary>
        /// The spatial transform from <see cref="Reference"/> to this <see cref="MeshTie"/>.
        /// </summary>
        internal Motion3 _trans;

        /// <summary>
        /// The textures applied to <see cref="Reference"/>, defined in tems of the textures for
        /// the <see cref="MeshTie"/>.
        /// </summary>
        internal List<DependentTexture> _textures;

        /// <summary>
        /// Identifies which segments of this <see cref="MeshTie"/> correspond to each segment
        /// of <see cref="Reference"/>.
        /// </summary>
        internal List<uint> _segs;

        /// <summary>
        /// Constructs a <see cref="MeshTie"/> from the given tie data.
        /// </summary>
        /// <param name="textureBound">One more than the highest texture index referenced by the data.</param>
        internal static MeshTie _build(ReadOnlySpan<byte> data, uint textureBound)
        {
            // Get the signature, centroid and texture centroids for the tie. These are required for matching
            // TODO: Add more information (such as vertex connectivity) to the signature
            int sig = 0;
            Vector3 centroid = Vector3.Zero;
            uint numVerts = 0;
            Span<Vector2> textureCentroids = stackalloc Vector2[(int)textureBound];
            Span<uint> numTextureRefs = stackalloc uint[(int)textureBound];
            ReadOnlySpan<byte> buffer = data;
            while (!buffer.IsEmpty)
            {
                var primHeader = Binary._fastReadAlignedNative<_ReferenceMeshTie._PrimitiveHeader>(ref buffer);
                sig = HashCodeHelper.Combine(sig, primHeader.GetHashCode());
                for (uint i = 0; i < primHeader.NumVertices; i++)
                {
                    SpanMeshVertex vert = VertexBinary.ReadAlignedNativeMeshVertex(ref buffer, primHeader.VertexType);
                    centroid += vert.Position;
                    numVerts++;
                    SpanList<VertexTextureFactor> vertTextures = vert.Textures;
                    for (uint j = 0; j < vertTextures.Length; j++)
                    {
                        VertexTextureFactor vertTexture = vertTextures[j];
                        textureCentroids[(int)vertTexture.Index] += vertTexture.Coord;
                        numTextureRefs[(int)vertTexture.Index]++;
                    }
                }
                Binary.Skip(ref buffer, primHeader.NumSegments * _ReferenceMeshTie._SegmentInfo.Size);
            }
            centroid /= numVerts;
            for (int i = 0; i < textureCentroids.Length; i++)
                textureCentroids[i] /= numTextureRefs[i];

            // Check for an existing reference mesh tie matching the signature
            _Matcher matcher = _Matcher.Create(centroid, List.Of(textureCentroids));
            var viewer = _refs.View(sig);
            using (var it = viewer.Iterate())
            {
                while (it.TryNext(out Weak<_ReferenceMeshTie> weakRef))
                {
                    _ReferenceMeshTie @ref = weakRef.Target;
                    if (@ref is null)
                    {
                        it.Clean();
                    }
                    else
                    {
                        // Can we use this reference mesh tie here?
                        var point = matcher.Mark();
                        if (matcher.TryMatch(@ref, data, false))
                        {
                            return new MeshTie
                            {
                                _ref = @ref,
                                _trans = matcher.Transform,
                                _textures = matcher.Textures,
                                _segs = matcher.Segments
                            };
                        }
                        matcher.Reset(point);
                    }
                }
            }

            // We'll probably need to add a new reference mesh tie. Get a lock on the signature in the registry
            if (!viewer.TryBrowse(out var browser))
            {
                browser = _refs.Browse(sig);
                var it = viewer.Iterate();
                while (it.TryNext(out Weak<_ReferenceMeshTie> weakRef))
                {
                    _ReferenceMeshTie @ref = weakRef.Target;
                    if (@ref is null)
                    {
                        it.Clean();
                    }
                    else
                    {
                        // Can we use this reference mesh tie here?
                        var point = matcher.Mark();
                        if (matcher.TryMatch(@ref, data, false))
                        {
                            MeshTie res = new MeshTie
                            {
                                _ref = @ref,
                                _trans = matcher.Transform,
                                _textures = matcher.Textures,
                                _segs = matcher.Segments
                            };
                            browser.Dispose();
                            return res;
                        }
                        matcher.Reset(point);
                    }
                }
            }

            // Create new reference mesh tie
            using (browser)
            {
                // How many unique textures are actually referenced in the tie?
                uint numTextures = 0;
                for (int i = 0; i < numTextureRefs.Length; i++)
                    if (numTextureRefs[i] > 0)
                        numTextures++;

                // Remove unused texture indices
                DependentTexture[] texturesItems = new DependentTexture[numTextures];
                numTextures = 0;
                for (int i = 0; i < numTextureRefs.Length; i++)
                {
                    ref uint textureInfo = ref numTextureRefs[i];
                    if (textureInfo > 0)
                    {
                        textureInfo = numTextures++;
                        texturesItems[textureInfo] = new DependentTexture(
                            (uint)i, Rotation2.Identity, Affine2.Translate(textureCentroids[i]));
                    }
                }
                ReadOnlySpan<uint> textureRefIndices = numTextureRefs;

                // Rewrite tie data in reference configuration
                buffer = data;
                byte[] nData = new byte[data.Length];
                uint nHead = 0;
                uint numSegs = 0;
                while (!buffer.IsEmpty)
                {
                    var primHeader = Binary._fastReadAlignedNative<_ReferenceMeshTie._PrimitiveHeader>(ref buffer);
                    Binary._fastWriteAlignedNative(nData, ref nHead, in primHeader);
                    for (uint i = 0; i < primHeader.NumVertices; i++)
                    {
                        SpanMeshVertex vert = VertexBinary.ReadAlignedNativeMeshVertex(ref buffer, primHeader.VertexType);
                        Binary._fastWriteAlignedNative(nData, ref nHead, vert.Position - centroid);
                        SpanList<VertexTextureFactor> vertTextures = vert.Textures;
                        for (uint j = 0; j < vertTextures.Length; j++)
                        {
                            VertexTextureFactor vertTexture = vertTextures[j];
                            uint vertTextureIndex = vertTexture.Index;
                            vertTexture.Coord = vertTexture.Coord - textureCentroids[(int)vertTextureIndex];
                            vertTexture.Index = textureRefIndices[(int)vertTextureIndex];
                            Binary._fastWriteAlignedNative(nData, ref nHead, in vertTexture);
                        }
                    }
                    numSegs += primHeader.NumSegments;
                    var segments = Binary.ReadAlignedNativeList<_ReferenceMeshTie._SegmentInfo>(ref buffer, primHeader.NumSegments);
                    Binary.WriteAlignedNativeList(nData, ref nHead, segments);
                }
                Debug.Assert(nHead == (uint)nData.Length);

                // Build reference mesh tie
                _ReferenceMeshTie @ref = new _ReferenceMeshTie(nData, numTextures, numSegs);

                // Add to registry
                browser.Add(Weak<_ReferenceMeshTie>.Build(@ref));

                // Finalize returned mesh tie
                _numRefs++;
                return new MeshTie
                {
                    _ref = @ref,
                    _trans = Motion3.Translate(centroid),
                    _textures = List.Of(texturesItems),
                    _segs = List.Identity(numSegs)
                };
            }
        }

        private static uint _numRefs = 0; // TODO: Debug code, remove

        /// <summary>
        /// A registry of <see cref="_ReferenceMeshTie"/>s that are in use, indexed by their structural signature.
        /// </summary>
        private static readonly Registry<int, Weak<_ReferenceMeshTie>> _refs =
            Registry<int, Weak<_ReferenceMeshTie>>.CreateEmpty();

        /// <summary>
        /// A helper class for finding the correspondence between a <see cref="_ReferenceMeshTie"/> and a
        /// particular presentation (<see cref="MeshTie"/>) of it.
        /// </summary>
        internal sealed class _Matcher
        {
            private ListBuilder<uint> _verts;
            private ListBuilder<uint> _segs;

            /// <summary>
            /// The offset applied to spatial coordinates of the subject mesh tie.
            /// </summary>
            private Vector3 _offset;

            /// <summary>
            /// The covariance matrix which relates vertex positions in the reference mesh tie to those in the
            /// subject mesh tie. This is used with <see cref="Rotation.Wahba"/> to compute the rotation between
            /// the ties.
            /// </summary>
            private Matrix3x3 _cov;

            /// <summary>
            /// The current best approximation of the rotation from the reference mesh tie to the subject mesh tie.
            /// Note that <see cref="_cov"/> determines the actual optimal rotation, but this requires an expensive
            /// computation that we want to avoid doing unless necessary.
            /// </summary>
            private Rotation3 _approxRot;
            
            /// <summary>
            /// The offsets applied to the texture coordinates of the subject mesh tie.
            /// </summary>
            private List<Vector2> _textureOffsets;
            
            /// <summary>
            /// Provides matching information about the textures for the reference mesh tie.
            /// </summary>
            private ListBuilder<_TextureInfo> _textureInfos;
            
            /// <summary>
            /// Tracks modifications (along with initial values) made to <see cref="_TextureInfo"/>. This is used
            /// to implement <see cref="Reset(Point)"/>.
            /// </summary>
            private ListBuilder<(uint, _TextureInfo)> _textureHistory;

            /// <summary>
            /// The spatial transformation from the reference mesh tie to the subject mesh tie, calculated using
            /// <see cref="_approxRot"/>.
            /// </summary>
            internal Motion3 _approxTrans => new Motion3(_approxRot, _offset);

            /// <summary>
            /// The rotation from the reference mesh tie to the subject mesh tie.
            /// </summary>
            internal Rotation3 _rot
            {
                get
                {
                    Rotation.Wahba(_cov, out _approxRot, out _);
                    return _approxRot;
                }
            }

            /// <summary>
            /// Provides matching information about a texture in the reference mesh tie.
            /// </summary>
            private struct _TextureInfo
            {
                /// <summary>
                /// The <see cref="_TextureInfo"/> that indicates no matching information exists.
                /// </summary>
                public static _TextureInfo Initial => new _TextureInfo
                {
                    _index = uint.MaxValue,
                    _cov = Matrix2x2.Zero
                };

                /// <summary>
                /// The index of the matching texture in the subject mesh tie, or <see cref="uint.MaxValue"/> if not
                /// yet determined.
                /// </summary>
                internal uint _index;
                
                /// <summary>
                /// The covariance matrix which relate texture coordinates in the reference mesh tie to those in the
                /// subject mesh tie. This is used to compute the linear relationship between coordinates.
                /// </summary>
                internal Matrix2x2 _cov;

                /// <summary>
                /// The total square length of all of the reference texture coordinates for this texture. This is used
                /// to compute <see cref="_scaling"/>.
                /// </summary>
                internal Scalar _refSqrLen;
                
                /// <summary>
                /// The total square length of all of the subject texture coordinates for this texture. This is used
                /// to compute <see cref="_scaling"/>.
                /// </summary>
                internal Scalar _subjSqrLen;

                /// <summary>
                /// The scale factor from the reference texture coordinates to subject texture coordinates. This could
                /// be negative to indicate reflection.
                /// </summary>
                internal Scalar _scaling
                {
                    get
                    {
                        return Scalar.Sqrt(_subjSqrLen / _refSqrLen);
                    }
                }

                /// <summary>
                /// The rotation from reference texture coordinates to subject texture coordinates.
                /// </summary>
                internal Rotation2 _rot
                {
                    get
                    {
                        Rotation.Wahba(_cov, out Rotation2 rot, out _);
                        return rot;
                    }
                }
            }

            /// <summary>
            /// Creates a new matcher.
            /// </summary>
            public static _Matcher Create(Vector3 offset, List<Vector2> textureOffsets)
            {
                return new _Matcher
                {
                    _verts = ListBuilder<uint>.CreateEmpty(),
                    _segs = ListBuilder<uint>.CreateEmpty(),
                    _offset = offset,
                    _cov = Matrix3x3.Zero,
                    _approxRot = Rotation3.Identity,
                    _textureOffsets = textureOffsets,
                    _textureInfos = ListBuilder<_TextureInfo>.CreateEmpty(),
                    _textureHistory = ListBuilder<(uint, _TextureInfo)>.CreateEmpty()
                };
            }

            /// <summary>
            /// The spatial transformation from the reference mesh tie to the subject mesh tie, if known.
            /// </summary>
            public Motion3 Transform => new Motion3(_rot, _offset);

            /// <summary>
            /// The textures applied to the <see cref="_ReferenceMeshTie"/>.
            /// </summary>
            public List<DependentTexture> Textures
            {
                get
                {
                    var textures = ListBuilder<DependentTexture>.CreateDefault(_textureInfos.Length);
                    for (uint i = 0; i < textures.Length; i++)
                    {
                        _TextureInfo textureInfo = _textureInfos[i];
                        Rotation2 rot = textureInfo._rot;
                        textures[i] = new DependentTexture(textureInfo._index, rot.Inverse,
                            new Similarity2(textureInfo._scaling, rot, false, _textureOffsets[textureInfo._index]));
                    }
                    return textures.Finish();
                }
            }

            /// <summary>
            /// Identifies which segments in the subject mesh tie correspond to each segment in
            /// the reference mesh tie.
            /// </summary>
            public SpanList<uint> Segments => _segs;

            /// <summary>
            /// Attempts to match the given <see cref="_ReferenceMeshTie"/> against a potential presentation of it. If
            /// successful, the <see cref="_Matcher"/> will be updated with information about the correspondence.
            /// </summary>
            /// <param name="shouldComplement">If <see cref="true"/>, indicates that the segments of the target mesh
            /// tie should be flipped.</param>
            public bool TryMatch(_ReferenceMeshTie @ref, ReadOnlySpan<byte> tieData, bool shouldComplement)
            {
                // Matcher must be in its initial state
                if (_segs.Length != 0)
                    throw new InvalidOperationException("Matcher must be in its initial state");

                // Match is only possible if the two mesh ties have the same data size
                if (@ref._data.Length != tieData.Length)
                    return false;

                // Perform matching
                _textureInfos.Clear();
                while (_textureInfos.Length < @ref.NumTextures)
                    _textureInfos.Append(_TextureInfo.Initial);
                _VertexContinuation cont = new _TieContinuation
                {
                    BaseVertexIndex = 0,
                    BaseSegmentIndex = 0,
                    ReferenceBuffer = @ref._data,
                    TargetBuffer = tieData,
                    ShouldComplement = shouldComplement
                };
                return _tryMatch(ref cont);
            }

            /// <summary>
            /// Encapsulates a set of elements that remain to be matched within the context of a mesh tie.
            /// </summary>
            private ref struct _TieContinuation
            {
                /// <summary>
                /// The "global" vertex index of the first vertex in the current primitive.
                /// </summary>
                public uint BaseVertexIndex;

                /// <summary>
                /// The "global" segment index of the first segment in the current primitive.
                /// </summary>
                public uint BaseSegmentIndex;

                /// <summary>
                /// The remaining data for the reference mesh tie, starting at a primitive header.
                /// </summary>
                public ReadOnlySpan<byte> ReferenceBuffer;

                /// <summary>
                /// The remaining data for the target mesh tie, starting at a primitive header.
                /// </summary>
                public ReadOnlySpan<byte> TargetBuffer;

                /// <summary>
                /// If <see cref="true"/>, indicates that the segments of the target mesh tie should be flipped.
                /// </summary>
                public bool ShouldComplement;
            }

            /// <summary>
            /// Encapsulates a set of elements that remain to be matched within the context of a mesh tie primitive.
            /// </summary>
            private ref struct _PrimitiveContinuation
            {
                /// <summary>
                /// The "global" vertex index of the first vertex in the current primitive.
                /// </summary>
                public uint BaseVertexIndex => Continuation.BaseVertexIndex - ReferenceVertices.Length;

                /// <summary>
                /// The "global" segment index of the first segment in the current primitive.
                /// </summary>
                public uint BaseSegmentIndex => Continuation.BaseSegmentIndex - ReferenceSegments.Length;

                /// <summary>
                /// The vertices of the reference primitive.
                /// </summary>
                public RegularMeshVertexSpanList ReferenceVertices;

                /// <summary>
                /// The segments of the reference primitive.
                /// </summary>
                public SpanList<_ReferenceMeshTie._SegmentInfo> ReferenceSegments;

                /// <summary>
                /// The vertices of the target primitive.
                /// </summary>
                public RegularMeshVertexSpanList TargetVertices;

                /// <summary>
                /// The segments of the target primitive.
                /// </summary>
                public SpanList<_ReferenceMeshTie._SegmentInfo> TargetSegments;

                /// <summary>
                /// The index of the first vertex within the current primitive that has not yet been matched.
                /// </summary>
                public uint NextVertexIndex;

                /// <summary>
                /// The index of the first segment within the current primitive that has not yet been matched.
                /// </summary>
                public uint NextSegmentIndex;

                /// <summary>
                /// If <see cref="true"/>, indicates that the segments of the target mesh tie should be flipped.
                /// </summary>
                public bool ShouldComplement => Continuation.ShouldComplement;

                /// <summary>
                /// Encapsulates the remaining entities to be matched.
                /// </summary>
                public _TieContinuation Continuation;

                public static implicit operator _PrimitiveContinuation(_TieContinuation source)
                {
                    return new _PrimitiveContinuation
                    {
                        ReferenceVertices = RegularMeshVertexSpanList.Empty,
                        ReferenceSegments = List.Empty<_ReferenceMeshTie._SegmentInfo>(),
                        TargetVertices = RegularMeshVertexSpanList.Empty,
                        TargetSegments = List.Empty<_ReferenceMeshTie._SegmentInfo>(),
                        NextVertexIndex = 0,
                        NextSegmentIndex = 0,
                        Continuation = source
                    };
                }
            }
            
            /// <summary>
            /// Encapsulates a set of elements that remain to be matched within the context of a mesh tie vertex.
            /// </summary>
            private ref struct _VertexContinuation
            {
                /// <summary>
                /// The remaining texture factors for the reference vertex.
                /// </summary>
                public SpanList<VertexTextureFactor> ReferenceTextures;

                /// <summary>
                /// The remaining texture factors for the target vertex.
                /// </summary>
                public SpanList<VertexTextureFactor> TargetTextures;

                /// <summary>
                /// Encapsulates the remaining entities to be matched.
                /// </summary>
                public _PrimitiveContinuation Continuation;

                public static implicit operator _VertexContinuation(_TieContinuation source)
                {
                    return (_PrimitiveContinuation)source;
                }

                public static implicit operator _VertexContinuation(_PrimitiveContinuation source)
                {
                    return new _VertexContinuation
                    {
                        ReferenceTextures = List.Empty<VertexTextureFactor>(),
                        TargetTextures = List.Empty<VertexTextureFactor>(),
                        Continuation = source
                    };
                }
            }

            /// <summary>
            /// Attempts to match the entities described by <paramref name="vertCont"/>, returning <see cref="true"/>
            /// iff a successful match was found. The <see cref="_Matcher"/> will be updated to reflect the successful
            /// match.
            /// </summary>
            private bool _tryMatch(ref _VertexContinuation vertCont)
            {
            vertMatch:
                // Match remaining vertex textures
                if (!vertCont.ReferenceTextures.IsEmpty)
                {
                    // Load the next texture to be matched
                    var refTexture = vertCont.ReferenceTextures[0];
                    vertCont.ReferenceTextures = vertCont.ReferenceTextures.SliceSuffix(1);
                    ref _TextureInfo refTextureInfo = ref _textureInfos[refTexture.Index];
                    _TextureInfo initTextureInfo = refTextureInfo;

                    // Only allow selection of target texture from the same overlay group
                    // TODO: Special consideration for duplicate texture indices within an overlay group (not sortable)
                    uint groupLen = 0;
                    while (groupLen < vertCont.TargetTextures.Length)
                    {
                        var targetTexture = vertCont.TargetTextures[groupLen++];
                        if (targetTexture.ShouldOverlay)
                            break;
                    }
                    var targetTextures = vertCont.TargetTextures.SlicePrefix(groupLen);
                    if (refTexture.ShouldOverlay)
                        vertCont.TargetTextures = vertCont.TargetTextures.SliceSuffix(groupLen);

                    // Search for a candidate matching texture on the target
                    Matrix3x3 initCov = _cov;
                    Rotation3 initApproxRot = _approxRot;
                    bool hasCandidate = false;
                    for (uint i = 0; i < targetTextures.Length; i++)
                    {
                        _TextureInfo nTextureInfo;
                        var targetTexture = targetTextures[i];

                        // Match texture alpha (approximate due to low alpha precision)
                        if (Scalar.Abs(refTexture.Alpha - targetTexture.Alpha) > 1 / 128.0)
                            continue;

                        // Match normal
                        Scalar normalWeight = 0.001;
                        Vector3 refNormal = refTexture.Tbn * new Vector3(0, 0, normalWeight);
                        Vector3 targetNormal = targetTexture.Tbn * new Vector3(0, 0, normalWeight);
                        Matrix3x3 nCov = initCov + Matrix.Outer(refNormal, targetNormal);
                        Rotation3 nApproxRot = initApproxRot;
                        if (!Vector3.AreLikelyEqual(nApproxRot * refNormal, targetNormal))
                        {
                            Rotation.Wahba(nCov, out nApproxRot, out _);
                            if (!Vector3.AreLikelyEqual(nApproxRot * refNormal, targetNormal))
                                continue;
                        }

                        // TODO: Match tangent/bitangent

                        // Match texture info
                        if (initTextureInfo._index != uint.MaxValue &&
                            initTextureInfo._index != targetTexture.Index)
                            continue;
                        nTextureInfo._index = targetTexture.Index;

                        // Match texture coordinates, updating the transform used for matching
                        Vector2 baseTieCoord = targetTexture.Coord - _textureOffsets[targetTexture.Index];
                        nTextureInfo._cov = initTextureInfo._cov + Matrix.Outer(refTexture.Coord, baseTieCoord);
                        nTextureInfo._refSqrLen = initTextureInfo._refSqrLen + refTexture.Coord.SqrLength;
                        nTextureInfo._subjSqrLen = initTextureInfo._subjSqrLen + baseTieCoord.SqrLength;
                        Similarity2 nTrans = new Similarity2(
                            nTextureInfo._scaling,
                            nTextureInfo._rot, false,
                            _textureOffsets[targetTexture.Index]);
                        if (!Vector2.AreLikelyEqual(nTrans * refTexture.Coord, targetTexture.Coord))
                            continue;

                        // If we already have a candidate, check it first
                        if (hasCandidate)
                        {
                            var point = Mark();
                            _VertexContinuation candVertCont = vertCont;
                            if (_tryMatch(ref candVertCont))
                                return true;
                            Reset(point);
                        }

                        // Update information for new candidate
                        _cov = nCov;
                        _approxRot = nApproxRot;
                        refTextureInfo = nTextureInfo;
                        hasCandidate = true;
                    }

                    // Continue matching on last candidate
                    if (hasCandidate)
                    {
                        _textureHistory.Append((refTexture.Index, initTextureInfo));
                        goto vertMatch;
                    }
                    return false;
                }

            primVertMatch:
                // Match remaining primitive vertices
                ref _PrimitiveContinuation primCont = ref vertCont.Continuation;
                uint baseVertIndex = primCont.BaseVertexIndex;
                if (primCont.NextVertexIndex < primCont.ReferenceVertices.Length)
                {
                    // Load the next vertex to be matched
                    var refVert = primCont.ReferenceVertices[primCont.NextVertexIndex++];

                    // Search for a candidate matching vertex on the target
                    Matrix3x3 initCov = _cov;
                    Motion3 initApproxTrans = _approxTrans;
                    uint candidateIndex = uint.MaxValue;
                    for (uint i = 0; i < primCont.TargetVertices.Length; i++)
                    {
                        var targetVert = primCont.TargetVertices[i];

                        // Match positions
                        Matrix3x3 nCov = initCov + Matrix.Outer(refVert.Position, targetVert.Position - _offset);
                        Rotation3 nApproxRot = initApproxTrans.Linear;
                        if (!Vector3.AreLikelyEqual(initApproxTrans * refVert.Position, targetVert.Position))
                        {
                            Rotation.Wahba(nCov, out nApproxRot, out _);
                            Motion3 nTrans = new Motion3(nApproxRot, _offset);
                            if (!Vector3.AreLikelyEqual(nTrans * refVert.Position, targetVert.Position))
                                continue;
                        }

                        // If we already have a candidate, check it first
                        if (candidateIndex < uint.MaxValue)
                        {
                            var point = Mark();
                            _verts.Append(candidateIndex);
                            _VertexContinuation candVertCont = vertCont;
                            if (_tryMatch(ref candVertCont))
                                return true;
                            Reset(point);
                        }

                        // Update information for new candidate
                        _cov = nCov;
                        _approxRot = nApproxRot;
                        vertCont.ReferenceTextures = refVert.Textures;
                        vertCont.TargetTextures = targetVert.Textures;
                        candidateIndex = i;
                    }

                    // Match vertex
                    if (candidateIndex < uint.MaxValue)
                    {
                        _verts.Append(baseVertIndex + candidateIndex);
                        goto vertMatch;
                    }
                    else
                    {
                        return false;
                    }
                }

            primSegMatch:
                // Match remaining primitive segments
                uint baseSegIndex = primCont.BaseSegmentIndex;
                if (primCont.NextSegmentIndex < primCont.ReferenceSegments.Length)
                {
                    // Load the next segment to be matched
                    _ReferenceMeshTie._SegmentInfo refSeg = primCont.ReferenceSegments[primCont.NextSegmentIndex++];
                    uint refSegTarget0 = _verts[refSeg.Index_0 + baseVertIndex];
                    uint refSegTarget1 = _verts[refSeg.Index_1 + baseVertIndex];

                    // Search for a matching target segment
                    if (primCont.Continuation.ShouldComplement)
                    {
                        for (uint i = 0; i < primCont.TargetSegments.Length; i++)
                        {
                            _ReferenceMeshTie._SegmentInfo targetSeg = primCont.TargetSegments[i];
                            if (refSegTarget0 == targetSeg.Index_1 + baseVertIndex &&
                                refSegTarget1 == targetSeg.Index_0 + baseVertIndex)
                            {
                                // Record matching segment and continue matching
                                _segs.Append(baseSegIndex + i);
                                goto primSegMatch;
                            }
                        }
                    }
                    else
                    {
                        for (uint i = 0; i < primCont.TargetSegments.Length; i++)
                        {
                            _ReferenceMeshTie._SegmentInfo targetSeg = primCont.TargetSegments[i];
                            if (refSegTarget0 == targetSeg.Index_0 + baseVertIndex &&
                                refSegTarget1 == targetSeg.Index_1 + baseVertIndex)
                            {
                                // Record matching segment and continue matching
                                _segs.Append(baseSegIndex + i);
                                goto primSegMatch;
                            }
                        }
                    }

                    // No matching segment found
                    return false;
                }
                
                // Match remaining primitives
                ref _TieContinuation tieCont = ref primCont.Continuation;
                if (!tieCont.ReferenceBuffer.IsEmpty)
                {
                    // Load primitive header
                    ref ReadOnlySpan<byte> refBuffer = ref tieCont.ReferenceBuffer;
                    ref ReadOnlySpan<byte> targetBuffer = ref tieCont.TargetBuffer;
                    Debug.Assert(refBuffer.Length == targetBuffer.Length);
                    var refPrimHeader = Binary._fastReadAlignedNative<_ReferenceMeshTie._PrimitiveHeader>(ref refBuffer);
                    var tiePrimHeader = Binary._fastReadAlignedNative<_ReferenceMeshTie._PrimitiveHeader>(ref targetBuffer);
                    if (refPrimHeader != tiePrimHeader)
                        return false;

                    // Match entities for primitive
                    MeshVertexType vertType = refPrimHeader.VertexType;
                    uint numVerts = refPrimHeader.NumVertices;
                    uint numSegs = refPrimHeader.NumSegments;
                    primCont.ReferenceVertices = VertexBinary.ReadAlignedNativeMeshVertexList(ref refBuffer, vertType, numVerts);
                    primCont.ReferenceSegments = Binary.ReadAlignedNativeList<_ReferenceMeshTie._SegmentInfo>(ref refBuffer, numSegs);
                    primCont.TargetVertices = VertexBinary.ReadAlignedNativeMeshVertexList(ref targetBuffer, vertType, numVerts);
                    primCont.TargetSegments = Binary.ReadAlignedNativeList<_ReferenceMeshTie._SegmentInfo>(ref targetBuffer, numSegs);
                    primCont.NextVertexIndex = 0;
                    primCont.NextSegmentIndex = 0;
                    tieCont.BaseVertexIndex += numVerts;
                    tieCont.BaseSegmentIndex += numSegs;
                    goto primVertMatch;
                }

                // Matching complete
                return true;
            }

            /// <summary>
            /// Marks the current state of the matcher with a backtracking point. This state can then be restored by calling
            /// <see cref="Reset(Point)"/>. If multiple points exist, they must be reset in reverse order of when they
            /// were created.
            /// </summary>
            public Point Mark()
            {
                return new Point
                {
                    _numVerts = _verts.Length,
                    _numSegs = _segs.Length,
                    _cov = _cov,
                    _approxRot = _approxRot,
                    _textureHistoryLen = _textureHistory.Length
                };
            }

            /// <summary>
            /// Resets the state of the solver to the given point.
            /// </summary>
            public void Reset(Point point)
            {
                // Remove latest vertices
                while (_verts.Length > point._numVerts)
                    _verts.Pop();

                // Remove latest segments
                while (_segs.Length > point._numSegs)
                    _segs.Pop();

                // Restore spatial info
                _cov = point._cov;
                _approxRot = point._approxRot;

                // Restore texture info
                while (_textureHistory.Length > point._textureHistoryLen)
                {
                    (uint index, _TextureInfo info) = _textureHistory.Pop();
                    _textureInfos[index] = info;
                }
            }

            /// <summary>
            /// A backtracking point for a matcher.
            /// </summary>
            public struct Point
            {
                internal uint _numVerts;
                internal uint _numSegs;
                internal Matrix3x3 _cov;
                internal Rotation3 _approxRot;
                internal uint _textureHistoryLen;
            }
        }
    }

    /// <summary>
    /// A particular arrangement of vertices and edges that can be exhibited by a <see cref="Mesh"/>, 
    /// presented in some reference orientation. When placed next to a complementary tie on an adjacent
    /// mesh, the meshes will meld together, eliminating seams and redundant geometry between the ties.
    /// </summary>
    internal sealed class _ReferenceMeshTie
    {
        internal readonly byte[] _data;
        internal _MeshTieComplement _complement;
        internal Bag<_MeshTieSymmetry> _symmetries;
        internal _ReferenceMeshTie(byte[] data, uint numTextures, uint numSegs)
        {
            _data = data;
            NumTextures = numTextures;
            NumSegments = numSegs;

            // TODO: Symmetries

            // Check if the mesh tie is self-complementary
            var textureOffsets = ListBuilder<Vector2>.CreateDefault(numTextures);
            MeshTie._Matcher matcher = MeshTie._Matcher.Create(Vector3.Zero, textureOffsets.Finish());
            if (matcher.TryMatch(this, data, true))
            {
                _complement = new _MeshTieComplement
                {
                    Reference = this,
                    Transform = matcher.Transform.Linear,
                    Textures = matcher.Textures,
                    Segments = matcher.Segments
                };
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// The number of distinct textures referenced by this <see cref="_ReferenceMeshTie"/>.
        /// </summary>
        public uint NumTextures { get; }

        /// <summary>
        /// The number of segments (aka edges) defined for this <see cref="_ReferenceMeshTie"/>.
        /// </summary>
        public uint NumSegments { get; }

        /// <summary>
        /// The complement for this <see cref="_ReferenceMeshTie"/>. This describes the matching tie that can
        /// meld with this tie.
        /// </summary>
        public _MeshTieComplement Complement => _complement;

        /// <summary>
        /// The set of all symmetries exhibited by this <see cref="_ReferenceMeshTie"/>, including the identity symmetry.
        /// </summary>
        public Bag<_MeshTieSymmetry> Symmetries => _symmetries;

        /// <summary>
        /// Provides general information about a primitive within a <see cref="_ReferenceMeshTie"/>.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Size = (int)Size)]
        internal struct _PrimitiveHeader : IEquatable<_PrimitiveHeader>
        {
            /// <summary>
            /// The size of this structure.
            /// </summary>
            public const uint Size = MeshVertexType.Size + 16;

            /// <summary>
            /// The vertex type for this primitive.
            /// </summary>
            public MeshVertexType VertexType;

            /// <summary>
            /// The number of vertices defined for this primitive.
            /// </summary>
            public uint NumVertices;

            /// <summary>
            /// The number of segments for this primitive.
            /// </summary>
            public uint NumSegments;

            public static bool operator ==(_PrimitiveHeader a, _PrimitiveHeader b)
            {
                return a.VertexType == b.VertexType &&
                    a.NumVertices == b.NumVertices &&
                    a.NumSegments == b.NumSegments;
            }

            public static bool operator !=(_PrimitiveHeader a, _PrimitiveHeader b)
            {
                return !(a == b);
            }

            public override bool Equals(object obj)
            {
                if (!(obj is _PrimitiveHeader))
                    return false;
                return this == (_PrimitiveHeader)obj;
            }

            bool IEquatable<_PrimitiveHeader>.Equals(_PrimitiveHeader other)
            {
                return this == other;
            }

            public override int GetHashCode()
            {
                return HashCodeHelper.Combine(
                    VertexType.GetHashCode(),
                    NumVertices.GetHashCode(),
                    NumSegments.GetHashCode());
            }
        }

        /// <summary>
        /// Describes a segment in a <see cref="_ReferenceMeshTie"/>.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Size = (int)Size)]
        internal struct _SegmentInfo
        {
            public _SegmentInfo(uint index_0, uint index_1)
            {
                Index_0 = index_0;
                Index_1 = index_1;
            }

            /// <summary>
            /// The size of this structure.
            /// </summary>
            public const uint Size = 8;

            /// <summary>
            /// The index of the first endpoint vertex of this segment.
            /// </summary>
            public uint Index_0;

            /// <summary>
            /// The index of the second endpoint vertex of this segment.
            /// </summary>
            public uint Index_1;
        }
        
        public static implicit operator MeshTie(_ReferenceMeshTie @ref)
        {
            return new MeshTie
            {
                _ref = @ref,
                _trans = Motion3.Identity,
                _textures = DependentTexture.Identity(@ref.NumTextures),
                _segs = List.Identity(@ref.NumSegments)
            };
        }
    }

    /// <summary>
    /// Describes the complement of a <see cref="_ReferenceMeshTie"/>.
    /// </summary>
    internal struct _MeshTieComplement
    {
        /// <summary>
        /// The reference configuration for the complement.
        /// </summary>
        public _ReferenceMeshTie Reference;

        /// <summary>
        /// The spatial transform from <see cref="Reference"/> to the complement.
        /// </summary>
        public Rotation3 Transform;

        /// <summary>
        /// The textures applied to <see cref="Reference"/>, defined in tems of the textures for the complement.
        /// </summary>
        public List<DependentTexture> Textures;

        /// <summary>
        /// Identifies which segments of the complement correspond to each segment of <see cref="Reference"/>.
        /// </summary>
        public List<uint> Segments;

        public static implicit operator MeshTie(_MeshTieComplement source)
        {
            return new MeshTie
            {
                _ref = source.Reference,
                _trans = source.Transform,
                _textures = source.Textures,
                _segs = source.Segments
            };
        }
    }

    /// <summary>
    /// A transformation which can be applied to a <see cref="_ReferenceMeshTie"/> to yield a functionally-identical
    /// mesh tie.
    /// </summary>
    internal struct _MeshTieSymmetry
    {
        /// <summary>
        /// The spatial transform from the reference configuration to the symmetrical configuration.
        /// </summary>
        public Rotation3 Transform;

        /// <summary>
        /// Describes the textures of the symmetrical configuration in terms of those of the reference
        /// configuration.
        /// </summary>
        public List<DependentTexture> Textures;

        /// <summary>
        /// Identifies which segments of the reference configuration correspond to each segment of
        /// symmetrical configuration.
        /// </summary>
        public List<uint> Segments;
    }
}