﻿using System;

namespace Alunite.Graphics
{
    /// <summary>
    /// Describes a process of transforming a radiance image into an image of <see cref="Paint"/>s. This encapsulates
    /// all post-processing effects needed to implement HDR rendering, such as bloom and tone-mapping.
    /// </summary>
    public struct Tone
    {

    }
}