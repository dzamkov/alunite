﻿using System;
using System.Diagnostics;

using Alunite.Data;
using Alunite.Data.Geometry;
using Alunite.Data.Probability;
using Alunite.Diagnostics;
using Alunite.Resources;

namespace Alunite.Graphics
{
    /// <summary>
    /// A visual material that can be applied over a parameteric two-dimensional surface.
    /// </summary>
    public struct Material
    {
        internal _ReferenceMaterial _ref;
        internal Rotation2 _trans;
        internal Material(_ReferenceMaterial @ref, Rotation2 trans)
        {
            _ref = @ref;
            _trans = trans;
        }

        /// <summary>
        /// The <see cref="MaterialTexture2"/> source for this material.
        /// </summary>
        public MaterialTexture2 Texture => _ref.Texture;

        /// <summary>
        /// The transform from material <em>coordinates</em> in this <see cref="Material"/> to internal
        /// texture coordinates on <see cref="Texture"/>.
        /// </summary>
        public Affine2 Localizer => _ref.Localizer * _trans;

        /// <summary>
        /// The type of this material.
        /// </summary>
        public MaterialType Type => _ref.Type;

        /// <summary>
        /// The most restrictive <see cref="MaterialKind"/> that describes this material.
        /// </summary>
        public MaterialKind Kind => _ref.Kind;
    }

    /// <summary>
    /// A <see cref="Material"/> whose coordinate type is <see cref="MaterialType.Square"/>.
    /// </summary>
    public struct SquareMaterial
    {
        _ReferenceMaterial _ref;
        internal SquareMaterial(_ReferenceMaterial @ref)
        {
            Debug.Assert(@ref.Type == MaterialType.Square);
            _ref = @ref;
        }

        /// <summary>
        /// The <see cref="MaterialTexture2"/> source for this material.
        /// </summary>
        public MaterialTexture2 Texture => _ref.Texture;

        /// <summary>
        /// The transform from material <em>coordinates</em> in this <see cref="Material"/> to internal
        /// texture coordinates on <see cref="Texture"/>.
        /// </summary>
        public Affine2 Localizer => _ref.Localizer;

        /// <summary>
        /// The most restrictive <see cref="MaterialKind"/> that describes this material.
        /// </summary>
        public MaterialKind Kind => _ref.Kind;

        public static explicit operator SquareMaterial(Material source)
        {
            if (source._ref.Type != MaterialType.Square)
                throw new InvalidCastException();
            return new SquareMaterial(source._ref);
        }

        public static implicit operator Material(SquareMaterial source)
        {
            return new Material(source._ref, default);
        }
    }

    /// <summary>
    /// A <see cref="Material"/> whose coordinate type is <see cref="MaterialType.Band"/>.
    /// </summary>
    public struct BandMaterial
    {
        _ReferenceMaterial _ref;
        internal BandMaterial(_ReferenceMaterial @ref)
        {
            Debug.Assert(@ref.Type == MaterialType.Band);
            _ref = @ref;
        }

        /// <summary>
        /// The <see cref="MaterialTexture2"/> source for this material.
        /// </summary>
        public MaterialTexture2 Texture => _ref.Texture;

        /// <summary>
        /// The transform from material <em>coordinates</em> in this <see cref="Material"/> to internal
        /// texture coordinates on <see cref="Texture"/>.
        /// </summary>
        public Affine2 Localizer => _ref.Localizer;

        /// <summary>
        /// The most restrictive <see cref="MaterialKind"/> that describes this material.
        /// </summary>
        public MaterialKind Kind => _ref.Kind;

        public static explicit operator BandMaterial(Material source)
        {
            if (source._ref.Type != MaterialType.Band)
                throw new InvalidCastException();
            return new BandMaterial(source._ref);
        }

        public static implicit operator Material(BandMaterial source)
        {
            return new Material(source._ref, default);
        }
    }

    /// <summary>
    /// A <see cref="Material"/> whose coordinate type is <see cref="MaterialType.Plane"/>.
    /// </summary>
    public struct PlaneMaterial
    {
        internal _ReferenceMaterial _ref;
        internal Rotation2 _trans;
        internal PlaneMaterial(_ReferenceMaterial @ref, Rotation2 trans)
        {
            Debug.Assert(@ref.Type == MaterialType.Plane);
            _ref = @ref;
            _trans = trans;
        }

        /// <summary>
        /// Constructs a <see cref="PlaneMaterial"/> directly from the given <see cref="MaterialTexture2"/>
        /// </summary>
        /// <param name="size">The size of <see cref="MaterialTexture2"/> in spatial units.</param>
        public static PlaneMaterial Build(MaterialTexture2 texture, Vector2 size, bool isIsotropic)
        {
            if (!texture.ShouldWrap_X || !texture.ShouldWrap_Y)
                throw new ArgumentException("Texture must be wrap on both axes", nameof(texture));
            if (!(size.X > 0) || !(size.Y > 0))
                throw new ArgumentException("Size must be positive", nameof(size));
            Vector2 scale = new Vector2(1 / size.X, 1 / size.Y);
            MaterialKind kind = MaterialKind.Plane;
            if (isIsotropic) kind &= MaterialKind.Isotropic;
            if (texture.IsOpaque) kind &= MaterialKind.Opaque;
            return new PlaneMaterial(
                new _ReferenceMaterial(texture, Vector2.Zero, scale, Rotation2.Identity, kind),
                Rotation2.Identity);
        }
        
        /// <summary>
        /// The <see cref="MaterialTexture2"/> source for this material.
        /// </summary>
        public MaterialTexture2 Texture => _ref.Texture;

        /// <summary>
        /// The transform from material <em>coordinates</em> in this <see cref="Material"/> to internal
        /// texture coordinates on <see cref="Texture"/>.
        /// </summary>
        public Affine2 Localizer => _ref.Localizer * _trans;

        /// <summary>
        /// The most restrictive <see cref="MaterialKind"/> that describes this material.
        /// </summary>
        public MaterialKind Kind => _ref.Kind;

        /// <summary>
        /// Applies a rotation to this material.
        /// </summary>
        public static PlaneMaterial operator *(Rotation2 trans, PlaneMaterial source)
        {
            // Need to invert the transform since it is applied to the material itself, instead of the
            // coordinates using it.
            return new PlaneMaterial(source._ref, trans.Inverse * source._trans);
        }

        public static explicit operator PlaneMaterial(Material source)
        {
            if (source._ref.Type != MaterialType.Plane)
                throw new InvalidCastException();
            return new PlaneMaterial(source._ref, source._trans);
        }

        public static implicit operator Material(PlaneMaterial source)
        {
            return new Material(source._ref, source._trans);
        }
    }

    /// <summary>
    /// A <see cref="Material"/> in its "reference" orientation, i.e. with no transform applied.
    /// </summary>
    internal sealed class _ReferenceMaterial
    {
        internal Vector2 _localizerOffset;
        internal Vector2 _localizerScale;
        internal Rotation2 _localizerRotation;
        internal _ReferenceMaterial(
            MaterialTexture2 texture,
            Vector2 localizerOffset,
            Vector2 localizerScale,
            Rotation2 localizerRotation,
            MaterialKind kind)
        {
            Texture = texture;
            _localizerOffset = localizerOffset;
            _localizerScale = localizerScale;
            _localizerRotation = localizerRotation;
            Kind = kind;
        }

        /// <summary>
        /// The type of this material.
        /// </summary>
        public MaterialType Type => Kind.Type;
        
        /// <summary>
        /// The <see cref="MaterialTexture2"/> source for this material.
        /// </summary>
        public MaterialTexture2 Texture { get; }

        /// <summary>
        /// The transform from material <em>coordinates</em> in this <see cref="_ReferenceMaterial"/> to internal
        /// texture coordinates on <see cref="Texture"/>.
        /// </summary>
        public Affine2 Localizer => 
            Affine2.Translate(_localizerOffset)
            * Affine2.Scale(_localizerScale)
            * _localizerRotation;

        /// <summary>
        /// If <see cref="true"/>, indicates that the material can be freely rotated before being applied. This requires
        /// <see cref="Type"/> to be <see cref="MaterialType.Plane"/>.
        /// </summary>
        public bool IsIstropic => Kind.RequiresIsotropic;
        
        /// <summary>
        /// The most restrictive <see cref="MaterialKind"/> that describes this material.
        /// </summary>
        public MaterialKind Kind { get; }

        public static implicit operator Material(_ReferenceMaterial source)
        {
            return new Material(source, Rotation2.Identity);
        }

        public static implicit operator MaterialForm(_ReferenceMaterial source)
        {
            return (Material)source;
        }
    }

    /// <summary>
    /// Extrapolates a set of <see cref="Image2{T}"/>s of BRDF parameters to a continuous two-dimensional space.
    /// </summary>
    public sealed class MaterialTexture2
    {
        public MaterialTexture2(
            bool shouldWrap_x,
            bool shouldWrap_y,
            bool isOpaque,
            BigImage2<Paint> albedo,
            BigImage2<Vector3> normal,
            BigImage2<Scalar> metalness,
            BigImage2<Scalar> roughness)
        {
            ShouldWrap_X = shouldWrap_x;
            ShouldWrap_Y = shouldWrap_y;
            IsOpaque = isOpaque;
            AlbedoImage = albedo;
            NormalImage = normal;
            MetalnessImage = metalness;
            RoughnessImage = roughness;
        }

        /// <summary>
        /// If true, indicates that the texture should wrap along the X axis for values outside the [0, 1] range.
        /// Otherwise, the texture is only defined in the [0, 1] range.
        /// </summary>
        public bool ShouldWrap_X { get; }

        /// <summary>
        /// If true, indicates that the texture should wrap along the Y axis for values outside the [0, 1] range.
        /// Otherwise, the texture is only defined in the [0, 1] range.
        /// </summary>
        public bool ShouldWrap_Y { get; }

        /// <summary>
        /// If true, indicates that <see cref="AlbedoImage"/> has an alpha of one everywhere.
        /// </summary>
        public bool IsOpaque { get; } 

        /// <summary>
        /// The image providing albedo and alpha information for this material texture.
        /// </summary>
        public BigImage2<Paint> AlbedoImage { get; }

        /// <summary>
        /// The image providing normal information for this material texture, using the convention where +X is
        /// towards +U, +Y is towards +V, and +Z is away from the material.
        /// </summary>
        public BigImage2<Vector3> NormalImage { get; }

        /// <summary>
        /// The image providing metalness information for this material texture. A value of 1 means the material
        /// is a metal. A value of 0 means the material is a dielectric. Values in between are for blending between
        /// metals and dielectrics such as dirty metallic surfaces.
        /// </summary>
        public BigImage2<Scalar> MetalnessImage { get; }

        /// <summary>
        /// The image providing roughness information for this material texture. A value of 1 means the material is
        /// completely rough. A value of 0 means the material is completely smooth. 
        /// </summary>
        public BigImage2<Scalar> RoughnessImage { get; }
    }

    /// <summary>
    /// Identifies a mutually exclusive category of <see cref="Material"/>s based on what their U and V coordinates
    /// represent.
    /// </summary>
    public enum MaterialType : byte
    {
        /// <summary>
        /// The material contains heterogeneous contents with U and V coordinates bounded in the [0, 1] range.
        /// There is no correspondence between lengths in texture coordinates and lengths in spatial coordinates.
        /// </summary>
        Square,

        /// <summary>
        /// The material contains heterogenous contents along the U coordinate and homogenous contents along
        /// the V coordinate. The U coordinate is bounded in the [0, 1] range and there is no correspondence
        /// between lengths along U and lengths in spatial coordinates. The V coordinate, on the other hand,
        /// is unbounded and lengths along V directly correspond to lengths in space.
        /// </summary>
        Band,

        /// <summary>
        /// The material contains homogenous (but not necessarily isotropic) contents along both the U and V
        /// coordinate. Coordinates are unbounded and lengths in texture coordinates directly correspond to
        /// lengths in space.
        /// </summary>
        Plane
    }

    /// <summary>
    /// Identifies a general category of <see cref="Material"/>s, used to describe materials or
    /// restrict material parameters. This is a finer classification than <see cref="MaterialType"/>.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct MaterialKind : IPrintable
    {
        private uint _flags;
        private MaterialKind(uint flags)
        {
            _flags = flags;
        }

        /// <summary>
        /// The <see cref="MaterialKind"/> which describes all <see cref="Material"/>s. When used as a parameter
        /// restriction, this indicates that the parameter will not be used, since there is no way to use a
        /// material without restricting its <see cref="MaterialType"/>.
        /// </summary>
        public static MaterialKind Any => new MaterialKind(0b11111);

        /// <summary>
        /// The <see cref="MaterialKind"/> which describes no material. This usually signals an error condition.
        /// </summary>
        public static MaterialKind None => new MaterialKind(0);

        /// <summary>
        /// The <see cref="MaterialKind"/> which describes all <see cref="SquareMaterial"/>s.
        /// </summary>
        public static MaterialKind Square => new MaterialKind(0b11001);

        /// <summary>
        /// The <see cref="MaterialKind"/> which describes all <see cref="BandMaterial"/>s.
        /// </summary>
        public static MaterialKind Band => new MaterialKind(0b11010);
        
        /// <summary>
        /// The <see cref="MaterialKind"/> which describes all <see cref="PlaneMaterial"/>s.
        /// </summary>
        public static MaterialKind Plane => new MaterialKind(0b11100);

        /// <summary>
        /// The <see cref="MaterialKind"/> which describes <see cref="PlaneMaterial"/>s that are isotropic. That is,
        /// materials that may be freely rotated before being applied.
        /// </summary>
        public static MaterialKind Isotropic => new MaterialKind(0b10100);

        /// <summary>
        /// The <see cref="MaterialKind"/> which describes <see cref="Material"/>s that are opaque. That is, they have
        /// an alpha of one at all coordinates.
        /// </summary>
        public static MaterialKind Opaque => new MaterialKind(0b01111);

        /// <summary>
        /// Indicates whether this is <see cref="None"/>.
        /// </summary>
        public bool IsNone => _flags == 0;

        /// <summary>
        /// The coordinate type of the material, only applicable if the <see cref="MaterialKind"/> is restricted to
        /// exactly one coordinate type.
        /// </summary>
        public MaterialType Type => (MaterialType)Bitwise.FindFirstOne(_flags);

        /// <summary>
        /// If <see cref="true"/>, indicates that the material must be of type <see cref="MaterialType.Square"/>.
        /// </summary>
        public bool RequiresSquare => (_flags & 0b00110) == 0;

        /// <summary>
        /// If <see cref="true"/>, indicates that the material must be of type <see cref="MaterialType.Band"/>.
        /// </summary>
        public bool RequiresBand => (_flags & 0b00101) == 0;

        /// <summary>
        /// If <see cref="true"/>, indicates that the material must be of type <see cref="MaterialType.Plane"/>.
        /// </summary>
        public bool RequiresPlane => (_flags & 0b00011) == 0;

        /// <summary>
        /// If <see cref="true"/>, indicates that the material may be freely rotated before being applied.
        /// </summary>
        public bool RequiresIsotropic => (_flags & 0b01000) == 0;

        /// <summary>
        /// If <see cref="true"/>, indicates that the material has an alpha of one at all coordinates.
        /// </summary>
        public bool RequiresOpaque => (_flags & 0b10000) == 0;

        /// <summary>
        /// Gets the intersection of the categories <paramref name="a"/> and <paramref name="b"/>, which contains all
        /// materials that are described by both.
        /// </summary>
        public static MaterialKind operator &(MaterialKind a, MaterialKind b)
        {
            return new MaterialKind(a._flags & b._flags);
        }

        /// <summary>
        /// Indicates whether all materials described by <paramref name="a"/> are also described
        /// by <paramref name="b"/>.
        /// </summary>
        public static bool operator <=(MaterialKind a, MaterialKind b)
        {
            return (a._flags & b._flags) == a._flags;
        }

        /// <summary>
        /// Indicates whether all materials described by <paramref name="b"/> are also described
        /// by <paramref name="a"/>.
        /// </summary>
        public static bool operator >=(MaterialKind a, MaterialKind b)
        {
            return b <= a;
        }

        public override string ToString()
        {
            return this.ToShortString();
        }

        void IPrintable.Print(IPrinter printer, Precedence prec)
        {
            // TODO: Consider precedence
            if (IsNone)
            {
                printer.Write("None");
            }
            else
            {
                bool first = true;
                if (RequiresSquare)
                {
                    first = false;
                    printer.Write("Square");
                }
                else if (RequiresBand)
                {
                    first = false;
                    printer.Write("Band");
                }
                else if (RequiresPlane)
                {
                    first = false;
                    printer.Write("Plane");
                }
                if (RequiresIsotropic)
                {
                    if (first)
                        first = false;
                    else
                        printer.Write(" & ");
                    printer.Write("Isotropic");
                }
                if (RequiresOpaque)
                {
                    if (first)
                        first = false;
                    else
                        printer.Write(" & ");
                    printer.Write("Opaque");
                }
                if (first)
                    printer.Write("Any");
            }
        }
    }
    
    /// <summary>
    /// A <see cref="Material"/> parameterized by a set of variables.
    /// </summary>
    public struct MaterialForm
    {
        internal _ReferenceMaterial _ref;
        internal uint _paramIndex;
        internal Rotation2 _trans;
        
        /// <summary>
        /// Constructs a <see cref="MaterialForm"/> representing the material parameter with the given index.
        /// </summary>
        public static MaterialForm Parameter(uint index)
        {
            return new MaterialForm
            {
                _paramIndex = index,
                _trans = Rotation2.Identity
            };
        }

        /// <summary>
        /// Determines whether this <see cref="MaterialForm"/> is a constant <see cref="Material"/>, and if so,
        /// gets that material.
        /// </summary>
        public bool Is(out Material material)
        {
            material = new Material(_ref, _trans);
            return !(_ref is null);
        }

        /// <summary>
        /// Determines the most restrictive <see cref="MaterialKind"/> that contains all possible
        /// values of this <see cref="MaterialForm"/>.
        /// </summary>
        /// <param name="matParamKinds">The <see cref="MaterialKind"/>s of the material parameters referenced by
        /// this <see cref="MaterialForm"/>.</param>
        public MaterialKind Kind(SpanList<MaterialKind> matParamKinds)
        {
            if (!(_ref is null))
                return _ref.Kind;
            return matParamKinds[_paramIndex];
        }
        
        public static implicit operator MaterialForm(Material source)
        {
            return new MaterialForm
            {
                _ref = source._ref,
                _trans = source._trans
            };
        }
    }

    /// <summary>
    /// Describes a <see cref="MaterialForm"/> as it is applied to a <see cref="Mesh"/>.
    /// </summary>
    public struct MaterialUsage
    {
        private MaterialUsage(MaterialForm matForm, MaterialKind kind, TextureLaxity laxity)
        {
            Material = matForm;
            Kind = kind;
            Laxity = laxity;
        }

        /// <summary>
        /// The material to be applied.
        /// </summary>
        public MaterialForm Material { get; }

        /// <summary>
        /// The required <see cref="MaterialKind"/> for <see cref="Material"/>. This is used to restrict
        /// the parameter values.
        /// </summary>
        public MaterialKind Kind { get; }

        /// <summary>
        /// The set of allowable transforms that can be freely applied to the material's coordinates
        /// within the mesh.
        /// </summary>
        public TextureLaxity Laxity { get; }

        /// <summary>
        /// Constructs a <see cref="MaterialUsage"/> which instantiates a particular material parameter.
        /// </summary>
        /// <param name="kind">The required <see cref="MaterialKind"/> for the material.</param>
        public static MaterialUsage Parameter(uint paramIndex, MaterialKind kind, TextureLaxity laxity)
        {
            return new MaterialUsage(MaterialForm.Parameter(paramIndex), kind, laxity);
        }

        /// <summary>
        /// Constructs a <see cref="MaterialUsage"/> based on the given parameters.
        /// </summary>
        public static MaterialUsage Build(Material mat, TextureLaxity laxity)
        {
            return new MaterialUsage(mat, mat._ref.Kind, laxity);
        }
    }
}