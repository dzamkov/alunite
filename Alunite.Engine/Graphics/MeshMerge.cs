﻿using System;
using System.Diagnostics;

using Alunite.Data;
using Alunite.Data.Serialization;
using Alunite.Data.Geometry;

namespace Alunite.Graphics
{
    partial class Mesh
    {
        /// <summary>
        /// Merges two <see cref="Mesh"/>s, substituting parameters in the process.
        /// </summary>
        /// <param name="solver">The <see cref="MeshSolver"/> used to specify constraints for the resulting
        /// mesh parameters. This is used to eliminate seams where the two meshes meet.</param>
        public static Mesh MergeApply(
            MeshSolver solver,
            Mesh a, SpanList<DependentTexture> aTextures,
            Mesh b, SpanList<DependentTexture> bTextures)
        {
            // Create structures to track common segments between meshes
            uint aNumHotTris = a._numHotTris;
            uint bNumHotTris = b._numHotTris;
            uint numHotTris = aNumHotTris + bNumHotTris;
            var initLinkInfo = _EndpointLinkInfo.UnlinkedCold;
            var endpointLinkInfos = ListBuilder<_EndpointLinkInfo>.CreateReplicate(numHotTris * 3, initLinkInfo);

            // Match ties across meshes
            BoolListBuilder aIsTieMatched = BoolListBuilder.CreateReplicateFalse(a._ties.Length);
            BoolListBuilder bIsTieMatched = BoolListBuilder.CreateReplicateFalse(b._ties.Length);
            for (uint i = 0; i < b._ties.Length; i++)
            {
                _Tie bTie = b._ties[i];
                _ReferenceMeshTie bComplRef = bTie.Reference.Complement.Reference;
                Motion3 bComplTrans = bTie.Transform * bTie.Reference.Complement.Transform;
                for (uint j = 0; j < a._ties.Length; j++)
                {
                    _Tie aTie = a._ties[j];
                    if (aTie.Reference == bComplRef && Motion3.AreLikelyEqual(aTie.Transform, bComplTrans))
                    {
                        Debug.Assert(!aIsTieMatched[j]);

                        // Equate matching textures to remove seams
                        for (uint k = 0; k < bComplRef.NumTextures; k++)
                        {
                            DependentTexture aTieTexture = aTie.Textures[k];
                            DependentTexture bTieTexture = bTie.Reference.Complement.Textures[k].Apply(bTie.Textures);
                            aTieTexture = aTieTexture.Apply(aTextures);
                            bTieTexture = bTieTexture.Apply(bTextures);
                            solver.RequireEqual(aTieTexture, bTieTexture);
                        }

                        // Record common segments
                        for (uint k = 0; k < bComplRef.NumSegments; k++)
                        {
                            _Endpoint bEndpoint = aNumHotTris + bTie.Segments[bTie.Reference.Complement.Segments[k]];
                            _Endpoint aEndpoint = aTie.Segments[k];
                            ref _EndpointLinkInfo aEndpointLinkInfo = ref endpointLinkInfos[aEndpoint.Index];
                            ref _EndpointLinkInfo bEndpointLinkInfo = ref endpointLinkInfos[bEndpoint.Index];
                            Debug.Assert(aEndpointLinkInfo.IsUnlinkedCold);
                            Debug.Assert(bEndpointLinkInfo.IsUnlinkedCold);
                            aEndpointLinkInfo = _EndpointLinkInfo.Coincident(bEndpoint.Next);
                            bEndpointLinkInfo = _EndpointLinkInfo.Coincident(aEndpoint.Next);
                        }

                        // Mark matched ties
                        aIsTieMatched[j] = true;
                        bIsTieMatched[i] = true;
                        goto foundMatch;
                    }
                }
            foundMatch:;
            }

            // Mark segments that will remain "hot" in the final mesh
            uint nNumTies = 0;
            for (uint i = 0; i < aIsTieMatched.Length; i++)
            {
                if (!aIsTieMatched[i])
                {
                    _Tie aTie = a._ties[i];
                    for (uint j = 0; j < aTie.Segments.Length; j++)
                        endpointLinkInfos[aTie.Segments[j].Index] = _EndpointLinkInfo.UnlinkedHot;
                    nNumTies++;
                }
            }
            for (uint i = 0; i < bIsTieMatched.Length; i++)
            {
                if (!bIsTieMatched[i])
                {
                    _Tie bTie = b._ties[i];
                    for (uint j = 0; j < bTie.Segments.Length; j++)
                        endpointLinkInfos[(aNumHotTris + bTie.Segments[j]).Index] = _EndpointLinkInfo.UnlinkedHot;
                    nNumTies++;
                }
            }

            // Calculate data size for final mesh
            uint maxDataSize = _mergeMaxDataSize(a._data, b._data);

            // Merge data
            byte[] nData = new byte[maxDataSize];
            uint nHead = 0;
            uint nTextureBound = 0;
            uint nHotTriIndex = 0;
            uint aHotTriIndex = 0;
            uint bHotTriIndex = aNumHotTris;
            _mergeApply(nData, ref nHead, ref nTextureBound, ref nHotTriIndex,
                endpointLinkInfos, ref aHotTriIndex, ref bHotTriIndex,
                a._data, aTextures,
                b._data, bTextures);
            Debug.Assert(aHotTriIndex == aNumHotTris);
            Debug.Assert(bHotTriIndex == numHotTris);

            // Trim data array
            if (nHead < maxDataSize)
            {
                byte[] nnData = new byte[nHead];
                Binary.Copy(nData.AsSpan(0, (int)nHead), nnData);
                nData = nnData;
            }

            // Build ties
            _Tie[] nTieItems = new _Tie[nNumTies];
            nNumTies = 0;
            for (uint i = 0; i < aIsTieMatched.Length; i++)
            {
                if (!aIsTieMatched[i])
                {
                    _Tie aTie = a._ties[i];
                    DependentTexture[] nTextureItems = new DependentTexture[aTie.Textures.Length];
                    for (uint j = 0; j < (uint)nTextureItems.Length; j++)
                        nTextureItems[j] = aTie.Textures[j].Apply(aTextures);
                    _Endpoint[] nSegItems = new _Endpoint[aTie.Segments.Length];
                    for (uint j = 0; j < (uint)nSegItems.Length; j++)
                    {
                        var linkInfo = endpointLinkInfos[aTie.Segments[j].Index];
                        Debug.Assert(linkInfo.Type >= _EndpointLinkType.FinalClean);
                        nSegItems[j] = linkInfo.Other;
                    }
                    nTieItems[nNumTies++] = new _Tie
                    {
                        Reference = aTie.Reference,
                        Transform = aTie.Transform,
                        Textures = new List<DependentTexture>(nTextureItems),
                        Segments = new List<_Endpoint>(nSegItems)
                    };
                }
            }
            for (uint i = 0; i < bIsTieMatched.Length; i++)
            {
                if (!bIsTieMatched[i])
                {
                    _Tie bTie = b._ties[i];
                    DependentTexture[] nTextureItems = new DependentTexture[bTie.Textures.Length];
                    for (uint j = 0; j < (uint)nTextureItems.Length; j++)
                        nTextureItems[j] = bTie.Textures[j].Apply(bTextures);
                    _Endpoint[] nSegItems = new _Endpoint[bTie.Segments.Length];
                    for (uint j = 0; j < (uint)nSegItems.Length; j++)
                    {
                        var linkInfo = endpointLinkInfos[(aNumHotTris + bTie.Segments[j]).Index];
                        Debug.Assert(linkInfo.Type >= _EndpointLinkType.FinalClean);
                        nSegItems[j] = linkInfo.Other;
                    }
                    nTieItems[nNumTies++] = new _Tie
                    {
                        Reference = bTie.Reference,
                        Transform = bTie.Transform,
                        Textures = new List<DependentTexture>(nTextureItems),
                        Segments = new List<_Endpoint>(nSegItems)
                    };
                }
            }
            Debug.Assert(nNumTies == (uint)nTieItems.Length);
            List<_Tie> nTies = new List<_Tie>(nTieItems);

            // Finalize mesh
            return new Mesh(nData, nTies, nTextureBound);
        }

        /// <summary>
        /// Gets an upper bound on the size of <see cref="_data"/> when merging two <see cref="Mesh"/>s.
        /// </summary>
        private static uint _mergeMaxDataSize(byte[] aData, byte[] bData)
        {
            uint bodySize;
            uint aHead = 0;
            uint bHead = 0;
            _PrimitiveHeader aHeader, bHeader;
            uint dataSize = 0;
        haveNeither:
            if (aHead < (uint)aData.Length)
            {
                aHeader = Binary._fastReadAlignedNative<_PrimitiveHeader>(aData, ref aHead);
            haveA:
                if (bHead < (uint)bData.Length)
                {
                    bHeader = Binary._fastReadAlignedNative<_PrimitiveHeader>(bData, ref bHead);
                haveBoth:
                    int compare = MeshVertexType.Compare(aHeader.VertexType, bHeader.VertexType);
                    if (compare < 0)
                    {
                        bodySize = aHeader.BodySize;
                        aHead += bodySize;
                        dataSize += _PrimitiveHeader.Size;
                        dataSize += bodySize;
                        if (aHead < (uint)aData.Length)
                        {
                            aHeader = Binary._fastReadAlignedNative<_PrimitiveHeader>(aData, ref aHead);
                            goto haveBoth;
                        }
                        goto closeB;
                    }
                    else if (compare > 0)
                    {
                        bodySize = bHeader.BodySize;
                        bHead += bodySize;
                        dataSize += _PrimitiveHeader.Size;
                        dataSize += bodySize;
                        goto haveA;
                    }
                    else
                    {
                        Debug.Assert(compare == 0);
                        aHead += aHeader.BodySize;
                        bHead += bHeader.BodySize;
                        dataSize += _PrimitiveHeader.Size;
                        dataSize += new _PrimitiveHeader
                        {
                            VertexType = aHeader.VertexType,
                            NumVertices = aHeader.NumVertices + bHeader.NumVertices,
                            NumHotVertices = aHeader.NumHotVertices + bHeader.NumHotVertices,
                            NumTriangles = aHeader.NumTriangles + bHeader.NumTriangles,
                            NumHotTriangles = aHeader.NumHotTriangles + bHeader.NumHotTriangles
                        }.BodySize;
                        goto haveNeither;
                    }
                }
                else
                {
                    goto closeA;
                }
            }
            else if (bHead < (uint)bData.Length)
            {
                bHeader = Binary._fastReadAlignedNative<_PrimitiveHeader>(bData, ref bHead);
                goto closeB;
            }
            else
            {
                goto done;
            }
        closeA:
            bodySize = aHeader.BodySize;
            aHead += bodySize;
            dataSize += _PrimitiveHeader.Size;
            dataSize += bodySize;
            if (aHead < (uint)aData.Length)
            {
                aHeader = Binary._fastReadAlignedNative<_PrimitiveHeader>(aData, ref aHead);
                goto closeA;
            }
            goto done;
        closeB:
            bodySize = bHeader.BodySize;
            bHead += bodySize;
            dataSize += _PrimitiveHeader.Size;
            dataSize += bodySize;
            if (bHead < (uint)bData.Length)
            {
                bHeader = Binary._fastReadAlignedNative<_PrimitiveHeader>(bData, ref bHead);
                goto closeB;
            }
            goto done;
        done:
            return dataSize;
        }

        /// <summary>
        /// Gets the mesh data for a merged mesh. 
        /// </summary>
        private static void _mergeApply(
            byte[] nData, ref uint nHead, ref uint nTextureBound, ref uint nHotTriIndex,
            ListBuilder<_EndpointLinkInfo> endpointLinkInfos,
            ref uint aHotTriIndex, ref uint bHotTriIndex,
            byte[] aData, SpanList<DependentTexture> aTextures,
            byte[] bData, SpanList<DependentTexture> bTextures)
        {
            uint aHead = 0;
            uint bHead = 0;
            _PrimitiveHeader aHeader, bHeader;
        haveNeither:
            if (aHead < (uint)aData.Length)
            {
                aHeader = Binary._fastReadAlignedNative<_PrimitiveHeader>(aData, ref aHead);
            haveA:
                if (bHead < (uint)bData.Length)
                {
                    bHeader = Binary._fastReadAlignedNative<_PrimitiveHeader>(bData, ref bHead);
                haveBoth:
                    int compare = MeshVertexType.Compare(aHeader.VertexType, bHeader.VertexType);
                    if (compare < 0)
                    {
                        _copyApplyPrim(nData, ref nHead, ref nTextureBound, ref nHotTriIndex,
                            endpointLinkInfos, ref aHotTriIndex,
                            aTextures, aData, ref aHead, aHeader);
                        if (aHead < (uint)aData.Length)
                        {
                            aHeader = Binary._fastReadAlignedNative<_PrimitiveHeader>(aData, ref aHead);
                            goto haveBoth;
                        }
                        goto closeB;
                    }
                    else if (compare > 0)
                    {
                        _copyApplyPrim(nData, ref nHead, ref nTextureBound, ref nHotTriIndex,
                            endpointLinkInfos, ref bHotTriIndex,
                            bTextures, bData, ref bHead, bHeader);
                        goto haveA;
                    }
                    else
                    {
                        Debug.Assert(compare == 0);
                        _mergeApplyPrim(nData, ref nHead, ref nTextureBound, ref nHotTriIndex,
                            endpointLinkInfos, ref aHotTriIndex, ref bHotTriIndex,
                            aTextures, aData, ref aHead, aHeader,
                            bTextures, bData, ref bHead, bHeader);
                        goto haveNeither;
                    }
                }
                else
                {
                    goto closeA;
                }
            }
            else if (bHead < (uint)bData.Length)
            {
                bHeader = Binary._fastReadAlignedNative<_PrimitiveHeader>(bData, ref bHead);
                goto closeB;
            }
            else
            {
                goto done;
            }
        closeA:
            _copyApplyPrim(nData, ref nHead, ref nTextureBound, ref nHotTriIndex,
                endpointLinkInfos, ref aHotTriIndex,
                aTextures, aData, ref aHead, aHeader);
            if (aHead < (uint)aData.Length)
            {
                aHeader = Binary._fastReadAlignedNative<_PrimitiveHeader>(aData, ref aHead);
                goto closeA;
            }
            goto done;
        closeB:
            _copyApplyPrim(nData, ref nHead, ref nTextureBound, ref nHotTriIndex,
                endpointLinkInfos, ref bHotTriIndex,
                bTextures, bData, ref bHead, bHeader);
            if (bHead < (uint)bData.Length)
            {
                bHeader = Binary._fastReadAlignedNative<_PrimitiveHeader>(bData, ref bHead);
                goto closeB;
            }
            goto done;
        done:;
        }

        /// <summary>
        /// Copies data for a primitive while applying a texture substitution.
        /// </summary>
        private static void _copyApplyPrim(
            byte[] nData, ref uint nHead, ref uint nTextureBound, ref uint nHotTriIndex,
            ListBuilder<_EndpointLinkInfo> endpointLinkInfos, ref uint sHotTriIndex,
            SpanList<DependentTexture> sTextures, byte[] sData, ref uint sHead, _PrimitiveHeader sHeader)
        {
            // Copy header
            Binary._fastWriteAlignedNative(nData, ref nHead, in sHeader);

            // Copy vertex data
            for (uint i = 0; i < sHeader.NumVertices; i++)
            {
                var vert = VertexBinary._fastReadAlignedNativeMeshVertex(sData, ref sHead, sHeader.VertexType);
                _writeTransformApplyVert(nData, ref nHead, ref nTextureBound, Motion3.Identity, sTextures, vert);
            }

            // Copy triangle index data
            var indices = Binary.ReadAlignedNativeList<_Endpoint>(sData, ref sHead, sHeader.NumTriangles * 3);
            Binary.WriteAlignedNativeList(nData, ref nHead, indices);

            // Copy triangle segment data
            var segInfos = Binary.ReadAlignedNativeList<_SegmentInfo>(sData, ref sHead, sHeader.NumHotTriangles * 3);
            Binary.WriteAlignedNativeList(nData, ref nHead, segInfos);

            // Update link infos
            for (uint i = 0; i < sHeader.NumHotTriangles; i++)
            {
                for (uint j = 0; j < 3; j++)
                {
                    _Endpoint endpoint = new _Endpoint(i, j);
                    var linkInfo = _EndpointLinkInfo.Final(false, nHotTriIndex + endpoint);
                    endpointLinkInfos[(endpoint + sHotTriIndex).Index] = linkInfo;
                }
            }

            // Advance indices
            nHotTriIndex += sHeader.NumHotTriangles;
            sHotTriIndex += sHeader.NumHotTriangles;
        }

        /// <summary>
        /// Gets the mesh data for the body of a merged primitive.
        /// </summary>
        private static void _mergeApplyPrim(
            byte[] nData, ref uint nHead, ref uint nTextureBound, ref uint nHotTriIndex,
            ListBuilder<_EndpointLinkInfo> endpointLinkInfos, ref uint aHotTriIndex, ref uint bHotTriIndex,
            SpanList<DependentTexture> aTextures, byte[] aData, ref uint aHead, _PrimitiveHeader aHeader,
            SpanList<DependentTexture> bTextures, byte[] bData, ref uint bHead, _PrimitiveHeader bHeader)
        {
            MeshVertexType vertType = aHeader.VertexType;
            uint aNumHotVerts = aHeader.NumHotVertices;
            uint bNumHotVerts = bHeader.NumHotVertices;
            uint aNumColdVerts = aHeader.NumVertices - aNumHotVerts;
            uint bNumColdVerts = bHeader.NumVertices - bNumHotVerts;
            uint aNumHotTris = aHeader.NumHotTriangles;
            uint bNumHotTris = bHeader.NumHotTriangles;
            uint aNumColdTris = aHeader.NumTriangles - aNumHotTris;
            uint bNumColdTris = bHeader.NumTriangles - bNumHotTris;
            uint aNumHotIndices = aNumHotTris * 3;
            uint bNumHotIndices = bNumHotTris * 3;

            // Read source data
            var aVerts = VertexBinary.ReadAlignedNativeMeshVertexList(aData, ref aHead, vertType, aHeader.NumVertices);
            var bVerts = VertexBinary.ReadAlignedNativeMeshVertexList(bData, ref bHead, vertType, bHeader.NumVertices);
            var aIndices = Binary.ReadAlignedNativeList<uint>(aData, ref aHead, aHeader.NumTriangles * 3);
            var bIndices = Binary.ReadAlignedNativeList<uint>(bData, ref bHead, bHeader.NumTriangles * 3);
            var aSegInfos = Binary.ReadAlignedNativeList<_SegmentInfo>(aData, ref aHead, aNumHotIndices);
            var bSegInfos = Binary.ReadAlignedNativeList<_SegmentInfo>(bData, ref bHead, bNumHotIndices);

            // Reserve space for header
            ref _PrimitiveHeader header = ref Binary.AllocateAlignedNative<_PrimitiveHeader>(nData, ref nHead);
            uint bodyHead = nHead;

            // Copy cold vertex data
            uint numColdVerts = aNumColdVerts + bNumColdVerts;
            uint nVertHead = nHead;
            for (uint i = 0; i < aNumColdVerts; i++)
                _writeTransformApplyVert(nData, ref nHead, ref nTextureBound, Motion3.Identity, aTextures, aVerts[i]);
            for (uint i = 0; i < bNumColdVerts; i++)
                _writeTransformApplyVert(nData, ref nHead, ref nTextureBound, Motion3.Identity, bTextures, bVerts[i]);

            // Determine which hot vertices are still used by unmatched tie segments
            uint notUsed = uint.MaxValue - numColdVerts;
            var hotIndexMap = ListBuilder<uint>.CreateDefault(aNumHotVerts + bNumHotVerts);
            for (uint i = 0; i < hotIndexMap.Length; i++)
                hotIndexMap[i] = notUsed;
            for (uint i = 0; i < aHeader.NumHotTriangles; i++)
            {
                for (uint j = 0; j < 3; j++)
                {
                    _Endpoint endpoint = new _Endpoint(i, j);
                    if (endpointLinkInfos[(endpoint + aHotTriIndex).Index].IsUnlinkedHot)
                    {
                        uint index_0 = aIndices[(endpoint + aNumColdTris).Index];
                        uint index_1 = aIndices[(endpoint.Next + aNumColdTris).Index];
                        if (index_0 >= aNumColdVerts)
                        {
                            uint hotIndex_0 = index_0 - aNumColdVerts;
                            hotIndexMap[hotIndex_0] = hotIndex_0;
                        }
                        if (index_1 >= aNumColdVerts)
                        {
                            uint hotIndex_1 = index_1 - aNumColdVerts;
                            hotIndexMap[hotIndex_1] = hotIndex_1;
                        }
                    }
                }
            }
            for (uint i = 0; i < bHeader.NumHotTriangles; i++)
            {
                for (uint j = 0; j < 3; j++)
                {
                    _Endpoint endpoint = new _Endpoint(i, j);
                    if (endpointLinkInfos[(endpoint + bHotTriIndex).Index].IsUnlinkedHot)
                    {
                        uint index_0 = bIndices[(endpoint + bNumColdTris).Index];
                        uint index_1 = bIndices[(endpoint.Next + bNumColdTris).Index];
                        if (index_0 >= bNumColdVerts)
                        {
                            uint hotIndex_0 = aNumHotVerts + index_0 - bNumColdVerts;
                            hotIndexMap[hotIndex_0] = hotIndex_0;
                        }
                        if (index_1 >= bNumColdVerts)
                        {
                            uint hotIndex_1 = aNumHotVerts + index_1 - bNumColdVerts;
                            hotIndexMap[hotIndex_1] = hotIndex_1;
                        }
                    }
                }
            }

            // Merge coincident vertices
            void merge(uint aHotIndex, uint bHotIndex)
            {
                // Normalize identifier for first index
                uint anHotIndex = hotIndexMap[aHotIndex];
                if (anHotIndex < aHotIndex)
                {
                next:
                    uint annHotIndex = hotIndexMap[anHotIndex];
                    if (annHotIndex < anHotIndex)
                    {
                        hotIndexMap[aHotIndex] = annHotIndex;
                        aHotIndex = anHotIndex;
                        anHotIndex = annHotIndex;
                        goto next;
                    }
                    else
                    {
                        aHotIndex = anHotIndex;
                    }
                }

                // Normalize identifier for second index
                uint bnHotIndex = hotIndexMap[bHotIndex];
                if (bnHotIndex < bHotIndex)
                {
                next:
                    uint bnnHotIndex = hotIndexMap[bnHotIndex];
                    if (bnnHotIndex < bnHotIndex)
                    {
                        hotIndexMap[bHotIndex] = bnnHotIndex;
                        bHotIndex = bnHotIndex;
                        bnHotIndex = bnnHotIndex;
                        goto next;
                    }
                    else
                    {
                        bHotIndex = bnHotIndex;
                    }
                }

                // Merge identifiers
                if (aHotIndex < bHotIndex)
                    hotIndexMap[bHotIndex] = aHotIndex;
                else if (bHotIndex < aHotIndex)
                    hotIndexMap[aHotIndex] = bHotIndex;
            }
            for (uint i = 0; i < aNumHotTris; i++)
            {
                for (uint j = 0; j < 3; j++)
                {
                    _Endpoint aEndpoint = new _Endpoint(i, j);
                    if (endpointLinkInfos[(aEndpoint + aHotTriIndex).Index].IsCoincident(out _Endpoint bEndpoint))
                    {
                        Util.Assert(bEndpoint.TriangleIndex >= bHotTriIndex);
                        bEndpoint -= bHotTriIndex;
                        uint aIndex = aIndices[(aEndpoint + aNumColdTris).Index];
                        uint bIndex = bIndices[(bEndpoint + bNumColdTris).Index];
                        Debug.Assert(aIndex >= aNumColdVerts);
                        Debug.Assert(bIndex >= bNumColdVerts);
                        merge(aIndex - aNumColdVerts, aNumHotVerts + bIndex - bNumColdVerts);
                    }
                }
            }
            for (uint i = 0; i < bNumHotTris; i++)
            {
                for (uint j = 0; j < 3; j++)
                {
                    _Endpoint bEndpoint = new _Endpoint(i, j);
                    if (endpointLinkInfos[(bEndpoint + bHotTriIndex).Index].IsCoincident(out _Endpoint aEndpoint))
                    {
                        Util.Assert(aEndpoint.TriangleIndex >= aHotTriIndex);
                        aEndpoint -= aHotTriIndex;
                        uint aIndex = aIndices[(aEndpoint + aNumColdTris).Index];
                        uint bIndex = bIndices[(bEndpoint + bNumColdTris).Index];
                        Debug.Assert(aIndex >= aNumColdVerts);
                        Debug.Assert(bIndex >= bNumColdVerts);
                        merge(aIndex - aNumColdVerts, aNumHotVerts + bIndex - bNumColdVerts);
                    }
                }
            }

            // Write hot vertex data, leaving out unused vertices, and normalize indices
            uint nNumHotVerts = 0;
            for (uint i = 0; i < aNumHotVerts; i++)
            {
                uint hotIndex = i;
                ref uint nHotIndex = ref hotIndexMap[hotIndex];
                if (nHotIndex == hotIndex)
                {
                    // Write vertex data
                    nHotIndex = nNumHotVerts++;
                    _writeTransformApplyVert(nData, ref nHead, ref nTextureBound,
                        Motion3.Identity, aTextures, aVerts[aNumColdVerts + i]);
                }
                else if (nHotIndex < hotIndexMap.Length)
                {
                    nHotIndex = hotIndexMap[nHotIndex];
                }
            }
            for (uint i = 0; i < bNumHotVerts; i++)
            {
                uint hotIndex = aNumHotVerts + i;
                ref uint nHotIndex = ref hotIndexMap[hotIndex];
                if (nHotIndex == hotIndex)
                {
                    // Write vertex data
                    nHotIndex = nNumHotVerts++;
                    _writeTransformApplyVert(nData, ref nHead, ref nTextureBound,
                        Motion3.Identity, bTextures, bVerts[bNumColdVerts + i]);
                }
                else if (nHotIndex < hotIndexMap.Length)
                {
                    nHotIndex = hotIndexMap[nHotIndex];
                }
            }
            for (uint i = 0; i < hotIndexMap.Length; i++)
                hotIndexMap[i] += numColdVerts;
            uint nNumVerts = numColdVerts + nNumHotVerts;

            // Copy cold triangle index data
            uint aNumColdIndices = aNumColdTris * 3;
            uint bNumColdIndices = bNumColdTris * 3;
            for (uint i = 0; i < aNumColdIndices; i++)
                Binary._fastWriteAlignedNativeUInt32(nData, ref nHead, aIndices[i]);
            for (uint i = 0; i < bNumColdIndices; i++)
                Binary._fastWriteAlignedNativeUInt32(nData, ref nHead, aNumColdVerts + bIndices[i]);

            // Allocate space for hot triangle index data 
            // TODO: Correct upper bound for number of triangles?
            uint numHotIndices = aNumHotIndices + bNumHotIndices;
            uint hotIndexHead = nHead;
            Span<uint> nHotIndices = Binary.AllocateAlignedNativeList<uint>(nData, ref nHead, numHotIndices);

            // Allocate space for hot triangle segment data
            var nSegInfos = Binary.AllocateAlignedNativeList<_MergeSegmentInfo>(nData, ref nHead, numHotIndices);

            // Construct merged polygons
            _MergeTriangulator triangulator;
            uint nNumHotTris = 0;
            unsafe
            {
                triangulator = new _MergeTriangulator
                {
                    EndpointLinkInfos = endpointLinkInfos,
                    Vertices = VertexBinary.ReadAlignedNativeMeshVertexList(nData, ref nVertHead, vertType, nNumVerts),
                    Indices = nHotIndices,
                    SegmentInfos = nSegInfos,
                    NextFreeTriangleIndex = &nNumHotTris,
                    A = new _MergeTriangulator._SourceInfo
                    {
                        HotTriangleIndex = aHotTriIndex,
                        ColdVertexIndex = 0,
                        NumColdVertices = aNumColdVerts,
                        NumTriangles = aNumHotTris,
                        HotIndexMap = ((SpanList<uint>)hotIndexMap).SlicePrefix(aNumHotVerts),
                        Indices = aIndices.SliceSuffix(aNumColdIndices),
                        SegmentInfos = aSegInfos
                    },
                    B = new _MergeTriangulator._SourceInfo
                    {
                        HotTriangleIndex = bHotTriIndex,
                        ColdVertexIndex = aNumColdVerts,
                        NumColdVertices = bNumColdVerts,
                        NumTriangles = bNumHotTris,
                        HotIndexMap = ((SpanList<uint>)hotIndexMap).SliceSuffix(aNumHotVerts),
                        Indices = bIndices.SliceSuffix(bNumColdIndices),
                        SegmentInfos = bSegInfos
                    }
                };
            }
            triangulator.AddAll();

            // Merge polygons together
            for (uint i = 0; i < aNumHotTris; i++)
            {
                for (uint j = 0; j < 3; j++)
                {
                    _Endpoint endpoint = new _Endpoint(i, j);
                    if (aSegInfos[endpoint.Index].IsConnected(out _Endpoint otherEndpoint))
                        triangulator._tryConnect(endpoint, otherEndpoint, aHotTriIndex);
                }
            }
            for (uint i = 0; i < bNumHotTris; i++)
            {
                for (uint j = 0; j < 3; j++)
                {
                    _Endpoint endpoint = new _Endpoint(i, j);
                    if (bSegInfos[endpoint.Index].IsConnected(out _Endpoint otherEndpoint))
                        triangulator._tryConnect(endpoint, otherEndpoint, bHotTriIndex);
                }
            }

            // Identify new cold triangles and "demote" them by moving them to the start of the
            // triangle list
            uint nNumTris = aNumColdTris + bNumColdTris + nNumHotTris;
            uint curTriIndex = nNumHotTris;
            uint numDemotedTris = 0;
            while (curTriIndex > numDemotedTris)
            {
                uint curEndpointIndex = --curTriIndex * 3;
                ref uint index_0 = ref nHotIndices[(int)curEndpointIndex + 0];
                ref uint index_1 = ref nHotIndices[(int)curEndpointIndex + 1];
                ref uint index_2 = ref nHotIndices[(int)curEndpointIndex + 2];
                if (index_0 < numColdVerts && index_1 < numColdVerts && index_2 < numColdVerts)
                {
                    _Endpoint endpoint;
                    ref _MergeSegmentInfo segInfo_0 = ref nSegInfos[(int)curEndpointIndex + 0];
                    ref _MergeSegmentInfo segInfo_1 = ref nSegInfos[(int)curEndpointIndex + 1];
                    ref _MergeSegmentInfo segInfo_2 = ref nSegInfos[(int)curEndpointIndex + 2];

                reswap:
                    // Swap cold triangle to the beginning
                    uint demotedTriIndex = numDemotedTris++;
                    if (demotedTriIndex != curTriIndex)
                    {
                        Debug.Assert(demotedTriIndex < curTriIndex);
                        uint demotedEndpointIndex = demotedTriIndex * 3;
                        ref uint demotedIndex_0 = ref nHotIndices[(int)demotedEndpointIndex + 0];
                        ref uint demotedIndex_1 = ref nHotIndices[(int)demotedEndpointIndex + 1];
                        ref uint demotedIndex_2 = ref nHotIndices[(int)demotedEndpointIndex + 2];
                        Util.Swap(ref index_0, ref demotedIndex_0);
                        Util.Swap(ref index_1, ref demotedIndex_1);
                        Util.Swap(ref index_2, ref demotedIndex_2);

                        // Update segment connection info for original triangle
                        if (segInfo_0.IsConnected(out endpoint))
                            nSegInfos[(int)endpoint.Index] = _MergeSegmentInfo.Connected(new _Endpoint(demotedTriIndex, 0));
                        if (segInfo_1.IsConnected(out endpoint))
                            nSegInfos[(int)endpoint.Index] = _MergeSegmentInfo.Connected(new _Endpoint(demotedTriIndex, 1));
                        if (segInfo_2.IsConnected(out endpoint))
                            nSegInfos[(int)endpoint.Index] = _MergeSegmentInfo.Connected(new _Endpoint(demotedTriIndex, 2));
                        segInfo_0 = nSegInfos[(int)demotedEndpointIndex + 0];
                        segInfo_1 = nSegInfos[(int)demotedEndpointIndex + 1];
                        segInfo_2 = nSegInfos[(int)demotedEndpointIndex + 2];

                        // Is the triangle we just swapped also cold?
                        if (index_0 < numColdVerts && index_1 < numColdVerts && index_2 < numColdVerts)
                            goto reswap;

                        // Update segment connection info for swapped triangle
                        if (segInfo_0.IsConnected(out endpoint))
                        {
                            nSegInfos[(int)endpoint.Index] = _MergeSegmentInfo.Connected(new _Endpoint(curTriIndex, 0));
                        }
                        else
                        {
                            Util.Assert(segInfo_0.IsUnconnected(out uint linkIndex));
                            endpointLinkInfos[linkIndex].Other = new _Endpoint(curTriIndex, 0);
                        }
                        if (segInfo_1.IsConnected(out endpoint))
                        {
                            nSegInfos[(int)endpoint.Index] = _MergeSegmentInfo.Connected(new _Endpoint(curTriIndex, 1));
                        }
                        else
                        {
                            Util.Assert(segInfo_1.IsUnconnected(out uint linkIndex));
                            endpointLinkInfos[linkIndex].Other = new _Endpoint(curTriIndex, 1);
                        }
                        if (segInfo_2.IsConnected(out endpoint))
                        {
                            nSegInfos[(int)endpoint.Index] = _MergeSegmentInfo.Connected(new _Endpoint(curTriIndex, 2));
                        }
                        else
                        {
                            Util.Assert(segInfo_2.IsUnconnected(out uint linkIndex));
                            endpointLinkInfos[linkIndex].Other = new _Endpoint(curTriIndex, 2);
                        }
                    }
                    else
                    {
                        // Update segment connection info for final cold triangle
                        if (segInfo_0.IsConnected(out endpoint))
                            nSegInfos[(int)endpoint.Index] = _MergeSegmentInfo.Connected(new _Endpoint(curTriIndex, 0));
                        if (segInfo_1.IsConnected(out endpoint))
                            nSegInfos[(int)endpoint.Index] = _MergeSegmentInfo.Connected(new _Endpoint(curTriIndex, 1));
                        if (segInfo_2.IsConnected(out endpoint))
                            nSegInfos[(int)endpoint.Index] = _MergeSegmentInfo.Connected(new _Endpoint(curTriIndex, 2));
                        break;
                    }
                }
            }

            // Finalize segment connection info
            nHead = hotIndexHead;
            nHead += nNumHotTris * (3 * sizeof(uint));
            nNumHotTris -= numDemotedTris;
            nSegInfos = nSegInfos.Slice((int)(numDemotedTris * 3), (int)(nNumHotTris * 3));
            _SegmentInfo unconnectedSegInfo = _SegmentInfo.Unconnected;
            for (uint i = 0; i < nSegInfos.Length; i++)
            {
                _MergeSegmentInfo segInfo = nSegInfos[(int)i];
                if (segInfo.IsConnected(out _Endpoint endpoint))
                {
                    endpoint -= numDemotedTris;
                    Binary._fastWriteAlignedNative(nData, ref nHead, in endpoint);
                }
                else
                {
                    Binary._fastWriteAlignedNative(nData, ref nHead, in unconnectedSegInfo);
                }
            }

            // Update header
            header = new _PrimitiveHeader
            {
                VertexType = vertType,
                NumVertices = nNumVerts,
                NumHotVertices = nNumHotVerts,
                NumTriangles = nNumTris,
                NumHotTriangles = nNumHotTris
            };

            // Sanity check for written data size
            Debug.Assert(bodyHead + header.BodySize == nHead);

            // Finalize link info
            int linkOffset = (int)nHotTriIndex - (int)numDemotedTris;
            uint aHotEndpointIndex = aHotTriIndex * 3;
            uint bHotEndpointIndex = bHotTriIndex * 3;
            for (uint i = 0; i < aNumHotIndices; i++)
                _EndpointLinkInfo._addOther(ref endpointLinkInfos[aHotEndpointIndex + i], linkOffset);
            for (uint i = 0; i < bNumHotIndices; i++)
                _EndpointLinkInfo._addOther(ref endpointLinkInfos[bHotEndpointIndex + i], linkOffset);

            // Advance indices
            aHotTriIndex += aNumHotTris;
            bHotTriIndex += bNumHotTris;
            nHotTriIndex += nNumHotTris;
        }

        /// <summary>
        /// A helper class for generating and refining triangles during a merge operation.
        /// </summary>
        private unsafe ref struct _MergeTriangulator
        {
            /// <summary>
            /// Describes how geometry in the original meshes are linked together.
            /// </summary>
            public Span<_EndpointLinkInfo> EndpointLinkInfos;
            
            /// <summary>
            /// The vertices that may be referenced by this <see cref="_MergeTriangulator"/>.
            /// </summary>
            public RegularMeshVertexSpanList Vertices;

            /// <summary>
            /// The indices for the triangles produced by this <see cref="_MergeTriangulator"/>.
            /// </summary>
            public Span<uint> Indices;

            /// <summary>
            /// The <see cref="_MergeSegmentInfo"/>s for the triangles produced by this
            /// <see cref="_MergeTriangulator"/>.
            /// </summary>
            public Span<_MergeSegmentInfo> SegmentInfos;

            /// <summary>
            /// The next unused triangle index in this <see cref="_MergeTriangulator"/>.
            /// </summary>
            public uint* NextFreeTriangleIndex;

            /// <summary>
            /// Describes the first source mesh for this <see cref="_MergeTriangulator"/>.
            /// </summary>
            public _SourceInfo A;

            /// <summary>
            /// Describes the second source mesh for this <see cref="_MergeTriangulator"/>.
            /// </summary>
            public _SourceInfo B;

            /// <summary>
            /// Describes a source of geometry data for a <see cref="_MergeTriangulator"/>.
            /// </summary>
            public ref struct _SourceInfo
            {
                /// <summary>
                /// The index of the first triangle for this source in <see cref="EndpointLinkInfos"/>.
                /// </summary>
                public uint HotTriangleIndex;

                /// <summary>
                /// The index of the first cold vertex for this source in <see cref="Vertices"/>.
                /// </summary>
                public uint ColdVertexIndex;

                /// <summary>
                /// The number of cold vertices that may be referenced by this source.
                /// </summary>
                public uint NumColdVertices;

                /// <summary>
                /// The number of triangles for this source.
                /// </summary>
                public uint NumTriangles;

                /// <summary>
                /// A mapping from hot indices in this source to the vertex indices in <see cref="Vertices"/>.
                /// Vertices that map to <see cref="uint.MaxValue"/> have been dissolved.
                /// </summary>
                public SpanList<uint> HotIndexMap;

                /// <summary>
                /// The indices for the triangles in this source.
                /// </summary>
                public SpanList<uint> Indices;

                /// <summary>
                /// The <see cref="_SegmentInfo"/>s for the triangles in this source.
                /// </summary>
                public SpanList<_SegmentInfo> SegmentInfos;

                /// <summary>
                /// Tries mapping a vertex index from this source to one in the merged mesh, or returns
                /// <see cref="false"/> if the vertex has been dissolved.
                /// </summary>
                public bool TryMapIndex(uint sourceIndex, out uint resIndex)
                {
                    if (sourceIndex < NumColdVertices)
                    {
                        resIndex = ColdVertexIndex + sourceIndex;
                        return true;
                    }
                    else
                    {
                        resIndex = HotIndexMap[sourceIndex - NumColdVertices];
                        return resIndex != uint.MaxValue;
                    }
                }
            }

            /// <summary>
            /// Adds all merged geometry to the triangulator.
            /// </summary>
            public void AddAll()
            {
                for (uint i = 0; i < A.NumTriangles; i++)
                    TryAddPolygon(i, true);
                for (uint i = 0; i < B.NumTriangles; i++)
                    TryAddPolygon(i, false);
            }

            /// <summary>
            /// Attempts to add the merged polygon to the triangulator that contains the given triangle from
            /// one of the triangulator's sources. This will fail if the polygon has already been processed,
            /// or if the triangle has been completely dissolved.
            /// </summary>
            public bool TryAddPolygon(uint triIndex, bool isOnA)
            {
                _SourceInfo curSource = (isOnA ? A : B);
                bool needsRefinement = false;

                // Starting vertex for the polygon must not have been dissolved
                uint startIndex;
                _Endpoint startEndpoint;
                uint phase = 0;
                while (phase < 3)
                {
                    startEndpoint = new _Endpoint(triIndex, phase);
                    if (curSource.TryMapIndex(curSource.Indices[startEndpoint.Index], out startIndex))
                        goto traverseStart;
                    phase++;
                }
                return false;

            traverseStart:
                // Traverse around the endpoint vertex to find the first segment of the polygon
                uint prevIndex;
                _Endpoint prevEndpoint;
                _MergeSegmentInfo startSegInfo;
                {
                    uint startEndpointIndex = (curSource.HotTriangleIndex + startEndpoint).Index;
                    _EndpointLinkInfo info = EndpointLinkInfos[(int)startEndpointIndex];
                    switch (info.Type)
                    {
                        case _EndpointLinkType.UnlinkedCold:
                        case _EndpointLinkType.UnlinkedHot:
                            prevEndpoint = startEndpoint.Next;
                            if (curSource.TryMapIndex(curSource.Indices[prevEndpoint.Index], out prevIndex))
                            {
                                startSegInfo = _MergeSegmentInfo.Unconnected(startEndpointIndex);
                                goto traverse;
                            }
                            else
                            {
                                needsRefinement = true;
                                _SegmentInfo curSegInfo = curSource.SegmentInfos[startEndpoint.Index];
                                if (curSegInfo.IsConnected(out startEndpoint))
                                {
                                    startEndpoint = startEndpoint.Next;
                                    goto traverseStart;
                                }
                                else
                                {
                                    startSegInfo = _MergeSegmentInfo.Unconnected(startEndpointIndex);
                                    goto traverseStartDissolved;
                                }
                            }
                        case _EndpointLinkType.Coincident:
                            isOnA = !isOnA;
                            curSource = (isOnA ? A : B);
                            startEndpoint = info.Other - curSource.HotTriangleIndex;
                            goto traverseStart;
                        case _EndpointLinkType.Edge:
                            throw new NotImplementedException();
                        case _EndpointLinkType.Interior:
                            throw new NotImplementedException();
                        default: // Polygon has already been added for triangle
                            return false;
                    }
                }

            traverseStartDissolved:
                // Traverse around the current (dissolved) vertex to find the first segment
                {
                    _EndpointLinkInfo info = EndpointLinkInfos[(int)(curSource.HotTriangleIndex + prevEndpoint).Index];
                    switch (info.Type)
                    {
                        case _EndpointLinkType.UnlinkedCold:
                        case _EndpointLinkType.UnlinkedHot:
                            _SegmentInfo curSegInfo = curSource.SegmentInfos[prevEndpoint.Index];
                            if (curSegInfo.IsConnected(out _Endpoint nextEndpoint))
                            {
                                prevEndpoint = nextEndpoint.Next;
                                goto traverseStartDissolved;
                            }
                            else
                            {
                                Debug.Assert(curSegInfo.IsUnconnected);
                                prevEndpoint = prevEndpoint.Next;
                                if (curSource.TryMapIndex(curSource.Indices[prevEndpoint.Index], out prevIndex))
                                    goto traverse;
                                else
                                    goto traverseStartDissolved;
                            }
                        case _EndpointLinkType.Coincident:
                            isOnA = !isOnA;
                            curSource = (isOnA ? A : B);
                            prevEndpoint = info.Other - curSource.HotTriangleIndex;
                            goto traverseStartDissolved;
                        default:
                            // Should never be hit. Vertices may only be dissolved if they are coincident with
                            // another vertex.
                            throw new Exception();
                    }
                }

            traverse:
                // Traverse to find the next polygon segment
                _Endpoint curEndpoint;
                _MergeSegmentInfo prevSegInfo;
                uint prevEndpointIndex;
                {
                    prevEndpointIndex = (curSource.HotTriangleIndex + prevEndpoint).Index;
                    ref _EndpointLinkInfo info = ref EndpointLinkInfos[(int)prevEndpointIndex];
                    switch (info.Type)
                    {
                        case _EndpointLinkType.UnlinkedCold:
                        case _EndpointLinkType.UnlinkedHot:
                            curEndpoint = prevEndpoint.Next;
                            if (curSource.TryMapIndex(curSource.Indices[curEndpoint.Index], out uint curIndex))
                            {
                                if (curIndex != startIndex)
                                {
                                    prevSegInfo = _MergeSegmentInfo.Unconnected(prevEndpointIndex);
                                    AddTriangle(needsRefinement,
                                        startIndex, startSegInfo,
                                        prevIndex, prevSegInfo,
                                        curIndex, out startSegInfo);
                                    prevIndex = curIndex;
                                    prevEndpoint = curEndpoint;
                                    goto traverse;
                                }
                                else
                                {
                                    // Close polygon
                                    Util.Assert(startSegInfo.IsConnected(out _Endpoint nEndpoint));
                                    SegmentInfos[(int)nEndpoint.Index] = _MergeSegmentInfo.Unconnected(prevEndpointIndex);
                                    info = _EndpointLinkInfo.Final(needsRefinement, nEndpoint);
                                    return true;
                                }
                            }
                            else
                            {
                                _SegmentInfo curSegInfo = curSource.SegmentInfos[prevEndpoint.Index];
                                if (curSegInfo.IsConnected(out prevEndpoint))
                                {
                                    prevEndpoint = prevEndpoint.Next;
                                    needsRefinement = true;
                                    goto traverse;
                                }
                                else
                                {
                                    Debug.Assert(curSegInfo.IsUnconnected);
                                    prevSegInfo = _MergeSegmentInfo.Unconnected(prevEndpointIndex);
                                    goto traverseDissolved;
                                }
                            }
                        case _EndpointLinkType.Coincident:
                            isOnA = !isOnA;
                            curSource = (isOnA ? A : B);
                            prevEndpoint = info.Other - curSource.HotTriangleIndex;
                            goto traverse;
                        case _EndpointLinkType.Edge:
                            throw new NotImplementedException();
                        case _EndpointLinkType.Interior:
                            throw new NotImplementedException();
                        default:
                            // Should never be hit. If the first segment hasn't been finalized yet, none of the
                            // segments on the polygon should be finalized.
                            throw new Exception();
                    }
                }

            traverseDissolved:
                // Traverse around the current (dissolved) vertex to find the segment connected to the previous segment
                {
                    _EndpointLinkInfo info = EndpointLinkInfos[(int)(curSource.HotTriangleIndex + curEndpoint).Index];
                    switch (info.Type)
                    {
                        case _EndpointLinkType.UnlinkedCold:
                        case _EndpointLinkType.UnlinkedHot:
                            _SegmentInfo curSegInfo = curSource.SegmentInfos[curEndpoint.Index];
                            if (curSegInfo.IsConnected(out _Endpoint nextEndpoint))
                            {
                                curEndpoint = nextEndpoint.Next;
                                goto traverseDissolved;
                            }
                            else
                            {
                                Debug.Assert(curSegInfo.IsUnconnected);
                                curEndpoint = curEndpoint.Next;
                                if (curSource.TryMapIndex(curSource.Indices[curEndpoint.Index], out uint curIndex))
                                {
                                    if (curIndex != startIndex)
                                    {
                                        AddTriangle(true,
                                            startIndex, startSegInfo,
                                            prevIndex, prevSegInfo,
                                            curIndex, out startSegInfo);
                                        prevIndex = curIndex;
                                        prevEndpoint = curEndpoint;
                                        needsRefinement = true;
                                        goto traverse;
                                    }
                                    else
                                    {
                                        // Close polygon
                                        Util.Assert(startSegInfo.IsConnected(out _Endpoint nEndpoint));
                                        ref _EndpointLinkInfo prevInfo = ref EndpointLinkInfos[(int)prevEndpointIndex];
                                        SegmentInfos[(int)nEndpoint.Index] = _MergeSegmentInfo.Unconnected(prevEndpointIndex);
                                        prevInfo = _EndpointLinkInfo.Final(needsRefinement, nEndpoint);
                                        return true;
                                    }
                                }
                                else
                                {
                                    goto traverseDissolved;
                                }
                            }
                        case _EndpointLinkType.Coincident:
                            isOnA = !isOnA;
                            curSource = (isOnA ? A : B);
                            curEndpoint = info.Other - curSource.HotTriangleIndex;
                            goto traverseDissolved;
                        default:
                            // Should never be hit. Vertices may only be dissolved if they are coincident with
                            // another vertex.
                            throw new Exception();
                    }
                }
            }

            /// <summary>
            /// Adds a new triangle to the triangulator, refining existing triangles as needed.
            /// </summary>
            public unsafe void AddTriangle(
                bool needsRefinement,
                uint aIndex, _MergeSegmentInfo abSegInfo,
                uint bIndex, _MergeSegmentInfo bcSegInfo,
                uint cIndex, out _MergeSegmentInfo acSegInfo)
            {
                Debug.Assert(aIndex != cIndex);

                // Populate index data for triangle
                uint triIndex = (*NextFreeTriangleIndex)++;
                uint baseIndex = triIndex * 3;
                Indices[(int)(baseIndex + 0)] = aIndex;
                Indices[(int)(baseIndex + 1)] = bIndex;
                Indices[(int)(baseIndex + 2)] = cIndex;

                // Process first connection
                _Endpoint connectEndpoint;
                SegmentInfos[(int)(baseIndex + 0)] = abSegInfo;
                if (abSegInfo.IsConnected(out connectEndpoint))
                {
                    // TODO: Delaunay refinement
                    ref _MergeSegmentInfo complSegInfo = ref SegmentInfos[(int)connectEndpoint.Index];
                    Debug.Assert(complSegInfo.IsInvalid);
                    complSegInfo = _MergeSegmentInfo.Connected(new _Endpoint(triIndex, 0));
                }
                else
                {
                    // Finalize link info for unconnected segment
                    Util.Assert(abSegInfo.IsUnconnected(out uint linkIndex));
                    ref _EndpointLinkInfo linkInfo = ref EndpointLinkInfos[(int)linkIndex];
                    Debug.Assert(linkInfo.Type != _EndpointLinkType.FinalClean);
                    Debug.Assert(linkInfo.Type != _EndpointLinkType.FinalDirty);
                    linkInfo = _EndpointLinkInfo.Final(needsRefinement, new _Endpoint(triIndex, 0));
                }

                // Process second connection
                SegmentInfos[(int)(baseIndex + 1)] = bcSegInfo;
                if (bcSegInfo.IsConnected(out connectEndpoint))
                {
                    // TODO: Delaunay refinement
                    ref _MergeSegmentInfo complSegInfo = ref SegmentInfos[(int)connectEndpoint.Index];
                    Debug.Assert(complSegInfo.IsInvalid);
                    complSegInfo = _MergeSegmentInfo.Connected(new _Endpoint(triIndex, 1));
                }
                else
                {
                    // Finalize link info for unconnected segment
                    Util.Assert(bcSegInfo.IsUnconnected(out uint linkIndex));
                    ref _EndpointLinkInfo linkInfo = ref EndpointLinkInfos[(int)linkIndex];
                    Debug.Assert(linkInfo.Type != _EndpointLinkType.FinalClean);
                    Debug.Assert(linkInfo.Type != _EndpointLinkType.FinalDirty);
                    linkInfo = _EndpointLinkInfo.Final(needsRefinement, new _Endpoint(triIndex, 1));
                }

                // Process third connection
                SegmentInfos[(int)(baseIndex + 2)] = _MergeSegmentInfo.Invalid;
                acSegInfo = _MergeSegmentInfo.Connected(new _Endpoint(triIndex, 2));
            }

            /// <summary>
            /// Attempts to connect the segments for the given source <see cref="_Endpoint"/>s, as needed when
            /// merging separate polygons. This will fail if the endpoints do not correspond to segments of
            /// generated polygons.
            /// </summary>
            public bool TryConnect(_Endpoint a, _Endpoint b, bool isOnA)
            {
                return _tryConnect(a, b, isOnA ? A.HotTriangleIndex : B.HotTriangleIndex);
            }

            /// <summary>
            /// Attempts to connect the segments for the given source <see cref="_Endpoint"/>s, as needed when
            /// merging separate polygons. This will fail if the endpoints do not correspond to segments of
            /// generated polygons.
            /// </summary>
            internal bool _tryConnect(_Endpoint a, _Endpoint b, uint baseHotTriIndex)
            {
                ref _EndpointLinkInfo aLinkInfo = ref EndpointLinkInfos[(int)(baseHotTriIndex + a).Index];
                if (aLinkInfo.IsFinal(out bool aIsDirty, out a))
                {
                    ref _EndpointLinkInfo bLinkInfo = ref EndpointLinkInfos[(int)(baseHotTriIndex + b).Index];
                    if (bLinkInfo.IsFinal(out bool bIsDirty, out b))
                    {
                        // TODO: Delaunay refinement
                        ref _MergeSegmentInfo aSegInfo = ref SegmentInfos[(int)a.Index];
                        ref _MergeSegmentInfo bSegInfo = ref SegmentInfos[(int)b.Index];
                        aSegInfo = _MergeSegmentInfo.Connected(b);
                        bSegInfo = _MergeSegmentInfo.Connected(a);
                        aLinkInfo = _EndpointLinkInfo.UnlinkedCold;
                        bLinkInfo = _EndpointLinkInfo.UnlinkedCold;
                        return true;
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// A variant of <see cref="_SegmentInfo"/> used within a <see cref="_MergeTriangulator"/>.
        /// </summary>
        [DebuggerDisplay("{ToString(),nq}")]
        internal struct _MergeSegmentInfo
        {
            private uint _def;
            private _MergeSegmentInfo(uint def)
            {
                _def = def;
            }

            /// <summary>
            /// An invalid <see cref="_MergeSegmentInfo"/> used to indicate a segment hasn't been processed yet.
            /// </summary>
            public static _MergeSegmentInfo Invalid => new _MergeSegmentInfo(uint.MaxValue);

            /// <summary>
            /// Describes a triangle segment that is not connected to any triangles within the same polygon. The index
            /// of the "hanging" endpoint is given by <paramref name="index"/>.
            /// </summary>
            public static _MergeSegmentInfo Unconnected(uint index)
            {
                Debug.Assert(index < 0x7FFFFFFF);
                return new _MergeSegmentInfo(0x80000000 | index);
            }

            /// <summary>
            /// Describes a triangle segment that is connected, within the same polygon, to the segment whose
            /// initial endpoint is the <paramref name="startEndpoint"/>. This is a complementary relationship that
            /// should also be reflected by the other segment.
            /// </summary>
            public static _MergeSegmentInfo Connected(_Endpoint startEndpoint)
            {
                Debug.Assert(startEndpoint._def < 0x80000000);
                return new _MergeSegmentInfo(startEndpoint._def);
            }

            /// <summary>
            /// Indicates whether this is <see cref="Invalid"/>.
            /// </summary>
            public bool IsInvalid => _def == uint.MaxValue;

            /// <summary>
            /// Indicates whether this is <see cref="Unconnected(uint)"/>, and if so, gets the index of the
            /// hanging endpoint.
            /// </summary>
            public bool IsUnconnected(out uint index)
            {
                index = _def & ~0x80000000;
                return unchecked((int)_def < (int)uint.MaxValue);
            }

            /// <summary>
            /// Indicates whether this is <see cref="Connected(_Endpoint)"/>, and if so, gets the initial
            /// endpoint of the complementary segment.
            /// </summary>
            public bool IsConnected(out _Endpoint startEndpoint)
            {
                startEndpoint = new _Endpoint { _def = _def };
                return _def < 0x80000000;
            }

            public override string ToString()
            {
                if (IsInvalid)
                    return nameof(Invalid);
                if (IsUnconnected(out uint index))
                    return nameof(Unconnected) + "(" + index.ToStringInvariant() + ")";
                Util.Assert(IsConnected(out _Endpoint startEndpoint));
                return nameof(Connected) + "(" + startEndpoint.ToString() + ")";
            }
        }

        /// <summary>
        /// Identifies and describes what an endpoint is "linked" to during a merge operation. This describes which
        /// element is next in a counter-clockwise traversal of the perimeter of a merged polygon.
        /// </summary>
        [DebuggerDisplay("{ToString(),nq}")]
        private struct _EndpointLinkInfo
        {
            private uint _def;
            private _EndpointLinkInfo(_EndpointLinkType type, _Endpoint other)
            {
                Debug.Assert(other._def < 0x20000000);
                _def = (uint)((byte)type << 29) | other._def;
            }

            /// <summary>
            /// The overall <see cref="_EndpointLinkType"/> of the link.
            /// </summary>
            public _EndpointLinkType Type => (_EndpointLinkType)(_def >> 29);

            /// <summary>
            /// The other endpoint involved in the link, if applicable.
            /// </summary>
            public _Endpoint Other
            {
                get
                {
                    return new _Endpoint { _def = _def & 0x1FFFFFFF };
                }
                set
                {
                    Debug.Assert(value._def < 0x20000000);
                    _def = (_def & 0xE0000000) | value._def;
                }
            }

            /// <summary>
            /// Indicates that the endpoint is coincident with another endpoint on the other merged mesh.
            /// </summary>
            public static _EndpointLinkInfo Coincident(_Endpoint other)
            {
                return new _EndpointLinkInfo(_EndpointLinkType.Coincident, other);
            }

            /// <summary>
            /// Indicates that the endpoint is on a segment of the other merged mesh, or it starts a segment on
            /// which there is an endpoint from the other merged mesh.
            /// </summary>
            public static _EndpointLinkInfo Edge(_Endpoint other)
            {
                return new _EndpointLinkInfo(_EndpointLinkType.Edge, other);
            }
            
            /// <summary>
            /// Indicates that the endpoint is on a segment of the other merged mesh, and that the next endpoint
            /// along the segment is on the original mesh.
            /// </summary>
            public static _EndpointLinkInfo Interior(_Endpoint next)
            {
                return new _EndpointLinkInfo(_EndpointLinkType.Interior, next);
            }

            /// <summary>
            /// Indicates that the endpoint is not linked to anything and is not part of any unmatched
            /// <see cref="MeshTie"/>s.
            /// </summary>
            public static _EndpointLinkInfo UnlinkedCold => new _EndpointLinkInfo(_EndpointLinkType.UnlinkedCold, default);

            /// <summary>
            /// Indicates that the endpoint is not linked to anything, but is part of a <see cref="MeshTie"/> that
            /// may be linked later on.
            /// </summary>
            public static _EndpointLinkInfo UnlinkedHot => new _EndpointLinkInfo(_EndpointLinkType.UnlinkedHot, default);

            /// <summary>
            /// Indicates that the new segment for the endpoint has been built.
            /// </summary>
            /// <param name="isDirty">Indicates whether a triangle linking to the new segment will need a
            /// refinement check.</param>
            public static _EndpointLinkInfo Final(bool isDirty, _Endpoint @res)
            {
                _EndpointLinkType type = isDirty ? _EndpointLinkType.FinalDirty : _EndpointLinkType.FinalClean;
                return new _EndpointLinkInfo(type, @res);
            }
            
            /// <summary>
            /// Indicates whether this is <see cref="Coincident(_Endpoint)"/>.
            /// </summary>
            public bool IsCoincident(out _Endpoint other)
            {
                other = Other;
                return Type == _EndpointLinkType.Coincident;
            }

            /// <summary>
            /// Indicates whether this is <see cref="UnlinkedCold"/>.
            /// </summary>
            public bool IsUnlinkedCold => Type == _EndpointLinkType.UnlinkedCold;

            /// <summary>
            /// Indicates whether this is <see cref="UnlinkedHot"/>.
            /// </summary>
            public bool IsUnlinkedHot => Type == _EndpointLinkType.UnlinkedHot;

            /// <summary>
            /// Indicates whether this is <see cref="Final(bool, _Endpoint)"/>.
            /// </summary>
            public bool IsFinal(out bool isDirty, out _Endpoint res)
            {
                _EndpointLinkType type = Type;
                isDirty = (type == _EndpointLinkType.FinalDirty);
                res = Other;
                return type >= _EndpointLinkType.FinalClean;
            }

            /// <summary>
            /// Adds the given offset to the triangle index of <see cref="Other"/>.
            /// </summary>
            internal static void _addOther(ref _EndpointLinkInfo info, int triOffset)
            {
                info._def += unchecked((uint)triOffset) << 2;
            }
            
            public override string ToString()
            {
                if (Type == _EndpointLinkType.UnlinkedCold)
                    return nameof(UnlinkedCold);
                if (Type == _EndpointLinkType.UnlinkedHot)
                    return nameof(UnlinkedHot);
                return Type.ToString() + "(" + Other + ")";
            }
        }

        /// <summary>
        /// Identifies a general type of endpoint link.
        /// </summary>
        private enum _EndpointLinkType : byte
        {
            UnlinkedCold,
            UnlinkedHot,
            Coincident,
            Edge,
            Interior,
            FinalClean,
            FinalDirty,
        }
    }
    
    /// <summary>
    /// A helper class for specifying constraints between the parameters for a <see cref="Mesh"/>.
    /// </summary>
    public struct MeshSolver
    {
        private bool _hasContradiction;
        internal ListBuilder<_TextureInfo> _textureInfos;

        /// <summary>
        /// Describes the value of a texture parameter. This may either be an instantiation of a "base" texture, or a
        /// transformed copy of another texture parameter.
        /// </summary>
        internal struct _TextureInfo
        {
            private uint _index;
            private TextureLaxity _laxity;
            private Rotation2 _tbn;
            private Affine2 _coord;

            /// <summary>
            /// Constructs a <see cref="_TextureInfo"/> describing a texture that is a unique instantiation of a base
            /// texture.
            /// </summary>
            /// <param name="laxity">The allowed transformations for the instantiation.</param>
            /// <param name="tbn">The transform applied to the TBN basis of the vertices referencing the texture,
            /// applied after <paramref name="laxity"/>.</param>
            /// <param name="coord">The transform applied to the coordinates for the texture,
            /// applied after <paramref name="laxity"/>.</param>
            public static _TextureInfo Instantiate(uint baseIndex, TextureLaxity laxity, Rotation2 tbn, Affine2 coord)
            {
                return new _TextureInfo
                {
                    _index = baseIndex | 0x80000000,
                    _laxity = laxity,
                    _tbn = tbn,
                    _coord = coord
                };
            }
            
            /// <summary>
            /// Constructs a <see cref="_TextureInfo"/> describing a texture that is a transformed copy of another
            /// texture with a lower index.
            /// </summary>
            /// <param name="sourceIndex">The index of the source texture. This must be less than the index of
            /// this texture.</param>
            public static _TextureInfo Dependent(uint sourceIndex, Rotation2 tbn, Affine2 coord)
            {
                return new _TextureInfo
                {
                    _index = sourceIndex,
                    _tbn = tbn,
                    _coord = coord
                };
            }

            /// <summary>
            /// Determines whether this <see cref="_TextureInfo"/> describes an <see cref="Instantiate"/> texture, and if
            /// so, gets its properties.
            /// </summary>
            public bool IsInstantiate(out uint baseIndex, out TextureLaxity laxity, out Rotation2 tbn, out Affine2 coord)
            {
                if ((_index & 0x80000000) > 0)
                {
                    baseIndex = _index & ~0x80000000;
                    laxity = _laxity;
                    tbn = _tbn;
                    coord = _coord;
                    return true;
                }
                baseIndex = default;
                laxity = default;
                tbn = default;
                coord = default;
                return false;
            }

            /// <summary>
            /// Determines whether this <see cref="_TextureInfo"/> describes a <see cref="Dependent"/> texture, and if
            /// so, gets its properties.
            /// </summary>
            public bool IsDependent(out uint sourceIndex, out Rotation2 tbn, out Affine2 trans)
            {
                if ((_index & 0x80000000) == 0)
                {
                    sourceIndex = _index;
                    tbn = _tbn;
                    trans = _coord;
                    return true;
                }
                sourceIndex = default;
                tbn = default;
                trans = default;
                return false;
            }
        }

        /// <summary>
        /// Indicates whether it is possible for all constraints specified for this solver to be satisfied simultaneously.
        /// </summary>
        public bool IsSatisfiable => !_hasContradiction;

        /// <summary>
        /// Specifies that the given <see cref="DependentTexture"/>s have an identical appearance when instantiated.
        /// </summary>
        public void RequireEqual(DependentTexture a, DependentTexture b)
        {
        retry:
            // If the textures reference the same source, they must have the same transform
            if (a.Index == b.Index)
            {
                if (Rotation2.AreEqualOdds(a.Tbn, b.Tbn) *
                    Affine2.AreEqualOdds(a.Coord, b.Coord) > 1)
                    return;
                goto contradiction;
            }

            // Try matching distinct textures
            uint aIndex, bIndex;
            TextureLaxity aLaxity, bLaxity;
            Rotation2 aInitTbn, bInitTbn;
            Affine2 aInitCoord, bInitCoord;
            ref _TextureInfo aInfo = ref _textureInfos[a.Index];
            ref _TextureInfo bInfo = ref _textureInfos[b.Index];
            if (aInfo.IsInstantiate(out aIndex, out aLaxity, out aInitTbn, out aInitCoord))
            {
                if (bInfo.IsInstantiate(out bIndex, out bLaxity, out bInitTbn, out bInitCoord))
                {
                    if (aIndex == bIndex)
                    {
                        // Verify that the textures are related by a motion
                        Rotation2 abFullTbn = b.Tbn * bInitTbn * (a.Tbn * aInitTbn).Inverse;
                        Affine2 abFullCoord = b.Coord * bInitCoord * (a.Coord * aInitCoord).Inverse;
                        Rotation2 abFullRot = (Rotation2)abFullCoord.Linear;
                        if (Matrix2x2.AreLikelyEqual(abFullCoord.Linear, abFullRot) &&
                            Rotation2.AreLikelyEqual(abFullTbn, abFullRot.Inverse))
                        {
                            Motion2 abFullMotion = new Motion2(abFullRot, abFullCoord.Offset);
                            if (a.Index < b.Index)
                            {
                                // Redefine B in terms of A
                                if (!bLaxity.TryRestrict(abFullMotion, out Motion2 aPreTrans, ref aLaxity))
                                    goto contradiction;
                                aInitTbn = aInitTbn * aPreTrans.Linear.Inverse;
                                aInitCoord = aInitCoord * aPreTrans;
                                aInfo = _TextureInfo.Instantiate(aIndex, aLaxity, aInitTbn, aInitCoord);
                                bInitTbn = a.Tbn * b.Tbn.Inverse;
                                bInitCoord = a.Coord * b.Coord.Inverse;
                                bInfo = _TextureInfo.Dependent(a.Index, bInitTbn, bInitCoord);
                            }
                            else
                            {
                                // Redefine A in terms of B
                                if (!aLaxity.TryRestrict(abFullMotion.Inverse, out Motion2 bPreTrans, ref bLaxity))
                                    goto contradiction;
                                bInitTbn = bInitTbn * bPreTrans.Linear.Inverse;
                                bInitCoord = bInitCoord * bPreTrans;
                                bInfo = _TextureInfo.Instantiate(bIndex, bLaxity, bInitTbn, bInitCoord);
                                aInitTbn = b.Tbn * a.Tbn.Inverse;
                                aInitCoord = b.Coord * a.Coord.Inverse;
                                aInfo = _TextureInfo.Dependent(b.Index, aInitTbn, aInitCoord);
                            }
                            return;
                        }
                    }
                }
                else
                {
                    Util.Assert(bInfo.IsDependent(out bIndex, out bInitTbn, out bInitCoord));
                    b = new DependentTexture(bIndex, bInitTbn * b.Tbn, bInitCoord * b.Coord);
                    goto retry;
                }
            }
            else
            {
                Util.Assert(aInfo.IsDependent(out aIndex, out aInitTbn, out aInitCoord));
                throw new NotImplementedException();
            }
        contradiction:
            _hasContradiction = true;
        }
    }
}
