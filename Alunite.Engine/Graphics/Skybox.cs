﻿using System;

using Alunite.Resources;

namespace Alunite.Graphics
{
    /// <summary>
    /// Describes the visual contents of a physically-based scene that appear "at infinity".
    /// </summary>
    public struct Skybox
    {
        public BigImage2<Radiance> Image;
    }
}