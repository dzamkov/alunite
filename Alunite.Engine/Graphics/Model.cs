﻿using System;
using System.Diagnostics;

using Alunite.Data.Geometry;
using Alunite.Resources;

using Alunite.Data;
using Alunite.Data.Probability;

namespace Alunite.Graphics
{
    /// <summary>
    /// Encapsulates a reusable, probabilistic procedure for rendering a physically-based visual effect or object
    /// in three-dimensional space.
    /// </summary>
    public struct Model
    {
        internal Model(_InnerModelForm inner,
            Permutation<MaterialTexture2> baseTextureInsts,
            List<ModelForm._TextureInstance> textureInsts)
        {
            _inner = inner;
            _baseTextureInsts = baseTextureInsts;
            _textureInsts = textureInsts;
        }

        /// <summary>
        /// The source <see cref="_InnerModelForm"/> for this model.
        /// </summary>
        internal _InnerModelForm _inner;

        /// <summary>
        /// The basic <see cref="MaterialTexture2"/>s provided as texture parameters to <see cref="_inner"/>.
        /// These are also used as sources for <see cref="_textureInsts"/>.
        /// </summary>
        internal Permutation<MaterialTexture2> _baseTextureInsts;

        /// <summary>
        /// The instantiations of <see cref="_baseTextureInsts"/> provided as parameters to <see cref="_inner"/>.
        /// </summary>
        internal List<ModelForm._TextureInstance> _textureInsts;

        /// <summary>
        /// The number of texture parameters supplied to <see cref="_inner"/>.
        /// </summary>
        private uint _numInnerTextures => _baseTextureInsts.Length + _textureInsts.Length;

        /// <summary>
        /// A <see cref="Model"/> with no visual contents.
        /// </summary>
        public static Model Empty => new Model(_InnerModelForm.Empty,
            Permutation.Empty<MaterialTexture2>(),
            List.Empty<ModelForm._TextureInstance>());

        /// <summary>
        /// Loads the model asset at the given path.
        /// </summary>
        public static Model FromAsset(Path assetPath)
        {
            return (Model)ModelForm.FromAsset(assetPath);
        }

        /// <summary>
        /// Constructs an opaque model using the given <see cref="Material"/> and mesh.
        /// </summary>
        public static Model Solid(Material mat, BigList<SolidVertex> verts, BigList<uint> indices)
        {
            // TODO: Delay loading for really big lists
            Blend1SolidVertex[] nVertItems = new Blend1SolidVertex[verts.Length];
            using (verts.Read(out IListReader<SolidVertex> vertReader))
            {
                for (uint i = 0; i < verts.Length; i++)
                {
                    var vert = vertReader.ReadItem();
                    nVertItems[i] = new Blend1SolidVertex
                    {
                        Position = vert.Position,
                        Texture = VertexTextureFactor.Build(0, vert.MaterialCoord, vert.Tbn, vert.MaterialAlpha, false)
                    };
                }
            }

            throw new NotImplementedException();
        }

        /// <summary>
        /// Indicates whether this is <see cref="Empty"/>.
        /// </summary>
        public bool IsEmpty => _inner.IsEmpty;

        /// <summary>
        /// Constructs an instance of this model.
        /// </summary>
        public InstanceModel Instantiate(ISampler sampler)
        {
            if (_textureInsts.IsEmpty)
                return new InstanceModel(_inner, _baseTextureInsts);
            Span<DependentTexture> innerTextures = stackalloc DependentTexture[(int)_numInnerTextures];

            // Retain existing base textures
            for (uint i = 0; i < _baseTextureInsts.Length; i++)
                innerTextures[(int)i] = DependentTexture.Parameter(i);

            // Instantiate lax textures
            for (uint i = 0; i < _textureInsts.Length; i++)
            {
                var textureInst = _textureInsts[i];
                Motion2 trans = textureInst.Laxity.Sample(sampler);
                innerTextures[(int)(_baseTextureInsts.Length + i)] = new DependentTexture(
                    textureInst.TextureIndex, trans.Linear.Inverse, trans.Linear);
            }

            // Construct new model
            return new InstanceModel(_inner.TransformApply(Motion3.Identity, List.Of(innerTextures)), _baseTextureInsts);
        }

        /// <summary>
        /// Renders this model to the given <see cref="IRenderer"/>.
        /// </summary>
        public void Render(IRenderer renderer)
        {
            if (_textureInsts.Length > 0)
            {
                // TODO: Instantiate
                throw new NotImplementedException();
            }
            else
            {
                _inner.Render(renderer, _baseTextureInsts);
            }
        }

        /// <summary>
        /// Merges the visual contents of the given models.
        /// </summary>
        public static Model operator +(Model a, Model b)
        {
            if (a.IsEmpty)
                return b;
            if (b.IsEmpty)
                return a;

            // Set up material instance parameters for inner forms
            Span<DependentTexture> aInsts = stackalloc DependentTexture[(int)a._numInnerTextures];
            Span<DependentTexture> bInsts = stackalloc DependentTexture[(int)b._numInnerTextures];

            // Build new base texture instances
            var nBaseTexturesInstsBuilder = PermutationBuilder<MaterialTexture2>.Create(a._baseTextureInsts);
            for (uint i = 0; i < a._baseTextureInsts.Length; i++)
                aInsts[(int)i] = DependentTexture.Parameter(i);
            for (uint i = 0; i < b._baseTextureInsts.Length; i++)
                bInsts[(int)i] = DependentTexture.Parameter(
                    nBaseTexturesInstsBuilder.Include(b._baseTextureInsts[i]));

            // Build new texture instances
            uint nInstIndex = nBaseTexturesInstsBuilder.Length;
            uint numTextureInsts = a._textureInsts.Length + b._textureInsts.Length;
            var rawTextureInstItems = Array.Empty<ModelForm._TextureInstance>();
            if (numTextureInsts > 0)
            {
                rawTextureInstItems = new ModelForm._TextureInstance[numTextureInsts];
                for (uint i = 0; i < a._textureInsts.Length; i++)
                {
                    rawTextureInstItems[i] = a._textureInsts[i];
                    aInsts[(int)(i + a._baseTextureInsts.Length)] = DependentTexture.Parameter(nInstIndex++);
                }
                for (uint i = 0; i < b._textureInsts.Length; i++)
                {
                    var sTextureInst = b._textureInsts[i];
                    sTextureInst.TextureIndex = bInsts[(int)sTextureInst.TextureIndex].Index;
                    rawTextureInstItems[a._textureInsts.Length + i] = sTextureInst;
                    bInsts[(int)(i + b._baseTextureInsts.Length)] = DependentTexture.Parameter(nInstIndex++);
                }
            }

            // Combine forms
            var textureInfos = ListBuilder<MeshSolver._TextureInfo>.CreateEmpty(
                nBaseTexturesInstsBuilder.Length + numTextureInsts);
            for (uint i = 0; i < nBaseTexturesInstsBuilder.Length; i++)
            {
                textureInfos.Append(
                    MeshSolver._TextureInfo.Instantiate(i,
                        TextureLaxity.None,
                        Rotation2.Identity,
                        Affine2.Identity));
            }
            for (uint i = 0; i < (uint)rawTextureInstItems.Length; i++)
            {
                var textureInst = rawTextureInstItems[i];
                textureInfos.Append(
                    MeshSolver._TextureInfo.Instantiate(
                        textureInst.TextureIndex,
                        textureInst.Laxity,
                        Rotation2.Identity,
                        Affine2.Identity));
            }
            MeshSolver solver = new MeshSolver { _textureInfos = textureInfos };
            _InnerModelForm nForm = _InnerModelForm.MergeApply(solver, 
                a._inner, List.Of(aInsts), b._inner, List.Of(bInsts));

            // Apply solver constraints
            Span<DependentTexture> nTextures = stackalloc DependentTexture[(int)textureInfos.Length];
            for (uint i = 0; i < nBaseTexturesInstsBuilder.Length; i++)
                nTextures[(int)i] = new DependentTexture(i, Rotation2.Identity, Affine2.Identity);
            var nTextureInsts = ListBuilder<ModelForm._TextureInstance>.CreateEmpty();
            for (uint i = 0; i < rawTextureInstItems.Length; i++)
            {
                TextureLaxity laxity;
                Rotation2 tbn;
                Affine2 coord;
                uint textureIndex = nBaseTexturesInstsBuilder.Length + i;
                var textureInfo = textureInfos[textureIndex];
                if (textureInfo.IsInstantiate(out uint baseIndex, out laxity, out tbn, out coord))
                {
                    uint nTextureInstIndex = nTextureInsts.Append(new ModelForm._TextureInstance
                    {
                        TextureIndex = baseIndex,
                        Laxity = laxity
                    }) + nBaseTexturesInstsBuilder.Length;
                    nTextures[(int)textureIndex] = new DependentTexture(nTextureInstIndex, tbn, coord);
                }
                else
                {
                    Util.Assert(textureInfo.IsDependent(out uint sourceIndex, out tbn, out coord));
                    DependentTexture nTexture = nTextures[(int)sourceIndex];
                    nTextures[(int)textureIndex] = new DependentTexture(
                        nTexture.Index,
                        tbn * nTexture.Tbn,
                        coord * nTexture.Coord);
                }
            }
            nForm = nForm.TransformApply(Motion3.Identity, List.Of(nTextures));

            // Finalize model
            return new Model(nForm,
                nBaseTexturesInstsBuilder.Finish(),
                nTextureInsts.Finish());
        }

        /// <summary>
        /// Applies a <see cref="Motion3"/> to a model.
        /// </summary>
        public static Model operator *(Motion3 motion, Model model)
        {
            return new Model(
                model._inner.TransformApply(motion, DependentTexture.Identity(model._numInnerTextures)),
                model._baseTextureInsts,
                model._textureInsts);
        }
    }

    /// <summary>
    /// A restricted <see cref="Model"/> which does not allow any per-instance variability. This is a general description
    /// of something that can be rendered using a <see cref="IRenderer"/>.
    /// </summary>
    public struct InstanceModel
    {
        internal InstanceModel(_InnerModelForm inner, Permutation<MaterialTexture2> textureInsts)
        {
            _inner = inner;
            _textureInsts = textureInsts;
        }

        /// <summary>
        /// The source <see cref="_InnerModelForm"/> for this model.
        /// </summary>
        internal _InnerModelForm _inner;

        /// <summary>
        /// The basic <see cref="MaterialTexture2"/>s provided as material instance parameters to <see cref="_inner"/>.
        /// </summary>
        internal Permutation<MaterialTexture2> _textureInsts;

        /// <summary>
        /// Renders this model to the given <see cref="IRenderer"/>.
        /// </summary>
        public void Render(IRenderer renderer)
        {
            _inner.Render(renderer, _textureInsts);
        }

        /// <summary>
        /// Applies a <see cref="Motion3"/> to a model.
        /// </summary>
        public static InstanceModel operator *(Motion3 motion, InstanceModel model)
        {
            return new InstanceModel(
                model._inner.TransformApply(motion, DependentTexture.Identity(model._textureInsts.Length)),
                model._textureInsts);
        }

        public static implicit operator Model(InstanceModel source)
        {
            return new Model(source._inner, source._textureInsts, List.Empty<ModelForm._TextureInstance>());
        }
    }

    /// <summary>
    /// A <see cref="Model"/> parameterized by a set of <see cref="Material"/> and scalar variables.
    /// </summary>
    public struct ModelForm
    {
        internal ModelForm(
            List<MaterialKind> matParamKinds,
            _InnerModelForm inner,
            Permutation<MaterialTexture2> baseTextureInsts,
            List<_TextureInstance> textureInsts,
            List<_MaterialInstance> matInsts)
        {
            MaterialParameterKinds = matParamKinds;
            _inner = inner;
            _baseTextureInsts = baseTextureInsts;
            _textureInsts = textureInsts;
            _matInsts = matInsts;
        }

        /// <summary>
        /// The <see cref="MaterialKind"/>s for the the material parameters of this <see cref="ModelForm"/>.
        /// Parameters marked with <see cref="MaterialKind.Any"/> are not used in the model.
        /// </summary>
        public List<MaterialKind> MaterialParameterKinds { get; }

        /// <summary>
        /// The number of material parameters required by this <see cref="ModelForm"/>.
        /// </summary>
        public uint NumMaterialParameters => MaterialParameterKinds.Length;

        /// <summary>
        /// The source <see cref="_InnerModelForm"/> for this form.
        /// </summary>
        internal readonly _InnerModelForm _inner;

        /// <summary>
        /// The basic <see cref="MaterialTexture2"/>s provided as material instance parameters to <see cref="_inner"/>.
        /// These are also used as sources for <see cref="_textureInsts"/>.
        /// </summary>
        internal readonly Permutation<MaterialTexture2> _baseTextureInsts;

        /// <summary>
        /// The instantiations of <see cref="_baseTextureInsts"/> provided as parameters to <see cref="_inner"/>. The
        /// identifiers for these material instances immediately follow <see cref="_baseTextureInsts"/>.
        /// </summary>
        internal readonly List<_TextureInstance> _textureInsts;

        /// <summary>
        /// The instantiations of material parameters provided as parameters to <see cref="_inner"/>. The identifiers
        /// for these material instances immediately follow <see cref="_textureInsts"/>.
        /// </summary>
        internal readonly List<_MaterialInstance> _matInsts;

        /// <summary>
        /// Describes a "lax" instantiation of a texture defined in <see cref="_baseTextureInsts"/>.
        /// </summary>
        internal struct _TextureInstance
        {
            /// <summary>
            /// The index of the source texture in <see cref="_baseTextureInsts"/> from which this material instance is derived.
            /// </summary>
            public uint TextureIndex;
            
            /// <summary>
            /// The <see cref="TextureLaxity"/> for this material. This must not be <see cref="TextureLaxity.None"/>, since
            /// otherwise the base texture can just be referenced directly.
            /// </summary>
            public TextureLaxity Laxity;
        }

        /// <summary>
        /// Describes an instantiation of a material passed as a parameter to a <see cref="ModelForm"/>.
        /// </summary>
        internal struct _MaterialInstance
        {
            /// <summary>
            /// The index of the material parameter this is an instance of.
            /// </summary>
            public uint SourceIndex;

            /// <summary>
            /// The <see cref="TextureLaxity"/> for this material instance, applied after all other material transforms.
            /// </summary>
            public TextureLaxity Laxity;
        }

        /// <summary>
        /// A <see cref="ModelForm"/> with no visual contents.
        /// </summary>
        public static ModelForm Empty { get; } = new ModelForm(
            List.Empty<MaterialKind>(),
            _InnerModelForm.Empty,
            Permutation.Empty<MaterialTexture2>(),
            List.Empty<_TextureInstance>(),
            List.Empty<_MaterialInstance>());

        /// <summary>
        /// Loads the model asset at the given path.
        /// </summary>
        public static ModelForm FromAsset(Path assetPath)
        {
            return GltfScene.FromAsset(assetPath).Import();
        }

        /// <summary>
        /// Constructs a <see cref="ModelForm"/> based on a <see cref="Mesh"/>.
        /// </summary>
        /// <param name="mats">The materials referenced by <see cref="Mesh"/>.</param>
        public static ModelForm FromMesh(Mesh mesh, SpanList<MaterialUsage> mats)
        {
            Span<DependentTexture> innerInsts = stackalloc DependentTexture[(int)mats.Length];

            // Build texture/material instances
            IndexSet matDepends = mesh.TextureDepends;
            IndexSetBuilder nMatDepends = IndexSetBuilder.CreateEmpty();
            var matParamKinds = ListBuilder<MaterialKind>.CreateEmpty();
            var nBaseTextureInsts = PermutationBuilder<MaterialTexture2>.CreateEmpty();
            var nTextureInsts = ListBuilder<_TextureInstance>.CreateEmpty();
            var nMatInsts = ListBuilder<_MaterialInstance>.CreateEmpty();
            for (uint i = 0; i < mats.Length; i++)
            {
                if (matDepends.Contains(i))
                {
                    MaterialUsage matUsage = mats[i];
                    if (matUsage.Material._ref is null)
                    {
                        // Create material instance and reference that
                        uint matInstIndex = nMatInsts.Append(new _MaterialInstance
                        {
                            SourceIndex = matUsage.Material._paramIndex,
                            Laxity = matUsage.Laxity.Conjugate(matUsage.Material._trans).Upon(matUsage.Kind),
                        });
                        nMatDepends.Include(matUsage.Material._paramIndex);
                        innerInsts[(int)i] = new DependentTexture(
                            0x40000000 | matInstIndex,
                            matUsage.Material._trans.Inverse,
                            matUsage.Material._trans);

                        // Add restriction to incoming parameter
                        while (matParamKinds.Length <= matUsage.Material._paramIndex)
                            matParamKinds.Append(MaterialKind.Any);
                        matParamKinds[matUsage.Material._paramIndex] &= matUsage.Kind;
                    }
                    else
                    {
                        uint baseTextureInstIndex = nBaseTextureInsts.Include(matUsage.Material._ref.Texture);
                        if (matUsage.Kind.RequiresSquare && matUsage.Laxity.IsNone)
                        {
                            // Reference base texture directly
                            innerInsts[(int)i] = new DependentTexture(
                                baseTextureInstIndex,
                                matUsage.Material._ref._localizerRotation.Inverse,
                                matUsage.Material._ref.Localizer);
                        }
                        else
                        {
                            // Create texture instance and reference that
                            uint textureInstIndex = nTextureInsts.Append(new _TextureInstance
                            {
                                TextureIndex = nBaseTextureInsts.Include(matUsage.Material._ref.Texture),
                                Laxity = matUsage.Laxity.Conjugate(matUsage.Material._trans).Upon(matUsage.Kind)
                            });
                            innerInsts[(int)i] = new DependentTexture(
                                0x80000000 | textureInstIndex,
                                matUsage.Material._ref._localizerRotation.Inverse,
                                matUsage.Material._ref.Localizer);
                        }
                    }
                }
            }

            // Correct texture instance references
            uint textureInstOffset = nBaseTextureInsts.Length;
            uint matInstOffset = nBaseTextureInsts.Length + nTextureInsts.Length;
            for (int i = 0; i < innerInsts.Length; i++)
            {
                ref uint sourceIndex = ref innerInsts[i].Index;
                if ((sourceIndex & 0x80000000) != 0)
                    sourceIndex += textureInstOffset;
                if ((sourceIndex & 0x40000000) != 0)
                    sourceIndex += matInstOffset;
                sourceIndex &= 0x0FFFFFFF;
            }

            // Build model
            return new ModelForm(
                matParamKinds.Finish(),
                new _CompoundModelForm(mesh, List.Empty<_InnerModelForm>()),
                nBaseTextureInsts.Finish(),
                nTextureInsts.Finish(),
                nMatInsts.Finish());
        }

        /// <summary>
        /// Constructs an opaque model using the given mesh.
        /// </summary>
        public static ModelForm Solid(MaterialUsage mat, BigList<SolidVertex> verts, BigList<uint> indices)
        {
            // TODO: Delay loading for really big lists
            // TODO: Build mesh directly
            Blend1SolidVertex[] nVertItems = new Blend1SolidVertex[verts.Length];
            using (verts.Read(out IListReader<SolidVertex> vertReader))
            {
                for (uint i = 0; i < verts.Length; i++)
                {
                    var vert = vertReader.ReadItem();
                    nVertItems[i] = new Blend1SolidVertex
                    {
                        Position = vert.Position,
                        Texture = VertexTextureFactor.Build(0, vert.MaterialCoord, vert.Tbn, vert.MaterialAlpha, false)
                    };
                }
            }
            return FromMesh(Mesh.Build(List.Of(nVertItems), (List<uint>)indices), List.Of(mat));
        }

        /// <summary>
        /// Indicates whether this is <see cref="Empty"/>.
        /// </summary>
        public bool IsEmpty => _inner.IsEmpty;

        /// <summary>
        /// Substitutes the parameters of this form with the given arguments.
        /// </summary>
        public Model Apply(SpanList<Material> mats)
        {
            MaterialForm[] matFormItems = new MaterialForm[mats.Length];
            for (uint i = 0; i < mats.Length; i++)
                matFormItems[i] = mats[i];
            return (Model)Apply(List.Empty<MaterialKind>(), List.Of(matFormItems));
        }

        /// <summary>
        /// Substitutes the parameters of this form with the given arguments.
        /// </summary>
        /// <param name="matParamKinds">The <see cref="MaterialKind"/>s of the material parameters referenced by
        /// <paramref name="mats"/>.</param>
        public ModelForm Apply(SpanList<MaterialKind> matParamKinds, SpanList<MaterialForm> mats)
        {
            uint numInnerInsts = _baseTextureInsts.Length + _textureInsts.Length + _matInsts.Length;
            Span<DependentTexture> innerInsts = stackalloc DependentTexture[(int)numInnerInsts];

            // Verify argument kinds
            if (mats.Length < MaterialParameterKinds.Length)
                throw new ArgumentException("Insufficient number of material arguments");
            for (uint i = 0; i < MaterialParameterKinds.Length; i++)
                if (!(mats[i].Kind(matParamKinds) <= MaterialParameterKinds[i]))
                    throw new ArgumentException("Material argument has wrong kind");

            // Retain existing texture instances
            for (uint i = 0; i < _baseTextureInsts.Length; i++)
                innerInsts[(int)i] = DependentTexture.Parameter(i);
            for (uint i = 0; i < _textureInsts.Length; i++)
                innerInsts[(int)(_baseTextureInsts.Length + i)] = DependentTexture.Parameter(0x80000000 | i);

            // Build new texture/material instances
            IndexSetBuilder nMatDepends = IndexSetBuilder.CreateEmpty();
            var nBaseTextureInsts = PermutationBuilder<MaterialTexture2>.Create(_baseTextureInsts);
            var nTextureInsts = ListBuilder<_TextureInstance>.Create(_textureInsts);
            var nMatInsts = ListBuilder<_MaterialInstance>.CreateEmpty();
            for (uint i = 0; i < _matInsts.Length; i++)
            {
                var matInst = _matInsts[i];
                uint innerIndex = _baseTextureInsts.Length + _textureInsts.Length + i;
                MaterialForm matForm = mats[matInst.SourceIndex];
                MaterialKind matKind = matForm.Kind(matParamKinds);
                if (matForm.Is(out Material mat))
                {
                    uint baseTextureInstIndex = nBaseTextureInsts.Include(mat._ref.Texture);
                    if (matKind.RequiresSquare && matInst.Laxity.IsNone)
                    {
                        // Reference base texture directly
                        innerInsts[(int)innerIndex] = new DependentTexture(
                            baseTextureInstIndex,
                            mat._ref._localizerRotation.Inverse,
                            mat._ref.Localizer);
                    }
                    else
                    {
                        // Create texture instance and reference that
                        uint textureInstIndex = nTextureInsts.Append(new _TextureInstance
                        {
                            TextureIndex = nBaseTextureInsts.Include(mat._ref.Texture),
                            Laxity = matInst.Laxity.Conjugate(matForm._trans).Upon(matKind)
                        });
                        innerInsts[(int)innerIndex] = new DependentTexture(
                            0x80000000 | textureInstIndex,
                            mat._ref._localizerRotation.Inverse,
                            mat._ref.Localizer);
                    }
                }
                else
                {
                    // Create material instance and reference that
                    uint matInstIndex = nMatInsts.Append(new _MaterialInstance
                    {
                        SourceIndex = matForm._paramIndex,
                        Laxity = matInst.Laxity.Conjugate(matForm._trans).Upon(matKind),
                    });
                    nMatDepends.Include(matForm._paramIndex);
                    innerInsts[(int)innerIndex] = new DependentTexture(
                        0x40000000 | matInstIndex,
                        matForm._trans.Inverse,
                        matForm._trans);
                }
            }

            // Correct texture instance references
            uint textureInstOffset = nBaseTextureInsts.Length;
            uint matInstOffset = nBaseTextureInsts.Length + nTextureInsts.Length;
            for (uint i = 0; i < numInnerInsts; i++)
            {
                ref uint sourceIndex = ref innerInsts[(int)i].Index;
                if ((sourceIndex & 0x80000000) != 0)
                    sourceIndex += textureInstOffset;
                if ((sourceIndex & 0x40000000) != 0)
                    sourceIndex += matInstOffset;
                sourceIndex &= 0x0FFFFFFF;
            }
            
            // Build model
            return new ModelForm(
                _filterParamKinds(matParamKinds, nMatDepends.Finish()),
                _inner.TransformApply(Motion3.Identity, List.Of(innerInsts)),
                nBaseTextureInsts.Finish(),
                nTextureInsts.Finish(),
                nMatInsts.Finish());
        }
        
        /// <summary>
        /// Filters a parameter list of <see cref="MaterialKind"/>s based on which parameter are actually
        /// referenced.
        /// </summary>
        private static SliceList<MaterialKind> _filterParamKinds(SpanList<MaterialKind> source, IndexSet depends)
        {
            var res = ListBuilder<MaterialKind>.CreateDefault(depends.Bound);
            for (uint i = 0; i < res.Length; i++)
            {
                if (depends.Contains(i))
                    res[i] = source[i];
                else
                    res[i] = MaterialKind.Any;
            }
            return res.Finish();
        }

        /// <summary>
        /// Merges the visual contents of the given models.
        /// </summary>
        public static ModelForm operator +(ModelForm a, ModelForm b)
        {
            if (a.IsEmpty)
                return b;
            if (b.IsEmpty)
                return a;

            // Combine parameter lists
            ListBuilder<MaterialKind> nMatParamKinds;
            if (a.MaterialParameterKinds.Length < b.MaterialParameterKinds.Length)
            {
                nMatParamKinds = ListBuilder<MaterialKind>.CreateDefault(b.MaterialParameterKinds.Length);
                for (uint i = 0; i < a.MaterialParameterKinds.Length; i++)
                    nMatParamKinds[i] = a.MaterialParameterKinds[i] & b.MaterialParameterKinds[i];
                for (uint i = a.MaterialParameterKinds.Length; i < b.MaterialParameterKinds.Length; i++)
                    nMatParamKinds[i] = b.MaterialParameterKinds[i];
            }
            else
            {

                nMatParamKinds = ListBuilder<MaterialKind>.CreateDefault(a.MaterialParameterKinds.Length);
                for (uint i = 0; i < b.MaterialParameterKinds.Length; i++)
                    nMatParamKinds[i] = a.MaterialParameterKinds[i] & b.MaterialParameterKinds[i];
                for (uint i = b.MaterialParameterKinds.Length; i < a.MaterialParameterKinds.Length; i++)
                    nMatParamKinds[i] = a.MaterialParameterKinds[i];
            }

            // Set up material instance parameters for inner forms
            uint aNumInnerInsts = a._baseTextureInsts.Length + a._textureInsts.Length + a._matInsts.Length;
            uint bNumInnerInsts = b._baseTextureInsts.Length + b._textureInsts.Length + b._matInsts.Length;
            Span<DependentTexture> aInsts = stackalloc DependentTexture[(int)aNumInnerInsts];
            Span<DependentTexture> bInsts = stackalloc DependentTexture[(int)bNumInnerInsts];

            // Build new base texture instances
            var nBaseTexturesInstsBuilder = PermutationBuilder<MaterialTexture2>.Create(a._baseTextureInsts);
            for (uint i = 0; i < a._baseTextureInsts.Length; i++)
                aInsts[(int)i] = DependentTexture.Parameter(i);
            for (uint i = 0; i < b._baseTextureInsts.Length; i++)
                bInsts[(int)i] = DependentTexture.Parameter(
                    nBaseTexturesInstsBuilder.Include(b._baseTextureInsts[i]));

            // Build new texture instances
            uint nInstIndex = nBaseTexturesInstsBuilder.Length;
            uint numTextureInsts = a._textureInsts.Length + b._textureInsts.Length;
            var nTextureInstItems = Array.Empty<_TextureInstance>();
            if (numTextureInsts > 0)
            {
                nTextureInstItems = new _TextureInstance[numTextureInsts];
                for (uint i = 0; i < a._textureInsts.Length; i++)
                {
                    nTextureInstItems[i] = a._textureInsts[i];
                    aInsts[(int)(i - a._baseTextureInsts.Length)] = DependentTexture.Parameter(nInstIndex++);
                }
                for (uint i = 0; i < b._textureInsts.Length; i++)
                {
                    var sTextureInst = b._textureInsts[i];
                    sTextureInst.TextureIndex = bInsts[(int)sTextureInst.TextureIndex].Index;
                    nTextureInstItems[a._textureInsts.Length + i] = sTextureInst;
                    bInsts[(int)(i - b._baseTextureInsts.Length)] = DependentTexture.Parameter(nInstIndex++);
                }
            }

            // Build new material instances
            uint numMatInsts = a._matInsts.Length + b._matInsts.Length;
            var nMatInstItems = Array.Empty<_MaterialInstance>();
            if (numMatInsts > 0)
            {
                nMatInstItems = new _MaterialInstance[numMatInsts];
                for (uint i = 0; i < a._matInsts.Length; i++)
                {
                    nMatInstItems[i] = a._matInsts[i];
                    aInsts[(int)(i - a._baseTextureInsts.Length - a._textureInsts.Length)] =
                        DependentTexture.Parameter(nInstIndex++);
                }
                for (uint i = 0; i < b._matInsts.Length; i++)
                {
                    nMatInstItems[a._matInsts.Length + i] = b._matInsts[i];
                    bInsts[(int)(i - b._baseTextureInsts.Length - b._textureInsts.Length)] =
                        DependentTexture.Parameter(nInstIndex++);
                }
            }

            // Combine forms
            var textureInfos = ListBuilder<MeshSolver._TextureInfo>.CreateEmpty();
            throw new NotImplementedException();
            MeshSolver solver = new MeshSolver { _textureInfos = textureInfos };
            _InnerModelForm nForm = _InnerModelForm.MergeApply(solver,
                a._inner, List.Of(aInsts), b._inner, List.Of(bInsts));

            // Finalize model
            // TODO: Incorporate results of solver
            return new ModelForm(
                nMatParamKinds.Finish(), nForm,
                nBaseTexturesInstsBuilder.Finish(),
                new List<_TextureInstance>(nTextureInstItems),
                new List<_MaterialInstance>(nMatInstItems));
        }

        public static implicit operator ModelForm(Model source)
        {
            return new ModelForm(
                List.Empty<MaterialKind>(),
                source._inner,
                source._baseTextureInsts,
                source._textureInsts,
                List.Empty<_MaterialInstance>());
        }

        public static explicit operator Model(ModelForm source)
        {
            if (!source.MaterialParameterKinds.IsEmpty)
                throw new Exception("Model is parameteric");
            Debug.Assert(source._matInsts.IsEmpty);
            return new Model(source._inner, source._baseTextureInsts, source._textureInsts);
        }
    }

    /// <summary>
    /// A <see cref="ModelForm"/> which accepts (and in some cases, requires) <see cref="InstanceMaterial"/>
    /// parameters in addition to the more abstract <see cref="Material"/> parameters.
    /// </summary>
    internal abstract class _InnerModelForm
    {
        /// <summary>
        /// A <see cref="ModelForm"/> with no visual contents.
        /// </summary>
        public static _InnerModelForm Empty => _CompoundModelForm.Empty;

        /// <summary>
        /// Indicates whether this is <see cref="Empty"/>.
        /// </summary>
        public bool IsEmpty => this == Empty;
        
        /// <summary>
        /// Renders the contents of this <see cref="ModelForm"/> using the given arguments.
        /// </summary>
        /// <param name="textureInsts">The basic <see cref="MaterialTexture2"/>s provided as
        /// material instance parameters.</param>
        public abstract void Render(IRenderer renderer, Permutation<MaterialTexture2> textureInsts);
        
        /// <summary>
        /// Applies a spatial transformation to this <see cref="_InnerModelForm"/> while simultaneously
        /// applying a substitution to the texture parameters it references.
        /// </summary>
        public abstract _InnerModelForm TransformApply(Motion3 trans, SpanList<DependentTexture> textures);

        /// <summary>
        /// Combines the visual contents of two <see cref="_InnerModelForm"/>s, substituting parameters in the process.
        /// </summary>
        public static _InnerModelForm MergeApply(
            MeshSolver solver,
            _InnerModelForm a, SpanList<DependentTexture> aTextures,
            _InnerModelForm b, SpanList<DependentTexture> bTextures)
        {
            if (a is _CompoundModelForm aCompound)
            {
                if (b is _CompoundModelForm bCompound)
                {
                    return _CompoundModelForm.MergeApply(solver, aCompound, aTextures, bCompound, bTextures);
                }
            }
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// A <see cref="ModelForm"/> defined by combining the visual effects of several <see cref="ModelForm"/>s
    /// and/or meshes.
    /// </summary>
    internal sealed class _CompoundModelForm : _InnerModelForm
    {
        internal _CompoundModelForm(Mesh mesh, List<_InnerModelForm> comps)
        {
            Mesh = mesh;
            Components = comps;
        }

        /// <summary>
        /// The mesh component of this <see cref="_CompoundModelForm"/>.
        /// </summary>
        public Mesh Mesh { get; }
        
        /// <summary>
        /// The component effects for this <see cref="_CompoundModelForm"/>, excluding <see cref="Mesh"/>.
        /// </summary>
        public List<_InnerModelForm> Components { get; }

        /// <summary>
        /// A <see cref="ModelForm"/> with no visual contents.
        /// </summary>
        public static new _CompoundModelForm Empty { get; } = 
            new _CompoundModelForm(Mesh.Empty, List.Empty<_InnerModelForm>());

        /// <summary>
        /// Combines the visual contents of two <see cref="_CompoundModelForm"/>s, substituting parameters in the process.
        /// </summary>
        public static _InnerModelForm MergeApply(
            MeshSolver solver,
            _CompoundModelForm a, SpanList<DependentTexture> aTextures,
            _CompoundModelForm b, SpanList<DependentTexture> bTextures)
        {
            // Rebuild mesh
            var nMesh = Mesh.MergeApply(solver, a.Mesh, aTextures, b.Mesh, bTextures);

            // Rebuild components
            var nComps = List.Empty<_InnerModelForm>();
            uint numComps = a.Components.Length + b.Components.Length;
            if (numComps > 0)
            {
                throw new NotImplementedException();
            }
            
            // Construct new form
            return new _CompoundModelForm(nMesh, nComps);
        }

        /// <summary>
        /// Merges a group of segment indices from a source list into a target array, rebasing vertex indices in
        /// the process.
        /// </summary>
        internal static void _mergeIndices(
            uint[] targetIndices, ref uint targetHead, ref uint targetBaseVertIndex,
            ref SpanList<uint> sourceIndices, ref uint sourceBaseVertIndex, uint numVerts)
        {
            uint i = 0;
            while (i < sourceIndices.Length)
            {
                uint vertOffset = sourceIndices[i] - sourceBaseVertIndex;
                if (vertOffset >= numVerts)
                    break;
                targetIndices[targetHead++] = targetBaseVertIndex + vertOffset;
                i++;
            }
            sourceIndices = sourceIndices.Slice(i, sourceIndices.Length - i);
            targetBaseVertIndex += numVerts;
            sourceBaseVertIndex += numVerts;
        }

        public override void Render(IRenderer renderer, Permutation<MaterialTexture2> textureInsts)
        {
            // Render mesh
            Mesh.Render(renderer, textureInsts);

            // Render other components
            if (!Components.IsEmpty)
                throw new NotImplementedException();
        }

        public override _InnerModelForm TransformApply(Motion3 trans, SpanList<DependentTexture> textures)
        {

            // Rebuild mesh
            var nMesh = Mesh.TransformApply(trans, textures);

            // Rebuild components
            var nComps = List.Empty<_InnerModelForm>();
            if (!Components.IsEmpty)
            {
                throw new NotImplementedException();
            }

            // Construct new form
            return new _CompoundModelForm(nMesh, nComps);
        }
    }
}