﻿using System;
using System.Threading;
using System.Diagnostics;
using System.Runtime.CompilerServices;

using Alunite.Data;
using Alunite.Data.Serialization;
using Alunite.Resources;

using OpenTK.Graphics.OpenGL4;

#pragma warning disable CS0420 // A reference to a volatile field will not be treated as volatile

namespace Alunite.Graphics.Bindings.OpenGL
{
    /// <summary>
    /// Identifies an OpenGL texture object.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct TextureObj : IFreeable
    {
        public TextureObj(int name)
        {
            Name = name;
        }

        /// <summary>
        /// The OpenGL name for the texture.
        /// </summary>
        public int Name;

        /// <summary>
        /// Indicates that the resource will no longer be used. This must be called on the main thread for
        /// the OpenGL instance.
        /// </summary>
        public void FreeSynced()
        {
            GL.DeleteTexture(Name);
        }

        public static bool operator ==(TextureObj A, TextureObj B)
        {
            return A.Name == B.Name;
        }

        public static bool operator !=(TextureObj A, TextureObj B)
        {
            return A.Name != B.Name;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is TextureObj))
                return false;
            return this == (TextureObj)obj;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
        
        public override string ToString()
        {
            return "Texture(" + Name.ToStringInvariant() + ")";
        }
    }

    /// <summary>
    /// Identifies a method of encoding values of type <typeparamref name="T"/> as pixels in an OpenGL
    /// texture.
    /// </summary>
    public sealed class TextureEncoding<T> : _ITextureEncodingAny
    {
        internal TextureEncoding(params (Encoding<T>, TextureFormat)[] formats)
        {
            var formatsBuilder = new MapBuilder<Encoding<T>, TextureFormat>();
            foreach ((Encoding<T> encoding, TextureFormat format) in formats)
                formatsBuilder.Add(encoding, format);
            Formats = formatsBuilder.Finish();
        }

        /// <summary>
        /// Specifies which <see cref="TextureFormat"/> to use to load a texture of this encoding from
        /// binary pixel data of the given encoding. Note that only a small set of possible binary
        /// encodings will have a compatible OpenGL format.
        /// </summary>
        public Map<Encoding<T>, TextureFormat> Formats { get; }

        /// <summary>
        /// Finds a <see cref="TextureFormat"/> which can be used to load an image of a particular
        /// pixel <see cref="Encoding{T}"/>. This will also apply a conversion to the <see cref="IImageRowReader"/>
        /// if needed.
        /// </summary>
        public TextureFormat Import(uint size_x, ref Encoding<T> encoding, ref IImageRowReader reader)
        {
            if (Formats.TryGet(encoding, out TextureFormat res))
            {
                // Use explicit format
                return res;
            }
            else
            {
                Encoding<T> natural = Encoding.Default<T>(EncodingStrategy.Balanced);
                reader = new MapRowReader<T, T>(x => x, encoding, natural, reader, size_x);
                encoding = natural;
                return Formats[natural];
            }
        }
    }

    /// <summary>
    /// A <see cref="TextureEncoding{T}"/> of some type.
    /// </summary>
    internal interface _ITextureEncodingAny
    {

    }

    /// <summary>
    /// Specifies an OpenGL texture format that can be used for loading binary pixel data.
    /// </summary>
    public struct TextureFormat
    {
        public PixelInternalFormat InternalFormat;
        public PixelFormat LoadFormat;
        public PixelType LoadType;
        public TextureFormat(PixelInternalFormat internalFormat, PixelFormat loadFormat, PixelType loadType)
        {
            InternalFormat = internalFormat;
            LoadFormat = loadFormat;
            LoadType = loadType;
        }
    }

    /// <summary>
    /// Contains functions related to <see cref="TextureEncoding"/>.
    /// </summary>
    public static class TextureEncoding
    {
        /// <summary>
        /// The natural texture encoding for colors.
        /// </summary>
        public static TextureEncoding<Color> Color { get; } = new TextureEncoding<Color>(
            (Graphics.Color.Encoding.B8_G8_R8, new TextureFormat(
                PixelInternalFormat.Rgb8,
                PixelFormat.Bgr, PixelType.UnsignedByte)),
            (Graphics.Color.Encoding.Sb8_Sg8_Sr8, new TextureFormat(
                PixelInternalFormat.Srgb8,
                PixelFormat.Bgr, PixelType.UnsignedByte)));

        /// <summary>
        /// The natural texture encoding for paints, using pre-multiplied RGB components.
        /// </summary>
        public static TextureEncoding<Paint> Paint { get; } = new TextureEncoding<Paint>(
            (Graphics.Paint.Encoding.B8_G8_R8, new TextureFormat(
                PixelInternalFormat.Rgb8,
                PixelFormat.Bgr, PixelType.UnsignedByte)),
            (Graphics.Paint.Encoding.Sb8_Sg8_Sr8, new TextureFormat(
                PixelInternalFormat.Srgb8,
                PixelFormat.Bgr, PixelType.UnsignedByte)));

        /// <summary>
        /// The natural texture encoding for radiances.
        /// </summary>
        public static TextureEncoding<Radiance> Radiance { get; } = new TextureEncoding<Radiance>(
            (Graphics.Radiance.Encoding.Rfn32_Gfn32_Bfn32, new TextureFormat(
                PixelInternalFormat.Rgb32f,
                PixelFormat.Rgb, PixelType.Float)));

        /// <summary>
        /// The natural encoding for <see cref="Data.Scalar"/>s, with the value in the R component.
        /// </summary>
        public static TextureEncoding<Scalar> Scalar { get; } = new TextureEncoding<Scalar>(
            (Data.Scalar.Encoding.U8, new TextureFormat(
                PixelInternalFormat.R8,
                PixelFormat.Red, PixelType.UnsignedByte)),
            (Data.Scalar.Encoding.S8, new TextureFormat(
                PixelInternalFormat.R8Snorm,
                PixelFormat.Red, PixelType.Byte)),
            (Data.Scalar.Encoding.Fn32, new TextureFormat(
                PixelInternalFormat.R32f,
                PixelFormat.Red, PixelType.Float)));

        /// <summary>
        /// The natural texture encoding for <see cref="Data.Vector3"/>s, which encodes X as R, Y as G and Z as B.
        /// </summary>
        public static TextureEncoding<Vector3> Vector3 { get; } = new TextureEncoding<Vector3>(
            (Data.Vector3.Encoding.Zu8_Yu8_Xu8, new TextureFormat(
                PixelInternalFormat.Rgb8,
                PixelFormat.Bgr, PixelType.UnsignedByte)),
            (Data.Vector3.Encoding.Zs8_Ys8_Xs8, new TextureFormat(
                PixelInternalFormat.Rgb8Snorm,
                PixelFormat.Bgr, PixelType.Byte)));
    }
    
    /// <summary>
    /// A <see cref="OpenGL.TextureObj"/> which implements a particular texture, managed by an
    /// <see cref="Instance"/>.
    /// </summary>
    public sealed class TextureInstance : ResourceInstance
    {
        public TextureInstance(Instance owner, TextureObj textureObj)
            : base(owner)
        {
            TextureObj = textureObj;
        }
        
        /// <summary>
        /// The <see cref="OpenGL.TextureObj"/> for this program.
        /// </summary>
        public TextureObj TextureObj { get; }
    }

    partial class Instance
    {
        /// <summary>
        /// Gets a <see cref="TextureObj"/> which implements the given texture.
        /// </summary>
        public IDisposable UseTexture(ColorTexture2 texture, out TextureObj textureObj)
        {
            return _useTexture(TextureEncoding.Color, _colorTexture2Insts, texture, out textureObj);
        }

        /// <summary>
        /// Gets a <see cref="TextureObj"/> which implements the given texture.
        /// </summary>
        public IDisposable UseTexture(PaintTexture2 texture, out TextureObj textureObj)
        {
            return _useTexture(TextureEncoding.Paint, _paintTexture2Insts, texture, out textureObj);
        }

        /// <summary>
        /// Gets a <see cref="TextureObj"/> which implements the given texture.
        /// </summary>
        public IDisposable UseTexture(RadianceTexture2 texture, out TextureObj textureObj)
        {
            return _useTexture(TextureEncoding.Radiance, _radianceTexture2Insts, texture, out textureObj);
        }

        /// <summary>
        /// A registry of <see cref="TextureInstance"/>s that implement a particular
        /// <see cref="ColorTexture2"/>.
        /// </summary>
        private readonly Registry<ColorTexture2, TextureInstance> _colorTexture2Insts
            = Registry<ColorTexture2, TextureInstance>.CreateEmpty();

        /// <summary>
        /// A registry of <see cref="TextureInstance"/>s that implement a particular
        /// <see cref="PaintTexture2"/>.
        /// </summary>
        private readonly Registry<PaintTexture2, TextureInstance> _paintTexture2Insts
            = Registry<PaintTexture2, TextureInstance>.CreateEmpty();

        /// <summary>
        /// A registry of <see cref="TextureInstance"/>s that implement a particular
        /// <see cref="RadianceTexture2"/>.
        /// </summary>
        private readonly Registry<RadianceTexture2, TextureInstance> _radianceTexture2Insts
            = Registry<RadianceTexture2, TextureInstance>.CreateEmpty();

        /// <summary>
        /// Gets a <see cref="TextureObj"/> which implements the given texture.
        /// </summary>
        internal IDisposable _useTexture<TTexture, T>(
            TextureEncoding<T> encoding,
            Registry<TTexture, TextureInstance> registry,
            TTexture texture,
            out TextureObj textureObj)
            where TTexture : ITexture2<T>
        {
            // Check registry for suitable pre-existing texture
            var viewer = registry.View(texture);
            using (var it = viewer.Iterate())
            {
                while (it.TryNext(out TextureInstance textureInst))
                {
                    if (textureInst.TryUse())
                    {
                        textureObj = textureInst.TextureObj;
                        return textureInst;
                    }
                    else
                    {
                        it.Clean();
                    }
                }
            }

            // Check again while the registry is locked
            if (!viewer.TryBrowse(out var browser))
                browser = registry.Browse(texture);
            try
            {
                using (var it = browser.Iterate())
                {
                    while (it.TryNext(out TextureInstance textureInst))
                    {
                        if (textureInst.TryUse())
                        {
                            textureObj = textureInst.TextureObj;
                            return textureInst;
                        }
                        else
                        {
                            it.Clean();
                        }
                    }
                }

                // Create texture instance
                TextureObj localTextureObj = textureObj = _newTexture();
                Queue(() => _loadTextureSynced(localTextureObj, texture.Sampler, encoding, texture.Image));
                var res = new TextureInstance(this, textureObj);
                browser.Add(res);
                return res;
            }
            finally
            {
                // Release lock on registry entry
                browser.Dispose();
            }
        }

        /// <summary>
        /// Creates a new <see cref="TextureObj"/> with no associated data.
        /// </summary>
        internal TextureObj _newTexture()
        {
            // TODO: Fallback if we are not on the main thread
            Debug.Assert(IsSynced);
            return new TextureObj(GL.GenTexture());
        }

        /// <summary>
        /// Loads the image data and parameters for a two-dimensional <see cref="TextureObj"/>.
        /// </summary>
        /// <param name="pixelBuffer">The buffer from which to load image data. Note that the buffer
        /// will be consumed.</param>
        internal void _loadTexture2(
            TextureObj texture, Sampler2 sampler, Index2 size,
            IBufferBuilder pixelBuffer, TextureFormat format)
        {
            if (IsSynced)
                _loadTexture2Synced(texture, sampler, size, pixelBuffer, format);
            else
                Queue(() => _loadTexture2Synced(texture, sampler, size, pixelBuffer, format));
        }

        /// <summary>
        /// Loads the image data and parameters for a two-dimensional <see cref="TextureObj"/>.
        /// </summary>
        /// <param name="pixelBuffer">The buffer from which to load image data. Note that the buffer
        /// will be consumed.</param>
        internal void _loadTexture2Synced(
            TextureObj texture, Sampler2 sampler, Index2 size,
            IBufferBuilder pixelBuffer, TextureFormat format)
        {
            // TODO: Respect sampler parameters
            GL.BindTexture(TextureTarget.Texture2D, texture.Name);
            GL.TexParameter(TextureTarget.Texture2D,
                TextureParameterName.TextureMinFilter,
                (int)TextureMinFilter.LinearMipmapLinear);
            GL.TexParameter(TextureTarget.Texture2D,
                TextureParameterName.TextureMagFilter,
                (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D,
                TextureParameterName.TextureWrapS,
                (int)TextureWrapMode.Repeat);
            GL.TexParameter(TextureTarget.Texture2D,
                TextureParameterName.TextureWrapT,
                (int)TextureWrapMode.Repeat);

            // TODO: Proper support for anisotropic filtering
            GL.TexParameter(TextureTarget.Texture2D, (TextureParameterName)0x84FE, 8.0f);
            /* TODO
            GL.TexImage2D(TextureTarget.Texture2D, 0,
                format.InternalFormat, (int)image.Size.X, (int)image.Size.Y, 0,
                format.LoadFormat, format.LoadType,
                ref Unsafe.AsRef(in buffer[0])); */
            GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);
        }

        /// <summary>
        /// Loads the image data and parameters for an two-dimensional array <see cref="TextureObj"/>.
        /// </summary>
        /// <param name="pixelBuffer">The buffer from which to load image data. Note that the buffer
        /// will be consumed.</param>
        internal void _loadTexture2ArraySynced(
            TextureObj texture, Index3 size,
            IBufferBuilder pixelBuffer, TextureFormat format)
        {
            // TODO: Sampler parameters?
            GL.BindTexture(TextureTarget.Texture2DArray, texture.Name);
            GL.TexParameter(TextureTarget.Texture2DArray,
                TextureParameterName.TextureMinFilter,
                (int)TextureMinFilter.LinearMipmapLinear);
            GL.TexParameter(TextureTarget.Texture2DArray,
                TextureParameterName.TextureMagFilter,
                (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2DArray,
                TextureParameterName.TextureWrapS,
                (int)TextureWrapMode.Repeat);
            GL.TexParameter(TextureTarget.Texture2DArray,
                TextureParameterName.TextureWrapT,
                (int)TextureWrapMode.Repeat);

            // TODO: Proper support for anisotropic filtering
            GL.TexParameter(TextureTarget.Texture2D, (TextureParameterName)0x84FE, 8.0f);
            if (pixelBuffer is MemoryBufferBuilder memoryPixelBuffer)
            {
                GL.TexImage3D(TextureTarget.Texture2DArray, 0,
                    format.InternalFormat, (int)size.X, (int)size.Y, (int)size.Z, 0,
                    format.LoadFormat, format.LoadType, memoryPixelBuffer.Data);
            }
            else
            {
                throw new NotImplementedException();
            }
            GL.GenerateMipmap(GenerateMipmapTarget.Texture2DArray);
        }

        /// <summary>
        /// Loads the image data and parameters for a <see cref="TextureObj"/>.
        /// </summary>
        internal void _loadTextureSynced<T>(
            TextureObj texture, Sampler2 sampler,
            TextureEncoding<T> encoding, BigImage2<T> image)
        {
            // TODO: Respect sampler parameters
            GL.BindTexture(TextureTarget.Texture2D, texture.Name);
            GL.TexParameter(TextureTarget.Texture2D,
                TextureParameterName.TextureMinFilter,
                (int)TextureMinFilter.LinearMipmapLinear);
            GL.TexParameter(TextureTarget.Texture2D,
                TextureParameterName.TextureMagFilter,
                (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D,
                TextureParameterName.TextureWrapS,
                (int)_textureWrapMode(sampler.Wrapping_X));
            GL.TexParameter(TextureTarget.Texture2D,
                TextureParameterName.TextureWrapT,
                (int)_textureWrapMode(sampler.Wrapping_Y));

            // TODO: Proper support for anisotropic filtering
            GL.TexParameter(TextureTarget.Texture2D, (TextureParameterName)0x84FE, 8.0f);
            using (image.Read(out var imageReader))
            {
                Encoding<T> imageEncoding = image.Encoding;
                TextureFormat format = encoding.Import(image.Size.X, ref imageEncoding, ref imageReader);
                uint stride = imageReader.MinBufferSize;
                uint rowLength = stride / imageEncoding.Size.Max;
                Debug.Assert(rowLength * imageEncoding.Size.Max == stride);
                GL.PixelStore(PixelStoreParameter.UnpackRowLength, rowLength);
                ReadOnlySpan<byte> buffer = imageReader.ReadBlock(image.Size.Y, stride);
                GL.TexImage2D(TextureTarget.Texture2D, 0,
                    format.InternalFormat, (int)image.Size.X, (int)image.Size.Y, 0,
                    format.LoadFormat, format.LoadType,
                    ref Unsafe.AsRef(in buffer[0]));
                GL.PixelStore(PixelStoreParameter.UnpackRowLength, 0);
            }
            GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);
        }

        /// <summary>
        /// Gets the <see cref="TextureWrapMode"/> corresponding to the given <see cref="SamplerWrapping"/>.
        /// </summary>
        private static TextureWrapMode _textureWrapMode(SamplerWrapping wrapping)
        {
            return wrapping == SamplerWrapping.Clamp ? TextureWrapMode.ClampToEdge : TextureWrapMode.Repeat;
        }
    }
}
