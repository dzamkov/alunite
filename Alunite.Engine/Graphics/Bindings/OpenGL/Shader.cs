﻿using System;
using System.Diagnostics;

using Alunite.Data;
using Alunite.Resources;

using OpenTK.Graphics.OpenGL4;

namespace Alunite.Graphics.Bindings.OpenGL
{
    /// <summary>
    /// Identifies an OpenGL shader object.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct ShaderObj : IFreeable
    {
        public ShaderObj(int name)
        {
            Name = name;
        }

        /// <summary>
        /// The OpenGL name for the program.
        /// </summary>
        public int Name;

        /// <summary>
        /// Indicates that the resource will no longer be used. This must be called on the main thread for
        /// the OpenGL instance.
        /// </summary>
        public void FreeSynced()
        {
            GL.DeleteProgram(Name);
        }

        public override string ToString()
        {
            return "Shader(" + Name.ToStringInvariant() + ")";
        }
    }

    /// <summary>
    /// Identifies an OpenGL shader program object.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct ProgramObj : IFreeable
    {
        public ProgramObj(int name)
        {
            Name = name;
        }

        /// <summary>
        /// The OpenGL name for the program.
        /// </summary>
        public int Name;

        /// <summary>
        /// Indicates that the resource will no longer be used. This must be called on the main thread for
        /// the OpenGL instance.
        /// </summary>
        public void FreeSynced()
        {
            GL.DeleteProgram(Name);
        }

        public override string ToString()
        {
            return "Program(" + Name.ToStringInvariant() + ")";
        }
    }

    /// <summary>
    /// A helper class for defining a <see cref="ProgramObj"/>, without require syncing with the graphics
    /// thread.
    /// </summary>
    public sealed class ProgramBuilder
    {
        private MapBuilder<string, GlslMacroInfo> _macros;
        private BagBuilder<_ShaderInfo> _shaderInfos;

        /// <summary>
        /// Describes a shader that was attached to this <see cref="ProgramBuilder"/>.
        /// </summary>
        private struct _ShaderInfo
        {
            public ShaderType Type;
            public string Source;
            public _ShaderInfo(ShaderType type, string source)
            {
                Type = type;
                Source = source;
            }
        }

        /// <summary>
        /// Creates a new <see cref="ProgramBuilder"/>.
        /// </summary>
        public static ProgramBuilder Create()
        {
            return new ProgramBuilder
            {
                _macros = MapBuilder<string, GlslMacroInfo>.CreateEmpty(),
                _shaderInfos = BagBuilder<_ShaderInfo>.CreateEmpty()
            };
        }

        /// <summary>
        /// Defines a preprocessor macro with the given name that will be applied to all following shaders
        /// attached using <see cref="Attach"/>.
        /// </summary>
        public void Define(string name, GlslMacroInfo def)
        {
            _macros.Add(name, def);
        }

        /// <summary>
        /// Shortcut for <see cref="Define"/> with <see cref="GlslMacroInfo.Empty"/>.
        /// </summary>
        public void Define(string name)
        {
            _macros.Add(name, GlslMacroInfo.Empty);
        }

        /// <summary>
        /// The repository in which static embedded GLSL files are stored.
        /// </summary>
        public static RepositoryPath StaticRepository { get; } =
            Repository.Manifest(typeof(Instance).Assembly)
            / nameof(Alunite) / nameof(Graphics) / nameof(Bindings) / "Glsl";

        /// <summary>
        /// Attaches the given file to this progrma as a shader of the given type. The file path is given relative to
        /// <see cref="StaticRepository"/>.
        /// </summary>
        public void AttachStatic(ShaderType type, Path path)
        {
            Attach(type, StaticRepository / path);
        }

        /// <summary>
        /// Attaches the given file to this program as a shader of the given type.
        /// </summary>
        public void Attach(ShaderType type, RepositoryPathViewer path)
        {
            if (path.TryResolveAsFile(out BigBinary file))
            {
                StringBuilder sourceBuilder = StringBuilder.CreateEmpty();
                GlslPreprocessor processor = GlslPreprocessor.Create(_macros);
                processor.ProcessFile(sourceBuilder, path, file);
                string source = sourceBuilder.Finish();
                _shaderInfos.Add(new _ShaderInfo(type, source));
            }
            else
            {
                throw new Exception("File not found");
            }
        }

        /// <summary>
        /// Loads the content of this <see cref="ProgramBuilder"/> into a <see cref="ProgramObj"/>. This must be called
        /// on the main thread for the OpenGL instance.
        /// </summary>
        public void FinishSynced(ProgramObj progObj)
        {
            int status; string err;
            int shaderIndex = 0;
            int[] shaders = new int[_shaderInfos.Size];
            foreach (_ShaderInfo shaderInfo in _shaderInfos)
            {
                int shader = GL.CreateShader(shaderInfo.Type);
                shaders[shaderIndex++] = shader;
                GL.ShaderSource(shader, shaderInfo.Source);
                GL.CompileShader(shader);
                GL.GetShader(shader, ShaderParameter.CompileStatus, out status);
                if (status == 0)
                {
                    err = GL.GetShaderInfoLog(shader);
                    if (!string.IsNullOrEmpty(err))
                        throw new Exception(err); // TODO: Better exception type
                }
                GL.AttachShader(progObj.Name, shader);
            }
            GL.LinkProgram(progObj.Name);
            GL.GetProgram(progObj.Name, GetProgramParameterName.LinkStatus, out status);
            if (status == 0)
            {
                err = GL.GetProgramInfoLog(progObj.Name);
                if (!string.IsNullOrEmpty(err))
                    throw new Exception(err); // TODO: Better exception type
            }
            for (int i = 0; i < shaders.Length; i++)
                GL.DeleteShader(shaders[i]);
        }
    }
    
    /// <summary>
    /// Identifies the "Draw2" program.
    /// </summary>
    public sealed class Draw2Program : Resource<Draw2ProgramInstance>
    {
        private Draw2Program() { }

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        public static Draw2Program Instance { get; } = new Draw2Program();

        protected internal override Draw2ProgramInstance Instantiate(Instance owner)
        {
            ProgramBuilder prog = ProgramBuilder.Create();
            prog.AttachStatic(ShaderType.VertexShader, Path.Base / "Draw2.vert.glsl");
            prog.AttachStatic(ShaderType.FragmentShader, Path.Base / "Draw2.frag.glsl");

            ProgramObj progObj = owner._newProgram();
            var progInst = new Draw2ProgramInstance(owner, progObj);
            owner.Queue(delegate
            {
                prog.FinishSynced(progObj);
                progInst.TargetSizeUniformIndex = GL.GetUniformLocation(progObj.Name, "targetSize");
                progInst.ImageUniformIndex = GL.GetUniformLocation(progObj.Name, "image");
            });
            owner.Flush();
            return progInst;
        }
    }

    /// <summary>
    /// A <see cref="ProgramInstance"/> for a <see cref="Draw2Program"/>.
    /// </summary>
    public sealed class Draw2ProgramInstance : ProgramInstance
    {
        public int TargetSizeUniformIndex { get; internal set; }
        public int ImageUniformIndex { get; internal set; }
        public Draw2ProgramInstance(Instance owner, ProgramObj progObj)
            : base(owner, progObj)
        { }
    }
    
    /// <summary>
    /// Identifies the "Draw3" program.
    /// </summary>
    public sealed class Draw3Program : Resource<Draw3ProgramInstance>
    {
        private Draw3Program() { }

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        public static Draw3Program Instance { get; } = new Draw3Program();

        protected internal override Draw3ProgramInstance Instantiate(Instance owner)
        {
            ProgramBuilder prog = ProgramBuilder.Create();
            prog.AttachStatic(ShaderType.VertexShader, Path.Base / "Draw3.vert.glsl");
            prog.AttachStatic(ShaderType.FragmentShader, Path.Base / "Draw3.frag.glsl");

            ProgramObj progObj = owner._newProgram();
            var progInst = new Draw3ProgramInstance(owner, progObj);
            owner.Queue(delegate
            {
                prog.FinishSynced(progObj);
                progInst.WorldToDeviceUniformIndex = GL.GetUniformLocation(progObj.Name, "worldToDevice");
                progInst.ImageUniformIndex = GL.GetUniformLocation(progObj.Name, "image");
            });
            owner.Flush();
            return progInst;
        }
    }
    
    /// <summary>
    /// A <see cref="ProgramInstance"/> for a <see cref="Draw3Program"/>.
    /// </summary>
    public sealed class Draw3ProgramInstance : ProgramInstance
    {
        public int WorldToDeviceUniformIndex { get; internal set; }
        public int ImageUniformIndex { get; internal set; }
        public Draw3ProgramInstance(Instance owner, ProgramObj progObj)
            : base(owner, progObj)
        { }
    }

    /// <summary>
    /// Identifies the "RenderMesh" program.
    /// </summary>
    public sealed class RenderMeshProgram : Resource<RenderMeshProgramInstance>
    {
        public RenderMeshProgram(uint numBlendFactors)
        {
            NumBlendFactors = numBlendFactors;
        }

        /// <summary>
        /// The number of blend factors per vertex.
        /// </summary>
        public uint NumBlendFactors { get; }

        protected internal override RenderMeshProgramInstance Instantiate(Instance owner)
        {
            ProgramBuilder prog = ProgramBuilder.Create();
            prog.Define("NUM_BLEND_FACTORS", NumBlendFactors.ToStringInvariant());
            prog.AttachStatic(ShaderType.VertexShader, Path.Base / "RenderMesh.vert.glsl");
            prog.AttachStatic(ShaderType.FragmentShader, Path.Base / "RenderMesh.frag.glsl");

            ProgramObj progObj = owner._newProgram();
            var progInst = new RenderMeshProgramInstance(owner, progObj);
            owner.Queue(delegate
            {
                prog.FinishSynced(progObj);
                progInst.WorldToDeviceUniformIndex = GL.GetUniformLocation(progObj.Name, "worldToDevice");
                progInst.EyeBiasUniformIndex = GL.GetUniformLocation(progObj.Name, "eyeBias");
                progInst.BrdfTextureUniformIndex = GL.GetUniformLocation(progObj.Name, "brdfTexture");
                progInst.SkyboxDiffuseTextureUniformIndex = GL.GetUniformLocation(progObj.Name, "skyboxDiffuseTexture");
                progInst.SkyboxSpecularTextureUniformIndex = GL.GetUniformLocation(progObj.Name, "skyboxSpecularTexture");
                progInst.SkyboxSpecularNumLevelsUniformIndex = GL.GetUniformLocation(progObj.Name, "skyboxSpecularNumLevels");
                progInst.AlbedoTextureUniformIndex = GL.GetUniformLocation(progObj.Name, "albedoTexture");
                progInst.NormalTextureUniformIndex = GL.GetUniformLocation(progObj.Name, "normalTexture");
                progInst.MetalnessTextureUniformIndex = GL.GetUniformLocation(progObj.Name, "metalnessTexture");
                progInst.RoughnessTextureUniformIndex = GL.GetUniformLocation(progObj.Name, "roughnessTexture");
            });
            owner.Flush();
            return progInst;
        }
    }

    /// <summary>
    /// A <see cref="ProgramInstance"/> for a <see cref="RenderMeshProgram"/>.
    /// </summary>
    public sealed class RenderMeshProgramInstance : ProgramInstance
    {
        public int WorldToDeviceUniformIndex { get; internal set; }
        public int EyeBiasUniformIndex { get; internal set; }
        public int BrdfTextureUniformIndex { get; internal set; }
        public int SkyboxDiffuseTextureUniformIndex { get; internal set; }
        public int SkyboxSpecularTextureUniformIndex { get; internal set; }
        public int SkyboxSpecularNumLevelsUniformIndex { get; internal set; }
        public int AlbedoTextureUniformIndex { get; internal set; }
        public int NormalTextureUniformIndex { get; internal set; }
        public int MetalnessTextureUniformIndex { get; internal set; }
        public int RoughnessTextureUniformIndex { get; internal set; }
        public RenderMeshProgramInstance(Instance owner, ProgramObj progObj)
            : base(owner, progObj)
        { }
    }
    
    /// <summary>
    /// A lookup texture used for evaluating a general BRDF.
    /// </summary>
    public sealed class BrdfTexture : Resource<TextureInstance>
    {
        /// <summary>
        /// The only instance of this class.
        /// </summary>
        public static BrdfTexture Instance { get; } = new BrdfTexture();

        protected internal override TextureInstance Instantiate(Instance owner)
        {
            TextureObj textureObj = owner._newTexture();
            ProgramBuilder precomputeProg = ProgramBuilder.Create();
            precomputeProg.AttachStatic(ShaderType.VertexShader, Path.Base / "PrecomputeBrdf.vert.glsl");
            precomputeProg.AttachStatic(ShaderType.FragmentShader, Path.Base / "PrecomputeBrdf.frag.glsl");
            owner.Queue(delegate
            {
                ProgramObj precomputeProgObj = owner._newProgram();
                precomputeProg.FinishSynced(precomputeProgObj);

                // Allocate lookup texture
                const int thetaSize = 256;
                const int roughnessSize = 256;
                GL.BindTexture(TextureTarget.Texture2D, textureObj.Name);
                GL.TexImage2D(
                    TextureTarget.Texture2D,
                    0, PixelInternalFormat.Rgb16f, thetaSize, roughnessSize, 0, PixelFormat.Rgb,
                    PixelType.Float, IntPtr.Zero);
                GL.TexParameter(TextureTarget.Texture2D,
                    TextureParameterName.TextureMinFilter,
                    (int)TextureMinFilter.Linear);
                GL.TexParameter(TextureTarget.Texture2D,
                    TextureParameterName.TextureMagFilter,
                    (int)TextureMagFilter.Linear);
                GL.TexParameter(TextureTarget.Texture2D,
                    TextureParameterName.TextureWrapS,
                    (int)TextureWrapMode.ClampToEdge);
                GL.TexParameter(TextureTarget.Texture2D,
                    TextureParameterName.TextureWrapT,
                    (int)TextureWrapMode.ClampToEdge);

                // Set up precompute program
                using (var quadMesh = owner.Use(Mesh.StandardQuad))
                {
                    GL.BindVertexArray(quadMesh.VertexArrayObj.Name);
                    GL.UseProgram(precomputeProgObj.Name);

                    // Precompute lookup texture
                    int fbo = GL.GenFramebuffer();
                    GL.Viewport(0, 0, thetaSize, roughnessSize);
                    GL.BindFramebuffer(FramebufferTarget.Framebuffer, fbo);
                    for (uint i = 0; i < 6; i++)
                    {
                        GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0,
                            TextureTarget.Texture2D, textureObj.Name, 0);
                        GL.DrawElements(BeginMode.Triangles, (int)quadMesh.NumIndices, DrawElementsType.UnsignedInt, 0);
                    }
                    GL.DeleteFramebuffer(fbo);
                }
                precomputeProgObj.FreeSynced();
            });
            return new TextureInstance(owner, textureObj);
        }
    }

    /// <summary>
    /// A <see cref="OpenGL.ProgramObj"/> which implements a particular shader program, managed by an
    /// <see cref="Instance"/>.
    /// </summary>
    public class ProgramInstance : ResourceInstance
    {
        public ProgramInstance(Instance owner, ProgramObj progObj)
            : base(owner)
        {
            ProgramObj = progObj;
        }

        /// <summary>
        /// The <see cref="OpenGL.ProgramObj"/> for this program.
        /// </summary>
        public ProgramObj ProgramObj { get; }
    }

    partial class Instance
    {
        /// <summary>
        /// Creates a new <see cref="ProgramObj"/> with no associated data.
        /// </summary>
        internal ProgramObj _newProgram()
        {
            // TODO: Fallback if we are not on the main thread
            if (!IsSynced)
                throw new NotImplementedException();
            return new ProgramObj(GL.CreateProgram());
        }
    }
}