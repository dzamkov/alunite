﻿using System;

using Alunite.Data;
using Alunite.Reactive;
using Alunite.Resources;

using OpenTK.Graphics.OpenGL4;

namespace Alunite.Graphics.Bindings.OpenGL
{
    /// <summary>
    /// Manages the resources for an OpenGL instance.
    /// </summary>
    public sealed partial class Instance : IDisposable
    {
        /// <summary>
        /// Creates a <see cref="DynamicDrawer2"/> which uses this instance to build a <see cref="DynamicDrawJob"/>.
        /// </summary>
        /// <param name="size">The size of the render target.</param>
        /// <param name="color">The color to initially clear the render target to.</param>
        public DynamicDrawer2 CreateDrawer(Observer obs, Vector2i size, Color color)
        {
            DynamicDrawJobBuilder job = DynamicDrawJobBuilder.Create(this);
            job.DrawProcedure.Begin(DrawOperationType.Enable_ConstEnableCap);
            job.DrawProcedure.Append(EnableCap.FramebufferSrgb);
            job.DrawProcedure.Begin(DrawOperationType.Enable_ConstEnableCap);
            job.DrawProcedure.Append(EnableCap.CullFace);
            job.DrawProcedure.Begin(DrawOperationType.ClearColor_ConstSingle_ConstSingle_ConstSingle_ConstSingle);
            job.DrawProcedure.Append((float)color.R);
            job.DrawProcedure.Append((float)color.G);
            job.DrawProcedure.Append((float)color.B);
            job.DrawProcedure.Append(0f);
            job.DrawProcedure.Begin(DrawOperationType.Clear_ConstClearBufferMask);
            job.DrawProcedure.Append(ClearBufferMask.ColorBufferBit);
            return new DynamicDrawer2(job, obs, size);
        }

        /// <summary>
        /// Releases all resources maintained by the OpenGL instance.
        /// </summary>
        public void Dispose()
        {
            // TODO
        }

        /// <summary>
        /// Indicates if the current thread is the main thread for this OpenGL instance.
        /// </summary>
        public bool IsSynced => true;
        
        /// <summary>
        /// Queues the given action on the main thread for this OpenGL instance.
        /// </summary>
        public void Queue(Action action)
        {
            if (IsSynced)
                action();
            else
                throw new NotImplementedException();
        }

        /// <summary>
        /// Frees the given <see cref="IFreeable"/> on the main thread for this OpenGL instance.
        /// </summary>
        public void Free(IFreeable freeable)
        {
            if (IsSynced)
                freeable.FreeSynced();
            else
                throw new NotImplementedException();
        }

        /// <summary>
        /// Blocks until all actions queued with <see cref="Queue"/> have been executed.
        /// </summary>
        public void Flush()
        {
            // TODO
        }
    }

    /// <summary>
    /// A graphics resource which is managed by an <see cref="Instance"/>.
    /// </summary>
    public interface IFreeable
    {
        /// <summary>
        /// Indicates that the resource will no longer be used. This must be called on the main thread for
        /// the OpenGL instance.
        /// </summary>
        void FreeSynced();
    }

    /// <summary>
    /// An <see cref="IDisposable"/> wrapper over an <see cref="IFreeable"/> which automatically handles thread
    /// synchronization for freeing.
    /// </summary>
    public sealed class FreeDisposable<T> : IDisposable
        where T : IFreeable
    {
        public FreeDisposable(Instance inst, T target)
        {
            Instance = inst;
            Target = target;
        }

        /// <summary>
        /// The instance that manages <see cref="Target"/>.
        /// </summary>
        public Instance Instance { get; }

        /// <summary>
        /// The target of this <see cref="FreeDisposable{T}"/>.
        /// </summary>
        public T Target { get; }

        /// <summary>
        /// Indicates that <see cref="Target"/> will no longer be used.
        /// </summary>
        public void Dispose()
        {
            Instance.Free(Target);
        }
    }

    /// <summary>
    /// Contains helper functions related to <see cref="IFreeable"/>.
    /// </summary>
    public static class Freeable
    {
        /// <summary>
        /// Adds an <see cref="IDisposable"/> resource which frees the given <see cref="IFreeable"/> to the given
        /// bag of resources.
        /// </summary>
        public static void AddFree<T>(this BagBuilder<IDisposable> resources, Instance inst, T target)
            where T : IFreeable
        {
            resources.Add(new FreeDisposable<T>(inst, target));
        }
    }
}
