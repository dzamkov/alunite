﻿using System;
using System.Threading;
using System.Diagnostics;
using System.Runtime.CompilerServices;

using Alunite.Data;
using Alunite.Data.Serialization;

using OpenTK.Graphics.OpenGL4;
using System.Runtime.InteropServices;

namespace Alunite.Graphics.Bindings.OpenGL
{
    /// <summary>
    /// Identifies an OpenGL vertex array object.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct VertexArrayObj : IFreeable
    {
        public VertexArrayObj(int name)
        {
            Name = name;
        }

        /// <summary>
        /// The OpenGL name for the vertex array.
        /// </summary>
        public int Name;

        /// <summary>
        /// Indicates that the resource will no longer be used. This must be called on the main thread for
        /// the OpenGL instance.
        /// </summary>
        public void FreeSynced()
        {
            GL.DeleteVertexArray(Name);
        }

        public override string ToString()
        {
            return "VertexArray(" + Name.ToStringInvariant() + ")";
        }
    }

    /// <summary>
    /// Identifies a region within a <see cref="VertexArrayObj"/>.
    /// </summary>
    public struct VertexArrayRegion
    {
        public VertexArrayRegion(VertexArrayObj source, uint indexOffset, uint numIndices)
        {
            Source = source;
            IndexOffset = indexOffset;
            NumIndices = numIndices;
        }

        /// <summary>
        /// The <see cref="VertexArrayObj"/> for this region.
        /// </summary>
        public VertexArrayObj Source { get; }

        /// <summary>
        /// The offset of the first index for this span in the index buffer of <see cref="Source"/>.
        /// </summary>
        public uint IndexOffset { get; }

        /// <summary>
        /// The number of indices in the index buffer of <see cref="Source"/> associated with this span.
        /// </summary>
        public uint NumIndices { get; }
    }

    /// <summary>
    /// Identifies and describes a method of encoding vertex data in a binary buffer. Note that all formats
    /// use a single interleaved buffer.
    /// </summary>
    public sealed class VertexFormat
    {
        internal VertexFormat(uint size, List<VertexAttributeFormat> attrs)
        {
            Size = size;
            Attributes = attrs;
        }

        internal VertexFormat(uint size, params VertexAttributeFormat[] attrs)
        {
            Size = size;
            Attributes = new List<VertexAttributeFormat>(attrs);
        }

        /// <summary>
        /// The number of bytes needed to encode a vertex using this format.
        /// </summary>
        public uint Size { get; }

        /// <summary>
        /// The attributes that make up the vertex.
        /// </summary>
        public List<VertexAttributeFormat> Attributes { get; }

        /// <summary>
        /// The <see cref="VertexFormat"/> for a <see cref="Data.Vector2"/> vertex.
        /// </summary>
        public static VertexFormat Vector2 { get; } = new VertexFormat(8,
            new VertexAttributeFormat(0, 2, VertexAttribPointerType.Float));

        /// <summary>
        /// The <see cref="VertexFormat"/> for a <see cref="Data.Vector3"/> vertex.
        /// </summary>
        public static VertexFormat Vector3 { get; } = new VertexFormat(12,
            new VertexAttributeFormat(0, 3, VertexAttribPointerType.Float));

        /// <summary>
        /// The <see cref="VertexFormat"/> for <see cref="Graphics.TextureVertex2"/>.
        /// </summary>
        public static VertexFormat TextureVertex2 { get; } = new VertexFormat(16,
            new VertexAttributeFormat(0, 2, VertexAttribPointerType.Float),
            new VertexAttributeFormat(8, 2, VertexAttribPointerType.Float));

        /// <summary>
        /// The <see cref="VertexFormat"/> for <see cref="Graphics.TextureVertex3"/>.
        /// </summary>
        public static VertexFormat TextureVertex3 { get; } = new VertexFormat(20,
            new VertexAttributeFormat(0, 3, VertexAttribPointerType.Float),
            new VertexAttributeFormat(12, 2, VertexAttribPointerType.Float));

        /// <summary>
        /// The <see cref="VertexFormat"/> for <see cref="Graphics.Blend1SolidVertex"/>.
        /// </summary>
        public static VertexFormat Blend1SolidVertex { get; } = new VertexFormat(44,
            new VertexAttributeFormat(0, 3, VertexAttribPointerType.Float),
            new VertexAttributeFormat(12, 4, VertexAttribPointerType.Float),
            new VertexAttributeFormat(28, 4, VertexAttribPointerType.Float));

        /// <summary>
        /// The <see cref="VertexFormat"/> for <see cref="Graphics.Blend2SolidVertex"/>.
        /// </summary>
        public static VertexFormat Blend2SolidVertex { get; } = new VertexFormat(76,
            new VertexAttributeFormat(0, 3, VertexAttribPointerType.Float),
            new VertexAttributeFormat(12, 4, VertexAttribPointerType.Float),
            new VertexAttributeFormat(44, 4, VertexAttribPointerType.Float),
            new VertexAttributeFormat(28, 4, VertexAttribPointerType.Float),
            new VertexAttributeFormat(60, 4, VertexAttribPointerType.Float));

        /// <summary>
        /// The <see cref="VertexFormat"/> for <see cref="Graphics.Blend4SolidVertex"/>.
        /// </summary>
        public static VertexFormat Blend4SolidVertex { get; } = new VertexFormat(140,
            new VertexAttributeFormat(0, 3, VertexAttribPointerType.Float),
            new VertexAttributeFormat(12, 4, VertexAttribPointerType.Float),
            new VertexAttributeFormat(44, 4, VertexAttribPointerType.Float),
            new VertexAttributeFormat(76, 4, VertexAttribPointerType.Float),
            new VertexAttributeFormat(108, 4, VertexAttribPointerType.Float),
            new VertexAttributeFormat(28, 4, VertexAttribPointerType.Float),
            new VertexAttributeFormat(60, 4, VertexAttribPointerType.Float),
            new VertexAttributeFormat(92, 4, VertexAttribPointerType.Float),
            new VertexAttributeFormat(124, 4, VertexAttribPointerType.Float));
        
        /// <summary>
        /// Sets the currently-bound <see cref="VertexArrayObj"/> to use this format. This must be called on
        /// the main thread for the OpenGL instance.
        /// </summary>
        public void SetupSynced()
        {
            for (uint i = 0; i < Attributes.Length; i++)
            {
                VertexAttributeFormat attr = Attributes[i];
                GL.EnableVertexAttribArray(i);
                if (attr.IsIntegral)
                {
                    GL.VertexAttribIPointer(i,
                        (int)attr.NumComponents, (VertexAttribIntegerType)attr.ComponentType,
                        (int)Size, (IntPtr)attr.Offset);
                }
                else
                {
                    GL.VertexAttribPointer(i,
                        (int)attr.NumComponents, attr.ComponentType,
                        true, (int)Size, (int)attr.Offset);
                }
            }
        }
    }

    /// <summary>
    /// Describes how a particular attribute is encoded within a <see cref="VertexFormat"/>.
    /// </summary>
    public struct VertexAttributeFormat
    {
        public VertexAttributeFormat(uint offset, uint numComps, VertexAttribPointerType compType)
        {
            Offset = offset;
            NumComponents = numComps;
            IsIntegral = false;
            ComponentType = compType;
        }

        public VertexAttributeFormat(uint offset, uint numComps, VertexAttribIPointerType compType)
        {
            Offset = offset;
            NumComponents = numComps;
            IsIntegral = true;
            ComponentType = (VertexAttribPointerType)compType;
        }
        
        /// <summary>
        /// The byte offset of the beginning of this attribute from the beginning of the data for a vertex.
        /// </summary>
        public uint Offset;

        /// <summary>
        /// The number of components for this attribute.
        /// </summary>
        public uint NumComponents;

        /// <summary>
        /// Indicates whether this attribute's components should be interpreted as integral.
        /// </summary>
        public bool IsIntegral;

        /// <summary>
        /// The types of components for this attribute.
        /// </summary>
        public VertexAttribPointerType ComponentType;
    }

    /// <summary>
    /// Identifies and describes a method of encoding index data in a binary buffer.
    /// </summary>
    public sealed class IndexFormat
    {
        private IndexFormat(uint size, DrawElementsType drawType)
        {
            Size = size;
            DrawType = drawType;
        }

        /// <summary>
        /// The size, in bytes, of an index stored in this format.
        /// </summary>
        public uint Size { get; }

        /// <summary>
        /// The <see cref="DrawElementsType"/> of this format.
        /// </summary>
        public DrawElementsType DrawType { get; }

        /// <summary>
        /// The index format corresponding to <see cref="byte"/>.
        /// </summary>
        public static IndexFormat Byte { get; } = new IndexFormat(1, DrawElementsType.UnsignedByte);

        /// <summary>
        /// The index format corresponding to <see cref="ushort"/>.
        /// </summary>
        public static IndexFormat UInt16 { get; } = new IndexFormat(2, DrawElementsType.UnsignedShort);
        
        /// <summary>
        /// The index format corresponding to <see cref="uint"/>.
        /// </summary>
        public static IndexFormat UInt32 { get; } = new IndexFormat(4, DrawElementsType.UnsignedInt);
    }

    /// <summary>
    /// Describes how vertex data is encoded in a <see cref="VertexBufferObj"/> and <see cref="IndexBufferObj"/> pair.
    /// </summary>
    public struct VertexArrayFormat
    {
        public VertexArrayFormat(VertexFormat vertex, IndexFormat index)
        {
            Vertex = vertex;
            Index = index;
        }

        /// <summary>
        /// The format for the vertices stored in the <see cref="VertexBufferObj"/>.
        /// </summary>
        public VertexFormat Vertex;

        /// <summary>
        /// The type of the indices stored in the <see cref="IndexBufferObj"/>.
        /// </summary>
        public IndexFormat Index;
    }

    /// <summary>
    /// An interface for writing data to a vertex array.
    /// </summary>
    public interface IVertexArrayWriter
    {
        /// <summary>
        /// The format of data accepted by this <see cref="IVertexArrayWriter"/>.
        /// </summary>
        VertexArrayFormat Format { get; }

        /// <summary>
        /// A buffer containing the next few bytes of vertex data to be written to the vertex array. The size
        /// of this buffer is arbitrary and variable, however, <see cref="TryPrepare"/> can be used allocate up
        /// to a given minimum size.
        /// </summary>
        Span<byte> VertexBuffer { get; }

        /// <summary>
        /// A buffer containing the next few bytes of index data to be written to the index array. The size
        /// of this buffer is arbitrary and variable, however, <see cref="TryPrepare"/> can be used allocate up
        /// to a given minimum size.
        /// </summary>
        Span<byte> IndexBuffer { get; }

        /// <summary>
        /// The index of the first vertex in <see cref="VertexBuffer"/>.
        /// </summary>
        uint BaseVertexIndex { get; }

        /// <summary>
        /// Removes the given number of bytes from the beginning of <see cref="VertexBuffer"/> and
        /// <see cref="IndexBuffer"/> and commits them to the vertex array.
        /// </summary>
        /// <param name="sizeVertices">The number of bytes to flush in <see cref="VertexBuffer"/>. This must
        /// be a multiple of the vertex size.</param>
        /// <param name="sizeIndices">The number of bytes to flush in <see cref="IndexBuffer"/>. This must
        /// be a multiple of the index size.</param>
        void Flush(uint sizeVertices, uint sizeIndices);

        /// <summary>
        /// Ensures there is enough capacity for the given number of bytes in <see cref="VertexBuffer"/> and
        /// <see cref="IndexBuffer"/>. If this can not be achieved by expanding the buffers, this will
        /// return false.
        /// </summary>
        /// <param name="sizeVertices">The number of bytes required in <see cref="VertexBuffer"/>. This must be
        /// a multiple of the vertex size.</param>
        /// <param name="sizeIndices">The number of bytes required in <see cref="IndexBuffer"/>. This must be
        /// a multiple of the index size.</param>
        bool TryPrepare(uint sizeVertices, uint sizeIndices);
    }

    /// <summary>
    /// Contains helper functions related to <see cref="IVertexArrayWriter"/>.
    /// </summary>
    public static class VertexArrayWriter
    {
        /// <summary>
        /// Attempts to write a mesh to an <see cref="IVertexArrayWriter"/>. Returns false if the mesh is too large.
        /// </summary>
        public static unsafe bool TryWrite<T>(this IVertexArrayWriter writer, SpanList<T> verts, SpanList<uint> indices)
            where T : unmanaged
        {
            VertexArrayFormat format = writer.Format;
            if (format.Vertex.Size != sizeof(T))
                throw new ArgumentException("Vertex format mismatch");
            uint sizeVertices = format.Vertex.Size * verts.Length;
            uint sizeIndices = format.Index.Size * indices.Length;
            if (writer.TryPrepare(sizeVertices, sizeIndices))
            {
                // Write vertices
                Span<byte> vertexBuffer = writer.VertexBuffer;
                ReadOnlySpan<byte> vertData = MemoryMarshal.AsBytes<T>(verts);
                Binary.Copy(vertData, vertexBuffer.Slice(0, vertData.Length));

                // Write indices
                Span<byte> indexBuffer = writer.IndexBuffer;
                _writeIndices(ref indexBuffer, format.Index, writer.BaseVertexIndex, indices);

                // Flush written data
                writer.Flush(sizeVertices, sizeIndices);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Writes an array of indices to the given index buffer using the given format.
        /// </summary>
        /// <param name="baseVertexIndex">An offset added to all indices when writing to the buffer.</param>
        public static void _writeIndices(
            ref Span<byte> indexBuffer, IndexFormat format,
            uint baseVertexIndex, ReadOnlySpan<uint> indices)
        {
            if (format == IndexFormat.UInt32)
            {
                // TODO: Don't do a bounds check for each index
                for (int i = 0; i < indices.Length; i++)
                    Binary.WriteAlignedNativeUInt32(ref indexBuffer, baseVertexIndex + indices[i]);
            }
            else if (format == IndexFormat.UInt16)
            {
                throw new NotImplementedException();
            }
            else if (format == IndexFormat.Byte)
            {
                throw new NotImplementedException();
            }
            else
            {
                throw new NotSupportedException();
            }
        }
    }

    /// <summary>
    /// An <see cref="IVertexArrayWriter"/> which is used to create a vertex array.
    /// </summary>
    public interface IVertexArrayBuilder : IVertexArrayWriter
    {
        /// <summary>
        /// Gets a <see cref="VertexArrayRegion"/> for the vertex data supplied to this writer.
        /// </summary>
        /// <param name="resources">The resources for the context in which the vertex array region will be
        /// used.</param>
        VertexArrayRegion UseFinish(BagBuilder<IDisposable> resources);
    }

    /// <summary>
    /// A <see cref="IVertexArrayBuilder"/> which works by temporarily storing vertex data in an in-memory buffer
    /// and transferring it to GL buffers once all writing is complete.
    /// </summary>
    public sealed class MemoryVertexArrayBuilder : IVertexArrayWriter, IVertexArrayBuilder
    {
        private byte[] _vertexData;
        private byte[] _indexData;
        private uint _vertexHead;
        private uint _indexHead;
        private MemoryVertexArrayBuilder(Instance inst, VertexArrayFormat format)
        {
            Instance = inst;
            Format = format;
            _vertexData = new byte[128];
            _indexData = new byte[128];
            _vertexHead = 0;
            _indexHead = 0;
        }

        /// <summary>
        /// Creates a new <see cref="MemoryVertexArrayBuilder"/>.
        /// </summary>
        public static MemoryVertexArrayBuilder Create(Instance inst, VertexArrayFormat format)
        {
            return new MemoryVertexArrayBuilder(inst, format);
        }

        /// <summary>
        /// The <see cref="OpenGL.Instance"/> the resulting buffer will be created in.
        /// </summary>
        public Instance Instance { get; }

        /// <summary>
        /// The format of data accepted by this <see cref="IVertexArrayWriter"/>.
        /// </summary>
        public VertexArrayFormat Format { get; }

        /// <summary>
        /// A buffer containing the next few bytes of vertex data to be written to the vertex array. The size
        /// of this buffer is arbitrary and variable, however, <see cref="TryPrepare"/> can be used allocate up
        /// to a given minimum size.
        /// </summary>
        public Span<byte> VertexBuffer => _vertexData.AsSpan((int)_vertexHead);

        /// <summary>
        /// A buffer containing the next few bytes of index data to be written to the index array. The size
        /// of this buffer is arbitrary and variable, however, <see cref="TryPrepare"/> can be used allocate up
        /// to a given minimum size.
        /// </summary>
        public Span<byte> IndexBuffer => _indexData.AsSpan((int)_indexHead);

        /// <summary>
        /// The index of the first vertex in <see cref="VertexBuffer"/>.
        /// </summary>
        public uint BaseVertexIndex => _vertexHead / Format.Vertex.Size;

        /// <summary>
        /// Removes the given number of bytes from the beginning of <see cref="VertexBuffer"/> and
        /// <see cref="IndexBuffer"/> and commits them to the vertex array.
        /// </summary>
        /// <param name="sizeVertices">The number of bytes to flush in <see cref="VertexBuffer"/>. This must
        /// be a multiple of the vertex size.</param>
        /// <param name="sizeIndices">The number of bytes to flush in <see cref="IndexBuffer"/>. This must
        /// be a multiple of the index size.</param>
        public void Flush(uint sizeVertices, uint sizeIndices)
        {
            _vertexHead += sizeVertices;
            _indexHead += sizeIndices;
        }

        /// <summary>
        /// Ensures there is enough capacity for the given number byte in <see cref="VertexBuffer"/> and
        /// <see cref="IndexBuffer"/>. If this can not be achieved by expanding the buffers, this will
        /// return false.
        /// </summary>
        /// <param name="sizeVertices">The number of bytes required in <see cref="VertexBuffer"/>. This must be
        /// a multiple of the vertex size.</param>
        /// <param name="sizeIndices">The number of bytes required in <see cref="IndexBuffer"/>. This must be
        /// a multiple of the index size.</param>
        public bool TryPrepare(uint sizeVertices, uint sizeIndices)
        {
            if ((_vertexData.Length - _vertexHead) < sizeVertices)
            {
                uint nVertexDataSize = Bitwise.UpperPow2(_vertexHead + sizeVertices);
                byte[] nVertexData = new byte[nVertexDataSize];
                Binary.Copy(_vertexData.AsSpan(0, (int)_vertexHead), nVertexData.AsSpan(0, (int)_vertexHead));
                _vertexData = nVertexData;
            }
            if ((_indexData.Length - _indexHead) < sizeIndices)
            {
                uint nIndexDataSize = Bitwise.UpperPow2(_indexHead + sizeIndices);
                byte[] nIndexData = new byte[nIndexDataSize];
                Binary.Copy(_indexData.AsSpan(0, (int)_indexHead), nIndexData.AsSpan(0, (int)_indexHead));
                _indexData = nIndexData;
            }
            return true;
        }

        /// <summary>
        /// Gets a <see cref="VertexArrayRegion"/> for the vertex data supplied to this writer.
        /// </summary>
        public VertexArrayRegion UseFinish(BagBuilder<IDisposable> resources)
        {
            BufferObj vbo = Instance._newBuffer();
            BufferObj ibo = Instance._newBuffer();
            VertexArrayObj vertexArray = Instance._newVertexArray();
            Instance.Queue(delegate
            {
                GL.BindVertexArray(vertexArray.Name);
                GL.BindBuffer(BufferTarget.ArrayBuffer, vbo.Name);
                GL.BufferData(BufferTarget.ArrayBuffer,
                    (int)_vertexHead, ref _vertexData[0],
                    BufferUsageHint.StaticDraw);
                GL.BindBuffer(BufferTarget.ElementArrayBuffer, ibo.Name);
                GL.BufferData(BufferTarget.ElementArrayBuffer,
                    (int)_indexHead, ref _indexData[0],
                    BufferUsageHint.StaticDraw);
                Format.Vertex.SetupSynced();
            });
            resources.AddFree(Instance, vertexArray);
            resources.AddFree(Instance, vbo);
            resources.AddFree(Instance, ibo);
            return new VertexArrayRegion(vertexArray, 0, _indexHead / Format.Index.Size);
        }
    }

    partial class Instance
    {
        /// <summary>
        /// Creates a <see cref="IVertexArrayBuilder"/> to build a vertex array with the given vertex format.
        /// </summary>
        public IVertexArrayBuilder BuildVertexArray(VertexFormat format)
        {
            // TODO: Look into using persistently-mapped buffers to avoid copys
            return MemoryVertexArrayBuilder.Create(this, new VertexArrayFormat(format, IndexFormat.UInt32));
        }

        /// <summary>
        /// Creates a new <see cref="BufferObj"/> with no associated data.
        /// </summary>
        internal VertexArrayObj _newVertexArray()
        {
            // TODO: Fallback if we are not on the main thread
            Debug.Assert(IsSynced);
            return new VertexArrayObj(GL.GenVertexArray());
        }
    }
}