﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

using Alunite.Data;

using OpenTK.Graphics.OpenGL4;

namespace Alunite.Graphics.Bindings.OpenGL
{
    /// <summary>
    /// Describes an instantiation of a mesh resource owned by an <see cref="Instance"/>.
    /// </summary>
    public sealed class MeshInstance : ResourceInstance
    {
        public MeshInstance(Instance owner, VertexArrayObj vertexArrayObj, uint numIndices)
            : base(owner)
        {
            VertexArrayObj = vertexArrayObj;
            NumIndices = numIndices;
        }

        /// <summary>
        /// The <see cref="OpenGL.VertexArrayObj"/> for this mesh.
        /// </summary>
        public VertexArrayObj VertexArrayObj { get; }

        /// <summary>
        /// The number of indices in the index buffer for <see cref="VertexArrayObj"/>.
        /// </summary>
        public uint NumIndices { get; }

        /// <summary>
        /// The <see cref="OpenGL.VertexArrayRegion"/> for this mesh.
        /// </summary>
        public VertexArrayRegion VertexArrayRegion => new VertexArrayRegion(VertexArrayObj, 0, NumIndices);
    }

    /// <summary>
    /// A <see cref="Resource"/> that describes a mesh. Also contains various reusable meshes.
    /// </summary>
    public abstract class Mesh : Resource<MeshInstance>
    {
        public Mesh(VertexFormat vertFormat, List<uint> indices)
        {
            VertexFormat = vertFormat;
            Indices = indices;
        }

        /// <summary>
        /// The <see cref="OpenGL.VertexFormat"/> for this mesh.
        /// </summary>
        public VertexFormat VertexFormat { get; }

        /// <summary>
        /// The indices for this mesh.
        /// </summary>
        public List<uint> Indices { get; }
        
        /// <summary>
        /// The vertex data for this mesh.
        /// </summary>
        public abstract ReadOnlySpan<byte> VertexData { get; }

        protected internal sealed override MeshInstance Instantiate(Instance owner)
        {
            VertexArrayObj vertexArrayObj = owner._newVertexArray();
            owner.Queue(delegate
            {
                var vertData = VertexData;
                BufferObj vbo = owner._newBuffer();
                BufferObj ibo = owner._newBuffer();
                GL.BindVertexArray(vertexArrayObj.Name);
                GL.BindBuffer(BufferTarget.ArrayBuffer, vbo.Name);
                GL.BufferData(BufferTarget.ArrayBuffer,
                    vertData.Length, ref Unsafe.AsRef(in vertData[0]),
                    BufferUsageHint.StaticDraw);
                GL.BindBuffer(BufferTarget.ElementArrayBuffer, ibo.Name);
                GL.BufferData(BufferTarget.ElementArrayBuffer,
                    sizeof(uint) * (int)Indices.Length, Indices._items,
                    BufferUsageHint.StaticDraw);
                VertexFormat.SetupSynced();
            });
            return new MeshInstance(owner, vertexArrayObj, Indices.Length);
        }

        /// <summary>
        /// A triangular <see cref="Mesh"/> for a quad that fully covers ([-1, 1], [-1, 1]).
        /// </summary>
        public static Mesh StandardQuad { get; } = new Mesh<Vector2>(
            VertexFormat.Vector2,
            new Vector2[]
            {
                new Vector2(-1, -1),
                new Vector2(1, -1),
                new Vector2(-1, 1),
                new Vector2(1, 1)
            },
            new uint[]
            {
                0, 1, 2,
                2, 1, 3
            });

        /// <summary>
        /// A triangular <see cref="Mesh"/> for an inward-facing cube within the bound
        /// ([-1, 1], [-1, 1], [-1, 1]).
        /// </summary>
        public static Mesh StandardInwardCube { get; } = new Mesh<Vector3>(
            VertexFormat.Vector3,
            new Vector3[]
            {
                new Vector3(-1, -1, -1),
                new Vector3(1, -1, -1),
                new Vector3(-1, 1, -1),
                new Vector3(1, 1, -1),
                new Vector3(-1, -1, 1),
                new Vector3(1, -1, 1),
                new Vector3(-1, 1, 1),
                new Vector3(1, 1, 1),
            },
            new uint[]
            {
                1, 3, 0,
                3, 2, 0,
                7, 3, 1,
                1, 5, 7,
                7, 5, 4,
                4, 6, 7,
                7, 2, 3,
                6, 2, 7,
                2, 6, 0,
                6, 4, 0,
                5, 1, 0,
                4, 5, 0
            });
    }

    /// <summary>
    /// A <see cref="Mesh"/> where each vertex is described using a value of type <typeparamref name="TVertex"/>.
    /// </summary>
    public sealed class Mesh<TVertex> : Mesh
        where TVertex : unmanaged
    {
        public Mesh(VertexFormat vertFormat, List<TVertex> verts, List<uint> indices)
            : base(vertFormat, indices)
        {
            Vertices = verts;
        }

        internal Mesh(VertexFormat vertFormat, TVertex[] verts, uint[] indices)
            : this(vertFormat, new List<TVertex>(verts), new List<uint>(indices))
        { }

        /// <summary>
        /// The vertices for this mesh.
        /// </summary>
        public List<TVertex> Vertices { get; }

        public sealed override ReadOnlySpan<byte> VertexData => MemoryMarshal.Cast<TVertex, byte>(Vertices._items);
    }
}
