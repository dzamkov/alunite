﻿using System;
using System.Threading;
using System.Diagnostics;

using Alunite.Data;
using Alunite.Data.Serialization;
using Alunite.Resources;
using OpenTK.Graphics.OpenGL4;

namespace Alunite.Graphics.Bindings.OpenGL
{
    /// <summary>
    /// Specifies where in a texture atlas a particular texture occurs. This is independent of the image size
    /// of the atlas.
    /// </summary>
    public struct AtlasSlot
    {
        internal AtlasSlot(uint index, byte invScale_x, byte invScale_y, uint offset_x, uint offset_y)
        {
            Index = index;
            InverseScale_X = invScale_x;
            InverseScale_Y = invScale_y;
            Offset_X = offset_x;
            Offset_Y = offset_y;
        }

        /// <summary>
        /// The index of the atlas layer this slot is in.
        /// </summary>
        public uint Index { get; }
        
        /// <summary>
        /// The log base-2 of the size of the atlas in relation to the size of the slot along the X axis. For textures
        /// that wrap around the X axis, this must be 0.
        /// </summary>
        public byte InverseScale_X { get; }
        
        /// <summary>
        /// The log base-2 of the size of the atlas in relation to the size of the slot along the Y axis. For textures
        /// that wrap around the Y axis, this must be 0.
        /// </summary>
        public byte InverseScale_Y { get; }

        /// <summary>
        /// The offset of the slot in the atlas along the X axis, in units of slot's length.
        /// </summary>
        public uint Offset_X { get; }

        /// <summary>
        /// The offset of the slot in the atlas along the Y axis, in units of slot's length.
        /// </summary>
        public uint Offset_Y { get; }
    }

    /// <summary>
    /// A set of two-dimensional texture arrays that implement a set of <see cref="MaterialTexture2"/>s, managed by
    /// an <see cref="OpenGL.Instance"/>.
    /// </summary>
    internal sealed class _MaterialAtlasInstance : IDisposable, ITransient
    {
        internal _MaterialAtlasInstance(Instance owner, 
            TextureObj albedo,
            TextureObj normal,
            TextureObj metalness,
            TextureObj roughness)
        {
            Owner = owner;
            AlbedoTexture = albedo;
            NormalTexture = normal;
            MetalnessTexture = metalness;
            RoughnessTexture = roughness;
        }

        /// <summary>
        /// The <see cref="OpenGL.Instance"/> owning this <see cref="_MaterialAtlasInstance"/>.
        /// </summary>
        public Instance Owner { get; }

        /// <summary>
        /// The albedo texture array for this atlas.
        /// </summary>
        public TextureObj AlbedoTexture { get; }

        /// <summary>
        /// The normal texture array for this atlas.
        /// </summary>
        public TextureObj NormalTexture { get; }

        /// <summary>
        /// The metalness texture array for this atlas.
        /// </summary>
        public TextureObj MetalnessTexture { get; }

        /// <summary>
        /// The roughness texture array for this atlas.
        /// </summary>
        public TextureObj RoughnessTexture { get; }

        /// <summary>
        /// Indicates whether this <see cref="_MaterialAtlasInstance"/> can accept additional users. Note that
        /// this does not indicate whether the resource currently has users. Once this becomes <see cref="false"/>,
        /// it will remain so forever.
        /// </summary>
        public bool IsAlive => true;

        /// <summary>
        /// Attempts to add a usage to the shared resource, returning false if this is not possible. If
        /// this returns true, <see cref="Dispose"/> must be called one extra time.
        /// </summary>
        public bool TryUse()
        {
            return true;
        }

        /// <summary>
        /// Removes a user from the atlas.
        /// </summary>
        public void Dispose()
        {
            // TODO
        }
    }

    /// <summary>
    /// Identifies a texture within an <see cref="_MaterialAtlasInstance"/>. This also tracks the usage of the
    /// texture.
    /// </summary>
    internal sealed class _MaterialAtlasTextureInstance : IDisposable, ITransient
    {
        internal _MaterialAtlasTextureInstance(_MaterialAtlasInstance atlas, AtlasSlot slot)
        {
            Atlas = atlas;
            Slot = slot;
        }

        /// <summary>
        /// The atlas in which this <see cref="_MaterialAtlasTextureInstance"/> is defined.
        /// </summary>
        public _MaterialAtlasInstance Atlas { get; }

        /// <summary>
        /// The slot for this <see cref="_MaterialAtlasTextureInstance"/> within <see cref="Atlas"/>.
        /// </summary>
        public AtlasSlot Slot { get; }

        /// <summary>
        /// Indicates whether this texture can accept additional users.
        /// </summary>
        public bool IsAlive => Atlas.IsAlive;

        /// <summary>
        /// Adds an additional usage to this texture. This may only be called if the calling thread has
        /// a usage on <see cref="Atlas"/>.
        /// </summary>
        public void Use()
        {
            // TODO
        }

        /// <summary>
        /// Removes a user from the member.
        /// </summary>
        public void Dispose()
        {
            // TODO
        }
    }

    partial class Instance
    {
        /// <summary>
        /// Gets a <see cref="TextureObj"/>s for a set of texture arrays that contain all of the given material textures.
        /// </summary>
        /// <param name="resources">The resources for the context in which the texture will be used.</param>
        /// <param name="slots">Describes where each input material occurs in the resulting atlas.</param>
        public void UseMaterialTextureAtlas(
            BagBuilder<IDisposable> resources,
            Permutation<MaterialTexture2> textures,
            Span<AtlasSlot> slots,
            out TextureObj albedo,
            out TextureObj normal,
            out TextureObj metalness,
            out TextureObj roughness)
        {
            // Quickly check registry for suitable pre-existing atlas. Do this by selecting a "pivot" texture which
            // appears in a small number of atlases, and then check if any of those atlases contain all of the textures.
            _MaterialAtlasInstance atlas;
            SetBuilder<_MaterialAtlasInstance> attempted = SetBuilder<_MaterialAtlasInstance>.CreateEmpty();
            uint pivotIndex = 0;
            var pivotIterator = _materialTextureInsts.Iterate(textures[(uint)pivotIndex]);
            uint numRemPivotInsts = uint.MaxValue;
            while (pivotIterator.TryNext(out _MaterialAtlasTextureInstance pivotInst))
            {
                atlas = pivotInst.Atlas;

                // Check whether atlas is useable
                if (attempted.Contains(atlas))
                    continue;
                if (!atlas.TryUse())
                {
                    pivotIterator.Clean();
                    continue;
                }

                // Check whether all other textures appear in atlas
                uint resStartIndex = resources._size;
                for (uint i = 0; i < textures.Length; i++)
                {
                    if (i == pivotIndex)
                    {
                        pivotInst.Use();
                        resources.Add(pivotInst);
                        slots[(int)i] = pivotInst.Slot;
                    }
                    else
                    {
                        // Check whether this texture appears in atlas
                        var otherInsts = _materialTextureInsts.View(textures[i]);
                        var otherIterator = otherInsts.Iterate();
                        uint numOtherInsts = 0;
                        while (otherIterator.TryNext(out _MaterialAtlasTextureInstance otherInst))
                        {
                            if (otherInst.Atlas == atlas)
                            {
                                otherInst.Use();
                                resources.Add(otherInst);
                                slots[(int)i] = otherInst.Slot;
                                goto foundAtlas;
                            }
                            numOtherInsts++;
                        }

                        // If not, is this texture a better choice for a pivot?
                        if (numOtherInsts < numRemPivotInsts)
                        {
                            pivotIndex = i;
                            pivotIterator = otherInsts.Iterate();
                            numRemPivotInsts = numOtherInsts;
                        }
                        goto nextAtlas;

                    foundAtlas:;
                    }
                }

                // Yep, this atlas works
                albedo = atlas.AlbedoTexture;
                normal = atlas.NormalTexture;
                metalness = atlas.MetalnessTexture;
                roughness = atlas.RoughnessTexture;
                atlas.Dispose();
                return;

            nextAtlas:
                // Release resources we've acquired related to this atlas
                for (uint i = resStartIndex; i < resources._size; i++)
                    resources._buffer[i].Dispose();
                resources._size = resStartIndex;
                atlas.Dispose();

                // Mark the atlas as already attempted
                attempted.Include(atlas);
            }

            // TODO: More thorough registry check with locking
            // TODO: Syncing with Instance

            // Pack images into slots
            // TODO: Pack together materials of different sizes
            Index3 albedoSize = new Index3(textures[0].AlbedoImage.Size, textures.Length);
            Index3 normalSize = new Index3(textures[0].NormalImage.Size, textures.Length);
            Index3 metalnessSize = new Index3(textures[0].MetalnessImage.Size, textures.Length);
            Index3 roughnessSize = new Index3(textures[0].RoughnessImage.Size, textures.Length);
            for (uint i = 0; i < textures.Length; i++)
                slots[(int)i] = new AtlasSlot(i, 0, 0, 0, 0);

            // TODO: Search for best format and encoding taking all images into account.
            Encoding<Paint> albedoEncoding = textures[0].AlbedoImage.Encoding;
            Encoding<Vector3> normalEncoding = textures[0].NormalImage.Encoding;
            Encoding<Scalar> metalnessEncoding = textures[0].MetalnessImage.Encoding;
            Encoding<Scalar> roughnessEncoding = textures[0].RoughnessImage.Encoding;

            bool hasFormat = TextureEncoding.Paint.Formats.TryGet(albedoEncoding, out TextureFormat albedoFormat);
            hasFormat &= TextureEncoding.Vector3.Formats.TryGet(normalEncoding, out TextureFormat normalFormat);
            hasFormat &= TextureEncoding.Scalar.Formats.TryGet(metalnessEncoding, out TextureFormat metalnessFormat);
            hasFormat &= TextureEncoding.Scalar.Formats.TryGet(roughnessEncoding, out TextureFormat roughnessFormat);
            Debug.Assert(hasFormat);

            // Build albedo atlas texture
            BigImage2<Paint>[] albedoImages = new BigImage2<Paint>[textures.Length];
            for (uint i = 0; i < textures.Length; i++)
                albedoImages[i] = textures[i].AlbedoImage;
            IBufferBuilder albedoBuffer = _buildAtlasPixelUnpackBuffer(
                albedoSize, albedoEncoding, List.Of(albedoImages), List.Of(slots));
            albedo = _newTexture();
            _loadTexture2ArraySynced(albedo, albedoSize, albedoBuffer, albedoFormat);

            // Build normal atlas texture
            BigImage2<Vector3>[] normalImages = new BigImage2<Vector3>[textures.Length];
            for (uint i = 0; i < textures.Length; i++)
                normalImages[i] = textures[i].NormalImage;
            IBufferBuilder normalBuffer = _buildAtlasPixelUnpackBuffer(
                normalSize, normalEncoding, List.Of(normalImages), List.Of(slots));
            normal = _newTexture();
            _loadTexture2ArraySynced(normal, normalSize, normalBuffer, normalFormat);

            // Build metalness atlas texture
            BigImage2<Scalar>[] metalnessImages = new BigImage2<Scalar>[textures.Length];
            for (uint i = 0; i < textures.Length; i++)
                metalnessImages[i] = textures[i].MetalnessImage;
            IBufferBuilder metalnessBuffer = _buildAtlasPixelUnpackBuffer(
                metalnessSize, metalnessEncoding, List.Of(metalnessImages), List.Of(slots));
            metalness = _newTexture();
            _loadTexture2ArraySynced(metalness, metalnessSize, metalnessBuffer, metalnessFormat);

            // Build roughness atlas texture
            BigImage2<Scalar>[] roughnessImages = new BigImage2<Scalar>[textures.Length];
            for (uint i = 0; i < textures.Length; i++)
                roughnessImages[i] = textures[i].RoughnessImage;
            IBufferBuilder roughnessBuffer = _buildAtlasPixelUnpackBuffer(
                roughnessSize, roughnessEncoding, List.Of(roughnessImages), List.Of(slots));
            roughness = _newTexture();
            _loadTexture2ArraySynced(roughness, roughnessSize, roughnessBuffer, roughnessFormat);

            // Create material texture atlas for tracking and caching
            atlas = new _MaterialAtlasInstance(this, albedo, normal, metalness, roughness);
            for (uint i = 0; i < textures.Length; i++)
            {
                var inst = new _MaterialAtlasTextureInstance(atlas, slots[(int)i]);
                _materialTextureInsts.Add(textures[i], inst);
                resources.Add(inst);
            }
        }
        
        /// <summary>
        /// A registry of <see cref="_MaterialAtlasTextureInstance"/>s that implement a particular
        /// <see cref="MaterialTexture2"/>.
        /// </summary>
        private readonly Registry<MaterialTexture2, _MaterialAtlasTextureInstance> _materialTextureInsts
            = Registry<MaterialTexture2, _MaterialAtlasTextureInstance>.CreateEmpty();

        /// <summary>
        /// Creates an <see cref="IBufferBuilder"/> for a pixel unpack buffer that contains the image data for a
        /// texture atlas of the given images. This will upsample images as needed.
        /// </summary>
        /// <param name="size">The size of each layer in the atlas.</param>
        /// <param name="encoding">The desired encoding of the buffer.</param>
        /// <param name="slots">The atlas slots corresponding to each image in <paramref name="slots"/>.</param>
        private IBufferBuilder _buildAtlasPixelUnpackBuffer<T>(
            Index3 size, Encoding<T> encoding,
            SpanList<BigImage2<T>> images,
            SpanList<AtlasSlot> slots)
        {
            // Allocate buffer
            Debug.Assert(encoding.Size.Multiplicity == 0);
            Debug.Assert(encoding.Align.Offset == 0);
            uint stride = Bitwise.AlignUp(size.X * encoding.Size.Min, 4);
            ulong len = stride * size.Y * size.Z;
            IBufferBuilder buffer = BuildPixelUnpackBuffer(len);

            // Write image data to buffer
            Debug.Assert(images.Length == slots.Length);
            for (uint i = 0; i < images.Length; i++)
            {
                BigImage2<T> image = images[i];
                AtlasSlot slot = slots[i];
                uint imageSize_x = size.X >> slot.InverseScale_X;
                uint imageSize_y = size.Y >> slot.InverseScale_Y;
                ulong offset = encoding.Size.Min * slot.Offset_X * imageSize_x +
                    stride * (slot.Offset_Y * imageSize_y + size.Y * slot.Index);
                uint stretch_x = imageSize_x / image.Size.X;
                uint stretch_y = imageSize_y / image.Size.Y;
                Debug.Assert(image.Size.X * stretch_x == imageSize_x);
                Debug.Assert(image.Size.Y * stretch_y == imageSize_y);
                using (image.Read(out IImageRowReader reader))
                {
                    if (stretch_x != 1 || stretch_y != 1)
                        throw new NotImplementedException();

                    // TODO: Unaligned row reading
                    Debug.Assert(reader.MinBufferSize <= stride);
                    Debug.Assert(Bitwise.AlignUp(stride, encoding.Align.Period) == stride);
                    Debug.Assert(Bitwise.AlignUp(offset, encoding.Align.Period) == offset);
                    for (uint j = 0; j < imageSize_y; j++)
                    {
                        reader.ReadRow(buffer.Data.Slice((int)offset));
                        offset += stride;
                    }
                }
            }
            return buffer;
        }
    }
}
