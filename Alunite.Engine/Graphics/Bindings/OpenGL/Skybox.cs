﻿using System;

using Alunite.Data;
using Alunite.Data.Geometry;
using Alunite.Resources;

using OpenTK.Graphics.OpenGL4;

namespace Alunite.Graphics.Bindings.OpenGL
{
    /// <summary>
    /// Wraps a <see cref="Skybox"/> as a <see cref="Resource"/>.
    /// </summary>
    public sealed class SkyboxResource : Resource<SkyboxInstance>
    {
        public SkyboxResource(BigImage2<Radiance> image)
        {
            Image = image;
        }

        /// <summary>
        /// The radiance image for the skybox, using an equirectangular projection.
        /// </summary>
        public BigImage2<Radiance> Image { get; }

        protected internal override SkyboxInstance Instantiate(Instance owner)
        {
            TextureObj diffuse = owner._newTexture();
            TextureObj specular = owner._newTexture();
            const int diffuseSize = 32;
            int specularSize = (int)Bitwise.UpperPow2(Image.Size.X / 4);
            int specularNumLevels = (int)Bitwise.Log2((uint)specularSize);
            owner.Queue(delegate
            {
                var skyboxTexture = new RadianceTexture2(
                    new Sampler2(SamplerWrapping.Repeat, SamplerWrapping.Clamp, SamplerFiltering.Linear),
                    Image);
                using (var diffuseProgInst = owner.Use(PrecomputeSkyboxProgram.Diffuse))
                using (var specularProgInst = owner.Use(PrecomputeSkyboxProgram.Specular))
                using (owner.UseTexture(skyboxTexture, out TextureObj skyboxTextureObj))
                using (var quadMesh = owner.Use(Mesh.StandardQuad))
                {
                    // Allocate diffuse cubemap
                    GL.BindTexture(TextureTarget.TextureCubeMap, diffuse.Name);
                    for (uint i = 0; i < 6; i++)
                        GL.TexImage2D(
                            (TextureTarget)((uint)TextureTarget.TextureCubeMapPositiveX + i),
                            0, PixelInternalFormat.Rgb16f, diffuseSize, diffuseSize, 0, PixelFormat.Rgb,
                            PixelType.Float, IntPtr.Zero);
                    GL.TexParameter(TextureTarget.TextureCubeMap,
                        TextureParameterName.TextureMinFilter,
                        (int)TextureMinFilter.Linear);
                    GL.TexParameter(TextureTarget.TextureCubeMap,
                        TextureParameterName.TextureMagFilter,
                        (int)TextureMagFilter.Linear);
                    GL.TexParameter(TextureTarget.TextureCubeMap,
                        TextureParameterName.TextureWrapS,
                        (int)TextureWrapMode.ClampToEdge);
                    GL.TexParameter(TextureTarget.TextureCubeMap,
                        TextureParameterName.TextureWrapT,
                        (int)TextureWrapMode.ClampToEdge);
                    GL.TexParameter(TextureTarget.TextureCubeMap,
                        TextureParameterName.TextureWrapR,
                        (int)TextureWrapMode.ClampToEdge);

                    // Set up precompute program
                    GL.BindVertexArray(quadMesh.VertexArrayObj.Name);
                    GL.UseProgram(diffuseProgInst.ProgramObj.Name);
                    GL.Uniform1(diffuseProgInst.EquirectangularSkyboxTextureUniformIndex, 0);
                    GL.ActiveTexture(TextureUnit.Texture0);
                    GL.BindTexture(TextureTarget.Texture2D, skyboxTextureObj.Name);

                    // Precompute diffuse cubemap
                    int fbo = GL.GenFramebuffer();
                    GL.Viewport(0, 0, diffuseSize, diffuseSize);
                    GL.BindFramebuffer(FramebufferTarget.Framebuffer, fbo);
                    for (uint i = 0; i < 6; i++)
                    {
                        GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0,
                            (TextureTarget)((uint)TextureTarget.TextureCubeMapPositiveX + i), diffuse.Name, 0);
                        unsafe
                        {
                            Matrix3x3 trans = _cubemapTrans[i];
                            GL.ProgramUniformMatrix3(
                                diffuseProgInst.ProgramObj.Name,
                                diffuseProgInst.TransformUniformIndex,
                                1, false, (float*)&trans);
                        }
                        GL.DrawElements(BeginMode.Triangles, (int)quadMesh.NumIndices, DrawElementsType.UnsignedInt, 0);
                    }

                    // Allocate specular cubemap
                    GL.BindTexture(TextureTarget.TextureCubeMap, specular.Name);
                    for (int i = 0; i < 6; i++)
                        for (int j = 0; j < specularNumLevels; j++)
                            GL.TexImage2D(
                                (TextureTarget)((uint)TextureTarget.TextureCubeMapPositiveX + i),
                                j, PixelInternalFormat.Rgb16f,
                                specularSize >> j, specularSize >> j, 0, PixelFormat.Rgb,
                                PixelType.Float, IntPtr.Zero);
                    GL.TexParameter(TextureTarget.TextureCubeMap,
                        TextureParameterName.TextureMinFilter,
                        (int)TextureMinFilter.LinearMipmapLinear);
                    GL.TexParameter(TextureTarget.TextureCubeMap,
                        TextureParameterName.TextureMagFilter,
                        (int)TextureMagFilter.Linear);
                    GL.TexParameter(TextureTarget.TextureCubeMap,
                        TextureParameterName.TextureWrapS,
                        (int)TextureWrapMode.ClampToEdge);
                    GL.TexParameter(TextureTarget.TextureCubeMap,
                        TextureParameterName.TextureWrapT,
                        (int)TextureWrapMode.ClampToEdge);
                    GL.TexParameter(TextureTarget.TextureCubeMap,
                        TextureParameterName.TextureWrapR,
                        (int)TextureWrapMode.ClampToEdge);
                    GL.TexParameter(TextureTarget.TextureCubeMap,
                        TextureParameterName.TextureMaxLevel,
                        specularNumLevels - 1);

                    // Set up precompute program
                    GL.BindVertexArray(quadMesh.VertexArrayObj.Name);
                    GL.UseProgram(specularProgInst.ProgramObj.Name);
                    GL.Uniform1(specularProgInst.EquirectangularSkyboxTextureUniformIndex, 0);

                    // Precompute specular cubemap
                    for (int i = 0; i < 6; i++)
                    {
                        unsafe
                        {
                            Matrix3x3 trans = _cubemapTrans[i];
                            GL.ProgramUniformMatrix3(
                                specularProgInst.ProgramObj.Name,
                                specularProgInst.TransformUniformIndex,
                                1, false, (float*)&trans);
                        }
                        for (int j = 0; j < specularNumLevels; j++)
                        {
                            GL.Viewport(0, 0, specularSize >> j, specularSize >> j);
                            GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0,
                                (TextureTarget)((uint)TextureTarget.TextureCubeMapPositiveX + i), specular.Name, j);
                            GL.Uniform1(specularProgInst.LevelUniformIndex, (float)j / (specularNumLevels - 1));
                            GL.DrawElements(BeginMode.Triangles, (int)quadMesh.NumIndices, DrawElementsType.UnsignedInt, 0);
                        }
                    }
                    GL.DeleteFramebuffer(fbo);
                }
            });
            return new SkyboxInstance(owner, diffuse, specular, (uint)specularNumLevels);
        }

        /// <summary>
        /// The transforms needed to render each face of a cubemap.
        /// </summary>
        private static readonly Matrix3x3[] _cubemapTrans = new Matrix3x3[]
        {
            Roflection3i.PosZ_NegY_NegX,
            Roflection3i.NegZ_NegY_PosX,
            Roflection3i.PosX_PosZ_PosY,
            Roflection3i.PosX_NegZ_NegY,
            Roflection3i.PosX_NegY_PosZ,
            Roflection3i.NegX_NegY_NegZ
        };

        public static implicit operator SkyboxResource(Skybox source)
        {
            return new SkyboxResource(source.Image);
        }
    }

    /// <summary>
    /// Encapsulates the graphics objects needed to use a <see cref="Skybox"/>.
    /// </summary>
    public sealed class SkyboxInstance : ResourceInstance
    {
        public SkyboxInstance(Instance owner, TextureObj diffuse, TextureObj specular, uint numSpecularLevels)
            : base(owner)
        {
            Diffuse = diffuse;
            Specular = specular;
            NumSpecularLevels = numSpecularLevels;
        }

        /// <summary>
        /// A cubemap texture which describes the irradiance received by a diffuse surface in a particular
        /// direction.
        /// </summary>
        public TextureObj Diffuse { get; }

        /// <summary>
        /// A cubemap texture which describes the specular radiance received along a particular direction,
        /// with mipmap levels indexed by roughness/blurriness.
        /// </summary>
        public TextureObj Specular { get; }

        /// <summary>
        /// The number of mipmap/roughness levels stored in <see cref="Specular"/>.
        /// </summary>
        public uint NumSpecularLevels { get; }
    }

    /// <summary>
    /// Identifies the "PrecomputeSkybox" program.
    /// </summary>
    public sealed class PrecomputeSkyboxProgram : Resource<PrecomputeSkyboxProgramInstance>
    {
        private PrecomputeSkyboxProgram(string type)
        {
            Type = type;
        }

        /// <summary>
        /// The type of skybox texture this program is for.
        /// </summary>
        public string Type { get; }

        /// <summary>
        /// The <see cref="PrecomputeSkyboxProgram"/> for the diffuse texture.
        /// </summary>
        public static PrecomputeSkyboxProgram Diffuse { get; } = new PrecomputeSkyboxProgram("DIFFUSE");

        /// <summary>
        /// The <see cref="PrecomputeSkyboxProgram"/> for the specular texture.
        /// </summary>
        public static PrecomputeSkyboxProgram Specular { get; } = new PrecomputeSkyboxProgram("SPECULAR");

        protected internal override PrecomputeSkyboxProgramInstance Instantiate(Instance owner)
        {
            ProgramBuilder prog = ProgramBuilder.Create();
            prog.Define(Type);
            prog.AttachStatic(ShaderType.VertexShader, Path.Base / "PrecomputeSkybox.vert.glsl");
            prog.AttachStatic(ShaderType.FragmentShader, Path.Base / "PrecomputeSkybox.frag.glsl");

            ProgramObj progObj = owner._newProgram();
            var progInst = new PrecomputeSkyboxProgramInstance(owner, progObj);
            owner.Queue(delegate
            {
                prog.FinishSynced(progObj);
                progInst.TransformUniformIndex = GL.GetUniformLocation(progObj.Name, "transform");
                progInst.LevelUniformIndex = GL.GetUniformLocation(progObj.Name, "level");
                progInst.EquirectangularSkyboxTextureUniformIndex =
                    GL.GetUniformLocation(progObj.Name, "equirectangularSkyboxTexture");
            });
            owner.Flush();
            return progInst;
        }
    }

    /// <summary>
    /// A <see cref="ProgramInstance"/> for a <see cref="PrecomputeSkyboxProgram"/>.
    /// </summary>
    public sealed class PrecomputeSkyboxProgramInstance : ProgramInstance
    {
        public int TransformUniformIndex { get; internal set; }
        public int LevelUniformIndex { get; internal set; }
        public int EquirectangularSkyboxTextureUniformIndex { get; internal set; }
        public PrecomputeSkyboxProgramInstance(Instance owner, ProgramObj progObj)
            : base(owner, progObj)
        { }
    }

    /// <summary>
    /// Identifies the "RenderSkybox" program.
    /// </summary>
    public sealed class RenderSkyboxProgram : Resource<RenderSkyboxProgramInstance>
    {
        private RenderSkyboxProgram() { }

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        public static RenderSkyboxProgram Instance { get; } = new RenderSkyboxProgram();

        protected internal override RenderSkyboxProgramInstance Instantiate(Instance owner)
        {
            ProgramBuilder prog = ProgramBuilder.Create();
            prog.AttachStatic(ShaderType.VertexShader, Path.Base / "RenderSkybox.vert.glsl");
            prog.AttachStatic(ShaderType.FragmentShader, Path.Base / "RenderSkybox.frag.glsl");

            ProgramObj progObj = owner._newProgram();
            var progInst = new RenderSkyboxProgramInstance(owner, progObj);
            owner.Queue(delegate
            {
                prog.FinishSynced(progObj);
                progInst.WorldToDeviceUniformIndex = GL.GetUniformLocation(progObj.Name, "worldToDevice");
                progInst.SpecularTextureUniformIndex = GL.GetUniformLocation(progObj.Name, "specularTexture");
            });
            owner.Flush();
            return progInst;
        }
    }

    /// <summary>
    /// A <see cref="ProgramInstance"/> for a <see cref="RenderSkyboxProgram"/>.
    /// </summary>
    public sealed class RenderSkyboxProgramInstance : ProgramInstance
    {
        public int WorldToDeviceUniformIndex { get; internal set; }
        public int SpecularTextureUniformIndex { get; internal set; }
        public RenderSkyboxProgramInstance(Instance owner, ProgramObj progObj)
            : base(owner, progObj)
        { }
    }
}
