﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Scg = System.Collections.Generic;

using OpenTK.Graphics.OpenGL4;

using Alunite.Data;
using Alunite.Data.Serialization;
using Alunite.Resources;
using Alunite.Reactive;

namespace Alunite.Graphics.Bindings.OpenGL
{
    /// <summary>
    /// A non-variable sequence of drawing operations formed by combining a <see cref="DrawProcedure"/>
    /// with a <see cref="DrawBuffer"/>.
    /// </summary>
    public struct DrawJob : IDisposable
    {
        public DrawJob(Shared<DisposableBag> resources, DrawProcedure proc, DrawBuffer buffer)
        {
            Resources = resources;
            Procedure = proc;
            Buffer = buffer;
        }

        /// <summary>
        /// The resources that <see cref="Procedure"/> depends on.
        /// </summary>
        public Shared<DisposableBag> Resources { get; }

        /// <summary>
        /// The procedure component of this job.
        /// </summary>
        public DrawProcedure Procedure { get; }

        /// <summary>
        /// The data buffer for this job.
        /// </summary>
        public DrawBuffer Buffer { get; }

        /// <summary>
        /// Executes this job on the current thread.
        /// </summary>
        public void Execute()
        {
            Procedure.Execute(Buffer.Read());
        }

        /// <summary>
        /// Releases the resources being held by this job.
        /// </summary>
        public void Dispose()
        {
            Resources.Dispose();
            Buffer.Dispose();
        }
    }

    /// <summary>
    /// Encapsulates all the information and resources for a sequence of time-varying drawing operations,
    /// limited to a particular period of time.
    /// </summary>
    public struct DynamicDrawJob : IDisposable
    {
        public DynamicDrawJob(Shared<DisposableBag> resources, PrepareProcedure prepareProc, DrawProcedure drawProc)
        {
            Resources = resources;
            PrepareProcedure = prepareProc;
            DrawProcedure = drawProc;
        }

        /// <summary>
        /// The set of persistent resources needed by the resulting <see cref="DrawJob"/>.
        /// </summary>
        public Shared<DisposableBag> Resources { get; }

        /// <summary>
        /// The prepare procedure for this <see cref="DynamicDrawJob"/>.
        /// </summary>
        public PrepareProcedure PrepareProcedure { get; }

        /// <summary>
        /// The draw procedure for this <see cref="DynamicDrawJob"/>.
        /// </summary>
        public DrawProcedure DrawProcedure { get; }

        /// <summary>
        /// Applies the operations contained in this <see cref="DynamicDrawJob"/> onto the current render target.
        /// </summary>
        public void Draw(Observer obs)
        {
            DrawBufferBuilder bufferBuilder = DrawBufferBuilder.Create();
            PrepareProcedure.Execute(obs, bufferBuilder);
            DrawBuffer buffer = bufferBuilder.Finish();
            DrawProcedure.Execute(buffer.Read());
            buffer.Dispose();
        }

        /// <summary>
        /// Gets a snapshot of this <see cref="DynamicDrawJob"/> at the current moment.
        /// </summary>
        public DrawJob Use(Observer obs)
        {
            DrawBufferBuilder bufferBuilder = DrawBufferBuilder.Create();
            PrepareProcedure.Execute(obs, bufferBuilder);
            DrawBuffer buffer = bufferBuilder.Finish();
            return new DrawJob(Resources, DrawProcedure, buffer);
        }

        /// <summary>
        /// Releases the resources being held by this dynamic job.
        /// </summary>
        public void Dispose()
        {
            Resources.Dispose();
        }
    }

    /// <summary>
    /// A helper class for constructing a <see cref="DynamicDrawJob"/>.
    /// </summary>
    public struct DynamicDrawJobBuilder : IBuilder<DynamicDrawJob>
    {
        /// <summary>
        /// The <see cref="Instance"/> which owns the resources for the resulting <see cref="DrawJob"/>.
        /// </summary>
        public Instance Instance;

        /// <summary>
        /// The set of persistent resources needed by the resulting <see cref="DrawJob"/>.
        /// </summary>
        public BagBuilder<IDisposable> Resources;

        /// <summary>
        /// The prepare procedure for this <see cref="DynamicDrawJob"/>.
        /// </summary>
        public PrepareProcedureBuilder PrepareProcedure;

        /// <summary>
        /// The draw procedure for this <see cref="DynamicDrawJob"/>.
        /// </summary>
        public DrawProcedureBuilder DrawProcedure;

        /// <summary>
        /// Creates a new initially-empty <see cref="DynamicDrawJobBuilder"/>.
        /// </summary>
        public static DynamicDrawJobBuilder Create(Instance inst)
        {
            return new DynamicDrawJobBuilder
            {
                Instance = inst,
                Resources = BagBuilder<IDisposable>.CreateEmpty(),
                PrepareProcedure = PrepareProcedureBuilder.Create(),
                DrawProcedure = DrawProcedureBuilder.Create()
            };
        }

        /// <summary>
        /// Gets the <see cref="DynamicDrawJob"/> resulting from this builder.
        /// </summary>
        public DynamicDrawJob Finish()
        {
            return new DynamicDrawJob(
                Shared.Share((DisposableBag)Resources.Finish()),
                PrepareProcedure.Finish(),
                DrawProcedure.Finish());
        }
    }

    /// <summary>
    /// A stored sequence of operations which prepares resources needed by a <see cref="DrawProcedure"/>, e.g. rendering
    /// to a texture prior to its use.
    /// </summary>
    public struct PrepareProcedure
    {
        public PrepareProcedure(List<PrepareOperation> ops)
        {
            Operations = ops;
        }

        /// <summary>
        /// An empty <see cref="PrepareProcedure"/>.
        /// </summary>
        public static PrepareProcedure Empty => new PrepareProcedure(List.Empty<PrepareOperation>());

        /// <summary>
        /// The operations of this procedure.
        /// </summary>
        public List<PrepareOperation> Operations;

        /// <summary>
        /// Gathers required data and resources for the associated <see cref="DrawProcedure"/> and adds them to the given
        /// <see cref="DrawBufferBuilder"/>, in the order expected by the draw procedure.
        /// </summary>
        /// <param name="obs">The <see cref="Observer"/> used to evaluate dynamic values. This may be null if the
        /// procedure is static.</param>
        public void Execute(Observer obs, DrawBufferBuilder data)
        {
            foreach (PrepareOperation op in Operations)
                op(obs, data);
        }
    }

    /// <summary>
    /// Gathers required data and resources for an associated <see cref="DrawProcedure"/> and adds them to the given
    /// <see cref="DrawBufferBuilder"/>, in the order expected by the draw procedure.
    /// </summary>
    /// <param name="obs">The <see cref="Observer"/> used to evaluate dynamic values. This may be null if the
    /// operation is static.</param>
    public delegate void PrepareOperation(Observer obs, DrawBufferBuilder data);

    /// <summary>
    /// A helper class for constructing a <see cref="PrepareProcedure"/> as a set of <see cref="PrepareOperation"/>s.
    /// </summary>
    public struct PrepareProcedureBuilder : IBuilder<PrepareProcedure>
    {
        private ListBuilder<PrepareOperation> _ops;

        /// <summary>
        /// Create a new initially-empty <see cref="PrepareProcedureBuilder"/>.
        /// </summary>
        public static PrepareProcedureBuilder Create()
        {
            return new PrepareProcedureBuilder
            {
                _ops = new ListBuilder<PrepareOperation>()
            };
        }

        /// <summary>
        /// Appends a <see cref="PrepareOperation"/> to the end of the resulting <see cref="PrepareProcedure"/>.
        /// </summary>
        public void Append(PrepareOperation op)
        {
            _ops.Push(op);
        }

        /// <summary>
        /// Appends a <see cref="PrepareProcedure"/> to the end of the resulting <see cref="PrepareProcedure"/>.
        /// </summary>
        public void Concat(PrepareProcedure proc)
        {
            _ops.Concat(proc.Operations);
        }

        /// <summary>
        /// Gets the <see cref="PrepareProcedure"/> currently being described by this builder.
        /// </summary>
        public PrepareProcedure Finish()
        {
            if (_ops.Length == 0)
                return PrepareProcedure.Empty;
            return new PrepareProcedure(_ops.Finish());
        }
    }

    /// <summary>
    /// A stored sequence of operations that can be executed to perform drawing operations on a given context, given
    /// a <see cref="DrawBuffer"/> as input. The procedure is limited to to reading the data and issuing operations based on
    /// it. It may not perform additional computations or lookups. 
    /// </summary>
    [DebuggerTypeProxy(typeof(DrawProcedureDebugView))]
    public struct DrawProcedure
    {
        internal uint[] _insts;
        internal DrawProcedure(uint[] insts)
        {
            _insts = insts;
        }

        /// <summary>
        /// A <see cref="DrawProcedure"/> which does nothing when executed.
        /// </summary>
        public static DrawProcedure Empty => new DrawProcedure(Array.Empty<uint>());

        /// <summary>
        /// The list of operations making up this <see cref="DrawProcedure"/>.
        /// </summary>
        public Scg.IEnumerable<DrawOperation> Operations
        {
            get
            {
                uint index = 0;
                while (index < _insts.Length)
                {
                    DrawOperationType type = (DrawOperationType)_insts[index];
                    yield return new DrawOperation(_insts, index);
                    index++;
                    index += DrawOperation._getArgSize(type);
                }
            }
        }

        /// <summary>
        /// Executes this procedure on the current thread, using the given <see cref="DrawBufferReader"/> for data input.
        /// </summary>
        public void Execute(DrawBufferReader data)
        {
            uint i = 0;
            while (i < _insts.Length)
            {
                switch ((DrawOperationType)_insts[i++])
                {
                    case DrawOperationType.Skip_DynResource:
                        data.ReadResource();
                        break;
                    case DrawOperationType.Call_DynSharedDrawJob:
                        ((Shared<DrawJob>)data.ReadResource()).Target.Execute();
                        break;
                    case DrawOperationType.Enable_ConstEnableCap:
                        GL.Enable((EnableCap)_insts[i++]);
                        break;
                    case DrawOperationType.Disable_ConstEnableCap:
                        GL.Disable((EnableCap)_insts[i++]);
                        break;
                    case DrawOperationType.ClearColor_ConstSingle_ConstSingle_ConstSingle_ConstSingle:
                        GL.ClearColor(
                            _insts[i++].AsSingle(),
                            _insts[i++].AsSingle(),
                            _insts[i++].AsSingle(),
                            _insts[i++].AsSingle());
                        break;
                    case DrawOperationType.Clear_ConstClearBufferMask:
                        GL.Clear((ClearBufferMask)_insts[i++]);
                        break;
                    case DrawOperationType.UseProgram_ConstProgram:
                        GL.UseProgram((int)_insts[i++]);
                        break;
                    case DrawOperationType.PointSize_ConstSingle:
                        GL.PointSize(_insts[i++].AsSingle());
                        break;
                    case DrawOperationType.LineWidth_ConstSingle:
                        GL.LineWidth(_insts[i++].AsSingle());
                        break;
                    case DrawOperationType.Uniform_ConstInt32_ConstInt32:
                        GL.Uniform1((int)_insts[i++], (int)_insts[i++]);
                        break;
                    case DrawOperationType.Uniform_ConstInt32_ConstSingle_ConstSingle:
                        GL.Uniform2((int)_insts[i++], _insts[i++].AsSingle(), _insts[i++].AsSingle());
                        break;
                    case DrawOperationType.Uniform_ConstInt32_DynInt32:
                        GL.Uniform1((int)_insts[i++], data.ReadAlignedNativeInt32());
                        break;
                    case DrawOperationType.Uniform_ConstInt32_DynScalar:
                        GL.Uniform1((int)_insts[i++],
                            data.ReadAlignedNativeSingle());
                        break;
                    case DrawOperationType.Uniform_ConstInt32_DynVector2:
                        GL.Uniform2((int)_insts[i++],
                            data.ReadAlignedNativeSingle(),
                            data.ReadAlignedNativeSingle());
                        break;
                    case DrawOperationType.Uniform_ConstInt32_DynVector3:
                        GL.Uniform3((int)_insts[i++],
                            data.ReadAlignedNativeSingle(),
                            data.ReadAlignedNativeSingle(),
                            data.ReadAlignedNativeSingle());
                        break;
                    case DrawOperationType.Uniform_ConstInt32_DynVector4:
                        GL.Uniform4((int)_insts[i++],
                            data.ReadAlignedNativeSingle(),
                            data.ReadAlignedNativeSingle(),
                            data.ReadAlignedNativeSingle(),
                            data.ReadAlignedNativeSingle());
                        break;
                    case DrawOperationType.Uniform_ConstInt32_DynMatrix3x3:
                        {
                            var mat = data.ReadNativeSingleSpan(9);
                            GL.UniformMatrix3((int)_insts[i++], 1, false, ref MemoryMarshal.GetReference(mat));
                        }
                        break;
                    case DrawOperationType.Uniform_ConstInt32_DynMatrix3x4:
                        {
                            var mat = data.ReadNativeSingleSpan(12);
                            GL.UniformMatrix4x3((int)_insts[i++], 1, false, ref MemoryMarshal.GetReference(mat));
                        }
                        break;
                    case DrawOperationType.Uniform_ConstInt32_DynMatrix4x4:
                        {
                            var mat = data.ReadNativeSingleSpan(16);
                            GL.UniformMatrix4((int)_insts[i++], 1, false, ref MemoryMarshal.GetReference(mat));
                        }
                        break;
                    case DrawOperationType.ActiveTexture_ConstTextureUnit:
                        GL.ActiveTexture((TextureUnit)_insts[i++]);
                        break;
                    case DrawOperationType.BindTexture_ConstTextureTarget_ConstTexture:
                        GL.BindTexture((TextureTarget)_insts[i++], (int)_insts[i++]);
                        break;
                    case DrawOperationType.BindTexture_ConstTextureTarget_DynTexture:
                        GL.BindTexture((TextureTarget)_insts[i++], data.ReadAlignedNativeInt32());
                        break;
                    case DrawOperationType.BindVertexArray_ConstVertexArray:
                        GL.BindVertexArray((int)_insts[i++]);
                        break;
                    case DrawOperationType.DrawElements_ConstPrimitiveType_ConstInt32_ConstDrawElementsType_ConstInt32:
                        GL.DrawElements(
                            (PrimitiveType)_insts[i++],
                            (int)_insts[i++],
                            (DrawElementsType)_insts[i++],
                            (int)_insts[i++]);
                        break;
                    default:
                        throw new Exception();
                }
            }
        }
    }

    /// <summary>
    /// The debugger type proxy for <see cref="DrawProcedure"/>s.
    /// </summary>
    public sealed class DrawProcedureDebugView
    {
        private DrawProcedure _source;
        public DrawProcedureDebugView(DrawProcedure source)
        {
            _source = source;
        }

        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public DrawOperation[] Operations => System.Linq.Enumerable.ToArray(_source.Operations);
    }

    /// <summary>
    /// Describes an operation in a <see cref="DrawProcedure"/>.
    /// </summary>
    public struct DrawOperation
    {
        private uint[] _insts;
        private uint _index;
        internal DrawOperation(uint[] insts, uint index)
        {
            _insts = insts;
            _index = index;
        }

        /// <summary>
        /// The type of this operation.
        /// </summary>
        public DrawOperationType Type => (DrawOperationType)_insts[_index];

        /// <summary>
        /// Gets the number of <see cref="uint"/> required to supply the arguments for a operation of the given type.
        /// </summary>
        internal static uint _getArgSize(DrawOperationType type)
        {
            return _argSize[(uint)type];
        }

        /// <summary>
        /// The table used to implement <see cref="_getArgSize"/>.
        /// </summary>
        private static readonly uint[] _argSize = new uint[]
        {
            0, // Skip_DynResource
            0, // Call_DynSharedDrawJob
            1, // Enable_ConstEnableCap
            1, // Disable_ConstEnableCap
            4, // ClearColor_ConstSingle_ConstSingle_ConstSingle_ConstSingle
            1, // Clear_ConstClearBufferMask
            1, // UseProgram_ConstProgram
            1, // PointSize_ConstSingle
            1, // LineWidth_ConstSingle
            2, // Uniform_ConstInt32_ConstInt32
            3, // Uniform_ConstInt32_ConstSingle_ConstSingle
            1, // Uniform_ConstInt32_DynInt32
            1, // Uniform_ConstInt32_DynScalar
            1, // Uniform_ConstInt32_DynVector2
            1, // Uniform_ConstInt32_DynVector3
            1, // Uniform_ConstInt32_DynVector4
            1, // Uniform_ConstInt32_DynMatrix3x3
            1, // Uniform_ConstInt32_DynMatrix3x4
            1, // Uniform_ConstInt32_DynMatrix4x4
            1, // ActiveTexture_ConstTextureUnit
            2, // BindTexture_ConstTextureTarget_ConstTexture
            1, // BindTexture_ConstTextureTarget_DynTexture
            1, // BindVertexArray_ConstVertexArray
            4, // DrawElements_ConstPrimitiveType_ConstInt32_ConstDrawElementsType_ConstInt32
        };

        public override string ToString()
        {
            uint i = _index;
            switch ((DrawOperationType)_insts[i++])
            {
                case DrawOperationType.Skip_DynResource:
                    return "Skip [IDisposable]";
                case DrawOperationType.Call_DynSharedDrawJob:
                    return "Call [Shared<DrawJob>]";
                case DrawOperationType.Enable_ConstEnableCap:
                    return string.Format("Enable {0}", (EnableCap)_insts[i++]);
                case DrawOperationType.Disable_ConstEnableCap:
                    return string.Format("Disable {0}", (EnableCap)_insts[i++]);
                case DrawOperationType.ClearColor_ConstSingle_ConstSingle_ConstSingle_ConstSingle:
                    return string.Format("ClearColor {0} {1} {2} {3}",
                        _insts[i++].AsSingle().ToStringInvariant(),
                        _insts[i++].AsSingle().ToStringInvariant(),
                        _insts[i++].AsSingle().ToStringInvariant(),
                        _insts[i++].AsSingle().ToStringInvariant());
                case DrawOperationType.Clear_ConstClearBufferMask:
                    return string.Format("Clear {0}", (ClearBufferMask)_insts[i++]);
                case DrawOperationType.UseProgram_ConstProgram:
                    return string.Format("Use {0}", new ProgramObj((int)_insts[i++]));
                case DrawOperationType.PointSize_ConstSingle:
                    return string.Format("PointSize {0}", _insts[i++].AsSingle().ToStringInvariant());
                case DrawOperationType.LineWidth_ConstSingle:
                    return string.Format("LineWidth {0}", _insts[i++].AsSingle().ToStringInvariant());
                case DrawOperationType.Uniform_ConstInt32_ConstInt32:
                    return string.Format("Uniform {0} {1}", (int)_insts[i++], (int)_insts[i++]);
                case DrawOperationType.Uniform_ConstInt32_ConstSingle_ConstSingle:
                    return string.Format("Uniform {0} ({1}, {2})", (int)_insts[i++],
                        _insts[i++].AsSingle().ToStringInvariant(),
                        _insts[i++].AsSingle().ToStringInvariant());
                case DrawOperationType.Uniform_ConstInt32_DynInt32:
                    return string.Format("Uniform {0} [Int32]", (int)_insts[i++]);
                case DrawOperationType.Uniform_ConstInt32_DynScalar:
                    return string.Format("Uniform {0} [Scalar]", (int)_insts[i++]);
                case DrawOperationType.Uniform_ConstInt32_DynVector2:
                    return string.Format("Uniform {0} [Vector2]", (int)_insts[i++]);
                case DrawOperationType.Uniform_ConstInt32_DynVector3:
                    return string.Format("Uniform {0} [Vector3]", (int)_insts[i++]);
                case DrawOperationType.Uniform_ConstInt32_DynVector4:
                    return string.Format("Uniform {0} [Vector4]", (int)_insts[i++]);
                case DrawOperationType.Uniform_ConstInt32_DynMatrix3x3:
                    return string.Format("Uniform {0} [Matrix3x3]", (int)_insts[i++]);
                case DrawOperationType.Uniform_ConstInt32_DynMatrix3x4:
                    return string.Format("Uniform {0} [Matrix3x4]", (int)_insts[i++]);
                case DrawOperationType.Uniform_ConstInt32_DynMatrix4x4:
                    return string.Format("Uniform {0} [Matrix4x4]", (int)_insts[i++]);
                case DrawOperationType.ActiveTexture_ConstTextureUnit:
                    return string.Format("ActiveTextxure {0}", (TextureUnit)_insts[i++]);
                case DrawOperationType.BindTexture_ConstTextureTarget_ConstTexture:
                    return string.Format("BindTexture {0} {1}",
                        (TextureTarget)_insts[i++], new TextureObj((int)_insts[i++]));
                case DrawOperationType.BindTexture_ConstTextureTarget_DynTexture:
                    return string.Format("BindTexture {0} [Texture]", (TextureTarget)_insts[i++]);
                case DrawOperationType.BindVertexArray_ConstVertexArray:
                    return string.Format("BindVertexArray {0}", new VertexArrayObj((int)_insts[i++]));
                case DrawOperationType.DrawElements_ConstPrimitiveType_ConstInt32_ConstDrawElementsType_ConstInt32:
                    return string.Format("DrawElements {0} {1} {2} {3}",
                        (PrimitiveType)_insts[i++],
                        (int)_insts[i++],
                        (DrawElementsType)_insts[i++],
                        (int)_insts[i++]);
                default:
                    throw new Exception();
            }
        }
    }

    /// <summary>
    /// Identifies a type of operation in a <see cref="DrawProcedure"/>.
    /// </summary>
    public enum DrawOperationType : uint
    {
        Skip_DynResource,
        Call_DynSharedDrawJob,
        Enable_ConstEnableCap,
        Disable_ConstEnableCap,
        ClearColor_ConstSingle_ConstSingle_ConstSingle_ConstSingle,
        Clear_ConstClearBufferMask,
        UseProgram_ConstProgram,
        PointSize_ConstSingle,
        LineWidth_ConstSingle,
        Uniform_ConstInt32_ConstInt32,
        Uniform_ConstInt32_ConstSingle_ConstSingle,
        Uniform_ConstInt32_DynInt32,
        Uniform_ConstInt32_DynScalar,
        Uniform_ConstInt32_DynVector2,
        Uniform_ConstInt32_DynVector3,
        Uniform_ConstInt32_DynVector4,
        Uniform_ConstInt32_DynMatrix3x3,
        Uniform_ConstInt32_DynMatrix3x4,
        Uniform_ConstInt32_DynMatrix4x4,
        ActiveTexture_ConstTextureUnit,
        BindTexture_ConstTextureTarget_ConstTexture,
        BindTexture_ConstTextureTarget_DynTexture,
        BindVertexArray_ConstVertexArray,
        DrawElements_ConstPrimitiveType_ConstInt32_ConstDrawElementsType_ConstInt32
    }

    /// <summary>
    /// A helper for constructing a <see cref="DrawProcedure"/> by adding operations incrementally.
    /// </summary>
    public sealed class DrawProcedureBuilder : IBuilder<DrawProcedure>
    {
        private uint[] _insts;
        private uint _nextInstIndex;
        private DrawProcedureBuilder()
        {
            _insts = new uint[32];
            _nextInstIndex = 0;
        }

        /// <summary>
        /// Creates a new initially-empty <see cref="DrawProcedureBuilder"/>.
        /// </summary>
        public static DrawProcedureBuilder Create()
        {
            return new DrawProcedureBuilder();
        }

        /// <summary>
        /// Begins an operation of the given type. If this operation takes constant arguments, they must be appended
        /// immediately afterward.
        /// </summary>
        public void Begin(DrawOperationType type)
        {
            Table.Allocate(ref _insts, _nextInstIndex, 1);
            _insts[_nextInstIndex++] = (uint)type;
        }

        /// <summary>
        /// Attaches a constant argument of type <see cref="uint"/> to a previous operation.
        /// </summary>
        public void Append(uint arg)
        {

            Table.Allocate(ref _insts, _nextInstIndex, 1);
            _insts[_nextInstIndex++] = arg;
        }

        /// <summary>
        /// Attaches a constant argument of type <see cref="int"/> to a previous operation.
        /// </summary>
        public void Append(int arg)
        {
            Append(unchecked((uint)arg));
        }

        /// <summary>
        /// Attaches a constant argument of type <see cref="float"/> to a previous operation.
        /// </summary>
        public void Append(float arg)
        {
            Append(arg.AsUInt32());
        }

        /// <summary>
        /// Attaches a constant argument of type <see cref="EnableCap"/> to a previous operation.
        /// </summary>
        public void Append(EnableCap cap)
        {
            Append((int)cap);
        }

        /// <summary>
        /// Attaches a constant argument of type <see cref="ClearBufferMask"/> to a previous operation.
        /// </summary>
        public void Append(ClearBufferMask mask)
        {
            Append((int)mask);
        }

        /// <summary>
        /// Attaches a constant argument of type <see cref="ProgramObj"/> to a previous operation.
        /// </summary>
        public void Append(ProgramObj prog)
        {
            Append(prog.Name);
        }

        /// <summary>
        /// Attaches a constant argument of type <see cref="TextureUnit"/> to a previous operation.
        /// </summary>
        public void Append(TextureUnit unit)
        {
            Append((int)unit);
        }

        /// <summary>
        /// Attaches a constant argument of type <see cref="TextureTarget"/> to a previous operation.
        /// </summary>
        public void Append(TextureTarget target)
        {
            Append((int)target);
        }

        /// <summary>
        /// Attaches a constant argument of type <see cref="TextureObj"/> to a previous operation.
        /// </summary>
        public void Append(TextureObj texture)
        {
            Append(texture.Name);
        }

        /// <summary>
        /// Attaches a constant argument of type <see cref="VertexArrayObj"/> to a previous operation.
        /// </summary>
        public void Append(VertexArrayObj array)
        {
            Append(array.Name);
        }

        /// <summary>
        /// Attaches a constant argument of type <see cref="PrimitiveType"/> to a previous operation.
        /// </summary>
        public void Append(PrimitiveType type)
        {
            Append((int)type);
        }

        /// <summary>
        /// Attaches a constant argument of type <see cref="DrawElementsType"/> to a previous operation.
        /// </summary>
        public void Append(DrawElementsType type)
        {
            Append((int)type);
        }

        /// <summary>
        /// Calls the given <see cref="DrawProcedure"/> as part of this procedure.
        /// </summary>
        public void Call(DrawProcedure proc)
        {
            Table.Allocate(ref _insts, _nextInstIndex, (uint)proc._insts.Length);
            Array.Copy(proc._insts, 0, _insts, _nextInstIndex, proc._insts.Length);
            _nextInstIndex += (uint)proc._insts.Length;
        }

        /// <summary>
        /// Gets the <see cref="DrawProcedure"/> resulting from the given builder and calls it as part of
        /// this procedure.
        /// </summary>
        public void Call(DrawProcedureBuilder proc)
        {
            Table.Allocate(ref _insts, _nextInstIndex, proc._nextInstIndex);
            Array.Copy(proc._insts, 0, _insts, _nextInstIndex, proc._nextInstIndex);
            _nextInstIndex += (uint)proc._nextInstIndex;
        }

        /// <summary>
        /// Gets the <see cref="DrawProcedure"/> resulting from this builder.
        /// </summary>
        public DrawProcedure Finish()
        {
            uint[] fInsts = new uint[_nextInstIndex];
            Array.Copy(_insts, fInsts, _nextInstIndex);
            return new DrawProcedure(fInsts);
        }
    }

    /// <summary>
    /// A helper for defining the <see cref="DrawProcedure"/> needed to set up the uniforms for a shader.
    /// </summary>
    public sealed class DrawUniformsSetter
    {
        public DrawUniformsSetter(DrawProcedureBuilder proc)
        {
            Procedure = proc;
        }

        /// <summary>
        /// The underlying <see cref="DrawProcedureBuilder"/> to which the uniform setting procedure is to be written.
        /// </summary>
        public DrawProcedureBuilder Procedure { get; }

        /// <summary>
        /// Identifies the next texture unit that has not yet been assigned.
        /// </summary>
        public uint NextFreeTextureUnit;

        /// <summary>
        /// Appends an operation to <see cref="Procedure"/> which reads a <see cref="int"/> from the draw buffer
        /// and writes it to the uniform at the given location.
        /// </summary>
        public void SetInt32(int loc)
        {
            Procedure.Begin(DrawOperationType.Uniform_ConstInt32_DynInt32);
            Procedure.Append(loc);
        }

        /// <summary>
        /// Appends an operation to <see cref="Procedure"/> which reads a <see cref="Scalar"/> from the draw buffer
        /// and writes it to the uniform at the given location.
        /// </summary>
        public void SetScalar(int loc)
        {
            Procedure.Begin(DrawOperationType.Uniform_ConstInt32_DynScalar);
            Procedure.Append(loc);
        }

        /// <summary>
        /// Appends an operation to <see cref="Procedure"/> which reads a <see cref="Vector2"/> from the draw buffer
        /// and writes it to the uniform at the given location.
        /// </summary>
        public void SetVector2(int loc)
        {
            Procedure.Begin(DrawOperationType.Uniform_ConstInt32_DynVector2);
            Procedure.Append(loc);
        }

        /// <summary>
        /// Appends an operation to <see cref="Procedure"/> which reads a <see cref="Vector3"/> from the draw buffer
        /// and writes it to the uniform at the given location.
        /// </summary>
        public void SetVector3(int loc)
        {
            Procedure.Begin(DrawOperationType.Uniform_ConstInt32_DynVector3);
            Procedure.Append(loc);
        }

        /// <summary>
        /// Appends an operation to <see cref="Procedure"/> which reads a <see cref="Vector4"/> from the draw buffer
        /// and writes it to the uniform at the given location.
        /// </summary>
        public void SetVector4(int loc)
        {
            Procedure.Begin(DrawOperationType.Uniform_ConstInt32_DynVector4);
            Procedure.Append(loc);
        }

        /// <summary>
        /// Appends an operation to <see cref="Procedure"/> which reads a <see cref="Matrix3x3"/> from the draw buffer
        /// and writes it to the uniform at the given location.
        /// </summary>
        public void SetMatrix3x3(int loc)
        {
            Procedure.Begin(DrawOperationType.Uniform_ConstInt32_DynMatrix3x3);
            Procedure.Append(loc);
        }

        /// <summary>
        /// Appends an operation to <see cref="Procedure"/> which reads a <see cref="Matrix3x4"/> from the draw buffer
        /// and writes it to the uniform at the given location.
        /// </summary>
        public void SetMatrix3x4(int loc)
        {
            Procedure.Begin(DrawOperationType.Uniform_ConstInt32_DynMatrix3x4);
            Procedure.Append(loc);
        }

        /// <summary>
        /// Appends an operation to <see cref="Procedure"/> which reads a <see cref="Matrix4x4"/> from the draw buffer
        /// and writes it to the uniform at the given location.
        /// </summary>
        public void SetMatrix4x4(int loc)
        {
            Procedure.Begin(DrawOperationType.Uniform_ConstInt32_DynMatrix4x4);
            Procedure.Append(loc);
        }
    }
}
