﻿using System;
using System.Diagnostics;

using Alunite.Data;
using Alunite.Data.Geometry;
using Alunite.Data.Serialization;
using Alunite.Reactive;

using OpenTK.Graphics.OpenGL4;

namespace Alunite.Graphics.Bindings.OpenGL
{
    /// <summary>
    /// A helper for constructing a <see cref="DynamicDrawJob"/> using an <see cref="IRenderer"/> interface.
    /// </summary>
    public sealed class DynamicRenderer : IDynamicRenderer, IBuilder<DynamicDrawJob>, IDisposable
    {
        private SkyboxInstance _skyboxInst;
        internal DynamicRenderer(DynamicDrawJobBuilder job, Observer obs,
            Dynamic<Motion3> worldToView,
            Dynamic<Projection> viewToDevice)
        {
            Job = job;
            Observer = obs;
            WorldToView = worldToView;
            ViewToDevice = viewToDevice;
        }

        /// <summary>
        /// The <see cref="DynamicDrawJobBuilder"/> for the job being produced by this drawer. There may be
        /// outstanding draw operations that have not been committed to the job. Use <see cref="Flush"/> to
        /// ensure all operations are committed.
        /// </summary>
        public DynamicDrawJobBuilder Job { get; }

        /// <summary>
        /// The OpenGL instance which maintains the resources for the resulting <see cref="DynamicDrawJob"/>.
        /// </summary>
        public Instance Instance => Job.Instance;

        /// <summary>
        /// The <see cref="Reactive.Observer"/> used to resolve dynamic values that this drawer can't generalize beyond
        /// the current moment.
        /// </summary>
        public Observer Observer { get; }

        /// <summary>
        /// The transformation from world space to view space.
        /// </summary>
        public Dynamic<Motion3> WorldToView { get; }

        /// <summary>
        /// The projection from view space to device coordinates.
        /// </summary>
        public Dynamic<Projection> ViewToDevice { get; }

        /// <summary>
        /// Performs the necessary setup tasks to <see cref="Job"/> so that this <see cref="DynamicRenderer"/>
        /// can be used.
        /// </summary>
        public void Enter(Skybox skybox)
        {
            // Load skybox
            _skyboxInst = Instance.Use((SkyboxResource)skybox);
            Job.Resources.Add(_skyboxInst);

            // Render skybox
            RenderSkyboxProgramInstance progInst = Instance.Use(RenderSkyboxProgram.Instance);
            Job.Resources.Add(progInst);
            Job.DrawProcedure.Begin(DrawOperationType.UseProgram_ConstProgram);
            Job.DrawProcedure.Append(progInst.ProgramObj);

            Dynamic<Motion3> worldToView = WorldToView;
            Dynamic<Projection> viewToDevice = ViewToDevice;
            Job.DrawProcedure.Begin(DrawOperationType.Uniform_ConstInt32_DynMatrix4x4);
            Job.DrawProcedure.Append(progInst.WorldToDeviceUniformIndex);
            Job.PrepareProcedure.Append((obs, buffer) =>
            {
                buffer.Prepare(64);
                buffer.WriteAlignedNative(viewToDevice[obs] * worldToView[obs]);
            });

            Job.DrawProcedure.Begin(DrawOperationType.Uniform_ConstInt32_ConstInt32);
            Job.DrawProcedure.Append(progInst.SpecularTextureUniformIndex);
            Job.DrawProcedure.Append(0);
            Job.DrawProcedure.Begin(DrawOperationType.ActiveTexture_ConstTextureUnit);
            Job.DrawProcedure.Append(TextureUnit.Texture0);
            Job.DrawProcedure.Begin(DrawOperationType.BindTexture_ConstTextureTarget_ConstTexture);
            Job.DrawProcedure.Append(TextureTarget.TextureCubeMap);
            Job.DrawProcedure.Append(_skyboxInst.Specular);

            var meshInst = Instance.Use(Mesh.StandardInwardCube);
            Job.Resources.Add(meshInst);
            Job.DrawProcedure.Begin(DrawOperationType.BindVertexArray_ConstVertexArray);
            Job.DrawProcedure.Append(meshInst.VertexArrayRegion.Source);
            Job.DrawProcedure.Begin(DrawOperationType.DrawElements_ConstPrimitiveType_ConstInt32_ConstDrawElementsType_ConstInt32);
            Job.DrawProcedure.Append(PrimitiveType.Triangles);
            Job.DrawProcedure.Append(meshInst.VertexArrayRegion.NumIndices);
            Job.DrawProcedure.Append(DrawElementsType.UnsignedInt);
            Job.DrawProcedure.Append(meshInst.VertexArrayRegion.IndexOffset);

            // Enable depth testing
            Job.DrawProcedure.Begin(DrawOperationType.Enable_ConstEnableCap);
            Job.DrawProcedure.Append(EnableCap.DepthTest);
        }

        /// <summary>
        /// Adds a mesh to the scene.
        /// </summary>
        public void RenderMesh(
            Permutation<MaterialTexture2> textures,
            RegularMeshVertexSpanList verts,
            SpanList<uint> indices)
        {
            if (verts.Type.NumTextures <= 1)
            {
                var vertexArrayBuilder = Instance.BuildVertexArray(VertexFormat.Blend1SolidVertex);
                bool wasWritten = vertexArrayBuilder.TryWrite((SpanList<Blend1SolidVertex>)verts, indices);
                Debug.Assert(wasWritten);
                VertexArrayRegion vertexArrayRegion = vertexArrayBuilder.UseFinish(Job.Resources);
                _renderMesh(_renderBlend1SolidProgram, textures, vertexArrayRegion, indices);
            }
            else if (verts.Type.NumTextures <= 2)
            {
                var vertexArrayBuilder = Instance.BuildVertexArray(VertexFormat.Blend2SolidVertex);
                bool wasWritten = vertexArrayBuilder.TryWrite((SpanList<Blend2SolidVertex>)verts, indices);
                Debug.Assert(wasWritten);
                VertexArrayRegion vertexArrayRegion = vertexArrayBuilder.UseFinish(Job.Resources);
                _renderMesh(_renderBlend2SolidProgram, textures, vertexArrayRegion, indices);
            }
            else if (verts.Type.NumTextures <= 4)
            {
                var vertexArrayBuilder = Instance.BuildVertexArray(VertexFormat.Blend4SolidVertex);
                bool wasWritten = vertexArrayBuilder.TryWrite((SpanList<Blend4SolidVertex>)verts, indices);
                Debug.Assert(wasWritten);
                VertexArrayRegion vertexArrayRegion = vertexArrayBuilder.UseFinish(Job.Resources);
                _renderMesh(_renderBlend4SolidProgram, textures, vertexArrayRegion, indices);
            }
            else
            {
                // TODO
            }
        }

        /// <summary>
        /// A <see cref="RenderMeshProgram"/> which renders a mesh of <see cref="Blend1SolidVertex"/>s.
        /// </summary>
        private static RenderMeshProgram _renderBlend1SolidProgram = new RenderMeshProgram(1);

        /// <summary>
        /// A <see cref="RenderMeshProgram"/> which renders a mesh of <see cref="Blend2SolidVertex"/>s.
        /// </summary>
        private static RenderMeshProgram _renderBlend2SolidProgram = new RenderMeshProgram(2);

        /// <summary>
        /// A <see cref="RenderMeshProgram"/> which renders a mesh of <see cref="Blend4SolidVertex"/>s.
        /// </summary>
        private static RenderMeshProgram _renderBlend4SolidProgram = new RenderMeshProgram(4);

        /// <summary>
        /// Adds a mesh to the scene.
        /// </summary>
        private void _renderMesh(
            RenderMeshProgram program,
            Permutation<MaterialTexture2> textures,
            VertexArrayRegion vertexArrayRegion,
            SpanList<uint> indices)
        {
            // Get program
            var progInst = Instance.Use(program);
            Job.Resources.Add(progInst);

            // Get BRDF texture
            var brdfInst = Instance.Use(BrdfTexture.Instance);
            Job.Resources.Add(brdfInst);

            // Find or build material atlas
            // TODO: Apply transform to material coordinates
            Span<AtlasSlot> slots = stackalloc AtlasSlot[(int)textures.Length];
            Instance.UseMaterialTextureAtlas(Job.Resources, textures, slots,
                out TextureObj albedoTextureObj,
                out TextureObj normalTextureObj,
                out TextureObj metalnessTextureObj,
                out TextureObj roughnessTextureObj);
            
            Dynamic<Motion3> worldToView = WorldToView;
            Dynamic<Projection> viewToDevice = ViewToDevice;
            
            Job.DrawProcedure.Begin(DrawOperationType.UseProgram_ConstProgram);
            Job.DrawProcedure.Append(progInst.ProgramObj);

            Job.DrawProcedure.Begin(DrawOperationType.Uniform_ConstInt32_DynMatrix4x4);
            Job.DrawProcedure.Append(progInst.WorldToDeviceUniformIndex);
            Job.PrepareProcedure.Append((obs, buffer) =>
            {
                buffer.Prepare(64);
                buffer.WriteAlignedNative(viewToDevice[obs] * worldToView[obs]);
            });

            Job.DrawProcedure.Begin(DrawOperationType.Uniform_ConstInt32_DynVector4);
            Job.DrawProcedure.Append(progInst.EyeBiasUniformIndex);
            Job.PrepareProcedure.Append((obs, buffer) =>
            {
                Motion3 curWorldToView = worldToView[obs];
                Vector4 eyeBias = viewToDevice[obs].EyeBias;
                eyeBias = new Vector4(curWorldToView.Linear.Inverse * (eyeBias.X_Y_Z + curWorldToView.Offset * eyeBias.W), eyeBias.W);
                buffer.Prepare(16);
                buffer.WriteAlignedNative(eyeBias);
            });

            Job.DrawProcedure.Begin(DrawOperationType.Uniform_ConstInt32_ConstInt32);
            Job.DrawProcedure.Append(progInst.BrdfTextureUniformIndex);
            Job.DrawProcedure.Append(0);
            Job.DrawProcedure.Begin(DrawOperationType.ActiveTexture_ConstTextureUnit);
            Job.DrawProcedure.Append(TextureUnit.Texture0);
            Job.DrawProcedure.Begin(DrawOperationType.BindTexture_ConstTextureTarget_ConstTexture);
            Job.DrawProcedure.Append(TextureTarget.Texture2D);
            Job.DrawProcedure.Append(brdfInst.TextureObj);

            Job.DrawProcedure.Begin(DrawOperationType.Uniform_ConstInt32_ConstInt32);
            Job.DrawProcedure.Append(progInst.SkyboxDiffuseTextureUniformIndex);
            Job.DrawProcedure.Append(1);
            Job.DrawProcedure.Begin(DrawOperationType.ActiveTexture_ConstTextureUnit);
            Job.DrawProcedure.Append(TextureUnit.Texture1);
            Job.DrawProcedure.Begin(DrawOperationType.BindTexture_ConstTextureTarget_ConstTexture);
            Job.DrawProcedure.Append(TextureTarget.TextureCubeMap);
            Job.DrawProcedure.Append(_skyboxInst.Diffuse);

            Job.DrawProcedure.Begin(DrawOperationType.Uniform_ConstInt32_ConstInt32);
            Job.DrawProcedure.Append(progInst.SkyboxSpecularTextureUniformIndex);
            Job.DrawProcedure.Append(2);
            Job.DrawProcedure.Begin(DrawOperationType.ActiveTexture_ConstTextureUnit);
            Job.DrawProcedure.Append(TextureUnit.Texture2);
            Job.DrawProcedure.Begin(DrawOperationType.BindTexture_ConstTextureTarget_ConstTexture);
            Job.DrawProcedure.Append(TextureTarget.TextureCubeMap);
            Job.DrawProcedure.Append(_skyboxInst.Specular);

            Job.DrawProcedure.Begin(DrawOperationType.Uniform_ConstInt32_ConstInt32);
            Job.DrawProcedure.Append(progInst.SkyboxSpecularNumLevelsUniformIndex);
            Job.DrawProcedure.Append(_skyboxInst.NumSpecularLevels);

            Job.DrawProcedure.Begin(DrawOperationType.Uniform_ConstInt32_ConstInt32);
            Job.DrawProcedure.Append(progInst.AlbedoTextureUniformIndex);
            Job.DrawProcedure.Append(3);
            Job.DrawProcedure.Begin(DrawOperationType.ActiveTexture_ConstTextureUnit);
            Job.DrawProcedure.Append(TextureUnit.Texture3);
            Job.DrawProcedure.Begin(DrawOperationType.BindTexture_ConstTextureTarget_ConstTexture);
            Job.DrawProcedure.Append(TextureTarget.Texture2DArray);
            Job.DrawProcedure.Append(albedoTextureObj);

            Job.DrawProcedure.Begin(DrawOperationType.Uniform_ConstInt32_ConstInt32);
            Job.DrawProcedure.Append(progInst.NormalTextureUniformIndex);
            Job.DrawProcedure.Append(4);
            Job.DrawProcedure.Begin(DrawOperationType.ActiveTexture_ConstTextureUnit);
            Job.DrawProcedure.Append(TextureUnit.Texture4);
            Job.DrawProcedure.Begin(DrawOperationType.BindTexture_ConstTextureTarget_ConstTexture);
            Job.DrawProcedure.Append(TextureTarget.Texture2DArray);
            Job.DrawProcedure.Append(normalTextureObj);

            Job.DrawProcedure.Begin(DrawOperationType.Uniform_ConstInt32_ConstInt32);
            Job.DrawProcedure.Append(progInst.MetalnessTextureUniformIndex);
            Job.DrawProcedure.Append(5);
            Job.DrawProcedure.Begin(DrawOperationType.ActiveTexture_ConstTextureUnit);
            Job.DrawProcedure.Append(TextureUnit.Texture5);
            Job.DrawProcedure.Begin(DrawOperationType.BindTexture_ConstTextureTarget_ConstTexture);
            Job.DrawProcedure.Append(TextureTarget.Texture2DArray);
            Job.DrawProcedure.Append(metalnessTextureObj);

            Job.DrawProcedure.Begin(DrawOperationType.Uniform_ConstInt32_ConstInt32);
            Job.DrawProcedure.Append(progInst.RoughnessTextureUniformIndex);
            Job.DrawProcedure.Append(6);
            Job.DrawProcedure.Begin(DrawOperationType.ActiveTexture_ConstTextureUnit);
            Job.DrawProcedure.Append(TextureUnit.Texture6);
            Job.DrawProcedure.Begin(DrawOperationType.BindTexture_ConstTextureTarget_ConstTexture);
            Job.DrawProcedure.Append(TextureTarget.Texture2DArray);
            Job.DrawProcedure.Append(roughnessTextureObj);

            Job.DrawProcedure.Begin(DrawOperationType.BindVertexArray_ConstVertexArray);
            Job.DrawProcedure.Append(vertexArrayRegion.Source);
            Job.DrawProcedure.Begin(DrawOperationType.DrawElements_ConstPrimitiveType_ConstInt32_ConstDrawElementsType_ConstInt32);
            Job.DrawProcedure.Append(PrimitiveType.Triangles);
            Job.DrawProcedure.Append(vertexArrayRegion.NumIndices);
            Job.DrawProcedure.Append(DrawElementsType.UnsignedInt);
            Job.DrawProcedure.Append(vertexArrayRegion.IndexOffset);
        }
        
        /// <summary>
        /// Commits all outstanding draw operations to <see cref="Job"/>.
        /// </summary>
        public void Flush()
        {

        }

        /// <summary>
        /// Performs the necessary cleanup tasks to <see cref="Job"/> after rendering is done.
        /// </summary>
        public void Exit()
        {
            // Disable depth testing
            Job.DrawProcedure.Begin(DrawOperationType.Disable_ConstEnableCap);
            Job.DrawProcedure.Append(EnableCap.DepthTest);
        }

        /// <summary>
        /// Gets the <see cref="DynamicDrawJob"/> which applies the drawing operations specified using this
        /// <see cref="DynamicDrawer3"/>.
        /// </summary>
        public DynamicDrawJob Finish()
        {
            Flush();
            Exit();
            return Job.Finish();
        }

        void IDisposable.Dispose()
        {
            Flush();
            Exit();
        }
    }
}
