﻿using System;
using System.Diagnostics;

using OpenTK.Graphics.OpenGL4;

namespace Alunite.Graphics.Bindings.OpenGL
{
    /// <summary>
    /// Identifies an OpenGL buffer object.
    /// </summary>
    public struct BufferObj : IFreeable
    {
        public BufferObj(int name)
        {
            Name = name;
        }

        /// <summary>
        /// The OpenGL name for the buffer.
        /// </summary>
        public int Name;

        /// <summary>
        /// Indicates that the resource will no longer be used. This must be called on the main thread for
        /// the OpenGL instance.
        /// </summary>
        public void FreeSynced()
        {
            GL.DeleteBuffer(Name);
        }

        public override string ToString()
        {
            return "Buffer(" + Name.ToStringInvariant() + ")";
        }
    }
    
    /// <summary>
    /// An interface for supplying data for a <see cref="BufferObj"/>.
    /// </summary>
    public interface IBufferWriter
    {
        /// <summary>
        /// The data for the buffer.
        /// </summary>
        Span<byte> Data { get; }
    }

    /// <summary>
    /// A <see cref="IBufferWriter"/> which is used to create a buffer.
    /// </summary>
    public interface IBufferBuilder : IBufferWriter
    {
        /// <summary>
        /// The type of buffer created by this builder.
        /// </summary>
        BufferTarget Target { get; }
    }

    /// <summary>
    /// An <see cref="IBufferBuilder"/> which works by temporarily storing data in an in-memory buffer
    /// and transferring it to a GL buffer once all writing is complete.
    /// </summary>
    public sealed class MemoryBufferBuilder : IBufferBuilder
    {
        private MemoryBufferBuilder(Instance inst, BufferTarget target, ulong len)
        {
            Instance = inst;
            Target = target;
            Data = new byte[len];
        }

        /// <summary>
        /// Creates a new <see cref="MemoryBufferBuilder"/>.
        /// </summary>
        public static MemoryBufferBuilder Create(Instance inst, BufferTarget target, ulong len)
        {
            return new MemoryBufferBuilder(inst, target, len);
        }

        /// <summary>
        /// The <see cref="OpenGL.Instance"/> the resulting buffer will be created in.
        /// </summary>
        public Instance Instance { get; }

        /// <summary>
        /// The type of buffer created by this builder.
        /// </summary>
        public BufferTarget Target { get; }

        /// <summary>
        /// The data for the buffer.
        /// </summary>
        public byte[] Data { get; }

        Span<byte> IBufferWriter.Data => Data;
    }

    partial class Instance
    {
        /// <summary>
        /// Creates an <see cref="IBufferBuilder"/> for a buffer used to supply image data for a texture.
        /// </summary>
        internal IBufferBuilder BuildPixelUnpackBuffer(ulong len)
        {
            // TODO: Use mapped pixel buffers
            return MemoryBufferBuilder.Create(this, BufferTarget.PixelUnpackBuffer, len);
        }

        /// <summary>
        /// Creates a new <see cref="BufferObj"/> with no associated data.
        /// </summary>
        internal BufferObj _newBuffer()
        {
            // TODO: Fallback if we are not on the main thread
            Debug.Assert(IsSynced);
            return new BufferObj(GL.GenBuffer());
        }
    }
}
