﻿using System;
using System.Diagnostics;

using Alunite.Data;
using Alunite.Data.Geometry;
using Alunite.Data.Serialization;
using Alunite.Reactive;
using Alunite.Resources;

using OpenTK.Graphics.OpenGL4;

namespace Alunite.Graphics.Bindings.OpenGL
{
    /// <summary>
    /// A helper for constructing a <see cref="DynamicDrawJob"/> using an <see cref="IDrawer2"/> interface.
    /// </summary>
    public sealed class DynamicDrawer2 : IDynamicDrawer2, IBuilder<DynamicDrawJob>, IDisposable
    {
        internal DynamicDrawer2(DynamicDrawJobBuilder job, Observer obs, Vector2i size)
        {
            Job = job;
            Observer = obs;
            Size = size;
        }

        /// <summary>
        /// The <see cref="DynamicDrawJobBuilder"/> for the job being produced by this drawer. There may be
        /// outstanding draw operations that have not been committed to the job. Use <see cref="Flush"/> to
        /// ensure all operations are committed.
        /// </summary>
        public DynamicDrawJobBuilder Job { get; }

        /// <summary>
        /// The OpenGL instance which maintains the resources for the resulting <see cref="DynamicDrawJob"/>.
        /// </summary>
        public Instance Instance => Job.Instance;

        /// <summary>
        /// The <see cref="Reactive.Observer"/> used to resolve dynamic values that this drawer can't generalize beyond
        /// the current moment.
        /// </summary>
        public Observer Observer { get; }

        /// <summary>
        /// The size of the render target.
        /// </summary>
        public Vector2i Size { get; }

        /// <summary>
        /// Rasterizes a mesh onto the drawing surface and composites it using the given <see cref="Texture2"/>.
        /// </summary>
        public unsafe void Draw(PaintTexture2 texture, SpanList<TextureVertex2> verts, SpanList<uint> indices)
        {
            var vertexArrayBuilder = Instance.BuildVertexArray(VertexFormat.TextureVertex2);
            bool wasWritten = vertexArrayBuilder.TryWrite(verts, indices);
            Debug.Assert(wasWritten);
            var vertexArrayRegion = vertexArrayBuilder.UseFinish(Job.Resources);

            Draw2ProgramInstance progInst = Instance.Use(Draw2Program.Instance);
            Job.Resources.Add(Instance.UseTexture(texture, out TextureObj textureObj));
            Job.Resources.Add(progInst);

            Job.DrawProcedure.Begin(DrawOperationType.UseProgram_ConstProgram);
            Job.DrawProcedure.Append(progInst.ProgramObj);
            Job.DrawProcedure.Begin(DrawOperationType.Uniform_ConstInt32_ConstSingle_ConstSingle);
            Job.DrawProcedure.Append(progInst.TargetSizeUniformIndex);
            Job.DrawProcedure.Append((float)Size.X);
            Job.DrawProcedure.Append((float)Size.Y);
            Job.DrawProcedure.Begin(DrawOperationType.Uniform_ConstInt32_ConstInt32);
            Job.DrawProcedure.Append(progInst.ImageUniformIndex);
            Job.DrawProcedure.Append(0);
            Job.DrawProcedure.Begin(DrawOperationType.ActiveTexture_ConstTextureUnit);
            Job.DrawProcedure.Append(TextureUnit.Texture0);
            Job.DrawProcedure.Begin(DrawOperationType.BindTexture_ConstTextureTarget_ConstTexture);
            Job.DrawProcedure.Append(TextureTarget.Texture2D);
            Job.DrawProcedure.Append(textureObj);
            Job.DrawProcedure.Begin(DrawOperationType.BindVertexArray_ConstVertexArray);
            Job.DrawProcedure.Append(vertexArrayRegion.Source);
            Job.DrawProcedure.Begin(DrawOperationType.DrawElements_ConstPrimitiveType_ConstInt32_ConstDrawElementsType_ConstInt32);
            Job.DrawProcedure.Append(PrimitiveType.Triangles);
            Job.DrawProcedure.Append(vertexArrayRegion.NumIndices);
            Job.DrawProcedure.Append(DrawElementsType.UnsignedInt);
            Job.DrawProcedure.Append(vertexArrayRegion.IndexOffset);
        }

        /// <summary>
        /// Draws a projection of a three-dimensional figure, specified using the returned
        /// <see cref="IDrawer3"/>. Once all three-dimensional drawing operations are complete, the
        /// returned <see cref="IDisposable"/> should be disposed. Drawing to the
        /// <see cref="IDrawer2"/> may not resume until this is done.
        /// </summary>
        public IDisposable Project(Viewport viewport, out IDrawer3 drawer)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates a <see cref="IDrawer3"/> to draw to a three-dimensional space that is projected onto
        /// the given <see cref="Viewport"/> of this drawer. Once all three-dimensional drawing operations
        /// are complete, the returned <see cref="IDisposable"/> should be disposed. Drawing to the
        /// <see cref="IDrawer2"/> may not resume until this is done.
        /// </summary>
        public IDisposable Project(Dynamic<Viewport> viewport, out IDrawer3 drawer)
        {
            // TODO: Respect viewport bounds
            Dynamic<Matrix4x4> worldToDevice = viewport.Map(v => v.ViewToDevice * v.WorldToView);
            DynamicDrawer3 sDrawer = new DynamicDrawer3(Job, Observer, worldToDevice);
            drawer = sDrawer;
            return sDrawer;
        }

        /// <summary>
        /// Draws a projection of a rendered three-dimensional space, specified using the returned
        /// <see cref="IRenderer"/>. Once all renderable effects have been specified, the returned
        /// <see cref="IDisposable"/> should be disposed. Drawing to the <see cref="IDrawer2"/> may not
        /// resume until this is done.
        /// </summary>
        public IDisposable ProjectRender(
            Skybox skybox, Viewport viewport, Tone tone,
            out IRenderer renderer)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Draws a projection of a rendered three-dimensional space, specified using the returned
        /// <see cref="IRenderer"/>. Once all renderable effects have been specified, the returned
        /// <see cref="IDisposable"/> should be disposed. Drawing to the <see cref="IDrawer2"/> may not
        /// resume until this is done.
        /// </summary>
        public IDisposable ProjectRender(
            Skybox skybox, Dynamic<Viewport> viewport, Dynamic<Tone> tone,
            out IRenderer renderer)
        {
            // TODO: Respect viewport bounds
            Dynamic<Motion3> worldToView = viewport.Map(v => v.WorldToView);
            Dynamic<Projection> viewToDevice = viewport.Map(v => v.ViewToDevice);
            DynamicRenderer sRenderer = new DynamicRenderer(Job, Observer, worldToView, viewToDevice);
            sRenderer.Enter(skybox);
            renderer = sRenderer;
            return sRenderer;
        }

        /// <summary>
        /// Gets the <see cref="DynamicDrawJob"/> which applies the drawing operations specified using this
        /// <see cref="DynamicDrawer2"/>.
        /// </summary>
        public DynamicDrawJob Finish()
        {
            Flush();
            return Job.Finish();
        }

        /// <summary>
        /// Commits all outstanding draw operations to <see cref="Job"/>.
        /// </summary>
        public void Flush()
        {

        }

        void IDisposable.Dispose()
        {
            Flush();
        }
    }

    /// <summary>
    /// A helper for constructing a <see cref="DynamicDrawJob"/> using an <see cref="IDrawer3"/> interface.
    /// </summary>
    public sealed class DynamicDrawer3 : IDynamicDrawer3, IBuilder<DynamicDrawJob>, IDisposable
    {
        internal DynamicDrawer3(DynamicDrawJobBuilder job, Observer obs, Dynamic<Matrix4x4> worldToDevice)
        {
            Job = job;
            Observer = obs;
            WorldToDevice = worldToDevice;
        }

        /// <summary>
        /// The <see cref="DynamicDrawJobBuilder"/> for the job being produced by this drawer. There may be
        /// outstanding draw operations that have not been committed to the job. Use <see cref="Flush"/> to
        /// ensure all operations are committed.
        /// </summary>
        public DynamicDrawJobBuilder Job { get; }

        /// <summary>
        /// The OpenGL instance which maintains the resources for the resulting <see cref="DynamicDrawJob"/>.
        /// </summary>
        public Instance Instance => Job.Instance;

        /// <summary>
        /// The <see cref="Reactive.Observer"/> used to resolve dynamic values that this drawer can't generalize beyond
        /// the current moment.
        /// </summary>
        public Observer Observer { get; }

        /// <summary>
        /// The transformation/projection matrix from world space to device coordinates.
        /// </summary>
        public Dynamic<Matrix4x4> WorldToDevice { get; }

        /// <summary>
        /// Adds a textured mesh to the drawing volume.
        /// </summary>
        public void Draw(PaintTexture2 texture, SpanList<TextureVertex3> verts, SpanList<uint> indices)
        {
            Dynamic<Matrix4x4> worldToDevice = WorldToDevice;
            var vertexArrayBuilder = Instance.BuildVertexArray(VertexFormat.TextureVertex3);
            bool wasWritten = vertexArrayBuilder.TryWrite(verts, indices);
            Debug.Assert(wasWritten);
            var vertexArrayRegion = vertexArrayBuilder.UseFinish(Job.Resources);

            Draw3ProgramInstance progInst = Instance.Use(Draw3Program.Instance);
            Job.Resources.Add(Instance.UseTexture(texture, out TextureObj textureObj));
            Job.Resources.Add(progInst);

            Job.DrawProcedure.Begin(DrawOperationType.UseProgram_ConstProgram);
            Job.DrawProcedure.Append(progInst.ProgramObj);
            Job.DrawProcedure.Begin(DrawOperationType.Uniform_ConstInt32_DynMatrix4x4);
            Job.DrawProcedure.Append(progInst.WorldToDeviceUniformIndex);
            Job.PrepareProcedure.Append((obs, buffer) =>
            {
                buffer.Prepare(64);
                buffer.WriteAlignedNative(worldToDevice[obs]);
            });
            Job.DrawProcedure.Begin(DrawOperationType.Uniform_ConstInt32_ConstInt32);
            Job.DrawProcedure.Append(progInst.ImageUniformIndex);
            Job.DrawProcedure.Append(0);
            Job.DrawProcedure.Begin(DrawOperationType.ActiveTexture_ConstTextureUnit);
            Job.DrawProcedure.Append(TextureUnit.Texture0);
            Job.DrawProcedure.Begin(DrawOperationType.BindTexture_ConstTextureTarget_ConstTexture);
            Job.DrawProcedure.Append(TextureTarget.Texture2D);
            Job.DrawProcedure.Append(textureObj);
            Job.DrawProcedure.Begin(DrawOperationType.BindVertexArray_ConstVertexArray);
            Job.DrawProcedure.Append(vertexArrayRegion.Source);
            Job.DrawProcedure.Begin(DrawOperationType.DrawElements_ConstPrimitiveType_ConstInt32_ConstDrawElementsType_ConstInt32);
            Job.DrawProcedure.Append(PrimitiveType.Triangles);
            Job.DrawProcedure.Append(vertexArrayRegion.NumIndices);
            Job.DrawProcedure.Append(DrawElementsType.UnsignedInt);
            Job.DrawProcedure.Append(vertexArrayRegion.IndexOffset);
        }

        /// <summary>
        /// Gets the <see cref="DynamicDrawJob"/> which applies the drawing operations specified using this
        /// <see cref="DynamicDrawer3"/>.
        /// </summary>
        public DynamicDrawJob Finish()
        {
            Flush();
            return Job.Finish();
        }

        /// <summary>
        /// Commits all outstanding draw operations to <see cref="Job"/>.
        /// </summary>
        public void Flush()
        {

        }

        void IDisposable.Dispose()
        {
            Flush();
        }
    }
}
