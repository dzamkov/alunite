﻿using System;
using System.Runtime.InteropServices;

using Alunite.Data;
using Alunite.Data.Serialization;

namespace Alunite.Graphics.Bindings.OpenGL
{
    /// <summary>
    /// Encapsulates the frame-varying data needed to execute a <see cref="DrawProcedure"/>. This is typically
    /// produced by an <see cref="PrepareProcedure"/>. Note that draw data may hold references to resources
    /// that need to be disposed when the data is no longer needed.
    /// </summary>
    /// <remarks>The seperation of the drawing process into prepare and draw steps with <see cref="DrawBuffer"/> as
    /// an intermediary has several key advantages: first, this ensures that drawing can be done all at once,
    /// with no interruptions from preparing resources. This means the state of the context is predictable at any
    /// point in the draw procedure. Second, this offers great flexibility in when preparations and drawing
    /// are done. Preparation doesn't (directly) execute any drawing commands, so it can be done on any thread.
    /// Since all data needed for drawing is encapsulated in <see cref="DrawBuffer"/>, the drawing thread doesn't
    /// need to perform any additional computations or lookups.
    /// </remarks>
    public struct DrawBuffer : IDisposable
    {
        private readonly byte[] _data;
        private readonly IDisposable[] _resources;
        internal DrawBuffer(byte[] data, IDisposable[] resources)
        {
            _data = data;
            _resources = resources;
        }

        /// <summary>
        /// Constructs a <see cref="DrawBufferReader"/> for reading this buffer.
        /// </summary>
        public DrawBufferReader Read()
        {
            return new DrawBufferReader(_data, _resources);
        }

        /// <summary>
        /// Releases the resources being held by this <see cref="DrawBuffer"/>.
        /// </summary>
        public void Dispose()
        {
            foreach (var resource in _resources)
                resource?.Dispose();
        }
    }

    /// <summary>
    /// A stream-like interface for reading from a <see cref="DrawBuffer"/>.
    /// </summary>
    public sealed class DrawBufferReader : IBinaryReader
    {
        private readonly byte[] _data;
        private readonly IDisposable[] _resources;
        private uint _head;
        private uint _curResourceIndex;
        internal DrawBufferReader(byte[] data, IDisposable[] resources)
        {
            _data = data;
            _resources = resources;
            _curResourceIndex = 0;
        }

        /// <summary>
        /// A buffer containing the next few unread bytes from the stream. The size of this buffer is arbitrary
        /// and variable, however, <see cref="Prepare(uint)"/> can be used populate it up to a given minimum size.
        /// </summary>
        public ReadOnlySpan<byte> Buffer => _data.AsSpan((int)_head);

        /// <summary>
        /// Advances the stream by the given number of bytes.
        /// </summary>
        public void Skip(uint num)
        {
            _head += num;
        }

        /// <summary>
        /// Reads a span of <see cref="float"/>s from the stream using native byte order.
        /// </summary>
        public Span<float> ReadNativeSingleSpan(uint num)
        {
            uint start = _head;
            uint size = num * sizeof(float);
            _head += size;
            return MemoryMarshal.Cast<byte, float>(_data.AsSpan((int)start, (int)size));
        }

        /// <summary>
        /// Reads an arbitrary resource from the <see cref="DrawBuffer"/>.
        /// </summary>
        public IDisposable ReadResource()
        {
            return _resources[_curResourceIndex++];
        }

        void IBinaryReader.Prepare(uint num)
        {
            // Nothing to do here. All the data is already in Buffer.
        }
    }

    /// <summary>
    /// A streaming interface for building a <see cref="DrawBuffer"/>.
    /// </summary>
    public sealed class DrawBufferBuilder : IBinaryWriter, IBuilder<DrawBuffer>
    {
        private byte[] _data;
        private IDisposable[] _resources;
        private uint _head;
        private uint _nextResourceIndex;
        private DrawBufferBuilder()
        {
            _data = Array.Empty<byte>();
            _resources = Array.Empty<IDisposable>();
            _head = 0;
            _nextResourceIndex = 0;
        }

        /// <summary>
        /// Creates a new initially-empty <see cref="DrawBufferBuilder"/>.
        /// </summary>
        public static DrawBufferBuilder Create()
        {
            return new DrawBufferBuilder();
        }

        /// <summary>
        /// A buffer containing the next few bytes to be written to the stream. The size of this buffer is arbitrary
        /// and variable, however, <see cref="Prepare(uint)"/> can be used allocate up to a given minimum size.
        /// </summary>
        public Span<byte> Buffer => _data.AsSpan((int)_head);

        /// <summary>
        /// Removes the given number of bytes from the beginning of <see cref="Buffer"/> and writes them to the stream.
        /// </summary>
        public void Flush(uint num)
        {
            _head += num;
        }

        /// <summary>
        /// Ensures that <see cref="Buffer"/> has room for at least the given number of bytes.
        /// </summary>
        public void Prepare(uint num)
        {
            if ((_data.Length - _head) < num)
            {
                uint nDataSize = Bitwise.UpperPow2(_head + num);
                byte[] nData = new byte[nDataSize];
                Binary.Copy(_data.AsSpan(0, (int)_head), nData.AsSpan(0, (int)_head));
                _data = nData;
            }
        }

        /// <summary>
        /// Writes an arbitrary resource to the resulting <see cref="DrawBuffer"/>. This will be
        /// disposed when the <see cref="DrawBuffer"/> is disposed.
        /// </summary>
        public void WriteResource(IDisposable resource)
        {
            Table.Allocate(ref _resources, _nextResourceIndex, 1);
            _resources[_nextResourceIndex++] = resource;
        }

        /// <summary>
        /// Gets the <see cref="DrawBuffer"/> resulting from this builder.
        /// </summary>
        public DrawBuffer Finish()
        {
            return new DrawBuffer(_data, _resources);
        }
    }
}