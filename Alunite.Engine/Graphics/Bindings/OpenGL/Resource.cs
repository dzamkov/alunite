﻿using System;

using Alunite.Data;
using Alunite.Resources;

namespace Alunite.Graphics.Bindings.OpenGL
{
    /// <summary>
    /// An abstract description of a graphical resource that can be instantiated by an <see cref="Instance"/>.
    /// </summary>
    public abstract class Resource
    {

    }

    /// <summary>
    /// A <see cref="Resource"/> that is instantiated as a <typeparamref name="TInstance"/>.
    /// </summary>
    public abstract class Resource<TInstance> : Resource
        where TInstance : ResourceInstance
    {
        /// <summary>
        /// Creates an instance of this resource for the given <see cref="Instance"/>. Note that this might not be
        /// called on the graphics thread, so manual synchronization may be needed.
        /// </summary>
        protected internal abstract TInstance Instantiate(Instance owner);
    }

    /// <summary>
    /// Describes an instantiation of a <see cref="Resource"/> owned by an <see cref="Instance"/>. This is
    /// reference-counted to track usage, but deletion is ultimately controlled by the
    /// <see cref="Instance"/>.
    /// </summary>
    public class ResourceInstance : IDisposable, ITransient
    {
        public ResourceInstance(Instance owner)
        {
            Owner = owner;
        }

        /// <summary>
        /// The <see cref="Instance"/> owning this <see cref="ResourceInstance"/>.
        /// </summary>
        public Instance Owner { get; }

        /// <summary>
        /// Indicates whether this <see cref="ResourceInstance"/> can accept additional users. Note that
        /// this <em>does not</em> indicate whether the resource currently has users. Once this becomes
        /// <see cref="false"/>, it will remain so forever.
        /// </summary>
        public bool IsAlive => true;

        /// <summary>
        /// Attempts to add a usage to the resource instance, returning false if this is not possible. If
        /// this returns true, <see cref="Dispose"/> must be called one extra time.
        /// </summary>
        public bool TryUse()
        {
            return true;
        }

        /// <summary>
        /// Removes a user from the resource instance.
        /// </summary>
        public void Dispose()
        {
            // TODO
        }
    }

    partial class Instance
    {
        /// <summary>
        /// Gets or creates an instance of <paramref name="resource"/> for use with this <see cref="Instance"/>. The
        /// instance should be disposed once no longer needed.
        /// </summary>
        public TInstance Use<TInstance>(Resource<TInstance> resource)
            where TInstance : ResourceInstance
        {
            // Check registry for suitable pre-existing instance
            var viewer = _resourceInsts.View(resource);
            using (var it = viewer.Iterate())
            {
                while (it.TryNext(out ResourceInstance inst))
                {
                    if (inst.TryUse())
                        return (TInstance)inst;
                    else
                        it.Clean();
                }
            }

            // Check again while the registry is locked
            if (!viewer.TryBrowse(out var browser))
                browser = _resourceInsts.Browse(resource);
            try
            {
                using (var it = browser.Iterate())
                {
                    while (it.TryNext(out ResourceInstance inst))
                    {
                        if (inst.TryUse())
                            return (TInstance)inst;
                        else
                            it.Clean();
                    }
                }

                // Create instance
                TInstance res = resource.Instantiate(this);
                browser.Add(res);
                return res;
            }
            finally
            {
                // Release lock on registry entry
                browser.Dispose();
            }
        }


        /// <summary>
        /// A generic registry for <see cref="ResourceInstance"/>s.
        /// </summary>
        private readonly Registry<Resource, ResourceInstance> _resourceInsts
            = Registry<Resource, ResourceInstance>.CreateEmpty();
    }
}
