﻿using System;

using Alunite.Data;
using Alunite.Resources;

namespace Alunite.Graphics.Bindings
{
    /// <summary>
    /// A helper class for processing directives and expanding macros within raw GLSL source input.
    /// </summary>
    public sealed class GlslPreprocessor
    {
        private MapBuilder<string, GlslMacroInfo> _macros;

        /// <summary>
        /// Creates a new <see cref="GlslPreprocessor"/> with a clean initial state.
        /// </summary>
        public static GlslPreprocessor CreateInitial()
        {
            return new GlslPreprocessor
            {
                _macros = MapBuilder<string, GlslMacroInfo>.CreateEmpty()
            };
        }

        /// <summary>
        /// Creates a new <see cref="GlslPreprocessor"/> with the given set of initial macros.
        /// </summary>
        public static GlslPreprocessor Create(Map<string, GlslMacroInfo> macros)
        {
            return new GlslPreprocessor
            {
                _macros = MapBuilder<string, GlslMacroInfo>.Create(macros)
            };
        }

        /// <summary>
        /// Defines a preprocessor macro with the given name.
        /// </summary>
        public void Define(string name, GlslMacroInfo def)
        {
            _macros.Add(name, def);
        }

        /// <summary>
        /// Shortcut for <see cref="Define"/> with <see cref="GlslMacroInfo.Empty"/>.
        /// </summary>
        public void Define(string name)
        {
            _macros.Add(name, GlslMacroInfo.Empty);
        }

        /// <summary>
        /// Attempts to process a file and write the transformed GLSL to <paramref name="output"/>. Returns
        /// <see cref="false"/> if the file is not found.
        /// </summary>
        public bool TryProcessFile(
            IListWriter<char> output,
            RepositoryPathViewer path)
        {
            if (path.TryResolveAsFile(out BigBinary file))
            {
                ProcessFile(output, path, file);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Processes a file and write the transformed GLSL to <paramref name="output"/>.
        /// </summary>
        public void ProcessFile(
            IListWriter<char> output,
            RepositoryPathViewer path,
            BigBinary file)
        {
            using (file.Read(0, out var binaryReader))
            using (binaryReader.DecodeUtf8(out var stringReader))
            {
                stringReader.Buffered(out IBufferedListReader<char> bufferedReader);
                ProcessDocument(output, path.Parent, bufferedReader);
            }
        }

        /// <summary>
        /// Processes a file (given by <paramref name="input"/>) and writes the transformed GLSL to
        /// <paramref name="output"/>.
        /// </summary>
        /// <param name="path">The directory in which the input is being processed. This is used to resolve included
        /// files.</param>
        public unsafe void ProcessDocument(
            IListWriter<char> output,
            RepositoryPathViewer path,
            IBufferedListReader<char> input)
        {
            // Declare variables used during processing
            SpanList<char> buffer = input.Buffer;
            uint head = 0;
            uint ifDepth = 0;

        atLineStart:
            switch (_readChar(ref buffer, ref head, input, output))
            {
                case '\0':
                    goto end;
                case '\t':
                case '\n':
                case ' ':
                    goto atLineStart;
                case '\r':
                    output.Write(buffer.SlicePrefix(head - 1));
                    input.Skip(head);
                    buffer = input.Buffer;
                    head = 0;
                    goto atLineStart;
                case '#':
                    output.Write(buffer.SlicePrefix(head - 1));
                    input.Skip(head);
                    goto inDirective;
                #region case 'A' .. 'Z':
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                case 'G':
                case 'H':
                case 'I':
                case 'J':
                case 'K':
                case 'L':
                case 'M':
                case 'N':
                case 'O':
                case 'P':
                case 'Q':
                case 'R':
                case 'S':
                case 'T':
                case 'U':
                case 'V':
                case 'W':
                case 'X':
                case 'Y':
                case 'Z':
                #endregion
                case '_':
                #region case 'a' .. 'z':
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                case 'g':
                case 'h':
                case 'i':
                case 'j':
                case 'k':
                case 'l':
                case 'm':
                case 'n':
                case 'o':
                case 'p':
                case 'q':
                case 'r':
                case 's':
                case 't':
                case 'u':
                case 'v':
                case 'w':
                case 'x':
                case 'y':
                case 'z':
                    #endregion
                    output.Write(buffer.SlicePrefix(head - 1));
                    input.Skip(head - 1);
                    goto inIdent;
                case '\uFEFF': // Byte order mark or zero-width space
                    output.Write(buffer.SlicePrefix(head - 1));
                    input.Skip(head);
                    buffer = input.Buffer;
                    head = 0;
                    goto atLineStart;
                default:
                    goto normal;
            }

        normal:
            switch (_readChar(ref buffer, ref head, input, output))
            {
                case '\0':
                    goto end;
                case '\n':
                    goto atLineStart;
                case '\r':
                    output.Write(buffer.SlicePrefix(head - 1));
                    input.Skip(head);
                    buffer = input.Buffer;
                    head = 0;
                    goto normal;
                #region case 'A' .. 'Z':
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                case 'G':
                case 'H':
                case 'I':
                case 'J':
                case 'K':
                case 'L':
                case 'M':
                case 'N':
                case 'O':
                case 'P':
                case 'Q':
                case 'R':
                case 'S':
                case 'T':
                case 'U':
                case 'V':
                case 'W':
                case 'X':
                case 'Y':
                case 'Z':
                #endregion
                case '_':
                #region case 'a' .. 'z':
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                case 'g':
                case 'h':
                case 'i':
                case 'j':
                case 'k':
                case 'l':
                case 'm':
                case 'n':
                case 'o':
                case 'p':
                case 'q':
                case 'r':
                case 's':
                case 't':
                case 'u':
                case 'v':
                case 'w':
                case 'x':
                case 'y':
                case 'z':
                    #endregion
                    output.Write(buffer.SlicePrefix(head - 1));
                    input.Skip(head - 1);
                    goto inIdent;
                default:
                    goto normal;
            }

        inDirective:
            string macroName;
            uint? macroNumParams;
            Map<string, uint> macroArgs;
            GlslMacroBodyBuilder macroBodyBuilder;
            {
                // Parse directive
                if (!input.AcceptClass(CharSet.Alphabetic, out string directive))
                    throw new Exception("Expected directive");
                switch(directive)
                {
                    case "include":
                        {
                            if (input.AcceptWhitespace())
                            {
                                if (input.AcceptStringLiteral(out string includePathStr))
                                {
                                    var includePath = path / RelativePath.Parse(includePathStr);
                                    if (!TryProcessFile(output, includePath))
                                        throw new Exception("Included file not found");
                                    buffer = input.Buffer;
                                    head = 0;
                                    goto endDirective;
                                }
                            }
                            throw new Exception("Invalid directive");
                        }
                    case "define":
                        {
                            if (input.AcceptWhitespace())
                            {
                                if (_acceptIdent(input, out macroName))
                                {
                                    if (input.AcceptWhitespace())
                                    {
                                        macroNumParams = null;
                                        macroArgs = Map<string, uint>.Empty;
                                        macroBodyBuilder = GlslMacroBodyBuilder.CreateEmpty();
                                        buffer = input.Buffer;
                                        head = 0;
                                        goto macroBodyNormal;
                                    }
                                    else if (input.AcceptItem('('))
                                    {
                                        throw new NotImplementedException();
                                    }
                                }
                            }
                            throw new Exception("Invalid directive");
                        }
                    case "undef":
                        throw new NotImplementedException();
                    case "if":
                        throw new NotImplementedException();
                    case "ifdef":
                        {
                            if (input.AcceptWhitespace())
                            {
                                if (_acceptIdent(input, out string ident))
                                {
                                    if (!_macros.ContainsKey(ident))
                                        ifDepth++;
                                    buffer = input.Buffer;
                                    head = 0;
                                    goto endDirective;
                                }
                            }
                            throw new Exception("Invalid directive");
                        }
                    case "ifndef":
                        {
                            if (input.AcceptWhitespace())
                            {
                                if (_acceptIdent(input, out string ident))
                                {
                                    if (_macros.ContainsKey(ident))
                                        ifDepth++;
                                    buffer = input.Buffer;
                                    head = 0;
                                    goto endDirective;
                                }
                            }
                            throw new Exception("Invalid directive");
                        }
                    case "else":
                        ifDepth++;
                        buffer = input.Buffer;
                        head = 0;
                        goto endDirective;
                    case "elif":
                        throw new NotImplementedException();
                    case "endif":
                        buffer = input.Buffer;
                        head = 0;
                        goto endDirective;
                    default:
                        // Pass unrecognized directive through unchanged
                        output.WriteItem('#');
                        output.Write(directive);
                        buffer = input.Buffer;
                        head = 0;
                        goto passDirective;
                }
            }

        skipAtLineStart:
            switch (_readChar(ref buffer, ref head, input))
            {
                case '\0':
                    goto end;
                case '\t':
                case '\n':
                case ' ':
                case '\r':
                case '\uFEFF':
                    goto skipAtLineStart;
                case '#':
                    input.Skip(head);
                    goto skipInDirective;
                default:
                    goto skipNormal;
            }

        skipNormal:
            switch (_readChar(ref buffer, ref head, input))
            {
                case '\0':
                    goto end;
                case '\n':
                    goto skipAtLineStart;
                default:
                    goto skipNormal;
            }

        skipInDirective:
            {
                // Parse directive
                if (!input.AcceptClass(CharSet.Alphabetic, out string directive))
                    throw new Exception("Expected directive");
                switch (directive)
                {
                    case "if":
                    case "ifdef":
                    case "ifndef":
                        ifDepth++;
                        buffer = input.Buffer;
                        head = 0;
                        goto endDirective;
                    case "else":
                        if (ifDepth == 1)
                            ifDepth = 0;
                        buffer = input.Buffer;
                        head = 0;
                        goto endDirective;
                    case "elif":
                        throw new NotImplementedException();
                    case "endif":
                        ifDepth--;
                        buffer = input.Buffer;
                        head = 0;
                        goto endDirective;
                    default:
                        // Ignore all other directives
                        buffer = input.Buffer;
                        head = 0;
                        goto endDirective;
                }
            }

        macroBodyNormal:
            switch (_readChar(ref buffer, ref head, input, macroBodyBuilder))
            {
                case '\0':
                    Define(macroName, new GlslMacroInfo(macroNumParams, macroBodyBuilder.Finish()));
                    goto end;
                case '\n':
                    macroBodyBuilder.Write(buffer.SlicePrefix(head - 1));
                    Define(macroName, new GlslMacroInfo(macroNumParams, macroBodyBuilder.Finish()));
                    input.Skip(head);
                    buffer = input.Buffer;
                    head = 0;
                    goto atLineStart;
                case '\r':
                    macroBodyBuilder.Write(buffer.SlicePrefix(head - 1));
                    input.Skip(head);
                    buffer = input.Buffer;
                    head = 0;
                    goto macroBodyNormal;
                #region case 'A' .. 'Z':
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                case 'G':
                case 'H':
                case 'I':
                case 'J':
                case 'K':
                case 'L':
                case 'M':
                case 'N':
                case 'O':
                case 'P':
                case 'Q':
                case 'R':
                case 'S':
                case 'T':
                case 'U':
                case 'V':
                case 'W':
                case 'X':
                case 'Y':
                case 'Z':
                #endregion
                case '_':
                #region case 'a' .. 'z':
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                case 'g':
                case 'h':
                case 'i':
                case 'j':
                case 'k':
                case 'l':
                case 'm':
                case 'n':
                case 'o':
                case 'p':
                case 'q':
                case 'r':
                case 's':
                case 't':
                case 'u':
                case 'v':
                case 'w':
                case 'x':
                case 'y':
                case 'z':
                    #endregion
                    macroBodyBuilder.Write(buffer.SlicePrefix(head - 1));
                    input.Skip(head - 1);
                    Util.Assert(input.AcceptClass(_identChars, out string ident));
                    if (macroArgs.TryGet(ident, out uint argIndex))
                        macroBodyBuilder.WriteArgument(argIndex);
                    else
                        macroBodyBuilder.Write(ident);
                    buffer = input.Buffer;
                    head = 0;
                    goto macroBodyNormal;
                default:
                    goto macroBodyNormal;
            }

        endDirective:
            switch (_readChar(ref buffer, ref head, input))
            {
                case '\0':
                    goto end;
                case '\n':
                    input.Skip(head);
                    buffer = input.Buffer;
                    head = 0;
                    if (ifDepth > 0)
                        goto skipAtLineStart;
                    else
                        goto atLineStart;
                default:
                    goto endDirective;
            }

        passDirective:
            switch (_readChar(ref buffer, ref head, input, output))
            {
                case '\0':
                    goto end;
                case '\n':
                    goto atLineStart;
                case '\r':
                    output.Write(buffer.SlicePrefix(head - 1));
                    input.Skip(head);
                    buffer = input.Buffer;
                    head = 0;
                    goto passDirective;
                default:
                    goto passDirective;
            }

        inIdent:
            {
                Util.Assert(input.AcceptClass(_identChars, out string ident));
                _processIdent(output, input, ident);
                buffer = input.Buffer;
                head = 0;
                goto normal;
            }

        end:;
        }

        /// <summary>
        /// Reads the next character from a buffer or returns the null character if the end of <paramref name="input"/>
        /// has been reached.
        /// </summary>
        private static char _readChar(
            ref SpanList<char> buffer,
            ref uint head,
            IBufferedListReader<char> input)
        {
        retry:
            if (head < buffer.Length)
            {
                return buffer[head++];
            }
            else
            {
                if (buffer.Length >= ListReader<char>.MinBufferLength)
                {
                    // Repopulate buffer
                    input.Skip(head);
                    buffer = input.Buffer;
                    head = 0;
                    goto retry;
                }
                else
                {
                    return '\0';
                }
            }
        }

        /// <summary>
        /// Reads the next character from a buffer or returns the null character if the end of <paramref name="input"/>
        /// has been reached. This will automatically repopulating the buffer as needed and copy buffered characters
        /// to <paramref name="output"/> upon repopulating the buffer.
        /// </summary>
        private static char _readChar(
            ref SpanList<char> buffer,
            ref uint head,
            IBufferedListReader<char> input,
            IListWriter<char> output)
        {
        retry:
            if (head < buffer.Length)
            {
                return buffer[head++];
            }
            else
            {
                output.Write(buffer);
                if (buffer.Length >= ListReader<char>.MinBufferLength)
                {
                    // Repopulate buffer
                    input.Skip(head);
                    buffer = input.Buffer;
                    head = 0;
                    goto retry;
                }
                else
                {
                    return '\0';
                }
            }
        }

        /// <summary>
        /// Greedily parses a GLSL identifier.
        /// </summary>
        private static unsafe bool _acceptIdent(IBufferedListReader<char> input, out string ident)
        {
            var buffer = input.Buffer;
            if (buffer.Length > 0 && _identStartChars.Contains(buffer[0]))
                return input.AcceptClass(_identChars, out ident);
            ident = default;
            return false;
        }

        /// <summary>
        /// The set of allowable characters at the start of a GLSL identifier.
        /// </summary>
        private static readonly CharSet _identStartChars = CharSet.Of('_') | CharSet.Alphabetic;

        /// <summary>
        /// The set of allowable characters inside a GLSL identifier.
        /// </summary>
        private static readonly CharSet _identChars = _identStartChars | CharSet.Numeric;

        /// <summary>
        /// Processes an appearance of an identifier within a string.
        /// </summary>
        private void _processIdent(IListWriter<char> output, IBufferedListReader<char> input, string ident)
        {
            if (_macros.TryGet(ident, out GlslMacroInfo macroInfo))
            {
                if (macroInfo.NumParameters is null)
                {
                    macroInfo.Body.Expand(output, this, List.Empty<string>());
                }
                else if (input.AcceptItem('('))
                {
                    throw new NotImplementedException();
                }
            }
            else
            {
                output.Write(ident);
            }
        }

        /// <summary>
        /// Processes a string and writes the transformed GLSL to <paramref name="output"/>. Directives within the
        /// string are ignored, but macros are expanded.
        /// </summary>
        public void ProcessString(IListWriter<char> output, string str)
        {
            ProcessString(output, StringReader.Create(str));
        }

        /// <summary>
        /// Processes a string and writes the transformed GLSL to <paramref name="output"/>. Directives within the
        /// string are ignored, but macros are expanded.
        /// </summary>
        public unsafe void ProcessString(IListWriter<char> output, IBufferedListReader<char> input)
        {
            // Declare variables used during processing
            SpanList<char> buffer = input.Buffer;
            uint head = 0;

        normal:
            switch (_readChar(ref buffer, ref head, input, output))
            {
                case '\0':
                    goto end;
                #region case 'A' .. 'Z':
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                case 'G':
                case 'H':
                case 'I':
                case 'J':
                case 'K':
                case 'L':
                case 'M':
                case 'N':
                case 'O':
                case 'P':
                case 'Q':
                case 'R':
                case 'S':
                case 'T':
                case 'U':
                case 'V':
                case 'W':
                case 'X':
                case 'Y':
                case 'Z':
                #endregion
                case '_':
                #region case 'a' .. 'z':
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                case 'g':
                case 'h':
                case 'i':
                case 'j':
                case 'k':
                case 'l':
                case 'm':
                case 'n':
                case 'o':
                case 'p':
                case 'q':
                case 'r':
                case 's':
                case 't':
                case 'u':
                case 'v':
                case 'w':
                case 'x':
                case 'y':
                case 'z':
                    #endregion
                    output.Write(buffer.SlicePrefix(head - 1));
                    input.Skip(head - 1);
                    goto inIdentifier;
                default:
                    goto normal;
            }

        inIdentifier:
            {
                Util.Assert(input.AcceptClass(_identChars, out string ident));
                _processIdent(output, input, ident);
                buffer = input.Buffer;
                head = 0;
                goto normal;
            }

        end:;
        }
    }

    /// <summary>
    /// Provides the definition for a preprocessor macro to be used with a <see cref="GlslPreprocessor"/>.
    /// </summary>
    public struct GlslMacroInfo
    {
        internal GlslMacroInfo(uint? numParams, GlslMacroBody body)
        {
            NumParameters = numParams;
            Body = body;
        }

        /// <summary>
        /// The number of parameters accepted by this macro, or <see cref="null"/> for object-like macros.
        /// </summary>
        public uint? NumParameters { get; }
        
        /// <summary>
        /// The body of this macro.
        /// </summary>
        public GlslMacroBody Body { get; }

        /// <summary>
        /// The <see cref="GlslMacroInfo"/> for the empty string.
        /// </summary>
        public static GlslMacroInfo Empty => new GlslMacroInfo(null, "");

        public static implicit operator GlslMacroInfo(string literal)
        {
            return new GlslMacroInfo(null, literal);
        }
    }

    /// <summary>
    /// Describes the body of a preprocessor macro to be used with a <see cref="GlslPreprocessor"/>. This is a
    /// concatenation of literal strings along with argument references.
    /// </summary>
    public struct GlslMacroBody
    {
        /// <summary>
        /// The literal content of this <see cref="GlslMacroBody"/> all concatenated together.
        /// </summary>
        internal string _literal;

        /// <summary>
        /// The argument references made in this <see cref="GlslMacroBody"/>, sorted by order of appearance.
        /// </summary>
        internal List<_ArgumentRef> _argRefs;

        /// <summary>
        /// Describes a usage of an argument within a <see cref="GlslMacroBody"/>.
        /// </summary>
        internal struct _ArgumentRef
        {
            public _ArgumentRef(uint offset, uint index)
            {
                Offset = offset;
                Index = index;
            }

            /// <summary>
            /// The location within <see cref="_literal"/> where this argument reference is added.
            /// </summary>
            public uint Offset;

            /// <summary>
            /// The index of the argument this is a reference to.
            /// </summary>
            public uint Index;
        }

        /// <summary>
        /// Constructs a <see cref="GlslMacroBody"/> which expands to the value of the argument with the
        /// given index.
        /// </summary>
        public static GlslMacroBody Argument(uint index)
        {
            return new GlslMacroBody
            {
                _literal = string.Empty,
                _argRefs = List.Of(new _ArgumentRef(0, index))
            };
        }
        
        // TODO: Should be able to expand a Map instead of a MapBuilder
        /// <summary>
        /// Recursively instantiates and expands this <see cref="GlslMacroBody"/>.
        /// </summary>
        /// <param name="args">The arguments for the macro, prior to prescan.</param>
        public void Expand(IListWriter<char> output, GlslPreprocessor preprocessor, SpanList<string> args)
        {
            preprocessor.ProcessString(output, _literal);
        }

        public static GlslMacroBody operator +(GlslMacroBody a, GlslMacroBody b)
        {
            List<_ArgumentRef> argRefs = List.Empty<_ArgumentRef>();
            uint numArgRefs = a._argRefs.Length + b._argRefs.Length;
            if (numArgRefs > 0)
            {
                _ArgumentRef[] argRefItems = new _ArgumentRef[numArgRefs];
                for (uint i = 0; i < a._argRefs.Length; i++)
                    argRefItems[i] = a._argRefs[i];
                for (uint i = 0; i < b._argRefs.Length; i++)
                {
                    var bArgRef = b._argRefs[i];
                    argRefItems[a._argRefs.Length + i] = new _ArgumentRef(
                        (uint)a._literal.Length + bArgRef.Offset,
                        bArgRef.Index);
                }
                argRefs = new List<_ArgumentRef>(argRefItems);
            }
            return new GlslMacroBody
            {
                _literal = a._literal + b._literal
            };
        }

        public static implicit operator GlslMacroBody(string literal)
        {
            return new GlslMacroBody
            {
                _literal = literal,
                _argRefs = List.Empty<_ArgumentRef>()
            };
        }
    }

    /// <summary>
    /// A helper class for constructing a <see cref="GlslMacroBody"/> by incrementally appending content.
    /// </summary>
    public struct GlslMacroBodyBuilder : IListWriter<char>, IBuilder<GlslMacroBody>
    {
        private StringBuilder _literal;
        private ListBuilder<GlslMacroBody._ArgumentRef> _argRefs;

        /// <summary>
        /// Creates a new initially-empty <see cref="GlslMacroBodyBuilder"/>.
        /// </summary>
        public static GlslMacroBodyBuilder CreateEmpty()
        {
            return new GlslMacroBodyBuilder
            {
                _literal = StringBuilder.CreateEmpty(),
                _argRefs = ListBuilder<GlslMacroBody._ArgumentRef>.CreateEmpty()
            };
        }

        /// <summary>
        /// Appends the given literal characters to the <see cref="GlslMacroBody"/>.
        /// </summary>
        public unsafe void Write(ReadOnlySpan<char> chs)
        {
            _literal.Write(chs);
        }

        /// <summary>
        /// Appends the expansion of the argument with the given index.
        /// </summary>
        public void WriteArgument(uint index)
        {
            _argRefs.Append(new GlslMacroBody._ArgumentRef(_literal.Length, index));
        }

        /// <summary>
        /// Gets the <see cref="GlslMacroBody"/> that was constructed using this <see cref="GlslMacroBodyBuilder"/>.
        /// </summary>
        public GlslMacroBody Finish()
        {
            return new GlslMacroBody
            {
                _literal = _literal.Finish(),
                _argRefs = _argRefs.Finish()
            };
        }
    }
}
