﻿#version 400
layout(location = 0) in vec2 attrPos;
out vec2 pos;

void main() {
	pos = attrPos;
	gl_Position = vec4(pos, 0, 1);
}