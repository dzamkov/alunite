﻿#define PI 3.1415926535897932384626433832795

// Gets a "random" vector uniformly distributed on ([0, 1], [0, 1]) by taking a vector from the
// low-discrepancy Hammersley sequence.
vec2 sampleUniform2Hammersley(uint i, uint n) {
	uint bits = i;
	bits = (bits << 16u) | (bits >> 16u);
	bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
	bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
	bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
	bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
	return vec2(float(i) / float(n), float(bits) * 2.3283064365386963e-10);
}

// Given a random vector uniformly distributed on ([0, 1], [0, 1]), generates a random
// vector from the probability distribution of unit vectors with Z > 0 where each vector
// has probability proportional to Z.
vec3 sampleCosineHemisphereFromUniform2(vec2 source) {
	float phi = source.x * (2 * PI);
	float r = sqrt(source.y);
	return vec3(r * cos(phi), r * sin(phi), sqrt(1 - r * r));
}

// Given a random vector uniformly distributed on ([0, 1], [0, 1]), generates a random
// vector from the cosine-weighted ndfTrowbridge distribution with the given alpha value.
vec3 sampleCosineNdfTrowbridgeFromUniform2(float roughness, vec2 source) {
	float alpha = roughness * roughness;
    float phi = source.x * (2 * PI);
    float z = sqrt((1 - source.y) / (1 + (alpha * alpha - 1) * source.y));
    float r = sqrt(1 - z * z);
	return vec3(r * cos(phi), r * sin(phi), z);
}

#define sampleCosineNdfFromUniform2 sampleCosineNdfTrowbridgeFromUniform2

vec3 fresnelSchlick(vec3 directSpecular, vec3 grazingSpecular, float eyeDotHalf) {
    return directSpecular + (grazingSpecular - directSpecular) * pow(1 - eyeDotHalf, 5);
}

float visSmith(float roughness, float lightDotNormal, float eyeDotNormal) {
	float alpha = roughness * roughness;
    float k = alpha / 2;
	return (lightDotNormal / (lightDotNormal * (1 - k) + k)) * (eyeDotNormal / (eyeDotNormal * (1 - k) + k));
}

float ndfTrowbridge(float halfDotNormal, float roughness) {
    float alpha = roughness * roughness;
    float f = (halfDotNormal * halfDotNormal) * (alpha - 1) + 1;
    return alpha / (PI * f * f);
}

#define fresnel fresnelSchlick
#define vis visSmith
#define ndf ndfTrowbridge