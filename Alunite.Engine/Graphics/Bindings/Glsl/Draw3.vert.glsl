﻿#version 400
uniform mat4 worldToDevice;
layout(location = 0) in vec3 pos;
layout(location = 1) in vec2 uvIn;
out vec2 uv;

void main() {
	uv = uvIn;
	gl_Position = worldToDevice * vec4(pos, 1);
}