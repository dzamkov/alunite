﻿#version 400
uniform samplerCube specularTexture;
in vec3 dir;
out vec4 color;

void main() {
	if (dir.z < 0) // TODO: remove once no longer necessary
		discard;
	color = textureLod(specularTexture, dir, 0);
}