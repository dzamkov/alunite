﻿#version 400
uniform vec2 targetSize;
layout(location = 0) in vec2 pos;
layout(location = 1) in vec2 uvIn;
out vec2 uv;

void main() {
	uv = uvIn;
	gl_Position = vec4(
		pos.x / targetSize.x * 2 - 1,
		pos.y / targetSize.y * -2 + 1,
		0, 1);
}