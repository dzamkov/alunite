﻿#version 400
precision highp float;
in vec2 pos;

#include "Pbr.glsl"

void main() {
	float eyeDotNormal = (pos.x + 1) / 2;
	vec3 eyeDir = vec3(-sqrt(1 - eyeDotNormal * eyeDotNormal), 0, eyeDotNormal);
	vec3 normalDir = vec3(0, 0, 1);
	float roughness = (pos.y + 1) / 2;

	uint numSamples = 1024;
	float directSpecularFactor = 0;
	float grazingSpecularFactor = 0;
	float diffuseFactor = 0;
	for (uint i = 0; i < numSamples; i++) {
		vec2 xi = sampleUniform2Hammersley(i, numSamples);
		vec3 halfDir = sampleCosineNdfTrowbridgeFromUniform2(roughness, xi);
		float halfDotNormal = halfDir.z;
		float eyeDotHalf = dot(eyeDir, halfDir);
		if (eyeDotHalf > 0) {
			vec3 lightDir = -normalize(reflect(eyeDir, halfDir));
			float lightDotNormal = lightDir.z;
			if (lightDotNormal > 0) {
				float scale = eyeDotHalf / (eyeDotNormal * halfDotNormal);
				float _vis = vis(roughness, lightDotNormal, eyeDotNormal);
				float fresnelFactor = pow(1 - eyeDotHalf, 5);
				directSpecularFactor += (1 - fresnelFactor) * _vis * scale;
				grazingSpecularFactor += fresnelFactor * _vis * scale;
				diffuseFactor += (1 - fresnelFactor) * _vis * scale;
			}
		}
	}
	directSpecularFactor /= float(numSamples);
	grazingSpecularFactor /= float(numSamples);
	diffuseFactor = 1 - grazingSpecularFactor;
	gl_FragColor = vec4(directSpecularFactor, grazingSpecularFactor, diffuseFactor, 1);
}