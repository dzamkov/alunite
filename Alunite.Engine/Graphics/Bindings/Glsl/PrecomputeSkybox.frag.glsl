﻿#version 400
#define PI 3.1415926535897932384626433832795
uniform sampler2D equirectangularSkyboxTexture;
#ifdef SPECULAR
	uniform float level;
#endif
in vec3 dir;
out vec4 color;

#include "Pbr.glsl"

const vec2 invAtan = vec2(-0.1591549, -0.3183099);
vec3 getSkyboxRadiance(vec3 dir, float lod) {
	vec3 ndir = normalize(dir);
	vec2 uv = vec2(atan(ndir.y, ndir.x), asin(ndir.z));
	uv *= invAtan;
	uv += vec2(0.5);
	return textureLod(equirectangularSkyboxTexture, uv, lod).rgb;
}

void main() {
	vec3 ndir = normalize(dir);
	vec3 up = vec3(0, 0, 1);
	vec3 left = normalize(cross(up, ndir));
	up = normalize(cross(ndir, left));
	mat3 localTrans = mat3(left, up, ndir);

	#ifdef DIFFUSE
		vec3 irradiance = vec3(0);
		uint numSamples = 20000;
		for (uint i = 0; i < numSamples; i++) {
			vec2 xi = sampleUniform2Hammersley(i, numSamples);
			vec3 local = sampleCosineHemisphereFromUniform2(xi);
			irradiance += getSkyboxRadiance(localTrans * local, 4);
		}
		irradiance /= float(numSamples);
		color = vec4(irradiance, 1);
	#else
		float roughness = level;
		vec3 radiance = vec3(0);
		uint numSamples = uint(1 + roughness * 40000);
		for (uint i = 0; i < numSamples; i++) {
			vec2 xi = sampleUniform2Hammersley(i, numSamples);
			vec3 local = sampleCosineNdfTrowbridgeFromUniform2(roughness, xi);
			local = reflect(vec3(0, 0, -1), local);
			if (local.z > 0)
				radiance += getSkyboxRadiance(localTrans * local, pow(roughness, 0.2) * 6);
		}
		radiance /= float(numSamples);
		color = vec4(radiance, 1);
	#endif
}