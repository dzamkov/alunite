﻿#version 400
uniform mat3 transform;
layout(location = 0) in vec2 attrPos;
out vec3 dir;

void main() {
	dir = transform * vec3(attrPos, 1);
	gl_Position = vec4(attrPos, 0, 1);
}