﻿#version 400
#define PI 3.1415926535897932384626433832795
uniform sampler2D brdfTexture;
uniform samplerCube skyboxDiffuseTexture;
uniform samplerCube skyboxSpecularTexture;
uniform int skyboxSpecularNumLevels;
uniform sampler2DArray albedoTexture;
uniform sampler2DArray normalTexture;
uniform sampler2DArray metalnessTexture;
uniform sampler2DArray roughnessTexture;
uniform vec4 eyeBias;
in vec3 worldPos;
in mat3 tbn[NUM_BLEND_FACTORS];
in vec3 matCoordIndex[NUM_BLEND_FACTORS];
in float matAlpha[NUM_BLEND_FACTORS];
out vec4 color;

#include "Pbr.glsl"

void main() {
	vec3 eyeDir = -normalize(worldPos * eyeBias.w + eyeBias.xyz);

	// Get base material properties from first material
	vec4 albedo = texture(albedoTexture, matCoordIndex[0]) * abs(matAlpha[0]);
	vec3 baseNormalDir = texture(normalTexture, matCoordIndex[0]).rgb * albedo.a;
	float metalness = texture(metalnessTexture, matCoordIndex[0]).r * albedo.a;
	float roughness = texture(roughnessTexture, matCoordIndex[0]).r * albedo.a;
	vec3 normalDir = tbn[0] * baseNormalDir;

	// Blend material properties from additional materials
	#pragma optionNV (unroll all)
	for (int i = 1; i < NUM_BLEND_FACTORS; i++) {
		vec4 sAlbedo = texture(albedoTexture, matCoordIndex[i]) * abs(matAlpha[i]);
		vec3 sBaseNormalDir = texture(normalTexture, matCoordIndex[i]).rgb * sAlbedo.a;
		float sMetalness = texture(metalnessTexture, matCoordIndex[i]).r * sAlbedo.a;
		float sRoughness = texture(roughnessTexture, matCoordIndex[i]).r * sAlbedo.a;
		vec3 sNormalDir = tbn[i] * sBaseNormalDir;

		albedo += sAlbedo;
		metalness += sMetalness;
		roughness += sRoughness;
		normalDir += sNormalDir;
	}
	albedo.rgb /= albedo.a;
	metalness /= albedo.a;
	roughness /= albedo.a;
	normalDir = normalize(normalDir);

	// Compute diffuse and specular values
	vec3 dielectricSpecular = vec3(0.04, 0.04, 0.04);
	vec3 directSpecular = mix(dielectricSpecular, albedo.rgb, metalness);
	vec3 diffuse = mix(albedo.rgb * (vec3(1) - dielectricSpecular), vec3(0, 0, 0), metalness);
	vec3 grazingSpecular = vec3(1, 1, 1);

	// Compute BRDF
	vec4 brdf = texture(brdfTexture, vec2(dot(eyeDir, normalDir), roughness));
	vec3 diffuseIrradiance = textureLod(skyboxDiffuseTexture, normalDir, 0).rgb;
	float specularLod = roughness * float(skyboxSpecularNumLevels - 1);
	vec3 specularRadiance = textureLod(skyboxSpecularTexture, reflect(-eyeDir, normalDir), specularLod).rgb;
	vec3 radiance = specularRadiance * (brdf.r * directSpecular + brdf.g) + diffuseIrradiance * (diffuse * brdf.b);
	gl_FragColor = vec4(radiance, 1);
}