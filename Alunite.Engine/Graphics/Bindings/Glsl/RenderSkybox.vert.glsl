﻿#version 400
uniform mat4 worldToDevice;
layout(location = 0) in vec3 attrPos;
out vec3 dir;

void main() {
	dir = attrPos;
	vec4 pos = worldToDevice * vec4(attrPos, 0);
	gl_Position = pos.xyww;
}