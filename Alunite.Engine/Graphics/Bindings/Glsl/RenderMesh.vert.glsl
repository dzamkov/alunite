﻿#version 400
uniform mat4 worldToDevice;
layout(location = 0) in vec3 attrWorldPos;
layout(location = 1) in vec4 attrTbn[NUM_BLEND_FACTORS];
in vec4 attrMatCoordIndexAlpha[NUM_BLEND_FACTORS];
out vec3 worldPos;
out mat3 tbn[NUM_BLEND_FACTORS];
out vec3 matCoordIndex[NUM_BLEND_FACTORS];
out float matAlpha[NUM_BLEND_FACTORS];

mat3 quatToMat(vec4 quat) {
    vec3 ab2_ac2_ad2 = 2 * quat.w * quat.xyz;
    vec3 bb2_cc2_dd2 = 2 * quat.xyz * quat.xyz;
    vec3 bc2_cd2_bd2 = 2 * quat.xyz * quat.yzx;
    float bb2 = bb2_cc2_dd2.x;
    float cc2 = bb2_cc2_dd2.y;
    float dd2 = bb2_cc2_dd2.z;
    float ab2 = ab2_ac2_ad2.x;
    float ac2 = ab2_ac2_ad2.y;
    float ad2 = ab2_ac2_ad2.z;
    float bc2 = bc2_cd2_bd2.x;
    float cd2 = bc2_cd2_bd2.y;
    float bd2 = bc2_cd2_bd2.z;
    return sign(quat.w) * mat3(
        1 - cc2 - dd2, bc2 + ad2, bd2 - ac2,
        bc2 - ad2, 1 - dd2 - bb2, cd2 + ab2,
        bd2 + ac2, cd2 - ab2, 1 - cc2 - bb2);
}

void main() {

	// Forward material info
	#pragma optionNV (unroll all)
	for (int i = 0; i < NUM_BLEND_FACTORS; i++) {
		matCoordIndex[i] = attrMatCoordIndexAlpha[i].xyz;
		matAlpha[i] = attrMatCoordIndexAlpha[i].w;
		tbn[i] = quatToMat(attrTbn[i]);
	}

	// Set position
	worldPos = attrWorldPos;
	gl_Position = worldToDevice * vec4(worldPos, 1);
}