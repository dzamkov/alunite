﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;

using Alunite.Data;
using Alunite.Data.Geometry;
using Alunite.Data.Serialization;

namespace Alunite.Graphics
{
    /// <summary>
    /// Describes the geometry of a visual object, parameterized by a set of material and scalar variables.
    /// </summary>
    public sealed partial class Mesh
    {
        internal byte[] _data;
        internal List<_Tie> _ties;
        internal Mesh(byte[] data, List<_Tie> ties, uint textureBound)
        {
            _data = data;
            _ties = ties;
            TextureBound = textureBound;
        }

        /// <summary>
        /// Provides general information about a primitive within a mesh.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Size = (int)Size)]
        internal struct _PrimitiveHeader
        {
            /// <summary>
            /// The size of this structure.
            /// </summary>
            public const uint Size = MeshVertexType.Size + 16;

            /// <summary>
            /// The vertex type for this primitive.
            /// </summary>
            public MeshVertexType VertexType;

            /// <summary>
            /// The number of vertices defined for this primitive.
            /// </summary>
            public uint NumVertices;

            /// <summary>
            /// The number of "hot" vertices defined for this primitive. Such vertices may be removed during
            /// melding operations involving <see cref="_ReferenceMeshTie"/>s. These vertices are stored and indexed
            /// after "cold" vertices, which may not be removed during melding operations.
            /// </summary>
            public uint NumHotVertices;

            /// <summary>
            /// The number of triangles for this primitive.
            /// </summary>
            public uint NumTriangles;

            /// <summary>
            /// The number of "hot" triangles for this primitive. Such triangles may be removed and reconfigured
            /// during melding operations involving <see cref="_ReferenceMeshTie"/>s. These triangles are stored and
            /// indexed after "cold" triangles.
            /// </summary>
            public uint NumHotTriangles;

            /// <summary>
            /// The size, in bytes, of the body of the primitive beginning with this header.
            /// </summary>
            public uint BodySize
            {
                get
                {
                    uint size = NumVertices * VertexType.VertexSize;
                    size += NumTriangles * (3 * sizeof(uint));
                    size += NumHotTriangles * (3 * _SegmentInfo.Size);
                    return size;
                }
            }
        }

        /// <summary>
        /// Identifies a triangle endpoint in a <see cref="Mesh"/>.
        /// </summary>
        [DebuggerDisplay("{ToString(),nq}")]
        internal struct _Endpoint : IEquatable<_Endpoint>
        {
            internal uint _def;
            public _Endpoint(uint triIndex, uint phase)
            {
                if (!(phase < 3))
                    throw new ArgumentException("Phase must be 0, 1 or 2", nameof(phase));
                _def = (triIndex << 2) | phase;
            }

            /// <summary>
            /// The index of the triangle this endpoint belongs to.
            /// </summary>
            public uint TriangleIndex
            {
                get
                {
                    return _def >> 2;
                }
                set
                {
                    _def = (value << 2) | (_def & 0x3);
                }
            }

            /// <summary>
            /// The index of this endpoint within its triangle. This is either 0, 1 or 2.
            /// </summary>
            public uint Phase => _def & 0x3;

            /// <summary>
            /// The endpoint index where information about this endpoint is stored.
            /// </summary>
            public uint Index => TriangleIndex * 3 + Phase;

            /// <summary>
            /// The next endpoint in the same triangle as this endpoint, going counterclockwise.
            /// </summary>
            public _Endpoint Next => new _Endpoint { _def = (_def & ~0x3u) | _nextTable[_def & 0x3] };

            /// <summary>
            /// The next previous in the same triangle as this endpoint.
            /// </summary>
            public _Endpoint Previous => new _Endpoint { _def = (_def & ~0x3u) | _prevTable[_def & 0x3] };

            /// <summary>
            /// The table used to implement <see cref="Next"/>.
            /// </summary>
            internal static readonly byte[] _nextTable = new byte[] { 1, 2, 0, 0 };
            
            /// <summary>
            /// The table used to implement <see cref="Previous"/>.
            /// </summary>
            internal static readonly byte[] _prevTable = new byte[] { 2, 0, 1, 0 };
            
            public static _Endpoint operator +(uint triOffset, _Endpoint a)
            {
                return new _Endpoint { _def = a._def + (triOffset << 2) };
            }

            public static _Endpoint operator +(_Endpoint a, uint triOffset)
            {
                return new _Endpoint { _def = a._def + (triOffset << 2) };
            }

            public static _Endpoint operator -(_Endpoint a, uint triOffset)
            {
                return new _Endpoint { _def = a._def - (triOffset << 2) };
            }

            public static bool operator ==(_Endpoint a, _Endpoint b)
            {
                return a._def == b._def;
            }

            public static bool operator !=(_Endpoint a, _Endpoint b)
            {
                return !(a == b);
            }

            public override bool Equals(object obj)
            {
                if (!(obj is _Endpoint))
                    return false;
                return this == (_Endpoint)obj;
            }
            
            bool IEquatable<_Endpoint>.Equals(_Endpoint other)
            {
                return this == other;
            }

            public override int GetHashCode()
            {
                return _def.GetHashCode();
            }

            public override string ToString()
            {
                return TriangleIndex.ToStringInvariant() + Phase.ToSubscriptStringInvariant();
            }
        }

        /// <summary>
        /// Describes a "hot" triangle segment in a <see cref="Mesh"/>.
        /// </summary>
        [DebuggerDisplay("{ToString(),nq}")]
        [StructLayout(LayoutKind.Explicit, Size = 4)]
        internal struct _SegmentInfo
        {
            [FieldOffset(0)] private uint _def;
            private _SegmentInfo(uint def)
            {
                _def = def;
            }

            /// <summary>
            /// The size of this structure.
            /// </summary>
            public const uint Size = 4;

            /// <summary>
            /// Describes a triangle segment that is not connected to any other hot triangle within the same
            /// polygon.
            /// </summary>
            public static _SegmentInfo Unconnected => new _SegmentInfo(uint.MaxValue);

            /// <summary>
            /// Describes a triangle segment that is connected, within the same polygon, to the segment whose
            /// initial endpoint is the <paramref name="startEndpoint"/>. This is a complementary relationship that
            /// should also be reflected by the other segment.
            /// </summary>
            public static _SegmentInfo Connected(_Endpoint startEndpoint)
            {
                Debug.Assert(startEndpoint._def < uint.MaxValue);
                return new _SegmentInfo(startEndpoint._def);
            }

            /// <summary>
            /// Indicates whether this is <see cref="Unconnected"/>.
            /// </summary>
            public bool IsUnconnected => _def == uint.MaxValue;

            /// <summary>
            /// Indicates whether this is <see cref="Connected(_Endpoint)"/>, and if so, gets the initial
            /// endpoint  of the complementary segment.
            /// </summary>
            public bool IsConnected(out _Endpoint startEndpoint)
            {
                startEndpoint = new _Endpoint { _def = _def };
                return _def < uint.MaxValue;
            }
            /// <summary>
            /// Determines whether the given array of <see cref="_SegmentInfo"/>s satisfies the complementary
            /// connection requirement of <see cref="Connected"/>.
            /// </summary>
            public static bool AreComplementary(SpanList<_SegmentInfo> segInfos)
            {
                for (uint i = 0; i < segInfos.Length; i++)
                {
                    _SegmentInfo segInfo = segInfos[i];
                    if (segInfo.IsConnected(out _Endpoint other))
                        if (!segInfos[other.Index].IsConnected(out _Endpoint self) || self.Index != i)
                            return false;
                }
                return true;
            }

            public override string ToString()
            {
                if (IsUnconnected)
                    return nameof(Unconnected);
                Util.Assert(IsConnected(out _Endpoint startEndpoint));
                return nameof(Connected) + "(" + startEndpoint.ToString() + ")";
            }
        }

        /// <summary>
        /// Describes an instance of a <see cref="MeshTie"/> exhibited by a <see cref="Mesh"/>.
        /// </summary>
        internal struct _Tie
        {
            /// <summary>
            /// The reference configuration for this tie.
            /// </summary>
            public _ReferenceMeshTie Reference;

            /// <summary>
            /// The transform from the reference configuration described by <see cref="Reference"/> to the actual
            /// configuration exhibited by this mesh. 
            /// </summary>
            public Motion3 Transform;

            /// <summary>
            /// The textures applied to <see cref="Reference"/>, defined in terms of the textures for
            /// the <see cref="Mesh"/>.
            /// </summary>
            public List<DependentTexture> Textures;

            /// <summary>
            /// A list of <see cref="_PrimitiveEndpoint"/>s in this mesh whose right/clockwise edges correspond to
            /// the segments defined in <see cref="Reference"/>. Note that may include <see cref="_Endpoint"/>s from
            /// multiple different primitives. The stored triangle indices are mesh-wide indices of
            /// <em>hot</em> triangles.
            /// </summary>
            public List<_Endpoint> Segments;
        }
        
        /// <summary>
        /// A mesh with no content.
        /// </summary>
        public static Mesh Empty { get; } = new Mesh(Array.Empty<byte>(), List.Empty<_Tie>(), 0);

        /// <summary>
        /// Constructs an <see cref="Mesh"/> from the given vertices and indices.
        /// </summary>
        public static unsafe Mesh Build(RegularMeshVertexSpanList verts, SpanList<uint> indices)
        {
            uint numTris = indices.Length / 3;
            if (numTris * 3 != indices.Length)
                throw new ArgumentException("Number of indices must be a multiple of 3", nameof(indices));
            byte[] data = new byte[sizeof(_PrimitiveHeader) + verts._data.Length + indices.Length * sizeof(uint)];
            uint head = 0;
            Binary._fastWriteAlignedNative(data, ref head, new _PrimitiveHeader
            {
                VertexType = verts.Type,
                NumVertices = verts.Length,
                NumHotVertices = 0,
                NumTriangles = numTris,
                NumHotTriangles = 0
            });
            Binary.WriteRaw(data, ref head, verts._data);
            Binary.WriteAlignedNativeList(data, ref head, indices);
            Debug.Assert(head == (uint)data.Length);
            uint textureBound = 0;
            for (uint i = 0; i <verts.Length; i++)
            {
                var textures = verts[i].Textures;
                for (uint j = 0; j < textures.Length; j++)
                {
                    VertexTextureFactor texture = textures[j];
                    if (texture.Index >= textureBound)
                        textureBound = texture.Index + 1;
                }
            }
            return new Mesh(data, List.Empty<_Tie>(), textureBound);
        }

        /// <summary>
        /// One more than the highest texture index referenced by this <see cref="Mesh"/>.
        /// </summary>
        public uint TextureBound { get; }

        /// <summary>
        /// Identifies which indexed texture parameters this <see cref="Mesh"/> references.
        /// </summary>
        public unsafe IndexSet TextureDepends
        {
            get
            {
                IndexSetBuilder dependsBuilder = IndexSetBuilder.CreateEmpty();
                uint head = 0;
                while (head < (uint)_data.Length)
                {
                    _PrimitiveHeader primHeader = Binary._fastReadAlignedNative<_PrimitiveHeader>(_data, ref head);
                    for (uint i = 0; i < primHeader.NumVertices; i++)
                    {
                        var vert = VertexBinary._fastReadAlignedNativeMeshVertex(_data, ref head, primHeader.VertexType);
                        SpanList<VertexTextureFactor> vertMats = vert.Textures;
                        for (uint j = 0; j < vertMats.Length; j++)
                        {
                            VertexTextureFactor mat = vertMats[j];
                            if (mat.Alpha > 0)
                                dependsBuilder.Include(mat.Index);
                        }
                    }
                    head += primHeader.NumTriangles * (3 * sizeof(uint));
                    head += primHeader.NumHotTriangles * (3 * _SegmentInfo.Size);
                }
                return dependsBuilder.Finish();
            }
        }

        /// <summary>
        /// Renders the contents of this <see cref="Mesh"/> using the given arguments.
        /// </summary>
        /// <param name="textures">The <see cref="MaterialTexture2"/>s provided as
        /// texture parameters.</param>
        public void Render(IRenderer renderer, Permutation<MaterialTexture2> textures)
        {
            uint head = 0;
            while (head < (uint)_data.Length)
            {
                var primHeader = Binary._fastReadAlignedNative<_PrimitiveHeader>(_data, ref head);
                var vertData = Binary.ReadRaw(_data, ref head, primHeader.VertexType.VertexSize * primHeader.NumVertices);
                var verts = new RegularMeshVertexSpanList(primHeader.VertexType, vertData);
                var indices = Binary.ReadAlignedNativeList<uint>(_data, ref head, primHeader.NumTriangles * 3);
                head += primHeader.NumHotTriangles * (3 * _SegmentInfo.Size);
                renderer.RenderMesh(textures, verts, indices);
            }
        }

        /// <summary>
        /// Applies a substitution to the texture parameters referenced by this mesh.
        /// </summary>
        internal Mesh Apply(SpanList<DependentTexture> textures)
        {
            return TransformApply(Motion3.Identity, textures);
        }

        /// <summary>
        /// Applies a spatial transformation to this mesh.
        /// </summary>
        public Mesh Transform(Motion3 trans)
        {
            return TransformApply(trans, DependentTexture.Identity(TextureBound));
        }

        /// <summary>
        /// Applies a spatial transformation to this mesh while simultaneously applying a substitution to the texture
        /// parameters it references.
        /// </summary>
        public Mesh TransformApply(Motion3 trans, SpanList<DependentTexture> textures)
        {
            // Transform vertices
            uint head = 0;
            byte[] nData = new byte[_data.Length];
            uint nHead = 0;
            uint nTextureBound = 0;
            while (head < (uint)_data.Length)
            {
                _PrimitiveHeader primHeader = Binary._fastReadAlignedNative<_PrimitiveHeader>(_data, ref head);
                Binary._fastWriteAlignedNative(nData, ref nHead, in primHeader);
                for (uint i = 0; i < primHeader.NumVertices; i++)
                {
                    var vert = VertexBinary._fastReadAlignedNativeMeshVertex(_data, ref head, primHeader.VertexType);
                    _writeTransformApplyVert(nData, ref nHead, ref nTextureBound, trans, textures, vert);
                }
                var indices = Binary.ReadAlignedNativeList<uint>(_data, ref head, primHeader.NumTriangles * 3);
                Binary.WriteAlignedNativeList(nData, ref nHead, indices);
                var segInfos = Binary.ReadAlignedNativeList<_SegmentInfo>(_data, ref head, primHeader.NumHotTriangles * 3);
                Binary.WriteAlignedNativeList(nData, ref nHead, segInfos);
            }
            Debug.Assert(nHead == (uint)nData.Length);

            // Transform ties
            var nTies = ListBuilder<_Tie>.CreateEmpty(_ties.Length);
            for (uint i = 0; i < _ties.Length; i++)
            {
                _Tie tie = _ties[i];
                var nTextures = ListBuilder<DependentTexture>.CreateDefault(tie.Textures.Length);
                for (uint j = 0; j < tie.Textures.Length; j++)
                    nTextures[j] = tie.Textures[j].Apply(textures);
                nTies.Append(new _Tie
                {
                    Reference = tie.Reference,
                    Transform = trans * tie.Transform,
                    Textures = nTextures.Finish(),
                    Segments = tie.Segments
                });
            }
            return new Mesh(nData, nTies.Finish(), nTextureBound);
        }

        /// <summary>
        /// Writes data for a mesh vertex while applying a spatial transformation and a texture substitution.
        /// </summary>
        private static void _writeTransformApplyVert(
            byte[] nData, ref uint nHead, ref uint nTextureBound,
            Motion3 trans, SpanList<DependentTexture> textures, SpanMeshVertex vert)
        {
            // Write position
            Binary._fastWriteAlignedNative(nData, ref nHead, trans * vert.Position);

            // Allocate space for texture factors
            SpanList<VertexTextureFactor> vertTextures = vert.Textures;
            uint numVertTextures = vertTextures.Length;
            var nVertTextures = Binary.AllocateAlignedNativeList<VertexTextureFactor>(nData, ref nHead, numVertTextures);

            // Apply texture transformations and sort texture factors
            uint groupStartIndex = 0;
            for (uint i = 0; i < vertTextures.Length; i++)
            {
                VertexTextureFactor vertTexture = vertTextures[i];
                DependentTexture nTexture = textures[vertTexture.Index];
                if (nTexture.Index >= nTextureBound)
                    nTextureBound = nTexture.Index + 1;
                VertexTextureFactor nVertTexture = VertexTextureFactor.Build(
                    nTexture.Index,
                    nTexture.Coord * vertTexture.Coord,
                    trans.Linear * vertTexture.Tbn * Rotation3.On_X_Y(nTexture.Tbn),
                    vertTexture.Alpha, false);
                nVertTextures[(int)i] = nVertTexture;

                // Ensure the factors in each overlay group stay sorted by texture index
                if (vertTexture.ShouldOverlay)
                {
                    uint groupEndIndex = i + 1;
                    var groupTextures = nVertTextures.Slice((int)groupStartIndex, (int)(groupEndIndex - groupStartIndex));
                    groupTextures.Sort(_VertexTextureFactorComparer.Instance);
                    nVertTextures[(int)i].ShouldOverlay = true;
                    groupStartIndex = groupEndIndex;
                }
            }
        }

        /// <summary>
        /// An <see cref="System.Collections.Generic.IComparer{T}"/> which compares <see cref="VertexTextureFactor"/>
        /// by their texture index.
        /// </summary>
        internal struct _VertexTextureFactorComparer : System.Collections.Generic.IComparer<VertexTextureFactor>
        {
            /// <summary>
            /// The only instance of this class.
            /// </summary>
            public static _VertexTextureFactorComparer Instance => new _VertexTextureFactorComparer();

            int System.Collections.Generic.IComparer<VertexTextureFactor>.Compare(
                VertexTextureFactor x, VertexTextureFactor y)
            {
                return x.Index.CompareTo(y.Index);
            }
        }

        /// <summary>
        /// The total number of "hot" triangles in this mesh, across all primitives.
        /// </summary>
        internal uint _numHotTris
        {
            get
            {
                uint head = 0;
                uint numHotTris = 0;
                while (head < (uint)_data.Length)
                {
                    _PrimitiveHeader primHeader = Binary._fastReadAlignedNative<_PrimitiveHeader>(_data, ref head);
                    numHotTris += primHeader.NumHotTriangles;
                    head += primHeader.NumVertices * primHeader.VertexType.VertexSize;
                    head += primHeader.NumTriangles * (3 * sizeof(uint));
                    head += primHeader.NumHotTriangles * (3 * _SegmentInfo.Size);
                }
                return numHotTris;
            }
        }
    }

    /// <summary>
    /// A helper class for constructing a <see cref="Mesh"/>.
    /// </summary>
    public sealed class MeshBuilder : IBuilder<Mesh>
    {
        private byte[] _data;
        private uint _head;
        private MapBuilder<MeshVertexType, uint> _primOffsets;
        private ListBuilder<_Tie> _ties;
        private uint _textureBound;
        private MeshBuilder()
        {
            _data = Array.Empty<byte>();
            _head = 0;
            _primOffsets = MapBuilder<MeshVertexType, uint>.CreateEmpty();
            _ties = ListBuilder<_Tie>.CreateEmpty();
            _textureBound = 0;
        }

        /// <summary>
        /// Describes an instance of a <see cref="MeshTie"/> exhibited by a <see cref="Mesh"/>.
        /// </summary>
        private struct _Tie
        {
            /// <summary>
            /// The reference configuration for this tie.
            /// </summary>
            public _ReferenceMeshTie Reference;

            /// <summary>
            /// The transform from the reference configuration described by <see cref="Reference"/> to the actual
            /// configuration exhibited by this mesh. 
            /// </summary>
            public Motion3 Transform;

            /// <summary>
            /// The textures applied to <see cref="Reference"/>, defined in terms of the textures for
            /// the <see cref="Mesh"/>.
            /// </summary>
            public List<DependentTexture> Textures;

            /// <summary>
            /// A list of <see cref="_Endpoint"/>s in this mesh whose right/clockwise edges correspond to the segments
            /// defined in <see cref="Reference"/>. Note that may include <see cref="_Endpoint"/>s from multiple
            /// primitives.
            /// </summary>
            public Mesh._Endpoint[] Segments;
        }

        /// <summary>
        /// Creates a new initially-empty <see cref="MeshBuilder"/>.
        /// </summary>
        public static MeshBuilder CreateEmpty()
        {
            return new MeshBuilder();
        }

        /// <summary>
        /// Adds a vertex to the resulting mesh, returning an <see cref="Index"/> which can be used to identify it.
        /// </summary>
        public Index AddVertex(SpanMeshVertex vert)
        {
            uint primOffset = _primOffset(vert.Type);
            uint vertOffset = _head;

            // Write vertex data
            Table.Allocate(ref _data, _head, _VertexHeader.Size + vert.Type.VertexSize);
            uint primHead = primOffset;
            ref _PrimitiveInfo primInfo = ref Binary._fastReadAlignedNative<_PrimitiveInfo>(_data, ref primHead);
            Binary._fastWriteAlignedNative(_data, ref _head, new _VertexHeader(primInfo.LastVertexOffset));
            VertexBinary._fastWriteAlignedNativeMeshVertex(_data, ref _head, vert);

            // Update texture bound
            for (uint i = 0; i < vert.Textures.Length; i++)
            {
                uint textureIndex = vert.Textures[i].Index;
                if (_textureBound <= textureIndex)
                    _textureBound = textureIndex + 1;
            }

            // Add vertex to list
            primInfo.NumVertices++;
            primInfo.LastVertexOffset = vertOffset;
            return new Index(primOffset, vertOffset);
        }

        /// <summary>
        /// Adds a vertex with the given properties to the resulting mesh, returning an <see cref="Index"/> which
        /// can be used to identify it.
        /// </summary>
        public Index BuildVertex(Vector3 pos, SpanList<VertexTextureFactor> textures)
        {
            MeshVertexType vertType = new MeshVertexType(textures.Length);
            uint primOffset = _primOffset(vertType);
            uint vertOffset = _head;
            
            // Write vertex data
            Table.Allocate(ref _data, _head, _VertexHeader.Size + vertType.VertexSize);
            uint primHead = primOffset;
            ref _PrimitiveInfo primInfo = ref Binary._fastReadAlignedNative<_PrimitiveInfo>(_data, ref primHead);
            Binary._fastWriteAlignedNative(_data, ref _head, new _VertexHeader(primInfo.LastVertexOffset));
            Binary._fastWriteAlignedNative(_data, ref _head, in pos);
            var nTextures = Binary.AllocateAlignedNativeList<VertexTextureFactor>(_data, ref _head, textures.Length);

            // Write texture factor data
            uint groupStartIndex = 0;
            for (uint i = 0; i < nTextures.Length; i++)
            {
                VertexTextureFactor texture = textures[i];

                // Ensure the factors in each overlay group are sorted by texture index
                if (texture.ShouldOverlay)
                {
                    texture.ShouldOverlay = false;
                    nTextures[(int)i] = texture;
                    uint groupEndIndex = i + 1;
                    var groupTextures = nTextures.Slice((int)groupStartIndex, (int)(groupEndIndex - groupStartIndex));
                    groupTextures.Sort(Mesh._VertexTextureFactorComparer.Instance);
                    nTextures[(int)i].ShouldOverlay = true;
                    groupStartIndex = groupEndIndex;
                }
                else
                {
                    nTextures[(int)i] = texture;
                }
            }
            if (groupStartIndex < (uint)nTextures.Length)
            {
                // Build final overlay group
                uint i = (uint)nTextures.Length - 1;
                var groupTextures = nTextures.Slice((int)groupStartIndex, nTextures.Length - (int)groupStartIndex);
                groupTextures.Sort(Mesh._VertexTextureFactorComparer.Instance);
                nTextures[(int)i].ShouldOverlay = true;
            }

            // Update texture bound
            for (uint i = 0; i < textures.Length; i++)
            {
                uint textureIndex = textures[i].Index;
                if (_textureBound <= textureIndex)
                    _textureBound = textureIndex + 1;
            }

            // Add vertex to list
            primInfo.NumVertices++;
            primInfo.LastVertexOffset = vertOffset;
            return new Index(primOffset, vertOffset);
        }

        /// <summary>
        /// Gets the offset for the primitive with the given <see cref="MeshVertexType"/>.
        /// </summary>
        private uint _primOffset(MeshVertexType vertexType)
        {
            // TODO: Cache last used vertex type?
            if (_primOffsets.Allocate(vertexType, out var entry))
            {
                entry.Value = _head;
                Table.Allocate(ref _data, _head, _PrimitiveInfo.Size);
                Binary._fastWriteAlignedNative(_data, ref _head, new _PrimitiveInfo
                {
                    VertexType = vertexType,
                    NumVertices = 0,
                    NumTriangles = 0,
                    LastVertexOffset = uint.MaxValue,
                    LastTriangleOffset = uint.MaxValue,
                    _usageInfo = uint.MaxValue
                });
            }
            return entry.Value;
        }

        /// <summary>
        /// Adds an oriented triangle to the mesh. All vertices in the triangle must be of the same vertex type.
        /// </summary>
        public void AddTriangle(Index a, Index b, Index c)
        {
            uint primOffset = a._primOffset;
            if (b._primOffset != primOffset || c._primOffset != primOffset)
                throw new ArgumentException("All triangle indices must be of the same vertex type");

            // Write triangle data
            uint triOffset = _head;
            Table.Allocate(ref _data, _head, _TriangleInfo.Size);
            uint primHead = primOffset;
            uint aVertHead = a._vertOffset;
            uint bVertHead = b._vertOffset;
            uint cVertHead = c._vertOffset;
            ref _PrimitiveInfo primInfo = ref Binary.ReadAlignedNative<_PrimitiveInfo>(_data, ref primHead);
            ref _VertexHeader aHeader = ref Binary.ReadAlignedNative<_VertexHeader>(_data, ref aVertHead);
            ref _VertexHeader bHeader = ref Binary.ReadAlignedNative<_VertexHeader>(_data, ref bVertHead);
            ref _VertexHeader cHeader = ref Binary.ReadAlignedNative<_VertexHeader>(_data, ref cVertHead);
            Binary._fastWriteAlignedNative(_data, ref _head, new _TriangleInfo
            {
                _usageInfo = aHeader._usageInfo | bHeader._usageInfo | cHeader._usageInfo | _TriangleInfo._usedCold,
                PrevTriangleOffset = primInfo.LastTriangleOffset,
                Endpoint_0 = new _EndpointInfo
                {
                    VertexOffset = a._vertOffset,
                    PrevVertexEndpoint = aHeader.LastEndpoint
                },
                Endpoint_1 = new _EndpointInfo
                {
                    VertexOffset = b._vertOffset,
                    PrevVertexEndpoint = bHeader.LastEndpoint
                },
                Endpoint_2 = new _EndpointInfo
                {
                    VertexOffset = c._vertOffset,
                    PrevVertexEndpoint = cHeader.LastEndpoint
                }
            });

            // Mark vertices as used
            aHeader._usageInfo |= _VertexHeader._usedCold;
            bHeader._usageInfo |= _VertexHeader._usedCold;
            cHeader._usageInfo |= _VertexHeader._usedCold;

            // Add triangle to lists
            primInfo.NumTriangles++;
            primInfo.LastTriangleOffset = triOffset;
            aHeader.LastEndpoint = _Endpoint._build(triOffset, 0);
            bHeader.LastEndpoint = _Endpoint._build(triOffset, 1);
            cHeader.LastEndpoint = _Endpoint._build(triOffset, 2);
        }
        
        /// <summary>
        /// Adds a "mesh tie" consisting of the given indices. When the resulting mesh is merged with another mesh
        /// that has a complementary, adjacent tie, the two meshes will meld together.
        /// </summary>
        public void AddTie(Set<Index> indices)
        {
            // Count number of primitives involved in the tie and assign tie-specific indices to vertices
            uint numTiePrims = 0;
            foreach (Index index in indices)
            {
                // Update primitive information
                uint primHead = index._primOffset;
                ref _PrimitiveInfo primInfo = ref Binary.ReadAlignedNative<_PrimitiveInfo>(_data, ref primHead);
                ref uint numTiePrimVerts = ref primInfo._usageInfo;
                if (numTiePrimVerts == uint.MaxValue)
                {
                    numTiePrims++;
                    numTiePrimVerts = 0;
                }

                // Update vertex information
                uint vertHead = index._vertOffset;
                ref _VertexHeader vertHeader = ref Binary.ReadAlignedNative<_VertexHeader>(_data, ref vertHead);
                Debug.Assert(vertHeader._usageInfo >= _VertexHeader._notUsed);
                vertHeader._usageInfo = numTiePrimVerts++;
            }

            // Build ordered list of primitives, and find total data requirements for the tie
            Span<uint> tiePrimOffsets = stackalloc uint[(int)numTiePrims];
            numTiePrims = 0;
            uint totalTieVertexSize = 0;
            uint numTieSegs = 0;
            foreach (var prim in _primOffsets)
            {
                uint primHead = prim.Value;
                ref _PrimitiveInfo primInfo = ref Binary._fastReadAlignedNative<_PrimitiveInfo>(_data, ref primHead);
                if (primInfo._usageInfo < uint.MaxValue)
                {
                    tiePrimOffsets[(int)numTiePrims++] = prim.Value;
                    totalTieVertexSize += primInfo.VertexType.VertexSize * primInfo._usageInfo;

                    // Search for involved segments in the primitives triangles
                    uint triOffset = primInfo.LastTriangleOffset;
                    while (triOffset < uint.MaxValue)
                    {
                        uint triHead = triOffset;
                        ref _TriangleInfo triInfo = ref Binary._fastReadAlignedNative<_TriangleInfo>(_data, ref triHead);
                        uint vertHead_0 = triInfo.Endpoint_0.VertexOffset;
                        uint vertHead_1 = triInfo.Endpoint_1.VertexOffset;
                        uint vertHead_2 = triInfo.Endpoint_2.VertexOffset;
                        uint tieIndex_0 = Binary._fastReadAlignedNative<_VertexHeader>(_data, ref vertHead_0)._usageInfo;
                        uint tieIndex_1 = Binary._fastReadAlignedNative<_VertexHeader>(_data, ref vertHead_1)._usageInfo;
                        uint tieIndex_2 = Binary._fastReadAlignedNative<_VertexHeader>(_data, ref vertHead_2)._usageInfo;
                        if (tieIndex_0 < _VertexHeader._notUsed)
                        {
                            triInfo._usageInfo = _TriangleInfo._usedHot;
                            if (tieIndex_1 < _VertexHeader._notUsed)
                            {
                                numTieSegs++;
                                if (tieIndex_2 < _VertexHeader._notUsed)
                                    numTieSegs += 2;
                            }
                            else if (tieIndex_2 < _VertexHeader._notUsed)
                            {
                                numTieSegs++;
                            }
                        }
                        else if (tieIndex_1 < _VertexHeader._notUsed)
                        {
                            triInfo._usageInfo = _TriangleInfo._usedHot;
                            if (tieIndex_2 < _VertexHeader._notUsed)
                                numTieSegs++;
                        }
                        else if (tieIndex_2 < _VertexHeader._notUsed)
                        {
                            triInfo._usageInfo = _TriangleInfo._usedHot;
                        }
                        triOffset = triInfo.PrevTriangleOffset;
                    }
                }
            }
            Debug.Assert(numTiePrims == (uint)tiePrimOffsets.Length);
            tiePrimOffsets.Sort(new _PrimOffsetComparer { _data = _data });

            // Build mesh data for tie
            Mesh._Endpoint[] tieSegs = new Mesh._Endpoint[numTieSegs];
            byte[] tieData = new byte[
                numTiePrims * _ReferenceMeshTie._PrimitiveHeader.Size +
                numTieSegs * _ReferenceMeshTie._SegmentInfo.Size +
                totalTieVertexSize];
            numTieSegs = 0;
            uint tieHead = 0;
            for (int i = 0; i < tiePrimOffsets.Length; i++)
            {
                uint primHead = tiePrimOffsets[i];
                ref _PrimitiveInfo primInfo = ref Binary._fastReadAlignedNative<_PrimitiveInfo>(_data, ref primHead);
                uint numTiePrimVerts = primInfo._usageInfo;
                uint vertSize = primInfo.VertexType.VertexSize;

                // Write segments first, it makes things a bit easier
                uint indexTieHead = tieHead;
                indexTieHead += _ReferenceMeshTie._PrimitiveHeader.Size;
                indexTieHead += numTiePrimVerts * vertSize;
                uint numTiePrimSegs = 0;
                uint triOffset = primInfo.LastTriangleOffset;
                while (triOffset < uint.MaxValue)
                {
                    uint triHead = triOffset;
                    _TriangleInfo triInfo = Binary._fastReadAlignedNative<_TriangleInfo>(_data, ref triHead);
                    uint vertHead_0 = triInfo.Endpoint_0.VertexOffset;
                    uint vertHead_1 = triInfo.Endpoint_1.VertexOffset;
                    uint vertHead_2 = triInfo.Endpoint_2.VertexOffset;
                    uint tieIndex_0 = Binary._fastReadAlignedNative<_VertexHeader>(_data, ref vertHead_0)._usageInfo;
                    uint tieIndex_1 = Binary._fastReadAlignedNative<_VertexHeader>(_data, ref vertHead_1)._usageInfo;
                    uint tieIndex_2 = Binary._fastReadAlignedNative<_VertexHeader>(_data, ref vertHead_2)._usageInfo;
                    if (tieIndex_0 < _VertexHeader._notUsed && tieIndex_1 < _VertexHeader._notUsed)
                    {
                        tieSegs[numTieSegs++] = (Mesh._Endpoint)_Endpoint._build(triOffset, 0);
                        Binary._fastWriteAlignedNative(tieData, ref indexTieHead,
                            new _ReferenceMeshTie._SegmentInfo(tieIndex_0, tieIndex_1));
                        numTiePrimSegs++;
                    }
                    if (tieIndex_1 < _VertexHeader._notUsed && tieIndex_2 < _VertexHeader._notUsed)
                    {
                        tieSegs[numTieSegs++] = (Mesh._Endpoint)_Endpoint._build(triOffset, 1);
                        Binary._fastWriteAlignedNative(tieData, ref indexTieHead,
                            new _ReferenceMeshTie._SegmentInfo(tieIndex_1, tieIndex_2));
                        numTiePrimSegs++;
                    }
                    if (tieIndex_2 < _VertexHeader._notUsed && tieIndex_0 < _VertexHeader._notUsed)
                    {
                        tieSegs[numTieSegs++] = (Mesh._Endpoint)_Endpoint._build(triOffset, 2);
                        Binary._fastWriteAlignedNative(tieData, ref indexTieHead,
                            new _ReferenceMeshTie._SegmentInfo(tieIndex_2, tieIndex_0));
                        numTiePrimSegs++;
                    }
                    triOffset = triInfo.PrevTriangleOffset;
                }

                // Write primitive header
                Binary._fastWriteAlignedNative(tieData, ref tieHead, new _ReferenceMeshTie._PrimitiveHeader
                {
                    VertexType = primInfo.VertexType,
                    NumVertices = numTiePrimVerts,
                    NumSegments = numTiePrimSegs
                });

                // Write vertex data and clean up vertex usage info
                uint vertTieHead = tieHead;
                uint vertOffset = primInfo.LastVertexOffset;
                while (vertOffset < uint.MaxValue)
                {
                    uint vertHead = vertOffset;
                    ref _VertexHeader vertHeader = ref Binary._fastReadAlignedNative<_VertexHeader>(_data, ref vertHead);
                    if (vertHeader._usageInfo < _VertexHeader._notUsed)
                    {
                        var vert = VertexBinary._fastReadAlignedNativeMeshVertex(
                            _data, ref vertHead, primInfo.VertexType);
                        uint curVertTieHead = vertTieHead + vertHeader._usageInfo * vertSize;
                        VertexBinary._fastWriteAlignedNativeMeshVertex(tieData, ref curVertTieHead, vert);
                        vertHeader._usageInfo = _VertexHeader._usedHot;
                    }
                    vertOffset = vertHeader.PrevVertexOffset;
                }
                tieHead = indexTieHead;

                // Clean up primitive usage info
                primInfo._usageInfo = uint.MaxValue;
            }
            Debug.Assert(tieHead == (uint)tieData.Length);

            // Construct tie
            MeshTie tie = MeshTie._build(tieData, _textureBound);

            // Finalize tie for mesh builder
            Util.Permute<Mesh._Endpoint>(tieSegs, tie._segs);
            _ties.Append(new _Tie
            {
                Reference = tie._ref,
                Transform = tie._trans,
                Textures = tie._textures,
                Segments = tieSegs
            });
        }

        /// <summary>
        /// The comparer for primitives based on their <see cref="MeshVertexType"/>.
        /// </summary>
        private struct _PrimOffsetComparer : System.Collections.Generic.IComparer<uint>
        {
            internal byte[] _data;
            int System.Collections.Generic.IComparer<uint>.Compare(uint x, uint y)
            {
                return MeshVertexType.Compare(
                    Binary._fastReadAlignedNative<_PrimitiveInfo>(_data, ref x).VertexType,
                    Binary._fastReadAlignedNative<_PrimitiveInfo>(_data, ref y).VertexType);
            }
        }

        /// <summary>
        /// Identifies a vertex within a <see cref="MeshBuilder"/>.
        /// </summary>
        public struct Index
        {
            internal Index(uint primOffset, uint vertOffset)
            {
                _primOffset = primOffset;
                _vertOffset = vertOffset;
            }

            /// <summary>
            /// The offset of the primitive information in <see cref="_data"/> which describes the primitive
            /// for this vertex.
            /// </summary>
            internal uint _primOffset;

            /// <summary>
            /// The offset of the vertex information in <see cref="_data"/>.
            /// </summary>
            internal uint _vertOffset;
        }

        /// <summary>
        /// Provides information about a primitive in a <see cref="MeshBuilder"/>.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Size = (int)Size)]
        private struct _PrimitiveInfo
        {
            /// <summary>
            /// The size of this structure.
            /// </summary>
            public const uint Size = MeshVertexType.Size + 16;

            /// <summary>
            /// The vertex type for this primitive.
            /// </summary>
            public MeshVertexType VertexType;

            /// <summary>
            /// The number of vertices defined for this primitive (so far).
            /// </summary>
            public uint NumVertices;

            /// <summary>
            /// The number of triangles defined for this primitive (so far).
            /// </summary>
            public uint NumTriangles;

            /// <summary>
            /// The offset of the last vertex belonging to this primitive in <see cref="_data"/>.
            /// </summary>
            public uint LastVertexOffset;

            /// <summary>
            /// The offset of the last triangle belonging to this primitive in <see cref="_data"/>.
            /// </summary>
            public uint LastTriangleOffset;

            /// <summary>
            /// An arbitrary value indicating where this primitive is used in some context. Outside of a few method
            /// calls, this will always be <see cref="uint.MaxValue"/>.
            /// </summary>
            internal uint _usageInfo;
        }

        /// <summary>
        /// Provides information about a vertex in a <see cref="MeshBuilder"/>.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Size = (int)Size)]
        private struct _VertexHeader
        {
            public _VertexHeader(uint prevVertOffset)
            {
                PrevVertexOffset = prevVertOffset;
                LastEndpoint = _Endpoint.Null;
                _usageInfo = _notUsed;
            }

            /// <summary>
            /// The size of this structure.
            /// </summary>
            public const uint Size = 12;

            /// <summary>
            /// The offset of the previous vertex belonging to the same primitive as this vertex.
            /// </summary>
            public uint PrevVertexOffset;

            /// <summary>
            /// The last triangle endpoint that this vertex is connected to.
            /// </summary>
            public _Endpoint LastEndpoint;

            /// <summary>
            /// An arbitrary value indicating where this vertex is used in some context. Outside of a few method calls,
            /// this will be <see cref="_notUsed"/>, <see cref="_usedCold"/> or <see cref="_usedHot"/>.
            /// </summary>
            internal uint _usageInfo;

            /// <summary>
            /// A value for <see cref="_usageInfo"/> which indicates that the vertex is not used in a triangle or
            /// mesh tie.
            /// </summary>
            internal const uint _notUsed = 0xFFFFFFFC;

            /// <summary>
            /// A value for <see cref="_usageInfo"/> which indicates that the vertex is used in a triangle, but not in
            /// a mesh tie.
            /// </summary>
            internal const uint _usedCold = 0xFFFFFFFE;

            /// <summary>
            /// A value for <see cref="_usageInfo"/> which indicates that the vertex is used in a mesh tie.
            /// </summary>
            internal const uint _usedHot = 0xFFFFFFFF;
        }

        /// <summary>
        /// Provides information about a triangle in a <see cref="MeshBuilder"/>.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Size = (int)Size)]
        private struct _TriangleInfo
        {
            /// <summary>
            /// The size of this structure.
            /// </summary>
            public const uint Size = 32;

            /// <summary>
            /// The offset of the previous triangle belonging to the same primitive as this triangle.
            /// </summary>
            public uint PrevTriangleOffset;

            /// <summary>
            /// The first endpoint for this triangle.
            /// </summary>
            public _EndpointInfo Endpoint_0;

            /// <summary>
            /// The second endpoint for this triangle.
            /// </summary>
            public _EndpointInfo Endpoint_1;

            /// <summary>
            /// The third endpoint for this triangle.
            /// </summary>
            public _EndpointInfo Endpoint_2;
            
            /// <summary>
            /// Gets a particular endpoint of this <see cref="_TriangleInfo"/>.
            /// </summary>
            public _EndpointInfo Endpoint(uint phase)
            {
                if (!(phase < 3))
                    throw new ArgumentException("Phase must be 0, 1 or 2", nameof(phase));
                return Unsafe.Add(ref this.Endpoint_0, (int)phase);
            }

            /// <summary>
            /// An arbitrary value indicating where this vertex is used in some context. Outside of a few method calls,
            /// this will be <see cref="_usedCold"/> or <see cref="_usedHot"/>.
            /// </summary>
            internal uint _usageInfo;

            /// <summary>
            /// A value for <see cref="_usageInfo"/> which indicates that a triangle is not used in any mesh tie.
            /// </summary>
            internal const uint _usedCold = 0xFFFFFFFE;

            /// <summary>
            /// A value for <see cref="_usageInfo"/> which indicates that the triangle is used in a mesh tie.
            /// </summary>
            internal const uint _usedHot = 0xFFFFFFFF;
        }

        /// <summary>
        /// Provides information about a triangle endpoint in a <see cref="MeshBuilder"/>.
        /// </summary>
        private struct _EndpointInfo
        {
            /// <summary>
            /// The offset of the vertex this endpoint connects to.
            /// </summary>
            public uint VertexOffset;

            /// <summary>
            /// The previous triangle endpoint connected to the same vertex as this endpoint.
            /// </summary>
            public _Endpoint PrevVertexEndpoint;
        }
        
        /// <summary>
        /// Identifies a triangle endpoint in a <see cref="MeshBuilder"/>.
        /// </summary>
        private struct _Endpoint
        {
            private uint _def;
            private _Endpoint(uint def)
            {
                _def = def;
            }

            /// <summary>
            /// A sentinel value for <see cref="_Endpoint"/> used to indicate an absence of an endpoint.
            /// </summary>
            public static _Endpoint Null
            {
                get
                {
                    // Fortunately, endpoints can't occur at 0 offset, so we can use 0 as the null value
                    return default;
                }
            }

            /// <summary>
            /// Constructs an <see cref="_Endpoint"/>.
            /// </summary>
            /// <param name="triOffset">The offset of triangle for the endpoint.</param>
            /// <param name="phase">The index (0, 1 or 2) of the endpoint within the triangle.</param>
            internal static _Endpoint _build(uint triOffset, uint phase)
            {
                Debug.Assert((triOffset & 0x3) == 0);
                Debug.Assert(phase < 3);
                return new _Endpoint(triOffset | phase);
            }

            /// <summary>
            /// The offset of the triangle this endpoint belongs to.
            /// </summary>
            public uint TriangleOffset => _def & ~0x3u;

            /// <summary>
            /// The index of this endpoint within its triangle. This is either 0, 1 or 2.
            /// </summary>
            public uint Phase => _def & 0x3;

            /// <summary>
            /// The next endpoint in the same triangle as this endpoint, going counterclockwise.
            /// </summary>
            public _Endpoint Next => new _Endpoint { _def = (_def & ~0x3u) | Mesh._Endpoint._nextTable[_def & 0x3] };

            /// <summary>
            /// The next previous in the same triangle as this endpoint.
            /// </summary>
            public _Endpoint Previous => new _Endpoint { _def = (_def & ~0x3u) | Mesh._Endpoint._prevTable[_def & 0x3] };

            /// <summary>
            /// Indicates whether this is <see cref="Null"/>
            /// </summary>
            public bool IsNull => _def == 0;

            public static explicit operator Mesh._Endpoint(_Endpoint source)
            {
                return new Mesh._Endpoint { _def = source._def };
            }

            public static explicit operator _Endpoint(Mesh._Endpoint source)
            {
                return new _Endpoint(source._def);
            }
        }

        /// <summary>
        /// Gets the <see cref="Mesh"/> resulting from this <see cref="MeshBuilder"/>.
        /// </summary>
        public Mesh Finish()
        {
            var sortedPrimOffsets = _primOffsets.Finish().Sort(MeshVertexType.Comparer.Instance);

            // Reverse vertex and triangle linked lists so that they can be traversed in the order they were
            // added. Also, remove unused vertices and compute the final data size for the mesh
            uint dataSize = 0;
            foreach (uint primOffset in sortedPrimOffsets)
            {
                uint primHead = primOffset;
                ref _PrimitiveInfo primInfo = ref Binary._fastReadAlignedNative<_PrimitiveInfo>(_data, ref primHead);

                // Process vertices
                uint prevVertOffset = 0;
                uint curVertOffset = primInfo.LastVertexOffset;
                uint remVertices = primInfo.NumVertices;
                while (remVertices > 0)
                {
                    remVertices--;
                    uint vertHead = curVertOffset;
                    ref _VertexHeader vertHeader = ref Binary._fastReadAlignedNative<_VertexHeader>(_data, ref vertHead);
                    if (vertHeader._usageInfo > _VertexHeader._notUsed)
                    {
                        uint nextVertOffset = vertHeader.PrevVertexOffset;
                        vertHeader.PrevVertexOffset = prevVertOffset;
                        prevVertOffset = curVertOffset;
                        curVertOffset = nextVertOffset;
                    }
                    else
                    {
                        // Remove unused vertex
                        primInfo.NumVertices--;
                    }
                }
                primInfo.LastVertexOffset = prevVertOffset;

                // Process triangles
                uint numHotTris = 0;
                uint prevTriOffset = 0;
                uint curTriOffset = primInfo.LastTriangleOffset;
                uint remTris = primInfo.NumTriangles;
                while (remTris > 0)
                {
                    remTris--;
                    uint triHead = curTriOffset;
                    ref _TriangleInfo triInfo = ref Binary._fastReadAlignedNative<_TriangleInfo>(_data, ref triHead);
                    if (triInfo._usageInfo == _TriangleInfo._usedHot)
                        numHotTris++;
                    uint nextTriOffset = triInfo.PrevTriangleOffset;
                    triInfo.PrevTriangleOffset = prevTriOffset;
                    prevTriOffset = curTriOffset;
                    curTriOffset = nextTriOffset;
                }
                primInfo.LastTriangleOffset = prevTriOffset;

                // Add contributions from this primitive to the final data size of the mesh
                dataSize += Mesh._PrimitiveHeader.Size;
                dataSize += primInfo.NumVertices * primInfo.VertexType.VertexSize;
                dataSize += primInfo.NumTriangles * 3 * sizeof(uint);
                dataSize += numHotTris * 3 * Mesh._SegmentInfo.Size;
            }
            
            // Build data for mesh
            byte[] nData = new byte[dataSize];
            uint nHead = 0;
            uint hotTriIndex = 0;
            foreach (uint primOffset in sortedPrimOffsets)
            {
                uint primHead = primOffset;
                ref _PrimitiveInfo primInfo = ref Binary._fastReadAlignedNative<_PrimitiveInfo>(_data, ref primHead);
                Debug.Assert(primInfo._usageInfo == uint.MaxValue);

                // Reserve space for primitive header
                primHead = nHead;
                nHead += Mesh._PrimitiveHeader.Size;

                // Write cold vertex data
                uint primVertIndex = 0;
                uint vertSize = primInfo.VertexType.VertexSize;
                uint curVertOffset = primInfo.LastVertexOffset;
                for (uint i = 0; i < primInfo.NumVertices; i++)
                {
                    uint vertHead = curVertOffset;
                    ref _VertexHeader vertHeader = ref Binary._fastReadAlignedNative<_VertexHeader>(_data, ref vertHead);
                    if (vertHeader._usageInfo == _VertexHeader._usedCold)
                    {
                        vertHeader._usageInfo = primVertIndex++;
                        var vert = VertexBinary._fastReadAlignedNativeMeshVertex(_data, ref vertHead, primInfo.VertexType);
                        VertexBinary._fastWriteAlignedNativeMeshVertex(nData, ref nHead, vert);
                    }
                    curVertOffset = vertHeader.PrevVertexOffset;
                }

                // Write hot vertex data
                uint numColdVerts = primVertIndex;
                curVertOffset = primInfo.LastVertexOffset;
                for (uint i = 0; i < primInfo.NumVertices; i++)
                {
                    uint vertHead = curVertOffset;
                    ref _VertexHeader vertHeader = ref Binary._fastReadAlignedNative<_VertexHeader>(_data, ref vertHead);
                    if (vertHeader._usageInfo == _VertexHeader._usedHot)
                    {
                        vertHeader._usageInfo = primVertIndex++;
                        var vert = VertexBinary._fastReadAlignedNativeMeshVertex(_data, ref vertHead, primInfo.VertexType);
                        VertexBinary._fastWriteAlignedNativeMeshVertex(nData, ref nHead, vert);
                    }
                    curVertOffset = vertHeader.PrevVertexOffset;
                }

                // Write cold triangle index data
                uint curTriOffset = primInfo.LastTriangleOffset;
                uint numHotTris = 0;
                for (uint i = 0; i < primInfo.NumTriangles; i++)
                {
                    uint triHead = curTriOffset;
                    ref _TriangleInfo triInfo = ref Binary._fastReadAlignedNative<_TriangleInfo>(_data, ref triHead);
                    if (triInfo._usageInfo == _TriangleInfo._usedCold)
                    {
                        uint vertHead_0 = triInfo.Endpoint_0.VertexOffset;
                        uint vertHead_1 = triInfo.Endpoint_1.VertexOffset;
                        uint vertHead_2 = triInfo.Endpoint_2.VertexOffset;
                        uint index_0 = Binary._fastReadAlignedNative<_VertexHeader>(_data, ref vertHead_0)._usageInfo;
                        uint index_1 = Binary._fastReadAlignedNative<_VertexHeader>(_data, ref vertHead_1)._usageInfo;
                        uint index_2 = Binary._fastReadAlignedNative<_VertexHeader>(_data, ref vertHead_2)._usageInfo;
                        Binary._fastWriteAlignedNativeUInt32(nData, ref nHead, index_0);
                        Binary._fastWriteAlignedNativeUInt32(nData, ref nHead, index_1);
                        Binary._fastWriteAlignedNativeUInt32(nData, ref nHead, index_2);
                    }
                    else
                    {
                        Debug.Assert(triInfo._usageInfo == _TriangleInfo._usedHot);
                        numHotTris++;
                    }
                    curTriOffset = triInfo.PrevTriangleOffset;
                }

                // Write hot triangle index data
                uint primBaseHotTriIndex = hotTriIndex;
                curTriOffset = primInfo.LastTriangleOffset;
                for (uint i = 0; i < primInfo.NumTriangles; i++)
                {
                    uint triHead = curTriOffset;
                    ref _TriangleInfo triInfo = ref Binary._fastReadAlignedNative<_TriangleInfo>(_data, ref triHead);
                    if (triInfo._usageInfo == _TriangleInfo._usedHot)
                    {
                        triInfo._usageInfo = hotTriIndex++;
                        uint vertHead_0 = triInfo.Endpoint_0.VertexOffset;
                        uint vertHead_1 = triInfo.Endpoint_1.VertexOffset;
                        uint vertHead_2 = triInfo.Endpoint_2.VertexOffset;
                        uint index_0 = Binary._fastReadAlignedNative<_VertexHeader>(_data, ref vertHead_0)._usageInfo;
                        uint index_1 = Binary._fastReadAlignedNative<_VertexHeader>(_data, ref vertHead_1)._usageInfo;
                        uint index_2 = Binary._fastReadAlignedNative<_VertexHeader>(_data, ref vertHead_2)._usageInfo;
                        Binary._fastWriteAlignedNativeUInt32(nData, ref nHead, index_0);
                        Binary._fastWriteAlignedNativeUInt32(nData, ref nHead, index_1);
                        Binary._fastWriteAlignedNativeUInt32(nData, ref nHead, index_2);
                    }
                    curTriOffset = triInfo.PrevTriangleOffset;
                }

                // Write hot triangle segment data
                var segs = Binary.AllocateAlignedNativeList<Mesh._SegmentInfo>(nData, ref nHead, numHotTris * 3);
                for (uint i = 0; i < segs.Length; i++)
                    segs[(int)i] = Mesh._SegmentInfo.Unconnected;
                curTriOffset = primInfo.LastTriangleOffset;
                for (uint i = 0; i < primInfo.NumTriangles; i++)
                {
                    uint triHead = curTriOffset;
                    ref _TriangleInfo triInfo = ref Binary._fastReadAlignedNative<_TriangleInfo>(_data, ref triHead);
                    if (triInfo._usageInfo != _TriangleInfo._usedCold)
                    {
                        uint vertOffset_0 = triInfo.Endpoint_0.VertexOffset;
                        uint vertOffset_1 = triInfo.Endpoint_1.VertexOffset;
                        uint vertOffset_2 = triInfo.Endpoint_2.VertexOffset;
                        _groupSegment(primBaseHotTriIndex,
                            segs, primInfo.VertexType,
                            new Mesh._Endpoint(triInfo._usageInfo, 0),
                            vertOffset_0, vertOffset_1, vertOffset_2,
                            triInfo.Endpoint_0.PrevVertexEndpoint);
                        _groupSegment(primBaseHotTriIndex,
                            segs, primInfo.VertexType,
                            new Mesh._Endpoint(triInfo._usageInfo, 1),
                            vertOffset_1, vertOffset_2, vertOffset_0,
                            triInfo.Endpoint_1.PrevVertexEndpoint);
                        _groupSegment(primBaseHotTriIndex,
                            segs, primInfo.VertexType,
                            new Mesh._Endpoint(triInfo._usageInfo, 2),
                            vertOffset_2, vertOffset_0, vertOffset_1,
                            triInfo.Endpoint_2.PrevVertexEndpoint);
                    }
                    curTriOffset = triInfo.PrevTriangleOffset;
                }

                // Write header
                Binary._fastWriteAlignedNative(nData, ref primHead, new Mesh._PrimitiveHeader
                {
                    VertexType = primInfo.VertexType,
                    NumVertices = primInfo.NumVertices,
                    NumHotVertices = primInfo.NumVertices - numColdVerts,
                    NumTriangles = primInfo.NumTriangles,
                    NumHotTriangles = hotTriIndex - primBaseHotTriIndex
                });
            }
            Debug.Assert(nHead == (uint)nData.Length);

            // Finalize ties
            ListBuilder<Mesh._Tie> nTies = ListBuilder<Mesh._Tie>.CreateDefault(_ties.Length);
            for (uint i = 0; i < _ties.Length; i++)
            {
                _Tie tie = _ties[i];

                // Finalize segment indices
                Mesh._Endpoint[] segs = tie.Segments;
                for (int j = 0; j < segs.Length; j++)
                {
                    ref Mesh._Endpoint seg = ref segs[j];
                    _Endpoint sourceSeg = (_Endpoint)seg;
                    uint triOffset = sourceSeg.TriangleOffset;
                    _TriangleInfo triInfo = Binary._fastReadAlignedNative<_TriangleInfo>(_data, ref triOffset);
                    seg = new Mesh._Endpoint(triInfo._usageInfo, sourceSeg.Phase);
                }

                // Finalize tie
                nTies[i] = new Mesh._Tie
                {
                    Reference = tie.Reference,
                    Transform = tie.Transform,
                    Textures = tie.Textures,
                    Segments = List.Of(segs)
                };
            }

            // Make sure the builder isn't used again
            _data = null;

            // Return final mesh
            return new Mesh(nData, nTies.Finish(), _textureBound);
        }

        /// <summary>
        /// Determines whether a segment of a "hot" triangle is adjacent to a coplanar triangle, and if so, updates
        /// the triangle data to reflect this relationship.
        /// </summary>
        private void _groupSegment(
            uint baseHotTriIndex,
            Span<Mesh._SegmentInfo> segInfos,
            MeshVertexType vertType,
            Mesh._Endpoint endpoint,
            uint startVertOffset,
            uint endVertOffset,
            uint acrossVertOffset,
            _Endpoint adjStartEndpoint)
        {
            while (!adjStartEndpoint.IsNull)
            {
                uint adjTriHead = adjStartEndpoint.TriangleOffset;
                ref var adjTriInfo = ref Binary._fastReadAlignedNative<_TriangleInfo>(_data, ref adjTriHead);

                // Only group hot triangles into polygons
                if (adjTriInfo._usageInfo < _TriangleInfo._usedCold)
                {
                    Debug.Assert(adjTriInfo.Endpoint(adjStartEndpoint.Phase).VertexOffset == startVertOffset);
                    uint adjStartVertOffset = adjTriInfo.Endpoint(adjStartEndpoint.Previous.Phase).VertexOffset;
                    if (adjStartVertOffset == endVertOffset)
                    {
                        // Determine whether triangles are coplanar
                        uint adjAcrossVertOffset = adjTriInfo.Endpoint(adjStartEndpoint.Next.Phase).VertexOffset;
                        uint startVertHead = startVertOffset + _VertexHeader.Size;
                        uint endVertHead = endVertOffset + _VertexHeader.Size;
                        uint acrossVertHead = acrossVertOffset + _VertexHeader.Size;
                        uint adjAcrossVertHead = adjAcrossVertOffset + _VertexHeader.Size;
                        var startVert = VertexBinary.ReadAlignedNativeMeshVertex(_data, ref startVertHead, vertType);
                        var endVert = VertexBinary.ReadAlignedNativeMeshVertex(_data, ref endVertHead, vertType);
                        var acrossVert = VertexBinary.ReadAlignedNativeMeshVertex(_data, ref acrossVertHead, vertType);
                        var adjAcrossVert = VertexBinary.ReadAlignedNativeMeshVertex(_data, ref adjAcrossVertHead, vertType);
                        Vector3 startPos = startVert.Position;
                        Vector3 endDiff = Vector3.Normalize(endVert.Position - startPos);
                        Vector3 acrossDiff = Vector3.Normalize(acrossVert.Position - startPos);
                        Vector3 adjAcrossDiff = Vector3.Normalize(adjAcrossVert.Position - startPos);
                        if (Vector3.Dot(Vector3.Cross(endDiff, acrossDiff), adjAcrossDiff).IsLikelyZero)
                        {
                            // Link segments
                            uint adjTriIndex = adjTriInfo._usageInfo;
                            var primEndpoint = endpoint - baseHotTriIndex;
                            var primAdjEndpoint = new Mesh._Endpoint(adjTriIndex - baseHotTriIndex, adjStartEndpoint.Phase);
                            var primComplEndpoint = primAdjEndpoint.Previous;
                            ref var segInfo = ref segInfos[(int)primEndpoint.Index];
                            ref var complSegInfo = ref segInfos[(int)primComplEndpoint.Index];
                            Debug.Assert(segInfo.IsUnconnected);
                            Debug.Assert(complSegInfo.IsUnconnected);
                            segInfo = Mesh._SegmentInfo.Connected(primComplEndpoint);
                            complSegInfo = Mesh._SegmentInfo.Connected(primEndpoint);
                            break;
                        }
                    }
                }
                adjStartEndpoint = adjTriInfo.Endpoint(adjStartEndpoint.Phase).PrevVertexEndpoint;
            }
        }
    }
}
