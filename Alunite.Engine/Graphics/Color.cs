﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

using Alunite.Data;
using Alunite.Data.Serialization;

namespace Alunite.Graphics
{
    /// <summary>
    /// Identifies a color in a linear RGB color space.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct Color : IEquatable<Color>
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never)] public Vector3 R_G_B;
        public Color(Vector3 r_g_b)
        {
            R_G_B = r_g_b;
        }
        
        public Color(Scalar r, Scalar g, Scalar b)
        {
            R_G_B = new Vector3(r, g, b);
        }

        /// <summary>
        /// The red component of this color, between 0 and 1.
        /// </summary>
        public Scalar R
        {
            get { return R_G_B.X; }
            set { R_G_B.X = value; }
        }

        /// <summary>
        /// The green component of this color, between 0 and 1.
        /// </summary>
        public Scalar G
        {
            get { return R_G_B.Y; }
            set { R_G_B.Y = value; }
        }

        /// <summary>
        /// The blue component of this color, between 0 and 1.
        /// </summary>
        public Scalar B
        {
            get { return R_G_B.Z; }
            set { R_G_B.Z = value; }
        }

        /// <summary>
        /// Linearly interpolates between two colors.
        /// </summary>
        public static Color Lerp(Color x, Color y, Scalar a)
        {
            return new Color(Vector3.Lerp(x.R_G_B, y.R_G_B, a));
        }
        
        /// <summary>
        /// An color white.
        /// </summary>
        public static Color White => new Color(1, 1, 1);
        
        /// <summary>
        /// The color black.
        /// </summary>
        public static Color Black => new Color(0, 0, 0);

        /// <summary>
        /// Contains potential encodings for <see cref="Color"/> values.
        /// </summary>
        public static class Encoding
        {
            /// <summary>
            /// A color encoding where components are bytes in BGR order. The component values are in linear colorspace.
            /// </summary>
            public static Encoding<Color> B8_G8_R8 => _B8_G8_R8.Instance;

            /// <summary>
            /// A color encoding where components are bytes in BGR order. The component values are encoded in sRGB,
            /// rather than a linear colorspace.
            /// </summary>
            public static Encoding<Color> Sb8_Sg8_Sr8 => _Sb8_Sg8_Sr8.Instance;

            /// <summary>
            /// A color encoding where components are 32-bit native endian floats in RGB order.
            /// </summary>
            [DefaultEncoding]
            public static Encoding<Color> Rfn32_Gfn32_Bfn32 { get; } = new BlittableEncoding<Color>(12, 4);

            /// <summary>
            /// The implementation for <see cref="B8_G8_R8"/>.
            /// </summary>
            private sealed class _B8_G8_R8 : Encoding<Color>
            {
                public _B8_G8_R8() : base(3, 1) { }

                /// <summary>
                /// The only instance of this class.
                /// </summary>
                public static _B8_G8_R8 Instance { get; } = new _B8_G8_R8();

                public override unsafe ref byte FastRead(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out Color value)
                {
                    return ref FastReadAligned(ref head, ref buffer, reader, out value);
                }

                public override ref byte FastReadAligned(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out Color value)
                {
                    Scalar b = Scalar.Encoding.Decode_U8(head);
                    head = ref Unsafe.AddByteOffset(ref head, (IntPtr)1);
                    Scalar g = Scalar.Encoding.Decode_U8(head);
                    head = ref Unsafe.AddByteOffset(ref head, (IntPtr)1);
                    Scalar r = Scalar.Encoding.Decode_U8(head);
                    head = ref Unsafe.AddByteOffset(ref head, (IntPtr)1);
                    value = new Color(r, g, b);
                    return ref head;
                }

                public override ref byte FastWrite(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in Color value)
                {
                    return ref FastWriteAligned(ref head, ref buffer, writer, in value);
                }

                public override ref byte FastWriteAligned(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in Color value)
                {
                    throw new NotImplementedException();
                }
            }

            /// <summary>
            /// The implementation for <see cref="Sb8_Sg8_Sr8"/>.
            /// </summary>
            private sealed class _Sb8_Sg8_Sr8 : Encoding<Color>
            {
                public _Sb8_Sg8_Sr8() : base(3, 1)  { }

                /// <summary>
                /// The only instance of this class.
                /// </summary>
                public static _Sb8_Sg8_Sr8 Instance { get; } = new _Sb8_Sg8_Sr8();

                public override ref byte FastRead(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out Color value)
                {
                    throw new NotImplementedException();
                }

                public override ref byte FastReadAligned(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out Color value)
                {
                    throw new NotImplementedException();
                }

                public override ref byte FastWrite(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in Color value)
                {
                    throw new NotImplementedException();
                }

                public override ref byte FastWriteAligned(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in Color value)
                {
                    throw new NotImplementedException();
                }
            }
        }

        public static Color operator *(Scalar a, Color b)
        {
            return b * a;
        }

        public static Color operator *(Color a, Scalar b)
        {
            return new Color(a.R_G_B * b);
        }

        public static bool operator ==(Color a, Color b)
        {
            return a.R_G_B == b.R_G_B;
        }

        public static bool operator !=(Color a, Color b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Color))
                return false;
            return this == (Color)obj;
        }

        bool IEquatable<Color>.Equals(Color other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return R_G_B.GetHashCode();
        }

        public override string ToString()
        {
            return R_G_B.ToString();
        }
    }

    /// <summary>
    /// Combines a <see cref="Graphics.Color"/> with an alpha value indicate how much to weight the color
    /// for compositing.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct Paint
    {
        public Vector4 R_G_B_A;
        public Paint(Color color, Scalar a)
        {
            R_G_B_A = new Vector4(color.R_G_B * a, a);
        }
        
        public Paint(Scalar r, Scalar g, Scalar b, Scalar a)
        {
            R_G_B_A = new Vector4(r, g, b, a);
        }

        /// <summary>
        /// An opaque white paint.
        /// </summary>
        public static Paint White => new Paint(1, 1, 1, 1);

        /// <summary>
        /// An opaque black paint.
        /// </summary>
        public static Paint Black => new Paint(0, 0, 0, 1);

        /// <summary>
        /// A completely transparent paint.
        /// </summary>
        public static Paint Invisible => new Paint(0, 0, 0, 0);

        /// <summary>
        /// Constructs an opaque paint of the given color.
        /// </summary>
        public static Paint Opaque(Color color)
        {
            return new Paint(color.R, color.G, color.B, 1);
        }

        /// <summary>
        /// The red component of this paint, between 0 and 1.
        /// </summary>
        public Scalar R
        {
            get { return R_G_B_A.X; }
            set { R_G_B_A.X = value; }
        }

        /// <summary>
        /// The green component of this paint, between 0 and 1.
        /// </summary>
        public Scalar G
        {
            get { return R_G_B_A.Y; }
            set { R_G_B_A.Y = value; }
        }

        /// <summary>
        /// The blue component of this paint, between 0 and 1.
        /// </summary>
        public Scalar B
        {
            get { return R_G_B_A.Z; }
            set { R_G_B_A.Z = value; }
        }

        /// <summary>
        /// The RGB components of this paint.
        /// </summary>
        public Vector3 R_G_B
        {
            get { return R_G_B_A.X_Y_Z; }
            set { R_G_B_A.X_Y_Z = value; }
        }

        /// <summary>
        /// The alpha component of this paint, between 0 and 1.
        /// </summary>
        public Scalar A
        {
            get { return R_G_B_A.W; }
            set { R_G_B_A.W = value; }
        }

        /// <summary>
        /// The color of this paint. This is undefined if the paint is invisible.
        /// </summary>
        public Color Color => new Color(R_G_B * (1.0 / A));

        /// <summary>
        /// Contains potential encodings for <see cref="Paint"/> values.
        /// </summary>
        public static class Encoding
        {
            /// <summary>
            /// Constructs a <see cref="Paint"/> encoding based on the given <see cref="Color"/> encoding which assumes
            /// all values have an alpha of 1 (i.e. are opaque).
            /// </summary>
            public static Encoding<Paint> Opaque(Encoding<Color> source)
            {
                return _opaqueCache.GetOrAdd(source, _opaqueFactory);
            }

            /// <summary>
            /// A cache of encodings produced by <see cref="Opaque(Encoding{Color})"/>.
            /// </summary>
            private static readonly LookupDictionary<Encoding<Color>, Encoding<Paint>> _opaqueCache =
                new LookupDictionary<Encoding<Color>, Encoding<Paint>>();

            /// <summary>
            /// The factory function for <see cref="_opaqueCache"/>.
            /// </summary>
            private static Func<Encoding<Color>, Encoding<Paint>> _opaqueFactory = source => new _Opaque(source);

            /// <summary>
            /// A paint encoding where components are bytes in BGR order and alpha is assumed to be 1.
            /// The component values are in a linear colorspace.
            /// </summary>
            public static Encoding<Paint> B8_G8_R8 { get; } = Opaque(Color.Encoding.B8_G8_R8);

            /// <summary>
            /// A paint encoding where components are bytes in BGR order and alpha is assumed to be 1.
            /// The component values are encoded in sRGB rather than a linear colorspace.
            /// </summary>
            public static Encoding<Paint> Sb8_Sg8_Sr8 { get; } = Opaque(Color.Encoding.Sb8_Sg8_Sr8);

            /// <summary>
            /// A paint encoding where components are bytes in BGRA order. The alpha is not pre-multiplied.
            /// The component values are in a linear colorspace.
            /// </summary>
            public static Encoding<Paint> B8_G8_R8_A8 => _B8_G8_R8_A8.Instance;

            /// <summary>
            /// A paint encoding where components are bytes in BGRA order. The alpha is not pre-multiplied.
            /// The component values are encoded in sRGB rather than a linear colorspace.
            /// </summary>
            public static Encoding<Paint> Sb8_Sg8_Sr8_A8 => _Sb8_Sg8_Sr8_A8.Instance;

            /// <paint>
            /// A pixel encoding where components are little-endian <see cref="ushort"/>s in RGBA order. The alpha is
            /// not pre-multiplied. The component values are in a linear colorspace.
            /// </summary>
            public static Encoding<Paint> R16_G16_B16_A16 => _R16_G16_B16_A16.Instance;

            /// <summary>
            /// Determines whether the given paint encoding can be reduced to a color encoding with an assumed
            /// alpha of 1.
            /// </summary>
            public static bool IsOpaque(Encoding<Paint> encoding, out Encoding<Color> source)
            {
                if (encoding is _Opaque opaque)
                {
                    source = opaque.Source;
                    return true;
                }
                source = default;
                return false;
            }

            /// <summary>
            /// Encodes a paint to a <see cref="uint"/> using the <see cref="R8_G8_B8_A8"/> encoding.
            /// </summary>
            public static uint Encode_R8_G8_B8_A8(Paint value)
            {
                return (uint)Scalar.Encoding.Encode_U8(value.R) |
                    ((uint)Scalar.Encoding.Encode_U8(value.G) << 8) |
                    ((uint)Scalar.Encoding.Encode_U8(value.B) << 16) |
                    ((uint)Scalar.Encoding.Encode_U8(value.A) << 24);
            }

            /// <summary>
            /// Decodes a paint from a <see cref="uint"/> using the <see cref="R8_G8_B8_A8"/> encoding.
            /// </summary>
            public static Paint Decode_R8_G8_B8_A8(uint value)
            {
                return new Paint(
                    Scalar.Encoding.Decode_U8(unchecked((byte)value)),
                    Scalar.Encoding.Decode_U8(unchecked((byte)(value >> 8))),
                    Scalar.Encoding.Decode_U8(unchecked((byte)(value >> 16))),
                    Scalar.Encoding.Decode_U8(unchecked((byte)(value >> 24))));
            }

            /// <summary>
            /// The implementation of <see cref="Opaque(Encoding{Color})"/>.
            /// </summary>
            private sealed class _Opaque : Encoding<Paint>
            {
                public _Opaque(Encoding<Color> source) : base(source.Size, source.Align)
                {
                    Source = source;
                }

                /// <summary>
                /// The color encoding on which this is based.
                /// </summary>
                public Encoding<Color> Source { get; }

                public override unsafe ref byte FastRead(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out Paint value)
                {
                    value.R_G_B_A = new Vector4(0, 0, 0, 1);
                    ref Color color = ref Unsafe.As<Paint, Color>(ref value);
                    return ref Source.FastRead(ref head, ref buffer, reader, out color);
                }

                public override ref byte FastReadAligned(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out Paint value)
                {
                    value.R_G_B_A = new Vector4(0, 0, 0, 1);
                    ref Color color = ref Unsafe.As<Paint, Color>(ref value);
                    return ref Source.FastReadAligned(ref head, ref buffer, reader, out color);
                }

                public override unsafe ref byte FastWrite(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in Paint value)
                {
                    ref Color color = ref Unsafe.As<Paint, Color>(ref Unsafe.AsRef(in value));
                    return ref Source.FastWrite(ref head, ref buffer, writer, in color);
                }

                public override ref byte FastWriteAligned(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in Paint value)
                {
                    ref Color color = ref Unsafe.As<Paint, Color>(ref Unsafe.AsRef(in value));
                    return ref Source.FastWriteAligned(ref head, ref buffer, writer, in color);
                }
            }

            /// <summary>
            /// The implementation for <see cref="B8_G8_R8_A8"/>.
            /// </summary>
            private sealed class _B8_G8_R8_A8 : Encoding<Paint>
            {
                public _B8_G8_R8_A8() : base(4, 1) { }

                /// <summary>
                /// The only instance of this class.
                /// </summary>
                public static _B8_G8_R8_A8 Instance { get; } = new _B8_G8_R8_A8();

                public override ref byte FastRead(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out Paint value)
                {
                    return ref FastReadAligned(ref head, ref buffer, reader, out value);
                }

                public override unsafe ref byte FastReadAligned(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out Paint value)
                {
                    Scalar b = Scalar.Encoding.Decode_U8(head);
                    head = ref Unsafe.AddByteOffset(ref head, (IntPtr)1);
                    Scalar g = Scalar.Encoding.Decode_U8(head);
                    head = ref Unsafe.AddByteOffset(ref head, (IntPtr)1);
                    Scalar r = Scalar.Encoding.Decode_U8(head);
                    head = ref Unsafe.AddByteOffset(ref head, (IntPtr)1);
                    Scalar a = Scalar.Encoding.Decode_U8(head);
                    head = ref Unsafe.AddByteOffset(ref head, (IntPtr)1);
                    value = new Paint(new Color(r, g, b), a);
                    return ref head;
                }

                public override ref byte FastWrite(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in Paint value)
                {
                    return ref FastWriteAligned(ref head, ref buffer, writer, in value);
                }

                public override ref byte FastWriteAligned(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in Paint value)
                {
                    throw new NotImplementedException();
                }
            }

            /// <summary>
            /// The implementation for <see cref="Sb8_Sg8_Sr8_A8"/>.
            /// </summary>
            private sealed class _Sb8_Sg8_Sr8_A8 : Encoding<Paint>
            {
                public _Sb8_Sg8_Sr8_A8() : base(4, 1) { }

                /// <summary>
                /// The only instance of this class.
                /// </summary>
                public static _Sb8_Sg8_Sr8_A8 Instance { get; } = new _Sb8_Sg8_Sr8_A8();

                public override unsafe ref byte FastRead(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out Paint value)
                {
                    throw new NotImplementedException();
                }

                public override ref byte FastReadAligned(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out Paint value)
                {
                    throw new NotImplementedException();
                }

                public override ref byte FastWrite(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in Paint value)
                {
                    throw new NotImplementedException();
                }

                public override ref byte FastWriteAligned(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in Paint value)
                {
                    throw new NotImplementedException();
                }
            }

            /// <summary>
            /// The implementation for <see cref="R16_G16_B16_A16"/>.
            /// </summary>
            private sealed class _R16_G16_B16_A16 : Encoding<Paint>
            {
                public _R16_G16_B16_A16() : base(8, 2) { }

                /// <summary>
                /// The only instance of this class.
                /// </summary>
                public static _R16_G16_B16_A16 Instance { get; } = new _R16_G16_B16_A16();

                public override unsafe ref byte FastRead(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out Paint value)
                {
                    Scalar r = Scalar.Encoding.Decode_U16(Unsafe.ReadUnaligned<ushort>(ref head));
                    head = ref Unsafe.AddByteOffset(ref head, (IntPtr)2);
                    Scalar g = Scalar.Encoding.Decode_U16(Unsafe.ReadUnaligned<ushort>(ref head));
                    head = ref Unsafe.AddByteOffset(ref head, (IntPtr)2);
                    Scalar b = Scalar.Encoding.Decode_U16(Unsafe.ReadUnaligned<ushort>(ref head));
                    head = ref Unsafe.AddByteOffset(ref head, (IntPtr)2);
                    Scalar a = Scalar.Encoding.Decode_U16(Unsafe.ReadUnaligned<ushort>(ref head));
                    head = ref Unsafe.AddByteOffset(ref head, (IntPtr)2);
                    value = new Paint(new Color(r, g, b), a);
                    return ref head;
                }

                public override unsafe ref byte FastReadAligned(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out Paint value)
                {
                    Scalar r = Scalar.Encoding.Decode_U16(Unsafe.As<byte, ushort>(ref head));
                    head = ref Unsafe.AddByteOffset(ref head, (IntPtr)2);
                    Scalar g = Scalar.Encoding.Decode_U16(Unsafe.As<byte, ushort>(ref head));
                    head = ref Unsafe.AddByteOffset(ref head, (IntPtr)2);
                    Scalar b = Scalar.Encoding.Decode_U16(Unsafe.As<byte, ushort>(ref head));
                    head = ref Unsafe.AddByteOffset(ref head, (IntPtr)2);
                    Scalar a = Scalar.Encoding.Decode_U16(Unsafe.As<byte, ushort>(ref head));
                    head = ref Unsafe.AddByteOffset(ref head, (IntPtr)2);
                    value = new Paint(new Color(r, g, b), a);
                    return ref head;
                }

                public override ref byte FastWrite(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in Paint value)
                {
                    throw new NotImplementedException();
                }

                public override ref byte FastWriteAligned(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in Paint value)
                {
                    throw new NotImplementedException();
                }
            }
        }

        public static Paint operator *(Paint a, Paint b)
        {
            return new Paint(a.R * b.R, a.G * b.G, a.B * b.B, a.A * b.A);
        }

        public override string ToString()
        {
            return R_G_B_A.ToString();
        }
    }

    /// <summary>
    /// A color-separated measure of radiant energy. Depending on context, this can be used to represent radiance,
    /// radiant intensity, radiant flux or irradiance. Unlike <see cref="Color"/>, there is no upper bound on radiance.
    /// </summary>
    public struct Radiance
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never)] public Vector3 R_G_B;
        public Radiance(Vector3 r_g_b)
        {
            R_G_B = r_g_b;
        }

        public Radiance(Scalar r, Scalar g, Scalar b)
        {
            R_G_B = new Vector3(r, g, b);
        }

        /// <summary>
        /// The red component of this radiance, between 0 and 1.
        /// </summary>
        public Scalar R
        {
            get { return R_G_B.X; }
            set { R_G_B.X = value; }
        }

        /// <summary>
        /// The green component of this radiance, between 0 and 1.
        /// </summary>
        public Scalar G
        {
            get { return R_G_B.Y; }
            set { R_G_B.Y = value; }
        }

        /// <summary>
        /// The blue component of this radiance, between 0 and 1.
        /// </summary>
        public Scalar B
        {
            get { return R_G_B.Z; }
            set { R_G_B.Z = value; }
        }

        /// Contains potential encodings for <see cref="Radiance"/> values.
        /// </summary>
        public static class Encoding
        {
            /// <summary>
            /// A radiance encoding where components are bytes in RGB order and share a signed 8-bit exponent.
            /// </summary>
            public static Encoding<Radiance> R8_G8_B8_e8 => _R8_G8_B8_e8.Instance;

            /// <summary>
            /// A radiance encoding where components are 32-bit native endian floats in RGB order.
            /// </summary>
            [DefaultEncoding]
            public static Encoding<Radiance> Rfn32_Gfn32_Bfn32 { get; } = new BlittableEncoding<Radiance>(12, 4);

            /// <summary>
            /// The implementation for <see cref="R8_G8_B8_e8"/>.
            /// </summary>
            private sealed class _R8_G8_B8_e8 : Encoding<Radiance>
            {
                public _R8_G8_B8_e8() : base(4, 1) { }

                /// <summary>
                /// The only instance of this class.
                /// </summary>
                public static _R8_G8_B8_e8 Instance { get; } = new _R8_G8_B8_e8();

                public override ref byte FastRead(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out Radiance value)
                {
                    return ref FastReadAligned(ref head, ref buffer, reader, out value);
                }

                public override unsafe ref byte FastReadAligned(
                    ref byte head, ref ReadOnlySpan<byte> buffer,
                    IBinaryReader reader, out Radiance value)
                {
                    byte r = head;
                    head = ref Unsafe.AddByteOffset(ref head, (IntPtr)1);
                    byte g = head;
                    head = ref Unsafe.AddByteOffset(ref head, (IntPtr)1);
                    byte b = head;
                    head = ref Unsafe.AddByteOffset(ref head, (IntPtr)1);
                    byte e = head;
                    head = ref Unsafe.AddByteOffset(ref head, (IntPtr)1);
                    Scalar mult = Scalar.Pow(2, e - 128 - 8);
                    value = new Radiance(new Vector3(r, g, b) * mult);
                    return ref head;
                }

                public override ref byte FastWrite(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in Radiance value)
                {
                    return ref FastWriteAligned(ref head, ref buffer, writer, in value);
                }

                public override ref byte FastWriteAligned(
                    ref byte head, ref Span<byte> buffer,
                    IBinaryWriter writer, in Radiance value)
                {
                    throw new NotImplementedException();
                }
            }
        }
    }
}