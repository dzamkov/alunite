﻿using System;
using System.Diagnostics;

using Alunite.Data;
using Alunite.Data.Geometry;
using Alunite.Data.Probability;
using Alunite.Resources;

namespace Alunite.Graphics
{
    /// <summary>
    /// Extrapolates a <see cref="Color"/> <see cref="Image2{T}"/> to a continuous two-dimensional space.
    /// </summary>
    public struct ColorTexture2 : ITexture2<Color>, IEquatable<ColorTexture2>
    {
        public ColorTexture2(Sampler2 sampler, BigImage2<Color> image)
        {
            Sampler = sampler;
            Image = image;
        }

        /// <summary>
        /// The sampler for this texture.
        /// </summary>
        public Sampler2 Sampler { get; }

        /// <summary>
        /// The source image for this texture.
        /// </summary>
        public BigImage2<Color> Image { get; }

        public static bool operator ==(ColorTexture2 a, ColorTexture2 b)
        {
            return a.Image == b.Image && a.Sampler == b.Sampler;
        }

        public static bool operator !=(ColorTexture2 a, ColorTexture2 b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is ColorTexture2))
                return false;
            return this == (ColorTexture2)obj;
        }

        bool IEquatable<ColorTexture2>.Equals(ColorTexture2 other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return HashCodeHelper.Combine(
                Sampler.GetHashCode(),
                Image.GetHashCode());
        }
    }

    /// <summary>
    /// Extrapolates a <see cref="Paint"/> <see cref="Image2{T}"/> to a continuous two-dimensional space.
    /// </summary>
    public struct PaintTexture2 : ITexture2<Paint>, IEquatable<PaintTexture2>
    {
        public PaintTexture2(Sampler2 sampler, BigImage2<Paint> image)
        {
            Sampler = sampler;
            Image = image;
        }

        /// <summary>
        /// The sampler for this texture.
        /// </summary>
        public Sampler2 Sampler { get; }

        /// <summary>
        /// The source image for this texture.
        /// </summary>
        public BigImage2<Paint> Image { get; }

        public static bool operator ==(PaintTexture2 a, PaintTexture2 b)
        {
            return a.Image == b.Image && a.Sampler == b.Sampler;
        }

        public static bool operator !=(PaintTexture2 a, PaintTexture2 b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is PaintTexture2))
                return false;
            return this == (PaintTexture2)obj;
        }

        bool IEquatable<PaintTexture2>.Equals(PaintTexture2 other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return HashCodeHelper.Combine(
                Sampler.GetHashCode(),
                Image.GetHashCode());
        }
    }

    /// <summary>
    /// Extrapolates a <see cref="Radiance"/> <see cref="Image2{T}"/> to a continuous two-dimensional space.
    /// </summary>
    public struct RadianceTexture2 : ITexture2<Radiance>, IEquatable<RadianceTexture2>
    {
        public RadianceTexture2(Sampler2 sampler, BigImage2<Radiance> image)
        {
            Sampler = sampler;
            Image = image;
        }

        /// <summary>
        /// The sampler for this texture.
        /// </summary>
        public Sampler2 Sampler { get; }

        /// <summary>
        /// The source image for this texture.
        /// </summary>
        public BigImage2<Radiance> Image { get; }

        public static bool operator ==(RadianceTexture2 a, RadianceTexture2 b)
        {
            return a.Image == b.Image && a.Sampler == b.Sampler;
        }

        public static bool operator !=(RadianceTexture2 a, RadianceTexture2 b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is RadianceTexture2))
                return false;
            return this == (RadianceTexture2)obj;
        }

        bool IEquatable<RadianceTexture2>.Equals(RadianceTexture2 other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return HashCodeHelper.Combine(
                Sampler.GetHashCode(),
                Image.GetHashCode());
        }
    }

    /// <summary>
    /// Extrapolates an <see cref="Image2{T}"/> to a continuous two-dimensional space.
    /// </summary>
    public interface ITexture2<T>
    {
        /// <summary>
        /// The sampler for this texture.
        /// </summary>
        Sampler2 Sampler { get; }

        /// <summary>
        /// The source image for this texture.
        /// </summary>
        BigImage2<T> Image { get; }
    }

    /// <summary>
    /// Specifies the sampling behavior of a two-dimensional texture. The extrapolates a discrete finite image
    /// into a continuous two-dimensional space.
    /// </summary>
    public struct Sampler2 : IEquatable<Sampler2>
    {
        public Sampler2(SamplerWrapping wrapping_x, SamplerWrapping wrapping_y, SamplerFiltering filtering)
        {
            Wrapping_X = wrapping_x;
            Wrapping_Y = wrapping_y;
            Filtering = filtering;
        }

        /// <summary>
        /// The sampling behavior when sampled outside the [0, 1] interval along the X axis.
        /// </summary>
        public SamplerWrapping Wrapping_X;

        /// <summary>
        /// The sampling behavior when sampled outside the [0, 1] interval along the Y axis.
        /// </summary>
        public SamplerWrapping Wrapping_Y;

        /// <summary>
        /// The sampling behavior between pixels.
        /// </summary>
        public SamplerFiltering Filtering;

        public static bool operator ==(Sampler2 a, Sampler2 b)
        {
            return a.Wrapping_X == b.Wrapping_X && a.Wrapping_Y == b.Wrapping_Y && a.Filtering == b.Filtering;
        }

        public static bool operator !=(Sampler2 a, Sampler2 b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Sampler2))
                return false;
            return this == (Sampler2)obj;
        }

        bool IEquatable<Sampler2>.Equals(Sampler2 other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return HashCodeHelper.Combine(
                Wrapping_X.GetHashCode(),
                Wrapping_Y.GetHashCode(),
                Filtering.GetHashCode());
        }
    }

    /// <summary>
    /// Describes the sampling behavior of a texture when sampled outside the [0, 1] interval
    /// along a particular dimension.
    /// </summary>
    public enum SamplerWrapping : byte
    {
        /// <summary>
        /// The texture content outside the [0, 1] range is undefined.
        /// </summary>
        Undefined,

        /// <summary>
        /// The texture is repeated outside the [0, 1] range.
        /// </summary>
        Repeat,

        /// <summary>
        /// The closest value within the [0, 1] range is used.
        /// </summary>
        Clamp
    }

    /// <summary>
    /// Describes the sampling behavior of a texture between pixels defined in the source image.
    /// </summary>
    public enum SamplerFiltering : byte
    {
        /// <summary>
        /// The nearest pixel value is used.
        /// </summary>
        Nearest,

        /// <summary>
        /// Pixel values are linearly interpolated.
        /// </summary>
        Linear
    }

    /// <summary>
    /// Describes the set of possible <see cref="Motion2"/>s that can be applied to a texture's
    /// coordinates when used in a mesh. Laxity is used to add variation to models, and to eliminate seams in texture
    /// coordinates when meshes are merged.
    /// </summary>
    [DebuggerDisplay("{ToString(),nq}")]
    public struct TextureLaxity
    {
        private byte _code;
        private Vector2 _vec;
        private TextureLaxity(byte code, Vector2 vec)
        {
            _code = code;
            _vec = vec;
        }

        /// <summary>
        /// No transformations are allowed other than the identity.
        /// </summary>
        public static TextureLaxity None => new TextureLaxity(0, default);

        /// <summary>
        /// Translations along <paramref name="dir"/> are allowed.
        /// </summary>
        public static TextureLaxity Linear(Vector2 dir)
        {
            return new TextureLaxity(1, Vector2.Normalize(dir));
        }

        /// <summary>
        /// Translations consisting of some fraction of <paramref name="offset"/> and some vector perpendicular to it
        /// are allowed.
        /// </summary>
        public static TextureLaxity Band(Vector2 offset)
        {
            return new TextureLaxity(2, offset);
        }

        /// <summary>
        /// All translations are allowed.
        /// </summary>
        public static TextureLaxity Translation => new TextureLaxity(3, default);

        /// <summary>
        /// Rotations about the given point are allowed.
        /// </summary>
        public static TextureLaxity Rotation(Vector2 center)
        {
            return new TextureLaxity(4, center);
        }

        /// <summary>
        /// All <see cref="Motion2"/> transforms are allowed.
        /// </summary>
        public static TextureLaxity Any => new TextureLaxity(5, default);

        /// <summary>
        /// Indicates whether this is <see cref="None"/>.
        /// </summary>
        public bool IsNone => _code == 0;

        /// <summary>
        /// Samples from a uniform distribution of the motions allowed by this <see cref="TextureLaxity"/>.
        /// </summary>
        public Motion2 Sample(ISampler sampler)
        {
            Scalar bound = 100;
            switch (_code)
            {
                case 0:
                    return Motion2.Identity;
                case 1:
                    return Motion2.Translate(_vec * (sampler.SampleUnit() * bound));
                case 2:
                    return Motion2.Translate(
                        _vec * sampler.SampleUnit() +
                        Vector2.Normalize(_vec.Cross) * (sampler.SampleUnit() * bound));
                case 3:
                    return Motion2.Translate(sampler.SampleUnit() * bound, sampler.SampleUnit() * bound);
                case 4:
                    return
                        Motion2.Translate(_vec) *
                        Rotation2.FromAngle(sampler.SampleUnit() * (Scalar.Pi * 2)) *
                        Motion2.Translate(-_vec);
                case 5:
                    return new Motion2(
                        Rotation2.FromAngle(sampler.SampleUnit() * (Scalar.Pi * 2)),
                        new Vector2(sampler.SampleUnit() * bound, sampler.SampleUnit() * bound));
                default: throw new InvalidOperationException();
            }
        }

        /// <summary>
        /// Gets the set of <see cref="Motion2"/>s resulting from the conjugation of motions in this laxity
        /// with <paramref name="trans"/>.
        /// </summary>
        public TextureLaxity Conjugate(Rotation2 trans)
        {
            return new TextureLaxity(_code, trans * _vec);
        }

        /// <summary>
        /// Gets the set of <see cref="Motion2"/>s that may be freely applied to the texture coordinates of a material
        /// with the given <see cref="MaterialKind"/>, after transforming by a <see cref="Motion2"/> in this laxity.
        /// </summary>
        public TextureLaxity Upon(MaterialKind kind)
        {
            if (kind.RequiresIsotropic)
                return Any;
            if (kind.RequiresPlane)
                return _code >= 4 ? Any : Translation;
            if (kind.RequiresBand)
                throw new NotImplementedException();
            return this;
        }

        /// <summary>
        /// Consider the set of <see cref="Motion2"/>s formed by applying <paramref name="trans"/> to a valid
        /// transformation in <paramref name="other"/>. This will attempt to restrict the set such that it includes
        /// only the transformations of this <see cref="TextureLaxity"/>, or return <see cref="false"/> if the
        /// resulting set would be empty.
        /// </summary>
        /// <param name="preTrans">An extra transform to be applied before <paramref name="trans"/> to
        /// the modified <paramref name="other"/> laxity.</param>
        public bool TryRestrict(Motion2 trans, out Motion2 preTrans, ref TextureLaxity other)
        {
            switch (_code)
            {
                case 0:
                    // TODO: Verify the set contains the identity transform, update it to only contain that transform
                    throw new NotImplementedException();
                case 1:
                    throw new NotImplementedException();
                case 2:
                    throw new NotImplementedException();
                case 3:
                    throw new NotImplementedException();
                case 4:
                    throw new NotImplementedException();
                case 5:
                    preTrans = Motion2.Identity;
                    return true;
                default:
                    throw new InvalidOperationException();
            }
        }

        public override int GetHashCode()
        {
            return _code.GetHashCode();
        }

        public override string ToString()
        {
            switch (_code)
            {
                case 0: return nameof(None);
                case 1: return nameof(Linear) + _vec.ToString();
                case 2: return nameof(Band) + _vec.ToString();
                case 3: return nameof(Translation);
                case 4: return nameof(Rotation) + _vec.ToString();
                case 5: return nameof(Any);
                default: throw new InvalidOperationException();
            }
        }
    }

    /// <summary>
    /// A texture of some sort defined by an affine transformation of an indexed source texture.
    /// </summary>
    public struct DependentTexture
    {
        public DependentTexture(uint index, Rotation2 tbn, Affine2 coord)
        {
            Index = index;
            Tbn = tbn;
            Coord = coord;
        }

        /// <summary>
        /// The index of the source texture this texture is derived from.
        /// </summary>
        public uint Index;

        /// <summary>
        /// The transform applied to the TBN basis of the vertices referencing the texture.
        /// </summary>
        public Rotation2 Tbn;

        /// <summary>
        /// The transformation applied to the coordinates for the texture.
        /// </summary>
        public Affine2 Coord;

        /// <summary>
        /// Constructs a <see cref="DependentTexture"/> representing the given parameter without modification.
        /// </summary>
        public static DependentTexture Parameter(uint index)
        {
            return new DependentTexture(index, Rotation2.Identity, Affine2.Identity);
        }

        /// <summary>
        /// Constructs a <see cref="List{T}"/> of the textures where every texture is identical to the source for
        /// its index.
        /// </summary>
        public static List<DependentTexture> Identity(uint len)
        {
            if (len < (uint)_identityTable.Length)
                return _identityTable[len];
            DependentTexture[] items = new DependentTexture[len];
            for (uint j = 0; j < len; j++)
                items[j] = Parameter(j);
            return new List<DependentTexture>(items);
        }

        /// <summary>
        /// A table of reusable <see cref="Identity"/> lists of small lengths.
        /// </summary>
        private static readonly List<DependentTexture>[] _identityTable = _buildIdentityTable();

        /// <summary>
        /// Builds <see cref="_identityTable"/>.
        /// </summary>
        private static List<DependentTexture>[] _buildIdentityTable()
        {
            List<DependentTexture>[] table = new List<DependentTexture>[32];
            table[0] = List.Empty<DependentTexture>();
            for (int i = 1; i < table.Length; i++)
            {
                DependentTexture[] items = new DependentTexture[i];
                for (uint j = 0; j < i; j++)
                    items[j] = Parameter(j);
                table[i] = new List<DependentTexture>(items);
            }
            return table;
        }

        /// <summary>
        /// Applies a substitution to the parameter referenced by this <see cref="DependentTexture"/>.
        /// </summary>
        public DependentTexture Apply(SpanList<DependentTexture> textures)
        {
            DependentTexture texture = textures[Index];
            return new DependentTexture(texture.Index,
                texture.Tbn * Tbn,
                texture.Coord * Coord);
        }
    }
}
