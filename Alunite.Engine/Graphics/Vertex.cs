﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

using Alunite.Data;
using Alunite.Data.Geometry;
using Alunite.Data.Serialization;

namespace Alunite.Graphics
{
    /// <summary>
    /// A vertex in a mesh.
    /// </summary>
    public interface IVertex2
    {
        /// <summary>
        /// The position of the vertex.
        /// </summary>
        Vector2 Position { get; }
    }

    /// <summary>
    /// A vertex in a mesh.
    /// </summary>
    public interface IVertex3
    {
        /// <summary>
        /// The position of the vertex.
        /// </summary>
        Vector3 Position { get; }
    }

    /// <summary>
    /// A <see cref="SolidVertex"/> which stores a texture index, allowing for multiple textures within
    /// the same mesh.
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 44)]
    public struct Blend1SolidVertex : IVertex3
    {
        /// <summary>
        /// The position of this vertex in three-dimensional space.
        /// </summary>
        [FieldOffset(0)] public Vector3 Position;

        /// <summary>
        /// Encapsulates the properties for the texture applied to this vertex.
        /// </summary>
        [FieldOffset(12)] public VertexTextureFactor Texture;
        
        Vector3 IVertex3.Position => Position;
    }

    /// <summary>
    /// An extension of <see cref="Blend1SolidVertex"/> which allows blending between 2 textures.
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 76)]
    public struct Blend2SolidVertex : IVertex3
    {
        /// <summary>
        /// The position of this vertex in three-dimensional space.
        /// </summary>
        [FieldOffset(0)] public Vector3 Position;

        /// <summary>
        /// The first texture factor applied to this vertex.
        /// </summary>
        [FieldOffset(12)] public VertexTextureFactor Texture_0;

        /// <summary>
        /// The second texture factor applied to this vertex.
        /// </summary>
        [FieldOffset(44)] public VertexTextureFactor Texture_1;
        
        Vector3 IVertex3.Position => Position;
    }

    /// <summary>
    /// An extension of <see cref="Blend1SolidVertex"/> which allows blending between 4 textures.
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 140)]
    public struct Blend4SolidVertex : IVertex3
    {
        /// <summary>
        /// The position of this vertex in three-dimensional space.
        /// </summary>
        [FieldOffset(0)] public Vector3 Position;

        /// <summary>
        /// The first texture factor applied to this vertex.
        /// </summary>
        [FieldOffset(12)] public VertexTextureFactor Texture_0;

        /// <summary>
        /// The second texture factor applied to this vertex.
        /// </summary>
        [FieldOffset(44)] public VertexTextureFactor Texture_1;

        /// <summary>
        /// The third texture factor applied to this vertex.
        /// </summary>
        [FieldOffset(76)] public VertexTextureFactor Texture_2;

        /// <summary>
        /// The fourth texture factor applied to this vertex.
        /// </summary>
        [FieldOffset(108)] public VertexTextureFactor Texture_3;

        Vector3 IVertex3.Position => Position;
    }

    /// <summary>
    /// Describes the contribution of a single texture to a vertex.
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = (int)Size)]
    public struct VertexTextureFactor
    {
        /// <summary>
        /// The size of this structure.
        /// </summary>
        public const uint Size = 32;

        /// <summary>
        /// The transformation from the tangent space of the source texture to object space.
        /// This implicitly defines the normal of the vertex.
        /// </summary>
        // TODO: Replace with Roflection to account for handedness
        [FieldOffset(0)] public Roflection3 Tbn;

        /// <summary>
        /// Combines the coordinate, index, alpha multipler and blend mode of the texture applied to
        /// this vertex.
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        [FieldOffset(16)] private System.Numerics.Vector4 _def;

        /// <summary>
        /// A "dummy" blend factor that doesn't contribute to the vertex appearance.
        /// </summary>
        public static VertexTextureFactor Empty => new VertexTextureFactor
        {
            Tbn = Rotation3.Identity,
            _def = new System.Numerics.Vector4(0f, 0f, 0f, +0f)
        };

        /// <summary>
        /// Constructs a <see cref="VertexTextureFactor"/> with the given properties.
        /// </summary>
        public static VertexTextureFactor Build(
            uint index, Vector2 coord, Roflection3 tbn,
            Scalar alpha, bool shouldOverlay)
        {
            float w = alpha._mean;
            if (shouldOverlay) w = Math.Min(-w, _zeroOverlay);
            return new VertexTextureFactor
            {
                Tbn = tbn,
                _def = new System.Numerics.Vector4(coord.X._mean, coord.Y._mean, index, w)
            };
        }

        /// <summary>
        /// The index of the source texture for this factor.
        /// </summary>
        public uint Index
        {
            get
            {
                return (uint)_def.Z;
            }
            set
            {
                _def.Z = value;
            }
        }

        /// <summary>
        /// The position of the vertex on the source texture.
        /// </summary>
        public Vector2 Coord
        {
            get
            {
                return new Vector2(_def.X, _def.Y);
            }
            set
            {
                _def.X = value.X._mean;
                _def.Y = value.Y._mean;
            }
        }

        /// <summary>
        /// The vertex-specific alpha multiplier for the source texture.
        /// </summary>
        public Scalar Alpha
        {
            get
            {
                return Math.Abs(_def.W);
            }
            set
            {
                float def = Math.Abs(value._mean);
                if (ShouldOverlay) def = Math.Min(-def, _zeroOverlay);
                _def.W = def;
            }
        }
        
        /// <summary>
        /// If <see cref="true"/>, indicates that contributions from all following texture factors
        /// should be multiplied by the "remaining alpha" for the fragment being processed.
        /// </summary>
        public bool ShouldOverlay
        {
            get
            {
                return _def.W < 0;
            }
            set
            {
                float def = Math.Abs(_def.W);
                if (value) def = Math.Min(-def, _zeroOverlay);
                _def.W = def;
            }
        }

        /// <summary>
        /// The largest allowable negative value in _def.W, used to signal <see cref="ShouldOverlay"/> even
        /// when <see cref="Alpha"/> is "zero".
        /// </summary>
        private const float _zeroOverlay = -Scalar._normalEps;
    }

    /// <summary>
    /// Identifies a type of vertex in a <see cref="Mesh"/>.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Size = (int)Size)]
    public struct MeshVertexType : IEquatable<MeshVertexType>
    {
        public MeshVertexType(uint numTextures)
        {
            NumTextures = numTextures;
        }

        /// <summary>
        /// The size of this structure.
        /// </summary>
        public const uint Size = 4;

        /// <summary>
        /// The number of texture factors per vertex.
        /// </summary>
        public uint NumTextures { get; }

        /// <summary>
        /// The size, in bytes, of a vertex of this type.
        /// </summary>
        public uint VertexSize => 12 + NumTextures * VertexTextureFactor.Size;

        /// <summary>
        /// The <see cref="MeshVertexType"/> corresponding to <see cref="Blend1SolidVertex"/>.
        /// </summary>
        public static MeshVertexType Blend1Solid => new MeshVertexType(1);

        /// <summary>
        /// The <see cref="MeshVertexType"/> corresponding to <see cref="Blend2SolidVertex"/>.
        /// </summary>
        public static MeshVertexType Blend2Solid => new MeshVertexType(2);

        /// <summary>
        /// The <see cref="MeshVertexType"/> corresponding to <see cref="Blend4SolidVertex"/>.
        /// </summary>
        public static MeshVertexType Blend4Solid => new MeshVertexType(4);

        /// <summary>
        /// Compares two <see cref="MeshVertexType"/>s using an arbitrary, but consistent, ordering.
        /// </summary>
        public static int Compare(MeshVertexType a, MeshVertexType b)
        {
            return a.NumTextures.CompareTo(b.NumTextures);
        }

        /// <summary>
        /// The comparer for <see cref="MeshVertexType"/>.
        /// </summary>
        public struct Comparer : System.Collections.Generic.IComparer<MeshVertexType>
        {
            /// <summary>
            /// The only instance of this class.
            /// </summary>
            public static Comparer Instance => new Comparer();

            int System.Collections.Generic.IComparer<MeshVertexType>.Compare(MeshVertexType x, MeshVertexType y)
            {
                return Compare(x, y);
            }
        }

        public static bool operator ==(MeshVertexType a, MeshVertexType b)
        {
            return a.NumTextures == b.NumTextures;
        }

        public static bool operator !=(MeshVertexType a, MeshVertexType b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is MeshVertexType))
                return false;
            return this == (MeshVertexType)obj;
        }
        
        bool IEquatable<MeshVertexType>.Equals(MeshVertexType other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return NumTextures.GetHashCode();
        }
    }

    /// <summary>
    /// Describes a vertex in a <see cref="Mesh"/>.
    /// </summary>
    public struct MeshVertex : IVertex3
    {
        private byte[] _data;
        internal MeshVertex(MeshVertexType type, byte[] data)
        {
            Debug.Assert((uint)data.Length == type.VertexSize);
            Type = type;
            _data = data;
        }

        /// <summary>
        /// The type of this vertex.
        /// </summary>
        public MeshVertexType Type { get; }

        /// <summary>
        /// The position of this vertex.
        /// </summary>
        public Vector3 Position
        {
            get
            {
                uint head = 0;
                return Binary._fastReadAlignedNative<Vector3>(_data, ref head);
            }
        }

        /// <summary>
        /// The texture factors for this vertex.
        /// </summary>
        public unsafe SpanList<VertexTextureFactor> Textures
        {
            get
            {
                uint head = 0;
                head += (uint)sizeof(Vector3);
                return Binary.ReadAlignedNativeList<VertexTextureFactor>(_data, ref head, Type.NumTextures);
            }
        }
    }

    /// <summary>
    /// A specialization of <see cref="MeshVertex"/> which directly references a vertex in memory, elimating the need
    /// to copy vertex data.
    /// </summary>
    public ref struct SpanMeshVertex
    {
        internal ReadOnlySpan<byte> _data;
        internal SpanMeshVertex(MeshVertexType type, ReadOnlySpan<byte> data)
        {
            Debug.Assert((uint)data.Length == type.VertexSize);
            Type = type;
            _data = data;
        }

        /// <summary>
        /// The alignment requirement for a <see cref="SpanMeshVertex"/>'s data.
        /// </summary>
        public const uint Align = 4;

        /// <summary>
        /// The type of this vertex.
        /// </summary>
        public MeshVertexType Type { get; }

        /// <summary>
        /// The position of this vertex.
        /// </summary>
        public Vector3 Position
        {
            get
            {
                ReadOnlySpan<byte> buffer = _data;
                return Binary._fastReadAlignedNative<Vector3>(ref buffer);
            }
        }

        /// <summary>
        /// The texture factors for this vertex.
        /// </summary>
        public unsafe SpanList<VertexTextureFactor> Textures
        {
            get
            {
                ReadOnlySpan<byte> buffer = _data;
                Binary.Skip(ref buffer, (uint)sizeof(Vector3));
                return Binary.ReadAlignedNativeList<VertexTextureFactor>(ref buffer, Type.NumTextures);
            }
        }

        public static implicit operator MeshVertex(SpanMeshVertex source)
        {
            return new MeshVertex(source.Type, source._data.ToArray());
        }
    }

    /// <summary>
    /// A specialization of <see cref="SpanList{T}"/> for mesh vertices, all of the same type.
    /// </summary>
    public ref struct RegularMeshVertexSpanList
    {
        internal ReadOnlySpan<byte> _data;
        internal RegularMeshVertexSpanList(MeshVertexType type, ReadOnlySpan<byte> data)
        {
            Type = type;
            _data = data;
        }

        /// <summary>
        /// The type of vertices in this list.
        /// </summary>
        public MeshVertexType Type { get; }
        
        /// <summary>
        /// The length of this list.
        /// </summary>
        public uint Length => (uint)_data.Length / Type.VertexSize;

        /// <summary>
        /// Gets the vertex at the given index.
        /// </summary>
        public SpanMeshVertex this[uint index]
        {
            get
            {
                uint vertSize = Type.VertexSize;
                return new SpanMeshVertex(Type, _data.Slice((int)(index * vertSize), (int)vertSize));
            }
        }

        /// <summary>
        /// The set of texture indices referenced by any vertex in this list.
        /// </summary>
        public IndexSet TextureDepends
        {
            get
            {
                ReadOnlySpan<byte> buffer = _data;
                IndexSetBuilder textureDepends = IndexSetBuilder.CreateEmpty();
                while (!buffer.IsEmpty)
                {
                    var pos = Binary._fastReadAlignedNative<Vector3>(ref buffer);
                    for (uint i = 0; i < Type.NumTextures; i++)
                    {
                        var factor = Binary._fastReadAlignedNative<VertexTextureFactor>(ref buffer);
                        textureDepends.Include(factor.Index);
                    }
                }
                return textureDepends.Finish();
            }
        }

        /// <summary>
        /// The empty list.
        /// </summary>
        public static RegularMeshVertexSpanList Empty => new RegularMeshVertexSpanList(default, ReadOnlySpan<byte>.Empty);

        /// <summary>
        /// Indicates whether this is the empty list.
        /// </summary>
        public bool IsEmpty => _data.IsEmpty;

        /// <summary> 
        /// Gets an enumerator for this list. This will unnecessarily copy vertex data, so should only
        /// be used for debugging purposes.
        /// </summary>
        public System.Collections.Generic.IEnumerator<MeshVertex> GetEnumerator()
        {
            return ((List<MeshVertex>)this).GetEnumerator();
        }

        public static implicit operator RegularMeshVertexSpanList(List<Blend1SolidVertex> source)
        {
            return new RegularMeshVertexSpanList(
                MeshVertexType.Blend1Solid, 
                MemoryMarshal.AsBytes<Blend1SolidVertex>(source));
        }

        public static implicit operator RegularMeshVertexSpanList(SpanList<Blend1SolidVertex> source)
        {
            return new RegularMeshVertexSpanList(
                MeshVertexType.Blend1Solid,
                MemoryMarshal.AsBytes<Blend1SolidVertex>(source));
        }

        public static implicit operator RegularMeshVertexSpanList(List<Blend2SolidVertex> source)
        {
            return new RegularMeshVertexSpanList(
                MeshVertexType.Blend2Solid,
                MemoryMarshal.AsBytes<Blend2SolidVertex>(source));
        }

        public static implicit operator RegularMeshVertexSpanList(SpanList<Blend2SolidVertex> source)
        {
            return new RegularMeshVertexSpanList(
                MeshVertexType.Blend2Solid,
                MemoryMarshal.AsBytes<Blend2SolidVertex>(source));
        }

        public static implicit operator RegularMeshVertexSpanList(List<Blend4SolidVertex> source)
        {
            return new RegularMeshVertexSpanList(
                MeshVertexType.Blend4Solid,
                MemoryMarshal.AsBytes<Blend4SolidVertex>(source));
        }

        public static implicit operator RegularMeshVertexSpanList(SpanList<Blend4SolidVertex> source)
        {
            return new RegularMeshVertexSpanList(
                MeshVertexType.Blend4Solid,
                MemoryMarshal.AsBytes<Blend4SolidVertex>(source));
        }

        public static explicit operator SpanList<Blend1SolidVertex>(RegularMeshVertexSpanList source)
        {
            if (source.Type.NumTextures > 1)
                throw new InvalidCastException("Too many blend factors in source list");
            if (source.Type.NumTextures < 1)
                return new SpanList<Blend1SolidVertex>(MemoryMarshal.Cast<byte, Blend1SolidVertex>(_expand(source, 1)));
            return new SpanList<Blend1SolidVertex>(MemoryMarshal.Cast<byte, Blend1SolidVertex>(source._data));
        }

        public static explicit operator SpanList<Blend2SolidVertex>(RegularMeshVertexSpanList source)
        {
            if (source.Type.NumTextures > 2)
                throw new InvalidCastException("Too many blend factors in source list");
            if (source.Type.NumTextures < 2)
                return new SpanList<Blend2SolidVertex>(MemoryMarshal.Cast<byte, Blend2SolidVertex>(_expand(source, 2)));
            return new SpanList<Blend2SolidVertex>(
                MemoryMarshal.Cast<byte, Blend2SolidVertex>(source._data));
        }

        public static explicit operator SpanList<Blend4SolidVertex>(RegularMeshVertexSpanList source)
        {
            if (source.Type.NumTextures > 4)
                throw new InvalidCastException("Too many blend factors in source list");
            if (source.Type.NumTextures < 4)
                return new SpanList<Blend4SolidVertex>(MemoryMarshal.Cast<byte, Blend4SolidVertex>(_expand(source, 4)));
            return new SpanList<Blend4SolidVertex>(
                MemoryMarshal.Cast<byte, Blend4SolidVertex>(source._data));
        }

        /// <summary>
        /// Expands the vertices in a <see cref="RegularMeshVertexSpanList"/> to have the given number of blend factors,
        /// adding <see cref="VertexTextureFactor.Empty"/> where needed.
        /// </summary>
        private static byte[] _expand(RegularMeshVertexSpanList list, uint numBlendFactors)
        {
            Debug.Assert(list.Type.NumTextures < numBlendFactors);
            byte[] nData = new byte[new MeshVertexType(numBlendFactors).VertexSize * list.Length];
            uint nHead = 0;
            ReadOnlySpan<byte> buffer = list._data;
            VertexTextureFactor emptyFactor = VertexTextureFactor.Empty;
            while (!buffer.IsEmpty)
            {
                Vector3 pos = Binary._fastReadAlignedNative<Vector3>(ref buffer);
                Binary._fastWriteAlignedNative(nData, ref nHead, in pos);
                for (uint j = 0; j < list.Type.NumTextures; j++)
                {
                    VertexTextureFactor factor = Binary._fastReadAlignedNative<VertexTextureFactor>(ref buffer);
                    Binary._fastWriteAlignedNative(nData, ref nHead, in factor);
                }
                for (uint j = list.Type.NumTextures; j < numBlendFactors; j++)
                    Binary._fastWriteAlignedNative(nData, ref nHead, in emptyFactor);
            }
            return nData;
        }

        public static implicit operator List<MeshVertex>(RegularMeshVertexSpanList source)
        {
            MeshVertex[] items = new MeshVertex[source.Length];
            ReadOnlySpan<byte> buffer = source._data;
            for (uint i = 0; i < (uint)items.Length; i++)
                items[i] = VertexBinary._fastReadAlignedNativeMeshVertex(ref buffer, source.Type);
            return new List<MeshVertex>(items);
        }
    }
    
    /// <summary>
    /// Contains helper functions for reading and writing vertex data in a binary format.
    /// </summary>
    public static class VertexBinary
    {
        /// <summary>
        /// Reads a <see cref="MeshVertex"/> of the given type from a buffer using native encoding, assuming the read
        /// head is aligned according to <see cref="SpanMeshVertex.Align"/>.
        /// </summary>
        public static SpanMeshVertex ReadAlignedNativeMeshVertex(ref ReadOnlySpan<byte> buffer, MeshVertexType type)
        {
            return new SpanMeshVertex(type, Binary.ReadRaw(ref buffer, type.VertexSize));
        }

        /// <summary>
        /// Reads a list of <see cref="MeshVertex"/>s of the given type from a buffer using native encoding, assuming
        /// the read head is aligned according to <see cref="SpanMeshVertex.Align"/>.
        /// </summary>
        public static RegularMeshVertexSpanList ReadAlignedNativeMeshVertexList(
            ref ReadOnlySpan<byte> buffer,
            MeshVertexType type, uint len)
        {
            return new RegularMeshVertexSpanList(type, Binary.ReadRaw(ref buffer, type.VertexSize * len));
        }

        /// <summary>
        /// Reads a <see cref="MeshVertex"/> of the given type from a buffer using native encoding, assuming the read
        /// head is aligned according to <see cref="SpanMeshVertex.Align"/>.
        /// </summary>
        public static SpanMeshVertex ReadAlignedNativeMeshVertex(byte[] data, ref uint head, MeshVertexType type)
        {
            return new SpanMeshVertex(type, Binary.ReadRaw(data, ref head, type.VertexSize));
        }

        /// <summary>
        /// Reads a list of <see cref="MeshVertex"/> of the given type from a buffer using native encoding, assuming
        /// the read head is aligned according to <see cref="SpanMeshVertex.Align"/>.
        /// </summary>
        public static RegularMeshVertexSpanList ReadAlignedNativeMeshVertexList(
            byte[] data, ref uint head,
            MeshVertexType type, uint len)
        {
            return new RegularMeshVertexSpanList(type, Binary.ReadRaw(data, ref head, type.VertexSize * len));
        }

        /// <summary>
        /// Reads a <see cref="MeshVertex"/> of the given type from a buffer using native encoding, assuming the read
        /// head is aligned according to <see cref="SpanMeshVertex.Align"/>. Violation of any assumptions will result
        /// in undefined behavior.
        /// </summary>
        internal static SpanMeshVertex _fastReadAlignedNativeMeshVertex(
            ref ReadOnlySpan<byte> buffer, MeshVertexType type)
        {
            return new SpanMeshVertex(type, Binary.ReadRaw(ref buffer, type.VertexSize));
        }

        /// <summary>
        /// Reads a <see cref="MeshVertex"/> of the given type from a buffer using native encoding, assuming the read
        /// head is aligned according to <see cref="SpanMeshVertex.Align"/>. Violation of any assumptions will result
        /// in undefined behavior.
        /// </summary>
        internal static SpanMeshVertex _fastReadAlignedNativeMeshVertex(
            byte[] data, ref uint head, MeshVertexType type)
        {
            return new SpanMeshVertex(type, Binary.ReadRaw(data, ref head, type.VertexSize));
        }

        /// <summary>
        /// Writes a <see cref="MeshVertex"/> to a buffer using native encoding, assuming the write head is aligned
        /// according to <see cref="SpanMeshVertex.Align"/>. Violation of any assumptions will result in undefined
        /// behavior.
        /// </summary>
        internal static void _fastWriteAlignedNativeMeshVertex(
            byte[] data, ref uint head, SpanMeshVertex vert)
        {
            Binary.WriteRaw(data, ref head, vert._data);
        }
    }
}