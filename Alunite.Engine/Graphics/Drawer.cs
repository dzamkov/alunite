﻿using System;
using System.Runtime.InteropServices;

using Alunite.Data;
using Alunite.Data.Geometry;
using Alunite.Reactive;

namespace Alunite.Graphics
{
    /// <summary>
    /// An interface for drawing <see cref="Paint"/> figures onto a two-dimensional surface.
    /// </summary>
    public interface IDrawer2
    {
        /// <summary>
        /// Adds a textured mesh to the drawing surface.
        /// </summary>
        void Draw(PaintTexture2 texture, SpanList<TextureVertex2> verts, SpanList<uint> indices);

        /// <summary>
        /// Draws a projection of a three-dimensional figure, specified using the returned
        /// <see cref="IDrawer3"/>. Once all three-dimensional drawing operations are complete, the
        /// returned <see cref="IDisposable"/> should be disposed. Drawing to the
        /// <see cref="IDrawer2"/> may not resume until this is done.
        /// </summary>
        IDisposable Project(Viewport viewport, out IDrawer3 drawer);

        /// <summary>
        /// Draws a projection of a rendered three-dimensional space, specified using the returned
        /// <see cref="IRenderer"/>. Once all renderable effects have been specified, the returned
        /// <see cref="IDisposable"/> should be disposed. Drawing to the <see cref="IDrawer2"/> may not
        /// resume until this is done.
        /// </summary>
        IDisposable ProjectRender(Skybox skybox, Viewport viewport, Tone tone, out IRenderer renderer);
    }

    /// <summary>
    /// An extension of <see cref="IDrawer2"/> which accepts time-varying drawing operations.
    /// </summary>
    public interface IDynamicDrawer2 : IDrawer2
    {
        /// <summary>
        /// Draws a projection of a three-dimensional figure, specified using the returned
        /// <see cref="IDrawer3"/>. Once all three-dimensional drawing operations are complete, the
        /// returned <see cref="IDisposable"/> should be disposed. Drawing to the
        /// <see cref="IDrawer2"/> may not resume until this is done.
        /// </summary>
        IDisposable Project(Dynamic<Viewport> viewport, out IDrawer3 drawer);

        /// <summary>
        /// Draws a projection of a rendered three-dimensional space, specified using the returned
        /// <see cref="IRenderer"/>. Once all renderable effects have been specified, the returned
        /// <see cref="IDisposable"/> should be disposed. Drawing to the <see cref="IDrawer2"/> may not
        /// resume until this is done.
        /// </summary>
        IDisposable ProjectRender(Skybox skybox, Dynamic<Viewport> viewport, Dynamic<Tone> tone, out IRenderer renderer);
    }

    /// <summary>
    /// An interface for drawing <see cref="Paint"/> figures in a three-dimensional volume.
    /// </summary>
    public interface IDrawer3
    {
        /// <summary>
        /// Adds a textured mesh to the drawing volume.
        /// </summary>
        void Draw(PaintTexture2 texture, SpanList<TextureVertex3> verts, SpanList<uint> indices);
    }

    /// <summary>
    /// An extension of <see cref="IDrawer2"/> which accepts time-varying drawing operations.
    /// </summary>
    public interface IDynamicDrawer3 : IDrawer3
    {

    }

    /// <summary>
    /// A vertex in a textured mesh used for draw operations in a <see cref="IDrawer2"/>.
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 16)]
    public struct TextureVertex2 : IVertex2
    {
        public TextureVertex2(Vector2 pos, Vector2 src)
        {
            Position = pos;
            Source = src;
        }

        /// <summary>
        /// The position of this vertex on the render target.
        /// </summary>
        [FieldOffset(0)] public Vector2 Position;

        /// <summary>
        /// The position of this vertex on the source texture.
        /// </summary>
        [FieldOffset(8)] public Vector2 Source;

        Vector2 IVertex2.Position => Position;
    }

    /// <summary>
    /// A vertex in a textured mesh used for draw operations in a <see cref="IDrawer3"/>.
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 20)]
    public struct TextureVertex3 : IVertex3
    {
        public TextureVertex3(Vector3 pos, Vector2 src)
        {
            Position = pos;
            Source = src;
        }

        /// <summary>
        /// The position of this vertex on the render target.
        /// </summary>
        [FieldOffset(0)] public Vector3 Position;

        /// <summary>
        /// The position of this vertex on the source texture.
        /// </summary>
        [FieldOffset(12)] public Vector2 Source;

        Vector3 IVertex3.Position => Position;
    }

    /// <summary>
    /// Describes a transformation from some three-dimensional world space to a two-dimensional rectangle on
    /// an image.
    /// </summary>
    public struct Viewport
    {
        public Viewport(Motion3 worldToView, Projection viewToDevice, Box2i target)
        {
            WorldToView = worldToView;
            ViewToDevice = viewToDevice;
            Target = target;
        }

        /// <summary>
        /// The transformation from world space to view space.
        /// </summary>
        public Motion3 WorldToView;

        /// <summary>
        /// The projection from view space to device coordinates.
        /// </summary>
        public Projection ViewToDevice;
        
        /// <summary>
        /// The <see cref="Box2i"/> on the render target that the view is projected onto.
        /// </summary>
        public Box2i Target;
    }
}