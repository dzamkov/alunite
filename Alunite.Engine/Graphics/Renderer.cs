﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

using Alunite.Data;
using Alunite.Data.Geometry;

namespace Alunite.Graphics
{
    /// <summary>
    /// An interface for defining the physically-based visual contents of a three-dimensional space.
    /// This is similar to <see cref="IDrawer3"/>, but instead of directly drawing <see cref="Paint"/>s,
    /// this interface specifies materials, surfaces and visual effects which are subject to lighting.
    /// </summary>
    public interface IRenderer
    {
        /// <summary>
        /// Adds a mesh to the scene.
        /// </summary>
        void RenderMesh(
            Permutation<MaterialTexture2> textures,
            RegularMeshVertexSpanList verts,
            SpanList<uint> indices);
    }

    /// <summary>
    /// An extension of <see cref="IRenderer"/> which accepts time-varying effects.
    /// </summary>
    public interface IDynamicRenderer : IRenderer
    {

    }

    /// <summary>
    /// Contains helper functions related to <see cref="IRenderer"/>.
    /// </summary>
    public static class Renderer
    {
        /// <summary>
        /// Adds a solid mesh to the scene.
        /// </summary>
        public static void RenderSolid(this IRenderer renderer, 
            MaterialTexture2 texture, 
            SpanList<SolidVertex> verts,
            SpanList<uint> indices)
        {
            // Rewrite mesh as an indexed material, using index 0 for the sole material
            Span<Blend1SolidVertex> nVerts = stackalloc Blend1SolidVertex[(int)verts.Length];
            for (uint i = 0; i < verts.Length; i++)
            {
                SolidVertex vert = verts[i];
                nVerts[(int)i] = new Blend1SolidVertex
                {
                    Position = vert.Position,
                    Texture = VertexTextureFactor.Build(0, vert.MaterialCoord, vert.Tbn, vert.MaterialAlpha, false)
                };
            }

            // Render it
            renderer.RenderMesh(Permutation.Of(texture), List.Of(nVerts), indices);
        }

        /// <summary>
        /// Adds a <see cref="Model"/> to the scene.
        /// </summary>
        public static void Render(this IRenderer renderer, Model model)
        {
            model.Render(renderer);
        }
    }

    /// <summary>
    /// A vertex in a solid mesh used for render operations in an <see cref="IRenderer"/>.
    /// </summary>
    public struct SolidVertex : IVertex3
    {
        public SolidVertex(Vector3 pos, Vector2 matCoord, Scalar matAlpha, Rotation3 tbn)
        {
            Position = pos;
            MaterialCoord = matCoord;
            MaterialAlpha = matAlpha;
            Tbn = tbn;
        }

        /// <summary>
        /// The position of this vertex in three-dimensional space.
        /// </summary>
        public Vector3 Position;

        /// <summary>
        /// The position of this vertex on the source material.
        /// </summary>
        public Vector2 MaterialCoord;

        /// <summary>
        /// The vertex-specific alpha multiplier for the source material.
        /// </summary>
        public Scalar MaterialAlpha;

        /// <summary>
        /// The transformation from the tangent space of the source material to object space.
        /// This implicitly defines the normal of the vertex.
        /// </summary>
        // TODO: Replace with Roflection to account for handedness
        public Rotation3 Tbn;

        Vector3 IVertex3.Position => Position;
    }
}