﻿using System;

using Alunite.Data;

namespace Alunite.Diagnostics
{
    /// <summary>
    /// A streaming interface for producing a human-readable (though technical) description of a value.
    /// </summary>
    public interface IPrinter : IListWriter<char>
    {
        /// <summary>
        /// Appends a description or reference to the given value to the printed output.
        /// </summary>
        /// <param name="prec">The minimum precedence required of the printed value. If this would normally
        /// exceed the precedence of the value's description, the value should be wrapped in parentheses.</param>
        void Print<T>(Precedence prec, T value);
    }

    /// <summary>
    /// An object whose technical description can be written to an <see cref="IPrinter"/>.
    /// </summary>
    public interface IPrintable
    {
        /// <summary>
        /// Appends a description of this object to the given <see cref="IPrinter"/>.
        /// </summary>
        /// <param name="prec">The minimum precedence required of the printed value. If this would normally
        /// exceed the precedence of the value's description, the value should be wrapped in parentheses.</param>
        void Print(IPrinter printer, Precedence prec);
    }

    /// <summary>
    /// Identifies the syntactical precedence of an expression. This determines what operators can be applied adjacent
    /// to the expression without changing its interpretation.
    /// </summary>
    /// <remarks>See https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/operators/ </remarks>
    public enum Precedence
    {
        Primary,
        Unary,
        Multiplicative,
        Additive,
        Shift,
        Relational,
        Equality,
        BooleanAnd,
        BooleanXor,
        BooleanOr,
        ConditionalAnd,
        ConditionalOr,
        Assignment,
        Free
    }

    /// <summary>
    /// Contains functions related to <see cref="IPrinter"/> and <see cref="IPrintable"/>.
    /// </summary>
    public static class Printer
    {
        /// <summary>
        /// Prints the expanded description of the given value to the given <see cref="IPrinter"/>.
        /// </summary>
        /// <param name="prec">The minimum precedence required of the printed value. If this would normally
        /// exceed the precedence of the value's description, the value should be wrapped in parentheses.</param>
        public static void PrintExpanded<T>(this IPrinter printer, Precedence prec, T value)
        {
            if (value is IPrintable printableValue)
            {
                printableValue.Print(printer, prec);
            }
            else if (value is string strValue)
            {
                printer.WriteItem('\"');
                printer.WriteEscaped(strValue);
                printer.WriteItem('\"');
            }
            else if (value is char chValue)
            {
                printer.WriteItem('\'');
                printer.WriteEscapedItem(chValue);
                printer.WriteItem('\'');
            }
            else
            {
                printer.Write(value.ToString());
            }
        }

        /// <summary>
        /// Gets a maximally-expanded string describing a printable value.
        /// </summary>
        public static string ToExpandedString(this IPrintable printable)
        {
            StringBuilder str = StringBuilder.CreateEmpty();
            ExpandedStringPrinter printer = new ExpandedStringPrinter(str);
            printable.Print(printer, Precedence.Free);
            return str.Finish();
        }

        /// <summary>
        /// Gets a reasonably-sized string describing a printable value. This is intended for situations where there
        /// is an alternative method of inspecting the value.
        /// </summary>
        public static string ToShortString(this IPrintable printable)
        {
            // TODO
            return printable.ToExpandedString();
        }
    }

    /// <summary>
    /// An <see cref="IPrinter"/> which outputs directly to a <see cref="StringBuilder"/> and
    /// fully expands all sub-values.
    /// </summary>
    public struct ExpandedStringPrinter : IPrinter
    {
        public ExpandedStringPrinter(StringBuilder target)
        {
            Target = target;
        }

        /// <summary>
        /// The underlying <see cref="StringBuilder"/> for this <see cref="ExpandedStringPrinter"/>.
        /// </summary>
        public StringBuilder Target { get; }

        void IListWriter<char>.Write(ReadOnlySpan<char> chs)
        {
            Target.Write(chs);
        }

        void IPrinter.Print<T>(Precedence prec, T value)
        {
            this.PrintExpanded(prec, value);
        }
    }
}
