﻿using System;
using System.Diagnostics;
using System.Json;
using System.Runtime.CompilerServices;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Reflection.Emit;

using Alunite.Data;

#pragma warning disable CS0649

namespace Alunite
{
    /// <summary>
    /// A collection of useful functions and values that may be imported statically.
    /// </summary>
    public static class Static
    {
        /// <summary>
        /// Closes the given builder and gets the object resulting from it.
        /// </summary>
        public static T Finish<T>(IBuilder<T> builder)
        {
            return builder.Finish();
        }
    }

    /// <summary>
    /// A collection of utility functions.
    /// </summary>
    public static class Util
    {
        /// <summary>
        /// Verifies that a condition is <see cref="true"/> in debug builds. This is similar to
        /// <see cref="Debug.Assert"/>, but will not conditionally compile the condition parameter,
        /// ensuring that the side effects of its computation will always be visible.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Assert(bool cond)
        {
            Debug.Assert(cond);
        }

        /// <summary>
        /// Swaps two values.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Swap<T>(ref T a, ref T b)
        {
            T temp = a;
            a = b;
            b = temp;
        }

        /// <summary>
        /// Applies a permutation, in-place, to a span of items.
        /// </summary>
        public static void Permute<T>(this Span<T> items, SpanList<uint> perm)
        {
            // TODO: Can make guaranteed O(n) if we use an extra bit per item
            for (uint i = 0; i < (uint)items.Length; i++)
            {
                uint target = perm[i];
                while (target < i)
                    target = perm[target];
                Swap(ref items[(int)i], ref items[(int)target]);
            }
        }

        /// <summary>
        /// Computes the greatest common divisor between two numbers.
        /// </summary>
        public static uint Gcd(uint a, uint b)
        {
            uint lo, hi;
            if (a > b)
            {
                lo = b;
                hi = a;
            }
            else
            {
                lo = a;
                hi = b;
            }
            while (lo > 0)
            {
                uint temp = lo;
                lo = hi % lo;
                hi = temp;
            }
            return hi;
        }

        /// <summary>
        /// Indicates whether writes and reads of the given type are atomic.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool IsReadWriteAtomic<T>()
        {
            var type = typeof(T);
            if (type.IsClass)
                return true;
            switch (Type.GetTypeCode(type))
            {
                case TypeCode.Boolean:
                case TypeCode.Byte:
                case TypeCode.Char:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.SByte:
                case TypeCode.Single:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                    return true;
                case TypeCode.Int64:
                case TypeCode.Double:
                case TypeCode.UInt64:
                    return IntPtr.Size == 8;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Determines whether the given type can satisfy the "unmanaged" constraint.
        /// </summary>
        public static bool IsUnmanaged(Type type)
        {
            if (type.IsPrimitive || type.IsPointer || type.IsEnum)
                return true;
            if (type.IsGenericType || !type.IsValueType)
                return false;
            foreach (var field in type.GetFields(
                System.Reflection.BindingFlags.NonPublic |
                System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance))
            {
                if (!IsUnmanaged(field.FieldType))
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Checks whether the given key exists in the given object, and if so, returns the corresponding
        /// value converted to a <see cref="JsonObject"/>.
        /// </summary>
        public static bool TryGetValue(this JsonObject obj, string key, out JsonObject value)
        {
            if (obj.TryGetValue(key, out JsonValue source))
            {
                value = (JsonObject)source;
                return true;
            }
            value = default;
            return false;
        }

        /// <summary>
        /// Checks whether the given key exists in the given object, and if so, returns the corresponding
        /// value converted to a <see cref="JsonArray"/>.
        /// </summary>
        public static bool TryGetValue(this JsonObject obj, string key, out JsonArray value)
        {
            if (obj.TryGetValue(key, out JsonValue source))
            {
                value = (JsonArray)source;
                return true;
            }
            value = default;
            return false;
        }

        /// <summary>
        /// Checks whether the given key exists in the given object, and if so, returns the corresponding
        /// value converted to a <see cref="string"/>.
        /// </summary>
        public static bool TryGetValue(this JsonObject obj, string key, out string value)
        {
            if (obj.TryGetValue(key, out JsonValue source))
            {
                value = source;
                return true;
            }
            value = default;
            return false;
        }

        /// <summary>
        /// Checks whether the given key exists in the given object, and if so, returns the corresponding
        /// value converted to a <see cref="int"/>.
        /// </summary>
        public static bool TryGetValue(this JsonObject obj, string key, out int value)
        {
            if (obj.TryGetValue(key, out JsonValue source))
            {
                value = source;
                return true;
            }
            value = default;
            return false;
        }

        /// <summary>
        /// Checks whether the given key exists in the given object, and if so, returns the corresponding
        /// value converted to a <see cref="uint"/>.
        /// </summary>
        public static bool TryGetValue(this JsonObject obj, string key, out uint value)
        {
            if (obj.TryGetValue(key, out JsonValue source))
            {
                value = source;
                return true;
            }
            value = default;
            return false;
        }

        /// <summary>
        /// Uses this array to pick corresponding items from a given source array.
        /// </summary>
        public static T[] Pick<T>(this uint[] pattern, T[] args)
        {
            T[] res = new T[pattern.Length];
            for (uint i = 0; i < pattern.Length; i++)
                res[i] = args[pattern[i]];
            return res;
        }

        /// <summary>
        /// Gets the preferred alignment, in bytes, of the given type.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static unsafe uint AlignOf<T>()
            where T : unmanaged
        {
            // TODO: Replace with sizeof in C# 8.0
            return (uint)(Unsafe.SizeOf<_AlignTester<T>>() - sizeof(T));
        }

        /// <summary>
        /// Helper for implementing <see cref="AlignOf{T}"/>.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        private struct _AlignTester<T>
            where T : unmanaged
        {
            public byte A;
            public T B;
        }

        /// <summary>
        /// Sets one of the given objects references to the other, preferring to keep the older object.
        /// </summary>
        /// <remarks>This is intended to be used with immutable objects that have structural equality semantics. If
        /// two objects are created that logically represent the same value, this method can be used to eliminate
        /// one of them.</remarks>
        public static void Coalesce<T>(ref T a, ref T b)
            where T : class
        {
            int aGen = GC.GetGeneration(a);
            int bGen = GC.GetGeneration(b);
            if (aGen < bGen)
            {
                a = b;
            }
            else if (bGen < aGen)
            {
                b = a;
            }
            else
            {
                // TODO: Would be better if we could use the actual pointer address rather than the hash code, but
                // it looks like we'll need CIL for that. Revisit this once the project setup is mature enough to
                // allow for CIL code.
                int aHash = RuntimeHelpers.GetHashCode(a);
                int bHash = RuntimeHelpers.GetHashCode(b);
                if (aHash < bHash)
                {
                    b = a;
                }
                else
                {
                    a = b;
                }
            }
        }
    }

    /// <summary>
    /// Contains helper functions for casting between types.
    /// </summary>
    public static class CastHelper
    {
        /// <summary>
        /// Reinterprets a <see cref="float"/> as a <see cref="uint"/>.
        /// </summary>
        public static unsafe uint AsUInt32(this float value)
        {
            return *(uint*)&value;
        }

        /// <summary>
        /// Reinterprets a <see cref="uint"/> as a <see cref="float"/>.
        /// </summary>
        public static unsafe float AsSingle(this uint value)
        {
            return *(float*)&value;
        }

        /// <summary>
        /// Reinterprets a <see cref="double"/> as a <see cref="ulong"/>.
        /// </summary>
        public static unsafe ulong AsUInt64(this double value)
        {
            return *(ulong*)&value;
        }

        /// <summary>
        /// Reinterprets a <see cref="ulong"/> as a <see cref="double"/>.
        /// </summary>
        public static unsafe double AsDouble(this ulong value)
        {
            return *(double*)&value;
        }
    }

    /// <summary>
    /// Contains helper functions for converting values into strings.
    /// </summary>
    public static class StringHelper
    {
        /// <summary>
        /// Converts a numeric value into a string using the invariant culture.
        /// </summary>
        public static string ToStringInvariant(this int value)
        {
            return value.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts a numeric value into a string using the invariant culture.
        /// </summary>
        public static string ToStringInvariant(this uint value)
        {
            return value.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts a numeric value into a string using the invariant culture.
        /// </summary>
        public static string ToStringInvariant(this float value)
        {
            return value.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts a numeric value into a string of unicode subscript characters.
        /// </summary>
        public static string ToSubscriptStringInvariant(this uint value)
        {
            string str = value.ToStringInvariant();
            char[] chars = str.ToCharArray();
            for (int i = 0; i < chars.Length; i++)
                chars[i] = (char)((chars[i] - '0') + '₀');
            return new string(chars);
        }

        /// <summary>
        /// Adds quotes around the given string and escapes characters as necessary.
        /// </summary>
        public static string Quote(this string value)
        {
            StringBuilder str = StringBuilder.CreateEmpty();
            str.WriteItem('\"');
            str.WriteEscaped(value);
            str.WriteItem('\"');
            return str.Finish();
        }

        /// <summary>
        /// Gets the string for an embedded resource in the current assembly.
        /// </summary>
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static string Resource(string name)
        {
            using (System.IO.Stream stream = Assembly
                .GetCallingAssembly()
                .GetManifestResourceStream(name))
            {
                var reader = new System.IO.StreamReader(stream);
                return reader.ReadToEnd();
            }
        }
    }

    /// <summary>
    /// Contains helper functions related to equality.
    /// </summary>
    public static class EqualityHelper
    {
        /// <summary>
        /// Gets the <see cref="IEqualityComparer{T}"/> which tests for logical equivalence for the given type.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static System.Collections.Generic.IEqualityComparer<T> Comparer<T>()
        {
            return System.Collections.Generic.EqualityComparer<T>.Default;
        }

        /// <summary>
        /// Determines whether the given values are logically equivalent, assuming that <see cref="null"/> is only
        /// equivalent to <see cref="null"/> and that objects of different types are never equivalent.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static new bool Equals(object x, object y)
        {
            if (x is null)
                return y is null;
            return x.Equals(y);
        }

        /// <summary>
        /// Determines whether the given values are logically equivalent.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool Equals<T>(T x, T y)
        {
            // TODO: Fallback to x.Equals(y) for reference types
            return System.Collections.Generic.EqualityComparer<T>.Default.Equals(x, y);
        }

        /// <summary>
        /// Gets the hash code for the given value, corresponding to the equality relationship defined
        /// by <see cref="Equals(object, object)"/>.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int GetHashCode(object x)
        {
            if (x is null)
                return 0;
            return x.GetHashCode();
        }

        /// <summary>
        /// Gets the hash code for the given value, corresponding to the equality relationship defined
        /// by <see cref="Equals{T}(T, T)"/>.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int GetHashCode<T>(T x)
        {
            return System.Collections.Generic.EqualityComparer<T>.Default.GetHashCode(x);
        }
    }

    /// <summary>
    /// Contains helper functions related to hash codes.
    /// </summary>
    public static class HashCodeHelper
    {
        /// <summary>
        /// Non-commutatively combines hash values 
        /// </summary>
        public static int Combine(int a, int b)
        {
            int hash = 0x3267f52f - a;
            hash ^= hash << 16;
            hash += hash >> 11;
            hash += hash << 25;
            hash ^= hash >> 17;
            hash += hash << 1;
            return unchecked(hash ^ a ^ b);
        }

        /// <summary>
        /// Non-commutatively combines hash values 
        /// </summary>
        public static int Combine(int a, int b, int c)
        {
            return Combine(Combine(a, b), c);
        }

        /// <summary>
        /// Non-commutatively combines hash values 
        /// </summary>
        public static int Combine(int a, int b, int c, int d)
        {
            return Combine(Combine(a, b), Combine(c, d));
        }

        /// <summary>
        /// Non-commutatively combines hash values 
        /// </summary>
        public static int Combine(int a, int b, int c, int d, int e)
        {
            return Combine(Combine(a, b, c), Combine(d, e));
        }

        /// <summary>
        /// Non-commutatively combines hash values 
        /// </summary>
        public static int Combine(int a, int b, int c, int d, int e, int f)
        {
            return Combine(Combine(a, b, c), Combine(d, e, f));
        }

        /// <summary>
        /// Non-commutatively combines hash values 
        /// </summary>
        public static int Combine(int a, int b, int c, int d, int e, int f, int g, int h)
        {
            return Combine(Combine(a, b, c, d), Combine(e, f, g, h));
        }
    }

    /// <summary>
    /// Contains helper functions related to enums.
    /// </summary>
    public static class EnumHelper
    {
        /// <summary>
        /// Converts an enum value to its corresponding int value.
        /// </summary>
        public static unsafe int ToInt32<T>(T value)
            where T : unmanaged, Enum
        {
            if (sizeof(T) == 1)
                return *(byte*)&value;
            else if (sizeof(T) == 2)
                return *(short*)&value;
            else if (sizeof(T) == 4)
                return *(int*)&value;
            else
                throw new NotSupportedException();
        }

        /// <summary>
        /// Converts an int value to its corresponding enum value.
        /// </summary>
        public static unsafe T FromInt32<T>(int value)
            where T : unmanaged, Enum
        {
            T res;
            if (sizeof(T) == 1)
                *(byte*)&res = (byte)value;
            else if (sizeof(T) == 2)
                *(short*)&res = (short)value;
            else if (sizeof(T) == 4)
                *(int*)&res = value;
            else
                throw new NotSupportedException();
            return res;
        }

        /// <summary>
        /// Gets one more than the largest valid value of the given enum.
        /// </summary>
        public static int Count<T>()
            where T : unmanaged, Enum
        {
            return _Count<T>.Value;
        }

        /// <summary>
        /// Helper used to implement <see cref="Count{T}"/>.
        /// </summary>
        private static class _Count<T>
            where T : unmanaged, Enum
        {
            public static readonly int Value;
            static _Count()
            {
                var values = Enum.GetValues(typeof(T));
                Value = ToInt32(((T[])values)[values.Length - 1]) + 1;
            }
        }
    }

    /// <summary>
    /// Contains helper functions related to <see cref="IDisposable"/>.
    /// </summary>
    public static class DisposableHelper
    {
        /// <summary>
        /// Constructs an <see cref="IDisposable"/> which groups together the given <see cref="IDisposable"/>s.
        /// </summary>
        public static IDisposable Group(IDisposable a, IDisposable b)
        {
            if (a is null)
                return b;
            if (b is null)
                return a;
            throw new NotImplementedException();
        }

        /// <summary>
        /// Constructs an <see cref="IDisposable"/> which groups together the given <see cref="IDisposable"/>s.
        /// </summary>
        public static IDisposable Group(params IDisposable[] disposables)
        {
            // TODO: Could be more performant
            IDisposable res = null;
            foreach (IDisposable source in disposables)
                res = Group(res, source);
            return res;
        }
    }

    /// <summary>
    /// Contains helper functions related to reflection.
    /// </summary>
    public static class ReflectionHelper
    {
        /// <summary>
        /// If necessary, modifies <paramref name="name"/> so it does not conflict with any existing
        /// types in the module.
        /// </summary>
        public static void DeconflictTypeName(this ModuleBuilder builder, ref string name)
        {
            // TODO
        }

        /// <summary>
        /// Gets the implementation of a virtual method on the given type.
        /// </summary>
        public static MethodInfo GetImplementation(this MethodInfo method, Type type)
        {
            if (method.DeclaringType.IsInterface)
            {
                var map = type.GetInterfaceMap(method.DeclaringType);
                for (int i = 0; i < map.InterfaceMethods.Length; i++)
                {
                    if (map.InterfaceMethods[i] == method)
                        return map.TargetMethods[i];
                }

                // Should never happen, the type must implement the method
                throw new Exception();
            }
            else
            {
                var flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
                foreach (var candidate in type.GetMethods(flags))
                {
                    if (candidate.GetBaseDefinition() == method)
                        return candidate;
                }
                return GetImplementation(method, type.BaseType);
            }
        }
        
        /// <summary>
        /// Determines whether the given type is a plain old data struct, and if so, gets all the fields of the struct
        /// in canonical order.
        /// </summary>
        public static bool IsPlain(this Type type, out List<FieldInfo> fields)
        {
            if (!type.IsPrimitive && (type.IsValueType || type.GetConstructor(Array.Empty<Type>()) != null))
            {
                FieldInfo[] fieldArr = _plainCache.GetOrAdd(type, _plainFactory);
                if (fieldArr != null)
                {
                    fields = new List<FieldInfo>(fieldArr);
                    return true;
                }
            }
            fields = default;
            return false;
        }

        /// <summary>
        /// A cache of fields returned by <see cref="IsPlain"/>.
        /// </summary>
        private static readonly LookupDictionary<Type, FieldInfo[]> _plainCache = new LookupDictionary<Type, FieldInfo[]>();

        /// <summary>
        /// The factory function used to construct new entries for <see cref="_plainCache"/>.
        /// </summary>
        private static readonly Func<Type, FieldInfo[]> _plainFactory = type =>
        {
            // TODO: Verify no overlapping fields
            var all = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
            FieldInfo[] fields = type.GetFields(all);
            for (int i = 0; i < fields.Length; i++)
                if (!fields[i].IsPublic)
                    return null;
            return fields;
        };
    }
    
    /// <summary>
    /// Tests a predicate against the input, and produces an output conditional on the result.
    /// </summary>
    public delegate bool FuncPredicate<T, TN>(T input, out TN output);

    /// <summary>
    /// An interface for constructing an object of type <typeparamref name="T"/> through incremental
    /// modifications to a hidden working copy. Once all desired modifications have been made,
    /// <see cref="Finish"/> can be called to finalize and return the object. This must eventually be
    /// called for any builder. Builders are typically not thread-safe.
    /// </summary>
    public interface IBuilder<out T>
    {
        /// <summary>
        /// Closes the builder and gets the object resulting from it. After this is called, there should
        /// be no further calls to the builder.
        /// </summary>
        T Finish();
    }

    /// <summary>
    /// Represents an object or value that may not be defined yet, but will be at some point in the future. This
    /// is typically used with <see cref="IBuilder{T}"/>s to allow access to resulting object's properties
    /// prior to its finalization. This is not thread-safe.
    /// </summary>
    public struct Delay<T>
    {
        internal T _value;
        internal Store _source;
        public Delay(T value)
        {
            _value = value;
            _source = _useValue;
        }

        public Delay(Func<T> get)
        {
            _value = default;
            _source = new Store(get);
        }

        public Delay(Store cell)
        {
            _value = default;
            _source = cell;
        }
        
        /// <summary>
        /// A sentinel value for <see cref="Source"/> used to indicate that the actual value is available in <see cref="_value"/>.
        /// </summary>
        internal static readonly Store _useValue = new Store();

        /// <summary>
        /// Gets the concrete value for this delay-defined value, assuming it has been defined by this point.
        /// </summary>
        public T Value
        {
            get
            {
                if (_source == _useValue)
                    return _value;
                _value = _source.Value;
                _source = _useValue;
                return _value;
            }
        }
        
        /// <summary>
        /// Provides a storage location for the value of a <see cref="Delay{T}"/>.
        /// </summary>
        public sealed class Store
        {
            private T _value;
            private Func<T> _def;
            internal Store() { }
            public Store(Func<T> get)
            {
                _value = default(T);
                _def = get;
            }

            /// <summary>
            /// Creates a new store with an initially-undefined value.
            /// </summary>
            public static Store Create()
            {
                return new Store(_undefined);
            }

            /// <summary>
            /// Indicates whether this store is defined to have a particular value.
            /// </summary>
            public bool IsDefined
            {
                get
                {
                    return _def != _undefined;
                }
            }

            /// <summary>
            /// Gets or sets the value of this store.
            /// </summary>
            public T Value
            {
                get
                {
                    if (_def == null)
                        return _value;
                    _value = _def();
                    _def = null;
                    return _value;
                }
                set
                {
                    if (_def != _undefined)
                        throw new PropertyRedefinedException();
                    _value = value;
                    _def = null;
                }
            }

            /// <summary>
            /// Sets the defining function for this store, assuming it is not yet defined.
            /// </summary>
            public Func<T> Definition
            {
                set
                {
                    if (_def != _undefined)
                        throw new PropertyRedefinedException();
                    _def = value;
                }
            }

            /// <summary>
            /// Gets the value of this store, assuming it is defined.
            /// </summary>
            public T GetValue()
            {
                return Value;
            }

            /// <summary>
            /// A sentinel value for <see cref="_def"/> which indicates that the store has not been defined yet.
            /// </summary>
            private static readonly Func<T> _undefined = () => throw new PropertyUndefinedException();
        }
        
        public static implicit operator Delay<T>(T value)
        {
            return new Delay<T>(value);
        }
    }

    /// <summary>
    /// Contains functions related to <see cref="Delay{T}"/>.
    /// </summary>
    public static class Delay
    {
        /// <summary>
        /// A helper function for implementing a getter for a delay-defined property. This allows the property to
        /// return a meaningful result prior to the property being set.
        /// </summary>
        /// <param name="field">A reference to the backing field for the delay-defined property</param>
        /// <param name="prop">A function which can be used to access the property being defined at a later time</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Delay<T> Get<T>(ref Delay<T> field)
        {
            if (field._source == null)
                field._source = Delay<T>.Store.Create();
            return field;
        }

        /// <summary>
        /// A helper function for implementing a setter for a delay-defined property.
        /// </summary>
        /// <param name="field">A reference to the backing field for the delay-defined property</param>
        /// <param name="value">The value the property is being set to</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Set<T>(ref Delay<T> field, Delay<T> value)
        {
            if (field._source == null)
                field = value;
            else if (value._source == Delay<T>._useValue)
                field._source.Value = value._value;
            else
                field._source.Definition = value._source.GetValue;
        }
    }

    /// <summary>
    /// An exception that is thrown when there is an attempt to set a property after its value has already
    /// been defined.
    /// </summary>
    public sealed class PropertyRedefinedException : Exception
    {
        public PropertyRedefinedException()
            : base("Attempt to redefine property")
        {

        }
    }

    /// <summary>
    /// An exception that is thrown when there is an attempt to use a property before sufficient information has
    /// been provided to define it.
    /// </summary>
    public sealed class PropertyUndefinedException : Exception
    {
        public PropertyUndefinedException()
            : base("The property has not been defined")
        {

        }
    }
}